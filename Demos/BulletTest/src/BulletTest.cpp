#include "GameStates/CMainState.h"
#include <Core/CCoreEngine.h>
#include <State/CStateManager.h>
#include <Renderer/CRenderManager.h>


int main(int argc, char *argv[])
{
	auto& core = CCoreEngine::Instance();

	core.StartUp();

	//Load Intro State
	CMainState intro_state;
	core.GetStateManager().ChangeState(&intro_state);

	core.Run();

	core.ShutDown();

	return 0;
}
