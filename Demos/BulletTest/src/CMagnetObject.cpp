#include "CMagnetObject.h"
#include <Renderer/CRenderDebug.h>
#include <OGRE\OgreVector3.h>
#include <OGRE\OgreColourValue.h>

CMagnetObject::CMagnetObject(btRigidBody* a_rigid_body, eMagnetType aMagnetType, f32 aMagnetForce) :
_rigid_body(a_rigid_body),
_magnet_type(aMagnetType),
_magnet_force(aMagnetForce)
{
}

CMagnetObject::~CMagnetObject()
{
}

void CMagnetObject::ApplyForce(f32 a_x_force, f32 a_y_force, f32 a_z_force)
{
	_rigid_body->applyCentralImpulse(btVector3(a_x_force, a_y_force, a_z_force));
}

void CMagnetObject::SetType(eMagnetType aMagnetType)
{
	_magnet_type = aMagnetType;
}

CMagnetObject::eMagnetType CMagnetObject :: GetType() const
{
	return _magnet_type;
}

f32 CMagnetObject::GetMagnetForce() const
{
	return _magnet_force;
}

btRigidBody* CMagnetObject::GetRigidBody() const
{
	return _rigid_body;
}

void CMagnetObject::DrawMagnetField() const
{
	if (HasMagnetProperty())
	{
		btTransform transform;
		_rigid_body->getMotionState()->getWorldTransform(transform);
		Ogre::ColourValue color = _magnet_type == MAGNET_TYPE_NEGATIVE ? Ogre::ColourValue::Red : Ogre::ColourValue::Blue;
		CRenderDebug::getSingleton().drawSphere(Ogre::Vector3(transform.getOrigin().getX(), transform.getOrigin().getY(), transform.getOrigin().getZ()), _magnet_force, color, true);
	}
}

void CMagnetObject::SwitchPolarity()
{
	_magnet_type = static_cast<eMagnetType>((_magnet_type + 1) % 3);
}

bool CMagnetObject::HasMagnetProperty() const
{
	return (_magnet_type != MAGNET_TYPE_NEUTRAL);
}