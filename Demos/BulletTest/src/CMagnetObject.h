#ifndef CMAGENTOBJECT_H_
#define CMAGENTOBJECT_H_

#include "btBulletDynamicsCommon.h"
#include "Core\CBasicTypes.h"

class CMagnetObject
{
public:
	enum eMagnetType
	{
		MAGNET_TYPE_POSITIVE,
		MAGNET_TYPE_NEGATIVE,
		MAGNET_TYPE_NEUTRAL
	};

	CMagnetObject(btRigidBody* a_rigid_body, eMagnetType aMagnetType, f32 aMagnetForce);
	~CMagnetObject();
	void ApplyForce(f32 a_x_force, f32 a_y_force, f32 a_z_force);
	void SetType(eMagnetType aMagnetType);
	eMagnetType GetType() const;
	f32 GetMagnetForce() const;
	btRigidBody* GetRigidBody() const;
	void DrawMagnetField() const;
	void SwitchPolarity();
	bool HasMagnetProperty() const;

private:
	btRigidBody*		_rigid_body;
	eMagnetType			_magnet_type;
	const f32			_magnet_force;

};

#endif 