#ifndef CMAGENTWORLD_H_
#define CMAGENTWORLD_H_

#include "btBulletDynamicsCommon.h"
#include "Core\CBasicTypes.h"
#include <vector>

class CMagnetObject;

class CMagnetWorld
{
public:
	CMagnetWorld();
	~CMagnetWorld();
	void Update(f64 dt);
	void AddMagnetObject(CMagnetObject* a_magnet_object);

private:
	typedef std::vector<CMagnetObject*>	tMagnetObjectVector;
	tMagnetObjectVector					m_magnet_objects;

};

#endif 