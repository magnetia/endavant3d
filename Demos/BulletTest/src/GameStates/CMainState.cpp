#include "CMainState.h"
#include "../CMagnetWorld.h"

#include <Core/CCoreEngine.h>
#include <Renderer/CRenderManager.h>
#include <Renderer/CRenderDebug.h>
#include <Input/CInputManager.h>
#include <Physics/CPhysicsManager.h>
#include <Physics/COgreMotionState.h>
#include <Camera/CCamera.h>
#include <Time/CTimeManager.h>

// OGRE
#include <OGRE/Ogre.h>

#include <OGRE/Overlay/OgreOverlayManager.h>
#include <OGRE/Overlay/OgreOverlayContainer.h>
#include <OGRE/Overlay/OgreTextAreaOverlayElement.h>
#include <OGRE/Overlay/OgreFontManager.h>
#include <OGRE/Overlay/OgreOverlay.h>


// Bullet
//#include "btBulletDynamicsCommon.h"

const f32 PI = 3.14159265f;

void cartesiancoord2sphericalcoord(f32 &a_radi, f32 &a_theta, f32 &a_phi, f32 a_x, f32 a_y, f32 a_z)
{
	a_radi = a_theta = a_phi = 0;
	a_radi = sqrt(a_x*a_x + a_y*a_y + a_z*a_z);
	if (!a_radi)
		return;
	a_theta = atan2(a_x, a_z);
	a_phi = acos(a_y / a_radi);
}

void sphericalcoord2cartesiancoord(f32 &a_x, f32 &a_y, f32 &a_z, f32 a_radi, f32 a_theta, f32 a_phi)
{
	a_x = a_y = a_z = 0;
	if (!a_radi)
		return;

	a_x = a_radi*sin(a_theta)*sin(a_phi);
	a_z = a_radi*cos(a_theta)*sin(a_phi);
	a_y = a_radi*cos(a_phi);
}

CMainState::CMainState() :
CState("MainState")
{
	auto scene_mgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();

	// Create camera
	Ogre::Camera *camera = scene_mgr->createCamera("MainCam");
	camera->setNearClipDistance(0.1f);
	camera->setFarClipDistance(1000);
	camera->setPosition(0, 150, 300);
	camera->lookAt(Ogre::Vector3::ZERO);

	// set viewport
	Ogre::Viewport *vp = CCoreEngine::Instance().GetRenderManager().GetRenderWindow()->addViewport(camera);
	vp->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 0.0));

	// Create a plane
	{
		Ogre::SceneNode* plane_scene_node = nullptr;
		// graphic
		{
			Ogre::Plane plane;
			plane.normal = Ogre::Vector3(0, 1, 0); plane.d = 0;
			Ogre::MeshManager::getSingleton().createPlane("FloorPlane", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
				plane, 200000, 200000, 20, 20, true, 1, 9000, 9000, Ogre::Vector3::UNIT_Z);

			Ogre::Entity* plane_entity = scene_mgr->createEntity("floor", "FloorPlane");
			plane_entity->setMaterialName("Ogre/Skin");


			plane_scene_node = scene_mgr->getRootSceneNode()->createChildSceneNode("HeadNode");
			plane_scene_node->getShowBoundingBox();
			plane_scene_node->attachObject(plane_entity);
		}

		// physic
		{
			btCollisionShape* groundShape = new btBoxShape(btVector3(btScalar(200.), btScalar(0.), btScalar(200.)));
			_collisionShapes.push_back(groundShape);

			btScalar mass(0.);
			btVector3 localInertia(0, 0, 0);

			plane_scene_node->setPosition(Ogre::Vector3(0.0f, 0.f, 0.0f));
			COgreMotionState* myMotionState = new COgreMotionState(plane_scene_node);
			btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, groundShape, localInertia);
			_plane = new btRigidBody(rbInfo);
			_rigid_bodies.push_back(_plane);

			//add the body to the dynamics world
			CCoreEngine::Instance().GetPhysicsManager().AddRigidBody(_plane);
		}
	}

	scene_mgr->setAmbientLight(Ogre::ColourValue(1.0f, 1.0f, 1.0f));

	Ogre::Light* light = scene_mgr->createLight("MainLight");
	light->setPosition(20, 80, 0);

	// Activate render debugger
	CRenderDebug::getSingleton().setEnabled(true);

	// Register keyboard keys
	CCoreEngine::Instance().GetInputManager().GetKeyboard().InsertKeyAction("ToggleDebugMode",	"L");
	CCoreEngine::Instance().GetInputManager().GetKeyboard().InsertKeyAction("CameraUp",			"W");
	CCoreEngine::Instance().GetInputManager().GetKeyboard().InsertKeyAction("CameraDown",		"S");
	CCoreEngine::Instance().GetInputManager().GetKeyboard().InsertKeyAction("CameraLeft",		"A");
	CCoreEngine::Instance().GetInputManager().GetKeyboard().InsertKeyAction("CameraRight",		"D");
	CCoreEngine::Instance().GetInputManager().GetKeyboard().InsertKeyAction("Reset",			"R");
	CCoreEngine::Instance().GetInputManager().GetKeyboard().InsertKeyAction("ZoomIn",			"Z");
	CCoreEngine::Instance().GetInputManager().GetKeyboard().InsertKeyAction("ZoomOut",			"X");
	CCoreEngine::Instance().GetInputManager().GetKeyboard().InsertKeyAction("Ball0_Toggle",		"0");
	CCoreEngine::Instance().GetInputManager().GetKeyboard().InsertKeyAction("Ball1_Toggle",		"1");
	CCoreEngine::Instance().GetInputManager().GetKeyboard().InsertKeyAction("Ball2_Toggle",		"2");
	CCoreEngine::Instance().GetInputManager().GetKeyboard().InsertKeyAction("Ball3_Toggle",		"3");
	CCoreEngine::Instance().GetInputManager().GetKeyboard().InsertKeyAction("Ball4_Toggle",		"4");
	CCoreEngine::Instance().GetInputManager().GetKeyboard().InsertKeyAction("Ball5_Toggle",		"5");
	CCoreEngine::Instance().GetInputManager().GetKeyboard().InsertKeyAction("Help",				"H");

	// Debug drawing
	CCoreEngine::Instance().GetPhysicsManager().SetDebugMode(btIDebugDraw::DBG_DrawWireframe);

	// Magnet world
	_magnet_world = new CMagnetWorld();

	// Creation magnet balls
	btScalar Radius = 20.;
	Ogre::Vector3 Position(0, Radius, 0);
	CreateBall(0, Position, 5., 20., CMagnetObject::MAGNET_TYPE_NEUTRAL);
	
	Position = Ogre::Vector3(-100, Radius, 0);
	CreateBall(1, Position, 0., 20., CMagnetObject::MAGNET_TYPE_NEUTRAL);
	
	Position = Ogre::Vector3(100, Radius, 0);
	CreateBall(2, Position, 0., 20., CMagnetObject::MAGNET_TYPE_NEUTRAL);

	Position = Ogre::Vector3(0, Radius, 100);
	CreateBall(3, Position, 0., 20., CMagnetObject::MAGNET_TYPE_NEUTRAL);

	Position = Ogre::Vector3(0, Radius, -100);
	CreateBall(4, Position, 0., 20., CMagnetObject::MAGNET_TYPE_NEUTRAL);

	Position = Ogre::Vector3(0, Radius*5, 0);
	CreateBall(5, Position, 0., 20., CMagnetObject::MAGNET_TYPE_NEUTRAL);

	CreateHelp();
}

void CMainState::CreateHelp()
{
	// CREATE A FONT
	// get the font manager
	Ogre::FontManager *fontMgr = Ogre::FontManager::getSingletonPtr();
	// create a font resource
	Ogre::FontPtr  font = fontMgr->create("MyFont", "General");
	// set as truetype
	font->setParameter("type", "truetype");
	// set the .ttf file name
	std::string font_name("arial.ttf");
	font->setParameter("source", font_name);
	// set the size
	font->setParameter("size", "18");
	// set the dpi
	font->setParameter("resolution", "96");
	// load the ttf
	font->load();

	// get the overlay manager
	Ogre::OverlayManager &overlayMgr = Ogre::OverlayManager::getSingleton();

	// Create a text area
	Ogre::TextAreaOverlayElement* textArea = static_cast<Ogre::TextAreaOverlayElement*>(
		overlayMgr.createOverlayElement("TextArea", "TextAreaName"));
	textArea->setMetricsMode(Ogre::GMM_PIXELS);
	textArea->setPosition(0, 0);
	textArea->setDimensions(300, 120);
	textArea->setCharHeight(26);
	// set the font name to the font resource that you just created.
	textArea->setFontName("MyFont");
	// say something
	textArea->setCaption("------ HELP ---------\nW, S, A, D -> Move spherical camera.\nZX -> ZoomIn/ZoomOut.\nL -> Toggle Debug Mode.\nR -> Reset.\n0-5 -> Toggle magnet field of balls.\nH -> Hide/Show help.");
	textArea->setColour(Ogre::ColourValue(1.0, 0.0, 0.0, 1.0));

	// Create an overlay, and add the panel
	Ogre::Overlay* overlay = overlayMgr.create("Help");
	overlay->add2D(reinterpret_cast<Ogre::OverlayContainer*>(textArea));
	overlay->show();
}

void CMainState::CreateBall(u32 a_Number, Ogre::Vector3 a_initial_position, btScalar a_mass, btScalar a_radius, CMagnetObject::eMagnetType a_magnet_type)
{
	auto scene_mgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();

	// Create a ball
	{
		Ogre::SceneNode* ball_scene_node = nullptr;
		btRigidBody* ball_rigid_body = nullptr;

		// graphic
		{
			Ogre::Entity* sphere = scene_mgr->createEntity(Ogre::SceneManager::PT_SPHERE);
			sphere->setMaterialName("Template/metal" + to_string(a_Number));

			ball_scene_node = scene_mgr->getRootSceneNode()->createChildSceneNode("HeadNode" + to_string(a_Number));
			ball_scene_node->getShowBoundingBox();
			ball_scene_node->scale(0.4f, 0.4f, 0.4f);
			ball_scene_node->attachObject(sphere);
		}

		// physic
		{
			btScalar mass(a_mass);
			btVector3 localInertia(0, 0, 0);

			ball_scene_node->setPosition(a_initial_position);
			COgreMotionState* myMotionState = new COgreMotionState(ball_scene_node);

			btSphereShape* colShape = new btSphereShape(a_radius);
			_collisionShapes.push_back(colShape);

			if(mass)
				colShape->calculateLocalInertia(mass, localInertia);

			btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);
			ball_rigid_body = new btRigidBody(rbInfo);
			_rigid_bodies.push_back(ball_rigid_body);

			//add the body to the dynamics world
			CCoreEngine::Instance().GetPhysicsManager().AddRigidBody(ball_rigid_body);

			CMagnetObject* ball = new CMagnetObject(ball_rigid_body, a_magnet_type, 55.);
			_magnet_world->AddMagnetObject(ball);
			_magnet_objects.push_back(ball);
		}
	}
}

CMainState::~CMainState()
{
	//delete collision shapes
	for (int j = 0; j<_collisionShapes.size(); j++)
	{
		btCollisionShape* shape = _collisionShapes[j];
		_collisionShapes[j] = nullptr;
		delete shape;
	}

	for (tMagnetObjectVector::iterator it = _magnet_objects.begin(); it != _magnet_objects.end(); ++it)
	{
		delete *it;
	}

	delete _magnet_world;
}

void CMainState::Start()
{

}
void CMainState::Finish()
{

}
void CMainState::Pause()
{

}
void CMainState::Resume()
{

}
void CMainState::Update(f64 dt)
{
	_magnet_world->Update(dt);

	CRenderDebug::getSingleton().drawLine(Ogre::Vector3(0, 0, 0), Ogre::Vector3(50, 0, 0), Ogre::ColourValue::Red);
	CRenderDebug::getSingleton().drawLine(Ogre::Vector3(0, 0, 0), Ogre::Vector3(0, 50, 0), Ogre::ColourValue::Green);
	CRenderDebug::getSingleton().drawLine(Ogre::Vector3(0, 0, 0), Ogre::Vector3(0, 0, 50), Ogre::ColourValue::Blue);

	if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("ToggleDebugMode"))
	{
		CRenderDebug::getSingleton().switchEnabled();
		CCoreEngine::Instance().GetPhysicsManager().SwitchDebugModeEnabled();
		
	}
	
	if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyPushed("CameraUp"))
	{
		Ogre::Vector3 pos = CCoreEngine::Instance().GetRenderManager().GetSceneManager()->getCamera("MainCam")->getPosition();
		
		Ogre::Real radi = 0, theta = 0, phi = 0;
		cartesiancoord2sphericalcoord(radi, theta, phi, pos.x, pos.y, pos.z);
		phi -= 0.01f;
		if (phi < 0.01)
			phi = 0.01f;
		sphericalcoord2cartesiancoord(pos.x, pos.y, pos.z, radi, theta, phi);

		CCoreEngine::Instance().GetRenderManager().GetSceneManager()->getCamera("MainCam")->setPosition(pos);
		CCoreEngine::Instance().GetRenderManager().GetSceneManager()->getCamera("MainCam")->lookAt(Ogre::Vector3::ZERO);
	}

	if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyPushed("CameraDown"))
	{
		Ogre::Vector3 pos = CCoreEngine::Instance().GetRenderManager().GetSceneManager()->getCamera("MainCam")->getPosition();
		
		Ogre::Real radi = 0, theta = 0, phi = 0;
		cartesiancoord2sphericalcoord(radi, theta, phi, pos.x, pos.y, pos.z);
		phi += 0.01f;
		if (phi > 3.13f)
			phi = 3.13f;
		sphericalcoord2cartesiancoord(pos.x, pos.y, pos.z, radi, theta, phi);

		CCoreEngine::Instance().GetRenderManager().GetSceneManager()->getCamera("MainCam")->setPosition(pos);
		CCoreEngine::Instance().GetRenderManager().GetSceneManager()->getCamera("MainCam")->lookAt(Ogre::Vector3::ZERO);
	}

	if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyPushed("CameraLeft"))
	{
		Ogre::Vector3 pos = CCoreEngine::Instance().GetRenderManager().GetSceneManager()->getCamera("MainCam")->getPosition();
		
		Ogre::Real radi = 0, theta = 0, phi = 0;
		cartesiancoord2sphericalcoord(radi, theta, phi, pos.x, pos.y, pos.z);
		theta -= 0.01f;
		sphericalcoord2cartesiancoord(pos.x, pos.y, pos.z, radi, theta, phi);

		CCoreEngine::Instance().GetRenderManager().GetSceneManager()->getCamera("MainCam")->setPosition(pos);
		CCoreEngine::Instance().GetRenderManager().GetSceneManager()->getCamera("MainCam")->lookAt(Ogre::Vector3::ZERO);
	}

	if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyPushed("CameraRight"))
	{
		Ogre::Vector3 pos = CCoreEngine::Instance().GetRenderManager().GetSceneManager()->getCamera("MainCam")->getPosition();
		
		Ogre::Real radi = 0, theta = 0, phi = 0;
		cartesiancoord2sphericalcoord(radi, theta, phi, pos.x, pos.y, pos.z);
		theta += 0.01f;
		sphericalcoord2cartesiancoord(pos.x, pos.y, pos.z, radi, theta, phi);
		
		CCoreEngine::Instance().GetRenderManager().GetSceneManager()->getCamera("MainCam")->setPosition(pos);
		CCoreEngine::Instance().GetRenderManager().GetSceneManager()->getCamera("MainCam")->lookAt(Ogre::Vector3::ZERO);
	}

	if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyPushed("Reset"))
	{
		CCoreEngine::Instance().GetPhysicsManager().Reset();
		for (tMagnetObjectVector::iterator it = _magnet_objects.begin(); it != _magnet_objects.end(); ++it)
		{
			(*it)->SetType(CMagnetObject::MAGNET_TYPE_NEUTRAL);
		}
	}

	if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyPushed("ZoomIn"))
	{
		Ogre::Vector3 pos = CCoreEngine::Instance().GetRenderManager().GetSceneManager()->getCamera("MainCam")->getPosition();
		Ogre::Real radi = 0, theta = 0, fi = 0;
		cartesiancoord2sphericalcoord(radi, theta, fi, pos.x, pos.y, pos.z);

		if (radi > 1.0f)
		{
			radi -= 1.0f;
		}

		sphericalcoord2cartesiancoord(pos.x, pos.y, pos.z, radi, theta, fi);

		CCoreEngine::Instance().GetRenderManager().GetSceneManager()->getCamera("MainCam")->setPosition(pos);
		CCoreEngine::Instance().GetRenderManager().GetSceneManager()->getCamera("MainCam")->lookAt(Ogre::Vector3::ZERO);
	}

	if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyPushed("ZoomOut"))
	{
		Ogre::Vector3 pos = CCoreEngine::Instance().GetRenderManager().GetSceneManager()->getCamera("MainCam")->getPosition();
		Ogre::Real radi = 0, theta = 0, fi = 0;
		cartesiancoord2sphericalcoord(radi, theta, fi, pos.x, pos.y, pos.z);

		if (radi < 2000.0f)
		{
			radi += 1.0f;
		}

		sphericalcoord2cartesiancoord(pos.x, pos.y, pos.z, radi, theta, fi);

		CCoreEngine::Instance().GetRenderManager().GetSceneManager()->getCamera("MainCam")->setPosition(pos);
		CCoreEngine::Instance().GetRenderManager().GetSceneManager()->getCamera("MainCam")->lookAt(Ogre::Vector3::ZERO);
	}
	if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("Ball0_Toggle"))
	{
		if (_magnet_objects.size() > 0)
			_magnet_objects.at(0)->SwitchPolarity();
	}
	if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("Ball1_Toggle"))
	{
		if (_magnet_objects.size() > 1)
			_magnet_objects.at(1)->SwitchPolarity();
	}
	if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("Ball2_Toggle"))
	{
		if (_magnet_objects.size() > 2)
			_magnet_objects.at(2)->SwitchPolarity();
	}
	if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("Ball3_Toggle"))
	{
		if (_magnet_objects.size() > 3)
			_magnet_objects.at(3)->SwitchPolarity();
	}
	if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("Ball4_Toggle"))
	{
		if (_magnet_objects.size() > 4)
			_magnet_objects.at(4)->SwitchPolarity();
	}
	if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("Ball5_Toggle"))
	{
		if (_magnet_objects.size() > 5)
			_magnet_objects.at(5)->SwitchPolarity();
	}
	if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("Help"))
	{
		static bool visibility = false;
		Ogre::Overlay* overlay = Ogre::OverlayManager::getSingleton().getByName("Help");
		if (visibility)
		{
			overlay->show();
		}
		else
		{
			overlay->hide();
		}
		visibility = !visibility;
	}
}