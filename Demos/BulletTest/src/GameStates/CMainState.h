#ifndef CINTROGAMESTATE_H_
#define CINTROGAMESTATE_H_

#include <../Engine/Core/CBasicTypes.h>
#include <../Engine/State/CState.h>
#include <btBulletDynamicsCommon.h>
#include <vector>
#include "../CMagnetObject.h"
#include <OGRE/OgreVector3.h>

class CMagnetObject;
class CMagnetWorld;

class CMainState : public CState
{
public:
	CMainState();
	~CMainState();

	void Start() override;
	void Finish() override;
	void Pause() override;
	void Resume() override;
	void Update(f64 dt) override;


private:
	void CreateHelp();
	void CreateBall(u32 a_Number, Ogre::Vector3 a_initial_position, btScalar a_mass, btScalar a_radius, CMagnetObject::eMagnetType a_magnet_type);

	typedef std::vector<btRigidBody*>	tRigidBodiesVector;
	tRigidBodiesVector					_rigid_bodies;
	btAlignedObjectArray<btCollisionShape*> _collisionShapes;

	btRigidBody* _plane;
	typedef std::vector<CMagnetObject*> tMagnetObjectVector;
	tMagnetObjectVector _magnet_objects;
	CMagnetWorld* _magnet_world;
};


#endif