#include "CMagnetWorld.h"
#include "CMagnetObject.h"


CMagnetWorld::CMagnetWorld()
{

}
	
CMagnetWorld::~CMagnetWorld()
{
}
	
void CMagnetWorld::Update(f64 dt)
{
	for (tMagnetObjectVector::iterator it_subject = m_magnet_objects.begin(); it_subject != m_magnet_objects.end(); it_subject++)
	{
		if ((*it_subject)->HasMagnetProperty() && !(*it_subject)->GetRigidBody()->isStaticOrKinematicObject())
		{
			for (tMagnetObjectVector::iterator it_effect = m_magnet_objects.begin(); it_effect != m_magnet_objects.end(); it_effect++)
			{
				if (it_subject != it_effect && (*it_effect)->HasMagnetProperty())
				{
					// Compute distance
					btTransform transform1, transform2;
					(*it_subject)->GetRigidBody()->getMotionState()->getWorldTransform(transform1);
					(*it_effect)->GetRigidBody()->getMotionState()->getWorldTransform(transform2);
					f32 distance = std::sqrt(std::pow(transform1.getOrigin().getX() - transform2.getOrigin().getX(), 2) + std::pow(transform1.getOrigin().getY() - transform2.getOrigin().getY(), 2) + std::pow(transform1.getOrigin().getZ() - transform2.getOrigin().getZ(), 2));
					f32 magnet_fields_total = (*it_effect)->GetMagnetForce() + (*it_subject)->GetMagnetForce();
					if (distance <= magnet_fields_total)
					{
						// Apply magnet force
						s8 factor = ((*it_subject)->GetType() == (*it_effect)->GetType()) ? 1 : -1;
						f32 x_force = (transform1.getOrigin().getX() - transform2.getOrigin().getX()) / 30.f * factor;
						f32 y_force = (transform1.getOrigin().getY() - transform2.getOrigin().getY()) / 30.f * factor;
						f32 z_force = (transform1.getOrigin().getZ() - transform2.getOrigin().getZ()) / 30.f * factor;
						(*it_subject)->ApplyForce(x_force, y_force, z_force);
						(*it_subject)->GetRigidBody()->forceActivationState(ACTIVE_TAG);
						(*it_subject)->GetRigidBody()->activate();
					}
				}
			}
		}

		(*it_subject)->DrawMagnetField();
	}
}

void CMagnetWorld::AddMagnetObject(CMagnetObject* a_magnet_object)
{
	m_magnet_objects.push_back(a_magnet_object);
}
