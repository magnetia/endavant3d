#include "CIntroGameState.h"
#include <Core/CCoreEngine.h>
#include <Renderer/CRenderManager.h>
#include <Renderer/CRenderDebug.h>

// OGRE
#include <OGRE/Ogre.h>

#include <OGRE/Overlay/OgreOverlayManager.h>
#include <OGRE/Overlay/OgreOverlayContainer.h>
#include <OGRE/Overlay/OgreTextAreaOverlayElement.h>
#include <OGRE/Overlay/OgreFontManager.h>
#include <OGRE/Overlay/OgreOverlay.h>
#include "../CharacterController.h"


#include "Input\CInputManager.h"
#include "Core\CCoreEngine.h"

// #debug:
#include "Utils/buffer.h"

CCharacterController * m_character;

CIntroGameState::CIntroGameState()
{
}

void CIntroGameState::Start()
{

	auto &keyb = CCoreEngine::Instance().GetInputManager().GetKeyboard();
	keyb.InsertKeyAction("CAM_CHANGE", "C");

	// add a camera
	auto scene_mgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();

	Ogre::Camera *m_camera = scene_mgr->createCamera("MainCam");
	m_camera->setNearClipDistance(0.1f);

	m_character = new CCharacterController(m_camera);

	//Cameres!
	m_freecamcontroller = std::unique_ptr<CCameraFree>(new CCameraFree("FreeCamera"));
	m_followcamcontroller = std::unique_ptr<CCameraFollower>(new CCameraFollower("FollowCharCam", m_character->m_BodyNode));
	m_followcamspherical = std::unique_ptr<CCameraFollowerSpherical>(new CCameraFollowerSpherical("FollowCharCamSpherical", m_character->m_BodyNode));
	m_freecamcontroller->SetActive();
	
	



	auto ogreroot = CCoreEngine::Instance().GetRenderManager().GetOgreRoot();

	// CREATE A FONT
	// get the font manager
	Ogre::FontManager *fontMgr = Ogre::FontManager::getSingletonPtr();
	// create a font resource
	Ogre::FontPtr  font = fontMgr->create("MyFont", "General");
	// set as truetype
	font->setParameter("type", "truetype");
	// set the .ttf file name
	std::string font_name("arial.ttf");
	font->setParameter("source", font_name);
	// set the size
	font->setParameter("size", "12");
	// set the dpi
	font->setParameter("resolution", "96");
	// load the ttf
	font->load();

	// CREATE A FONT
	// create a font resource
	Ogre::FontPtr  font2 = fontMgr->create("MyFont", "General");
	
	// set as truetype
	font2->setParameter("type", "truetype");
	// set the .ttf file name
	
	font2->setParameter("source", font_name);
	// set the size
	font2->setParameter("size", "26");
	// set the dpi
	font2->setParameter("resolution", "96");
	// load the ttf
	font2->load();

	// get the overlay manager
	Ogre::OverlayManager &overlayMgr = Ogre::OverlayManager::getSingleton();

	// Create a panel
	Ogre::OverlayContainer* panel = static_cast<Ogre::OverlayContainer*>(
		overlayMgr.createOverlayElement("Panel", "PanelName"));
	panel->setMetricsMode(Ogre::GMM_PIXELS);
	panel->setPosition(10, 10);
	panel->setDimensions(300, 120);
	panel->setColour(Ogre::ColourValue(1.0, 1.0, 0.0, 0.0));
	panel->show();




	//	 // Create a text area
	Ogre::TextAreaOverlayElement* textArea = static_cast<Ogre::TextAreaOverlayElement*>(
		overlayMgr.createOverlayElement("TextArea", "TextAreaName"));
	textArea->setMetricsMode(Ogre::GMM_PIXELS);
	textArea->setPosition(0, 0);
	textArea->setDimensions(300, 120);
	textArea->setCharHeight(26);
	// set the font name to the font resource that you just created.
	textArea->setFontName("MyFont");
	// say something
	textArea->setCaption("HELLO WORLD OGRE!!");
	textArea->setColour(Ogre::ColourValue(1.0, 1.0, 1.0, 1.0));




	// Create an overlay, and add the panel
	Ogre::Overlay* overlay = overlayMgr.create("OverlayName");
	overlay->add2D(reinterpret_cast<Ogre::OverlayContainer*>(textArea));



	// Add the text area to the panel
	//panel->addChild(textArea);


	// Show the overlay
	overlay->show();

	//LLUM
	scene_mgr->setAmbientLight(Ogre::ColourValue(0.0f,0.0f,0.0f));
	scene_mgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
	
	Ogre::Light* light = scene_mgr->createLight("MainLight");
	light->setPosition(0, 100,100);

	// Activate render debugger
	CRenderDebug::getSingleton().setEnabled(true);
	/*
	// graphic plane
	{
		Ogre::SceneNode* plane_scene_node = nullptr;
		Ogre::Plane plane;
		plane.normal = Ogre::Vector3(0, 1, 0); plane.d = 0;
		Ogre::MeshManager::getSingleton().createPlane("FloorPlane", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
			plane, 2000, 2000, 20, 20, true, 1, 10, 10, Ogre::Vector3::UNIT_Z);

		Ogre::Entity* plane_entity = scene_mgr->createEntity("floor", "FloorPlane");
		plane_entity->setMaterialName("Ogre/Floor");


		plane_scene_node = scene_mgr->getRootSceneNode()->createChildSceneNode("PlaY");
		plane_scene_node->getShowBoundingBox();
		plane_scene_node->attachObject(plane_entity);
	}
	*/
	// graphic plane
	{
		Ogre::SceneNode* plane_scene_node = nullptr;
		Ogre::Plane plane;
		plane.normal = Ogre::Vector3(0, 0, 1); plane.d = 0;
		Ogre::MeshManager::getSingleton().createPlane("FloorPlane2Z",Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,plane, 2000, 2000, 20, 20, true, 1, 10, 10, Ogre::Vector3::UNIT_Y);

		Ogre::Entity* plane_entity = scene_mgr->createEntity("floor2Z", "FloorPlane2Z");
		plane_entity->setMaterialName("Ogre/Floor");


		plane_scene_node = scene_mgr->getRootSceneNode()->createChildSceneNode("PlaZ");
		plane_scene_node->getShowBoundingBox();
		plane_scene_node->attachObject(plane_entity);
	}

}
void CIntroGameState::Finish()
{
	edv::dynamic_buffer dyn_buffer{ 2, 5U, "string" };
	edv::buffer<100> static_buffer{ 2, 5U, "string" };

	std::string test("prueba");
	dyn_buffer.write(test);
	std::cout << test;

	std::string test2;
	dyn_buffer.rewind();
	dyn_buffer.read(&test2);


}
void CIntroGameState::Pause()
{

}
void CIntroGameState::Resume()
{

}
void CIntroGameState::Update(f64 dt)
{
	CRenderDebug::getSingleton().drawLine(Ogre::Vector3(0, 0, 0), Ogre::Vector3(50, 0, 0), Ogre::ColourValue::Red);
	CRenderDebug::getSingleton().drawLine(Ogre::Vector3(0, 0, 0), Ogre::Vector3(0, 50, 0), Ogre::ColourValue::Green);
	CRenderDebug::getSingleton().drawLine(Ogre::Vector3(0, 0, 0), Ogre::Vector3(0, 0, 50), Ogre::ColourValue::Blue);

	
	m_character->Update(dt);



	static u32 cam_mode = 0;

	auto &keyb = CCoreEngine::Instance().GetInputManager().GetKeyboard();
	// Move camera upwards along to world's Y-axis.
	if (keyb.IsActionKeyDown("CAM_CHANGE"))
	{
		cam_mode++;
		cam_mode %= 3;
		if (cam_mode == 0)
			m_freecamcontroller->SetActive();
		else if (cam_mode == 1)
			m_followcamcontroller->SetActive();
		else
			m_followcamspherical->SetActive();
				
	}


	if (cam_mode == 0)
		m_freecamcontroller->Update(dt);
	else if (cam_mode == 1)
		m_followcamcontroller->Update(dt);
	else
		m_followcamspherical->Update(dt);



	std::cout<< "cam_mode: "<< cam_mode <<std::endl;

}