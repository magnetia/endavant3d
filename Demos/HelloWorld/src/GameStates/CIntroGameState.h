#ifndef CINTROGAMESTATE_H_
#define CINTROGAMESTATE_H_

#include <memory>
#include "State/CState.h"
#include "Utils/CSingleton.h"
#include "Camera/CCameraControllerFree.h"
#include "Camera/CCameraFollowerSpherical.h"
#include "Camera/CCameraFollower.h"


class CIntroGameState : public CState, public CSingleton<CIntroGameState>
{
public:
	CIntroGameState();

	void Start() override;
	void Finish() override;
	void Pause() override;
	void Resume() override;
	void Update(f64 dt) override;


private:
	std::unique_ptr<CCameraFree> m_freecamcontroller;
	std::unique_ptr<CCameraFollower> m_followcamcontroller;
	std::unique_ptr<CCameraFollowerSpherical> m_followcamspherical;
	
};


#endif
