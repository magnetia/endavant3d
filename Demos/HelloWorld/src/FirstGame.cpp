#include <Core/CCoreEngine.h>
#include <State/CStateManager.h>
#include "GameStates/CIntroGameState.h"

int main(int argc, char *argv[])
{
	auto& core = CCoreEngine::Instance();

	core.StartUp();

	core.GetStateManager().ChangeState(CIntroGameState::Pointer());
	core.Run();

	core.ShutDown();

	return 0;
}
