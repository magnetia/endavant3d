#include "CharacterController.h"
#include "Input\CInputManager.h"
#include "Core\CCoreEngine.h"

CCharacterController::CCharacterController(Ogre::Camera* _cam)
{
	SetupModel(_cam->getSceneManager());
	//SetupCameraController(_cam);
	//setupAnimations();



	
	auto &keyb = CCoreEngine::Instance().GetInputManager().GetKeyboard();
	keyb.InsertKeyAction("RIGHT", "K");
	keyb.InsertKeyAction("LEFT", "H");
	keyb.InsertKeyAction("UP", "U");
	keyb.InsertKeyAction("DOWN", "J");

	keyb.InsertKeyAction("JUMP", "N");
	keyb.InsertKeyAction("CROUCH", "M");
	

}


CCharacterController::~CCharacterController()
{
}


void CCharacterController::SetupModel(Ogre::SceneManager * _sceneMgr)
{
	// creo el model
	m_BodyNode = _sceneMgr->getRootSceneNode()->createChildSceneNode(Ogre::Vector3::UNIT_Y * 20);
	m_BodyEntity = _sceneMgr->createEntity("Head", "ogrehead.MESH");
	m_BodyNode->attachObject(m_BodyEntity);

	



	m_keyDirection = Ogre::Vector3::ZERO;
	m_VerticalSpeed = 0;
}

void CCharacterController::SetupCameraController(Ogre::Camera * _cam)
{
	// create a pivot at roughly the character's shoulder
	m_CameraPivot = _cam->getSceneManager()->getRootSceneNode()->createChildSceneNode();
	// this is where the camera should be soon, and it spins around the pivot
	m_CameraGoal = m_CameraPivot->createChildSceneNode(Ogre::Vector3(0, 0, 500));
	// this is where the camera actually is
	m_CameraNode = _cam->getSceneManager()->getRootSceneNode()->createChildSceneNode();
	m_CameraNode->setPosition(m_CameraPivot->getPosition() + m_CameraGoal->getPosition());

	m_CameraPivot->setFixedYawAxis(true);
	m_CameraGoal->setFixedYawAxis(true);
	m_CameraNode->setFixedYawAxis(true);

	// Set Clipping planes
	_cam->setNearClipDistance(0.1f);
	_cam->setFarClipDistance(10000);
	m_CameraNode->attachObject(_cam);

	m_pivotPicth = 0;

}

void CCharacterController::Update(f64 dt)
{
	auto &keyb = CCoreEngine::Instance().GetInputManager().GetKeyboard();
	auto movement = static_cast<Ogre::Real>(100.0 * dt);

	if (keyb.IsActionKeyPushed("RIGHT"))
	{
		m_BodyNode->translate(Ogre::Vector3(movement, 0.0, 0.0));
	}

	if (keyb.IsActionKeyPushed("LEFT"))
	{
		m_BodyNode->translate(Ogre::Vector3(-movement, 0.0, 0.0));
	}

	if (keyb.IsActionKeyPushed("DOWN"))
	{
		m_BodyNode->translate(Ogre::Vector3(0.0, 0.0, movement));
	}

	if (keyb.IsActionKeyPushed("UP"))
	{
		m_BodyNode->translate(Ogre::Vector3(0.0, 0.0, -movement));
	}

	if (keyb.IsActionKeyPushed("JUMP"))
	{
		m_BodyNode->translate(Ogre::Vector3(0.0, movement, 0.0));
	}

	if (keyb.IsActionKeyPushed("CROUCH"))
	{
		m_BodyNode->translate(Ogre::Vector3(0.0, -movement,0.0));
	}

	/*
	auto &mouse= CCoreEngine::Instance().GetInputManager().GetMouse();
	updateCameraGoal(-0.50f * mouse.GetPosRelX(), -0.50f * mouse.GetPosRelY(), 0);


	// place the camera pivot roughly at the character's shoulder
	m_CameraPivot->setPosition(m_BodyNode->getPosition() + Ogre::Vector3::UNIT_Y * 5);
	// move the camera smoothly to the goal
	Ogre::Vector3 goalOffset = m_CameraGoal->_getDerivedPosition() - m_CameraNode->getPosition();
	m_CameraNode->translate(goalOffset *static_cast<f32>(dt)* 9.0f);
	// always look at the pivot
	m_CameraNode->lookAt(m_CameraPivot->_getDerivedPosition(), Ogre::Node::TS_WORLD);*/

}

void CCharacterController::updateCameraGoal(Ogre::Real deltaYaw, Ogre::Real deltaPitch, Ogre::Real deltaZoom)
{
	m_CameraPivot->yaw(Ogre::Degree(deltaYaw), Ogre::Node::TS_WORLD);

	// bound the pitch
	if (!(m_pivotPicth + deltaPitch > -10 && deltaPitch > 0) &&
		!(m_pivotPicth + deltaPitch < -60 && deltaPitch < 0))
	{
		m_CameraPivot->pitch(Ogre::Degree(deltaPitch), Ogre::Node::TS_LOCAL);
		m_pivotPicth += deltaPitch;
	}

	Ogre::Real dist = m_CameraGoal->_getDerivedPosition().distance(m_CameraPivot->_getDerivedPosition());
	Ogre::Real distChange = deltaZoom * dist;

	// bound the zoom
	if (!(dist + distChange < 8 && distChange < 0) &&
		!(dist + distChange > 25 && distChange > 0))
	{
		m_CameraGoal->translate(0, 0, distChange, Ogre::Node::TS_LOCAL);
	}
}