#include <Core/CCoreEngine.h>
#include <State/CStateManager.h>
#include "GameStates/CMenuGameState.h"

int main(int argc, char *argv[])
{
	auto& core = CCoreEngine::Instance();

	core.StartUp();

	core.GetStateManager().ChangeState(CMenuGameState::Pointer());
	core.Run();

	core.ShutDown();

	return 0;
}
