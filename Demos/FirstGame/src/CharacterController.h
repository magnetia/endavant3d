#ifndef CCharacterController_H_
#define CCharacterController_H_

#include "OGRE\Ogre.h"
#include "Core\CBasicTypes.h"

class CCharacterController
{
public:
	CCharacterController(Ogre::Camera* _cam);
	~CCharacterController();
	void Update(f64 dt);

private:
	//Carrega el model
	void SetupModel(Ogre::SceneManager * _sceneMgr);
	void SetupCameraController(Ogre::Camera * _cam);

	void updateCameraGoal(Ogre::Real deltaYaw, Ogre::Real deltaPitch, Ogre::Real deltaZoom);

	// Model
	Ogre::Camera*		mCamera;
	Ogre::SceneNode*	m_BodyNode; //Node de la malla
	Ogre::Entity*		m_BodyEntity;

	Ogre::Vector3		m_keyDirection;
	Ogre::Vector3		m_targetDirection;
	Ogre::Real			m_VerticalSpeed;	//Per saltar



	// Camera

	Ogre::SceneNode* m_CameraPivot;
	Ogre::SceneNode* m_CameraGoal;
	Ogre::SceneNode* m_CameraNode;

	Ogre::Real		m_pivotPicth;
	/*  //SceneNode* mBodyNode;


	SceneNode* mCameraPivot;
	SceneNode* mCameraGoal;
	SceneNode* mCameraNode;
	Real mPivotPitch;
	Entity* mBodyEnt;
	Entity* mSword1;
	Entity* mSword2;
	RibbonTrail* mSwordTrail;
	AnimationState* mAnims[NUM_ANIMS];    // master animation list
	AnimID mBaseAnimID;                   // current base (full- or lower-body) animation
	AnimID mTopAnimID;                    // current top (upper-body) animation
	bool mFadingIn[NUM_ANIMS];            // which animations are fading in
	bool mFadingOut[NUM_ANIMS];           // which animations are fading out
	bool mSwordsDrawn;
	Vector3 mKeyDirection;      // player's local intended direction based on WASD keys
	Vector3 mGoalDirection;     // actual intended direction in world-space
	Real mVerticalVelocity;     // for jumping
	Real mTimer;                // general timer to see how long animations have been playing*/
};

#endif