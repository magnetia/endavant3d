#include "BasicController.h"
#include "Core/CCoreEngine.h"
#include "Input/CInputManager.h"
#include "Game/GameEntity.h"
#include <btBulletDynamicsCommon.h>

const std::string actionLeft = "CONTROLLER_LEFT";
const std::string actionRight = "CONTROLLER_RIGHT";
const std::string actionUp = "CONTROLLER_UP";
const std::string actionDown = "CONTROLLER_DOWN";

BasicController::BasicController(f32 a_speed) :
	m_speed(a_speed)
{

}

Behavior* BasicController::clone()
{
	return new BasicController(m_speed);
}

void BasicController::initImpl()
{
	auto& keyboard = CCoreEngine::Instance().GetInputManager().GetKeyboard();

	keyboard.InsertKeyAction(actionLeft, "A");
	keyboard.InsertKeyAction(actionRight, "D");
	keyboard.InsertKeyAction(actionUp, "W");
	keyboard.InsertKeyAction(actionDown, "S");
}

void BasicController::updateImpl(f64 dt)
{
	auto& keyboard = CCoreEngine::Instance().GetInputManager().GetKeyboard();
	btRigidBody* body = getParent()->getPhysics()->getRigidBody();

	if (keyboard.IsActionKeyPushed(actionLeft))
	{
		body->setLinearVelocity(btVector3(-m_speed, m_speed / 2, 0));
	}

	if (keyboard.IsActionKeyPushed(actionRight))
	{
		body->setLinearVelocity(btVector3(m_speed, m_speed / 2, 0));
	}
}

void BasicController::stopImpl()
{
	auto& keyboard = CCoreEngine::Instance().GetInputManager().GetKeyboard();

	keyboard.RemoveKeyAction(actionLeft);
	keyboard.RemoveKeyAction(actionRight);
	keyboard.RemoveKeyAction(actionUp);
	keyboard.RemoveKeyAction(actionDown);
}

void BasicController::pauseImpl()
{

}

void BasicController::resumeImpl()
{

}
