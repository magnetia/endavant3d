#ifndef CINTROGAMESTATE_H_
#define CINTROGAMESTATE_H_

#include "State/CState.h"
#include "Utils/CSingleton.h"

class CCameraController2D;
class CCharacterController;

class CIntroGameState : public CState, public CSingleton<CIntroGameState>
{
public:
	CIntroGameState();

	void Start() override;
	void Finish() override;
	void Pause() override;
	void Resume() override;
	void Update(f64 dt) override;


private:

	CCameraController2D *m_cameracontroller;
	CCharacterController *m_character;
};


#endif