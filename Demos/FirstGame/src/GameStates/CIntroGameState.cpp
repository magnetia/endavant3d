#include "CIntroGameState.h"
#include <Core/CCoreEngine.h>
#include <Renderer/CRenderManager.h>

// OGRE
#include <OGRE/Ogre.h>

#include <OGRE/Overlay/OgreOverlayManager.h>
#include <OGRE/Overlay/OgreOverlayContainer.h>
#include <OGRE/Overlay/OgreTextAreaOverlayElement.h>
#include <OGRE/Overlay/OgreFontManager.h>
#include <OGRE/Overlay/OgreOverlay.h>
#include "Camera/CCamera.h"
#include "../CharacterController.h"

CIntroGameState::CIntroGameState()
{
}

void CIntroGameState::Start()
{
	// add a camera
	
	auto scene_mgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();

	Ogre::Camera *m_camera = scene_mgr->createCamera("MainCam");
	m_character = new CCharacterController(m_camera);
	
	// set viewport
	Ogre::Viewport *vp = CCoreEngine::Instance().GetRenderManager().GetRenderWindow()->addViewport(m_camera);
	vp->setBackgroundColour(Ogre::ColourValue(250.0, 0.0, 0.0));
	



	auto ogreroot = CCoreEngine::Instance().GetRenderManager().GetOgreRoot();

	// CREATE A FONT
	// get the font manager
	Ogre::FontManager *fontMgr = Ogre::FontManager::getSingletonPtr();
	// create a font resource
	Ogre::FontPtr  font = fontMgr->create("MyFont", "General");
	// set as truetype
	font->setParameter("type", "truetype");
	// set the .ttf file name
	std::string font_name("arial.ttf");
	font->setParameter("source", font_name);
	// set the size
	font->setParameter("size", "12");
	// set the dpi
	font->setParameter("resolution", "96");
	// load the ttf
	font->load();

	// CREATE A FONT
	// create a font resource
	Ogre::FontPtr  font2 = fontMgr->create("MyFont", "General");
	
	// set as truetype
	font2->setParameter("type", "truetype");
	// set the .ttf file name
	
	font2->setParameter("source", font_name);
	// set the size
	font2->setParameter("size", "26");
	// set the dpi
	font2->setParameter("resolution", "96");
	// load the ttf
	font2->load();

	// get the overlay manager
	Ogre::OverlayManager &overlayMgr = Ogre::OverlayManager::getSingleton();

	// Create a panel
	Ogre::OverlayContainer* panel = static_cast<Ogre::OverlayContainer*>(
		overlayMgr.createOverlayElement("Panel", "PanelName"));
	panel->setMetricsMode(Ogre::GMM_PIXELS);
	panel->setPosition(10, 10);
	panel->setDimensions(300, 120);
	panel->setColour(Ogre::ColourValue(1.0, 1.0, 0.0, 0.0));
	panel->show();




	//	 // Create a text area
	Ogre::TextAreaOverlayElement* textArea = static_cast<Ogre::TextAreaOverlayElement*>(
		overlayMgr.createOverlayElement("TextArea", "TextAreaName"));
	textArea->setMetricsMode(Ogre::GMM_PIXELS);
	textArea->setPosition(0, 0);
	textArea->setDimensions(300, 120);
	textArea->setCharHeight(26);
	// set the font name to the font resource that you just created.
	textArea->setFontName("MyFont");
	// say something
	textArea->setCaption("MARCEL PUTERO");
	textArea->setColour(Ogre::ColourValue(1.0, 1.0, 1.0, 1.0));




	// Create an overlay, and add the panel
	Ogre::Overlay* overlay = overlayMgr.create("OverlayName");
	overlay->add2D(reinterpret_cast<Ogre::OverlayContainer*>(textArea));



	// Add the text area to the panel
	//panel->addChild(textArea);


	// Show the overlay
	overlay->show();

	scene_mgr->setAmbientLight(Ogre::ColourValue(0.5f, 0.5f, 0.5f));
	
	Ogre::Light* light = scene_mgr->createLight("MainLight");
	light->setPosition(20, 80, 0);





}
void CIntroGameState::Finish()
{

}
void CIntroGameState::Pause()
{

}
void CIntroGameState::Resume()
{

}
void CIntroGameState::Update(f64 dt)
{
	//m_cameracontroller->Update(dt);
	m_character->Update(dt);
}