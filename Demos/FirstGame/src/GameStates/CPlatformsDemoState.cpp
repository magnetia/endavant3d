#include "CPlatformsDemoState.h"
#include "OGRE/Ogre.h"
#include "Core/CCoreEngine.h"
#include "State/CStateManager.h"
#include "Renderer/CRenderManager.h"
#include "Input/CInputManager.h"
#include "../CharacterController.h"
#include "CMenuGameState.h"
#include "Game/GameEntity.h"
#include "Game/EntityOgreGraphics.h"
#include "Game/EntityBtPhysics.h"
#include "../BasicController.h"
#include "Game/Controller.h"

// #debug:
#include "Physics/CPhysicsManager.h"
#include <cstdlib>

const std::string CPlatformsDemoState::RESTART_NAME = "RESTART";
const std::string CPlatformsDemoState::RESTART_KEY = "R";
const std::string CPlatformsDemoState::TOGGLE_DEBUG_NAME = "DEBUG";
const std::string CPlatformsDemoState::TOGGLE_DEBUG_KEY = "X";
const std::string CPlatformsDemoState::SCENE_NAME = "Scene1.scene";

CPlatformsDemoState::CPlatformsDemoState() : 
	CState("CPlatformsDemoState"),
	m_initialized(false)
{

}

CPlatformsDemoState::~CPlatformsDemoState()
{
	// #todo: unregister key action if registered
}

void CPlatformsDemoState::Start()
{
	if (!m_initialized)
	{
		// register input
		CCoreEngine::Instance().GetInputManager().GetKeyboard().InsertKeyAction(RESTART_NAME, RESTART_KEY);
		CCoreEngine::Instance().GetInputManager().GetKeyboard().InsertKeyAction(TOGGLE_DEBUG_NAME, TOGGLE_DEBUG_KEY);

		// load scene
		CCoreEngine::Instance().GetRenderManager().addScene(CRenderManager::SCENE_FORMAT_DOTSCENE, SCENE_NAME);
		m_initialized = true;
	}

	GameEntity* ball1 = new GameEntity(EntityOgreGraphics::Config("ball1"), EntityBtPhysics::Config(EntityBtPhysics::COLLISION_SHAPE_SPHERE, 1, false));

	ball1->addBehavior(new Controller);

	m_entities.emplace_back(ball1);
	m_entities.emplace_back(new GameEntity(EntityOgreGraphics::Config("platf1"), EntityBtPhysics::Config(EntityBtPhysics::COLLISION_SHAPE_TRIANGLE_MESH, 0, false)));
	m_entities.emplace_back(new GameEntity(EntityOgreGraphics::Config("platf2"), EntityBtPhysics::Config(EntityBtPhysics::COLLISION_SHAPE_HULL, 10, true)));

	auto scene_mgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();

	// set viewport
	Ogre::Camera *cam = scene_mgr->getCamera("cam1");
	m_camcontroller.reset(new CCharacterController(cam));

	Ogre::Viewport *vp = CCoreEngine::Instance().GetRenderManager().GetRenderWindow()->addViewport(cam);
	vp->setBackgroundColour(Ogre::ColourValue(.0, .0, .0));

	// #debug: #todo: addFloor()
	btCollisionShape* groundShape = new btStaticPlaneShape(btVector3(0, 1, 0), 1);
	btDefaultMotionState* groundMotionState =
		new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, -10, 0)));

	btRigidBody::btRigidBodyConstructionInfo
		groundRigidBodyCI(0, groundMotionState, groundShape, btVector3(0, 0, 0));
	btRigidBody* groundRigidBody = new btRigidBody(groundRigidBodyCI);

	CCoreEngine::Instance().GetPhysicsManager().AddRigidBody(groundRigidBody);
}

void CPlatformsDemoState::Finish()
{
	m_entities.clear();
	m_camcontroller.reset();
}

void CPlatformsDemoState::Pause()
{

}

void CPlatformsDemoState::Resume()
{

}

void CPlatformsDemoState::Update(f64 dt)
{
	//m_camcontroller->Update(dt);

	for (auto& ptr : m_entities)
	{
		ptr->update(dt);
	}

	if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyPushed(RESTART_NAME))
	{
		CCoreEngine::Instance().GetPhysicsManager().Reset();
	}

	//static u32 last_val = 1;
	//if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyPushed(TOGGLE_DEBUG_NAME))
	//{
	//	last_val = last_val == 1 ? 0 : 1;
	//	CCoreEngine::Instance().GetPhysicsManager().SetDebugMode(last_val);

	//}
}
