#include "CMenuGameState.h"
#include "Core/CCoreEngine.h"
#include "State/CStateManager.h"
#include "CPlatformsDemoState.h"

CMenuGameState::CMenuGameState()
{

}

CMenuGameState::~CMenuGameState()
{

}

void CMenuGameState::Start()
{

}

void CMenuGameState::Finish()
{

}

void CMenuGameState::Pause()
{

}

void CMenuGameState::Resume()
{

}

void CMenuGameState::Update(f64 dt)
{
	CCoreEngine::Instance().GetStateManager().ChangeState(CPlatformsDemoState::Pointer());
}
