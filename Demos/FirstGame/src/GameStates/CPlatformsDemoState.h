#ifndef EDV_CPLATFORMS_DEMO_STATE_H_
#define EDV_CPLATFORMS_DEMO_STATE_H_

#include <memory>
#include <vector>
#include <string>
#include "State/CState.h"
#include "Utils/CSingleton.h"

class CCharacterController;
class GameEntity;

class CPlatformsDemoState : public CState, public CSingleton<CPlatformsDemoState>
{
public:
	CPlatformsDemoState();
	~CPlatformsDemoState();

	void Start() override;
	void Finish() override;
	void Pause() override;
	void Resume() override;
	void Update(f64 dt) override;

private:
	typedef std::unique_ptr<GameEntity>	entity_ptr;
	typedef std::vector<entity_ptr>		t_entities;

	static const std::string RESTART_NAME;
	static const std::string RESTART_KEY;
	static const std::string TOGGLE_DEBUG_NAME;
	static const std::string TOGGLE_DEBUG_KEY;
	static const std::string SCENE_NAME;

	t_entities								m_entities;
	bool									m_initialized;
	std::unique_ptr<CCharacterController>	m_camcontroller;
};


#endif
