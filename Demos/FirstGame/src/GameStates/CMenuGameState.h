#ifndef EDV_CMENU_GAME_STATE_H_
#define EDV_CMENU_GAME_STATE_H_

#include <memory>
#include <vector>
#include <string>
#include "State/CState.h"
#include "Utils/CSingleton.h"

class CMenuGameState : public CState, public CSingleton<CMenuGameState>
{
public:
	CMenuGameState();
	~CMenuGameState();

	void Start() override;
	void Finish() override;
	void Pause() override;
	void Resume() override;
	void Update(f64 dt) override;

private:

};


#endif
