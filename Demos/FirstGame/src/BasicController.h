#ifndef EDV_GAME_BASIC_CONTROLLER_H_
#define EDV_GAME_BASIC_CONTROLLER_H_

#include "Core/CBasicTypes.h"
#include "Game/Behavior.h"

class BasicController : public Behavior
{
public:
	BasicController(f32 a_speed);

	Behavior* clone() override;

private:
	void initImpl() override;
	void updateImpl(f64 dt) override;
	void stopImpl() override;
	void pauseImpl() override;
	void resumeImpl() override;

	f32 m_speed;
};

#endif
