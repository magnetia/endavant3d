#ifndef CCOREENGINE_H_
#define CCOREENGINE_H_

#include "Defines.h"

class CRenderManager;
class CInputManager;
class CLogManager;
class CTimeManager;
class CEventManager;
class CResourceManager;
class CSoundManager;
class CStateManager;
class CMainStateManager;
class CPhysicsManager;
class OptionManager;
class ScriptManager;
class GameManager;
class CameraManager;


class CCoreEngine {
public:
	bool	StartUp();
	void	ShutDown();
	void	Update();
	void	Render();

	static CCoreEngine &Instance();

	CRenderManager		&GetRenderManager(); 	//retorna la direccio del VideoManager
	CInputManager		&GetInputManager(); 	//retorna la direccio de l'inputmanager
	CLogManager			&GetLogManager(); 		//retorna la direccio del LogManager
	CTimeManager		&GetTimerManager(); 	//Retorna la direccio del Timer
	CEventManager		&GetEventManager();		//Retorna la direccio del gestor d'events
	CResourceManager	&GetResourceManager(); 	//Retorna la direccio del gestor de recursos
	CSoundManager		&GetSoundManager(); 	//Retorna la direccio del manager d'audio
	CMainStateManager	&GetMainStateManager(); //Retorna la direccio del manager d'estats principal
	//edv::net::manager	&GetNetworkManager();	//Retorna la direccio del manager de xarxa
	CPhysicsManager		&GetPhysicsManager(); 	//Retorna la direccio del manager de fisiques
	OptionManager		&GetOptionManager(); 	//Retorna la direccio del manager d'opcions
	ScriptManager		&GetScriptManager(); 	//Retorna la direccio del manager de scripting
	GameManager			&GetGameManager();		//Retorna la direccio del manager de joc
	CameraManager		&GetCameraManager();	//Retorna la direccio del manager de cameres
	


	void	Run();
	void	StopCore();
	inline bool 	IsRunning() { return m_run; };

	~CCoreEngine();
private:

	bool	m_run; 						//Flag de funcionament de l'Engine
	CCoreEngine();

	CRenderManager		*m_rendermngr;
	CInputManager 		*m_inputmngr;
	CLogManager	 		*m_logmngr;
	CTimeManager		*m_timermngr;
	CEventManager		*m_eventmngr;
	CResourceManager 	*m_resourcemngr;
	CSoundManager		*m_soundmngr;
	CMainStateManager		*m_statemngr;
	//edv::net::manager	*m_netmngr;
	CPhysicsManager		*m_physicsmngr;
	OptionManager		*m_optionmngr;
	ScriptManager		*m_scriptmngr;
	GameManager			*m_gamemngr;
	CameraManager		*m_cameramngr;


	bool	StartUpSDL();
	void	ShutDownSDL();
};

#endif /* CCOREENGINE_H_ */
