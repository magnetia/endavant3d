#include "CCoreEngine.h"
#include "Renderer/CRenderManager.h"
#include "Input/CInputManager.h"
#include "CLogManager.h"
#include "Time/CTimeManager.h"
#include "Events/CEventManager.h"
#include "Resources/CResourceManager.h"
#include "Sound/CSoundManager.h"
#include "State/CMainStateManager.h"
#include "Physics/CPhysicsManager.h"
#include "Options/OptionManager.h"
#include "Script/ScriptManager.h"
#include "Game/GameManager.h"
#include "Camera/CameraManager.h"
#include "exceptions.h"
#include <SDL2/SDL.h>
#include "Defines.h"


CCoreEngine::CCoreEngine():
m_run(false),
m_rendermngr(nullptr),
m_inputmngr(nullptr),
m_logmngr(nullptr),
m_timermngr(nullptr),
m_eventmngr(nullptr),
m_resourcemngr(nullptr),
m_soundmngr(nullptr),
m_statemngr(nullptr),
//m_netmngr(nullptr),
m_physicsmngr(nullptr),
m_optionmngr(nullptr),
m_scriptmngr(nullptr),
m_gamemngr(nullptr),
m_cameramngr(nullptr)
{

}

CCoreEngine::~CCoreEngine()
{
	delete m_statemngr;
	delete m_timermngr;
	delete m_cameramngr;
	delete m_rendermngr;
	delete m_inputmngr;
	delete m_eventmngr;
	delete m_soundmngr;
	//delete m_netmngr;
	delete m_resourcemngr;
	delete m_logmngr;
	delete m_physicsmngr;
	delete m_optionmngr;
	delete m_scriptmngr;
	delete m_gamemngr;

}

//Singleton
CCoreEngine &CCoreEngine::Instance()
{
	static CCoreEngine instance;
	return instance;
}

CameraManager		&CCoreEngine::GetCameraManager()
{
	return *m_cameramngr;
}

CLogManager		&CCoreEngine::GetLogManager()
{
	return *m_logmngr;
}
CRenderManager	&CCoreEngine::GetRenderManager()
{
	return *m_rendermngr;
}
CTimeManager	&CCoreEngine::GetTimerManager()
{
	return *m_timermngr;
}
CInputManager	&CCoreEngine::GetInputManager()
{
	return *m_inputmngr;
}

CEventManager	&CCoreEngine::GetEventManager()
{
	return *m_eventmngr;
}

CResourceManager &CCoreEngine::GetResourceManager()
{
	return *m_resourcemngr;
}

CSoundManager &CCoreEngine::GetSoundManager()
{
	return *m_soundmngr;
}

CMainStateManager& CCoreEngine::GetMainStateManager()
{
	return *m_statemngr;
}
/*edv::net::manager &CCoreEngine::GetNetworkManager()
{
	return *m_netmngr;
}
*/

CPhysicsManager& CCoreEngine::GetPhysicsManager()
{
	return *m_physicsmngr;
}

OptionManager& CCoreEngine::GetOptionManager()
{
	return *m_optionmngr;
}

ScriptManager& CCoreEngine::GetScriptManager()
{
	return *m_scriptmngr;
}

GameManager& CCoreEngine::GetGameManager()
{
	return *m_gamemngr;
}

void CCoreEngine::Run()
{
	m_rendermngr->GetOgreRoot()->addFrameListener(m_rendermngr);
	m_rendermngr->GetOgreRoot()->setFrameSmoothingPeriod(0.5f);
	//m_rendermngr->GetRenderWindow()->setVSyncEnabled(true);
	
	m_rendermngr->GetOgreRoot()->startRendering();
	/*
	try
	{
		while ( m_run )
		{
			Update();
			Render();
		}
	}
	catch (const Ogre::Exception& e)
	{
		LOG(LOG_ERROR, LOGSUB_ENGINE, "Ogre Exception thrown: %s", e.getFullDescription().c_str());
		throw;
	}
	catch (const std::exception& e)
	{
		LOG(LOG_ERROR, LOGSUB_ENGINE, "exception thrown: %s", e.what());
		throw;
	}
	catch (...)
	{
		LOG(LOG_ERROR, LOGSUB_ENGINE, "unknown exception thrown");
		throw;
	}

	StopCore();
	*/
}

void CCoreEngine::StopCore()
{
	m_run = false;
}

bool	CCoreEngine::StartUpSDL()
{
	SDL_version compiled;
	SDL_version linked;

	SDL_VERSION(&compiled);
	SDL_GetVersion(&linked);

	std::cout<<"We compiled against SDL version: "<< (u32) compiled.major<<"."<<(u32) compiled.minor<<"."<<(u32) compiled.patch<<std::endl;
	std::cout<<"And we are linking against SDL version: "<< (u32) linked.major<<"."<<(u32) linked.minor<<"."<<(u32) linked.patch<<std::endl;

	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_JOYSTICK) != 0)
	{
		std::cout<<"ERROR! Unable to initialize SDL: "<<SDL_GetError();
		return false;
    }

	return true;
}

void CCoreEngine::ShutDownSDL()
{
	SDL_Quit();
}

bool CCoreEngine::StartUp()
{
	srand(static_cast<u32>(time(NULL)));

	if (!StartUpSDL())
		return false;

	//CREO ELS SUBSISTEMES EN ORDRE
	m_optionmngr = new OptionManager;
	m_optionmngr->startUp();

	m_logmngr = new CLogManager;
	m_logmngr->StartUp();

	m_rendermngr = new CRenderManager;
	if (!m_rendermngr->StartUp())
		return false;

	m_resourcemngr = new CResourceManager;
	m_resourcemngr->StartUp();

	m_cameramngr = new CameraManager;
	m_cameramngr->StartUp();

	m_soundmngr = new CSoundManager;
	m_soundmngr->StartUp();
	
	m_eventmngr = new CEventManager;
	m_eventmngr->StartUp();
	
	m_inputmngr = new CInputManager;
	m_inputmngr->StartUp();
	
	m_timermngr = new CTimeManager;
	m_timermngr->StartUp();
	
	//m_netmngr = new edv::net::manager;
	//m_netmngr->StartUp();
	
	m_physicsmngr = new CPhysicsManager;
	m_physicsmngr->StartUp();
	
	m_gamemngr = new GameManager;
	m_gamemngr->startUp();
	
	m_scriptmngr = new ScriptManager;
	m_scriptmngr->startUp();
	
	m_statemngr = new CMainStateManager;
	m_statemngr->StartUp();

	LOG(LOG_INFO, LOGSUB_ENGINE, "Start all subsystems OK");
	m_run = true;

	return true;
}

void	CCoreEngine::Update()
{
	if (m_run)
	{

		f64 dt = 0.0F;
		m_timermngr->Update(dt);
		dt = m_timermngr->GetElapsedTimeSeconds();

		
		// Engine Update
		m_optionmngr->update(dt);
		m_inputmngr->Update(dt);
		m_eventmngr->Update(dt);
		m_soundmngr->Update(dt);
		m_resourcemngr->Update(dt);

		// Game Update Logic
		m_statemngr->Update(dt);


		// Engine Physics
		m_physicsmngr->Update(dt);

		// Engine Render
		m_cameramngr->Update(dt);
		m_rendermngr->Update(dt);
		
	}
}

void CCoreEngine::Render()
{
	if (m_run)
	{
		m_rendermngr->Render(); //Renders
	}
}

void	CCoreEngine::ShutDown()
{
	//EN ORDRE INVERS AL DE CREACIO
	LOG( LOG_INFO, LOGSUB_ENGINE,"ShutDown all subsystems:  ");

	m_statemngr->ShutDown();
	m_scriptmngr->shutDown();
	m_gamemngr->shutDown();
	m_physicsmngr->ShutDown();
	//m_netmngr->ShutDown();
	m_timermngr->ShutDown();
	m_inputmngr->ShutDown();
	m_eventmngr->ShutDown();
	m_soundmngr->ShutDown();
	m_cameramngr->ShutDown();
	m_resourcemngr->ShutDown();
	m_rendermngr->ShutDown();
	m_logmngr->ShutDown();
	m_optionmngr->shutDown();

	ShutDownSDL();
}
