#ifndef CRESOURCE_H_
#define CRESOURCE_H_

#include <string>
#include <stdexcept>
#include <mutex>
#include "Core/CBasicTypes.h"

#undef VERBOSE_RESOURCE_COUNTING // define to debug

enum E_RESOURCE_TYPE
{
	RESOURCE_TYPE_SOUND,
	RESOURCE_TYPE_TRACK,

	RESOURCE_TYPE_NONE
};

class CResource
{
public:
	static std::string ResourceTypeToString(E_RESOURCE_TYPE a_type);
	static E_RESOURCE_TYPE StringToResourceType(const std::string& a_name);

	CResource(E_RESOURCE_TYPE a_Type, const std::string& a_Id, const std::string& a_Path);
	virtual ~CResource();

	virtual E_RESOURCE_TYPE getType() const final;
	virtual const std::string& getId() const final;
	virtual const std::string& getPath() const final;
	virtual u32 getReferenceCount() const final;

	virtual void* getResource() = 0;

	virtual void grab() final;
	virtual void drop() final;

protected:
	virtual void load() = 0;
	virtual void unload() = 0;

	const E_RESOURCE_TYPE	m_Type;
	const std::string		m_Id;
	const std::string		m_Path;
	u32						m_Count;
	std::mutex				m_CountMutex;
};

#endif
