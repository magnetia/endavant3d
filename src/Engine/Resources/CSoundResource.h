#ifndef CSOUNDRESOURCE_H_
#define CSOUNDRESOURCE_H_

#include "CResource.h"

struct Mix_Chunk;

class CSoundResource : public CResource
{
public:
	CSoundResource(const std::string& a_Id, const std::string& a_Path);
	~CSoundResource();

	void* getResource() override;

private:
	void load() override;
	void unload() override;

	Mix_Chunk* 	m_Chunk;
};

#endif
