#include "CSoundResource.h"
#include "Core/CCoreEngine.h"
#include "SDL2/SDL_mixer.h"

CSoundResource::CSoundResource(const std::string& a_Id, const std::string& a_Path) :
	CResource(RESOURCE_TYPE_SOUND, a_Id, a_Path),
	m_Chunk(nullptr)
{

}

CSoundResource::~CSoundResource()
{

}

void CSoundResource::load()
{
	if (!m_Chunk)
	{
		m_Chunk = Mix_LoadWAV(m_Path.c_str());

		if (!m_Chunk)
		{
			const std::string error{ Mix_GetError() };
			throw std::runtime_error("CSoundResource::load error: " + error);
		}
	}
}

void CSoundResource::unload()
{
	if (m_Chunk)
	{
		Mix_FreeChunk(m_Chunk);
		m_Chunk = nullptr;
	}
}

void* CSoundResource::getResource()
{
	return m_Chunk;
}
