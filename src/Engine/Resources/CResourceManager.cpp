#include "CResourceManager.h"
#include "Core/CCoreEngine.h"
#include "Core/CLogManager.h"

//OGRE RESOURCES
#include "OGRE/Ogre.h"

CResourceManager::CResourceManager()
{

}

CResourceManager::~CResourceManager()
{

}

/// Method which will define the source of resources (other than current folder)
static void setupResources(void)
{
	// Load resource paths from config file
	Ogre::ConfigFile cf;
	cf.load("Config/resources.cfg");


	// Go through all sections & settings in the file
	Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

	Ogre::String secName, typeName, archName;
	while (seci.hasMoreElements())
	{
		secName = seci.peekNextKey();
		Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
		Ogre::ConfigFile::SettingsMultiMap::iterator i;
		for (i = settings->begin(); i != settings->end(); ++i)
		{
			typeName = i->first;
			archName = i->second;

			Ogre::ResourceGroupManager::getSingleton().addResourceLocation(	archName, typeName, secName);

		}
	}
}

void CResourceManager::StartUp(void)
{
	// #todo: xml

	LOG(LOG_INFO, LOGSUB_RESOURCES,"Startup!");
	
	setupResources();
	//get the resource manager
	Ogre::ResourceGroupManager &resGroupMgr = Ogre::ResourceGroupManager::getSingleton();
	// tell it to look at this location
	/*
	resGroupMgr.addResourceLocation("./media/graphics/materials/OgreSamples/materials/programs/GLSLES", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/materials/OgreSamples/materials/programs/GLSL150", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/materials/OgreSamples/materials/programs/GLSL", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/materials/OgreSamples/materials/programs/GLSL400", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/materials/OgreSamples/materials/programs/HLSL", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/materials/OgreSamples/materials/programs/Cg", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/materials/OgreSamples/RTShaderLib/GLSLES", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/materials/OgreSamples/RTShaderLib/GLSL", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/materials/OgreSamples/RTShaderLib/GLSL150", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/materials/OgreSamples/RTShaderLib/HLSL", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/materials/OgreSamples/RTShaderLib/Cg", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/materials/OgreSamples/materials/textures", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/materials/OgreSamples/materials/textures/nvidia", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/materials/OgreSamples/materials/textures/SSAO", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/materials/OgreSamples/materials/scripts", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/materials/OgreSamples/materials/scripts/SSAO", "FileSystem");

	resGroupMgr.addResourceLocation("./media", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/fonts", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/materials", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/models", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/textures", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/skeletons", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/compositors", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/programs", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/particles", "FileSystem", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, true);
	resGroupMgr.addResourceLocation("./media/graphics/particles/OgreParticleLab/Resources/Textures", "FileSystem");
	
	resGroupMgr.addResourceLocation("./media/graphics/new_particles/textures", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/new_particles", "FileSystem");

	resGroupMgr.addResourceLocation("./media/scenes/dotscene", "FileSystem");

	// Nou
	resGroupMgr.addResourceLocation("./media/gui/menu", "FileSystem");
	resGroupMgr.addResourceLocation("./media/gui/menu/buttons", "FileSystem");

	resGroupMgr.addResourceLocation("./media/hud", "FileSystem");

	resGroupMgr.addResourceLocation("./media/tilesets", "FileSystem", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, true);
	resGroupMgr.addResourceLocation("./media/actors", "FileSystem", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, true);
	*/
	//resGroupMgr.addResourceLocation("./media", "FileSystem", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, true);
	/*resGroupMgr.addResourceLocation("./media/graphics/fonts", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/hud", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/gui/menu", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/gui/menu/buttons", "FileSystem");

	resGroupMgr.addResourceLocation("./media/graphics/tilesets/default", "FileSystem");

	resGroupMgr.addResourceLocation("./media/graphics/actors/magnets", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/actors/enemies/clashnik", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/actors/enemies/minsect", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/actors/enemies/toweronic", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/actors/enemies/lethalbarrier", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/actors/enemies/gasmortal", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/actors/enemies/aplasta", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/actors/levelend", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/actors/neutroball", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/actors/accelerator", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/actors/nik", "FileSystem");

	resGroupMgr.addResourceLocation("./media/graphics/levels/skyboxes", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/levels/start_animation", "FileSystem");
	resGroupMgr.addResourceLocation("./media/graphics/levels/fadeInOut", "FileSystem");
	*/
	resGroupMgr.initialiseAllResourceGroups();
}
/*
[General]
FileSystem=./media/graphics/fonts", "FileSystem");
FileSystem=./media/graphics/hud", "FileSystem");
FileSystem=./media/graphics/gui/menu", "FileSystem");
FileSystem=./media/graphics/gui/menu/buttons", "FileSystem");
FileSystem=./media/graphics/tilesets/default", "FileSystem");
FileSystem=./media/graphics/actors/magnets", "FileSystem");
FileSystem=./media/graphics/actors/enemies/clashnik", "FileSystem");
FileSystem=./media/graphics/actors/enemies/minsect", "FileSystem");
FileSystem=./media/graphics/actors/enemies/toweronic", "FileSystem");
FileSystem=./media/graphics/actors/enemies/lethalbarrier", "FileSystem");
FileSystem=./media/graphics/actors/enemies/gasmortal", "FileSystem");
FileSystem=./media/graphics/actors/enemies/aplasta", "FileSystem");
FileSystem=./media/graphics/actors/levelend", "FileSystem");
FileSystem=./media/graphics/actors/neutroball", "FileSystem");
FileSystem=./media/graphics/actors/accelerator", "FileSystem");
FileSystem=./media/graphics/actors/nik", "FileSystem");
FileSystem=./media/graphics/actors/nik/die", "FileSystem");
FileSystem=./media/graphics/levels/skyboxes", "FileSystem");
FileSystem=./media/graphics/levels/start_animation", "FileSystem");
FileSystem=./media/graphics/levels/fadeInOut", "FileSystem");


*/
void CResourceManager::ShutDown(void)
{
	LOG(LOG_INFO, LOGSUB_RESOURCES,"ShutDown!");
}

void CResourceManager::Update(f64 dt)
{

}
