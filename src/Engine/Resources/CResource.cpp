#include "CResource.h"

#ifdef VERBOSE_RESOURCE_COUNTING
#include <Core/CCoreEngine.h>
#endif

std::string CResource::ResourceTypeToString(E_RESOURCE_TYPE a_type)
{
	std::string str;

	switch (a_type)
	{
	case RESOURCE_TYPE_SOUND:
		str = "sound";
		break;
	case RESOURCE_TYPE_TRACK:
		str = "track";
		break;
	default:
		str = "unknown";
		break;
	}

	return str;
}

E_RESOURCE_TYPE CResource::StringToResourceType(const std::string& a_name)
{
	E_RESOURCE_TYPE res = RESOURCE_TYPE_NONE;

	if (a_name == "sound")
	{
		res = RESOURCE_TYPE_SOUND;
	}
	else if (a_name == "track")
	{
		res = RESOURCE_TYPE_TRACK;
	}

	return res;
}

CResource::CResource(E_RESOURCE_TYPE a_Type, const std::string& a_Id, const std::string& a_Path) : m_Type(a_Type), m_Id(a_Id), m_Path(a_Path), m_Count(0)
{

}

CResource::~CResource()
{

}

E_RESOURCE_TYPE CResource::getType() const
{
	return m_Type;
}

const std::string& CResource::getId() const
{
	return m_Id;
}

const std::string& CResource::getPath() const
{
	return m_Path;
}

u32 CResource::getReferenceCount() const
{
	return m_Count;
}

void CResource::grab()
{
	std::lock_guard<std::mutex> lock(m_CountMutex);

	if (m_Count == 0)
	{
		load();
	}

	m_Count++;

#ifdef VERBOSE_RESOURCE_COUNTING
	LOG(LOG_DEVEL, LOGSUB_RESOURCES, "id: %s count: %u", m_Id.c_str(), m_Count);
#endif
}

void CResource::drop()
{
	std::lock_guard<std::mutex> lock(m_CountMutex);

	if (m_Count > 0)
	{
		m_Count--;

		if (m_Count == 0)
		{
			unload();
		}
	}
	else
	{
		throw std::runtime_error("bad reference counting for resource id " + getId());
	}

#ifdef VERBOSE_RESOURCE_COUNTING
	LOG(LOG_DEVEL, LOGSUB_RESOURCES, "id: %s count: %u", m_Id.c_str(), m_Count);
#endif
}
