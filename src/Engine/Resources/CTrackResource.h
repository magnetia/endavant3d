#ifndef CTRACKRESOURCE_H_
#define CTRACKRESOURCE_H_

#include "CResource.h"

struct _Mix_Music;

class CTrackResource : public CResource
{
public:
	CTrackResource(const std::string& a_Id, const std::string& a_Path);
	~CTrackResource();

	void* getResource() override;

private:
	void load() override;
	void unload() override;

	_Mix_Music* 	m_Track;
};

#endif
