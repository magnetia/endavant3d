#ifndef CRESOURCEMANAGER_H_
#define CRESOURCEMANAGER_H_

#include "Core/CBasicTypes.h"

class CResourceManager
{
public:
	CResourceManager();
	virtual ~CResourceManager();

	void StartUp(void);
	void ShutDown(void);
	void Update(f64 dt);
private:
};

#endif
