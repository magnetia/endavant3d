#include "CTrackResource.h"
#include <SDL2/SDL_mixer.h>
#include "Core/CCoreEngine.h"

CTrackResource::CTrackResource(const std::string& a_Id, const std::string& a_Path) :
	CResource(RESOURCE_TYPE_TRACK, a_Id, a_Path),
	m_Track(nullptr)
{

}

CTrackResource::~CTrackResource()
{

}

void CTrackResource::load()
{
	if (!m_Track)
	{
		m_Track = Mix_LoadMUS(m_Path.c_str());

		if (!m_Track)
		{
			throw std::runtime_error("CTrackResource::load error: " + std::string(Mix_GetError()));
		}
	}
}

void CTrackResource::unload()
{
	if (m_Track)
	{
		Mix_FreeMusic(m_Track);
		m_Track = nullptr;
	}
}

void* CTrackResource::getResource()
{
	return m_Track;
}
