#ifndef CSTATE_H_
#define CSTATE_H_

#include <string>
#include "Core/CBasicTypes.h"

class CState
{
public:

	CState(const std::string& aName = "UNDEFINED");
	virtual ~CState();

	virtual void Start() = 0;
	virtual void Finish() = 0;
	virtual void Pause() = 0;
	virtual void Resume() = 0;
	virtual void Update(f64 dt) = 0;

	const std::string& GetName() const;

private:
	const std::string m_Name;
};


#endif /* CSTATE_H_ */
