/*
 * CStateManager.h
 *
 *  Created on: 07/07/2013
 *      Author: Dani
 */

#ifndef CSTATEMANAGER_H_
#define CSTATEMANAGER_H_

#include "CState.h"
#include <stack>
#include <unordered_map>

class CStateManager
{
public:
	CStateManager(const std::string& a_Name = "UNNAMED");
	virtual ~CStateManager();

	virtual void Update(f64 dt);

	virtual void RegisterState(CState* a_State);
	virtual void UnregisterState(CState* a_State);
	virtual CState* GetState(const std::string& a_StateName);
	virtual void ChangeState(CState* a_State);
	virtual void ChangeState(const std::string& a_StateName);
	virtual void PushState(CState* a_State);
	virtual void PushState(const std::string& a_StateName);
	virtual void PopState();
	virtual void Clear();
	virtual CState* TopState();
	virtual std::string GetName() const final;
	virtual std::vector<CState *> GetAllRegisteredStates() const;

protected:
	typedef std::stack<CState*>							StateStack;
	typedef std::unordered_map<std::string, CState*>	RegisteredStates;

	StateStack				m_StateStack;
	RegisteredStates		m_RegisteredStates;
	std::string				m_Name;
};

#endif /* CSTATEMANAGER_H_ */
