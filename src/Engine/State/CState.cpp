#include "CState.h"

CState::CState(const std::string& aName) : m_Name(aName)
{
}

CState::~CState()
{
}

const std::string& CState::GetName() const
{
	return m_Name;
}
