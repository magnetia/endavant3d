#include "CStateManager.h"
#include "Core/CCoreEngine.h"
#include "Core/CLogManager.h"

CStateManager::CStateManager(const std::string& a_Name) :
	m_Name{a_Name}
{

}

CStateManager::~CStateManager()
{
	Clear();
}

void CStateManager::Update(f64 dt)
{
	if (!m_StateStack.empty())
		m_StateStack.top()->Update(dt);

}

void CStateManager::RegisterState(CState* a_State)
{
	if (a_State)
	{
		auto it = m_RegisteredStates.find(a_State->GetName());

		if (it == m_RegisteredStates.end())
		{
			m_RegisteredStates.emplace_hint(it, 
				RegisteredStates::value_type(a_State->GetName(), a_State));
		}
		else
		{
			throw std::runtime_error("attempt to register a duplicated state name"
				" in state manager " + m_Name);
		}
	}
}

void CStateManager::UnregisterState(CState* a_State)
{
	if (a_State)
	{
		LOG(LOG_DEVEL, LOGSUB_ENGINE, "[%s] UnregisterState %s", m_Name.c_str(), 
			a_State->GetName().c_str());

		m_RegisteredStates.erase(a_State->GetName());
	}
}

CState* CStateManager::GetState(const std::string& a_StateName)
{
	CState* state = nullptr;
	auto it = m_RegisteredStates.find(a_StateName);
	
	if (it != m_RegisteredStates.end())
	{
		state = it->second;
	}

	return state;
}

void CStateManager::ChangeState(CState* a_State)
{
	if(a_State)
	{
		// #todo: parlar. Faig que un change state desapili i cridi al Finish de tots els estats apilats.
		std::string originStatesStr;

		while (CState* originState = m_StateStack.empty() ? nullptr : m_StateStack.top())
		{
			// Exit state and pop it from stack
			originState->Finish();
			originStatesStr += originState->GetName() + " ";

			m_StateStack.pop();
		}

		// Store and init the new state.
		m_StateStack.push(a_State);
		m_StateStack.top()->Start();

		LOG( LOG_DEVEL, LOGSUB_STATE ,"[%s] ChangeState %s -> %s", 
			m_Name.c_str(),
			originStatesStr.empty() ? "-" : originStatesStr.c_str(),
			a_State->GetName().c_str());
	}
}

void CStateManager::ChangeState(const std::string& a_StateName)
{
	auto it = m_RegisteredStates.find(a_StateName);

	if (it != m_RegisteredStates.end())
	{
		ChangeState(it->second);
	}
}

void CStateManager::PushState(CState* a_State)
{
	if(a_State)
	{
		CState* originState = m_StateStack.empty()? nullptr : m_StateStack.top();
		if (originState)
		{
			// Pause current state
			originState->Pause();
		}

		// Store and init the new state.
		m_StateStack.push(a_State);
		m_StateStack.top()->Start();

		LOG( LOG_DEVEL, LOGSUB_STATE ,"[%s] PushState %s on top of %s", 
			m_Name.c_str(),
			a_State->GetName().c_str(), 
			originState? originState->GetName().c_str():"-");
	}
}

void CStateManager::PushState(const std::string& a_StateName)
{
	auto it = m_RegisteredStates.find(a_StateName);

	if (it != m_RegisteredStates.end())
	{
		PushState(it->second);
	}
}

void CStateManager::PopState()
{
	CState* originState = m_StateStack.empty()? nullptr : m_StateStack.top();
	if (originState)
	{
		// Finish current state and pop it
		originState->Finish();
		m_StateStack.pop();
	}

	CState* destState = m_StateStack.empty()? nullptr : m_StateStack.top();
	if (destState)
	{
		// Resume the last state
		destState->Resume();
	}

	LOG( LOG_DEVEL, LOGSUB_STATE ,"[%s] PopState %s from %s", 
		m_Name.c_str(),
		originState? originState->GetName().c_str():"-", 
		destState? destState->GetName().c_str():"-");
}

void CStateManager::Clear()
{
	while (!m_StateStack.empty())
	{
		m_StateStack.top()->Finish();
		m_StateStack.pop();

		LOG(LOG_DEVEL, LOGSUB_STATE, "[%s] Stack cleared", m_Name.c_str());
	}
}

CState* CStateManager::TopState()
{
	CState* state = nullptr;

	if (!m_StateStack.empty())
	{
		state = m_StateStack.top();
	}

	return state;
}

std::string CStateManager::GetName() const
{
	return m_Name;
}

std::vector<CState *> CStateManager::GetAllRegisteredStates() const
{
	std::vector<CState *> ret_vec;
	for (auto &state : m_RegisteredStates)
		ret_vec.push_back(state.second);

	return ret_vec;
	
}