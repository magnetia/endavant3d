#include "CMainStateManager.h"
#include "Core/CCoreEngine.h"
#include "Core/CLogManager.h"

CMainStateManager::CMainStateManager() :
	CStateManager{ "MainStateManager" }
{

}

CMainStateManager::~CMainStateManager()
{

}

void CMainStateManager::StartUp()
{
	LOG(LOG_INFO, LOGSUB_STATE, "StartUp");

	RegisterStateManager(this);
}

void CMainStateManager::ShutDown()
{
	LOG(LOG_INFO, LOGSUB_STATE, "ShutDown");

	Clear();
}

void CMainStateManager::RegisterStateManager(CStateManager* a_stateMgr)
{
	if (a_stateMgr)
	{
		for (auto it = m_registeredManagers.begin(); it != m_registeredManagers.end(); )
		{
			if (a_stateMgr->GetName() == it->first)
			{
				m_registeredManagers.erase(it++);
			}
			else
			{
				++it;
			}
		}

		m_registeredManagers[a_stateMgr->GetName()] = a_stateMgr;
	}
}

void CMainStateManager::UnregisterStateManager(CStateManager* a_stateMgr)
{
	if (a_stateMgr)
	{
		auto it = m_registeredManagers.find(a_stateMgr->GetName());

		if (it != m_registeredManagers.end())
		{
			m_registeredManagers.erase(it);
		}
	}
}

CStateManager* CMainStateManager::GetStateManager(const std::string& a_name)
{
	CStateManager* stateMgr = nullptr;
	auto it = m_registeredManagers.find(a_name);

	if (it != m_registeredManagers.end())
	{
		stateMgr = it->second;
	}

	return stateMgr;
}
