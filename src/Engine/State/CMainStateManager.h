#ifndef EDV_STATE_MAIN_STATE_MANAGER_H_
#define EDV_STATE_MAIN_STATE_MANAGER_H_

#include <string>
#include <unordered_map>
#include "CStateManager.h"

class CMainStateManager : public CStateManager
{
public:
	CMainStateManager();
	~CMainStateManager();

	void StartUp();
	void ShutDown();

	void RegisterStateManager(CStateManager* a_stateMgr);
	void UnregisterStateManager(CStateManager* a_stateMgr);
	CStateManager* GetStateManager(const std::string& a_name);

private:
	typedef std::unordered_map<std::string, CStateManager*> RegisteredManagers;

	RegisteredManagers m_registeredManagers;
};

#endif
