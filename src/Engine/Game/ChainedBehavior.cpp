#include <algorithm>
#include <iterator>
#include <limits>
#include "ChainedBehavior.h"
#include "GameEntity.h"

ChainedBehavior::ChainedBehavior(std::initializer_list<TimedBehavior*>&& a_list, s32 a_loops) :
// endavant!! (la humanitat s'extingira abans que passin 5.78e+300 anys)
	TimedBehavior{ a_loops < 0 ? std::numeric_limits<f64>::max() : [&]
		{
			f64 totalDuration = 0;

			for (auto* behavior : a_list)
			{
				totalDuration += behavior->duration() * a_loops;
			}

			return totalDuration;
		}() },
	m_behaviors{ a_list.begin(), a_list.end() },
	m_current{ m_behaviors.begin() },
	m_loops{ a_loops }
{

}

ChainedBehavior::ChainedBehavior(const ChainedBehavior& a_other) :
	TimedBehavior{a_other.remaining()},
	m_behaviors{},
	m_current{},
	m_loops{a_other.m_loops}
{
	std::transform(a_other.m_behaviors.begin(), a_other.m_behaviors.end(), std::back_inserter(m_behaviors), 
		[](const BehaviorPtr& behaviorPtr)
		{
			return std::move(BehaviorPtr{ static_cast<TimedBehavior*>(behaviorPtr->clone()) });
		});

	m_current = m_behaviors.begin() + (a_other.m_current - a_other.m_behaviors.begin());
}

Behavior* ChainedBehavior::clone()
{
	return new ChainedBehavior(*this);
}

void ChainedBehavior::initImpl()
{
	next();
}

void ChainedBehavior::updateImpl(f64 dt)
{
	if (m_current != m_behaviors.end())
	{
		if ((*m_current)->finished())
		{
			(*m_current)->stop();
			m_current++;

			next();
		}
		else
		{
			(*m_current)->update(dt);
		}
	}
}

void ChainedBehavior::stopImpl()
{
	if (m_current != m_behaviors.end())
	{
		(*m_current)->stop();
	}
}

void ChainedBehavior::pauseImpl()
{
	if (m_current != m_behaviors.end())
	{
		(*m_current)->pause();
	}
}

void ChainedBehavior::resumeImpl()
{
	if (m_current != m_behaviors.end())
	{
		(*m_current)->resume();
	}
}

void ChainedBehavior::next()
{
	if (m_current != m_behaviors.end())
	{
		(*m_current)->setParent(getParent());
		(*m_current)->init();
	}
	else
	{
		if (m_loops > 0)
		{
			m_loops--;
		}

		if (m_loops != 0)
		{
			m_current = m_behaviors.begin();
			(*m_current)->setParent(getParent());
			(*m_current)->init();
		}
		else
		{
			getParent()->removeBehavior(this);
		}
	}
}
