#ifndef EDV_GAME_ENTITY_BT_PHYSICS_H_
#define EDV_GAME_ENTITY_BT_PHYSICS_H_

#include <memory>
#include <btBulletCollisionCommon.h>
#include <BulletCollision/CollisionDispatch/btGhostObject.h>
#include "Core/CBasicTypes.h"
#include "Physics/BtWorldObject.h"
#include "Physics/MagneticProperty.h"
#include "Physics/CPhysicsManager.h"
#include "Utils/ChildOf.h"

class GameEntity;

class EntityBtPhysics : public ChildOf<GameEntity>
{
public:
	enum CollisionShape
	{
		COLLISION_SHAPE_NONE,
		COLLISION_SHAPE_SPHERE,
		COLLISION_SHAPE_BOX,
		COLLISION_SHAPE_HULL,
		COLLISION_SHAPE_TRIANGLE_MESH,
		COLLISION_SHAPE_CAPSULE,
	};

	enum CollisionDetection
	{
		COLLISION_DETECTION_NONE,
		COLLISION_DETECTION_DISABLE,
		COLLISION_DETECTION_GHOST,
		COLLISION_DETECTION_MANIFOLDS,
		COLLISION_DETECTION_TRIGGER,
	};

	// Expand Bullet collision flags.
	// Last Bullet CollisionFlags::CF_DISABLE_SPU_COLLISION_PROCESSING = 64
	enum EdvCollisionFlags
	{
		// Only notifies collision, not has physical response.
		CF_TRIGGER = 128,
	};

	struct CollisionConfig
	{
		CollisionShape		shape;
		CollisionDetection	detection;
		edv::Vector3f		translation;
		//@TODO create edv::Quaternion 
		Ogre::Quaternion	rotation;
		edv::Vector3f		scale;
		std::string			mesh;

		CollisionConfig(CollisionShape a_shape = COLLISION_SHAPE_NONE, CollisionDetection a_detection = COLLISION_DETECTION_NONE, 
			const edv::Vector3f& a_translation = edv::Vector3f{ 0.f, 0.f, 0.f }, const Ogre::Quaternion& a_rotation = Ogre::Quaternion{ 1.f, 0.f, 0.f, 0.f },
			const edv::Vector3f& a_scale = edv::Vector3f{ 1.f, 1.f, 1.f }, const std::string& a_mesh = "") :
				shape{ a_shape }, detection{ a_detection }, translation{ a_translation }, rotation{ a_rotation }, 
				scale{ a_scale }, mesh{ a_mesh } { }
	};

	struct Config
	{
		f32				mass;
		bool			kinematic;
		MagneticProperty::Config magnetic_config;
		bool			physics2D;
		CollisionConfig collisionConfig;

		Config(f32 m = 0.f, bool k = true, MagneticProperty::Config a_magnetic_config = MagneticProperty::Config{}, bool a_physics2D = false,
			const CollisionConfig& a_collisionConfig = CollisionConfig{}) :
				mass(m), kinematic(k), magnetic_config(a_magnetic_config), physics2D(a_physics2D), collisionConfig{ a_collisionConfig } { }

		Config(const Config& other) :
			mass{ other.mass }, kinematic{ other.kinematic }, magnetic_config{ other.magnetic_config },
			physics2D{ other.physics2D }, collisionConfig{ other.collisionConfig }
		{}

		bool IsInitialized() const { return (collisionConfig.shape != COLLISION_SHAPE_NONE); }
	};

	static CollisionShape stringToCollisionShape(const std::string& a_shape);
	static CollisionDetection stringToCollisionDetection(const std::string& a_detectionMode);
	static std::string collisionShapeToString(CollisionShape a_shape);
	static std::string collisionDetectionToString(CollisionDetection a_detectionMode);

	EntityBtPhysics();
	EntityBtPhysics(BtWorldObject* a_worldObject, const Config& a_config);
	~EntityBtPhysics();

	void init(BtWorldObject* a_worldObject, const Config& a_config);
	void update(f64 dt);
	void clear();
	EntityBtPhysics* clone(BtWorldObject* a_worldObject) const;

	edv::Vector3f		getInitialTranslation() const;
	Ogre::Quaternion	getInitialRotation() const;

	void setScale(const edv::Vector3f& a_scale);
	edv::Vector3f getScale() const;

	bool isKinematic() const;
	btRigidBody* getRigidBody();
	f32 getMass() const;
	CollisionShape getCollisionShape() const;
	CollisionDetection getCollisionDetectionMode() const;
	MagneticProperty* getMagneticProperty();
	bool has2DPhysics() const;
	btPairCachingGhostObject* getGhost() const;
	bool hasTriggerCollision() const;
	bool isStatic() const;

	edv::Vector3f getHalfSize() const;
	void setCollisionEnabled(bool a_enabled);

private:
	void createBody(const CollisionConfig& a_config, bool a_kinematic);

	std::unique_ptr<btCollisionShape>			m_bulletCollisionShape;
	std::unique_ptr<btMotionState>				m_motionState;
	std::unique_ptr<btRigidBody>				m_rigidBody;
	f32											m_mass;
	CollisionConfig								m_collisionConfig;
	std::unique_ptr<MagneticProperty>			m_magnetic_property;
	BtWorldObject*								m_worldObject;
	bool										m_physics2D;
	std::unique_ptr<btPairCachingGhostObject>	m_ghost;
	edv::Vector3f								m_initialScale;
};

#endif
