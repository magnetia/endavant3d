#include <cmath>
#include "Core/CCoreEngine.h"
#include "Time/CTimeManager.h"
#include "Game/GameEntity.h"
#include "RotationAnimator.h"

RotationAnimator::RotationAnimator(f64 a_duration, const Ogre::Degree &a_rotdegrees, const Ogre::Vector3& a_rotaxis) :
	TimedBehavior{ a_duration },
	m_rotationFactor{ 0 },
	m_rotaxis{ a_rotaxis },
	m_rotdegreesfinal{ a_rotdegrees }
{
	
}


Behavior* RotationAnimator::clone()
{
	return new RotationAnimator{ duration(), m_rotdegreesfinal, m_rotaxis };
}

void RotationAnimator::initImpl()
{
	TimedBehavior::initImpl();
	reset();
}

void RotationAnimator::updateImpl(f64 dt)
{
	auto* const node = getParent()->getGraphics()->getNode();

	if (finished())
	{
		node->setOrientation(m_finalorientation);
		//node->rotate(m_rotaxis, m_rotradianfinal);
	}
	else
	{
		node->rotate(m_rotaxis, Ogre::Real(dt) * m_rotationFactor);
	}
}

void RotationAnimator::stopImpl()
{

}

void RotationAnimator::reset()
{
	auto* const node = getParent()->getGraphics()->getNode();
	m_rotradianfinal = m_rotdegreesfinal;

	auto originalorientation = node->_getDerivedOrientation();
	node->rotate(m_rotaxis, m_rotradianfinal);
	m_finalorientation = node->_getDerivedOrientation();
	node->setOrientation(originalorientation);
	
	m_rotationFactor = m_rotradianfinal / Ogre::Real(m_duration);
	
}

