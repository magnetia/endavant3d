#ifndef EDV_GAME_COLLISION_NOTIFIER_H_
#define EDV_GAME_COLLISION_NOTIFIER_H_

#include "ActorMovement.h"

class CollisionNotifier : public ActorMovement
{
public:
	CollisionNotifier();
	~CollisionNotifier();
	void init() override;
	void applyMove(const edv::Vector3f& a_vector) override;
	void applyForce(const edv::Vector3f& a_vector) override;
	void applyImpulse(const edv::Vector3f& a_vector) override;
	void setGravity(const edv::Vector3f& a_vector) override;
	void setRestitution(f32 a_restitution) override;
	void setSliding(f32 a_sliding) override;
	void setFriction(f32 a_friction) override;
	void stepMove(f64 dt, GameCollisions& a_collisionInfo) override;
	bool hasMovement() override;
	void useGravity(bool a_use) override;
	bool onGround() const override;
	bool onCeiling() const override;
	edv::Vector3f getVelocity() override;
	void resetVelocity() override;
};

#endif
