#ifndef EDV_GAME_GLOWANIMATOR_H_
#define EDV_GAME_GLOWANIMATOR_H_

#include <string>
#include "Game/TimedBehavior.h"
#include "Externals/OGRE/Ogre.h"
#include "Utils/edvVector3.h"

class GlowAnimator : public TimedBehavior
{
public:
	class Factory : public Behavior::factory_type
	{
	public:
		Behavior* createElement(const Behavior::factory_param_type& a_config) override;
	};

	GlowAnimator(f64 a_duration = 0, const std::string& a_texture = "", const edv::Vector3f& a_initialLevel = {}, const edv::Vector3f& a_finalLevel = {});
	~GlowAnimator();

	Behavior* clone() override;

private:
	void initImpl() override;
	void stopImpl() override;
	void updateImpl(f64 dt) override;

	std::string			m_texture;
	edv::Vector3f		m_initialLevel;
	edv::Vector3f		m_finalLevel;
	edv::Vector3f		m_vector;
	f64					m_timeFactor;
	Ogre::Material*		m_material;
	Ogre::Pass*			m_pass;
	Ogre::Material*		m_originalMat;
};

#endif
