#include "SphericalCloud.h"
#include "OGRE/OgreTextureManager.h"
#include "VolumeRenderable.h"
#include "ThingRenderable.h"
#include "Core/CCoreEngine.h"
#include "Renderer/CRenderManager.h"
#include "Julia.h"
#include "Input/CInputManager.h"

SphericalCloud::SphericalCloud(const Config& a_config) :
	mTextureName{ "DynaTex_" + a_config.textureName },
	mNode{ nullptr },
	mReal{ a_config.fractalConfig.real },
	mImag{ a_config.fractalConfig.imag },
	mTheta{ a_config.fractalConfig.theta },
	mVRenderable{ nullptr },
	mTRenderable{ nullptr },
	mDevelopment{ false },
	mColorAddition{ a_config.colorAddition },
	mColorFactor{ a_config.colorFactor }
{
	auto sceneMgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();
	mNode = a_config.parent->createChildSceneNode(a_config.initialPosition);
	
	// Create dynamic texture
	mTexPtr = Ogre::TextureManager::getSingleton().createManual(
		mTextureName, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME, Ogre::TEX_TYPE_3D, 64, 64, 64, 0, Ogre::PF_A8R8G8B8);
	mVRenderable = new VolumeRenderable(a_config.volumeConfig.slices, a_config.volumeConfig.size, mTextureName);
	mNode->attachObject(mVRenderable);

	mTRenderable = new ThingRenderable(a_config.quadConfig.radius, a_config.quadConfig.count, a_config.quadConfig.qsize);
	mTRenderable->setMaterial(a_config.quadConfig.materialName);
	mNode->attachObject(mTRenderable);

	generate();
}

SphericalCloud::~SphericalCloud()
{
	Ogre::TextureManager::getSingleton().remove(mTextureName);
	delete mVRenderable;
	delete mTRenderable;
}


void SphericalCloud::modifyReal(f32 a_real)
{
	mReal = a_real;
	generate();
}

void SphericalCloud::modifyImag(f32 a_imag)
{
	mImag = a_imag;
	generate();
}

void SphericalCloud::modifyTheta(f32 a_theta)
{
	mTheta = a_theta;
	generate();
}

void SphericalCloud::modifyColorAddition(const edv::Vector3f& a_vector)
{
	mColorAddition = a_vector;
	generate();
}

void SphericalCloud::modifyColorFactor(const edv::Vector3f& a_vector)
{
	mColorFactor = a_vector;
	generate();
}

Ogre::SceneNode* SphericalCloud::getSceneNode()
{
	return mNode;
}

void SphericalCloud::setDevelopment(bool a_development)
{
	mDevelopment = a_development;
	
	// Register/Unregister actions
	if (mDevelopment)
	{	
		auto& keyboard = CCoreEngine::Instance().GetInputManager().GetKeyboard();

		keyboard.InsertKeyAction("modifyReal", "V");
		keyboard.InsertKeyAction("modifyImag", "N");
		keyboard.InsertKeyAction("modifyTheta", "M");
		keyboard.InsertKeyAction("modifyRFactor", "R");
		keyboard.InsertKeyAction("modifyGFactor", "G");
		keyboard.InsertKeyAction("modifyBFactor", "B");
	}
	else
	{
		auto& keyboard = CCoreEngine::Instance().GetInputManager().GetKeyboard();
		keyboard.RemoveKeyAction("modifyReal");
		keyboard.RemoveKeyAction("modifyImag");
		keyboard.RemoveKeyAction("modifyTheta");
		keyboard.RemoveKeyAction("modifyRFactor");
		keyboard.RemoveKeyAction("modifyGFactor");
		keyboard.RemoveKeyAction("modifyBFactor");
	}
}

void SphericalCloud::update(f64 dt)
{
	static_cast<ThingRenderable*>(mTRenderable)->addTime(static_cast<f32>(dt) * 0.05f);
	
	if (mDevelopment)
	{
		if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("modifyReal"))
		{
			f32 newValue = mReal + 0.1f;
			newValue = (newValue > 1.0f) ? -1.0f : newValue;
			modifyReal(newValue);
		}
		else if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("modifyImag"))
		{
			f32 newValue = mImag + 0.1f;
			newValue = (newValue > 1.0f) ? -1.0f : newValue;
			modifyImag(newValue);
		}
		else if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("modifyTheta"))
		{
			f32 newValue = mTheta + 0.1f;
			newValue = (newValue > 1.0f) ? -1.0f : newValue;
			modifyTheta(newValue);
		}
		else if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("modifyRFactor"))
		{
			f32 newValue = mColorFactor.x + 0.1f;
			newValue = (newValue > 1.0f) ? 0.0f : newValue;
			edv::Vector3f newVector{ newValue, mColorFactor.y, mColorFactor .z };
			modifyColorFactor(newVector);
		}
		else if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("modifyGFactor"))
		{
			f32 newValue = mColorFactor.y + 0.1f;
			newValue = (newValue > 1.0f) ? 0.0f : newValue;
			edv::Vector3f newVector{ mColorFactor.x, newValue, mColorFactor.z };
			modifyColorFactor(newVector);
		}
		else if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("modifyBFactor"))
		{
			f32 newValue = mColorFactor.z + 0.1f;
			newValue = (newValue > 1.0f) ? 0.0f : newValue;
			edv::Vector3f newVector{ mColorFactor.x, mColorFactor.y, newValue };
			modifyColorFactor(newVector);
		}
	}
}

void SphericalCloud::generate()
{
	/* Evaluate julia fractal for each point */
	Julia julia(mReal, mImag, mTheta);
	const float scale = 2.5;
	const float vcut = 29.0f;
	const float vscale = 1.0f / vcut;
	
	Ogre::HardwarePixelBufferSharedPtr buffer = mTexPtr->getBuffer(0, 0);
	//Ogre::StringStream d;
	//d << "HardwarePixelBuffer " << buffer->getWidth() << " " << buffer->getHeight() << " " << buffer->getDepth();
	//Ogre::LogManager::getSingleton().logMessage(d.str());

	buffer->lock(Ogre::HardwareBuffer::HBL_NORMAL);
	const Ogre::PixelBox &pb = buffer->getCurrentLock();
	//d.str("");
	//d << "PixelBox " << pb.getWidth() << " " << pb.getHeight() << " " << pb.getDepth() << " " << pb.rowPitch << " " << pb.slicePitch << " " << pb.data << " ";// << Ogre::PixelUtil::getFormatName(pb.format);
	//Ogre::LogManager::getSingleton().logMessage(d.str());

	Ogre::uint32 *pbptr = static_cast<Ogre::uint32*>(pb.data);
	for (size_t z = pb.front; z<pb.back; z++)
	{
		for (size_t y = pb.top; y<pb.bottom; y++)
		{
			for (size_t x = pb.left; x<pb.right; x++)
			{
				if (z == pb.front || z == (pb.back - 1) || y == pb.top || y == (pb.bottom - 1) ||
					x == pb.left || x == (pb.right - 1))
				{
					// On border, must be zero
					pbptr[x] = 0;
				}
				else
				{
					float val = julia.eval(((float)x / pb.getWidth() - 0.5f) * scale,
						((float)y / pb.getHeight() - 0.5f) * scale,
						((float)z / pb.getDepth() - 0.5f) * scale);
					if (val > vcut)
						val = vcut;

					float r = (float)x / pb.getWidth()	* mColorFactor.x + mColorAddition.x;
					float g = (float)y / pb.getHeight()	* mColorFactor.y + mColorAddition.y;
					float b = (float)z / pb.getDepth()	* mColorFactor.z + mColorAddition.z;

					Ogre::PixelUtil::packColour(r, g, b, (1.0f - (val*vscale))*0.7f, Ogre::PF_A8R8G8B8, &pbptr[x]);
				}
			}
			pbptr += pb.rowPitch;
		}
		pbptr += pb.getSliceSkip();
	}
	buffer->unlock();
}

