#ifndef EDV_GAME_SCALE_ANIMATOR_H_
#define EDV_GAME_SCALE_ANIMATOR_H_

#include "Game/TimedBehavior.h"
#include "Utils/edvVector3.h"

class ScaleAnimator : public TimedBehavior
{
public:
	ScaleAnimator(f64 a_duration, const edv::Vector3f& a_initialScale, const edv::Vector3f& a_finalScale);
	Behavior* clone() override;

	const edv::Vector3f& getTimeFactor() const;
	const edv::Vector3f& getInitialScale() const;
	const edv::Vector3f& getFinalScale() const;

private:
	void initImpl() override;
	void stopImpl() override;
	void updateImpl(f64 dt) override;

	void reset();

	edv::Vector3f	m_timeFactor;
	edv::Vector3f	m_initialScale;
	edv::Vector3f	m_finalScale;
};

#endif
