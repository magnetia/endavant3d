#ifndef EDV_GAME_ACTOR_H_
#define EDV_GAME_ACTOR_H_

#include <memory>
#include "Core/CBasicTypes.h"
#include "Utils/ChildOf.h"
#include "Utils/Factory.h"
#include "Utils/any.h"
#include "Utils/tree.h"
#include "Utils/edvVector3.h"
#include "Script/ScriptConfig.h"
#include "ActorMovement.h"

class GameEntity;

class Actor : public ChildOf<GameEntity>
{
public:
	typedef edv::tree<edv::any>					factory_param_type;
	typedef Factory<Actor, factory_param_type>	factory_type;

	Actor(s32 a_maxLife = 0, const ScriptConfig& a_scriptConfig = {});
	virtual ~Actor();

	virtual Actor* clone() = 0;
	virtual void init() final;
	virtual void update(f64 dt) final;
	virtual void stop() final;
	virtual void clear();
	// afegeix una colisio detectada pel manager de fisiques.
	virtual void addCollision(const CollisionInfo& a_collisionInfo);
	// processa una colisio independentment del seu origen
	virtual void onCollision(const CollisionInfo& a_collisionInfo);
	virtual void onCollisionEnter(GameEntity* a_entity);
	virtual void onCollisionExit(GameEntity* a_entity);

	// #todo: en comptes de duplicar la interficie del ActorMovement fer un getter d'aquest
	virtual void applyMove(const edv::Vector3f& a_vector);
	virtual void applyForce(const edv::Vector3f& a_vector);
	virtual void applyImpulse(const edv::Vector3f& a_vector);
	virtual void setGravity(const edv::Vector3f& a_vector);
	virtual edv::Vector3f getVelocity() const;
	virtual void resetVelocity();
	virtual f64 stateElapsed() const;
	virtual s32 getState() const;
	virtual void setState(s32 a_state);
	virtual void setLife(s32 a_life);
	virtual void setMaxLife();
	virtual void decreaseLife(s32 a_life);
	virtual void kill();
	virtual s32 getLife() const;
	virtual s32 getMaxLife() const;
	virtual bool isOnCeiling() const;
	virtual bool isOnGround() const;

protected:
	typedef std::unique_ptr<ActorMovement>	MovementPtr;

	virtual void initImpl() = 0;
	virtual void updateImpl(f64 dt) = 0;
	virtual void stopImpl() = 0;

	virtual const ScriptConfig& getScriptConfig() const;
	virtual ActorMovement* createMovement();

	s32	m_state;
	f64	m_stateTimestamp;
	s32 m_life;
	s32 m_maxLife;
	ScriptConfig	m_scriptConfig;
	MovementPtr		m_movement;
	GameCollisions	m_collisions;
	GameCollisions	m_lastFrameCollisions;
};

#endif
