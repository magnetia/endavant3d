#include "SpeedController.h"
#include "Core/CCoreEngine.h"
#include "Time/CTimeManager.h"
#include "Options/OptionManager.h"
#include "Physics/CPhysicsManager.h"
#include "GameEntity.h"

Behavior* SpeedController::Factory::createElement(const Behavior::factory_param_type& a_config)
{
	return new SpeedController{ a_config["max_speed"].as<f32>() };
}

SpeedController::SpeedController(f32 a_max_speed) :
	m_max_speed{ a_max_speed }
{
	
}

SpeedController::~SpeedController()
{

}

Behavior* SpeedController::clone()
{
	return new SpeedController(m_max_speed);
}

void SpeedController::initImpl()
{
	// Register tick callback
	m_callbackId = CCoreEngine::Instance().GetPhysicsManager().RegisterBulletTickCallback(std::bind(&SpeedController::physicsTickCallback, this, std::placeholders::_1));
}

void SpeedController::stopImpl()
{
	// Unregister tick callback
	if (!m_callbackId.empty())
	{
		CCoreEngine::Instance().GetPhysicsManager().UnRegisterBulletTickCallback(m_callbackId);
	}
}

void SpeedController::updateImpl(f64 dt)
{

}

void SpeedController::pauseImpl()
{
}

void SpeedController::resumeImpl()
{
}

void SpeedController::setMaxSpeed(f32 a_max_speed)
{
	m_max_speed = a_max_speed;
}

f32 SpeedController::getMaxSpeed() const
{
	return m_max_speed;
}

void SpeedController::physicsTickCallback(f32 a_timeStep)
{
	btRigidBody* rigid_body = getParent()->getPhysics()->getRigidBody();
	btVector3 velocity = rigid_body->getLinearVelocity();
	btScalar speed = velocity.length();
	if (speed > m_max_speed)
	{
		velocity *= m_max_speed / speed;
		rigid_body->setLinearVelocity(velocity);
	}
}
