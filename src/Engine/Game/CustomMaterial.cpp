#include "CustomMaterial.h"

CustomMaterial::CustomMaterial() :
	m_entity{ nullptr },
	m_originalMat{ nullptr },
	m_material{ nullptr },
	m_pass{ nullptr }
{

}

CustomMaterial::~CustomMaterial()
{

}

void CustomMaterial::reset()
{
	m_entity = nullptr;
	m_originalMat = nullptr;
	m_material = nullptr;
	m_pass = nullptr;
}
