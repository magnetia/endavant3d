#include "Controller.h"
#include "Core/CCoreEngine.h"
#include "Input/CInputManager.h"
#include "Game/GameEntity.h"
#include "Options\OptionManager.h"

Behavior* Controller::Factory::createElement(const Behavior::factory_param_type& a_config)
{
	return new Controller{{
		{ a_config["right/action_name"], a_config["right/key"],
		{ a_config["right/vect/x"], a_config["right/vect/y"], a_config["right/vect/z"] }, a_config["right/gpad_button"] },

		{ a_config["left/action_name"], a_config["left/key"],
		{ a_config["left/vect/x"], a_config["left/vect/y"], a_config["left/vect/z"] }, a_config["left/gpad_button"] },

		{ a_config["up/action_name"], a_config["up/key"],
		{ a_config["up/vect/x"], a_config["up/vect/y"], a_config["up/vect/z"] }, a_config["up/gpad_button"] },

		{ a_config["down/action_name"], a_config["down/key"],
		{ a_config["down/vect/x"], a_config["down/vect/y"], a_config["down/vect/z"] }, a_config["down/gpad_button"] },

		{ a_config["jump/action_name"], a_config["jump/key"],
		{ a_config["jump/vect/x"], a_config["jump/vect/y"], a_config["jump/vect/z"] }, a_config["jump/gpad_button"] },

		{ a_config["crouch/action_name"], a_config["crouch/key"],
		{ a_config["crouch/vect/x"], a_config["crouch/vect/y"], a_config["crouch/vect/z"] }, a_config["crouch/gpad_button"] },

	

		}};
}

Controller::Controller(const Actions& a_actions) :
	m_actions{ a_actions }
{

}

Behavior* Controller::clone()
{
	return new Controller{ m_actions };
}

void Controller::initImpl()
{
	addActions();
}

void Controller::updateImpl(f64 dt)
{
	edv::Vector3f move{ 0.f, 0.f, 0.f };
	const bool useGC = CCoreEngine::Instance().GetOptionManager().getOption("controls/usegamepad");

	if (useGC)
	{
		auto &gpad = CCoreEngine::Instance().GetInputManager().GetGameController();
		auto gamepads = gpad.getGameControllersIDs();

		if (!gamepads.empty())
		{
			for (auto action : m_actions)
			{
				if (gpad.getGameController(gamepads.at(0)).lock()->IsActionKeyPushed(action.name))
				{
					move += action.vect;
				}
			}
		}
	}
	else
	{
		auto& keyb = CCoreEngine::Instance().GetInputManager().GetKeyboard();
		for (auto action : m_actions)
		{
			if (keyb.IsActionKeyPushed(action.name))
			{
				move += action.vect;
			}
		}
	}


	if (!move.isZero())
	{
		getParent()->getActor()->applyMove(move);
	}

}

void Controller::stopImpl()
{
	removeActions();
}

void Controller::pauseImpl()
{
	removeActions();
}

void Controller::resumeImpl()
{
	addActions();
}

void Controller::addActions()
{
	removeActions();
	auto &keyb = CCoreEngine::Instance().GetInputManager().GetKeyboard();
	auto &gpad = CCoreEngine::Instance().GetInputManager().GetGameController();
	auto gamepads = gpad.getGameControllersIDs();

	for (auto& action : m_actions)
	{
		keyb.InsertKeyAction(action.name, action.key);
		if (!action.gcButtName.empty() && !gamepads.empty())
			gpad.getGameController(gamepads.at(0)).lock()->InsertKeyAction(action.name, action.gcButtName);

	}
}

void Controller::removeActions()
{
	auto &keyb = CCoreEngine::Instance().GetInputManager().GetKeyboard();
	auto &gpad = CCoreEngine::Instance().GetInputManager().GetGameController();
	auto gamepads = gpad.getGameControllersIDs();

	for (auto& action : m_actions)
	{
		keyb.RemoveKeyAction(action.name);
		if (!action.gcButtName.empty() && !gamepads.empty())
			gpad.getGameController(gamepads.at(0)).lock()->RemoveKeyAction(action.name);
	}
}
