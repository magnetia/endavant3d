#include "TextureAnimation.h"
#include "Ogre.h"
#include "Core/CCoreEngine.h"
#include "Time/CTimeManager.h"

TextureAnimation::TextureAnimation(f64 a_duration, const std::string& a_materialName, bool a_loop) :
	m_duration{ a_duration },
	m_startTimestamp{ 0. },
	m_totalFrames{ 0 },
	m_loop{ a_loop }
{
	init(a_materialName);
}

TextureAnimation::TextureAnimation(const Config& a_config) :
	m_duration{ a_config.duration },
	m_startTimestamp{ 0. },
	m_totalFrames{ 0 },
	m_loop{ a_config.loop }
{
	init(a_config.materialName);
}

void TextureAnimation::init(const std::string& a_materialName)
{
	// Get the material by name
	Ogre::ResourcePtr resptr = Ogre::MaterialManager::getSingleton().getByName(a_materialName);
	Ogre::Material * mat = dynamic_cast<Ogre::Material*>(resptr.getPointer());

	Ogre::Technique *tech = mat->getTechnique(0);
	Ogre::Pass *pass = tech->getPass(0);
	m_textureUnit = pass->getTextureUnitState(0);
	m_totalFrames = m_textureUnit->getNumFrames();
}

void TextureAnimation::start()
{
	m_startTimestamp = CCoreEngine::Instance().GetTimerManager().GetTotalTimeSeconds();
	m_textureUnit->setCurrentFrame(0);
}

void TextureAnimation::update(f64 dt)
{
	if (isActive())
	{
		if (!isFinished())
		{
			u32 frame = static_cast<u32>(getElapsed()/getDuration()*m_totalFrames);
			m_textureUnit->setCurrentFrame(frame);
		}
		else
		{
			if (m_loop)
			{
				start();
			}
			else
			{
				stop();
			}
		}
	}
}

void TextureAnimation::stop()
{
	m_textureUnit->setCurrentFrame(0);
	m_startTimestamp = 0.;
}

bool TextureAnimation::isFinished() const
{
	return (getElapsed() >= getDuration());
}

f64 TextureAnimation::getStartTime() const
{
	return m_startTimestamp;
}

f64 TextureAnimation::getDuration() const
{
	return m_duration;
}

f64 TextureAnimation::getElapsed() const
{
	const f64 now = CCoreEngine::Instance().GetTimerManager().GetTotalTimeSeconds();
	return (now - m_startTimestamp);
}

f64 TextureAnimation::getRemaining() const
{
	return (getDuration() - getElapsed());
}

bool TextureAnimation::isActive() const
{
	return (m_startTimestamp != 0.);
}