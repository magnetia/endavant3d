#include <BulletCollision/CollisionDispatch/btGhostObject.h>
#include "Core/CCoreEngine.h"
#include "Physics/btResultCallback.hpp"
#include "Physics/CPhysicsManager.h"
#include "Actor.h"
#include "GameEntity.h"
#include "KinematicController.h"

#include "Renderer/CRenderDebug.h"

// #debug:
#include <Core/CCoreEngine.h>
#include <Core/CLogManager.h>

KinematicController::KinematicController() :
	ActorMovement{},
	m_gravity_acceleration{ 0.f, -10.f, 0.f },
	m_linear_velocity{ 0.f, 0.f, 0.f },
	m_total_acceleartion{ 0.f, 0.f, 0.f },
	m_restitution{ 0.1f },
	m_sliding{ 0.1f },
	m_friction{ 0.f },
	m_use_gravity{true},
	m_on_ground{false},
	m_on_ceiling{ false },
	m_on_left{ false },
	m_on_right{ false },
	m_movement{ false },
	m_impulse{ false },
	m_collisionMargin{ 1.f },
	m_checkMargin{ 0.2f }
{
	
}

void KinematicController::init()
{
}

void KinematicController::applyMove(const edv::Vector3f& a_vector)
{
	m_total_acceleartion += a_vector;
	m_movement = true;
}

void KinematicController::applyForce(const edv::Vector3f& a_vector)
{
	m_total_acceleartion += a_vector;
}

void KinematicController::applyImpulse(const edv::Vector3f& a_vector)
{
	GameEntity* entity = getParent()->getParent();
	m_linear_velocity += a_vector*entity->getPhysics()->getRigidBody()->getLinearFactor();
	m_impulse = true;
}

bool KinematicController::checkPenetration()
{
	u32 numPenetrationLoops = 0;
	const u32 max_loops = 4;
	const btScalar recover_factor = 0.1f;
	while (recoverFromPenetration(recover_factor))
	{
		numPenetrationLoops++;
		if (numPenetrationLoops > max_loops)
		{
			//std::cout << "-------------------> recoverFromPenetration: MAX LOOPS " << getParent()->getParent()->getName() << std::endl;
			break;
		}
	}

	return (numPenetrationLoops != 0);
}

void KinematicController::stepMove(f64 dt, GameCollisions& a_collisionInfo)
{
	GameCollisions collisions;
	GameEntity* entity = getParent()->getParent();

	if (!entity->getPhysics()->getGhost())
		return;

	// - Initialize positons
	m_current_position = entity->getWorldPosition();
	m_target_position = m_current_position;

	// 0. Apply friction
	m_linear_velocity += applyFriction(m_linear_velocity);

	// 1. Apply gravity
	applyGravity(m_gravity_acceleration);
	
	// 2. Apply total force
	edv::Vector3f normal_hit(0.f, 0.f, 0.f);
	m_linear_velocity += (m_total_acceleartion)*static_cast<f32>(dt);
	m_linear_velocity *= entity->getPhysics()->getRigidBody()->getLinearFactor();

	if (!m_linear_velocity.isZero())
	{
		if (internalStep(collisions, normal_hit, dt, m_linear_velocity))
		{
			updateVelocityOnCollision(m_linear_velocity, normal_hit, m_sliding, m_restitution);
		}
	}

	// 4. Set new position
	setPosition(m_target_position);

	//std::cout << getParent()->getParent()->getName() << "current_position: x=" << m_current_position.x << " y=" << m_current_position.y << " z=" << m_current_position.z << std::endl;
	//std::cout << getParent()->getParent()->getName() << "target_position: x=" << m_target_position.x << " y=" << m_target_position.y << " z=" << m_target_position.z << std::endl;
	//std::cout << getParent()->getParent()->getName() << "distance: " << m_target_position.distance(m_target_position) << std::endl;

	// - Check position
	checkPosition(collisions);

	// 5. clear movement
	postStep();

	// 6. Notify collisions to other actors
	for (CollisionInfo& collision : collisions)
	{
		if (Actor* actor = collision.target->getActor())
		{
			actor->addCollision(CollisionInfo{ getParent()->getParent(), collision.hitPoint, collision.hitNormal });
		}
	}

	a_collisionInfo.insert(a_collisionInfo.begin(), collisions.begin(), collisions.end());
}

void KinematicController::setGravity(const edv::Vector3f& a_vector)
{
	m_gravity_acceleration = a_vector;
}

void KinematicController::setRestitution(f32 a_restitution)
{
	m_restitution = a_restitution;
}

void KinematicController::setSliding(f32 a_sliding)
{
	m_sliding = a_sliding;
}

void KinematicController::setFriction(f32 a_friction)
{
	m_friction = a_friction;
}

void KinematicController::setPosition(const edv::Vector3f& a_position)
{
	GameEntity* const entity = getParent()->getParent();
	const edv::Vector3f new_position = a_position*entity->getPhysics()->getRigidBody()->getLinearFactor();
	const edv::Vector3f prev_position = entity->getWorldPosition();
	entity->setWorldPosition(new_position);

	//std::cout << " dpos =" << new_position.x - prev_position.x << std::endl;

	if (auto ghost = entity->getPhysics()->getGhost())
	{
		// Synchronises motion state to ghost
		btTransform new_ghost_transform;
		entity->getPhysics()->getRigidBody()->getMotionState()->getWorldTransform(new_ghost_transform);
		ghost->setWorldTransform(new_ghost_transform);
	}

	m_current_position = a_position;
}

bool KinematicController::internalStep(GameCollisions& a_collisionInfo, edv::Vector3f& a_normal_hit, f64 dt, const edv::Vector3f& a_linear_velocity)
{
	bool collision = false;
	btVector3 linear_velocity(a_linear_velocity.to_btVector3());

	if (linear_velocity.isZero())
		return collision;

	// Compute new position
	btTransform from_transform, to_transform;
	from_transform.setIdentity(); to_transform.setIdentity();
	from_transform.setOrigin(m_current_position.to_btVector3());

	const f32 delta_time(static_cast<f32>(dt));
	const btVector3 ang_velocity(0.f, 0.f, 0.f);
	btTransformUtil::integrateTransform(from_transform, linear_velocity, ang_velocity, delta_time, to_transform);
	m_target_position = to_transform.getOrigin();

	// Check valid movement
	if ((m_target_position - m_current_position).length() == 0)
		return collision;
	
	// Check collision
	GameEntity* const entity = getParent()->getParent();
	btRigidBody* body = entity->getPhysics()->getRigidBody();
	btConvexShape* convex_shape = static_cast<btConvexShape*>(body->getCollisionShape());
	auto ghost = entity->getPhysics()->getGhost();
	const btVector3 linear_velocity_negative(-linear_velocity);
	const btScalar minSlopeDot(0.1f);
	btMultipleClosestNotMeConvexResultCallback callback(entity, linear_velocity_negative, minSlopeDot);
	f32 margin = convex_shape->getMargin();
	convex_shape->setMargin(m_collisionMargin);
	ghost->convexSweepTest(convex_shape, from_transform, to_transform, callback, CCoreEngine::Instance().GetPhysicsManager().getWorld()->getDispatchInfo().m_allowedCcdPenetration);
	convex_shape->setMargin(margin);
	GameEntity* hit_entity = nullptr;
	bool trigger_collision = false;
	
	//std::cout << "Collisions: " << callback.results.size() << std::endl;

	callback.m_hitNormalWorld.setZero();
	for (u32 i = 0; i < callback.results.size(); i++)
	{
		callback.m_hitNormalWorld += callback.results.at(i).m_hitNormalLocal;
		callback.m_closestHitFraction = std::min<f32>(callback.results.at(i).m_hitFraction, callback.m_closestHitFraction);
	}

	//std::cout << "m_closestHitFraction: " << callback.m_closestHitFraction << std::endl;

	if (callback.hasHit())
	{
		const btVector3 hitNormalWorld = callback.m_hitNormalWorld.normalized();
		//std::cout << "point: " << hitNormalWorld.x() << " " << hitNormalWorld.y() << hitNormalWorld.z() << std::endl;

		for (u32 i = 0; i < callback.results.size(); i++)
		{
			auto result = callback.results.at(i);
			// Si la entitat no es de collisio de tipus callback, processem la colisio
			hit_entity = static_cast<GameEntity*>(result.m_hitCollisionObject->getUserPointer());
			trigger_collision = hit_entity->getPhysics()->hasTriggerCollision();
			if (!trigger_collision)
			{
				collision = true;
				a_collisionInfo.push_back({ hit_entity, result.m_hitPointLocal, result.m_hitNormalLocal });
			}
		}

		if (collision)
		{
			// 1a opcio, si nomes volem aproximacio a la collisio.
			if (m_movement && m_on_ground || abs(hitNormalWorld.y()) > 0.9f)
			{
				btVector3 target_position{ m_target_position.to_btVector3() };
				target_position.setInterpolate3(m_current_position.to_btVector3(), target_position, callback.m_closestHitFraction);
				m_target_position = target_position;
			}
			else
			{
				// 2a opcio, si volem que tingui en compte la recol�locaci�.
				m_target_position = updateTargetPositionBasedOnCollision(m_current_position.to_btVector3(), m_target_position.to_btVector3(), hitNormalWorld, 0.1f, 0.1f);
			}
		}
		a_normal_hit = hitNormalWorld;
	}

	return collision;
}

void KinematicController::updateVelocityOnCollision(edv::Vector3f& a_velocity, const edv::Vector3f& a_normal_hit, f32 a_tangentFactor, f32 a_normalFactor)
{
	// Apply restitution and slidding
	btVector3 reflectDir = computeReflectionDirection(a_velocity.to_btVector3(), a_normal_hit.to_btVector3());
	btVector3 paralleltDir = parallelComponent(reflectDir, a_normal_hit.to_btVector3());
	btVector3 perpComp = perpindicularComponent(reflectDir, a_normal_hit.to_btVector3());
	a_velocity = paralleltDir*a_normalFactor + perpComp*a_tangentFactor;
	//std::cout << m_linear_velocity << std::endl;
}

void KinematicController::postStep()
{
	m_total_acceleartion.setZero();
	if (!m_linear_velocity.isZero() && m_linear_velocity.squaredLength() < 0.5f)
	{
		m_linear_velocity.setZero();
	}
	m_movement = m_impulse = false;
}

void KinematicController::checkPosition(GameCollisions& a_collisionInfo)
{
	// - Check penetration
	//checkPenetration();
	
	// - Check on ground
	m_on_ground = checkOnGround(a_collisionInfo);
	// - Check on ceiling
	m_on_ceiling = checkOnCeiling(a_collisionInfo);
	// - Check on left
	m_on_left = checkOnLeft(a_collisionInfo);
	// - Check on right
	m_on_right = checkOnRight(a_collisionInfo);
}

bool KinematicController::checkOnGround(GameCollisions& a_collisionInfo)
{
	bool returnValue{ false };
	GameEntity* entity = getParent()->getParent();
	if (btPairCachingGhostObject* ghost = entity->getPhysics()->getGhost())
	{
		btTransform transform = ghost->getWorldTransform();
		btVector3 aabbMin, aabbMax;
		ghost->getCollisionShape()->getAabb(transform, aabbMin, aabbMax);
		btScalar height = aabbMax.y() - aabbMin.y();
		btScalar width = aabbMax.x() - aabbMin.x();

		btTransform from_transform, to_transform;
		from_transform.setIdentity(); to_transform.setIdentity();
		from_transform.setOrigin(m_current_position.to_btVector3());
		to_transform.setOrigin(btVector3(m_current_position.x, m_current_position.y - m_collisionMargin - m_checkMargin, m_current_position.z));

		btRigidBody* body = entity->getPhysics()->getRigidBody();
		btConvexShape* convex_shape = static_cast<btConvexShape*>(body->getCollisionShape());
		const btVector3 neg_up_vector(0.f, 1.f, 0.f);
		const btScalar minSlopeDot(0.f);
		btMultipleClosestNotMeConvexResultCallback callback(entity, neg_up_vector, minSlopeDot);
		f32 margin = convex_shape->getMargin();
		convex_shape->setMargin(m_collisionMargin);
		ghost->convexSweepTest(convex_shape, from_transform, to_transform, callback, CCoreEngine::Instance().GetPhysicsManager().getWorld()->getDispatchInfo().m_allowedCcdPenetration);
		convex_shape->setMargin(margin);
		if (callback.hasHit())
		{
			GameEntity* hit_entity = static_cast<GameEntity*>(callback.m_hitCollisionObject->getUserPointer());
			bool trigger_collision = (hit_entity && hit_entity->getPhysics() && hit_entity->getPhysics()->hasTriggerCollision());
			returnValue = (!trigger_collision && callback.m_hitNormalWorld.y() > 0.9f);
		}
	}
	return returnValue;
}

bool KinematicController::checkOnCeiling(GameCollisions& a_collisionInfo)
{
	bool returnValue{ false };
	GameEntity* entity = getParent()->getParent();
	if (btPairCachingGhostObject* ghost = entity->getPhysics()->getGhost())
	{
		btTransform transform = ghost->getWorldTransform();
		btVector3 aabbMin, aabbMax;
		ghost->getCollisionShape()->getAabb(transform, aabbMin, aabbMax);
		btScalar height = aabbMax.y() - aabbMin.y();
		btScalar width = aabbMax.x() - aabbMin.x();

		btTransform from_transform, to_transform;
		from_transform.setIdentity(); to_transform.setIdentity();
		from_transform.setOrigin(m_current_position.to_btVector3());
		to_transform.setOrigin(btVector3(m_current_position.x, m_current_position.y + m_collisionMargin + m_checkMargin, m_current_position.z));

		btRigidBody* body = entity->getPhysics()->getRigidBody();
		btConvexShape* convex_shape = static_cast<btConvexShape*>(body->getCollisionShape());
		const btVector3 neg_up_vector(0.f, -1.f, 0.f);
		const btScalar minSlopeDot(0.f);
		btMultipleClosestNotMeConvexResultCallback callback(entity, neg_up_vector, minSlopeDot);
		f32 margin = convex_shape->getMargin();
		convex_shape->setMargin(m_collisionMargin);
		ghost->convexSweepTest(convex_shape, from_transform, to_transform, callback, CCoreEngine::Instance().GetPhysicsManager().getWorld()->getDispatchInfo().m_allowedCcdPenetration);
		convex_shape->setMargin(margin);
		if (callback.hasHit())
		{
			GameEntity* hit_entity = static_cast<GameEntity*>(callback.m_hitCollisionObject->getUserPointer());
			bool trigger_collision = (hit_entity && hit_entity->getPhysics() && hit_entity->getPhysics()->hasTriggerCollision());
			returnValue = (!trigger_collision && callback.m_hitNormalWorld.y() < -0.9f);
		}
	}
	return returnValue;
}

bool KinematicController::checkOnLeft(GameCollisions& a_collisionInfo)
{
	bool returnValue{ false };
	GameEntity* entity = getParent()->getParent();
	if (btPairCachingGhostObject* ghost = entity->getPhysics()->getGhost())
	{
		btTransform transform = ghost->getWorldTransform();
		btVector3 aabbMin, aabbMax;
		ghost->getCollisionShape()->getAabb(transform, aabbMin, aabbMax);
		btScalar height = aabbMax.y() - aabbMin.y();
		btScalar width = aabbMax.x() - aabbMin.x();

		btTransform from_transform, to_transform;
		from_transform.setIdentity(); to_transform.setIdentity();
		from_transform.setOrigin(m_current_position.to_btVector3());
		to_transform.setOrigin(btVector3(m_current_position.x - m_collisionMargin - m_checkMargin, m_current_position.y, m_current_position.z));

		btRigidBody* body = entity->getPhysics()->getRigidBody();
		btConvexShape* convex_shape = static_cast<btConvexShape*>(body->getCollisionShape());
		const btVector3 neg_up_vector(1.f, 0.f, 0.f);
		const btScalar minSlopeDot(0.f);
		btMultipleClosestNotMeConvexResultCallback callback(entity, neg_up_vector, minSlopeDot);
		f32 margin = convex_shape->getMargin();
		convex_shape->setMargin(m_collisionMargin);
		ghost->convexSweepTest(convex_shape, from_transform, to_transform, callback, CCoreEngine::Instance().GetPhysicsManager().getWorld()->getDispatchInfo().m_allowedCcdPenetration);
		convex_shape->setMargin(margin);
		if (callback.hasHit())
		{
			GameEntity* hit_entity = static_cast<GameEntity*>(callback.m_hitCollisionObject->getUserPointer());
			bool trigger_collision = (hit_entity && hit_entity->getPhysics() && hit_entity->getPhysics()->hasTriggerCollision());
			returnValue = (!trigger_collision && callback.m_hitNormalWorld.x() > 0.9f);
		}
	}
	return returnValue;
}

bool KinematicController::checkOnRight(GameCollisions& a_collisionInfo)
{
	bool returnValue{ false };
	GameEntity* entity = getParent()->getParent();
	if (btPairCachingGhostObject* ghost = entity->getPhysics()->getGhost())
	{
		btTransform transform = ghost->getWorldTransform();
		btVector3 aabbMin, aabbMax;
		ghost->getCollisionShape()->getAabb(transform, aabbMin, aabbMax);
		btScalar height = aabbMax.y() - aabbMin.y();
		btScalar width = aabbMax.x() - aabbMin.x();

		btTransform from_transform, to_transform;
		from_transform.setIdentity(); to_transform.setIdentity();
		from_transform.setOrigin(m_current_position.to_btVector3());
		to_transform.setOrigin(btVector3(m_current_position.x + m_collisionMargin + m_checkMargin, m_current_position.y, m_current_position.z));

		btRigidBody* body = entity->getPhysics()->getRigidBody();
		btConvexShape* convex_shape = static_cast<btConvexShape*>(body->getCollisionShape());
		const btVector3 neg_up_vector(-1.f, 0.f, 0.f);
		const btScalar minSlopeDot(0.f);
		btMultipleClosestNotMeConvexResultCallback callback(entity, neg_up_vector, minSlopeDot);
		f32 margin = convex_shape->getMargin();
		convex_shape->setMargin(m_collisionMargin);
		ghost->convexSweepTest(convex_shape, from_transform, to_transform, callback, CCoreEngine::Instance().GetPhysicsManager().getWorld()->getDispatchInfo().m_allowedCcdPenetration);
		convex_shape->setMargin(margin);
		if (callback.hasHit())
		{
			GameEntity* hit_entity = static_cast<GameEntity*>(callback.m_hitCollisionObject->getUserPointer());
			bool trigger_collision = (hit_entity && hit_entity->getPhysics() && hit_entity->getPhysics()->hasTriggerCollision());
			returnValue = (!trigger_collision && callback.m_hitNormalWorld.x() < -0.9f);
		}
	}
	return returnValue;
}

void KinematicController::applyGravity(const edv::Vector3f& a_velocity)
{
	if (!m_on_ground && m_use_gravity)
	{
		applyForce(m_gravity_acceleration);
	}
}

edv::Vector3f KinematicController::applyFriction(const edv::Vector3f& a_velocity)
{
	return (-a_velocity)*m_friction;
}

bool KinematicController::onGround() const
{
	return m_on_ground;
}

bool KinematicController::onCeiling() const
{
	return m_on_ceiling;
}

btVector3 KinematicController::computeReflectionDirection(const btVector3& a_direction, const btVector3& a_normal)
{
	return a_direction - (btScalar(2.0) * a_direction.dot(a_normal)) * a_normal;
}

btVector3 KinematicController::parallelComponent(const btVector3& a_direction, const btVector3& a_normal)
{
	btScalar magnitude = a_direction.dot(a_normal);
	return a_normal * magnitude;
}

btVector3 KinematicController::perpindicularComponent(const btVector3& a_direction, const btVector3& a_normal)
{
	return a_direction - parallelComponent(a_direction, a_normal);
}

btVector3 KinematicController::updateTargetPositionBasedOnCollision(const btVector3& a_from, const btVector3& a_to, const btVector3& a_normal_hit, btScalar a_tangentMag, btScalar a_normalMag)
{
	btVector3 current_position(a_from), target_position(a_to);
	btVector3 movementDirection = target_position - current_position;
	btScalar movementLength = movementDirection.length();
	if (movementLength>SIMD_EPSILON)
	{
		movementDirection.normalize();

		btVector3 reflectDir = computeReflectionDirection(movementDirection, a_normal_hit);
		reflectDir.normalize();

		btVector3 parallelDir, perpindicularDir;

		parallelDir = parallelComponent(reflectDir, a_normal_hit);
		perpindicularDir = perpindicularComponent(reflectDir, a_normal_hit);

		target_position = current_position;
		if (a_tangentMag != 0.0)
		{
			btVector3 parComponent = parallelDir * btScalar(a_tangentMag*movementLength);
			target_position += parComponent;
		}

		if (a_normalMag != 0.0)
		{
			btVector3 perpComponent = perpindicularDir * btScalar(a_normalMag*movementLength);
			target_position += perpComponent;
		}
	}
	return target_position;
}

bool KinematicController::recoverFromPenetration(btScalar a_recover_fraction)
{
	// Here we must refresh the overlapping paircache as the penetrating movement itself or the
	// previous recovery iteration might have used setWorldTransform and pushed us into an object
	// that is not in the previous cache contents from the last timestep, as will happen if we
	// are pushed into a new AABB overlap. Unhandled this means the next convex sweep gets stuck.
	//
	// Do this by calling the broadphase's setAabb with the moved AABB, this will update the broadphase
	// paircache and the ghostobject's internal paircache at the same time.    /BW

	btVector3 minAabb, maxAabb;
	btPairCachingGhostObject* ghost = getParent()->getParent()->getPhysics()->getGhost();

	if (!ghost)
		return false;

	btConvexShape* convex_shape = static_cast<btConvexShape*>(ghost->getCollisionShape());
	btDiscreteDynamicsWorld* collisionWorld = CCoreEngine::Instance().GetPhysicsManager().getWorld();
	convex_shape->getAabb(ghost->getWorldTransform(), minAabb, maxAabb);
	collisionWorld->getBroadphase()->setAabb(ghost->getBroadphaseHandle(),
		minAabb,
		maxAabb,
		collisionWorld->getDispatcher());

	bool penetration = false;

	collisionWorld->getDispatcher()->dispatchAllCollisionPairs(ghost->getOverlappingPairCache(), collisionWorld->getDispatchInfo(), collisionWorld->getDispatcher());

	btVector3 current_positon(ghost->getWorldTransform().getOrigin());

	btScalar maxPen = btScalar(0.0);
	for (int i = 0; i < ghost->getOverlappingPairCache()->getNumOverlappingPairs(); i++)
	{
		btManifoldArray manifoldArray;
		manifoldArray.resize(0);

		btBroadphasePair* collisionPair = &ghost->getOverlappingPairCache()->getOverlappingPairArray()[i];

		btCollisionObject* obj0 = static_cast<btCollisionObject*>(collisionPair->m_pProxy0->m_clientObject);
		btCollisionObject* obj1 = static_cast<btCollisionObject*>(collisionPair->m_pProxy1->m_clientObject);

		if ( (obj0 == obj1) || (obj0 && !obj0->hasContactResponse()) || (obj1 && !obj1->hasContactResponse()) ||
			((obj0->getCollisionFlags() & EntityBtPhysics::EdvCollisionFlags::CF_TRIGGER) != 0) || ((obj1->getCollisionFlags() & EntityBtPhysics::EdvCollisionFlags::CF_TRIGGER) != 0)
			)
			continue;

		//std::cout << "RECOVER1: " << static_cast<GameEntity*>(obj0->getUserPointer())->getGraphics()->getNode()->getName() << std::endl;
		//std::cout << "RECOVER2: " << static_cast<GameEntity*>(obj1->getUserPointer())->getGraphics()->getNode()->getName() << std::endl;

		if (collisionPair->m_algorithm)
			collisionPair->m_algorithm->getAllContactManifolds(manifoldArray);


		for (int j = 0; j < manifoldArray.size(); j++)
		{
			btPersistentManifold* manifold = manifoldArray[j];
			btScalar directionSign = manifold->getBody0() == ghost ? btScalar(-1.0) : btScalar(1.0);
			for (int p = 0; p < manifold->getNumContacts(); p++)
			{
				const btManifoldPoint&pt = manifold->getContactPoint(p);

				btScalar dist = pt.getDistance();

				if (dist < 0.f && abs(dist) > 0.01f)
				{
					if (dist < maxPen)
					{
						maxPen = dist;

					}
					current_positon += pt.m_normalWorldOnB * directionSign * dist * a_recover_fraction;
					penetration = true;
				}
				else
				{
				}
			}
		}
	}

	if (penetration)
	{
		setPosition(current_positon);
	}
	
	return penetration;
}

bool KinematicController::hasMovement()
{
	return !(m_linear_velocity.isZero());
}

void KinematicController::useGravity(bool a_use)
{
	m_use_gravity = a_use;
}

edv::Vector3f KinematicController::getVelocity()
{
	return m_linear_velocity;
}

void KinematicController::resetVelocity()
{
	m_linear_velocity.setZero();
}