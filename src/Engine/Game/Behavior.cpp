#include "Behavior.h"

Behavior::Behavior() :
	m_paused{ false }
{
	
}

Behavior::~Behavior()
{
	
}

void Behavior::init()
{
	m_paused = false;

	initImpl();
}

void Behavior::stop()
{
	m_paused = false;

	stopImpl();
}

void Behavior::update(f64 dt)
{
	updateImpl(dt);
}

void Behavior::pause()
{
	if (!paused()) 
	{ 
		m_paused = true;

		pauseImpl(); 
	}
}

void Behavior::resume()
{
	if (paused())
	{ 
		m_paused = false; 

		resumeImpl(); 
	}
}

bool Behavior::paused() const
{
	return m_paused;
}
