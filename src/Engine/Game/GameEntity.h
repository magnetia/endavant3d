#ifndef EDV_GAME_ENTITY_H_
#define EDV_GAME_ENTITY_H_

#include <memory>
#include <map>
#include <list>
#include "Core/CBasicTypes.h"
#include "Utils/IdGenerator.h"
#include "Behavior.h"
#include "EntityOgreGraphics.h"
#include "EntityBtPhysics.h"
#include "Actor.h"

class GameEntity
{
public:
	typedef IdGenerator<Behavior::Id> BehaviorIdGen;

	GameEntity();
	GameEntity(	const EntityOgreGraphics::Config& a_graphicsCfg, 
				const EntityBtPhysics::Config& a_physicsCfg,
				Actor* a_actor = nullptr);
	GameEntity( const EntityOgreGraphics::Config& a_graphicsCfg);
	~GameEntity();

	void reset();
	void reset(	const EntityOgreGraphics::Config& a_graphicsCfg, 
				const EntityBtPhysics::Config& a_physicsCfg,
				Actor* a_actor = nullptr);
	void reset( const EntityOgreGraphics::Config& a_graphicsCfg);

	void init();
	void update(f64 dt);
	void stop();
	void pause();
	void resume();

	void setEnabled(bool a_status);
	bool isEnabled() const;

	edv::Vector3f		getWorldPosition();
	Ogre::Quaternion	getWorldOrientation();
	Ogre::Vector2		getScreenPosition();
	edv::Vector3f		getScale() const;
	edv::Vector3f		getHalfSize() const;
	edv::Vector3f		getScaledHalfSize() const;
	void				setScale(const edv::Vector3f& a_scale);
	void				setWorldPosition(const edv::Vector3f& a_position);
	void				setWorldOrientation(const Ogre::Quaternion& a_orientation);

	bool good() const;
	GameEntity* clone(	const std::string& a_newName, 
						const Ogre::Vector3& a_newPos = Ogre::Vector3::ZERO,
						const Ogre::Quaternion& a_newRot = Ogre::Quaternion::IDENTITY) const;

	Behavior::Id addBehavior(Behavior* a_behavior);
	Behavior*	 getBehavior(Behavior::Id a_id);
	void removeBehavior(Behavior::Id a_id);
	void removeBehavior(Behavior* a_behavior);
	void removeAllBehaviors();
	u32 getNumBehaviors() const;

	f32 distanceToEntity(GameEntity* a_target);
	f32 angleToEntity(GameEntity* a_target);
	bool canSeeEntity(GameEntity* a_target);
	std::string getName() const;

	EntityOgreGraphics* getGraphics();
	EntityBtPhysics*	getPhysics();
	Actor*				getActor();

private:
	typedef std::unique_ptr<EntityOgreGraphics>		GraphicsPtr;
	typedef std::unique_ptr<EntityBtPhysics>		PhysicsPtr;
	typedef std::unique_ptr<Actor>					ActorPtr;
	typedef std::unique_ptr<Behavior>				BehaviorPtr;
	typedef std::map<Behavior::Id, BehaviorPtr>		Behaviors;
	typedef std::list<Behavior::Id>					BehaviorsToRemove;

	void clone(const std::string& a_newName, const GameEntity* a_entity);

	GraphicsPtr			m_graphics;
	PhysicsPtr			m_physics;
	ActorPtr			m_actor;
	Behaviors			m_behaviors;
	BehaviorIdGen		m_idGenerator;
	Behavior::Id		m_currentId;
	BehaviorsToRemove	m_behaviorsToRemove;
	bool				m_updating;
	bool				m_enabled;
};

#endif
