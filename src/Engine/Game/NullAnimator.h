#ifndef EDV_GAME_NULL_ANIMATOR_H_
#define EDV_GAME_NULL_ANIMATOR_H_

#include "Game/TimedBehavior.h"
#include "OGRE/Ogre.h"

// #debug:
#include "Core/CCoreEngine.h"
#include "Core/CLogManager.h"

class NullAnimator : public TimedBehavior
{
public:
	NullAnimator(f64 a_duration);
	Behavior* clone() override;

private:
	void initImpl() override;
	void stopImpl() override;
	void updateImpl(f64 dt);
};

#endif
