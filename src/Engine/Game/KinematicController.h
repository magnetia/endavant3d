#ifndef EDV_GAME_KINEMATIC_CONTROLLER_H_
#define EDV_GAME_KINEMATIC_CONTROLLER_H_

#include <btBulletCollisionCommon.h>
#include "ActorMovement.h"

class KinematicController : public ActorMovement
{
public:
	KinematicController();
	void init() override;
	void applyMove(const edv::Vector3f& a_vector) override;
	void applyForce(const edv::Vector3f& a_vector) override;
	void applyImpulse(const edv::Vector3f& a_vector) override;
	void stepMove(f64 dt, GameCollisions& a_collisionInfo) override;
	void setGravity(const edv::Vector3f& a_vector) override;
	void setRestitution(f32 a_restitution) override;
	void setSliding(f32 a_sliding) override;
	void setFriction(f32 a_friction) override;
	bool hasMovement() override;
	void useGravity(bool a_use) override;
	bool onGround() const;
	bool onCeiling() const;
	edv::Vector3f getVelocity() override;
	void resetVelocity() override;

protected:
	
	virtual void setPosition(const edv::Vector3f& a_position);
	virtual bool checkPenetration();
	virtual bool internalStep(GameCollisions& a_collisionInfo, edv::Vector3f& a_normal_hit, f64 dt, const edv::Vector3f& a_linear_velocity);
	virtual void postStep();
	virtual void checkPosition(GameCollisions& a_collisionInfo);
	virtual bool checkOnGround(GameCollisions& a_collisionInfo);
	virtual bool checkOnCeiling(GameCollisions& a_collisionInfo);
	virtual bool checkOnLeft(GameCollisions& a_collisionInfo);
	virtual bool checkOnRight(GameCollisions& a_collisionInfo);
	virtual void applyGravity(const edv::Vector3f& a_velocity);
	virtual edv::Vector3f applyFriction(const edv::Vector3f& a_velocity);
	virtual void updateVelocityOnCollision(edv::Vector3f& a_velocity, const edv::Vector3f& a_normal_hit, f32 a_tangentFactor, f32 a_normalFactor);

	//-- btKinematicCharacterController...
	btVector3 computeReflectionDirection(const btVector3& a_direction, const btVector3& a_normal);
	btVector3 parallelComponent(const btVector3& a_direction, const btVector3& a_normal);
	btVector3 perpindicularComponent(const btVector3& a_direction, const btVector3& a_normal);
	btVector3 updateTargetPositionBasedOnCollision(const btVector3& a_from, const btVector3& a_to, const btVector3& a_normal_hit, btScalar a_tangentMag = btScalar(0.0), btScalar a_normalMag = btScalar(1.0));
	bool recoverFromPenetration(btScalar a_recover_fraction = 0.01f);
	//--//

	edv::Vector3f	m_current_position;
	edv::Vector3f	m_target_position;
	edv::Vector3f	m_gravity_acceleration;

	edv::Vector3f	m_linear_velocity;
	edv::Vector3f	m_total_acceleartion;
	f32				m_restitution;
	f32				m_sliding;
	f32				m_friction;
	bool			m_use_gravity;
	bool			m_on_ground;
	bool			m_on_ceiling;
	bool			m_on_left;
	bool			m_on_right;
	bool			m_movement;
	bool			m_impulse;
	f32				m_collisionMargin;
	f32				m_checkMargin;
};

#endif
