#include "EntityOgreGraphics.h"
#include "Core/CCoreEngine.h"
#include "Renderer/CRenderManager.h"
#include "Physics/COgreMotionState.h"
#include "Physics/COgreKinematicMotionState.h"
#include "Externals/MeshStrider/EdvMeshStrider.h"

EntityOgreGraphics::EntityOgreGraphics() :
	m_node{ nullptr },
	m_entity{nullptr},
	m_load{ false }
{

}

EntityOgreGraphics::EntityOgreGraphics(const Config& a_config) :
	m_node{ nullptr },
	m_entity{ nullptr },
	m_load{ a_config.load }
{
	init(a_config);
}

EntityOgreGraphics::~EntityOgreGraphics()
{
	clear();
}

void EntityOgreGraphics::init(const Config& a_config)
{
	auto sceneMgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();

	clear();

	if (!m_node && m_load )
	{
		Ogre::SceneNode* parentNode = a_config.parent ? a_config.parent : sceneMgr->getRootSceneNode();
		m_node = parentNode->createChildSceneNode(a_config.nodeName, a_config.initialPos, a_config.initialRot);
		
		// Create entity if path exists
		if (!a_config.filePath.empty())
		{
			m_entity = sceneMgr->createEntity(a_config.nodeName, a_config.filePath);
			m_entity->getMesh()->buildEdgeList();

			if (!a_config.material.empty())
			{
				m_entity->setMaterialName(a_config.material);
			}

			m_node->attachObject(m_entity);

			// configure custom materials
			for (auto* materialCfg : a_config.customMaterials)
			{
				m_customMaterials.emplace_back(materialCfg);
				materialCfg->init(m_entity);
			}

			// configure animations
			if (Ogre::AnimationStateSet* set = m_entity->getAllAnimationStates())
			{
				Ogre::AnimationStateIterator it = set->getAnimationStateIterator();
				Ogre::AnimationState *mAnimState;

				while (it.hasMoreElements())
				{
					mAnimState = it.getNext();
					const std::string& animName = mAnimState->getAnimationName();

					for (auto it_a = a_config.animations.begin(); it_a != a_config.animations.end(); it_a++)
					{
						const AnimationConfig& animConfig = (*it_a);

						if (animName == animConfig.name)
						{
							mAnimState->setEnabled(animConfig.enabled);
							mAnimState->setLoop(animConfig.loop);
							m_animations[animName] = { mAnimState, animConfig.speed };
						}
					}
				}
			}
		}

		// configure particle systems
		for (auto& particleConfig : a_config.particles)
		{
			const std::string baseName = a_config.nodeName + particleConfig.name;
			auto it = m_particles.find(particleConfig.name);

			if (it == m_particles.end())
			{
				addParticleSystem(baseName, particleConfig);
			}
			else
			{
				throw std::runtime_error{ "Duplicated particle system name: " + particleConfig.name };
			}
		}

		// configure lights
		for (auto& lightConfig : a_config.lights)
		{
			const std::string baseName = a_config.nodeName + lightConfig.name;
			auto it = m_lights.find(lightConfig.name);

			if (it == m_lights.end())
			{
				addLight(lightConfig.type,  baseName, lightConfig.specular, lightConfig.diffuse, lightConfig.direction);
			}
			else
			{
				throw std::runtime_error{ "Duplicated light name: " + lightConfig.name };
			}
		}
	}
	else
	{
		m_node = CCoreEngine::Instance().GetRenderManager().GetSceneManager()->getSceneNode(a_config.nodeName);
	}

	if (!m_node)
	{
		throw std::runtime_error("error creating EntityOgreGraphics for node " + a_config.nodeName);
	}
	else
	{
		m_node->setScale(a_config.scale);
	}
}

void EntityOgreGraphics::update(f64 dt)
{
	for (const auto& materialPtr : m_customMaterials)
	{
		materialPtr->update(dt);
	}

	if (m_animationBlending.get())
	{
		m_animationBlending->addTime(static_cast<f32>(dt));
		
		if (m_animationBlending->isCompleted())
		{
			stopBlendAnimations();
		}
	}
	else
	{
		for (const auto& pair : m_animations)
		{
			const Animation& anim = pair.second;

			if (anim.state->getEnabled())
			{
				anim.state->addTime(static_cast<f32>(dt)* anim.speed);
			}
		}
	}

	for (const auto& pair : m_particles)
	{
		const Particle& particle = pair.second;

		if (!particle.attachToParent)
		{
			const Ogre::Vector3 entityPos = m_node->getPosition();
			const Ogre::Vector3 position = particle.bone ? entityPos + (particle.bone->getPosition() * m_node->getScale()) : entityPos;
			const Ogre::Quaternion orientation = particle.bone ? m_node->getOrientation() * particle.bone->getOrientation() : Ogre::Quaternion::IDENTITY;
			if (particle.bone)
			{
				particle.node->setOrientation(orientation);
			}

			if (particle.positionSync)
			{
				particle.node->setPosition(position + particle.translation);
			}
			
		}		
	}
}

void EntityOgreGraphics::clear()
{
	if (m_node)
	{
		if (m_load)
		{
			m_node->removeAndDestroyAllChildren();
			m_node->detachAllObjects();
			m_node->getParent()->removeChild(m_node);
			auto* sceneMgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();

			// destroy main entity
			if (m_entity)
			{
				sceneMgr->destroyEntity(m_entity);
			}

			// remove custom materials
			removeAllCustomMaterials();

			// destroy animations
			m_animations.clear();

			// destroy particles
			removeAllParticleSystems();

			// destroy lights
			removeAllLights();

			// destroy node
			sceneMgr->destroySceneNode(m_node);
		}

		m_entity = nullptr;
		m_node = nullptr;
	}
}

void EntityOgreGraphics::stopAllAnimations()
{
	for (auto& pair : m_animations)
	{
		pair.second.state->setEnabled(false);
	}
}

void EntityOgreGraphics::removeAllCustomMaterials()
{
	for (auto& materialPtr : m_customMaterials)
	{
		materialPtr->remove();
	}

	m_customMaterials.clear();
}

Ogre::Vector3 EntityOgreGraphics::getScale() const
{
	Ogre::Vector3 scale;

	if (m_node)
	{
		scale = m_node->getScale();
	}

	return scale;
}

void EntityOgreGraphics::setScale(const Ogre::Vector3& a_scale)
{
	if (m_node)
	{
		m_node->setScale(a_scale);
	}
}

EntityOgreGraphics* EntityOgreGraphics::clone(const std::string& a_newName, const Ogre::Vector3& a_newPos, 
	const Ogre::Quaternion& a_newRot) const
{
	EntityOgreGraphics* graphics = nullptr;

	if (m_node)
	{
		if (Ogre::SceneNode* const parent = m_node->getParentSceneNode())
		{
			Ogre::SceneNode* const node = parent->createChildSceneNode(a_newName, a_newPos, a_newRot);

			graphics = new EntityOgreGraphics;
			graphics->m_load = m_load;
			graphics->m_node = node;

			auto it = m_node->getAttachedObjectIterator();
			Ogre::Entity* newEntity = nullptr;
			// only clones first entity
			while (it.hasMoreElements() && !newEntity)
			{
				Ogre::Entity* entity = static_cast<Ogre::Entity*>(it.getNext());
				newEntity = entity->clone(a_newName);
				node->attachObject(newEntity);

				if (Ogre::AnimationStateSet* set = newEntity->getAllAnimationStates())
				{
					for (auto& pair : m_animations)
					{
						const Animation& anim = pair.second;
						const std::string& animName = anim.state->getAnimationName();
						Ogre::AnimationState* newAnim = set->getAnimationState(animName);

						if (newAnim)
						{
							newAnim->setEnabled(entity->getAnimationState(animName)->getEnabled());
							graphics->m_animations[animName] = { newAnim, anim.speed };
						}
					}
				}

				// #todo: clone custom materials?
				// #todo: clone particle systems

				graphics->m_entity = newEntity;
			}
		}
	}

	return graphics;
}

Ogre::SceneNode* EntityOgreGraphics::getNode() const
{
	return m_node;
}

Ogre::Entity* EntityOgreGraphics::getMainEntity() const
{
	return m_entity;
}

Ogre::AnimationState* EntityOgreGraphics::getAnimation(const std::string& a_animationName)
{
	Ogre::AnimationState* anim = nullptr;
	auto it = m_animations.find(a_animationName);

	if (it != m_animations.end())
	{
		anim = it->second.state;
	}

	return anim;
}

void EntityOgreGraphics::setAnimationSpeed(const std::string& a_animationName, f32 a_speed)
{
	Ogre::AnimationState* anim = nullptr;
	auto it = m_animations.find(a_animationName);

	if (it != m_animations.end())
	{
		it->second.speed = a_speed;
	}
}

f32 EntityOgreGraphics::getAnimationSpeed(const std::string& a_animationName) const
{
	f32 speed{ 0.f };
	Ogre::AnimationState* anim = nullptr;
	auto it = m_animations.find(a_animationName);

	if (it != m_animations.end())
	{
		speed = it->second.speed;
	}
	return speed;
}

void EntityOgreGraphics::blendAnimations(const std::string& a_animationSourceName, const std::string& a_animationTargetName, AnimationBlender::BlendingTransition a_transition, f32 a_duration)
{
	auto source = getAnimation(a_animationSourceName);
	auto target = getAnimation(a_animationTargetName);
	if (source && target && m_entity)
	{
		m_animationBlending.reset(new AnimationBlender{ m_entity });
		m_animationBlending->init(a_animationSourceName);
		m_animationBlending->blend(a_animationTargetName, a_transition, a_duration);
	}
}

void EntityOgreGraphics::stopBlendAnimations()
{
	m_animationBlending.reset();
}

btVector3 EntityOgreGraphics::getPosition() const
{
	btVector3 position;

	if (m_node)
	{
		position = COgreMotionState::ogreVector3toBtVector3(m_node->getPosition());
	}

	return position;
}

btQuaternion EntityOgreGraphics::getOrientation() const
{
	btQuaternion orientation;

	if (m_node)
	{
		orientation = COgreMotionState::ogreQuaternionToBtQuaternion(m_node->getOrientation());
	}

	return orientation;
}

btVector3 EntityOgreGraphics::getWorldPosition() const
{
	btVector3 position;

	if (m_node)
	{
		position = COgreMotionState::ogreVector3toBtVector3(m_node->_getDerivedPosition());
	}

	return position;
}

btQuaternion EntityOgreGraphics::getWorldOrientation() const
{
	btQuaternion orientation;

	if (m_node)
	{
		orientation = COgreMotionState::ogreQuaternionToBtQuaternion(m_node->_getDerivedOrientation());
	}

	return orientation;
}

std::vector<btVector3> EntityOgreGraphics::getVertices() const
{
	std::vector<btVector3> vertices;

	if (m_entity)
	{
		std::size_t vertexCount, indexCount;
		std::vector<Ogre::Vector3> verts;
		std::vector<u32> indexes;

		CCoreEngine::Instance().GetRenderManager().getMeshInformation(m_entity->getMesh().get(), vertexCount, verts, indexCount, indexes);

		for (u32 idx = 0; idx < vertexCount && idx < verts.size(); idx++)
		{
			vertices.push_back(COgreMotionState::ogreVector3toBtVector3(verts[idx]));
		}
	}

	return vertices;
}

btVector3 EntityOgreGraphics::getHalfSize() const
{
	btVector3 value;

	if (m_entity)
	{
		value = COgreMotionState::ogreVector3toBtVector3(m_entity->getBoundingBox().getHalfSize());
	}

	return value;
}

btVector3 EntityOgreGraphics::btGetScale() const
{
	btVector3 scale;

	if (m_node)
	{
		scale = COgreMotionState::ogreVector3toBtVector3(m_node->getScale());
	}

	return scale;
}

btStridingMeshInterface* EntityOgreGraphics::createStridingMeshInterface()
{
	btStridingMeshInterface* strider = nullptr;

	if (m_entity)
	{
		Ogre::MeshPtr mesh = m_entity->getMesh();
		strider = new EdvMeshStrider(mesh.get());
	}

	return strider;
}

btMotionState* EntityOgreGraphics::createMotionState(const btVector3& a_translation, const btQuaternion& a_rotation, bool a_kinematic)
{
	btMotionState* returnValue = (!a_kinematic) ? new COgreMotionState(m_node, a_translation, a_rotation) : new COgreKinematicMotionState(m_node, a_translation, a_rotation);
	return returnValue;
}

BtWorldObject* EntityOgreGraphics::createWorldObject(void* a_param)
{
	return new EntityOgreGraphics{ *static_cast<EntityOgreGraphics::Config*>(a_param) };
}

std::string EntityOgreGraphics::getObjectId() const
{
	std::string id;

	if (m_node)
	{
		id = m_node->getName();
	}

	return id;
}

Ogre::SceneNode* EntityOgreGraphics::getParticleSystemNode(const std::string& a_particleId) const
{
	Ogre::SceneNode* node = nullptr;
	auto it = m_particles.find(a_particleId);

	if (it != m_particles.end())
	{
		node = it->second.node;
	}

	return node;
}

Ogre::ParticleSystem* EntityOgreGraphics::getParticleSystem(const std::string& a_particleId) const
{
	Ogre::ParticleSystem* system = nullptr;
	auto it = m_particles.find(a_particleId);

	if (it != m_particles.end())
	{
		system = it->second.system;
	}

	return system;
}

Ogre::Bone* EntityOgreGraphics::getBone(Ogre::Entity* a_entity, const std::string& a_boneName)
{
	Ogre::Bone* bone = nullptr;

	if (!a_boneName.empty() && a_entity)
	{
		if (Ogre::Skeleton* skel = a_entity->getSkeleton())
		{
			Ogre::Skeleton::BoneIterator itb = skel->getBoneIterator();

			while (itb.hasMoreElements() && !bone)
			{
				Ogre::Bone* const currBone = static_cast<Ogre::Bone*>(itb.getNext());
				const std::string boneName = currBone->getName();

				if (boneName == a_boneName)
				{
					bone = currBone;
				}
			}
		}
	}

	return bone;
}

Ogre::Vector3 EntityOgreGraphics::getBoneWorldPosition(Ogre::Entity* a_entity, Ogre::Bone* a_bone)
{
	if (!a_entity || !a_bone)
		return Ogre::Vector3::ZERO;

	Ogre::Vector3 worldPosition = a_bone->_getDerivedPosition();

	//multiply with the parent derived transformation
	Ogre::Node* pParentNode = a_entity->getParentNode();
	Ogre::SceneNode* pSceneNode = a_entity->getParentSceneNode();
	while (pParentNode != nullptr)
	{
		//process the current i_Node
		if (pParentNode != pSceneNode)
		{
			//this is a tag point (a connection point between 2 entities). which means it has a parent i_Node to be processed
			worldPosition = pParentNode->_getFullTransform() * worldPosition;
			pParentNode = pParentNode->getParent();
		}
		else
		{
			//this is the scene i_Node meaning this is the last i_Node to process
			worldPosition = pParentNode->_getFullTransform() * worldPosition;
			break;
		}
	}

	return worldPosition;
}


EntityOgreGraphics::LightType EntityOgreGraphics::stringToLightType(const std::string& a_lightTypeStr)
{
	std::string typeStr;
	std::transform(a_lightTypeStr.begin(), a_lightTypeStr.end(), std::back_inserter(typeStr), ::tolower);

	LightType type = LIGHT_TYPE_NONE;

	if (typeStr == "point")
	{
		type = LIGHT_TYPE_POINT;
	}
	else if (typeStr == "directional")
	{
		type = LIGHT_TYPE_DIRECTIONAL;
	}
	else if (typeStr == "spotlight")
	{
		type = LIGHT_TYPE_SPOTLIGHT;
	}

	return type;
}

void EntityOgreGraphics::addParticleSystem(const std::string& a_name, const ParticleConfig& a_particleConfig)
{
	auto sceneMgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();

	Ogre::Vector3 position = a_particleConfig.attachToParent ? a_particleConfig.translation.to_OgreVector3() : m_node->getPosition() + a_particleConfig.translation.to_OgreVector3();
	Ogre::Quaternion orientation = a_particleConfig.attachToParent ? Ogre::Quaternion::IDENTITY : m_node->getOrientation();

	// try to get bone if specified
	Ogre::Bone* bone = getBone(m_entity, a_particleConfig.bone);

	if (bone)
	{
		position = m_node->getPosition() + (bone->getPosition() * m_node->getScale()) + a_particleConfig.translation.to_OgreVector3();
		orientation = m_node->getOrientation() * bone->getOrientation();
	}

	Ogre::ParticleSystem* const system = sceneMgr->createParticleSystem(a_name + "system", a_particleConfig.system);
	Ogre::SceneNode* const parent = a_particleConfig.attachToParent ? m_node : static_cast<Ogre::SceneNode*>(m_node->getParent());
	Ogre::SceneNode* const node = parent->createChildSceneNode(a_name + "node");

	node->setPosition(position);
	node->setOrientation(orientation);
	node->attachObject(system);
	node->setScale(a_particleConfig.scale.to_OgreVector3());

	m_particles.emplace(Particles::value_type{ a_name, Particle{ node, system, getBone(m_entity, a_particleConfig.bone), a_particleConfig.translation.to_OgreVector3(), a_particleConfig.attachToParent, a_particleConfig.positionSync } });
}

void EntityOgreGraphics::removeParticleSystem(const std::string& a_name)
{
	auto sceneMgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();
	auto it = m_particles.find(a_name);
 
	if (it != m_particles.end())
	{
		removeParticleSystem(it->second);
		m_particles.erase(it);
	}
}

void EntityOgreGraphics::removeAllParticleSystems()
{
	for (auto& particleCfg : m_particles)
	{
		removeParticleSystem(particleCfg.second);
	}

	m_particles.clear();
}

void EntityOgreGraphics::addLight(LightType a_type, const std::string& a_name, const Ogre::ColourValue& a_diffuse, const Ogre::ColourValue& a_specular,
	const edv::Vector3f& a_direction)
{
	auto sceneMgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();

	Ogre::Light* light = sceneMgr->createLight(a_name);
	light->setType(static_cast<Ogre::Light::LightTypes>(a_type));
	light->setDiffuseColour(a_diffuse);
	light->setSpecularColour(a_specular);
	light->setDirection(a_direction.to_OgreVector3());

	m_node->attachObject(light);
}

void EntityOgreGraphics::removeLight(const std::string& a_name)
{
	auto it = m_lights.find(a_name);

	if (it != m_lights.end())
	{
		removeLight(it->second);
		m_lights.erase(it);
	}
}

void EntityOgreGraphics::removeAllLights()
{
	for (auto& lightCfg : m_lights)
	{
		removeLight(lightCfg.second);
	}

	m_lights.clear();
}

void EntityOgreGraphics::removeParticleSystem(Particle& a_particle)
{
	auto sceneMgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();

	sceneMgr->destroyParticleSystem(a_particle.system);

	if (!a_particle.attachToParent)
	{
		sceneMgr->destroySceneNode(a_particle.node);
	}

	a_particle.node = nullptr;
}

void EntityOgreGraphics::removeLight(Ogre::Light* a_light)
{
	if (a_light)
	{
		auto sceneMgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();

		m_node->detachObject(a_light);
		sceneMgr->destroyLight(a_light);
	}
}
