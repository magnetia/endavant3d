#ifndef EDV_GAME_TIMED_BEHAVIOR_H_
#define EDV_GAME_TIMED_BEHAVIOR_H_

#include "Behavior.h"

class TimedBehavior : public Behavior
{
public:
	TimedBehavior(f64 a_duration);
	TimedBehavior(const TimedBehavior& a_other);

	virtual bool finished() const final;
	virtual f64 duration() const final;
	virtual f64 elapsed() const final;
	virtual f64 remaining() const final;
	virtual f64 startTime() const final;

protected:
	void initImpl() override;
	void pauseImpl() override;
	void resumeImpl() override;

	f64	m_duration;
	f64 m_missedTime;
	f64	m_startTimestamp;
	f64	m_pauseTimestamp;
};

#endif
