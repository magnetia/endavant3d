#include "GameEntity.h"
#include "GlowAnimator.h"

Behavior* GlowAnimator::Factory::createElement(const Behavior::factory_param_type& a_config)
{
	return new GlowAnimator{
		a_config["duration"],
		a_config["glow_texture"],
		{ a_config["initial_level/x"], a_config["initial_level/y"], a_config["initial_level/z"] },
		{ a_config["final_level/x"], a_config["final_level/y"], a_config["final_level/z"] }
	};
}

GlowAnimator::GlowAnimator(f64 a_duration, const std::string& a_texture, const edv::Vector3f& a_initialLevel, const edv::Vector3f& a_finalLevel) :
TimedBehavior{ a_duration },
m_texture{ a_texture },
m_initialLevel{ a_initialLevel },
m_finalLevel{ a_finalLevel },
m_vector{},
m_timeFactor{ 0. },
m_material{ nullptr },
m_pass{ nullptr },
m_originalMat{}
{

}

GlowAnimator::~GlowAnimator()
{

}

Behavior* GlowAnimator::clone()
{
	return new GlowAnimator{ duration(), m_texture, m_initialLevel, m_finalLevel };
}

void GlowAnimator::initImpl()
{
	TimedBehavior::initImpl();

	auto* node = getParent()->getGraphics()->getNode();
	auto it = node->getAttachedObjectIterator();

	// add only to first material found
	while (it.hasMoreElements() && !m_material)
	{
		Ogre::Entity* const entity = static_cast<Ogre::Entity*>(it.getNext());
		u32 subEntities = entity->getNumSubEntities();

		for (u32 se_idx = 0; se_idx < subEntities && !m_material; se_idx++)
		{
			Ogre::SubEntity* const subEntity = entity->getSubEntity(se_idx);
			m_originalMat = subEntity->getMaterial().get();

			const Ogre::MaterialPtr& materialPtr =
				subEntity->getMaterial()->clone(subEntity->getMaterialName() + node->getName());

			m_material = materialPtr.get();
			Ogre::Technique* technique = m_material->createTechnique();
			technique->setSchemeName("glow");

			const Ogre::ColourValue initialValue{ m_initialLevel.x, m_initialLevel.y, m_initialLevel.z };
			m_pass = technique->createPass();
			m_pass->setAmbient(initialValue);
			m_pass->setDiffuse(initialValue);
			m_pass->setSpecular(0, 0, 0, 1);
			m_pass->setEmissive(0, 0, 0);

			Ogre::TextureUnitState* textureUnit = m_pass->createTextureUnitState();
			textureUnit->setTextureName(m_texture);

			subEntity->setMaterial(materialPtr);
		}
	}

	m_vector = m_finalLevel - m_initialLevel;
	m_timeFactor = m_vector.length() / m_duration;
	m_vector.normalise();
}

void GlowAnimator::stopImpl()
{
	auto it = getParent()->getGraphics()->getNode()->getAttachedObjectIterator();
	bool found = false;

	// reset material properties
	while (it.hasMoreElements() && !found)
	{
		Ogre::Entity* const entity = static_cast<Ogre::Entity*>(it.getNext());
		u32 subEntities = entity->getNumSubEntities();

		for (u32 idx = 0; idx < subEntities && !found; idx++)
		{
			Ogre::SubEntity* const subEntity = entity->getSubEntity(idx);
			const Ogre::String matName = subEntity->getMaterialName();

			subEntity->setMaterialName(m_originalMat->getName());
			Ogre::MaterialManager::getSingleton().remove(matName);

			found = true;
			m_originalMat = nullptr;
			m_material = nullptr;
			m_pass = nullptr;
		}
	}
}

void GlowAnimator::updateImpl(f64 dt)
{
	if (m_material)
	{
		if (finished())
		{
			Ogre::ColourValue finalLevel{ m_finalLevel.x, m_finalLevel.y, m_finalLevel.z };

			m_pass->setAmbient(finalLevel);
			m_pass->setDiffuse(finalLevel);
		}
		else
		{
			f64 phase = fmod(elapsed(), duration());
			edv::Vector3f rel = m_vector * static_cast<f32>(phase * m_timeFactor);

			Ogre::ColourValue ambient = m_pass->getAmbient();
			ambient.r = m_initialLevel.x + rel.x;
			ambient.g = m_initialLevel.y + rel.y;
			ambient.b = m_initialLevel.z + rel.z;
			m_pass->setAmbient(ambient);

			Ogre::ColourValue diffuse = m_pass->getDiffuse();
			diffuse.r = m_initialLevel.x + rel.x;
			diffuse.g = m_initialLevel.y + rel.y;
			diffuse.b = m_initialLevel.z + rel.z;
			m_pass->setDiffuse(diffuse);
		}
	}
}
