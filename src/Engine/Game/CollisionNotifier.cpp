#include "Core/CCoreEngine.h"
#include "Physics/CPhysicsManager.h"
#include "Actor.h"
#include "GameEntity.h"
#include "CollisionNotifier.h"

CollisionNotifier::CollisionNotifier() :
	ActorMovement{}
{

}

CollisionNotifier::~CollisionNotifier()
{

}

void CollisionNotifier::init()
{

}

void CollisionNotifier::applyMove(const edv::Vector3f& a_vector)
{
	// nothing to do...
}

void CollisionNotifier::applyForce(const edv::Vector3f& a_vector)
{
	// nothing to do...
}

void CollisionNotifier::applyImpulse(const edv::Vector3f& a_vector)
{
	// nothing to do...
}

void CollisionNotifier::setGravity(const edv::Vector3f& a_vector)
{
	// nothing to do...
}

void CollisionNotifier::setRestitution(f32 a_restitution)
{
	// nothing to do...
}

void CollisionNotifier::setSliding(f32 a_sliding)
{
	// nothing to do...
}

void CollisionNotifier::setFriction(f32 a_friction)
{
	// nothing to do...
}

void CollisionNotifier::stepMove(f64 dt, GameCollisions& a_collisionInfo)
{
	// nothing to do...
}

bool CollisionNotifier::hasMovement()
{
	return false;
}

void CollisionNotifier::useGravity(bool a_use)
{
	// nothing to do...
}

bool CollisionNotifier::onGround() const
{
	return false;
}

bool CollisionNotifier::onCeiling() const
{
	return false;
}

edv::Vector3f CollisionNotifier::getVelocity()
{
	return edv::Vector3f{0.f, 0.f, 0.f};
}

void CollisionNotifier::resetVelocity()
{
	// nothing to do...
}

