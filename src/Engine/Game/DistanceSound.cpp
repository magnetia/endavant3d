#include "Core/CCoreEngine.h"
#include "GameManager.h"
#include "GameEntity.h"
#include "DistanceSound.h"

Behavior* DistanceSound::Factory::createElement(const Behavior::factory_param_type& a_config)
{
	return new DistanceSound{ a_config["sound"], a_config["range"] };
}

DistanceSound::DistanceSound(const std::string& a_soundId, f32 a_range) :
	m_soundId{ a_soundId },
	m_range{ a_range },
	m_channelId{ CSoundManager::UNASSIGNED_CHANNEL_ID },
	m_defaultVolume{ 0.f },
	m_active{ false }
{
	CSoundManager& soundManager{ CCoreEngine::Instance().GetSoundManager() };
	CSoundManager::tOptions soundOptions{ soundManager.getSoundOptions(m_soundId) };

	if (soundOptions["loops"].as<s32>() != -1)
	{
		throw std::runtime_error{ "sound " + m_soundId + " must loop. Check sound configuration and put loops=-1" };
	}

	if (soundOptions.path_exists("fade_in_ms") || soundOptions.path_exists("fade_out_ms"))
	{
		throw std::runtime_error{ "Incompatible option: fade in/out" };
	}

	soundManager.preloadSound(m_soundId);
}

DistanceSound::~DistanceSound()
{
	CCoreEngine::Instance().GetSoundManager().releaseSound(m_soundId);
}

Behavior* DistanceSound::clone()
{
	return new DistanceSound{ m_soundId, m_range };
}

bool DistanceSound::isActive() const
{
	return m_active;
}

void DistanceSound::initImpl()
{
	auto& soundMgr = CCoreEngine::Instance().GetSoundManager();
	m_channelId = soundMgr.playSound(m_soundId);
	m_defaultVolume = static_cast<f32>(soundMgr.getSoundVolume(m_channelId));
	soundMgr.setSoundVolume(m_channelId, 0);
	m_active = false;
}

void DistanceSound::stopImpl()
{
	if (m_channelId != CSoundManager::UNASSIGNED_CHANNEL_ID)
	{
		CCoreEngine::Instance().GetSoundManager().stopSound(m_channelId);
		m_channelId = CSoundManager::UNASSIGNED_CHANNEL_ID;
		m_defaultVolume = static_cast<f32>(CSoundManager::MAX_VOLUME);
		m_active = false;
	}
}

void DistanceSound::updateImpl(f64 dt)
{
	if (m_channelId != CSoundManager::UNASSIGNED_CHANNEL_ID)
	{
		if (GameEntity* const player = CCoreEngine::Instance().GetGameManager().getPlayer())
		{
			auto& soundMgr = CCoreEngine::Instance().GetSoundManager();
			const f32 distance = getParent()->distanceToEntity(player);

			if (distance <= m_range)
			{
				m_active = true;
				s32 volume = static_cast<s32>(m_defaultVolume * (1.f - (distance / m_range)));
				soundMgr.setSoundVolume(m_channelId, volume);
			}
			else if (m_active)
			{
				m_active = false;
				soundMgr.setSoundVolume(m_channelId, 0);
			}
		}
	}
}

void DistanceSound::pauseImpl()
{
	if (m_channelId != CSoundManager::UNASSIGNED_CHANNEL_ID)
	{
		CCoreEngine::Instance().GetSoundManager().pauseSound(m_channelId);
	}
}

void DistanceSound::resumeImpl()
{
	if (m_channelId != CSoundManager::UNASSIGNED_CHANNEL_ID)
	{
		CCoreEngine::Instance().GetSoundManager().resumeSound(m_channelId);
	}
}
