#ifndef EDV_GUI_ROTATION_ANIMATOR_H_
#define EDV_GUI_ROTATION_ANIMATOR_H_

#include "Game/TimedBehavior.h"
#include "OGRE/Ogre.h"
#include "Physics/MagneticProperty.h"

class MagneticAnimator : public TimedBehavior
{
public:
	class Factory : public Behavior::factory_type
	{
	public:
		Behavior* createElement(const Behavior::factory_param_type& a_config) override;
	};

	struct Config
	{
		MagneticProperty::eMagneticCharge initialMagneticCharge;
		f64	neutralDuration;
		f64	negativeDuration;
		f64	positiveDuration;
		bool loop;
		Config(MagneticProperty::eMagneticCharge a_initialMagneticCharge, f64 a_neutralDuration, f64 a_negativeDuration, f64 a_positiveDuration, bool a_loop) :
			initialMagneticCharge{ a_initialMagneticCharge }, neutralDuration{ a_neutralDuration }, negativeDuration{ a_negativeDuration }, 
			positiveDuration{ a_positiveDuration }, loop{ a_loop }
		{}
	};
	MagneticAnimator(const Config& a_config);

	Behavior* clone() override;

private:
	void initImpl() override;
	void stopImpl() override;
	void updateImpl(f64 dt) override;

	void setMagneticCharge(MagneticProperty::eMagneticCharge a_charge);
	void setDurationByCharge(MagneticProperty::eMagneticCharge a_charge);
	void reset();

	const u8 m_initialCharge;
	u8 m_currentCharge;
	std::array<f64, MagneticProperty::MAGNET_TYPE_TOTAL> m_duration_by_charge;
	bool	m_loop;
};

#endif
