#ifndef EDV_GAME_CUSTOM_MATERIAL_H_
#define EDV_GAME_CUSTOM_MATERIAL_H_

#include <OGRE/Ogre.h>
#include "Core/CBasicTypes.h"
#include "Utils/any.h"
#include "Utils/tree.h"
#include "Utils/Factory.h"

class CustomMaterial
{
public:
	typedef edv::tree<edv::any>							factory_param_type;
	typedef Factory<CustomMaterial, factory_param_type>	factory_type;

	CustomMaterial();
	virtual ~CustomMaterial();

	virtual void init(Ogre::Entity* a_entity = nullptr) = 0;
	virtual void update(f64 dt) = 0;
	virtual void remove() = 0;

protected:
	virtual void reset();

	Ogre::Entity*		m_entity;
	Ogre::Material*		m_originalMat;
	Ogre::Material*		m_material;
	Ogre::Pass*			m_pass;
};

#endif
