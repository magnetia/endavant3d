#ifndef EDV_GAME_ACTOR_MOVEMENT_H_
#define EDV_GAME_ACTOR_MOVEMENT_H_

#include <vector>
#include "Core/CBasicTypes.h"
#include "Utils/edvVector3.h"
#include "Utils/ChildOf.h"
#include "CollisionInfo.h"

class Actor;
class Collisions;

class GameEntity;

class ActorMovement : public ChildOf<Actor>
{
public:
	struct sMovementConfig
	{
		edv::Vector3f		gravity;
		f32					restitution;
		f32					sliding;
		f32					friction;

		sMovementConfig(const edv::Vector3f& a_gravity = { 0.f, -9.8f, 0.f }, f32 a_restitution = 0.1f, f32 a_sliding = 0.1f, f32 a_friction = 0.f) :
			gravity{ a_gravity }, restitution{ a_restitution }, sliding{ a_sliding }, friction{ a_friction }
		{ }
	};

	virtual ~ActorMovement() { }

	virtual void init() = 0;
	virtual void applyMove(const edv::Vector3f& a_vector) = 0;
	virtual void applyForce(const edv::Vector3f& a_vector) = 0;
	virtual void applyImpulse(const edv::Vector3f& a_vector) = 0;
	virtual void setGravity(const edv::Vector3f& a_vector) = 0;
	virtual void setRestitution(f32 a_restitution) = 0;
	virtual void setSliding(f32 a_sliding) = 0;
	virtual void setFriction(f32 a_friction) = 0;
	virtual void stepMove(f64 dt, GameCollisions& a_collisionInfo) = 0;
	virtual bool hasMovement() = 0;
	virtual void useGravity(bool a_use) = 0;
	virtual bool onGround() const = 0;
	virtual bool onCeiling() const = 0;
	virtual edv::Vector3f getVelocity() = 0;
	virtual void resetVelocity() = 0;
};

#endif
