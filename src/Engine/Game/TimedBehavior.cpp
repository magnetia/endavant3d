#include "TimedBehavior.h"
#include "Core/CCoreEngine.h"
#include "Time/CTimeManager.h"
#include "Game/GameEntity.h"

TimedBehavior::TimedBehavior(f64 a_duration) :
	m_duration{ a_duration },
	m_missedTime{ 0 },
	m_startTimestamp{ 0 },
	m_pauseTimestamp{ 0 }
{
	
}

TimedBehavior::TimedBehavior(const TimedBehavior& a_other) :
	m_duration{ a_other.m_duration },
	m_missedTime{ a_other.m_missedTime },
	m_startTimestamp{ a_other.m_startTimestamp },
	m_pauseTimestamp{a_other.m_pauseTimestamp}
{

}

bool TimedBehavior::finished() const
{
	return
		elapsed() >= duration();
}

f64 TimedBehavior::duration() const
{
	return m_duration;
}

f64 TimedBehavior::elapsed() const
{
	const f64 now = CCoreEngine::Instance().GetTimerManager().GetTotalTimeSeconds();

	return
		now - m_startTimestamp - m_missedTime;
}

f64 TimedBehavior::remaining() const
{
	return duration() - elapsed();
}

f64 TimedBehavior::startTime() const
{
	return m_startTimestamp;
}

void TimedBehavior::initImpl()
{
	m_startTimestamp = CCoreEngine::Instance().GetTimerManager().GetTotalTimeSeconds();
	m_missedTime = 0;
}

void TimedBehavior::pauseImpl()
{
	m_pauseTimestamp = CCoreEngine::Instance().GetTimerManager().GetTotalTimeSeconds();
}

void TimedBehavior::resumeImpl()
{
	m_missedTime += 
		CCoreEngine::Instance().GetTimerManager().GetTotalTimeSeconds() - m_pauseTimestamp;
}
