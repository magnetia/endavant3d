#include <algorithm>
#include "Core/CCoreEngine.h"
#include "Time/CTimeManager.h"
#include "Actor.h"
#include "GameEntity.h"
#include "Physics/CPhysicsManager.h"
#include "Physics/btResultCallback.hpp"
#include "Script/ScriptManager.h"
#include <BulletCollision/CollisionDispatch/btGhostObject.h>
#include <BulletDynamics/Character/btKinematicCharacterController.h>
#include "CollisionNotifier.h"
#include "GameManager.h"

// #debug
#include "Core/CLogManager.h"

Actor::Actor(s32 a_maxLife, const ScriptConfig& a_scriptConfig) :
	m_state{ 0 },
	m_stateTimestamp{ 0 },
	m_life{ a_maxLife },
	m_maxLife{ a_maxLife },
	m_scriptConfig{ a_scriptConfig },
	m_movement{ nullptr },
	m_collisions{ },
	m_lastFrameCollisions{ }
{

}

Actor::~Actor()
{

}

void Actor::init()
{
	// initialize movement
	m_movement.reset(createMovement());
	m_movement->setParent(this);
	m_movement->init();

	// initialize script
	if (!m_scriptConfig.empty())
	{
		auto& script = CCoreEngine::Instance().GetScriptManager();
		script.loadScript(m_scriptConfig.path);
		script.callProcedure(m_scriptConfig.scope + "OnInit", getParent());
	}

	// initialize derived class
	initImpl();
}

void Actor::update(f64 dt)
{
	// moviment
	if (m_movement)
	{
		m_movement->stepMove(dt, m_collisions);
	}

	// colisions
	auto& game = CCoreEngine::Instance().GetGameManager();
	for (CollisionInfo& collision : m_collisions)
	{
		if (std::find(m_lastFrameCollisions.begin(), m_lastFrameCollisions.end(), collision) == m_lastFrameCollisions.end())
		{
			if (game.hasEntity(collision.target))
			{
				onCollisionEnter(collision.target);
			}
		}

		onCollision(collision);
	}

	for (GameCollisions::const_iterator it = m_lastFrameCollisions.begin(); it != m_lastFrameCollisions.end(); it++)
	{
		const CollisionInfo& collision = (*it);

		if (std::find(m_collisions.begin(), m_collisions.end(), collision) == m_collisions.end())
		{
			if (game.hasEntity(collision.target))
			{
				onCollisionExit(collision.target);
			}
		}
	}

	// Update classes derivades i Lua
	if (!m_scriptConfig.empty())
	{
		CCoreEngine::Instance().GetScriptManager().callProcedure(m_scriptConfig.scope + "OnUpdate", dt, getParent());
	}

	updateImpl(dt);

	// assign last frame collisions
	m_lastFrameCollisions = m_collisions;

	// clear this frame collisions
	m_collisions.clear();
}

void Actor::stop()
{
	// delete movement
	m_movement.reset();
	stopImpl();
}

void Actor::clear()
{

}

void Actor::addCollision(const CollisionInfo& a_collisionInfo)
{
	m_collisions.push_back(a_collisionInfo);
}

void Actor::onCollision(const CollisionInfo& a_collisionInfo)
{
	if (!m_scriptConfig.empty())
	{
		auto& script = CCoreEngine::Instance().GetScriptManager();
		script.callProcedure(m_scriptConfig.scope + "OnCollision", 
			getParent(), a_collisionInfo.target, a_collisionInfo.hitPoint, a_collisionInfo.hitNormal);
	}
}

void Actor::onCollisionEnter(GameEntity* a_entity)
{
	if (!m_scriptConfig.empty())
	{
		auto& script = CCoreEngine::Instance().GetScriptManager();

		script.callProcedure(m_scriptConfig.scope + "OnCollisionEnter",
			getParent(), a_entity);
	}
}

void Actor::onCollisionExit(GameEntity* a_entity)
{
	if (!m_scriptConfig.empty())
	{
		auto& script = CCoreEngine::Instance().GetScriptManager();

		script.callProcedure(m_scriptConfig.scope + "OnCollisionExit",
			getParent(), a_entity);
	}
}

void Actor::applyMove(const edv::Vector3f& a_vector)
{
	if (m_movement)
	{
		m_movement->applyMove(a_vector);
	}
}

void Actor::applyForce(const edv::Vector3f& a_vector)
{
	if (m_movement)
	{
		m_movement->applyForce(a_vector);
	}
}

void Actor::applyImpulse(const edv::Vector3f& a_vector)
{
	if (m_movement)
	{
		m_movement->applyImpulse(a_vector);
	}
}

void Actor::setGravity(const edv::Vector3f& a_vector)
{
	if (m_movement)
	{
		m_movement->setGravity(a_vector);
	}
}

edv::Vector3f Actor::getVelocity() const
{
	edv::Vector3f velocity{ 0.f, 0.f, 0.f };
	if (m_movement)
	{
		velocity = m_movement->getVelocity();
	}
	return velocity;
}

void Actor::resetVelocity()
{
	if (m_movement)
	{
		m_movement->resetVelocity();
	}
}

f64 Actor::stateElapsed() const
{
	return CCoreEngine::Instance().GetTimerManager().GetTotalTimeSeconds() - m_stateTimestamp;
}

s32 Actor::getState() const
{
	return m_state;
}

void Actor::setState(s32 a_state)
{
	m_state = a_state;
	m_stateTimestamp = CCoreEngine::Instance().GetTimerManager().GetTotalTimeSeconds();
}

void Actor::setLife(s32 a_life)
{
	m_life = a_life > 0 ? (a_life < m_maxLife? a_life : m_life ) : 0;
}

void Actor::setMaxLife()
{
	m_life = m_maxLife;
}

void Actor::decreaseLife(s32 a_life)
{
	const s32 diff = m_life - a_life;
	m_life = diff > 0 ? diff : 0;
}

void Actor::kill()
{
	stop();
	m_life = 0;
}

s32 Actor::getLife() const
{
	return m_life;
}

s32 Actor::getMaxLife() const
{
	return m_maxLife;
}

bool Actor::isOnCeiling() const
{
	bool onCeiling = false;

	if (m_movement)
	{
		onCeiling = m_movement->onCeiling();
	}

	return onCeiling;
}

bool Actor::isOnGround() const
{
	bool onGround = false;

	if (m_movement)
	{
		onGround = m_movement->onGround();
	}

	return onGround;
}

const ScriptConfig& Actor::getScriptConfig() const
{
	return m_scriptConfig;
}

ActorMovement* Actor::createMovement()
{
	return new CollisionNotifier;
}
