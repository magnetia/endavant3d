#include "MouseListener.h"
#include "Core/CCoreEngine.h"
#include "Input/CInputManager.h"
#include "Renderer/CRenderManager.h"
#include "Physics/CPhysicsManager.h"
#include "Camera/CameraManager.h"
#include "Camera/Camera.h"
#include "Options\OptionManager.h"

MouseListener::MouseListener(OnClickFunc a_onClickFunc, OnUpFunc a_onUpFunc, OnHoverFunc a_onHoverFunc) :
	m_hovering{ false },
	m_onClick{ a_onClickFunc },
	m_onUp{ a_onUpFunc },
	m_onHover{ a_onHoverFunc }
{
}

MouseListener::MouseListener(MouseListener&& a_other) :
m_hovering{ a_other.m_hovering },
m_onClick{ a_other.m_onClick },
m_onUp{ a_other.m_onUp },
m_onHover{ a_other.m_onHover },
m_buttons{ a_other.m_buttons }
{
	a_other.m_onClick	= nullptr;
	a_other.m_onUp		= nullptr;
	a_other.m_onHover	= nullptr;
}

MouseListener::~MouseListener()
{
}

Behavior* MouseListener::clone()
{
	return new MouseListener(m_onClick,m_onUp, m_onHover);
}

void MouseListener::initImpl()
{
	addActions();
}

void MouseListener::updateImpl(f64 dt)
{
	const bool useGC = CCoreEngine::Instance().GetOptionManager().getOption("controls/usegamepad");

	if (useGC)
	{
		auto &gpad = CCoreEngine::Instance().GetInputManager().GetGameController();
		auto gamepads = gpad.getGameControllersIDs();
		if (!gamepads.empty())
		{
			auto gcontroller = gpad.getGameController(gamepads.at(0));
			for (auto& gcbutton : m_gcbuttons)
			{
				if (gcontroller.lock()->IsActionKeyPushed(gcbutton.first))
				{

					if (m_onClick)
						m_onClick(gcbutton.second);
				}
				else if (gcontroller.lock()->IsActionButtonReleased(gcbutton.first))
				{
					if (m_onUp)
						m_onUp(gcbutton.second);

				}
			}
		}
	}
	else
	{
		static const f32 HORIZON_DISTANCE = 1000.f;
		auto& mouse = CCoreEngine::Instance().GetInputManager().GetMouse();

		for (auto& status : m_buttons)
		{
			if (mouse.IsActionButtonPushed(status.first))
			{
				if (m_onClick)
					m_onClick(status.second);
			}
			else if (mouse.IsActionButtonReleased(status.first))
			{
				if (m_onUp)
					m_onUp(status.second);
			}
		}
	}
	


	// #todo: use raycast from physics manager
	/*
	const bool hovers = false;
	auto posmouse = CCoreEngine::Instance().GetInputManager().GetMouse().GetPos();
	auto world_mouse_pos = CCoreEngine::Instance().GetCameraManager().get_current_camera()->TransformScreenCoordToWorldCoord(Ogre::Vector2(static_cast<f32>(posmouse.x), static_cast<f32>(posmouse.y)));
	Ogre::Camera* const camera = CCoreEngine::Instance().GetRenderManager().GetOgreViewport()->getCamera();
	auto near_distance = camera->getNearClipDistance();
	auto far_distance = camera->getFarClipDistance();
	Ogre::Vector3 direction = camera->getDerivedDirection();

	edv::Vector3f orig(world_mouse_pos + direction*near_distance), dest(world_mouse_pos + direction*far_distance);
	GameEntity* hit = static_cast<GameEntity*>(CCoreEngine::Instance().GetPhysicsManager().RayTest(orig, dest));
	if (hit)
	{
		std::cout << "RAYTEST :" << hit->getName() << std::endl;
	}

	for (auto& status : m_buttons)
	{
		if (hovers)
		{
			if (!m_hovering)
			{
				m_hovering = true;

				if (m_onHover)
					m_onHover(true);
			}

			if (mouse.IsActionButtonPushed(status.first))
			{
				if (m_onClick)
					m_onClick(status.second);
			}
			else if (mouse.IsActionButtonUp(status.first))
			{
				if (m_onUp)
					m_onUp(status.second);
			}
		}
		else
		{
			if (m_hovering)
			{
				m_hovering = false;

				if (m_onHover)
					m_onHover(false);
			}
		}
	}*/
}

void MouseListener::stopImpl()
{
	removeActions();
}

void MouseListener::pauseImpl()
{
	removeActions();
}

void MouseListener::resumeImpl()
{
	addActions();
}

void MouseListener::addActions()
{
	removeActions();
	const bool useGC = CCoreEngine::Instance().GetOptionManager().getOption("controls/usegamepad");
	if (useGC)
	{
		auto &gpad = CCoreEngine::Instance().GetInputManager().GetGameController();
		auto gamepads = gpad.getGameControllersIDs();
		if (!gamepads.empty())
		{
			m_gcbuttons.push_back({ CCoreEngine::Instance().GetInputManager().GetGameController().getGameController(gamepads.at(0)).lock()->InsertKeyAction(EDV_GAMECONTROLLER_BUTTON_LEFTSHOULDER), MOUSE_BUTTON_LEFT });
			m_gcbuttons.push_back({ CCoreEngine::Instance().GetInputManager().GetGameController().getGameController(gamepads.at(0)).lock()->InsertKeyAction(EDV_GAMECONTROLLER_BUTTON_RIGHTSHOULDER), MOUSE_BUTTON_RIGHT });
		}
	}
	else
	{
		m_buttons.push_back({ CCoreEngine::Instance().GetInputManager().GetMouse().InsertButtonAction("MOUSE_LB"), MOUSE_BUTTON_LEFT });
		m_buttons.push_back({ CCoreEngine::Instance().GetInputManager().GetMouse().InsertButtonAction("MOUSE_RB"), MOUSE_BUTTON_RIGHT });
		m_buttons.push_back({ CCoreEngine::Instance().GetInputManager().GetMouse().InsertButtonAction("MOUSE_MB"), MOUSE_BUTTON_MIDDLE });
	}
}

void MouseListener::removeActions()
{
	const bool useGC = CCoreEngine::Instance().GetOptionManager().getOption("controls/usegamepad");
	if (useGC)
	{
		auto &gpad = CCoreEngine::Instance().GetInputManager().GetGameController();
		auto gamepads = gpad.getGameControllersIDs();
		if (!gamepads.empty())
		{
			for (Buttons::const_iterator it = m_gcbuttons.begin(); it != m_gcbuttons.end(); it++)
			{
				CCoreEngine::Instance().GetInputManager().GetGameController().getGameController(gamepads.at(0)).lock()->RemoveKeyAction((*it).first);
			}
			gamepads.clear();
		}
	}
	else
	{
		for (Buttons::const_iterator it = m_buttons.begin(); it != m_buttons.end(); it++)
		{
			CCoreEngine::Instance().GetInputManager().GetMouse().RemoveButtonAction((*it).first);
		}
		m_buttons.clear();
	}
}
