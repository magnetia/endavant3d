#include "Core/CCoreEngine.h"
#include "Core/CLogManager.h"
#include "Utils/tree_xml_utils.h"
#include "GameEntity.h"
#include "GameManager.h"

// Builtin behaviors
#include "Controller.h"
#include "SpeedController.h"
#include "LuaBehavior.h"
#include "DistanceSound.h"
#include "GlowAnimator.h"
#include "CelShader.h"
#include "MagneticAnimator.h"

const std::string ENTITY_CONFIG_PATH{ "Config/entity_options.xml" };
//const std::string LEVELS_CONFIG_PATH{ "Config/levels.xml" };

void GameManager::startUp()
{
	LOG(LOG_INFO, LOGSUB_GAME, "Starting Up!");

	loadBehaviors();
	loadCustomMaterials();
	loadEntities();
	loadLevels();
}

void GameManager::shutDown()
{
	LOG(LOG_INFO, LOGSUB_GAME , "Shutting Down!");

	m_entityConfig.clear();
	m_registeredEntities.clear();
	m_behaviorFactories.clear();
	m_actorFactories.clear();
	m_players.clear();
}

void GameManager::update(f64 dt)
{

}

GameManager::GameManager() :
	m_default_actor{nullptr},
	m_boss{ nullptr }
{

}

GameManager::~GameManager()
{

}

//Level* GameManager::createLevel(const std::string& a_levelName)
//{
//	Config::tree_type levelConf = m_levelsConfig.get_tree(a_levelName);
//
//	if (!levelConf.empty())
//	{
//		return new Level{ { levelConf.key(), levelConf["map"] } };
//	}
//}

GameEntity* GameManager::createEntity(const std::string& a_type, const std::string& a_name,
	const Ogre::Vector3& a_position, const Ogre::Quaternion& a_rotation, Ogre::SceneNode* a_parent)
{
	GameEntity* entity = nullptr;

	if (m_entityConfig.path_exists(a_type))
	{
		const Config::tree_type& config = m_entityConfig.get_tree(a_type);		
		EntityOgreGraphics::Config graphicsCfg{ a_name, a_position, a_rotation, false, "", a_parent };
		EntityBtPhysics::Config physicsCfg;

		// load entity graphics
		if (config.path_exists("graphics"))
		{
			// #todo: memoria dinamica
			const Config::tree_type& graphics = config.get_tree("graphics");
			std::vector<EntityOgreGraphics::AnimationConfig> animationsCfg;
			std::vector <EntityOgreGraphics::ParticleConfig> particlesCfg;
			std::vector<CustomMaterial*> customMaterialsCfg;
			std::vector<EntityOgreGraphics::LightConfig> lightsCfg;
			Ogre::Vector3 scaleValue{ 1.f, 1.f, 1.f };
			Ogre::Quaternion rotValue{ 1.f, 0.f, 0.f, 0.f };

			if (graphics.path_exists("scale"))
			{
				const Config::tree_type& scale = graphics.get_tree("scale");
				const f32 scaleX = scale["x"], scaleY = scale["y"], scaleZ = scale["z"];

				scaleValue.x = scaleX ? scaleX : 1.f;
				scaleValue.y = scaleY ? scaleY : 1.f;
				scaleValue.z = scaleZ ? scaleZ : 1.f;
			}

			if (graphics.path_exists("rotation"))
			{
				const Config::tree_type& rotation = graphics.get_tree("rotation");
				const std::vector<Ogre::Vector3> axisVec{ Ogre::Vector3::UNIT_X, Ogre::Vector3::UNIT_Y, Ogre::Vector3::UNIT_Z };
				u32 index = 0;

				for (const f32 degrees : {rotation["x"], rotation["y"], rotation["z"]})
				{
					if (degrees)
					{
						rotValue.FromAngleAxis(Ogre::Degree{ degrees }, axisVec[index]);
					}

					index++;
				}
			}

			if (graphics.path_exists("materials/custom"))
			{
				const Config::tree_type& materials = graphics.get_tree("materials/custom");
				Config::keys_type keys = materials.keys();

				for (auto key : keys)
				{
					auto it_cm = m_customMaterialFactories.find(key);

					if (it_cm != m_customMaterialFactories.end())
					{
						const Config::tree_type& materialConfig = materials.get_tree(key);
						customMaterialsCfg.push_back(it_cm->second->createElement(materialConfig));
					}
					else
					{
						throw std::runtime_error{ "custom material factory not found: " + key };
					}
				}
			}

			if (graphics.path_exists("animations"))
			{
				const Config::tree_type& animations = graphics.get_tree("animations");
				Config::keys_type keys = animations.keys();

				for (auto key : keys)
				{
					const Config::tree_type& animation = animations.get_tree(key);
					const f32 speed = animation["speed"];
					const bool loop = animation.try_get("loop", "false").as<bool>();
					animationsCfg.push_back({ key, animation["enabled"], speed ? speed : 1.f, loop });
				}
			}

			if (graphics.path_exists("particles"))
			{
				const Config::tree_type& particles = graphics.get_tree("particles");
				Config::keys_type keys = particles.keys();

				for (auto key : keys)
				{
					Ogre::Vector3 particleScaleValue{ 1.f, 1.f, 1.f };
					const Config::tree_type& particle = particles.get_tree(key);

					if (particle.path_exists("scale"))
					{
						const Config::tree_type& scale = particle.get_tree("scale");
						const f32 scaleX = scale["x"], scaleY = scale["y"], scaleZ = scale["z"];

						particleScaleValue.x = scaleX ? scaleX : 1.f;
						particleScaleValue.y = scaleY ? scaleY : 1.f;
						particleScaleValue.z = scaleZ ? scaleZ : 1.f;
					}

					particlesCfg.push_back({
						key,
						particle["system"],
						particle["bone"],
						{ particle["translation/x"], particle["translation/y"], particle["translation/z"] },
						particleScaleValue,
						particle["attach_to_parent"],
						particle.try_get("movement_sync", true).as<bool>()
					});
				}
			}

			if (graphics.path_exists("lights"))
			{
				// #todo: parsejar mes propietats de les llums
				const Config::tree_type& lights = graphics.get_tree("lights");
				Config::keys_type keys = lights.keys();

				for (auto key : keys)
				{
					const Config::tree_type& light = lights.get_tree(key);
					const EntityOgreGraphics::LightType lightType = EntityOgreGraphics::stringToLightType(light["type"]);

					const Ogre::ColourValue diffuse{
						light.try_get("diffuse/r", 1.f),
						light.try_get("diffuse/g", 1.f),
						light.try_get("diffuse/b", 1.f),
						light.try_get("diffuse/a", 1.f)
					};

					const Ogre::ColourValue specular{ 
						light.try_get("specular/r", 1.f), 
						light.try_get("specular/g", 1.f), 
						light.try_get("specular/b", 1.f),
						light.try_get("specular/a", 1.f)
					};

					const edv::Vector3f direction{
						light.try_get("direction/x", 0.f),
						light.try_get("direction/y", 0.f),
						light.try_get("direction/z", 1.f),
					};

					lightsCfg.push_back({ lightType, key, diffuse, specular, direction });
				}
			}

			// #todo: va a petarrr... xD
			graphicsCfg = { 
				a_name, 
				a_position + Ogre::Vector3{ graphics["translation/x"], graphics["translation/y"], graphics["translation/z"] },
				a_rotation * rotValue, true, graphics["mesh"], a_parent, graphics["materials/default"], scaleValue, animationsCfg, particlesCfg,
				customMaterialsCfg, lightsCfg
			};
		}

		// load entity physics
		if (config.path_exists("physics"))
		{
			const Config::tree_type& physics = config.get_tree("physics");
			MagneticProperty::Config magnetismCfg;
			EntityBtPhysics::CollisionConfig collisionCfg;
			EntityBtPhysics::CollisionDetection detectionMode = EntityBtPhysics::COLLISION_DETECTION_NONE;

			if (physics.path_exists("collision"))
			{
				const Config::tree_type& collision = physics.get_tree("collision");
				edv::Vector3f scaleValue{ 1.f, 1.f, 1.f };
				Ogre::Quaternion rotValue{ 1.f, 0.f, 0.f, 0.f };

				if (collision.path_exists("scale"))
				{
					const Config::tree_type& scale = collision.get_tree("scale");
					const f32 scaleX = scale["x"], scaleY = scale["y"], scaleZ = scale["z"];

					scaleValue.x = (scaleX ? scaleX : 1.f);
					scaleValue.y = (scaleY ? scaleY : 1.f);
					scaleValue.z = (scaleZ ? scaleZ : 1.f);
				}

				if (collision.path_exists("rotation"))
				{
					const Config::tree_type& rotation = collision.get_tree("rotation");
					const f32 rotW = rotation["w"];

					rotValue.w = (rotW ? rotW : 1.f);
					rotValue.x = (rotation["x"]);
					rotValue.y = (rotation["y"]);
					rotValue.z = (rotation["z"]);
				}

				if (collision.path_exists("detection"))
				{
					detectionMode = EntityBtPhysics::stringToCollisionDetection(collision["detection"]);
				}

				collisionCfg = { 
					EntityBtPhysics::stringToCollisionShape(collision["shape"]),
					detectionMode,
					{ collision["translation/x"], collision["translation/y"], collision["translation/z"] },
					rotValue,
					scaleValue,
					collision["mesh"]
				};
			}

			if (physics.path_exists("magnetism"))
			{
				const Config::tree_type& magnetism = physics.get_tree("magnetism");
				magnetismCfg = { MagneticProperty::stringToCharge(magnetism["charge"]), magnetism["force"], magnetism["scope"], magnetism.path_exists("factor") ? magnetism["factor"] : 1.f };
			}

			physicsCfg = { physics["mass"], physics["kinematic"], magnetismCfg, physics["physics2D"], collisionCfg };
		}

		// add actor
		Actor* actor = nullptr;

		if (config.path_exists("actor"))
		{
			const Config::tree_type& actorCfg = config.get_tree("actor");
			const std::string actorName = actorCfg.try_get("name", a_type).as<std::string>();
			auto it_a = m_actorFactories.find(actorName);

			if (it_a != m_actorFactories.end())
			{
				actor = it_a->second->createElement(config.path_exists("actor") ? config.get_tree("actor") : Config::tree_type{});
			}
			else if (m_default_actor.get() && config.path_exists("actor"))
			{
				actor = m_default_actor->createElement(config.get_tree("actor"));
			}
		}

		// create entity
		entity = new GameEntity{graphicsCfg, physicsCfg, actor};
		entity->init();

		// add behaviors
		if (config.path_exists("logic"))
		{
			const Config::tree_type& logic = config.get_tree("logic");
			auto behaviorNames = logic.keys();

			for (const auto& behaviorName : behaviorNames)
			{
				auto it_b = m_behaviorFactories.find(behaviorName);

				if (it_b != m_behaviorFactories.end())
				{
					entity->addBehavior(it_b->second->createElement(logic.get_tree(behaviorName)));
				}
				else
				{
					throw std::runtime_error{ "unable to find behavior of type " + behaviorName };
				}
			}
		}

		// register entity
		auto it_e = m_registeredEntities.find(a_name);

		if (it_e == m_registeredEntities.end())
		{
			m_registeredEntities.emplace_hint(it_e, std::make_pair(a_name, EntityPtr{ entity }));
		}
		else
		{
			throw std::runtime_error{ "duplicated entity name: " + a_name };
		}
	}

	return entity;
}

void GameManager::destroyEntity(const std::string& a_name)
{
	auto it = m_registeredEntities.find(a_name);
	
	if (it != m_registeredEntities.end())
	{
		if (Actor* const actor = it->second->getActor())
		{
			actor->stop();
		}

		m_registeredEntities.erase(it);
	}
}

void GameManager::destroyEntity(GameEntity* a_entity)
{
	for (auto it = m_registeredEntities.begin(); it != m_registeredEntities.end(); )
	{
		if (it->second.get() == a_entity)
		{
			if (Actor* const actor = it->second->getActor())
			{
				actor->stop();
			}

			m_registeredEntities.erase(it);
			break;
		}
		else
		{
			it++;
		}
	}
}

GameManager::EntityInfo GameManager::createEntity(const std::string& a_type,
	const Ogre::Vector3& a_position, const Ogre::Quaternion& a_rotation)
{
	std::pair<std::string, GameEntity*> result{ std::to_string(m_autoIdGen.nextId()), nullptr };
	result.second = createEntity(a_type, result.first, a_position, a_rotation);

	return result;
}

GameEntity* GameManager::getEntity(const std::string& a_name) const
{
	GameEntity* entity = nullptr;
	auto it_e = m_registeredEntities.find(a_name);

	if (it_e != m_registeredEntities.end())
	{
		entity = it_e->second.get();
	}

	return entity;
}

bool		GameManager::hasEntity(GameEntity* a_entity) const
{
	bool found = false;
	for (auto entity_it = m_registeredEntities.begin(); entity_it != m_registeredEntities.end() && !found; entity_it++)
	{
		if ((*entity_it).second.get() == a_entity)
		{
			found = true;
		}
	}

	return found;
}

GameEntity* GameManager::getPlayer(PlayerId a_id) const
{
	GameEntity* player = nullptr;
	PlayerId wantedId = 
		a_id == IdGenerator<PlayerId>::INVALID_ID ? m_playerIdGen.getCurrentId() : a_id;

	auto it = m_players.find(wantedId);

	if (it != m_players.end())
	{
		GameEntity* const entity = it->second;
		player = entity;
	}

	return player;
}

Level* GameManager::getLevel(const std::string& a_name)
{
	Level* level = nullptr;
	auto it = m_levels.find(a_name);

	if (it != m_levels.end())
	{
		level = it->second;
	}

	return level;
}

GameEntity* GameManager::getBoss() const
{
	return m_boss;
}

Level* GameManager::getCurrentLevel()
{
	Level* level = nullptr;

	if (!m_levels.empty())
	{
		level = m_levels.begin()->second;
	}

	return level;
}

std::vector<std::string> GameManager::getLevelsRegistered()
{
	std::vector<std::string> levels_ids;
	for (auto lvl : m_levels)
		levels_ids.push_back(lvl.first);
	return levels_ids;
}

void GameManager::registerComponent(ComponentType a_type, const std::string& a_name, void* a_factory)
{
	switch (a_type)
	{
	case COMPONENT_TYPE_BEHAVIOR:
		registerBehaviorFactory(a_name, static_cast<Behavior::factory_type*>(a_factory));
		break;

	case COMPONENT_TYPE_ACTOR:
		registerActorFactory(a_name, static_cast<Actor::factory_type*>(a_factory));
		break;

	case COMPONENT_TYPE_ACTOR_MOVEMENT:
		break;

	case COMPONENT_TYPE_CUSTOM_MATERIAL:
		registerCustomMaterialFactory(a_name, static_cast<CustomMaterial::factory_type*>(a_factory));
		break;

	default:
		throw std::runtime_error{ "attempt to register component \"" + a_name + "\" of unknown type " + std::to_string(a_type) };
		break;
	}
}

void GameManager::reloadEntityConfiguration()
{
	loadEntities();
}

GameManager::PlayerId GameManager::registerPlayer(GameEntity* a_player)
{
	PlayerId id = m_playerIdGen.nextId();

	m_players[id] = a_player;

	return id;
}

void GameManager::registerBoss(GameEntity* a_boss)
{
	m_boss = a_boss;
}

void GameManager::unregisterPlayer(PlayerId a_id)
{
	auto it = m_players.find(a_id);

	if (it != m_players.end())
	{
		m_players.erase(it);
	}
}

void GameManager::unregisterPlayer(GameEntity* a_player)
{
	bool found = false;

	for (auto it = m_players.begin(); !found && it != m_players.end(); )
	{
		GameEntity* const entity = it->second;

		if (entity == a_player)
		{
			it = m_players.erase(it);
			found = true;
		}
		else
		{
			it++;
		}
	}
}

void GameManager::unregisterBoss()
{
	m_boss = nullptr;
}

void GameManager::registerLevel(const std::string& a_name, Level* a_level)
{
	m_levels[a_name] = a_level;
}

void GameManager::unregisterLevel(const std::string& a_name)
{
	auto it = m_levels.find(a_name);

	if (it != m_levels.end())
	{
		m_levels.erase(it);
	}
}

void GameManager::unregisterLevel(Level* a_level)
{
	bool found = false;

	for (auto it = m_levels.begin(); !found && it != m_levels.end();)
	{
		Level* const level = it->second;

		if (level == a_level)
		{
			it = m_levels.erase(it);
			found = true;
		}
		else
		{
			it++;
		}
	}
}

void GameManager::registerDefaultActor(Actor::factory_type* a_actor)
{
	m_default_actor.reset(a_actor);
}

void GameManager::registerBehaviorFactory(const std::string& a_name, Behavior::factory_type* a_factory)
{
	auto it = m_behaviorFactories.find(a_name);

	if (it == m_behaviorFactories.end())
	{
		m_behaviorFactories.emplace_hint(it, BehaviorFactories::value_type{ a_name, BehaviorFactoryPtr{ a_factory } });
	}
	else
	{
		throw std::runtime_error{ "attempt to register a duplicated name (\"" + a_name + "\") for a behavior" };
	}
}

void GameManager::registerActorFactory(const std::string& a_name, Actor::factory_type* a_factory)
{
	auto it = m_actorFactories.find(a_name);

	if (it == m_actorFactories.end())
	{
		m_actorFactories.emplace_hint(it, ActorFactories::value_type{ a_name, ActorFactoryPtr{ a_factory } });
	}
	else
	{
		throw std::runtime_error{ "attempt to register a duplicated name (\"" + a_name + "\") for an actor" };
	}
}

void GameManager::registerCustomMaterialFactory(const std::string& a_name, CustomMaterial::factory_type* a_factory)
{
	auto it = m_customMaterialFactories.find(a_name);

	if (it == m_customMaterialFactories.end())
	{
		m_customMaterialFactories.emplace_hint(it, CustomMaterialFactories::value_type{ a_name, CustomMaterialFactoryPtr{ a_factory } });
	}
	else
	{
		throw std::runtime_error{ "attempt to register a duplicated name (\"" + a_name + "\") for a custom material" };
	}
}

void GameManager::loadBehaviors()
{
	// register builtin behaviors
	registerBehaviorFactory("Controller", new Controller::Factory);
	registerBehaviorFactory("SpeedController", new SpeedController::Factory);
	registerBehaviorFactory("LuaBehavior", new LuaBehavior::Factory);
	registerBehaviorFactory("DistanceSound", new DistanceSound::Factory);
	registerBehaviorFactory("GlowAnimator", new GlowAnimator::Factory);
	registerBehaviorFactory("MagneticAnimator", new MagneticAnimator::Factory);
}

void GameManager::loadCustomMaterials()
{
	registerCustomMaterialFactory("CelShader", new CelShader::Factory);
}

void GameManager::loadEntities()
{
	m_entityConfig.clear();
	pugi::xml_document document;

	if (pugi::xml_parse_result result = document.load_file(ENTITY_CONFIG_PATH.c_str()))
	{
		edv::tree_from_xml(m_entityConfig, document.child("EntityConfiguration"));
	}
}

void GameManager::loadLevels()
{
	//pugi::xml_document document;

	//if (pugi::xml_parse_result result = document.load_file(LEVELS_CONFIG_PATH.c_str()))
	//{
	//	edv::tree_from_xml(m_levelsConfig, document.child("Levels"));
	//}
}
