#include "Core/CCoreEngine.h"
#include "Renderer/CRenderManager.h"
#include "Game/GameEntity.h"
#include "ParticleSystemAnimator.h"

ParticleSystemAnimator::ParticleSystemAnimator(const std::string& a_name, f64 a_duration, const std::string& a_particleSystem, const std::string& a_boneName,
	const edv::Vector3f& a_translation, const edv::Vector3f& a_scale, OnStopAction a_onStopAction) :
	TimedBehavior{ a_duration },
	m_name{ a_name },
	m_particleSystemName{ a_particleSystem },
	m_boneName{ a_boneName },
	m_translation{ a_translation },
	m_scale{ a_scale },
	m_playing{ false },
	m_onStopAction{ a_onStopAction }
{

}

Behavior* ParticleSystemAnimator::clone()
{
	return new ParticleSystemAnimator{ m_name, duration(), m_particleSystemName, m_boneName, m_translation, m_scale };
}

void ParticleSystemAnimator::initImpl()
{
	TimedBehavior::initImpl();

	getParent()->getGraphics()->addParticleSystem(m_name, EntityOgreGraphics::ParticleConfig{ "", m_particleSystemName, m_boneName, m_translation, m_scale });
	m_playing = true;
}

void ParticleSystemAnimator::updateImpl(f64 dt)
{
	if (finished() && m_playing)
	{
		EntityOgreGraphics* const graphics{ getParent()->getGraphics() };

		switch (m_onStopAction)
		{
			case ON_STOP_SET_INVISIBLE:
			{
				graphics->getParticleSystemNode(m_name)->setVisible(false);
				stopImpl();
			}
			break;

			case ON_STOP_DISABLE_EMITTERS:
			{
				Ogre::ParticleSystem* const system{ graphics->getParticleSystem(m_name) };
				const u16 numEmitters{ system->getNumEmitters() };

				for (u16 emitterIdx = 0; emitterIdx < numEmitters; emitterIdx++)
				{
					Ogre::ParticleEmitter* const emitter{ system->getEmitter(emitterIdx) };

					emitter->setEnabled(false);
				}

				m_playing = false;
			}
			break;
		}
	}
}

void ParticleSystemAnimator::stopImpl()
{
	m_playing = false;
	getParent()->getGraphics()->removeParticleSystem(m_name);
}
