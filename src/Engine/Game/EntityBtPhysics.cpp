#include <iterator>
#include "Core/CCoreEngine.h"
#include "Physics/COgreMotionState.h"
#include "Externals/MeshStrider/MeshStrider.h"
#include "GameEntity.h"
#include "Actor.h"
#include "EntityBtPhysics.h"

EntityBtPhysics::EntityBtPhysics() :
	m_mass{ 0. },
	m_collisionConfig{ },
	m_worldObject{ nullptr },
	m_physics2D{ false },
	m_ghost{ nullptr },
	m_initialScale{ 1.f, 1.f, 1.f }
{

}

EntityBtPhysics::~EntityBtPhysics()
{
	
}

EntityBtPhysics::CollisionShape EntityBtPhysics::stringToCollisionShape(const std::string& a_shape)
{
	CollisionShape shape = COLLISION_SHAPE_NONE;
	std::string shapeStr;
	std::transform(a_shape.begin(), a_shape.end(), std::back_inserter(shapeStr), ::tolower);

	if (shapeStr == "box")
	{
		shape = COLLISION_SHAPE_BOX;
	}
	else if (shapeStr == "sphere")
	{
		shape = COLLISION_SHAPE_SPHERE;
	}
	else if (shapeStr == "capsule")
	{
		shape = COLLISION_SHAPE_CAPSULE;
	}
	else if (shapeStr == "hull")
	{
		shape = COLLISION_SHAPE_HULL;
	}
	else if (shapeStr == "triangle_mesh")
	{
		shape = COLLISION_SHAPE_TRIANGLE_MESH;
	}

	return shape;
}

std::string EntityBtPhysics::collisionShapeToString(CollisionShape a_shape)
{
	std::string shape = "none";

	switch (a_shape)
	{
	case COLLISION_SHAPE_SPHERE:
		shape = "sphere";
		break;
	case COLLISION_SHAPE_BOX:
		shape = "box";
		break;
	case COLLISION_SHAPE_HULL:
		shape = "hull";
		break;
	case COLLISION_SHAPE_TRIANGLE_MESH:
		shape = "triangle_mesh";
		break;
	case COLLISION_SHAPE_CAPSULE:
		shape = "capsule";
		break;
	}

	return shape;
}

EntityBtPhysics::CollisionDetection EntityBtPhysics::stringToCollisionDetection(const std::string& a_detectionMode)
{
	CollisionDetection mode = COLLISION_DETECTION_NONE;
	std::string modeStr;
	std::transform(a_detectionMode.begin(), a_detectionMode.end(), std::back_inserter(modeStr), ::tolower);

	if (modeStr == "disable")
	{
		mode = COLLISION_DETECTION_DISABLE;
	}
	else if (modeStr == "ghost")
	{
		mode = COLLISION_DETECTION_GHOST;
	}
	else if (modeStr == "manifolds")
	{
		mode = COLLISION_DETECTION_MANIFOLDS;
	}
	else if (modeStr == "trigger")
	{
		mode = COLLISION_DETECTION_TRIGGER;
	}

	return mode;
}

std::string EntityBtPhysics::collisionDetectionToString(CollisionDetection a_detectionMode)
{
	std::string mode = "none";

	switch (a_detectionMode)
	{
	case COLLISION_DETECTION_DISABLE:
		mode = "disable";
		break;

	case COLLISION_DETECTION_GHOST:
		mode = "ghost";
		break;

	case COLLISION_DETECTION_MANIFOLDS:
		mode = "manifolds";
		break;

	case COLLISION_DETECTION_TRIGGER:
		mode = "trigger";
		break;
	}

	return mode;
}

EntityBtPhysics::EntityBtPhysics(BtWorldObject* a_worldObject, const Config& a_config)
{
	init(a_worldObject, a_config);
}

void EntityBtPhysics::createBody(const CollisionConfig& a_config, bool a_kinematic)
{
	m_collisionConfig = a_config;

	if (a_config.shape != COLLISION_SHAPE_NONE)
	{
		if (m_worldObject)
		{
			// create motion state
			m_motionState.reset(m_worldObject->createMotionState(a_config.translation.to_btVector3(), btQuaternion(m_collisionConfig.rotation.x, m_collisionConfig.rotation.y, m_collisionConfig.rotation.z, m_collisionConfig.rotation.w), a_kinematic));
			btVector3 localInertia(.0, .0, .0);

			// create collision shape
			BtWorldObject* collisionWorldObject = m_worldObject;
			std::unique_ptr<BtWorldObject> collisionObjectPtr;

			if (!a_config.mesh.empty())
			{
				// #todo: aqui no haurien d'apareixer referencies a Ogre
				EntityOgreGraphics::Config cfg{ m_worldObject->getObjectId() + "_collision", {}, {}, true, a_config.mesh, nullptr, "" };

				collisionObjectPtr.reset(m_worldObject->createWorldObject(&cfg));
				collisionWorldObject = collisionObjectPtr.get();
			}

			switch (m_collisionConfig.shape)
			{
				case COLLISION_SHAPE_SPHERE:
				{
					btVector3 halfSize = collisionWorldObject->getHalfSize();
					m_bulletCollisionShape.reset(
						new btSphereShape{ std::max({ halfSize.x(), halfSize.y(), halfSize.z() }) });
				}
				break;

				case COLLISION_SHAPE_BOX:
				{
					m_bulletCollisionShape.reset(
						new btBoxShape{ collisionWorldObject->getHalfSize() });
				}
				break;

				case COLLISION_SHAPE_TRIANGLE_MESH:
				{
					btBvhTriangleMeshShape* shape = new btBvhTriangleMeshShape(collisionWorldObject->createStridingMeshInterface(), true, true);
					m_bulletCollisionShape.reset(shape);
				}
				break;

				case COLLISION_SHAPE_HULL:
				{
					std::vector<btVector3> vertices = collisionWorldObject->getVertices();
					btConvexHullShape* shape = new btConvexHullShape;

					for (u32 idx = 0; idx < vertices.size(); idx++)
					{
						shape->addPoint(vertices[idx]);
					}

					m_bulletCollisionShape.reset(shape);
				}
				break;

				case COLLISION_SHAPE_CAPSULE:
				{
					btVector3 halfSize = collisionWorldObject->getHalfSize();
					btScalar radius = std::max({ halfSize.x(), halfSize.z() });
					btScalar height = halfSize.y();
					m_bulletCollisionShape.reset(
						new btCapsuleShape{ radius, height });
				}
				break;

				default:
				{
					throw std::runtime_error("not implemented");
				}
				break;
			}

			// scale is collision configuration * scale of the world object
			m_initialScale = edv::Vector3f(a_config.scale * collisionWorldObject->btGetScale());

			if (m_bulletCollisionShape)
			{
				m_bulletCollisionShape->setLocalScaling(m_initialScale.to_btVector3());
			}

			// triangle meshes are static: inertia does not apply
			if (m_collisionConfig.shape != COLLISION_SHAPE_TRIANGLE_MESH && m_mass > 0.f)
			{
				m_bulletCollisionShape->calculateLocalInertia(m_mass, localInertia);
			}

			// create rigid body
			btRigidBody::btRigidBodyConstructionInfo rbInfo(m_mass,
				m_motionState.get(),
				m_bulletCollisionShape.get(),
				localInertia);

			m_rigidBody.reset(new btRigidBody(rbInfo));

			// kinematic configuration
			if (a_kinematic)
			{
				m_rigidBody->setCollisionFlags(m_rigidBody->getCollisionFlags() | btCollisionObject::CF_KINEMATIC_OBJECT);
				m_rigidBody->setActivationState(DISABLE_DEACTIVATION);
			}

			// 2D Physics configuration
			if (m_physics2D)
			{
				m_rigidBody->setLinearFactor(btVector3(1.0, 1.0, 0.0));
				m_rigidBody->setAngularFactor(btVector3(0.0, 0.0, 1.0));
			}

			// associate rigid body with game entity
			m_rigidBody->setUserPointer(getParent());

			// add rigid body to physics manager
			CCoreEngine::Instance().GetPhysicsManager().AddRigidBody(m_rigidBody.get());

			// configure collision detection
			btBroadphaseProxy::CollisionFilterGroups group = 
				a_kinematic ? btBroadphaseProxy::KinematicFilter : btBroadphaseProxy::DefaultFilter;

			switch (a_config.detection)
			{
				case COLLISION_DETECTION_DISABLE:
				{
					s32 bodyFlags = m_rigidBody->getCollisionFlags();
					m_rigidBody->setCollisionFlags(bodyFlags | btCollisionObject::CF_NO_CONTACT_RESPONSE);
				}
				break;

				case COLLISION_DETECTION_GHOST:
				{
					// create a ghost object for custom collisions  
					s32 bodyFlags = m_rigidBody->getCollisionFlags();
					m_rigidBody->setCollisionFlags(bodyFlags | btCollisionObject::CF_NO_CONTACT_RESPONSE);
					m_ghost.reset(new btPairCachingGhostObject);
					m_ghost->setWorldTransform(m_rigidBody->getWorldTransform());
					m_ghost->setCollisionShape(m_rigidBody->getCollisionShape());
					m_ghost->setCollisionFlags(bodyFlags); // #todo: configurable flags
					m_ghost->setUserPointer(getParent());

					// add collision object into world
					btDiscreteDynamicsWorld* world = CCoreEngine::Instance().GetPhysicsManager().getWorld();
					const s16 collidesWith = btBroadphaseProxy::DefaultFilter | btBroadphaseProxy::StaticFilter | btBroadphaseProxy::KinematicFilter;
					world->addCollisionObject(m_ghost.get(), group, collidesWith);
				}
				break;

				case COLLISION_DETECTION_MANIFOLDS:
				{
					// Enable filters for collision
					btBroadphaseProxy* bproxy = m_rigidBody->getBroadphaseHandle();

					if (bproxy)
					{
						bproxy->m_collisionFilterGroup = s16(group);
						const s16 collidesWith = btBroadphaseProxy::DefaultFilter | btBroadphaseProxy::StaticFilter | btBroadphaseProxy::KinematicFilter;
						bproxy->m_collisionFilterMask = collidesWith;
					}
				}
				break;

				case COLLISION_DETECTION_TRIGGER:
				{
					s32 bodyFlags = m_rigidBody->getCollisionFlags();
					m_rigidBody->setCollisionFlags(bodyFlags | btCollisionObject::CF_NO_CONTACT_RESPONSE | EdvCollisionFlags::CF_TRIGGER);

					// Enable filters for collision
					btBroadphaseProxy* bproxy = m_rigidBody->getBroadphaseHandle();

					if (bproxy)
					{
						bproxy->m_collisionFilterGroup = s16(group);
						const s16 collidesWith = btBroadphaseProxy::DefaultFilter | btBroadphaseProxy::KinematicFilter;
						bproxy->m_collisionFilterMask = collidesWith;
					}
				}
				break;

				default:
					break;
			}
		}
		else
		{
			throw std::runtime_error("attempt to use a nullptr worldObject");
		}
	}
}

void EntityBtPhysics::init(BtWorldObject* a_worldObject, const Config& a_config)
{
	m_worldObject = a_worldObject;
	m_mass = a_config.mass;
	m_magnetic_property.reset(new MagneticProperty(a_config.magnetic_config));
	m_physics2D = a_config.physics2D;

	createBody(a_config.collisionConfig, a_config.kinematic);
}

void EntityBtPhysics::update(f64 dt)
{
}

void EntityBtPhysics::clear()
{
	auto& physics = CCoreEngine::Instance().GetPhysicsManager();

	if (m_ghost)
	{
		physics.getWorld()->removeCollisionObject(m_ghost.get());
		m_ghost = nullptr;
	}

	if (m_rigidBody)
	{
		if (m_collisionConfig.shape == COLLISION_SHAPE_TRIANGLE_MESH)
		{
			if (btStridingMeshInterface* meshInterface = static_cast<btBvhTriangleMeshShape*>(m_bulletCollisionShape.get())->getMeshInterface())
			{
				delete meshInterface;
			}
		}

		physics.RemoveRigidBody(m_rigidBody.get());
		m_rigidBody = nullptr;
		m_bulletCollisionShape = nullptr;
	}

	m_worldObject = nullptr;
	m_mass = 0;
	m_magnetic_property.reset();
	m_physics2D = false;
}

EntityBtPhysics* EntityBtPhysics::clone(BtWorldObject* a_worldObject) const
{
	return new EntityBtPhysics{
		a_worldObject, 
		{ m_mass,
			isKinematic(),
			{ m_magnetic_property->getCharge(), m_magnetic_property->getForce(), m_magnetic_property->getScope() },
			m_physics2D,
			m_collisionConfig
		}
	};
}

edv::Vector3f EntityBtPhysics::getInitialTranslation() const
{
	return m_collisionConfig.translation;
}

Ogre::Quaternion EntityBtPhysics::getInitialRotation() const
{
	return m_collisionConfig.rotation;
}

void EntityBtPhysics::setScale(const edv::Vector3f& a_scale)
{
	if (m_rigidBody)
	{
		if (btCollisionShape* shape = m_rigidBody->getCollisionShape())
		{
			shape->setLocalScaling(a_scale.to_btVector3()/* / (m_collisionConfig.scale * m_initialScale)*/);
		}
	}
}

edv::Vector3f EntityBtPhysics::getScale() const
{
	edv::Vector3f scale;

	if (m_rigidBody)
	{
		scale = edv::Vector3f(m_rigidBody->getCollisionShape()->getLocalScaling());
	}

	return scale;
}

bool EntityBtPhysics::isKinematic() const
{
	bool kinematic = m_collisionConfig.shape == COLLISION_SHAPE_NONE;

	if (m_rigidBody)
	{
		kinematic = kinematic || (m_rigidBody->getCollisionFlags() & btCollisionObject::CF_KINEMATIC_OBJECT ? true : false);
	}

	return kinematic;
}

btRigidBody* EntityBtPhysics::getRigidBody()
{
	return m_rigidBody.get();
}

btScalar EntityBtPhysics::getMass() const
{
	return m_mass;
}

EntityBtPhysics::CollisionShape EntityBtPhysics::getCollisionShape() const
{
	return m_collisionConfig.shape;
}

EntityBtPhysics::CollisionDetection EntityBtPhysics::getCollisionDetectionMode() const
{
	return m_collisionConfig.detection;
}

MagneticProperty* EntityBtPhysics::getMagneticProperty()
{
	return m_magnetic_property.get();
}

bool EntityBtPhysics::has2DPhysics() const
{
	return m_physics2D;
}

btPairCachingGhostObject* EntityBtPhysics::getGhost() const
{
	btPairCachingGhostObject* ghost = nullptr;

	if (m_ghost)
	{
		ghost = m_ghost.get();
	}

	return ghost;
}

bool EntityBtPhysics::hasTriggerCollision() const
{
	return m_rigidBody && ( (m_rigidBody->getCollisionFlags() & EdvCollisionFlags::CF_TRIGGER) != 0);
}

bool EntityBtPhysics::isStatic() const
{
	return m_rigidBody && (m_rigidBody->getCollisionFlags() & btCollisionObject::CF_STATIC_OBJECT);
}

edv::Vector3f EntityBtPhysics::getHalfSize() const
{
	edv::Vector3f halfSize;	

	if (m_ghost || m_bulletCollisionShape)
	{
		btVector3 aabbMin, aabbMax;

		if (m_ghost)
		{
			m_ghost->getCollisionShape()->getAabb(m_ghost->getWorldTransform(), aabbMin, aabbMax);
		}
		else if (m_rigidBody && m_bulletCollisionShape)
		{
			m_bulletCollisionShape->getAabb(m_rigidBody->getWorldTransform(), aabbMin, aabbMax);			
		}

		halfSize = edv::Vector3f{ (aabbMax.x() - aabbMin.x()) / 2.f, (aabbMax.y() - aabbMin.y()) / 2.f, (aabbMax.z() - aabbMin.z()) / 2.f };
	}

	return halfSize;
}

void EntityBtPhysics::setCollisionEnabled(bool a_enabled)
{
	if (m_rigidBody)
	{
		s32 bodyFlags = m_rigidBody->getCollisionFlags();
		if (a_enabled)
		{
			m_rigidBody->setCollisionFlags(bodyFlags & ~btCollisionObject::CF_NO_CONTACT_RESPONSE);
		}
		else
		{
			m_rigidBody->setCollisionFlags(bodyFlags | btCollisionObject::CF_NO_CONTACT_RESPONSE);
		}
	}
}
