#include "Core/CCoreEngine.h"
#include "Physics/CPhysicsManager.h"
#include "Actor.h"
#include "GameEntity.h"
#include "DynamicMovement.h"
#include "Physics/btResultCallback.hpp"
#include "Renderer/CRenderDebug.h"

DynamicMovement::DynamicMovement() :
	ActorMovement{},
	m_on_ground{ false },
	m_on_ceiling{ false }
{

}

void DynamicMovement::init()
{

}

void DynamicMovement::applyMove(const edv::Vector3f& a_vector)
{
	// only kinematic movement...
}

void DynamicMovement::applyForce(const edv::Vector3f& a_vector)
{
	getRigidBody()->applyCentralForce(a_vector.to_btVector3());
}

void DynamicMovement::applyImpulse(const edv::Vector3f& a_vector)
{
	getRigidBody()->applyCentralImpulse(a_vector.to_btVector3());
}

void DynamicMovement::setGravity(const edv::Vector3f& a_vector)
{
	getRigidBody()->setGravity(a_vector.to_btVector3());
}

void DynamicMovement::setRestitution(f32 a_restitution)
{
	getRigidBody()->setRestitution(a_restitution);
}

void DynamicMovement::setSliding(f32 a_sliding)
{
	// only kinematic movement...
}

void DynamicMovement::setFriction(f32 a_friction)
{
	getRigidBody()->setFriction(a_friction);
}

void DynamicMovement::stepMove(f64 dt, GameCollisions& a_collisionInfo)
{
	// - Check on ground
	m_on_ground = checkOnGround();
	// - Check on ceiling
	m_on_ceiling = checkOnCeiling();
}

bool DynamicMovement::hasMovement()
{
	return (!getRigidBody()->getLinearVelocity().isZero());
}

void DynamicMovement::useGravity(bool a_use)
{
	edv::Vector3f gravity{ 0.f, 0.f, 0.f };
	if (a_use)
	{
		gravity = getRigidBody()->getGravity();
	}
	getRigidBody()->setGravity(gravity.to_btVector3());
}

btRigidBody* const DynamicMovement::getRigidBody()
{
	return getParent()->getParent()->getPhysics()->getRigidBody();
}

bool DynamicMovement::onGround() const
{
	return m_on_ground;
}

bool DynamicMovement::onCeiling() const
{
	return m_on_ceiling;
}

bool DynamicMovement::checkOnGround()
{
	bool returnValue{ false };
	GameEntity* entity = getParent()->getParent();
	if (btRigidBody* rigidBody = entity->getPhysics()->getRigidBody())
	{
		btTransform transform = rigidBody->getWorldTransform();
		btVector3 aabbMin, aabbMax;
		rigidBody->getCollisionShape()->getAabb(transform, aabbMin, aabbMax);
		btScalar height = aabbMax.y() - aabbMin.y();
		btScalar width = aabbMax.x() - aabbMin.x();

		// left point
		edv::Vector3f orig{ rigidBody->getWorldTransform().getOrigin().x() - width / 2.f, rigidBody->getWorldTransform().getOrigin().y(), rigidBody->getWorldTransform().getOrigin().z() };
		edv::Vector3f dest{ orig.x, orig.y - height / 2.f, orig.z };
		//CRenderDebug::getSingleton().drawLine(orig.to_OgreVector3(), dest.to_OgreVector3(), Ogre::ColourValue::Red);
		btClosestNotMeRayResultCallback callback{ entity, orig.to_btVector3(), dest.to_btVector3(), EntityBtPhysics::EdvCollisionFlags::CF_TRIGGER | btCollisionObject::CF_NO_CONTACT_RESPONSE };
		GameEntity* entity = static_cast<GameEntity*>(CCoreEngine::Instance().GetPhysicsManager().RayTestNotMe(orig, dest, callback));
		if (callback.hasHit())
		{
			returnValue = true;
		}

		// right point
		orig = edv::Vector3f(rigidBody->getWorldTransform().getOrigin().x() + width / 2.f, rigidBody->getWorldTransform().getOrigin().y(), rigidBody->getWorldTransform().getOrigin().z());
		dest = edv::Vector3f(orig.x, orig.y - height / 2.f, orig.z);
		//CRenderDebug::getSingleton().drawLine(orig.to_OgreVector3(), dest.to_OgreVector3(), Ogre::ColourValue::Red);
		callback = { entity, orig.to_btVector3(), dest.to_btVector3(), EntityBtPhysics::EdvCollisionFlags::CF_TRIGGER | btCollisionObject::CF_NO_CONTACT_RESPONSE };
		entity = static_cast<GameEntity*>(CCoreEngine::Instance().GetPhysicsManager().RayTestNotMe(orig, dest, callback));
		if (callback.hasHit())
		{
			returnValue |= true;
		}
	}
	return returnValue;
}

bool DynamicMovement::checkOnCeiling()
{
	bool returnValue{ false };
	GameEntity* entity = getParent()->getParent();
	if (btRigidBody* rigidBody = entity->getPhysics()->getRigidBody())
	{
		btTransform transform = rigidBody->getWorldTransform();
		btVector3 aabbMin, aabbMax;
		rigidBody->getCollisionShape()->getAabb(transform, aabbMin, aabbMax);
		btScalar height = aabbMax.y() - aabbMin.y();
		btScalar width = aabbMax.x() - aabbMin.x();

		edv::Vector3f orig{ rigidBody->getWorldTransform().getOrigin().x(), rigidBody->getWorldTransform().getOrigin().y(), rigidBody->getWorldTransform().getOrigin().z() };
		edv::Vector3f dest{ orig.x, orig.y + height / 2.f, orig.z };
		//CRenderDebug::getSingleton().drawLine(orig.to_OgreVector3(), dest.to_OgreVector3(), Ogre::ColourValue::Red);
		btClosestNotMeRayResultCallback callback{ entity, orig.to_btVector3(), dest.to_btVector3(), EntityBtPhysics::EdvCollisionFlags::CF_TRIGGER | btCollisionObject::CF_NO_CONTACT_RESPONSE };
		GameEntity* entity = static_cast<GameEntity*>(CCoreEngine::Instance().GetPhysicsManager().RayTestNotMe(orig, dest, callback));
		if (callback.hasHit() && entity != getParent()->getParent())
		{
			returnValue = true;
		}
	}
	return returnValue;
}

edv::Vector3f DynamicMovement::getVelocity()
{
	return edv::Vector3f(getRigidBody()->getLinearVelocity());
}

void DynamicMovement::resetVelocity()
{
	getRigidBody()->setLinearVelocity({0.f, 0.f, 0.f});
}