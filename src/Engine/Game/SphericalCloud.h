#ifndef EDV_SPHERICAL_CLOUD_H_
#define EDV_SPHERICAL_CLOUD_H_

#include "Utils/edvVector3.h"
#include "OGRE/OgreTexture.h"

namespace Ogre
{
	class SimpleRenderable;
	class SceneNode;
	class Vector3;
};

class SphericalCloud
{
public:

	struct QuadsConfig
	{
		f32 radius;
		u32 count;
		f32 qsize;
		std::string materialName;

		QuadsConfig(f32 a_radius, u32 a_count, f32 a_qsize, const std::string& a_materialName) :
			radius{ a_radius }, count{ a_count }, qsize{ a_qsize }, materialName{ a_materialName }
		{}
	};
	
	struct VolumeConfig
	{
		u32 slices;
		f32 size;

		VolumeConfig(u32 a_slices, f32 a_size) :
			slices{ a_slices }, size{ a_size }
		{}
	};

	struct JuliaFractalConfig
	{
		f32					real;
		f32					imag;
		f32					theta;

		JuliaFractalConfig(f32 a_real, f32 a_imag, f32 a_theta) :
			real{ a_real }, imag{ a_imag }, theta{ a_theta }
		{}
	};

	struct Config
	{
		std::string			textureName;
		QuadsConfig			quadConfig;
		VolumeConfig		volumeConfig;
		JuliaFractalConfig	fractalConfig;
		edv::Vector3f		colorAddition;
		edv::Vector3f		colorFactor;
		Ogre::SceneNode*	parent;
		const Ogre::Vector3& initialPosition;

		Config(const std::string& a_textureName, const QuadsConfig& a_quadConfig, const VolumeConfig& a_volumeCsonfig, const JuliaFractalConfig& a_fractalConfig,
			const edv::Vector3f& a_colorAddition, const edv::Vector3f& a_colorFactor, Ogre::SceneNode* a_parent, const Ogre::Vector3& a_position = Ogre::Vector3::ZERO) :
			textureName{ a_textureName }, quadConfig{ a_quadConfig }, volumeConfig{ a_volumeCsonfig }, fractalConfig{ a_fractalConfig }, colorAddition{ a_colorAddition },
			colorFactor{ a_colorFactor }, parent{ a_parent }, initialPosition{ a_position }
		{}
	};

	SphericalCloud(const Config& a_config);
	~SphericalCloud();

	void modifyReal(f32 a_real);
	void modifyImag(f32 a_imag);
	void modifyTheta(f32 a_theta);
	void modifyColorAddition(const edv::Vector3f& a_vector);
	void modifyColorFactor(const edv::Vector3f& a_vector);
	Ogre::SceneNode* getSceneNode();
	void setDevelopment(bool a_development);
	void update(f64 dt);

private:
	void generate();
	
	const std::string mTextureName;
	Ogre::SceneNode* mNode;
	f32 mReal;
	f32 mImag;
	f32 mTheta;
	Ogre::TexturePtr mTexPtr;
	Ogre::SimpleRenderable* mVRenderable;
	Ogre::SimpleRenderable* mTRenderable;
	bool mDevelopment;
	edv::Vector3f mColorAddition;
	edv::Vector3f mColorFactor;
};
#endif