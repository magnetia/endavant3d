#ifndef EDV_GAME_LUA_BEHAVIOR_H_
#define EDV_GAME_LUA_BEHAVIOR_H_

#include "Script/ScriptConfig.h"
#include "Behavior.h"

class LuaBehavior : public Behavior
{
public:
	// Factory
	class Factory : public Behavior::factory_type
	{
	public:
		Behavior* createElement(const Behavior::factory_param_type& a_config) override;
	};

	LuaBehavior(const ScriptConfig& a_config = {});
	~LuaBehavior();
	Behavior* clone() override;

private:
	void initImpl() override;
	void stopImpl() override;
	void updateImpl(f64 dt) override;
	void pauseImpl() override;
	void resumeImpl() override;

	ScriptConfig m_scriptConfig;
};

#endif
