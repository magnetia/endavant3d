#ifndef EDV_GAME_CEL_SHADER_H_
#define EDV_GAME_CEL_SHADER_H_

#include <string>
#include "Utils/edvVector3.h"
#include "CustomMaterial.h"

class CelShader : public CustomMaterial
{
public:
	struct Config
	{
		std::string		cloneName;
		Ogre::Vector4	shininess;
		Ogre::Vector4	diffuse;
		Ogre::Vector4	specular;

		Config(const std::string& a_cloneName = "", const Ogre::Vector4& a_shininess = {35, 0, 0, 0},
			const Ogre::Vector4& a_diffuse = { 1, 1, 1, 1 }, const Ogre::Vector4& a_specular = { 1, 1, 1, 1 }) :
			cloneName{ a_cloneName }, shininess{ a_shininess }, diffuse{ a_diffuse }, specular{ a_specular } { }
		Config(const Config& a_other) :
			cloneName{ a_other.cloneName }, shininess{ a_other.shininess }, diffuse{ a_other.diffuse }, specular{ a_other.specular } { }
	};

	class Factory : public CustomMaterial::factory_type
	{
	public:
		CustomMaterial* createElement(const CustomMaterial::factory_param_type& a_config) override;
	};

	CelShader(const Config& a_config);
	~CelShader();

	void init(Ogre::Entity* a_entity = nullptr) override;
	void update(f64 dt) override { }
	void remove() override;

private:
	Config		m_config;
};

#endif
