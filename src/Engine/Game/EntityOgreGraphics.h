#ifndef EDV_GAME_ENTITY_OGRE_GRAPHICS_H_
#define EDV_GAME_ENTITY_OGRE_GRAPHICS_H_

#include <memory>
#include <unordered_map>
#include <OGRE/Ogre.h>
#include "Core/CBasicTypes.h"
#include "Physics/BtWorldObject.h"
#include "Utils/edvVector3.h"
#include "Utils/IdGenerator.h"
#include "CustomMaterial.h"
#include "Renderer/AnimationBlender.h"

class LuaSceneNode;

class EntityOgreGraphics : public BtWorldObject
{
public:
	enum LightType
	{
		LIGHT_TYPE_POINT = Ogre::Light::LT_POINT,
		LIGHT_TYPE_DIRECTIONAL = Ogre::Light::LT_DIRECTIONAL,
		LIGHT_TYPE_SPOTLIGHT = Ogre::Light::LT_SPOTLIGHT,

		LIGHT_TYPE_NONE = 0xFF,
	};

	struct AnimationConfig
	{
		std::string name;
		bool		enabled;
		f32			speed;
		bool		loop;

		AnimationConfig(const std::string& a_name = "", bool a_enabled = false, f32 a_speed = 1.f, bool a_loop = false) :
			name{ a_name }, enabled{ a_enabled }, speed{ a_speed }, loop{ a_loop } { }
	};

	struct ParticleConfig
	{
		std::string		name;
		std::string		system;
		std::string		bone;
		edv::Vector3f	translation;
		edv::Vector3f	scale;
		bool			attachToParent;
		bool			positionSync;

		ParticleConfig(const std::string& a_name = "", const std::string& a_system = "", const std::string& a_bone = "", 
			const edv::Vector3f& a_translation = {}, const edv::Vector3f& a_scale = {1.f, 1.f, 1.f}, bool a_attachToParent = false, bool a_positionSync = true) :
			name{ a_name }, system{ a_system }, bone{ a_bone }, translation{ a_translation }, scale{ a_scale },
			attachToParent{ a_attachToParent }, positionSync{ a_positionSync }
		{ }
	};

	struct LightConfig
	{
		LightType			type;
		std::string			name;
		Ogre::ColourValue	diffuse;
		Ogre::ColourValue	specular;
		edv::Vector3f		direction;

		LightConfig(LightType a_type, const std::string& a_name, const Ogre::ColourValue& a_diffuse = Ogre::ColourValue{ 1.f, 1.f, 1.f, 1.f },
			const Ogre::ColourValue& a_specular = Ogre::ColourValue{ 1.f, 1.f, 1.f, 1.f }, const edv::Vector3f& a_direction = {}) :
				type{ a_type }, name{ a_name }, diffuse{ a_diffuse }, specular{ a_specular }, direction{ a_direction } {}
	};

	struct Config
	{
		std::string						nodeName;
		Ogre::Vector3					initialPos;
		Ogre::Quaternion				initialRot;
		bool							load;
		std::string						filePath;
		Ogre::SceneNode*				parent;
		std::string						material;
		Ogre::Vector3					scale;
		std::vector<AnimationConfig>	animations;
		std::vector<ParticleConfig>		particles;
		std::vector<CustomMaterial*>	customMaterials;
		std::vector<LightConfig>		lights;

		Config(	const std::string& a_nodeName = "",
				const Ogre::Vector3& a_initialPos = Ogre::Vector3::ZERO,
				const Ogre::Quaternion& a_initialRot = Ogre::Quaternion::IDENTITY,
				bool a_load = false,
				const std::string& a_filePath = "",
				Ogre::SceneNode* a_parent = nullptr,
				const std::string& a_material = "",
				const Ogre::Vector3& a_scale = Ogre::Vector3{ 1.f, 1.f, 1.f },
				const std::vector<AnimationConfig>& a_animations = std::vector<AnimationConfig>{},
				const std::vector<ParticleConfig>& a_particles = {},
				const std::vector<CustomMaterial*>& a_customMaterials = {},
				const std::vector<LightConfig>& a_lights = {}) :
					nodeName{ a_nodeName }, initialPos{ a_initialPos }, initialRot{ a_initialRot }, 
					load{ a_load }, filePath{ a_filePath }, parent{ a_parent }, material{a_material},
					scale{ a_scale }, animations{ a_animations }, particles{ a_particles },
					customMaterials{ a_customMaterials }, lights{ a_lights } { }
		bool IsInitialized() const{ return !nodeName.empty(); }
	};

	EntityOgreGraphics();
	EntityOgreGraphics(const Config& a_config);
	~EntityOgreGraphics();

	void init(const Config& a_config);
	void update(f64 dt);
	void clear();
	void stopAllAnimations();
	void removeAllCustomMaterials();

	Ogre::SceneNode* getNode() const;
	Ogre::Entity* getMainEntity() const;
	Ogre::AnimationState* getAnimation(const std::string& a_animationName);
	void setAnimationSpeed(const std::string& a_animationName, f32 a_speed);
	f32 getAnimationSpeed(const std::string& a_animationName) const;
	void blendAnimations(const std::string& a_animationSourceName, const std::string& a_animationTargetName, AnimationBlender::BlendingTransition a_transition, f32 a_duration);
	void stopBlendAnimations();
	LuaSceneNode getLuaNode();
	Ogre::Vector3 getScale() const;
	void setScale(const Ogre::Vector3& a_scale);
	EntityOgreGraphics* clone(const std::string& a_newName, const Ogre::Vector3& a_newPos = Ogre::Vector3::ZERO,
		const Ogre::Quaternion& a_newRot = Ogre::Quaternion::IDENTITY) const;

	btVector3 getPosition() const override;
	btQuaternion getOrientation() const override;
	btVector3 getWorldPosition() const override;
	btQuaternion getWorldOrientation() const override;
	std::vector<btVector3> getVertices() const override;
	btVector3 getHalfSize() const override;
	btVector3 btGetScale() const override;
	btStridingMeshInterface* createStridingMeshInterface() override;
	btMotionState* createMotionState(const btVector3& a_translation, const btQuaternion& a_rotation, bool a_kinematic = false) override;
	BtWorldObject* createWorldObject(void* a_param) override;
	std::string getObjectId() const override;
	Ogre::SceneNode* getParticleSystemNode(const std::string& a_particleId) const;
	Ogre::ParticleSystem* getParticleSystem(const std::string& a_particleId) const;
	void addParticleSystem(const std::string& a_name, const ParticleConfig& a_particleConfig);
	void removeParticleSystem(const std::string& a_name);
	void removeAllParticleSystems();
	void addLight(LightType a_type, const std::string& a_name, const Ogre::ColourValue& a_diffuse, const Ogre::ColourValue& a_specular,
		const edv::Vector3f& a_direction = { 0.f, 0.f, 1.f});
	void removeLight(const std::string& a_name);
	void removeAllLights();

	static Ogre::Bone* getBone(Ogre::Entity* a_entity, const std::string& a_boneName);
	static Ogre::Vector3 getBoneWorldPosition(Ogre::Entity* a_entity, Ogre::Bone* a_boneName);
	static LightType stringToLightType(const std::string& a_lightTypeStr);

private:
	struct Animation
	{
		Ogre::AnimationState*	state;
		f32						speed;

		Animation(Ogre::AnimationState* a_animation = nullptr, f32 a_speed = 1.f) :
			state{ a_animation }, speed{ a_speed } { }
	};

	struct Particle
	{
		Ogre::SceneNode*		node;
		Ogre::ParticleSystem*	system;
		Ogre::Bone*				bone;
		Ogre::Vector3			translation;
		bool					attachToParent;
		bool					positionSync;

		Particle(Ogre::SceneNode* a_node = nullptr, Ogre::ParticleSystem* a_system = nullptr, Ogre::Bone* a_bone = nullptr, 
			const Ogre::Vector3& a_translation = Ogre::Vector3::ZERO, bool a_attachToParent = false, bool a_positionSync = true) :
			node{ a_node }, system{ a_system }, bone{ a_bone }, translation{ a_translation }, attachToParent{ a_attachToParent }, positionSync{ a_positionSync }
		{}
	};

	typedef std::unordered_map<std::string, Animation>	Animations;
	typedef std::unordered_map<std::string, Particle>	Particles;
	typedef std::unique_ptr<CustomMaterial>				CustomMaterialPtr;
	typedef std::list<CustomMaterialPtr>				CustomMaterials;
	typedef std::unordered_map<std::string, Ogre::Light*> Lights;
	typedef std::unique_ptr<AnimationBlender>			tAnimationBlenderPtr;

	void removeParticleSystem(Particle& a_particle);
	void removeLight(Ogre::Light* a_light);

	Ogre::SceneNode*		m_node;
	Ogre::Entity*			m_entity;
	bool					m_load;
	Animations				m_animations;
	Particles				m_particles;
	CustomMaterials			m_customMaterials;
	Lights					m_lights;
	tAnimationBlenderPtr	m_animationBlending;
};

#endif
