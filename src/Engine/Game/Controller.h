#ifndef EDV_GAME_CAMERA_CONTROLLER_H_
#define EDV_GAME_CAMERA_CONTROLLER_H_

#include <vector>
#include "OGRE/Ogre.h"
#include "Behavior.h"

static const char DEFAULT_ACTION_LEFT[]		= "CONTROLLER_DEFAULT_LEFT";
static const char DEFAULT_ACTION_RIGHT[]	= "CONTROLLER_DEFAULT_RIGHT";
static const char DEFAULT_ACTION_UP[]		= "CONTROLLER_DEFAULT_UP";
static const char DEFAULT_ACTION_DOWN[]		= "CONTROLLER_DEFAULT_DOWN";
static const char DEFAULT_ACTION_JUMP[]		= "CONTROLLER_DEFAULT_JUMP";
static const char DEFAULT_ACTION_CROUCH[]	= "CONTROLLER_DEFAULT_CROUCH";

class Controller : public Behavior
{
public:
	// Factory
	class Factory : public Behavior::factory_type
	{
	public:
		Behavior* createElement(const Behavior::factory_param_type& a_config) override;
	};

	struct Action
	{
		std::string name;
		std::string key;
		Ogre::Vector3 vect;
		std::string gcButtName;
		Action(const std::string& a_name, const std::string& a_key, const Ogre::Vector3& a_vect, const std::string &a_gcbuttonname = "") :
			name(a_name), key(a_key), vect(a_vect), gcButtName(a_gcbuttonname)
		{
		}
	};

	typedef std::vector<Action> Actions;

	Controller(const Actions& a_actions = {
		{ DEFAULT_ACTION_RIGHT,	"K", { 1.0, 0.0, 0.0 } },
		{ DEFAULT_ACTION_LEFT,	"H", { -1.0, 0.0, 0.0 } },
		{ DEFAULT_ACTION_UP,	"U", { 0.0, 0.0, -1.0 } },
		{ DEFAULT_ACTION_DOWN,	"J", { 0.0, 0.0, 1.0 } },
		{ DEFAULT_ACTION_JUMP,	"N", { 0.0, 1.0, 0.0 } },
		{ DEFAULT_ACTION_CROUCH, "M", { 0.0, -1.0, 0.0 } }}
		);

	Behavior* clone() override;

private:
	void initImpl() override;
	void stopImpl() override;
	void updateImpl(f64 dt) override;
	void pauseImpl() override;
	void resumeImpl() override;
	void addActions();
	void removeActions();

	Actions m_actions;
};

#endif
