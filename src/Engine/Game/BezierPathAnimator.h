#ifndef EDV_GUI_BEZIER_ANIMATOR_H_
#define EDV_GUI_BEZIER_ANIMATOR_H_

#include "Game/TimedBehavior.h"
#include "OGRE/Ogre.h"
#include <initializer_list>
#include "Utils\CConversions.h"

class BezierPath
{
public:
	BezierPath(const Ogre::Vector3 &_p0, const Ogre::Vector3 &_p1, const Ogre::Vector3 &_p2, const Ogre::Vector3 &_p3, f64 _duration) :
		m_p0{ _p0 }, m_p1{ _p1 }, m_p2{ _p2 }, m_p3{ _p3 }, m_duration{ _duration }{}
	
	Ogre::Vector3	GetBezierPosition(f64 t)
	{
		if (t > 1.0 || t < 0.0)
			throw std::runtime_error("value " + to_string(t) + " must be [1,0]");

		Ogre::Vector3 pos;
		const f64 dtt = 1.0 - t;
		const f64 tp0 = std::pow(dtt, 3.0);
		const f64 tp1 = 3 * std::pow(dtt, 2.0) * t;
		const f64 tp2 = 3 * dtt * std::pow(t, 2.0);
		const f64 tp3 = std::pow(t, 3.0);

		pos.x = static_cast<Ogre::Real>((tp0 * m_p0.x) + (tp1 * m_p1.x) + (tp2 * m_p2.x) + (tp3 * m_p3.x));
		pos.y = static_cast<Ogre::Real>((tp0 * m_p0.y) + (tp1 * m_p1.y) + (tp2 * m_p2.y) + (tp3 * m_p3.y));
		pos.z = static_cast<Ogre::Real>((tp0 * m_p0.z) + (tp1 * m_p1.z) + (tp2 * m_p2.z) + (tp3 * m_p3.z));

		return pos;
	}

	f64 m_duration;

private:

	Ogre::Vector3	m_p0;
	Ogre::Vector3	m_p1;
	Ogre::Vector3	m_p2;
	Ogre::Vector3	m_p3;
	

};

class BezierPathAnimator : public TimedBehavior
{
public:
	BezierPathAnimator(std::initializer_list<BezierPath> _bezierpathlist);
	
	Behavior* clone() override;
	
private:
	void initImpl() override;
	void stopImpl() override;
	void updateImpl(f64 dt) override;

	std::vector<BezierPath>	m_bezierpaths;
	
};

#endif
