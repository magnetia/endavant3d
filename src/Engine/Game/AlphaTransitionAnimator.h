#ifndef EDV_GAME_ALPHATRANSITIONANIMATOR_H_
#define EDV_GAME_ALPHATRANSITIONANIMATOR_H_

#include <vector>
#include <string>
#include "Game/TimedBehavior.h"
#include "Externals/OGRE/Ogre.h"

class AlphaTransitionAnimator : public TimedBehavior
{
public:
	AlphaTransitionAnimator(f64 a_duration = 0, f32 a_beginLevel = 0, f32 a_endLevel = 1);
	~AlphaTransitionAnimator();

	Behavior* clone() override;

private:
	typedef std::vector<Ogre::Material*> Materials;

	void initImpl() override;
	void stopImpl() override;
	void updateImpl(f64 dt) override;

	f32			m_beginLevel;
	f32			m_endLevel;
	f32			m_increment;
	Materials	m_materials;
	Materials	m_originalMat;
};

#endif
