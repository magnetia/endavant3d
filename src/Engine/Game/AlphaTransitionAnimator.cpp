#include <algorithm>
#include "OGRE/OgreMaterialManager.h"
#include "GameEntity.h"
#include "AlphaTransitionAnimator.h"

AlphaTransitionAnimator::AlphaTransitionAnimator(f64 a_duration, f32 a_beginLevel, f32 a_endLevel) :
	TimedBehavior{ a_duration },
	m_beginLevel{ a_beginLevel },
	m_endLevel{ a_endLevel },
	m_increment{ [&]()
	{
		const f32 abs_bl = fabs(a_beginLevel);
		const f32 abs_el = fabs(a_endLevel);
		const f32 increment = (std::max(abs_bl, abs_el) - std::min(abs_bl, abs_el)) / static_cast<f32>(a_duration);

		return (abs_bl > abs_el? -1 : 1) * increment;
	}()}
{

}

AlphaTransitionAnimator::~AlphaTransitionAnimator()
{

}

Behavior* AlphaTransitionAnimator::clone()
{
	return new AlphaTransitionAnimator{ duration(), m_beginLevel, m_endLevel };
}

void AlphaTransitionAnimator::initImpl()
{
	TimedBehavior::initImpl();

	auto* graphics = getParent()->getGraphics();
	graphics->removeAllCustomMaterials();

	auto* node = graphics->getNode();
	auto it = node->getAttachedObjectIterator();

	while (it.hasMoreElements())
	{
		Ogre::Entity* const entity = static_cast<Ogre::Entity*>(it.getNext());
		u32 subEntities = entity->getNumSubEntities();

		for (u32 se_idx = 0; se_idx < subEntities; se_idx++)
		{
			Ogre::SubEntity* const subEntity = entity->getSubEntity(se_idx);
			m_originalMat.push_back(subEntity->getMaterial().get());

			const Ogre::MaterialPtr& material = 
				subEntity->getMaterial()->clone(subEntity->getMaterialName() + node->getName());
			subEntity->setMaterial(material);
			m_materials.push_back(material.get());

			// set initial value
			u32 techniques = material->getNumTechniques();

			for (u32 tec_idx = 0; tec_idx < techniques; tec_idx++)
			{
				Ogre::Technique* technique = material->getTechnique(tec_idx);
				u32 passes = technique->getNumPasses();

				for (u32 pass_idx = 0; pass_idx < passes; pass_idx++)
				{
					Ogre::Pass* pass = technique->getPass(pass_idx);
					Ogre::ColourValue color = pass->getDiffuse();

					color.a = m_beginLevel;
					pass->setDiffuse(color);;

					pass->setSceneBlending(Ogre::SceneBlendType::SBT_TRANSPARENT_ALPHA);
				}
			}
		}
	}
}

void AlphaTransitionAnimator::stopImpl()
{
	auto it = getParent()->getGraphics()->getNode()->getAttachedObjectIterator();
	auto it_orig = m_originalMat.begin();

	// reset material properties
	while (it.hasMoreElements())
	{
		Ogre::Entity* const entity = static_cast<Ogre::Entity*>(it.getNext());
		u32 subEntities = entity->getNumSubEntities();

		for (u32 idx = 0; idx < subEntities; idx++)
		{
			Ogre::SubEntity* const subEntity = entity->getSubEntity(idx);
			const Ogre::String matName = subEntity->getMaterialName();

			subEntity->setMaterialName((*it_orig)->getName());
			Ogre::MaterialManager::getSingleton().remove(matName);

			it_orig++;
		}
	}

	m_materials.clear();
	m_originalMat.clear();
}

void AlphaTransitionAnimator::updateImpl(f64 dt)
{
	if (!finished())
	{
		f64 phase = fmod(elapsed(), duration());

		for (u32 mat_idx = 0; mat_idx < m_materials.size(); mat_idx++)
		{
			Ogre::Material* material = m_materials[mat_idx];
			u32 techniques = material->getNumTechniques();

			for (u32 tec_idx = 0; tec_idx < techniques; tec_idx++)
			{
				Ogre::Technique* technique = material->getTechnique(tec_idx);
				u32 passes = technique->getNumPasses();

				// #todo: no tinc clar que s'hagi de fer per totes les passades, pero
				// com en trio una?
				for (u32 pass_idx = 0; pass_idx < passes; pass_idx++)
				{
					Ogre::Pass* pass = technique->getPass(pass_idx);
					Ogre::ColourValue color = pass->getDiffuse();

					color.a = m_beginLevel + (static_cast<f32>(phase) * m_increment);
					pass->setDiffuse(color);
				}
			}
		}
	}
}
