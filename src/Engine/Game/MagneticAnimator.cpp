#include <cmath>
#include "Core/CCoreEngine.h"
#include "Time/CTimeManager.h"
#include "Game/GameEntity.h"
#include "MagneticAnimator.h"

Behavior* MagneticAnimator::Factory::createElement(const Behavior::factory_param_type& a_config)
{
	return new MagneticAnimator{
		Config {
			static_cast<MagneticProperty:: eMagneticCharge>(a_config["initialCharge"].as<u32>()), a_config["positiveDuration"].as<f64>(), 
			a_config["negativeDuration"].as<f64>(), a_config["neutralDuration"].as<f64>(), a_config["loop"].as<bool>()
		}
	};
}

MagneticAnimator::MagneticAnimator(const Config& a_config) :
	TimedBehavior{ 0 },
	m_initialCharge{ static_cast < u8 > (a_config.initialMagneticCharge) },
	m_currentCharge{ m_initialCharge },
	m_duration_by_charge{ { a_config.positiveDuration, a_config.negativeDuration, a_config.neutralDuration } },
	m_loop{ a_config.loop}
{
}


Behavior* MagneticAnimator::clone()
{
	return new MagneticAnimator{ 
		Config{ 
			static_cast<MagneticProperty::eMagneticCharge>(m_initialCharge), 
			m_duration_by_charge[MagneticProperty::MAGNET_TYPE_NEUTRAL],
			m_duration_by_charge[MagneticProperty::MAGNET_TYPE_NEGATIVE] ,
			m_duration_by_charge[MagneticProperty::MAGNET_TYPE_POSITIVE],
			m_loop
		}
	};
}

void MagneticAnimator::initImpl()
{
	reset();
	TimedBehavior::initImpl();
}

void MagneticAnimator::updateImpl(f64 dt)
{
	if (finished())
	{
		m_currentCharge = ((m_currentCharge + 1) % static_cast<u8>(MagneticProperty::MAGNET_TYPE_TOTAL));
		
		const MagneticProperty::eMagneticCharge currentCharge = static_cast<MagneticProperty::eMagneticCharge>(m_currentCharge);
		setMagneticCharge(currentCharge);
		
		if (m_currentCharge == m_initialCharge && !m_loop)
		{
			stopImpl();
		}
		else
		{
			setDurationByCharge(currentCharge);
			TimedBehavior::initImpl();
		}
	}
}

void MagneticAnimator::stopImpl()
{
}

void MagneticAnimator::reset()
{
	const MagneticProperty::eMagneticCharge initialCharge = static_cast<MagneticProperty::eMagneticCharge>(m_initialCharge);
	setMagneticCharge(initialCharge);
	setDurationByCharge(initialCharge);
}

void MagneticAnimator::setMagneticCharge(MagneticProperty::eMagneticCharge a_charge)
{
	if (auto* magneticProperty = getParent()->getPhysics()->getMagneticProperty())
	{
		magneticProperty->setCharge(a_charge);
	}
}

void MagneticAnimator::setDurationByCharge(MagneticProperty::eMagneticCharge a_charge)
{
	m_duration = m_duration_by_charge[a_charge];
}

