#ifndef EDV_GAME_COLLISION_INFO_H_
#define EDV_GAME_COLLISION_INFO_H_

#include <vector>
#include "Utils/edvVector3.h"

class GameEntity;

struct CollisionInfo
{
	GameEntity*		target;
	edv::Vector3f	hitPoint;
	edv::Vector3f	hitNormal;

	CollisionInfo(GameEntity* a_target = nullptr, const edv::Vector3f& a_hitPoint = {}, const edv::Vector3f& a_hitNormal = {}) :
		target{ a_target }, hitPoint{ a_hitPoint }, hitNormal{ a_hitNormal } { }

	bool operator==(const CollisionInfo& a_other)
	{
		return target == a_other.target;
	}

	bool empty() const
	{
		return target != nullptr;
	}
};

typedef std::vector<CollisionInfo>	GameCollisions;

#endif
