#ifndef EDV_GAME_DISTANCE_SOUND_H_
#define EDV_GAME_DISTANCE_SOUND_H_

#include "Sound/CSoundManager.h"
#include "Behavior.h"

class DistanceSound : public Behavior
{
public:
	class Factory : public Behavior::factory_type
	{
	public:
		Behavior* createElement(const Behavior::factory_param_type& a_config) override;
	};

	DistanceSound(const std::string& a_soundId, f32 a_range);
	~DistanceSound();

	Behavior* clone() override final;

	bool isActive() const;

private:
	void initImpl() override;
	void stopImpl() override;
	void updateImpl(f64 dt) override;
	void pauseImpl() override;
	void resumeImpl() override;

	std::string					m_soundId;
	f32							m_range;
	CSoundManager::t_ChannelId	m_channelId;
	f32							m_defaultVolume;
	bool						m_active;
};

#endif
