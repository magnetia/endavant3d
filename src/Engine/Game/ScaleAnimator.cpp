#include "ScaleAnimator.h"

#include <cmath>
#include "Core/CCoreEngine.h"
#include "Time/CTimeManager.h"
#include "Game/GameEntity.h"
#include "ScaleAnimator.h"

ScaleAnimator::ScaleAnimator(f64 a_duration, const edv::Vector3f& a_initialScale, const edv::Vector3f& a_finalScale) :
	TimedBehavior{ a_duration },
	m_timeFactor{ 0.f, 0.f, 0.f },
	m_initialScale{ a_initialScale },
	m_finalScale{ a_finalScale }
{

}

Behavior* ScaleAnimator::clone()
{
	return new ScaleAnimator{ duration(), m_initialScale, m_finalScale };
}

const edv::Vector3f& ScaleAnimator::getTimeFactor() const
{
	return m_timeFactor;
}

const edv::Vector3f& ScaleAnimator::getInitialScale() const
{
	return m_initialScale;
}

const edv::Vector3f& ScaleAnimator::getFinalScale() const
{
	return m_finalScale;
}

void ScaleAnimator::initImpl()
{
	TimedBehavior::initImpl();
	reset();
}

void ScaleAnimator::updateImpl(f64 dt)
{
	if (finished())
	{
		getParent()->setScale(m_finalScale);
	}
	else
	{
		edv::Vector3f rel = m_timeFactor * static_cast<f32>(fmod(elapsed(), duration()));
		getParent()->setScale(m_initialScale + rel);
	}
}

void ScaleAnimator::stopImpl()
{
	
}

void ScaleAnimator::reset()
{
	if (m_duration)
	{
		const f32 duration = static_cast<f32>(m_duration);

		const f32 abs_ix = fabs(m_initialScale.x), abs_fx = fabs(m_finalScale.x);
		const f32 abs_iy = fabs(m_initialScale.y), abs_fy = fabs(m_finalScale.y);
		const f32 abs_iz = fabs(m_initialScale.z), abs_fz = fabs(m_finalScale.z);

		m_timeFactor = edv::Vector3f{
			(std::max(abs_ix, abs_fx) - std::min(abs_ix, abs_fx)) / duration,
			(std::max(abs_iy, abs_fy) - std::min(abs_iy, abs_fy)) / duration,
			(std::max(abs_iz, abs_fz) - std::min(abs_iz, abs_fz)) / duration
		};

		getParent()->setScale(m_initialScale);
	}
}

