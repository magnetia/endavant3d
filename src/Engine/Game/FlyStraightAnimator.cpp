#include <cmath>
#include "Core/CCoreEngine.h"
#include "Time/CTimeManager.h"
#include "Game/GameEntity.h"
#include "FlyStraightAnimator.h"

FlyStraightAnimator::FlyStraightAnimator(f64 a_duration, const Ogre::Vector3& a_orig, const Ogre::Vector3& a_dest) :
	TimedBehavior{ a_duration },
	m_timeFactor{ 0 },
	m_orig{ a_orig },
	m_dest{ a_dest },
	m_vector{}
{

}

FlyStraightAnimator::FlyStraightAnimator(f64 a_duration, const edv::Vector3f& a_orig, const edv::Vector3f& a_dest) :
	FlyStraightAnimator{ a_duration, a_orig.to_OgreVector3(), a_dest.to_OgreVector3() }
{

}

FlyStraightAnimator& FlyStraightAnimator::operator+=(const Ogre::Vector3& a_vect)
{
	m_dest += a_vect;
	reset();

	return *this;
}

Behavior* FlyStraightAnimator::clone()
{
	return new FlyStraightAnimator{ duration(), m_orig, m_dest };
}

void FlyStraightAnimator::initImpl()
{
	TimedBehavior::initImpl();
	reset();
}

void FlyStraightAnimator::updateImpl(f64 dt)
{
	auto* const node = getParent()->getGraphics()->getNode();

	if (finished())
	{
		node->_setDerivedPosition(m_dest);
	}
	else
	{
		f64 phase = fmod(elapsed(), duration());
		Ogre::Vector3 rel = m_vector * static_cast<f32>(phase * m_timeFactor);
		node->_setDerivedPosition(m_orig + rel);
	}
}

void FlyStraightAnimator::stopImpl()
{

}

void FlyStraightAnimator::reset()
{
	getParent()->getGraphics()->getNode()->_setDerivedPosition(m_orig);
	m_vector = m_dest - m_orig;
	m_timeFactor = m_vector.length() / m_duration;
	m_vector.normalise();
}
