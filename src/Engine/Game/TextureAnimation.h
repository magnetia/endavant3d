#ifndef EDV_GAME_TEXTURE_ANIMATION_H_
#define EDV_GAME_TEXTURE_ANIMATION_H_

#include "Core/CBasicTypes.h"
#include <string>

namespace Ogre
{
	class TextureUnitState;
}

class TextureAnimation
{
public:
	struct Config
	{
		f64 duration;
		std::string materialName;
		bool		loop;

		Config(f64 a_duration = 0., const std::string& a_materialName = "", bool a_loop = true) :
			duration{ a_duration }, materialName{ a_materialName }, loop{ a_loop }
		{}

		bool IsInitialized() const
		{
			return (duration != 0. && !materialName.empty());
		}
	};
	
	TextureAnimation(f64 a_duration, const std::string& a_materialName, bool a_loop);
	TextureAnimation(const Config& a_config);

	void start();
	void update(f64 dt);
	void stop();
	bool isFinished() const;
	f64 getStartTime() const;
	f64 getDuration() const;
	f64 getElapsed() const;
	f64 getRemaining() const;
	bool isActive() const;

private:
	void init(const std::string& a_materialName);

	f64	m_duration;
	f64	m_startTimestamp;
	Ogre::TextureUnitState* m_textureUnit;
	u32 m_totalFrames;
	bool m_loop;
};

#endif
