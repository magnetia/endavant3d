#include <cmath>
#include "Core/CCoreEngine.h"
#include "Time/CTimeManager.h"
#include "Game/GameEntity.h"
#include "BezierPathAnimator.h"

BezierPathAnimator::BezierPathAnimator(std::initializer_list<BezierPath> _bezierpathlist) :
	TimedBehavior{ [&]
	{
		f64 totalDuration = 0;
		for (auto bezierpath : _bezierpathlist)
			totalDuration += bezierpath.m_duration;
		return totalDuration;
	}() }
{
	for (auto bezierpath : _bezierpathlist)
		m_bezierpaths.push_back(bezierpath);
		
}

Behavior* BezierPathAnimator::clone()
{
	std::initializer_list<BezierPath> l(m_bezierpaths.data(), m_bezierpaths.data() + m_bezierpaths.size());
	return new BezierPathAnimator{l};
}

void BezierPathAnimator::initImpl()
{
	TimedBehavior::initImpl();

	getParent()->getGraphics()->getNode()->_setDerivedPosition(m_bezierpaths[0].GetBezierPosition(0));
	
}

void BezierPathAnimator::updateImpl(f64 dt)
{

	if (!finished() && !m_bezierpaths.empty())
	{

		// Busco quin path haig de calcular segons el temps [floor..ceil]
		f64 cur_time = elapsed();
		size_t cur_path = 0;
		f64 time_floor = 0;
		f64 time_ceil = m_bezierpaths[cur_path].m_duration;
		while ((cur_time > time_ceil) && (cur_path < m_bezierpaths.size()))
		{
			cur_path++;
			time_floor = time_ceil;
			time_ceil += m_bezierpaths[cur_path].m_duration;
		}

		//trec el temps dels paths anteriors per normalitzar posteriorment
		cur_time -= time_floor;
		time_ceil -= time_floor;
		time_floor = 0;
		
		//Normalitzo el temps en [0,1]
		f64 normalized_time = (cur_time - time_floor) / (time_ceil - time_floor);
		
		//Setejo la posicio actual
		Ogre::Vector3 pos = m_bezierpaths[cur_path].GetBezierPosition(normalized_time);
		//std::cout << " position(t = "<< normalized_time<<")  x: " << pos.x <<" y: "<< pos.y <<" z: " << pos.z <<std::endl;
		getParent()->getGraphics()->getNode()->_setDerivedPosition(pos);
			
			

	}
}

void BezierPathAnimator::stopImpl()
{

}
