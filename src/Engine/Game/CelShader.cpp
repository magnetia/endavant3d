#include "CelShader.h"

const char DIFFUSE_TEXTURE[] = "cel_shading_diffuse.png";
const char SPECULAR_TEXTURE[] = "cel_shading_specular.png";
const char EDGES_TEXTURE[] = "cel_shading_edge.png";

CustomMaterial* CelShader::Factory::createElement(const CustomMaterial::factory_param_type& a_config)
{
	return new CelShader{{
		a_config["clone_name"],
		{ a_config["shininess/r"], a_config["shininess/g"], a_config["shininess/b"], a_config["shininess/a"] },
		{ a_config["diffuse/r"], a_config["diffuse/g"], a_config["diffuse/b"], a_config["diffuse/a"] },
		{ a_config["specular/r"], a_config["specular/g"], a_config["specular/b"], a_config["specular/a"] }
	}};
}

CelShader::CelShader(const Config& a_config) :
	m_config{ a_config }
{

}

CelShader::~CelShader()
{

}

void CelShader::init(Ogre::Entity* a_entity)
{
	if (a_entity)
	{
		reset();
		m_entity = a_entity;

		// removes passes from first technique from first material and adds its stuff
		if (u32 subEntities = a_entity->getNumSubEntities())
		{
			Ogre::SubEntity* const subEntity = a_entity->getSubEntity(0);
			m_originalMat = subEntity->getMaterial().get();

			const std::string materialName = subEntity->getMaterialName() + "CelShader" + m_config.cloneName;
			const Ogre::MaterialPtr& materialPtr =
				subEntity->getMaterial()->clone(materialName);

			m_material = materialPtr.get();

			if (const u32 numTechniques = m_material->getNumTechniques())
			{
				Ogre::Technique* const technique = m_material->getTechnique(0);

				if (const u32 numPasses = technique->getNumPasses())
				{
					Ogre::Pass* const originalPass = technique->getPass(0);

					if (const u32 numTextureUnits = originalPass->getNumTextureUnitStates())
					{
						Ogre::TextureUnitState* const originalTextureUnit = originalPass->getTextureUnitState(0);
						const std::string textureName = originalTextureUnit->getTextureName();

						technique->removeAllPasses();

						// first pass: texture, no lights, flat shading
						Ogre::Pass* const firstPass = technique->createPass();
						firstPass->setLightingEnabled(false);
						firstPass->setShadingMode(Ogre::ShadeOptions::SO_FLAT);

						Ogre::TextureUnitState* const originalImgTextureUnit = firstPass->createTextureUnitState();
						originalImgTextureUnit->setTextureName(textureName);
						originalImgTextureUnit->setTextureFiltering(Ogre::TextureFilterOptions::TFO_TRILINEAR);

						// second pass: effect
						Ogre::Pass* const secondPass = technique->createPass();
						secondPass->setDepthBias(16.f);

						// vertex program
						secondPass->setVertexProgram("Ogre/CelShadingVP");
						auto vertexParamsPtr = secondPass->getVertexProgramParameters();
						vertexParamsPtr->setNamedAutoConstant("shininess", Ogre::GpuProgramParameters::ACT_CUSTOM, 1);
						secondPass->setVertexProgramParameters(vertexParamsPtr);

						// fragment program
						secondPass->setFragmentProgram("Ogre/CelShadingFP");
						auto fragmentParamsPtr = secondPass->getFragmentProgramParameters();
						fragmentParamsPtr->setNamedAutoConstant("diffuse", Ogre::GpuProgramParameters::ACT_CUSTOM, 2);
						fragmentParamsPtr->setNamedAutoConstant("specular", Ogre::GpuProgramParameters::ACT_CUSTOM, 3);
						secondPass->setFragmentProgramParameters(fragmentParamsPtr);

						// diffuse texture
						auto* diffuseTextureUnit = secondPass->createTextureUnitState();
						diffuseTextureUnit->setTextureName(DIFFUSE_TEXTURE, Ogre::TEX_TYPE_1D);
						diffuseTextureUnit->setTextureAddressingMode(Ogre::TextureUnitState::TAM_CLAMP);
						diffuseTextureUnit->setTextureFiltering(Ogre::TextureFilterOptions::TFO_NONE);

						// specular texture
						auto* specularTextureUnit = secondPass->createTextureUnitState();
						specularTextureUnit->setTextureName(SPECULAR_TEXTURE, Ogre::TEX_TYPE_1D);
						specularTextureUnit->setTextureAddressingMode(Ogre::TextureUnitState::TAM_CLAMP);
						specularTextureUnit->setTextureFiltering(Ogre::TextureFilterOptions::TFO_NONE);

						// edges texture
						auto* edgeTextureUnit = secondPass->createTextureUnitState();
						edgeTextureUnit->setTextureName(EDGES_TEXTURE, Ogre::TEX_TYPE_1D);
						edgeTextureUnit->setTextureAddressingMode(Ogre::TextureUnitState::TAM_CLAMP);
						edgeTextureUnit->setTextureFiltering(Ogre::TextureFilterOptions::TFO_NONE);

						// scene blend
						secondPass->setSceneBlending(Ogre::SceneBlendType::SBT_MODULATE);

						// pass parameters to programs
						subEntity->setCustomParameter(1, m_config.shininess);
						subEntity->setCustomParameter(2, m_config.diffuse);
						subEntity->setCustomParameter(3, m_config.specular);

						// apply material
						subEntity->setMaterialName(materialName);
					}
				}
			}
		}
	}
}

void CelShader::remove()
{
	if (u32 subEntities = m_entity->getNumSubEntities())
	{
		Ogre::SubEntity* const subEntity = m_entity->getSubEntity(0);
		const std::string matName = m_material->getName();

		subEntity->setMaterialName(m_originalMat->getName());
		Ogre::MaterialManager::getSingleton().remove(matName);
	}
}
