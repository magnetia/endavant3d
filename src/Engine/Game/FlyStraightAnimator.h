#ifndef EDV_GUI_FLYSTRAIGHT_ANIMATOR_H_
#define EDV_GUI_FLYSTRAIGHT_ANIMATOR_H_

#include "Game/TimedBehavior.h"
#include "OGRE/Ogre.h"
#include "Utils/edvVector3.h"

class FlyStraightAnimator : public TimedBehavior
{
public:
	FlyStraightAnimator(f64 a_duration, const Ogre::Vector3& a_orig, const Ogre::Vector3& a_dest);
	FlyStraightAnimator(f64 a_duration, const edv::Vector3f& a_orig, const edv::Vector3f& a_dest);
	FlyStraightAnimator& operator+=(const Ogre::Vector3& a_vect);

	Behavior* clone() override;

private:
	void initImpl() override;
	void stopImpl() override;
	void updateImpl(f64 dt) override;

	void reset();

	f64				m_timeFactor;
	Ogre::Vector3	m_orig;
	Ogre::Vector3	m_dest;
	Ogre::Vector3	m_vector;
};

#endif
