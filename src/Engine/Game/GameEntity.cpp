#include <stdexcept>
#include "Core/CCoreEngine.h"
#include "Physics/CPhysicsManager.h"
#include "GameEntity.h"

GameEntity::GameEntity() :
	m_currentId{ 0 },
	m_updating{ false },
	m_enabled{ true }
{

}

GameEntity::GameEntity(const EntityOgreGraphics::Config& a_graphicsCfg, const EntityBtPhysics::Config& a_physicsCfg, Actor* a_actor) :
	GameEntity()
{
	reset(a_graphicsCfg, a_physicsCfg, a_actor);
}

GameEntity::GameEntity(const EntityOgreGraphics::Config& a_graphicsCfg)
{
	reset(a_graphicsCfg);
}

GameEntity::~GameEntity()
{
	reset();
}

void GameEntity::reset()
{
	if (m_actor)
	{
		m_actor->clear();
	}

	for (const auto& pair : m_behaviors)
	{
		Behavior* behavior = pair.second.get();
		behavior->stop();
	}

	m_behaviors.clear();

	if (m_physics)
	{
		m_physics->clear();
	}

	if (m_graphics)
	{
		m_graphics->clear();
	}

	m_enabled = true;
}

void GameEntity::reset(const EntityOgreGraphics::Config& a_graphicsCfg, const EntityBtPhysics::Config& a_physicsCfg, Actor* a_actor)
{
	reset();

	m_graphics.reset(new EntityOgreGraphics(a_graphicsCfg));

	m_physics.reset(new EntityBtPhysics);
	m_physics->setParent(this);
	m_physics->init(m_graphics.get(), a_physicsCfg);

	if (a_actor)
	{
		m_actor.reset(a_actor);
		a_actor->setParent(this);
	}
}

void GameEntity::reset(const EntityOgreGraphics::Config& a_graphicsCfg)
{
	reset();

	m_graphics.reset(new EntityOgreGraphics(a_graphicsCfg));
}

void GameEntity::init()
{
	for (auto& pair : m_behaviors)
	{
		pair.second->init();
	}

	if (m_actor)
	{
		m_actor->init();
	}
}

void GameEntity::update(f64 dt)
{
	if (m_enabled)
	{
		m_updating = true;
			for (const auto& pair : m_behaviors)
			{
				const auto& behavior = pair.second;

				if (!behavior->paused())
				{
					behavior->update(dt);
				}
			}

			if (m_actor)
			{
				m_actor->update(dt);
			}

			if (m_graphics)
			{
				m_graphics->update(dt);
			}

			if (m_physics)
			{
				m_physics->update(dt);
			}
		m_updating = false;

		// behaviors may remove other behaviors, we should keep map consistent while iterating
		std::for_each(m_behaviorsToRemove.begin(), m_behaviorsToRemove.end(), [&](Behavior::Id id){ m_behaviors.erase(id); });
		m_behaviorsToRemove.clear();
	}
}

void GameEntity::stop()
{
	if (m_actor)
	{
		m_actor->stop();
	}

	for (auto& pair : m_behaviors)
	{
		pair.second->stop();
	}
}

void GameEntity::pause()
{
	for (auto& pair : m_behaviors)
	{
		pair.second->pause();
	}
}

void GameEntity::resume()
{
	for (auto& pair : m_behaviors)
	{
		pair.second->resume();
	}
}

// #todo: es necessari acoplar el concepte de habilitat amb pausat? segurament no: provar
void GameEntity::setEnabled(bool a_status)
{
	if (a_status != m_enabled)
	{
		if (m_enabled)
		{
			pause();
		}
		else
		{
			resume();
		}

		m_enabled = a_status;
	}
}

bool GameEntity::isEnabled() const
{
	return m_enabled;
}

edv::Vector3f GameEntity::getWorldPosition()
{
	edv::Vector3f pos(getGraphics()->getWorldPosition());
	if (getPhysics() && !getPhysics()->isKinematic())
	{
		if (btRigidBody* const body = getPhysics()->getRigidBody())
		{
			btTransform posaux;
			body->getMotionState()->getWorldTransform(posaux);
			pos = posaux.getOrigin();
		}
	}

	

	return pos;
}

void GameEntity::setWorldPosition(const edv::Vector3f& a_position)
{
	// This method only affects kinematic objects
	if (!getPhysics() || (getPhysics() && getPhysics()->isKinematic()))
	{
		getGraphics()->getNode()->_setDerivedPosition(a_position.to_OgreVector3());
	}
}

Ogre::Quaternion GameEntity::getWorldOrientation()
{
	btQuaternion orientation(getGraphics()->getWorldOrientation());
	if (getPhysics() && !getPhysics()->isKinematic())
	{
		if (btRigidBody* const body = getPhysics()->getRigidBody())
		{
			btTransform posaux;
			body->getMotionState()->getWorldTransform(posaux);
			orientation = posaux.getRotation();
		}
	}

	return Ogre::Quaternion{ orientation.w(), orientation.x(), orientation.y(), orientation.z() };
}

void GameEntity::setWorldOrientation(const Ogre::Quaternion& a_orientation)
{
	// This method only affects kinematic objects
	if (!getPhysics() || (getPhysics() && getPhysics()->isKinematic()))
	{
		getGraphics()->getNode()->_setDerivedOrientation(a_orientation);
	}
}

Ogre::Vector2 GameEntity::getScreenPosition()
{
	/* TODO!!1
	btTransform trans;
	getPhysics()->getRigidBody()->getMotionState()->getWorldTransform(trans);

	//Linia d'efecte
	return Ogre::Vector3(trans.getOrigin().x(), trans.getOrigin().y(), trans.getOrigin().z());
	*/
	return{ 0, 0 };

}

edv::Vector3f GameEntity::getScale() const
{
	edv::Vector3f scale;

	if (m_physics)
	{
		scale = m_physics->getScale();
	}
	else if (m_graphics)
	{
		scale = m_graphics->getNode()->getScale();
	}

	return scale;
}

edv::Vector3f GameEntity::getHalfSize() const
{
	edv::Vector3f halfSize;

	if (m_physics)
	{
		halfSize = m_physics->getHalfSize();
	}
	else if (m_graphics)
	{
		halfSize = m_graphics->getHalfSize();
	}

	return halfSize;
}

edv::Vector3f GameEntity::getScaledHalfSize() const
{
	return getHalfSize() * getScale();
}

void GameEntity::setScale(const edv::Vector3f& a_scale)
{
	const edv::Vector3f currScale = getScale();

	if (m_graphics)
	{
		m_graphics->setScale(a_scale.to_OgreVector3());
	}

	if (m_physics)
	{
		m_physics->setScale(a_scale.to_btVector3());
	}
}

// #todo: treure aquesta funcio. Han de poder haver-hi entitats sense
// grafics o sense fisiques.
bool GameEntity::good() const
{
	bool ok = true;

	for (auto it = m_behaviors.begin(); ok && it != m_behaviors.end(); it++)
	{
		if (!it->second)
		{
			ok = false;
		}
	}

	return ok && m_graphics && m_physics; // actor is optional
}

GameEntity* GameEntity::clone(const std::string& a_newName, const Ogre::Vector3& a_newPos, 
	const Ogre::Quaternion& a_newRot) const
{
	GameEntity* entity = nullptr;

	if (good())
	{
		GraphicsPtr graphics(m_graphics->clone(a_newName, a_newPos, a_newRot));

		if (graphics)
		{
			PhysicsPtr physics(m_physics->clone(graphics.get()));

			if (physics)
			{
				entity = new GameEntity;

				entity->m_graphics = std::move(graphics);

				entity->m_physics = std::move(physics);
				entity->m_physics->setParent(entity);

				if (m_actor)
				{
					entity->m_actor.reset(m_actor->clone());
					entity->m_actor->setParent(entity);
				}				

				if (btRigidBody* body = entity->m_physics->getRigidBody())
				{
					body->setUserPointer(entity);
				}

				for (const auto& pair : m_behaviors)
				{
					if (auto* behavior = pair.second->clone())
					{
						entity->addBehavior(behavior);
					}
				}
			}
		}
	}

	return entity;
}

Behavior::Id GameEntity::addBehavior(Behavior* a_behavior)
{
	if (a_behavior)
	{
		a_behavior->setParent(this);
		a_behavior->init();

		m_currentId = m_idGenerator.nextId();
		m_behaviors.emplace(Behaviors::value_type(m_currentId, BehaviorPtr(a_behavior)));

		return m_currentId;
	}

	throw std::runtime_error{ "invalid argument" };
}

Behavior* GameEntity::getBehavior(Behavior::Id a_id)
{
	Behavior* behavior = nullptr;
	auto it = m_behaviors.find(a_id);

	if (it != m_behaviors.end())
	{
		behavior = it->second.get();
	}

	return behavior;
}

void GameEntity::removeBehavior(Behavior::Id a_id)
{
	auto it = m_behaviors.find(a_id);

	if (it != m_behaviors.end())
	{
		Behavior* behavior = it->second.get();
		behavior->stop();

		if (m_updating)
		{
			m_behaviorsToRemove.push_back(a_id);
		}
		else
		{
			m_behaviors.erase(it);
		}
	}
}

void GameEntity::removeBehavior(Behavior* a_behavior)
{
	if (a_behavior)
	{
		bool found = false;
		auto it = m_behaviors.begin();

		while (!found && it != m_behaviors.end())
		{
			auto* behavior = it->second.get();

			if (behavior == a_behavior)
			{
				behavior->stop();

				if (m_updating)
				{
					m_behaviorsToRemove.push_back(it->first);
				}
				else
				{
					m_behaviors.erase(it);
				}

				found = true;
			}
			else
			{
				it++;
			}
		}
	}
}

void GameEntity::removeAllBehaviors()
{
	for (auto& pair : m_behaviors)
	{
		pair.second->stop();
	}

	if (m_updating)
	{
		std::transform(m_behaviors.begin(), m_behaviors.end(), std::back_inserter(m_behaviorsToRemove), 
			[](Behaviors::value_type& pair)
				{ return pair.first; }
		);
	}
	else
	{
		m_behaviors.clear();
	}
}

u32 GameEntity::getNumBehaviors() const
{
	return m_behaviors.size();
}

f32 GameEntity::distanceToEntity(GameEntity* a_target)
{
	f32 dist = 0.f;

	if (good() && a_target && a_target->good())
	{
		dist = Ogre::Vector3{
			(getWorldPosition() -
			a_target->getWorldPosition()).to_OgreVector3()
		}.length();
	}

	return dist;
}

f32 GameEntity::angleToEntity(GameEntity* a_target)
{
	f32 angle = 0.f;

	if (good() && a_target && a_target->good())
	{
		edv::Vector3f xAxisVector{ 1.f, 0.f, 0.f };
		edv::Vector3f vector{ a_target->getWorldPosition() - getWorldPosition() };
		const f32 dotProduct = xAxisVector.dotProduct(vector);
		angle = std::acos(dotProduct / (xAxisVector.length()*vector.length()));
		angle *= (vector.y > 0) ? 1.f : -1.f;
		angle = Ogre::Radian(angle).valueDegrees();
		angle = fmod(angle, 360.f);
		if (angle < 0.f) angle += 360.f;
	}

	return angle;
}

bool GameEntity::canSeeEntity(GameEntity* a_target)
{
	bool visible = false;

	if (good() && a_target && a_target->good())
	{
		GameEntity* const result = 
			static_cast<GameEntity*>(CCoreEngine::Instance().GetPhysicsManager().RayTest(getWorldPosition(), a_target->getWorldPosition()));

		visible = result == a_target;
	}

	return visible;
}

std::string GameEntity::getName() const
{
	std::string name;

	if (m_graphics)
	{
		name = m_graphics->getNode()->getName();
	}

	return name;
}

EntityOgreGraphics* GameEntity::getGraphics()
{
	return m_graphics.get();
}

EntityBtPhysics* GameEntity::getPhysics()
{
	return m_physics.get();
}

Actor* GameEntity::getActor()
{
	return m_actor.get();
}