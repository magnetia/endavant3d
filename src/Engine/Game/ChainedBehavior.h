#ifndef EDV_GAME_CHAINED_BEHAVIOR_H_
#define EDV_GAME_CHAINED_BEHAVIOR_H_

#include <memory>
#include <initializer_list>
#include <vector>
#include "TimedBehavior.h"

class ChainedBehavior : public TimedBehavior
{
public:
	ChainedBehavior(std::initializer_list<TimedBehavior*>&& a_list = {}, s32 a_loops = 1);
	ChainedBehavior(const ChainedBehavior& a_other);

	Behavior* clone() override;

private:
	typedef std::unique_ptr<TimedBehavior>	BehaviorPtr;
	typedef std::vector<BehaviorPtr>		Behaviors;

	void initImpl() override;
	void stopImpl() override;
	void updateImpl(f64 dt) override;
	void pauseImpl() override;
	void resumeImpl() override;

	void next();

	Behaviors			m_behaviors;
	Behaviors::iterator	m_current;
	s32					m_loops;
};

#endif
