#ifndef EDV_GAME_PARTICLE_SYSTEM_ANIMATOR_H_
#define EDV_GAME_PARTICLE_SYSTEM_ANIMATOR_H_

#include <string>
#include <OGRE/Ogre.h>
#include "Game/TimedBehavior.h"
#include "Utils/edvVector3.h"

class ParticleSystemAnimator : public TimedBehavior
{
public:
	enum OnStopAction
	{
		ON_STOP_SET_INVISIBLE,
		ON_STOP_DISABLE_EMITTERS
	};

	ParticleSystemAnimator(const std::string& a_name, f64 a_duration, const std::string& a_particleSystem, const std::string& a_boneName = "", 
		const edv::Vector3f& a_translation = { 0.f, 0.f, 0.f }, const edv::Vector3f& a_scale = { 1.f, 1.f, 1.f }, OnStopAction a_onStopAction = ON_STOP_SET_INVISIBLE);
	Behavior* clone() override;

private:
	void initImpl() override;
	void stopImpl() override;
	void updateImpl(f64 dt) override;

	std::string		m_name;
	std::string		m_particleSystemName;
	std::string		m_boneName;
	edv::Vector3f	m_translation;
	edv::Vector3f	m_scale;
	bool			m_playing;
	OnStopAction	m_onStopAction;
};

#endif
