#ifndef EDV_GAME_BEHAVIOR_H_
#define EDV_GAME_BEHAVIOR_H_

#include "Core/CBasicTypes.h"
#include "Utils/ChildOf.h"
#include "Utils/Factory.h"
#include "Utils/any.h"
#include "Utils/tree.h"

class GameEntity;

class Behavior : public ChildOf<GameEntity>
{
public:
	typedef u64										Id;
	typedef edv::tree<edv::any>						factory_param_type;
	typedef Factory<Behavior, factory_param_type>	factory_type;

	Behavior();
	virtual ~Behavior();

	virtual Behavior* clone() = 0;

	virtual void init() final;
	virtual void stop() final;
	virtual void update(f64 dt) final;
	virtual void pause() final;
	virtual void resume() final;
	virtual bool paused() const final;	

protected:
	virtual void initImpl() = 0;
	virtual void stopImpl() = 0;
	virtual void updateImpl(f64 dt) = 0;
	virtual void pauseImpl() = 0;
	virtual void resumeImpl() = 0;

private:
	bool m_paused;
};

#endif
