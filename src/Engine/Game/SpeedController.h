#ifndef EDV_GAME_SPEED_CONTROLLER_H_
#define EDV_GAME_SPEED_CONTROLLER_H_

#include <string>
#include "Behavior.h"

class SpeedController : public Behavior
{
public:
	// Factory
	class Factory : public Behavior::factory_type
	{
	public:
		Behavior* createElement(const Behavior::factory_param_type& a_config) override;
	};

	SpeedController(f32 a_max_speed = 3.0f);
	~SpeedController();

	Behavior* clone() override final;

	void setMaxSpeed(f32 a_max_speed);
	f32 getMaxSpeed() const;
	void physicsTickCallback(f32 a_timeStep);

protected:
	void initImpl() override final;
	void stopImpl() override final;
	void updateImpl(f64 dt) override final;
	void pauseImpl() override final;
	void resumeImpl() override final;

	f32							m_max_speed;
	std::string					m_callbackId;
};

#endif
