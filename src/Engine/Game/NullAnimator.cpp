#include "Core/CCoreEngine.h"
#include "Core/CLogManager.h"
#include "Time/CTimeManager.h"
#include "Game/GameEntity.h"
#include "NullAnimator.h"

NullAnimator::NullAnimator(f64 a_duration) :
	TimedBehavior{ a_duration }
{

}

Behavior* NullAnimator::clone()
{
	return new NullAnimator{ duration() };
}

void NullAnimator::initImpl()
{
	TimedBehavior::initImpl();
}

void NullAnimator::updateImpl(f64 dt)
{

}

void NullAnimator::stopImpl()
{

}
