#ifndef EDV_GAME_MOUSE_LISTENER_H_
#define EDV_GAME_MOUSE_LISTENER_H_

#include <string>
#include <vector>
#include <functional>
#include "Behavior.h"

class MouseListener : public Behavior
{
public:
	enum MouseButton
	{
		MOUSE_BUTTON_LEFT,
		MOUSE_BUTTON_RIGHT,
		MOUSE_BUTTON_MIDDLE,
	};

	typedef std::function<void(MouseButton)>	OnClickFunc;
	typedef std::function<void(MouseButton)>	OnUpFunc;
	typedef std::function<void(bool)>			OnHoverFunc;

	MouseListener(OnClickFunc a_onClickFunc = nullptr, OnUpFunc a_onUpFunc = nullptr, OnHoverFunc a_onHoverFunc = nullptr);
	MouseListener(MouseListener&& a_other);
	~MouseListener();

	Behavior* clone() override;

private:
	typedef std::pair<std::string, MouseButton>	MouseStatus;
	typedef std::vector<MouseStatus>			Buttons;

	void initImpl() override;
	void updateImpl(f64 dt) override;
	void stopImpl() override;
	void pauseImpl() override;
	void resumeImpl() override;
	void addActions();
	void removeActions();

	bool		m_hovering;
	OnClickFunc	m_onClick;
	OnUpFunc	m_onUp;
	OnHoverFunc	m_onHover;
	Buttons		m_buttons;
	Buttons		m_gcbuttons;
};

#endif
