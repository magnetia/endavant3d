#ifndef EDV_GUI_ROTATION_ANIMATOR_H_
#define EDV_GUI_ROTATION_ANIMATOR_H_

#include "Game/TimedBehavior.h"
#include "OGRE/Ogre.h"

class RotationAnimator : public TimedBehavior
{
public:
	RotationAnimator(f64 a_duration, const Ogre::Degree &a_rotdegrees, const Ogre::Vector3& a_rotaxis);

	Behavior* clone() override;

private:
	void initImpl() override;
	void stopImpl() override;
	void updateImpl(f64 dt) override;

	void reset();

	Ogre::Radian	m_rotationFactor;
	Ogre::Degree	m_rotdegreesfinal;
	Ogre::Radian	m_rotradianfinal;
	Ogre::Vector3	m_rotaxis;

	Ogre::Quaternion m_finalorientation;
};

#endif
