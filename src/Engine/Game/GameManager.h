#ifndef EDV_GAME_GAME_MANAGER_H_
#define EDV_GAME_GAME_MANAGER_H_

#include <memory>
#include <string>
#include <unordered_map>
#include "OGRE/Ogre.h"
#include "Core/CCoreEngine.h"
#include "Game/Behavior.h"
#include "Game/GameEntity.h"
#include "Game/Level.h"
#include "Script/ScriptManager.h"
#include "Utils/IdGenerator.h"
#include "Utils/tree.h"
#include "Utils/any.h"
#include "Utils/Factory.h"

// #todo: use edv vectors

class GameManager
{
public:
	enum ComponentType
	{
		COMPONENT_TYPE_BEHAVIOR,
		COMPONENT_TYPE_ACTOR,
		COMPONENT_TYPE_ACTOR_MOVEMENT,
		COMPONENT_TYPE_CUSTOM_MATERIAL,
	};

	typedef u64									PlayerId;
	typedef std::pair<std::string, GameEntity*>	EntityInfo;

	GameManager();
	~GameManager();

	void startUp();
	void shutDown();
	void update(f64 dt);

	//Level* createLevel(const std::string& a_levelName);

	// creates an entity of given type with name, position and rotation
	GameEntity* createEntity(const std::string& a_type, const std::string& a_name,
		const Ogre::Vector3& a_position = Ogre::Vector3::ZERO, 
		const Ogre::Quaternion& a_rotation = Ogre::Quaternion::IDENTITY, Ogre::SceneNode* a_parent = nullptr);
	void destroyEntity(const std::string& a_name);
	void destroyEntity(GameEntity* a_entity);

	// creates an entity of given type with position, rotation and auto generated name
	// name is returned with the created entity
	EntityInfo createEntity(const std::string& a_type, 
		const Ogre::Vector3& a_position = Ogre::Vector3::ZERO,
		const Ogre::Quaternion& a_rotation = Ogre::Quaternion::IDENTITY);

	GameEntity* getEntity(const std::string& a_name) const;
	bool		hasEntity(GameEntity* a_entity) const;
	GameEntity* getPlayer(PlayerId a_id = IdGenerator<PlayerId>::INVALID_ID) const;
	GameEntity* getBoss() const;
	Level*		getLevel(const std::string& a_name);
	Level*		getCurrentLevel();
	std::vector<std::string> getLevelsRegistered();


	void registerComponent(ComponentType a_type, const std::string& a_name, void* a_factory);
	void reloadEntityConfiguration();
	PlayerId registerPlayer(GameEntity* a_player);
	void registerBoss(GameEntity* a_boss);
	void unregisterPlayer(PlayerId a_id);
	void unregisterPlayer(GameEntity* a_player);
	void unregisterBoss();
	void registerLevel(const std::string& a_name, Level* a_level);
	void unregisterLevel(const std::string& a_name);
	void unregisterLevel(Level* a_level);
	void registerDefaultActor(Actor::factory_type* a_actor);

private:
	typedef edv::tree<edv::any>											Config;
	typedef std::unique_ptr<GameEntity>									EntityPtr;
	typedef std::unordered_map<std::string, EntityPtr>					RegisteredEntities;
	typedef std::unique_ptr<Behavior::factory_type>						BehaviorFactoryPtr;
	typedef std::unique_ptr<Actor::factory_type>						ActorFactoryPtr;
	typedef std::unique_ptr<CustomMaterial::factory_type>				CustomMaterialFactoryPtr;
	typedef std::unordered_map<std::string, BehaviorFactoryPtr>			BehaviorFactories;
	typedef std::unordered_map<std::string, ActorFactoryPtr>			ActorFactories;
	typedef std::unordered_map<std::string, CustomMaterialFactoryPtr>	CustomMaterialFactories;
	typedef std::unordered_map<PlayerId, GameEntity*>					Players;
	typedef std::unordered_map<std::string, Level*>						Levels;

	void registerBehaviorFactory(const std::string& a_name, Behavior::factory_type* a_factory);
	void registerActorFactory(const std::string& a_name, Actor::factory_type* a_factory);
	void registerCustomMaterialFactory(const std::string& a_name, CustomMaterial::factory_type* a_factory);
	void loadBehaviors();
	void loadCustomMaterials();
	void loadEntities();
	void loadLevels();

	Config					m_entityConfig;
	//Config				m_levelsConfig;
	RegisteredEntities		m_registeredEntities;
	BehaviorFactories		m_behaviorFactories;
	ActorFactories			m_actorFactories;
	CustomMaterialFactories	m_customMaterialFactories;
	Players					m_players;
	Levels					m_levels;
	IdGenerator<u64>		m_autoIdGen;
	IdGenerator<PlayerId>	m_playerIdGen;
	ActorFactoryPtr			m_default_actor;
	GameEntity*				m_boss;
};

#endif
