#include "Core/CCoreEngine.h"
#include "Script/ScriptManager.h"
#include "GameEntity.h"
#include "LuaBehavior.h"

Behavior* LuaBehavior::Factory::createElement(const Behavior::factory_param_type& a_config)
{
	return new LuaBehavior{{ a_config["path"], a_config["scope"] }};
}

LuaBehavior::LuaBehavior(const ScriptConfig& a_config) :
	m_scriptConfig{ a_config }
{

}

LuaBehavior::~LuaBehavior()
{

}

Behavior* LuaBehavior::clone()
{
	return new LuaBehavior;
}

void LuaBehavior::initImpl()
{
	if (!m_scriptConfig.empty())
	{
		auto& script = CCoreEngine::Instance().GetScriptManager();
		script.loadScript(m_scriptConfig.path);
		script.callProcedure(m_scriptConfig.scope + "Init", getParent());
	}
}

void LuaBehavior::stopImpl()
{
	if (!m_scriptConfig.empty())
	{
		CCoreEngine::Instance().GetScriptManager().callProcedure(m_scriptConfig.scope + "Stop", getParent());
	}
}

void LuaBehavior::updateImpl(f64 dt)
{
	if (!m_scriptConfig.empty())
	{
		CCoreEngine::Instance().GetScriptManager().callProcedure(m_scriptConfig.scope + "Update", dt, getParent());
	}
}

void LuaBehavior::pauseImpl()
{
	
}

void LuaBehavior::resumeImpl()
{

}
