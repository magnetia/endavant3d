#ifndef CEVENTHANDLER_H_
#define CEVENTHANDLER_H_

#include "Core/CBasicTypes.h"
#include <SDL2/SDL_events.h>

#include <set>
#include <map>
#include "CEventCodes.h"

class CEventHandler
{
public:
	typedef std::set< u32 > 				t_CodeList;
	typedef std::map< u32, t_CodeList > 	t_TypeList;

	CEventHandler();
	virtual ~CEventHandler();

	virtual void HandleEvent(const EVENT_TYPES a_Type, u32 a_Code, SDL_Event* a_Event = nullptr) = 0;

	void AddEvent(const EVENT_TYPES a_Type, const u32 a_Code);

	void RegisterEventManager();
	void UnRegisterEventManager();

private:


	t_TypeList m_TypeList;
	bool m_Registered;

};

#endif /* CEVENTHANDLER_H_ */
