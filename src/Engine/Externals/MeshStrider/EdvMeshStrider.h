#ifndef EDV_MeshStrider_h__
#define EDV_MeshStrider_h__

#include <btBulletCollisionCommon.h>
#include <btBulletDynamicsCommon.h>
#include <OGRE/Ogre.h>
#include <cassert>

#ifndef ASSERT
#define ASSERT assert
#endif

/// Shares vertices/indexes between Ogre and Bullet.

//-- Copia la inforamci� de la mesh a mem�ria per tal de que no haguem de bloquejar la mesh d'Ogre cada vegada que ens preguntin per a la seva informaci�.
// Alerta! �s m�s eficient en rendiment pero dupliquem l'espai a mem�ria de la informaci� de la mesh d'Ogre. A m�s, nomes agafa la configuracio inicial de la mesh, no te en compte els posteriors canvis.
// Ideal per meshes est�tiques.
//--// 
class EdvMeshStrider : public btStridingMeshInterface{

public:
	EdvMeshStrider(Ogre::Mesh * m = nullptr);
	~EdvMeshStrider();

	void set(Ogre::Mesh * m) { ASSERT(m); mMesh = m; }
	// inherited interface
	virtual int        getNumSubParts() const;

	virtual void    getLockedVertexIndexBase(unsigned char **vertexbase, int& numverts, PHY_ScalarType& type, int& stride, unsigned char **indexbase, int & indexstride, int& numfaces, PHY_ScalarType& indicestype, int subpart = 0);
	virtual void    getLockedReadOnlyVertexIndexBase(const unsigned char **vertexbase, int& numverts, PHY_ScalarType& type, int& stride, const unsigned char **indexbase, int & indexstride, int& numfaces, PHY_ScalarType& indicestype, int subpart = 0) const;

	virtual void    unLockVertexBase(int subpart);
	virtual void    unLockReadOnlyVertexBase(int subpart) const;

	virtual void    preallocateVertices(int numverts);
	virtual void    preallocateIndices(int numindices);
private:
	Ogre::Mesh * mMesh;
	
	struct sSubmeshConfigutation
	{
		unsigned char*	vertexBase;
		int				numverts;
		PHY_ScalarType	type;
		int				stride;
		unsigned char*	indexBase;
		int				indexStride;
		int				numfaces;
		PHY_ScalarType	indicestype;
	};

	std::vector<sSubmeshConfigutation> m_subMeshConfiguration;
};

#endif // MeshStrider_h__
