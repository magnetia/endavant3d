#include "EdvMeshStrider.h"

EdvMeshStrider::EdvMeshStrider(Ogre::Mesh * m) :
	mMesh(m)
{
	for (int i = 0; i < mMesh->getNumSubMeshes(); i++)
	{
		sSubmeshConfigutation smconfig;

		unsigned char*			vertexbase;
		int						numverts;
		PHY_ScalarType			type;
		int						stride;
		unsigned char*			indexbase;
		int						indexstride;
		int						numfaces;
		PHY_ScalarType			indicestype;
		
		getLockedReadOnlyVertexIndexBase((const unsigned char**)&vertexbase, numverts, type, stride, (const unsigned char**)&indexbase, indexstride, numfaces, indicestype, i);

		smconfig.numverts = numverts;
		smconfig.type = type;
		smconfig.stride = stride;
		smconfig.indexStride = indexstride;
		smconfig.numfaces = numfaces;
		smconfig.indicestype = indicestype;
		smconfig.vertexBase = new unsigned char[numverts*stride];
		memcpy(smconfig.vertexBase, vertexbase, numverts*stride);
		smconfig.indexBase = new unsigned char[numverts*indexstride];
		memcpy(smconfig.indexBase, indexbase, numverts*indexstride);
		unLockReadOnlyVertexBase(i);

		m_subMeshConfiguration.push_back(smconfig);
	}
}

EdvMeshStrider::~EdvMeshStrider()
{
	for (std::vector<sSubmeshConfigutation>::iterator it = m_subMeshConfiguration.begin(); it != m_subMeshConfiguration.end(); it++)
	{
		delete (*it).vertexBase;
		delete (*it).indexBase;
	}
}

int EdvMeshStrider::getNumSubParts() const
{
	int ret = mMesh->getNumSubMeshes();
	ASSERT(ret > 0);
	return ret;
}

void EdvMeshStrider::getLockedReadOnlyVertexIndexBase(
	const unsigned char **vertexbase,
	int& numverts,
	PHY_ScalarType& type,
	int& stride,
	const unsigned char **indexbase,
	int & indexstride,
	int& numfaces,
	PHY_ScalarType& indicestype,
	int subpart/*=0*/) const
{
	if (subpart < static_cast<int>(m_subMeshConfiguration.size()))
	{
		sSubmeshConfigutation smconfig = m_subMeshConfiguration.at(subpart);
		*vertexbase = smconfig.vertexBase;
		numverts = smconfig.numverts;
		type = smconfig.type;
		stride = smconfig.stride;
		*indexbase = smconfig.indexBase;
		indexstride = smconfig.indexStride;
		numfaces = smconfig.numfaces;
		indicestype = smconfig.indicestype;
	}
	else
	{
		Ogre::SubMesh* submesh = mMesh->getSubMesh(subpart);

		Ogre::VertexData* vertex_data = submesh->useSharedVertices ? mMesh->sharedVertexData : submesh->vertexData;

		const Ogre::VertexElement* posElem =
			vertex_data->vertexDeclaration->findElementBySemantic(Ogre::VES_POSITION);

		Ogre::HardwareVertexBufferSharedPtr vbuf =
			vertex_data->vertexBufferBinding->getBuffer(posElem->getSource());

		*vertexbase =
			reinterpret_cast<unsigned char*>(vbuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
		// There is _no_ baseVertexPointerToElement() which takes an Ogre::Real or a double
		//  as second argument. So make it float, to avoid trouble when Ogre::Real will
		//  be comiled/typedefed as double:
		//Ogre::Real* pReal;
		float* pReal;
		posElem->baseVertexPointerToElement((void*)*vertexbase, &pReal);
		*vertexbase = (unsigned char*)pReal;

		stride = (int)vbuf->getVertexSize();

		numverts = (int)vertex_data->vertexCount;
		ASSERT(numverts);

		type = PHY_FLOAT;

		Ogre::IndexData* index_data = submesh->indexData;
		Ogre::HardwareIndexBufferSharedPtr ibuf = index_data->indexBuffer;

		if (ibuf->getType() == Ogre::HardwareIndexBuffer::IT_32BIT){
			indicestype = PHY_INTEGER;
		}
		else{
			ASSERT(ibuf->getType() == Ogre::HardwareIndexBuffer::IT_16BIT);
			indicestype = PHY_SHORT;
		}

		if (submesh->operationType == Ogre::RenderOperation::OT_TRIANGLE_LIST){
			numfaces = (int)index_data->indexCount / 3;
			indexstride = (int)ibuf->getIndexSize() * 3;
		}
		else
			if (submesh->operationType == Ogre::RenderOperation::OT_TRIANGLE_STRIP){
			numfaces = (int)index_data->indexCount - 2;
			indexstride = (int)ibuf->getIndexSize();
			}
			else{
				ASSERT(0); // not supported
			}

		*indexbase = reinterpret_cast<unsigned char*>(ibuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
	}
}

void EdvMeshStrider::getLockedVertexIndexBase(unsigned char **vertexbase, int& numverts, PHY_ScalarType& type, int& stride, unsigned char **indexbase, int & indexstride, int& numfaces, PHY_ScalarType& indicestype, int subpart/*=0*/)
{
	ASSERT(0);
}

void EdvMeshStrider::unLockReadOnlyVertexBase(int subpart) const
{
	if (subpart >= static_cast<int>(m_subMeshConfiguration.size()))
	{
		Ogre::SubMesh* submesh = mMesh->getSubMesh(subpart);

		Ogre::VertexData* vertex_data = submesh->useSharedVertices ? mMesh->sharedVertexData : submesh->vertexData;

		const Ogre::VertexElement* posElem =
			vertex_data->vertexDeclaration->findElementBySemantic(Ogre::VES_POSITION);

		Ogre::HardwareVertexBufferSharedPtr vbuf =
			vertex_data->vertexBufferBinding->getBuffer(posElem->getSource());

		vbuf->unlock();

		Ogre::IndexData* index_data = submesh->indexData;
		Ogre::HardwareIndexBufferSharedPtr ibuf = index_data->indexBuffer;
		ibuf->unlock();
	}
}

void EdvMeshStrider::unLockVertexBase(int subpart)
{
	ASSERT(0);
}

void EdvMeshStrider::preallocateVertices(int numverts)
{
	ASSERT(0);
}

void EdvMeshStrider::preallocateIndices(int numindices)
{
	ASSERT(0);
}