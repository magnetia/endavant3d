#include "CTimeManager.h"
#include "Core/CCoreEngine.h"
#include "Renderer\CRenderManager.h"
#include "Core/CLogManager.h"
#include "SDL2/SDL.h"

const u32 CTimeManager::INVALID_TIMERID = 0;
static const f64 FPS_CAP_MAX = 150000.0;
static const f64 DT_CAP_MAX = 0.1;
static const f64 MIN_DT = (1.0 / FPS_CAP_MAX);    //MAX FRAME CAP!
typedef std::array<f64, 1> tdeltaarray;
static tdeltaarray m_lastdeltas;
static tdeltaarray::iterator m_currlastdelta;



typedef std::array<f64, 60000> tmerdarray;
static tmerdarray m_merderada;
static tmerdarray::iterator m_curmerda;

CTimeManager::CTimeManager():
m_CurrentTimerID(INVALID_TIMERID)
{
	LOG( LOG_INFO, LOGSUB_TIMER,"Object Created!");

	// Elapsed time (Delta)
	
	m_ElapsedTimeSeconds = 0.0F;
	m_TotalTimeSeconds = 0.0F;
	m_lastTicks = 0;

	// FPS
	m_FramesPerSecond = 0.0F;

}

CTimeManager::~CTimeManager()
{
}

void CTimeManager::StartUp(void)
{
	m_merderada.fill(0.0);
	m_curmerda = m_merderada.begin();
	m_lastdeltas.fill(MIN_DT);
	m_currlastdelta = m_lastdeltas.begin();

	LOG( LOG_INFO, LOGSUB_TIMER,"StartUp");
}

void CTimeManager::ShutDown(void)
{
	LOG( LOG_INFO, LOGSUB_TIMER,"ShutDown");
	auto l_itToErase =  m_TimersMap.begin();
	while (l_itToErase != m_TimersMap.end() )
	{
		m_TimersMap.erase(l_itToErase++);
	}
}

void CTimeManager::Update(f64 dt)
{
	CalculateElapsedTime();
	CalculateFPS();
	UpdateTimers();
	
	//LOG(LOG_INFO, LOGSUB_TIMER,"dTime = %f |  FPS = %f ", m_ElapsedTimeSeconds ,m_FramesPerSecond);
}




void	CTimeManager::CalculateElapsedTime()
{
	/*
	//Si es el primer cop
	if (!m_lastTicks)
		m_lastTicks = SDL_GetPerformanceCounter();
	
	f64 l_current_elapsed_time = 0;
	u64 l_currentticks;
	while (l_current_elapsed_time < MIN_DT)
	{
		l_currentticks = SDL_GetPerformanceCounter();
		l_current_elapsed_time = static_cast<f64>((l_currentticks - m_lastTicks)) / static_cast<f64>(SDL_GetPerformanceFrequency());
	}
	
	//per si estem debugant i tardem massa que la logica no se'n vagi a prendre pel sac
	if (l_current_elapsed_time > DT_CAP_MAX)
		l_current_elapsed_time = MIN_DT;
	
	m_lastTicks = l_currentticks;

	//Guardo el elapsed time a l'array.
	*m_currlastdelta = l_current_elapsed_time;
	m_currlastdelta++;
	if (m_currlastdelta == m_lastdeltas.end())
		m_currlastdelta = m_lastdeltas.begin();



	//Calculo el elapsed time smoothed
	f64 delta_sum = 0.0;
	for (auto delta : m_lastdeltas)
		delta_sum += delta;
	*/
	//TODO OJO QUE NO ESTIC FENT EL SMOOTHING
	//m_ElapsedTimeSeconds = delta_sum / m_lastdeltas.size();
	//m_ElapsedTimeSeconds = delta_sum ;
	//AGAFO ELAPSED TIME DEL FRAMELISTENER
	if ((f64)CCoreEngine::Instance().GetRenderManager().m_elapsedframetime > DT_CAP_MAX)
		m_ElapsedTimeSeconds = MIN_DT;
	else
		m_ElapsedTimeSeconds = (f64)CCoreEngine::Instance().GetRenderManager().m_elapsedframetime;

	m_TotalTimeSeconds += m_ElapsedTimeSeconds;

}

EV_TimerID CTimeManager::CreateTimer(const f64 a_DurationInSeconds,const bool a_looped,  ITimerCallback *a_CallBackObject)
{
	++m_CurrentTimerID; // TODO POT DONAR LA VOLTA ARREGLAR! (OVERFLOW)
	EVTimer	l_NewTimer(m_CurrentTimerID, a_DurationInSeconds,a_looped,a_CallBackObject);
	// TODO use .emplace and check if the timer was inserted.
	m_TimersMap.insert(std::pair<u32, EVTimer>(m_CurrentTimerID, l_NewTimer));

	return m_CurrentTimerID;
}



void CTimeManager::UpdateTimers()
{
	// How many timers finished?
	std::vector<EV_TimerID >	l_TimersExpired;
	for (auto &l_Timer: m_TimersMap )
		if (l_Timer.second.IsEnd() && !l_Timer.second.IsLoop())
			l_TimersExpired.push_back(l_Timer.first); 	// pq no eliminar directament de m_TimersMap si es compleix la
														// condicio i sino actualitzar?

	// Delete expired timers
	for (auto &l_IDToDelete: l_TimersExpired )
		m_TimersMap.erase(l_IDToDelete);

	// Update remaining timers
	for (auto &l_Timer: m_TimersMap )
		l_Timer.second.Update(m_ElapsedTimeSeconds);
}

bool CTimeManager::KillTimer(const EV_TimerID a_TimerID)
{

	auto l_itToErase =  m_TimersMap.find(a_TimerID);
	if (l_itToErase	== m_TimersMap.end() )
	{
		LOG(LOG_ERROR, LOGSUB_TIMER,"KillTimer - Timer Not Found!");
		return false;
	}
	else
	{
		m_TimersMap.erase(l_itToErase);
		return true;
	}
}

bool CTimeManager::IsEndTimer(const EV_TimerID id) const
{
	auto l_itToErase =  m_TimersMap.find(id);
	if (l_itToErase	== m_TimersMap.end() )
	{
		LOG(LOG_ERROR, LOGSUB_TIMER,"IsTimerIDEnd - Timer Not Found!");
		return false;
	}
	else
		return l_itToErase->second.IsEnd();

}

void CTimeManager::CalculateFPS()
{
	static u32 	l_FPScount = 0;		// Frame counter
	static f64	l_ETAccumSeconds = 0; // Elapsed time seconds accumulation per frame

	++l_FPScount;
	l_ETAccumSeconds += m_ElapsedTimeSeconds;

	if(	l_ETAccumSeconds > 1.0 ) //Every second
	{
		m_FramesPerSecond = l_FPScount/l_ETAccumSeconds;

		l_ETAccumSeconds = l_ETAccumSeconds - 1.0;
		l_FPScount = 0;
	}
}

f64 CTimeManager::GetTotalTimeSeconds() const
{
	return m_TotalTimeSeconds;
}

f64 CTimeManager::GetElapsedTimeSeconds() const
{
	return m_ElapsedTimeSeconds;
}

f64 CTimeManager::GetFPS() const
{
	return m_FramesPerSecond;
}

