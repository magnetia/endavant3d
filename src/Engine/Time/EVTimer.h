#ifndef EV_TIMERINFO_H_
#define EV_TIMERINFO_H_

#include "Core/CBasicTypes.h"
#include "Time/ITimerCallback.h"

class EVTimer
{
public:
	EVTimer(EV_TimerID a_ID, f64 a_TotalTimerInSeconds, bool a_looped, ITimerCallback *a_CallBackObject = nullptr);


	void	Update(f64 a_CurrentTimeInMiliseconds);

	bool	IsEnd() const { return m_ended; };
	bool	IsLoop() const { return m_loop; };
private:
	EVTimer();

	EV_TimerID	m_ID;
	f64			m_currremainingTime;
	f64			m_TotalTimeInMiliseconds;			// Total Ticks that this timer will have to complete.
	
	bool 		m_loop;								// Indicate if the timer will loop
	bool		m_ended;							// Indicate if the timer ended

	ITimerCallback *m_CallBackObject;				// Optional pointer to a function which should be called after the timer is expired

};

#endif