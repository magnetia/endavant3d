#ifndef CTIMEMANAGER_H_
#define CTIMEMANAGER_H_

#include "Core/CBasicTypes.h"
#include "Time/EVTimer.h"
#include "Time/ITimerCallback.h"
#include <set>
#include <list>
#include <map>
#include <memory>
#include <array>

class CTimeManager
{
public:
	static const u32 INVALID_TIMERID;

	CTimeManager();
	virtual ~CTimeManager();

	void StartUp(void);
	void ShutDown(void);
	void Update(f64 dt);

	f64 GetTotalTimeSeconds() const;
	f64 GetElapsedTimeSeconds() const;		//!< Get Delta time in seconds
	f64 GetFPS() const;						//!< Get Frames per second


	// Creates a timer with a given duration time. The function pointer is optional and the function
	// it points to will be called after the timer is expired. If the optional parameter looped is set to true, the timer will
	// be restarted after expiration (until the timer will be deleted with a KillTimer call).
	// Returns the timer identifier, which should be stored, so it is possible to kill/delete the timer with a KillTimer call.
	EV_TimerID CreateTimer(f64 a_DurationInSeconds, bool a_looped = false,  ITimerCallback *a_CallBackObject = nullptr);

	bool 	KillTimer(const EV_TimerID id);			// Kills / Deletes a timer with the given timer identifier.
	bool	IsEndTimer(const EV_TimerID id) const;
		
private:

    // ********************* Elapsed Time


	void	CalculateElapsedTime();
	
	
	u64		m_lastTicks;


	f64		m_LastTimeInMiliseconds;
    f64		m_ElapsedTimeSeconds;			//!< Elapsed time in seconds of the last frame
    f64		m_TotalTimeSeconds;				//!< Tiempo total desde el inicio de la ejecucion


	// ********************* FPS
	void	CalculateFPS();

	f64 m_FramesPerSecond;



	// TIMERS

    typedef std::map<EV_TimerID, EVTimer>		t_TimersInfoMap;
	t_TimersInfoMap								m_TimersMap;

	EV_TimerID									m_CurrentTimerID;

	void UpdateTimers();
};



#endif /* CTIMEMANAGER_H_ */
