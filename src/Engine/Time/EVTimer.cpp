#include "Time/EVTimer.h"

EVTimer::EVTimer(EV_TimerID a_ID, f64 a_TotalTimerInSeconds, bool a_looped, ITimerCallback *a_CallBackObject)
{

	m_ID = a_ID;
	m_TotalTimeInMiliseconds = a_TotalTimerInSeconds;  //Convert to mili-seconds
	m_currremainingTime = m_TotalTimeInMiliseconds;
	m_CallBackObject = a_CallBackObject;
	m_loop = a_looped;
	m_ended = false;
}

void EVTimer::Update(f64 a_dt)
{
	if (!m_ended)
	{

		m_currremainingTime -= a_dt;
		// Timer ended?
		if (m_currremainingTime <= 0.0)
		{
			// Callback function
			if (m_CallBackObject != nullptr)
				m_CallBackObject->CallBack(m_ID);

			m_ended = true;

			// If is a loop timer prepare the next iteration
			if (m_loop)
			{
				m_currremainingTime += m_TotalTimeInMiliseconds;	// Add the total time of the timer again
			}

		}
	}
	else //The timer looped
		m_ended = false;
}