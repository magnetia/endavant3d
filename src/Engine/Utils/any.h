#ifndef EDV_UTIL_ANY_H_
#define EDV_UTIL_ANY_H_

#include <string>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <Utils/generic.hpp>
#include "Core/CBasicTypes.h"

// #todo: poder fer servir manipuladors (std::dec, std::hex, etc.)

namespace edv
{
	class any
	{
	public:
		any(const std::string& a_value = "");
		any(const any& a_other);
		template <typename T> any(const T& a_value);
		template <typename T> void setValue(const T& a_value);
		template <> void setValue(const any& a_other);

		template <typename T> operator T() const;
		template <typename T> T as() const;

		template <> bool as<bool>() const;
		template <> std::string as<std::string>() const;

	private:
		friend std::ostream& operator<<(std::ostream& a_out, const any& a_input);

		std::string m_value;
	};

	template <typename T>
	any::any(const T& a_value)
	{
		setValue(a_value);
	}

	template <typename T>
	void any::setValue(const T& a_value)
	{
		std::stringstream ss;
		ss << a_value;
		m_value = ss.str();
	}

	template <>
	void any::setValue(const any& a_other)
	{
		m_value = a_other.m_value;
	}

	template <typename T>
	any::operator T() const
	{
		return as<T>();
	}

	template <typename T>
	T any::as() const
	{
		std::string valueStr = m_value;

		if (m_value.empty() && std::is_arithmetic<T>::value)
		{
			valueStr = "0";
		}

		T value;
		std::stringstream ss;

		ss << valueStr;
		ss >> value;

		return value;
	}

	template <>
	bool any::as<bool>() const
	{
		bool value = true;
		std::string lower;
		std::transform(m_value.begin(), m_value.end(), std::back_inserter(lower), ::tolower);

		if (m_value == "" || m_value == "0" || m_value == "false")
		{
			value = false;
		}

		return value;
	}

	template <>
	std::string any::as<std::string>() const
	{
		return m_value;
	}

	// edv::empty specialization ( #todo: fer que funcionin be les especialitzacions amb gcc [en que ens hem convertit?])
	template <>
	inline bool empty(const any& a_input)
	{
		return a_input.as<std::string>().empty();
	}

	std::ostream& operator<<(std::ostream& a_out, const any& a_input);
}

#endif
