#ifndef EDV_UTILS_GENERIC_H_
#define EDV_UTILS_GENERIC_H_

// endavant, sense mirar mai enrere.

#include <type_traits>
#include <limits>
#include <string>
#include <cmath>

namespace
{
	template <typename T>
	void throw_if_bad_range(const T& a_min, const T& a_max)
	{
		if (a_min >= a_max)
		{
			throw std::runtime_error{ "bad argument" };
		}
	}
}

namespace edv
{
	template <typename T>
	bool empty(const T& a_input)
	{
		static_assert(
			std::is_fundamental<T>::value ||
			std::is_pointer<T>::value ||
			std::is_member_object_pointer<T>::value ||
			std::is_member_function_pointer<T>::value, 
				"This type requires specialization");

		return a_input == 0;
	}

	template <>
	inline bool empty(const std::string& a_input)
	{
		return a_input.empty();
	}

	template <typename T>
	typename std::enable_if<std::is_floating_point<T>::value, T>::type random(const T& a_min, const T& a_max)
	{
		throw_if_bad_range(a_min, a_max);

		return a_min + static_cast<T>(std::rand()) / (static_cast<T>(RAND_MAX / (a_max - a_min)));
	}

	template <typename T>
	typename std::enable_if<std::is_integral<T>::value, T>::type random(const T& a_min, const T& a_max)
	{ 
		throw_if_bad_range(a_min, a_max);

		return a_min + static_cast<T>(std::rand()) % (a_max - a_min);
	}

	template <typename T>
	T increment_enum(T a_enum)
	{
		return static_cast<T>(static_cast<s32>(a_enum)+1);
	}

	template <typename T>
	struct range
	{
		T min;
		T max;

		range(const T& a_min, const T& a_max) : min{ a_min }, max{ a_max } { throw_if_bad_range(min, max); }
		range() : range(std::numeric_limits<T>::min(), std::numeric_limits<T>::max()) { }
		template <typename U> range(const range<U>& a_other) : range(static_cast<T>(a_other.min), static_cast<T>(a_other.max)) { }
		T random() { return edv::random(min, max); }
	};

	typedef range<int>			range32;
	typedef range<long long>	range64;
	typedef range<float>		range32f;
	typedef range<double>		range64f;
}

#endif
