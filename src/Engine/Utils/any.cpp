#include "Script/ScriptManager.h"
#include "any.h"

namespace edv
{
	any::any(const std::string& a_value) :
		m_value{ a_value }
	{

	}

	any::any(const any& a_other) :
		m_value{ a_other.m_value }
	{

	}

	std::ostream& operator<<(std::ostream& a_out, const any& a_input)
	{
		a_out << a_input.as<std::string>();

		return a_out;
	}
}
