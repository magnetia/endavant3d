#ifndef EDV_UTIL_TREE_H_
#define EDV_UTIL_TREE_H_

#include <string>
#include <initializer_list>
#include <list>
#include <exception>
#include <algorithm>
#include <iterator>
#include <functional>
#include "stuff.h"

#define DEFAULT_SEPARATOR "/"

namespace edv
{
	template <class Value, template <class, class> class Cont = std::list>
	class tree
	{
	public:
		// Compte amb agafar referencies fent servir vectors. El buffer intern 
		// del vector pot canviar la seva ubicacio.
		typedef tree<Value, Cont>								tree_type;
		typedef Cont<tree_type, std::allocator<tree_type>>		children_type;
		typedef Cont<std::string, std::allocator<std::string>>	keys_type;

		class tree_exception : public std::exception
		{
		public:
			tree_exception(const std::string& a_what) : m_what{ a_what } { }
			const char* what() const { return m_what.c_str(); }
		private:
			const std::string m_what;
		};

		// initialization
		tree(const std::string& a_key = DEFAULT_SEPARATOR,
			const Value& a_value = {},
			const std::string& a_separator = DEFAULT_SEPARATOR,
			std::initializer_list<tree_type>&& a_children = {});
		tree(const tree_type& a_other);
		tree(tree_type&& a_other);
		void reset(const std::string& a_key = DEFAULT_SEPARATOR,
			const Value& a_value = {},
			const std::string& a_separator = DEFAULT_SEPARATOR,
			std::initializer_list<tree_type>&& a_children = {});

		// value getters
		const Value& get(const std::string& a_path) const;
		Value& get_ref(const std::string& a_path);
		Value operator[](const std::string& a_path) const;
		Value try_get(const std::string& a_path, const Value& a_default) const;

		// tree getters
		const tree_type& get_tree(const std::string& a_path) const;
		tree_type& get_tree_ref(const std::string& a_path);
		tree_type try_get_tree(const std::string& a_path) const;

		// other getters
		bool path_exists(const std::string& a_path) const;
		const std::string& key() const;
		const Value& value() const;
		tree_type& back();
		bool leaf() const;
		bool empty() const;
		keys_type keys() const;

		// tree modifiers
		void push_back(const tree_type& a_tree);
		void emplace_back(tree_type&& a_tree);
		void clear();
		Value& add_path(const std::string& a_path, const Value& a_value);

	private:
		std::string remove_first_separator(const std::string& a_path) const;
		const tree_type* const get_tree_ptr(const std::string& a_path) const;

		std::string		m_key;
		Value			m_value;
		std::string		m_separator;
		children_type	m_children;
	};

	template <class Value, template <class, class> class Cont>
	tree<Value, Cont>::tree(const std::string& a_key = DEFAULT_SEPARATOR, const Value& a_value = {},
		const std::string& a_separator = DEFAULT_SEPARATOR, std::initializer_list<tree_type>&& a_children = {})
	{
		reset(a_key, a_value, a_separator, std::move(a_children));
	}

	template <class Value, template <class, class> class Cont>
	tree<Value, Cont>::tree(const tree_type& a_other) :
		m_key{ a_other.m_key }, m_value{ a_other.m_value }, m_separator{ a_other.m_separator }, m_children{ a_other.m_children } { }

	template <class Value, template <class, class> class Cont>
	tree<Value, Cont>::tree(tree_type&& a_other)
	{
		std::swap(m_key, a_other.m_key);
		std::swap(m_value, a_other.m_value);
		std::swap(m_separator, a_other.m_separator);
		std::swap(m_children, a_other.m_children);
	}

	template <class Value, template <class, class> class Cont>
	const Value& tree<Value, Cont>::get(const std::string& a_path) const
	{
		return get_tree(a_path).value();
	}

	template <class Value, template <class, class> class Cont>
	Value& tree<Value, Cont>::get_ref(const std::string& a_path)
	{
		return const_cast<Value&>(get(a_path));
	}

	template <class Value, template <class, class> class Cont>
	Value tree<Value, Cont>::operator[](const std::string& a_path) const
	{
		Value value;

		if (const tree_type* const tree_ptr = get_tree_ptr(a_path))
		{
			value = tree_ptr->value();
		}

		return value;
	}

	template <class Value, template <class, class> class Cont>
	Value tree<Value, Cont>::try_get(const std::string& a_path, const Value& a_default) const
	{
		Value value{ a_default };

		if (const tree_type* const tree_ptr = get_tree_ptr(a_path))
		{
			value = tree_ptr->value();
		}

		return value;
	}

	template <class Value, template <class, class> class Cont>
	const typename tree<Value, Cont>::tree_type& tree<Value, Cont>::get_tree(const std::string& a_path) const
	{
		if (!a_path.empty())
		{
			if (const tree_type* const tree_ptr = get_tree_ptr(a_path))
			{
				return *tree_ptr;
			}

			throw tree_exception{ "path not found" };
		}

		throw tree_exception{ "invalid parameter" };
	}

	template <class Value, template <class, class> class Cont>
	typename tree<Value, Cont>::tree_type& tree<Value, Cont>::get_tree_ref(const std::string& a_path)
	{
		return const_cast<tree_type&>(get_tree(a_path));
	}

	template <class Value, template <class, class> class Cont>
	typename tree<Value, Cont>::tree_type tree<Value, Cont>::try_get_tree(const std::string& a_path) const
	{
		if (!a_path.empty())
		{
			if (const tree_type* const tree_ptr = get_tree_ptr(a_path))
			{
				return *tree_ptr;
			}

			return tree_type{};
		}

		throw tree_exception{ "invalid parameter" };
	}

	template <class Value, template <class, class> class Cont>
	bool tree<Value, Cont>::path_exists(const std::string& a_path) const
	{
		bool exists = false;

		if (!a_path.empty())
		{
			if (const tree_type* const tree_ptr = get_tree_ptr(a_path))
			{
				exists = true;
			}
		}

		return exists;
	}

	template <class Value, template <class, class> class Cont>
	const std::string& tree<Value, Cont>::key() const
	{
		return m_key;
	}

	template <class Value, template <class, class> class Cont>
	const Value& tree<Value, Cont>::value() const
	{
		return m_value;
	}

	template <class Value, template <class, class> class Cont>
	void tree<Value, Cont>::reset(const std::string& a_key = DEFAULT_SEPARATOR, const Value& a_value = {},
		const std::string& a_separator = DEFAULT_SEPARATOR, std::initializer_list<tree_type>&& a_children = {})
	{
		m_key = a_key;
		m_value = a_value;
		m_separator = a_separator;
		m_children = a_children;
	}

	template <class Value, template <class, class> class Cont>
	void tree<Value, Cont>::push_back(const tree_type& a_tree)
	{
		m_children.push_back(a_tree);
	}

	template <class Value, template <class, class> class Cont>
	void tree<Value, Cont>::emplace_back(tree_type&& a_tree)
	{
		m_children.emplace_back(a_tree);
	}

	template <class Value, template <class, class> class Cont>
	typename tree<Value, Cont>::tree_type& tree<Value, Cont>::back()
	{
		return m_children.back();
	}

	template <class Value, template <class, class> class Cont>
	bool tree<Value, Cont>::leaf() const
	{
		return m_children.empty();
	}

	template <class Value, template <class, class> class Cont>
	bool tree<Value, Cont>::empty() const
	{
		return m_key.empty() && leaf();
	}

	template <class Value, template <class, class> class Cont>
	typename tree<Value, Cont>::keys_type tree<Value, Cont>::keys() const
	{
		keys_type keys;
		std::transform(m_children.begin(), m_children.end(), std::back_inserter(keys), [](const tree_type& t){ return t.key(); });

		return keys;
	}

	template <class Value, template <class, class> class Cont>
	void tree<Value, Cont>::clear()
	{
		m_key.clear();
		m_children.clear();
	}

	template <class Value, template <class, class> class Cont>
	Value& tree<Value, Cont>::add_path(const std::string& a_path, const Value& a_value)
	{
		std::string path{ a_path };
		std::list<std::string> paths_to_add;
		tree_type* tree{ nullptr };

		do
		{
			tree = const_cast<tree_type*>(get_tree_ptr(path));

			if (!tree)
			{
				const std::string::size_type barPos{ path.find_last_of('/') };

				if (barPos != std::string::npos)
				{
					if (path.size() > barPos)
					{
						std::string copy{ path };
						path = copy.substr(0, barPos);
						paths_to_add.push_back(copy.substr(barPos + 1, copy.size() - barPos));
					}
					else
					{
						path.clear();
					}
				}
				else
				{
					paths_to_add.push_back(path);
					path.clear();
				}
			}
		} 
		while (!path.empty() && !tree);

		if (paths_to_add.empty())
		{
			Value& target{ get_ref(a_path) };
			target = a_value;

			return target;
		}

		if (!tree)
		{
			tree = this;
		}

		const std::string varName{ paths_to_add.front() };
		paths_to_add.pop_front();

		while (!paths_to_add.empty())
		{
			tree->emplace_back({ paths_to_add.back() });
			paths_to_add.pop_back();
			tree = &tree->back();
		}

		tree->emplace_back({ varName, a_value });

		return tree->back().m_value;
	}

	template <class Value, template <class, class> class Cont>
	std::string tree<Value, Cont>::remove_first_separator(const std::string& a_path) const
	{
		std::string path{ a_path };
		auto sep = path.find(m_separator);

		if (sep != std::string::npos)
		{
			sep = sep + 1 < path.size() ? sep + 1 : sep;
			const std::string next_step = path.substr(0, sep);

			if (next_step == m_separator)
			{
				path.erase(path.begin(), path.begin() + sep);
			}
		}

		return path;
	}

	template <class Value, template <class, class> class Cont>
	const typename tree<Value, Cont>::tree_type* const tree<Value, Cont>::get_tree_ptr(const std::string& a_path) const
	{
		std::string path{ remove_first_separator(a_path) };

		const auto sep = path.find(m_separator);
		const std::string next_step = path.substr(0, sep);

		if (m_key == next_step)
		{
			if (sep == std::string::npos)
			{
				return this;
			}
			else
			{
				const std::string next_tree = path.substr(sep);

				for (auto& t : m_children)
				{
					if (const tree_type* const tree_ptr = t.get_tree_ptr(next_tree))
					{
						return tree_ptr;
					}
				}
			}
		}
		else
		{
			for (auto& t : m_children)
			{
				if (t.key() == next_step)
				{
					return t.get_tree_ptr(path);
				}
			}
		}

		return nullptr;
	}
}

#endif
