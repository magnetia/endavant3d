#ifndef EDV_UTIL_CHILD_OF_H_
#define EDV_UTIL_CHILD_OF_H_

template <typename T>
class ChildOf
{
public:
	ChildOf(T* a_parent = nullptr) : m_parent(a_parent) { }
	T* getParent() { return m_parent; }
	T* getParent() const { return m_parent; }
	void setParent(T* a_parent) { m_parent = a_parent; }

protected:
	~ChildOf() { }

private:
	T*	m_parent;
};

#endif
