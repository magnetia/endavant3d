#ifndef EDV_UTILS_STUFF_H_
#define EDV_UTILS_STUFF_H_

#include <initializer_list>
#include <vector>
#include <functional>
#include <type_traits>
#include "integer_sequence.hpp"

// can be used to disambiguate between overloaded member function signatures
template <typename Return, typename Class, typename... Args>
struct pointer_to_member
{
	typedef Return (Class::*type)(Args... args);
};

template <typename Return, typename Class, typename... Args>
struct const_pointer_to_member
{
	typedef Return(Class::*type)(Args... args) const;
};

template <typename T>
class delegate
{
public:
	// #todo: variadic number of arguments
	template <typename Class>
	void add(void(Class::*a_membFunc)(T), Class* a_instance)
		{ m_list.push_back(std::bind(a_membFunc, a_instance, std::placeholders::_1)); }

	void add(void(*a_func)(T))
		{ m_list.push_back(std::bind(a_func, std::placeholders::_1)); }

	void notify(T a_msg)
		{ for (auto function : m_list){ function(a_msg); } }

	void clear()
		{ m_list.clear() }

private:
	typedef std::function<void(T)>		function_type;
	typedef std::vector<function_type>	list_type;

	list_type m_list;
};

//// taken from http://stackoverflow.com/questions/21192659/variadic-templates-and-stdbind ////////////////////////////////
//template<int> // begin with 0 here!
//struct placeholder_template
//{};
//
//namespace std
//{
//	template<int N>
//	struct is_placeholder< placeholder_template<N> >
//		: integral_constant<int, N + 1> // the one is important
//	{};
//}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//// no instanciar de moment!!! reventa el compilador xDDD
//template <typename... Args>
//class delegate2
//{
//public:
//	template <typename Class>
//	void add(void(Class::*a_membFunc)(Args...), Class* a_instance)
//	{
//		add(a_membFunc, a_instance, kos::make_integer_sequence<s32, sizeof...(Args)>{});
//	}
//
//	void add(void(*a_func)(Args...))
//	{
//		add(a_func, kos::make_integer_sequence<s32, sizeof...(Args)>{});
//	}
//
//	void notify(Args... a_msg)
//	{
//		for (auto function : m_list){ function(a_msg...); }
//	}
//
//	void clear()
//	{
//		m_list.clear()
//	}
//
//private:
//	template <typename Class, s32... Is>
//	void add(void(Class::*a_membFunc)(Args...), Class* a_instance, kos::integer_sequence<s32, Is...>)
//	{
//		m_list.push_back(std::bind(a_membFunc, a_instance, placeholder_template<Is>{}...));
//	}
//
//	template <s32... Is>
//	void add(void(*a_func)(Args...), kos::integer_sequence<s32, Is...>)
//	{
//		m_list.push_back(std::bind(a_func, placeholder_template<Is>{}...));
//	}
//
//	typedef std::function<void(Args...)>	function_type;
//	typedef std::vector<function_type>		list_type;
//
//	list_type m_list;
//};

#endif
