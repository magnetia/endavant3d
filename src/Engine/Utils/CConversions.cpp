#include "CConversions.h"

std::string tail(const std::string& a_path, const std::string& a_separator)
{
	std::string t;
	auto sep = a_path.find_last_of(a_separator);

	if (sep != std::string::npos)
	{
		t = a_path.substr(sep + 1);
	}

	return t;
}
