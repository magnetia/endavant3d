#ifndef EDV_UTIL_ID_GENERATOR_H_
#define EDV_UTIL_ID_GENERATOR_H_

#include <type_traits>

// #todo: prendre mesures quan es desborda

template <typename T>
class IdGenerator
{
public:
	typedef T value_type;

	static const T INVALID_ID = static_cast<T>(0);

	IdGenerator();

	void reset();
	T getCurrentId() const;
	T nextId();

private:
	T m_currentId;
};

template <typename T>
IdGenerator<T>::IdGenerator()
{
	static_assert(std::is_integral<T>::value, "wrapped type must be integral");
	reset();
}

template <typename T>
void IdGenerator<T>::reset()
{
	m_currentId = static_cast<T>(0);
}

template <typename T>
T IdGenerator<T>::getCurrentId() const
{
	return m_currentId;
}

template <typename T>
T IdGenerator<T>::nextId()
{
	return ++m_currentId;
}

#endif
