#ifndef EDV_UTILS_BUFFER_H_
#define EDV_UTILS_BUFFER_H_

#include <cstring>
#include <string>
#include <memory>
#include "Core/CBasicTypes.h"

#define DYNAMIC_SIZE	0

namespace edv {
	template <u32 Capacity>
	class buffer_storage
	{
	public:
		u8* get() { return storage_; }
		void allocate(u32 a_capacity) { }
		void release() { }
		void replace(u8* a_raw) { }

	private:
		u8 		storage_[Capacity];
	};

	template <>
	class buffer_storage<DYNAMIC_SIZE>
	{
	public:
		typedef std::unique_ptr<u8[]> Ptr;

		u8* get() { return storage_.get(); }
		void allocate(u32 a_capacity) { storage_.reset(new u8[a_capacity]); }
		void release() { storage_.reset(); }
		void replace(u8* a_raw) { storage_.reset(a_raw); }
	private:
		Ptr storage_;
	};

	template <u32 Capacity = DYNAMIC_SIZE>
	class buffer
	{
	public:
		typedef buffer_storage<Capacity> storage_type;

		buffer(u32 capacity = Capacity);
		template <u32 Rcapacity> buffer(const buffer<Rcapacity>& other);
		// #todo: move constructor
		template <typename T, typename... Args> buffer(const T& first, const Args&... args);
		~buffer();

		void rewind();
		void reset();
		void clear();
		template <typename T> bool write(const T& orig, u32 size = sizeof(T));
		bool write(const std::string& str);
		template <u32 StrSize> bool write(char const (&orig)[StrSize]);
		template <typename T, typename... Args> bool write_n(const T& first, const Args&... arg);
		template <typename T> bool read(T* dst, u32 size = sizeof(T));
		bool read(std::string* dst);
		template <typename T, typename... Args> bool read_n(T* first, Args*... args);
		u32 size() const;
		u32 capacity() const;
		u8* data();
		const u8* data() const;
		template <u32 Rcapacity> buffer& operator=(const buffer<Rcapacity>& other);
		bool fits(u32 size) const;
		void resize(u32 capacity);

	private:
		void init(u32 capacity);
		void grow(u32 hint);
		bool dynamic() const;
		bool write_n();
		bool read_n();

		u32 			index_;
		u32 			size_;
		u32 			capacity_;
		storage_type	storage_;
	};

	typedef buffer<DYNAMIC_SIZE> dynamic_buffer;

	template <u32 Capacity>
	buffer<Capacity>::buffer(u32 capacity) :
		size_(0),
		capacity_(Capacity)
	{
		init(capacity);
	}

	template <u32 Capacity>
	template <typename T, typename... Args>
	buffer<Capacity>::buffer(const T& first, const Args&... args) :
		size_(0),
		capacity_(Capacity)
	{
		init(capacity_);
		write_n(first, args...);
	}

	template <u32 Capacity>
	template <u32 Rcapacity>
	buffer<Capacity>::buffer(const buffer<Rcapacity>& other)
	{
		*this = other;
	}

	template <u32 Capacity>
	buffer<Capacity>::~buffer()
	{
		if (dynamic())
		{
			storage_.release();
		}
	}

	template <u32 Capacity>
	void buffer<Capacity>::rewind()
	{
		index_ = 0;
	}

	template <u32 Capacity>
	void buffer<Capacity>::reset()
	{
		rewind();
		size_ = 0;
	}

	template <u32 Capacity>
	void buffer<Capacity>::clear()
	{
		if (dynamic())
			capacity_ = 0;

		init(capacity_);
	}

	template <u32 Capacity>
	u32 buffer<Capacity>::size() const
	{
		return size_;
	}

	template <u32 Capacity>
	u32 buffer<Capacity>::capacity() const
	{
		return capacity_;
	}

	template <u32 Capacity>
	template <typename T>
	bool buffer<Capacity>::write(const T& orig, u32 size)
	{
		if (dynamic() && !fits(size))
		{
			grow(size);
		}

		if (fits(size))
		{
			memcpy(storage_.get() + index_, &orig, size);
			index_ += size;

			if (index_ > size_)
				size_ += size;

			return true;
		}

		return false;
	}

	template <u32 Capacity>
	template <u32 StrSize>
	bool buffer<Capacity>::write(char const (&orig)[StrSize])
	{
		return write(std::string(orig));
	}

	template <u32 Capacity>
	bool buffer<Capacity>::write(const std::string& str)
	{
		const u16 str_sz = str.size();	// max string size: 65535 chars
		return
			write(str_sz) &&
			write(*str.c_str(), str_sz);
	}

	template <u32 Capacity>
	bool buffer<Capacity>::write_n()
	{
		return true;
	}

	template <u32 Capacity>
	template <typename T, typename... Args>
	bool 	buffer<Capacity>::write_n(const T& first, const Args&... args)
	{
		return
			write(first) &&
			write_n(args...);
	}

	template <u32 Capacity>
	template <typename T>
	bool buffer<Capacity>::read(T* dst, u32 size)
	{
		if (fits(size))
		{
			memcpy(dst, storage_.get() + index_, size);
			index_ += size;
			return true;
		}

		return false;
	}

	template <u32 Capacity>
	bool buffer<Capacity>::read(std::string* dst)
	{
		u16 str_sz = 0;

		if (read(&str_sz))
		{
			std::unique_ptr<char[]> b(new char[str_sz + 1/*\0*/]);
			if (read(b.get(), str_sz))
			{
				b.get()[str_sz] = 0;
				*dst = b.get();
				return true;
			}
		}

		return false;
	}

	template <u32 Capacity>
	bool buffer<Capacity>::read_n()
	{
		return true;
	}

	template <u32 Capacity>
	template <typename T, typename... Args>
	bool	buffer<Capacity>::read_n(T* first, Args*... args)
	{
		return
			read(first) &&
			read_n(args...);
	}

	template <u32 Capacity>
	u8* buffer<Capacity>::data()
	{
		return storage_.get();
	}

	template <u32 Capacity>
	const u8* buffer<Capacity>::data() const
	{
		return storage_.get();
	}

	template <u32 Capacity>
	template <u32 Rcapacity>
	buffer<Capacity>& buffer<Capacity>::operator=(const buffer<Rcapacity>& other)
	{
		if (dynamic())
		{
			storage_.release();
			index_ = other.index_;
			size_ = other.size();
			capacity_ = other.capacity();

			storage_.allocate(capacity_);
			memcpy(storage_.get(), other.data(), capacity_);
		}
		else
		{
			if (Capacity <= other.capacity())
			{
				index_ = other.index_;
				size_ = other.size();
				memcpy(storage_.get(), other.data(), size_);
			}
		}

		return *this;
	}

	template <u32 Capacity>
	void buffer<Capacity>::init(u32 capacity)
	{
		reset();

		if (dynamic())
		{
			storage_.release();
			capacity_ = capacity;
			if (!capacity_)
				capacity_ = sizeof(u32);
			storage_.allocate(capacity_);
		}
	}

	template <u32 Capacity>
	void buffer<Capacity>::grow(u32 hint)
	{
		const s32 needed = (size_ + hint) - capacity_;

		if (needed > 0)
		{
			if ((capacity_ * 2) > static_cast<u32>(needed))
				capacity_ *= 2;
			else
				capacity_ = (capacity_ + needed) * 2;

			u8* data = new u8[capacity_];
			memcpy(data, storage_.get(), size_);
			storage_.replace(data);
		}
	}

	template <u32 Capacity>
	bool buffer<Capacity>::dynamic() const
	{
		return Capacity == DYNAMIC_SIZE;
	}

	template <u32 Capacity>
	bool buffer<Capacity>::fits(u32 size) const
	{
		return index_ + size <= capacity_;
	}

	template <u32 Capacity>
	void buffer<Capacity>::resize(u32 capacity)
	{
		if (!dynamic())
			throw std::runtime_error("attempt to resize a static buffer");

		if (capacity != capacity_)
		{
			u8* data = new u8[capacity];

			if (capacity < capacity_)
			{
				if (size_ > capacity)
					size_ = capacity;
				if (index_ > capacity)
					index_ = capacity;
			}

			memcpy(data, storage_.get(), size_);
			storage_.replace(data);
			capacity_ = capacity;
		}
	}
}
#endif
