#ifndef EDV_UTIL_TREE_FROM_XML_H_
#define EDV_UTIL_TREE_FROM_XML_H_

#include <sstream>
#include "Externals/pugixml/pugixml.hpp"
#include "generic.hpp"
#include "tree.h"

namespace
{
	template <
		class Value, 
		template <class, class> class Cont = std::list>
	void tree_from_xml_rec(
		edv::tree<Value, Cont>& a_tree, 
		const pugi::xml_node& a_parent, 
		const std::string& a_node_name, 
		const std::string& a_element_name, 
		const std::string& a_element_value)
	{
		edv::tree<Value, Cont>& tree = a_tree.leaf() ? a_tree : a_tree.back();

		for (pugi::xml_node node = a_parent.child(a_node_name.c_str());
			node;
			node = node.next_sibling(a_node_name.c_str()))
		{
			tree.emplace_back({ 
				node.attribute(a_element_name.c_str()).as_string(), 
				node.attribute(a_element_value.c_str()).as_string() 
			});

			tree_from_xml_rec(tree, node, a_node_name, a_element_name, a_element_value);
		}
	}

	template <
		class Value, 
		template <class, class> class Cont = std::list>
	void tree_to_xml_rec(
		pugi::xml_node& a_parent,
		const edv::tree<Value, Cont>& a_tree,
		const std::string& a_node_name, 
		const std::string& a_element_name,
		const std::string& a_element_value)
	{
		// for each subtree
		edv::tree<Value, Cont>::keys_type keys = a_tree.keys();

		for (const std::string& key : keys)
		{
			const edv::tree<Value, Cont>& tree = a_tree.get_tree(key);

			// append name
			pugi::xml_node node = a_parent.append_child(a_node_name.c_str());
			node.append_attribute(a_element_name.c_str()).set_value(key.c_str());

			// append value if present
			const Value& value = tree.value();

			if (!edv::empty(value))
			{
				std::stringstream ss;
				std::string str_repr;

				ss << value;
				str_repr = ss.str();

				node.append_attribute(a_element_value.c_str()).set_value(str_repr.c_str());
			}

			// repeat
			tree_to_xml_rec(node, tree, a_node_name, a_element_name, a_element_value);
		}
	}
}

namespace edv
{
	template <
		class Value, 
		template <class, class> class Cont = std::list>
	void tree_from_xml(
		tree<Value, Cont>& a_tree, 
		const pugi::xml_node& a_node, 
		const std::string& a_node_name = "Node", 
		const std::string& a_element_name = "name",
		const std::string& a_element_value = "value")
	{
		tree_from_xml_rec(a_tree, a_node, a_node_name, a_element_name, a_element_value);
	}

	template <
		class Value,
		template <class, class> class Cont = std::list>
	void tree_from_xml_file(
		tree<Value, Cont>& a_tree,
		const std::string& a_file_name,
		const std::string& a_root_node_name,
		const std::string& a_node_name = "Node",
		const std::string& a_element_name = "name",
		const std::string& a_element_value = "value")
	{
		a_tree.clear();
		pugi::xml_document document;

		if (pugi::xml_parse_result result = document.load_file(a_file_name.c_str()))
		{
			edv::tree_from_xml(a_tree, document.child(a_root_node_name.c_str()), a_node_name, a_element_name, a_element_value);
		}
	}

	template <
		class Value, 
		template <class, class> class Cont = std::list>
	void tree_to_xml(
		pugi::xml_node& a_node,
		const tree<Value, Cont>& a_tree,
		const std::string& a_node_name = "Node",
		const std::string& a_element_name = "name",
		const std::string& a_element_value = "value")
	{
		tree_to_xml_rec(a_node, a_tree, a_node_name, a_element_name, a_element_value);
	}

	template <
		class Value,
		template <class, class> class Cont = std::list>
	void tree_to_xml_file(
		const std::string& a_file_name,
		const std::string& a_root_node_name,
		const tree<Value, Cont>& a_tree,
		const std::string& a_node_name = "Node",
		const std::string& a_element_name = "name",
		const std::string& a_element_value = "value")
	{
		std::ofstream output{ a_file_name.c_str(), std::ios_base::out };

		if (output.good())
		{
			// create xml document
			pugi::xml_document document;
			pugi::xml_node root = document.append_child(a_root_node_name.c_str());

			edv::tree_to_xml(root, a_tree, a_node_name, a_element_name, a_element_value);

			// dump to file
			std::stringstream contents;

			document.print(contents);

			const std::string contents_str = contents.str();
			std::ostreambuf_iterator<char> out_it{ output };

			std::copy(contents_str.begin(), contents_str.end(), out_it);
		}
	}
}

#endif
