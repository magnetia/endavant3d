/*
 * Conversions.h
 *
 *  Created on: 09/03/2012
 *      Author: dani
 */

#ifndef CONVERSIONS_H_
#define CONVERSIONS_H_

#include "Core\CBasicTypes.h"
#include <sstream>
#include <vector>
#include <string>

//Passa Qualsevol tipus de dades a String
template <class T> inline std::string to_string (const T& t)
{
	std::stringstream ss;
	ss << t;
	return ss.str();
}

inline void StringExplode(std::string InputString, std::vector<std::string> &AllTokens, char TokenChar)
{
    std::istringstream iss(InputString);
    while (!iss.eof())
    {
      std::string token;
      getline( iss, token, TokenChar );
      AllTokens.push_back( token );
    }
}

inline s32 StringToNumber(const std::string &Text)
{                               
	std::stringstream ss(Text);
	s32 result;
	return ss >> result ? result : 0;
}

std::string tail(const std::string& a_path, const std::string& a_separator = "/");

namespace edv
{
	template <typename Char>
	inline void clear_stream(std::basic_stringstream<Char>& a_stream)
	{
		a_stream.str(std::basic_string<Char>());
		a_stream.clear();
	}
}

#endif /* CONVERSIONS_H_ */
