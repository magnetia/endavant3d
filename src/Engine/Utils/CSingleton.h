#ifndef SINGLETON_H_
#define SINGLETON_H_

template <typename Type>
class CSingleton
{
public:
	static Type& Instance()
	{
		return Get();
	}

	static Type* Pointer()
	{
		return &Get();
	}

protected:
	CSingleton() { }
	~CSingleton() { }

private:
	CSingleton(CSingleton const&) { }
	void operator=(CSingleton const&) { }

	static Type& Get()
	{
		static Type instance;
		return instance;
	}
};

#endif
