#ifndef EDV_UTIL_FACTORY_H_
#define EDV_UTIL_FACTORY_H_

template <typename Class, typename Param>
class Factory
{
public:
	virtual ~Factory() { }
	virtual Class* createElement(const Param& a_param) = 0;
};

#endif
