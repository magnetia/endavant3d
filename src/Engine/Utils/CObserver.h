#ifndef EDV_OBSERVER_H_
#define EDV_OBSERVER_H_

#include <set>

class IObserver
{
public:
	virtual ~IObserver() { }

	virtual void notify(void*) = 0;
};

class CObservable
{
public:
	virtual ~CObservable() { }

	void addObserver(IObserver* a_Observer)
		{ m_Observers.insert(a_Observer); }
	void removeObserver(IObserver* a_Observer)
		{ m_Observers.erase(a_Observer); }

protected:
	void notifyAll()
		{ for (auto observer : m_Observers) observer->notify(this); }

private:
	typedef std::set<IObserver*> t_Observers;

	t_Observers m_Observers;
};

#endif
