#include "BoardMenuManager.h"
#include "Core/CCoreEngine.h"
#include "Input/CInputManager.h"
#include "State/CMainStateManager.h"
#include "Sound/CSoundManager.h"
#include "Core/CLogManager.h"
#include "Renderer/CRenderManager.h"
#include "Game/FlyStraightAnimator.h"



#include "BoardMenuManager.h"
#include "BoardButtonConfig.h"
#include "ChangeStateButton.h"
#include <iostream>






BoardMenuManager::BoardMenuManager() :
m_menustatemanager{ "BoardMenuManager" },
	m_parentNode{ nullptr },
	m_previousCamera{ nullptr },
	m_mouseWasRelative{ false }
	
{
	CCoreEngine::Instance().GetMainStateManager().RegisterStateManager(&m_menustatemanager);
}

BoardMenuManager::~BoardMenuManager()
{
	CCoreEngine::Instance().GetMainStateManager().UnregisterStateManager(&m_menustatemanager);
}

void	BoardMenuManager::StartUp()
{
	
}

void	BoardMenuManager::ShutDown()
{

}

void	BoardMenuManager::Render()
{

}

void 	BoardMenuManager::Update(f64 dt)
{
	if (m_menustatemanager.TopState())
		m_menustatemanager.TopState()->Update(dt);

}


void BoardMenuManager::load(const std::string& a_xmlPath)
{
	pugi::xml_document document;
	m_parentNode = CCoreEngine::Instance().GetRenderManager().GetSceneManager()->getRootSceneNode()->createChildSceneNode("canviarsobretot_parent");
	auto result = document.load_file(a_xmlPath.c_str());
	std::cout << result.description();
	if (result)
	{
		if (pugi::xml_node boardMenuNode = document.child("board_menu"))
		{
			

			LoadCamera(boardMenuNode);
			LoadBackground(boardMenuNode);
			LoadFontsConfig(boardMenuNode);
			LoadBaseButtonConfig(boardMenuNode);
			LoadMenus(boardMenuNode);
			setVisible(false);
			
		
		}
		else
		{
			//TODO ERRROR
		}
		
	}
	else
	{
		//LOG(LOG_ERROR, LOGSUB_GUI, "error loading xml file: %s", result.description());
	}
}

void BoardMenuManager::LoadCamera(pugi::xml_node &boardMenuNode)
{
	auto& renderMgr = CCoreEngine::Instance().GetRenderManager();

	// save previous camera
	m_previousCamera = renderMgr.GetOgreViewport()->getCamera();

	// load camera
	Ogre::Real nearClip = 0.1f, farClip = 9000.f;
	Ogre::Vector3 cameraPos{ 0, 0, 40 }, lookAt{ 0, 0, 0 };
	auto cameraNode = boardMenuNode.child("camera");

	if (!cameraNode.empty())
	{
		nearClip = cameraNode.attribute("near_clip").as_float(nearClip);
		farClip = cameraNode.attribute("far_clip").as_float(farClip);
		readVectorFromXML(cameraNode.child("position"), cameraPos);
		readVectorFromXML(cameraNode.child("lookAt"), lookAt);
	}

	m_menucamera = renderMgr.GetSceneManager()->createCamera(/*GetName()*/ "Canviarsobretot_menu_camera");
	m_menucamera->setPosition(cameraPos);
	m_menucamera->lookAt(lookAt);
	m_menucamera->setNearClipDistance(nearClip);
	m_menucamera->setFarClipDistance(farClip);
	renderMgr.GetOgreViewport()->setCamera(m_menucamera);

	// Alter the camera aspect ratio to match the viewport
	m_menucamera->setAspectRatio(Ogre::Real(renderMgr.GetOgreViewport()->getActualWidth()) / Ogre::Real(renderMgr.GetOgreViewport()->getActualHeight()));


	// set ambient light
	auto scene_mgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();
	scene_mgr->destroyAllLights();
	scene_mgr->setShadowTechnique(Ogre::SHADOWTYPE_NONE);
	scene_mgr->setAmbientLight(Ogre::ColourValue{ 1, 1, 1, 1 });
}


void BoardMenuManager::LoadBackground(pugi::xml_node &boardMenuNode)
{
	auto backgroundNode = boardMenuNode.child("background");

	if (!backgroundNode.empty() && !m_backgroundrect)
	{
		//Creo el fons
		
		const std::string backgroundTexture = backgroundNode.attribute("texture").as_string();
		
		// Create background material
		Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().getByName("GUI_background" + backgroundTexture, "General");
		if (material.isNull())
		{
			material = Ogre::MaterialManager::getSingleton().create("GUI_background_" + backgroundTexture, "General");
			material->getTechnique(0)->getPass(0)->createTextureUnitState(backgroundTexture);
			material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
			material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
			material->getTechnique(0)->getPass(0)->setLightingEnabled(false);
		}

		// Create background rectangle covering the whole screen
		m_backgroundrect.reset( new Ogre::Rectangle2D(true));
		m_backgroundrect->setCorners(-1.0, 1.0, 1.0, -1.0);
		m_backgroundrect->setMaterial("GUI_background_" + backgroundTexture);

		// Render the background before everything else
		m_backgroundrect->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);

		// Use infinite AAB to always stay visible
		Ogre::AxisAlignedBox aabInf;
		aabInf.setInfinite();
		m_backgroundrect->setBoundingBox(aabInf);

		// Attach background to the scene
		m_parentNode->attachObject(m_backgroundrect.get());
	
	}


	auto titleNode = boardMenuNode.child("title");

	if (!titleNode.empty() && !m_titleentity)
	{

		Ogre::Vector3 position;
		Ogre::Vector3 scale;

		readVectorFromXML(titleNode.child("position"), position);
		readVectorFromXML(titleNode.child("scale"), scale);
		EntityOgreGraphics::Config GraphicalConfig(
			"title_GUI",
			position,
			Ogre::Quaternion::IDENTITY,
			true,
			titleNode.attribute("graphic").as_string(),
			m_parentNode,
			"",
			scale
			
			);

		m_titleentity.reset(new GameEntity(GraphicalConfig));
	

		//Create gameentity
		//std::shared_ptr<GameEntity> entity = std::make_shared<GameEntity>(graphconfig, physconfig);
		//entity->getGraphics()->getNode()->setVisible(false);
	}



}


void BoardMenuManager::LoadFontsConfig(pugi::xml_node &boardMenuNode)
{
	auto fontnodecfg = boardMenuNode.child("fonts_config");

	if (!fontnodecfg.empty())
	{
		for (auto fontnode = fontnodecfg.child("font"); fontnode; fontnode = fontnode.next_sibling("font"))
		{
			auto  l_fontcfg = std::make_shared<MenuFontCfg>();

			l_fontcfg->m_idfont = fontnode.attribute("id").as_string();
			l_fontcfg->m_fontname = fontnode.attribute("font").as_string();
			l_fontcfg->m_fontsize = fontnode.attribute("fontsize").as_string();
	
			//Add cfg
			m_fontscfgmap[l_fontcfg->m_idfont] = l_fontcfg;
			CCoreEngine::Instance().GetRenderManager().GetRenderTextManager()->createTTFFont(l_fontcfg->m_idfont, l_fontcfg->m_fontname, l_fontcfg->m_fontsize);
		
		}
	}
}


void BoardMenuManager::LoadBaseButtonConfig(pugi::xml_node &boardMenuNode)
{
	auto buttonsConfigNode = boardMenuNode.child("base_buttons_config");

	if (!buttonsConfigNode.empty())
	{
		for (auto buttoncfgnode = buttonsConfigNode.child("base_button"); buttoncfgnode; buttoncfgnode = buttoncfgnode.next_sibling("base_button"))
		{
			const std::string buttoncfgname = buttoncfgnode.attribute("name").as_string();
			
			Ogre::Vector3 scale(1.0f);
			if (auto buffnode = buttoncfgnode.child("scale"))
			{
				readVectorFromXML(buffnode, scale);
			}

			// create base entity for cloning
			auto graphconfig = std::make_shared<EntityOgreGraphics::Config>(
				buttoncfgname,
				Ogre::Vector3::ZERO,
				Ogre::Quaternion::IDENTITY,
				true,
				buttoncfgnode.attribute("graphic").as_string(),
				m_parentNode,
				buttoncfgnode.attribute("material").as_string(),
				scale
			);

			auto physconfig = std::make_shared<EntityBtPhysics::Config>(
				buttoncfgnode.attribute("mass").as_float(),
				true,	// cinematic
				MagneticProperty::Config(),		// magnet, no config
				false,	// physics2D
				EntityBtPhysics::stringToCollisionShape(buttoncfgnode.attribute("collision").as_string())  // collision config
			);
			
			//Create gameentity
			//std::shared_ptr<GameEntity> entity = std::make_shared<GameEntity>(graphconfig, physconfig);
			//entity->getGraphics()->getNode()->setVisible(false);

			//Create buttonconfig
			auto l_buttoncfg = std::make_shared<BoardButtonConfig>();
			l_buttoncfg->m_graphicsCfg = std::move(graphconfig);
			l_buttoncfg->m_physicsCfg = std::move(physconfig);
			
			//Set optional parameters
			l_buttoncfg->m_fontID = buttoncfgnode.attribute("fontid").as_string();

			if (auto buffnode = buttoncfgnode.child("text_position"))
			{
				readVectorFromXML(buffnode, l_buttoncfg->m_textPos);
			}

			if (auto buffnode = buttoncfgnode.child("caption_offset"))
			{
				readVectorFromXML(buffnode.child("caption_offset"), l_buttoncfg->m_captionoffset);
			}

			if (auto buffnode = buttoncfgnode.child("left_offset"))
			{
				readVectorFromXML(buffnode.child("left_offset"), l_buttoncfg->m_left_offset);
			}
			if (auto buffnode = buttoncfgnode.child("value_offset"))
			{
				readVectorFromXML(buffnode.child("value_offset"), l_buttoncfg->m_value_offset);
			}

			if (auto buffnode = buttoncfgnode.child("right_offset"))
			{
				readVectorFromXML(buffnode.child("right_offset"), l_buttoncfg->m_right_offset);
			}
			
			if (auto buffnode = buttoncfgnode.child("meshes_offset"))
			{
				readVectorFromXML(buffnode.child("meshes_offset"), l_buttoncfg->m_meshes_offset);
			}
			
			if (auto buffnode = buttoncfgnode.child("anim_hover_duration"))
			{
				l_buttoncfg->m_anim_hover_duration = buffnode.attribute("duration").as_float();
			}

			m_buttoncfgmap[buttoncfgname] = l_buttoncfg;
		}
		
	}
}


void BoardMenuManager::LoadMenus(pugi::xml_node &boardMenuNode)
{
	auto menusnode = boardMenuNode.child("menu_config");

	if (!menusnode.empty())
	{
		for (auto menuNode = menusnode.child("menu"); menuNode; menuNode = menuNode.next_sibling("menu"))
		{
			auto l_boardMenu = std::make_shared<BoardMenu>(menuNode.attribute("id").as_string(), this);


			for (auto butt = menuNode.child("button"); butt; butt = butt.next_sibling("button"))
			{
				const std::string butttype = butt.attribute("type").as_string();
				const std::string buttid = butt.attribute("id").as_string();
				auto butcfgbase = getButtonConfig(butttype);
				auto butcfgcloned = butcfgbase->clone();
				//Assigno un nom de Scenenode unic
				butcfgcloned->m_graphicsCfg->nodeName += buttid;

				//Assigno posicio del button
				readVectorFromXML(butt.child("position"), butcfgcloned->m_graphicsCfg->initialPos);
				
				if (butttype == "change_menu_button")
				{
					const std::string next_state = butt.attribute("next_state").as_string();
					const std::string state_manager = butt.attribute("state_manager").as_string();
					const std::string text = butt.attribute("text").as_string();
					butcfgcloned->m_captiontext = text;
					auto newbutt = std::make_shared<ChangeStateButton>(butcfgcloned, buttid, l_boardMenu, state_manager, next_state);
					l_boardMenu->AddButton(newbutt);
					//next_state = "/MainMenu/MenuSelectLevel" state_manager = "MainMenuManager" text = "Start game"
				}
			}

			//Per cada text
			for (auto text_it = menuNode.child("text_menu"); text_it; text_it = text_it.next_sibling("text_menu"))
			{
				const std::string caption = text_it.attribute("caption").as_string();
				const std::string idtext = text_it.attribute("idtext").as_string();
				const std::string fontid= text_it.attribute("fontid").as_string();
				Ogre::Vector3 text_position;
				text_position.x = text_it.attribute("x").as_float();
				text_position.y = text_it.attribute("y").as_float();

				Ogre::Vector3 color;
				color.x = text_it.attribute("r").as_float() / 255.f;
				color.y = text_it.attribute("g").as_float() /255.f;
				color.z = text_it.attribute("b").as_float() /255.f;

				auto newtext = std::make_shared<BoardText>(text_position, caption, idtext, fontid, color);
				l_boardMenu->AddText(newtext);

			}


			addBoardMenu(std::move(l_boardMenu));
		}
	}
}


void BoardMenuManager::addBoardMenu(MenuPtr a_menuptr)
{
	a_menuptr->setVisible(false);
	m_menustatemanager.RegisterState(a_menuptr.get());
	m_menus.emplace_back(std::move(a_menuptr));
	

}


BoardMenuManager::MenuPtr BoardMenuManager::getMenuBoard(const std::string &a_menuID)
{
	for (auto menu : m_menus)
	{
		if (menu->GetName() == a_menuID)
		{
			return menu;
		}
	}

	return nullptr;
}
void	BoardMenuManager::setVisible(const bool a_visible)
{
	m_titleentity->getGraphics()->getNode()->setVisible(a_visible);
	m_backgroundrect->setVisible(a_visible);
	for (auto menu : m_menus)
	{
		menu->setVisible(a_visible);
	}

}
void	BoardMenuManager::showMenuBoard(const std::string &a_menuID)
{
	bool found = false;
	setVisible(false);
	m_backgroundrect->setVisible(true);
	m_titleentity->getGraphics()->getNode()->setVisible(true);

	//Setejo la camera
	auto& renderMgr = CCoreEngine::Instance().GetRenderManager();
	renderMgr.GetOgreViewport()->setCamera(m_menucamera);

	// set ambient light
	auto scene_mgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();
	scene_mgr->destroyAllLights();
	scene_mgr->setShadowTechnique(Ogre::SHADOWTYPE_NONE);
	scene_mgr->setAmbientLight(Ogre::ColourValue{ 1, 1, 1, 1 });

	if (!m_menus.empty())
	{
		//auto& mouse = CCoreEngine::Instance().GetInputManager().GetMouse();
	//	mouse.SetRelativeBehaviour(true);
		for (auto &menu : m_menus)
		{
			if (menu->GetName() == a_menuID)
			{
				menu.get()->setVisible(true);
				m_menustatemanager.ChangeState(menu.get());
				found = true;
			}
		}
	}

	if (!found)
	{
		//Exception TODO not found

	}

}

BoardMenuManager::MenuPtr BoardMenuManager::getcurrentMenuBoard()
{
	std::string cur_name;

	if (m_menustatemanager.TopState())
		cur_name = m_menustatemanager.TopState()->GetName();

	return getMenuBoard(cur_name);
}

std::shared_ptr<BoardButtonConfig> BoardMenuManager::getButtonConfig(const std::string& a_id)
{
	auto it = m_buttoncfgmap.find(a_id);

	if (it != m_buttoncfgmap.end())
	{
		 return it->second;
	}

	std::shared_ptr<BoardButtonConfig> nulll;
	return nulll;
}

void BoardMenuManager::unload()
{
	m_menustatemanager.Clear();

	for (auto it = m_menus.begin(); it != m_menus.end(); it++)
	{
		m_menustatemanager.UnregisterState((*it).get());
	}
	m_menus.clear();


	auto& renderMgr = CCoreEngine::Instance().GetRenderManager();

	if (m_parentNode)
	{
		//Ogre::BillboardSet* set = static_cast<Ogre::BillboardSet*>(m_parentNode->getAttachedObject(0));
		//renderMgr.GetSceneManager()->destroyBillboardSet(set);
		m_parentNode->detachAllObjects();
		m_parentNode->removeAndDestroyAllChildren();
		renderMgr.GetSceneManager()->destroySceneNode(m_parentNode);
		m_parentNode = nullptr;
	}

	// destroy menu camera
	Ogre::Camera* currentCamera = renderMgr.GetOgreViewport()->getCamera();
	renderMgr.GetSceneManager()->destroyCamera(currentCamera);

	// recover previous camera
	if (m_previousCamera)
	{
		renderMgr.GetOgreViewport()->setCamera(m_previousCamera);
		m_previousCamera = nullptr;
	}

	// recover previous mouse state
	//CCoreEngine::Instance().GetInputManager().GetMouse().SetRelativeBehaviour(m_mouseWasRelative);
	//m_mouseWasRelative = false;


	//TODO fer-ho per botons!
	CCoreEngine::Instance().GetSoundManager().releaseSound("menu_beep");
}

void BoardMenuManager::readVectorFromXML(pugi::xml_node& a_node, Ogre::Vector2& a_vector)
{
	if (!a_node.empty())
	{
		a_vector = Ogre::Vector2{
			a_node.attribute("x").as_float(a_vector.x),
			a_node.attribute("y").as_float(a_vector.y)
		};
	}
}

void BoardMenuManager::readVectorFromXML(pugi::xml_node& a_node, Ogre::Vector3& a_vector)
{
	if (!a_node.empty())
	{
		a_vector = Ogre::Vector3{
			a_node.attribute("x").as_float(a_vector.x),
			a_node.attribute("y").as_float(a_vector.y),
			a_node.attribute("z").as_float(a_vector.z)
		};
	}
}
