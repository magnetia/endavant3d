#include "BoardButtonStates.h"
#include "BoardButton.h"
#include "Game\GameEntity.h"
#include "Game\FlyStraightAnimator.h"

const std::string BoardButtonIdleState::STATENAME{ "IDLE" };
const std::string BoardButtonHoverState::STATENAME{ "HOVER" };
const std::string BoardButtonSelectedState::STATENAME{ "SELECTED" };
const std::string BoardButtonFinishState::STATENAME{ "FINISH" };
const std::string BoardButtonEndState::STATENAME{ "END" };

///////////////////
void BoardButtonIdleState::Update(f64 dt)
{
	getBoardButton()->getEntity()->update(dt);
}

///////////////////
void BoardButtonHoverState::Update(f64 dt)
{
	getBoardButton()->getEntity()->update(dt);
}

///////////////////
void BoardButtonSelectedState::Update(f64 dt)
{
	getBoardButton()->getEntity()->update(dt);
}



///////////////////
void BoardButtonFinishState::Start()
{
	m_animation = new FlyStraightAnimator{ 0.15, getBoardButton()->getPosition().to_OgreVector3(), Ogre::Vector3{ 0, 0, 0 } };
	getBoardButton()->getEntity()->addBehavior(m_animation);

}

void BoardButtonFinishState::Update(f64 dt)
{
	if (m_animation->finished())
	{
		getBoardButton()->ChangeButtonState(GUIBUTTON_END);
	}
	else
	{
		getBoardButton()->getEntity()->update(dt);
	}
}
void BoardButtonFinishState::Finish()
{
	auto butt = getBoardButton();
	if (butt)
	{
		auto entity = butt->getEntity();
		if (entity)
		{
			entity->removeAllBehaviors();
		}
	}
}

