#include "TextButton.h"
#include "Core/CCoreEngine.h"
#include "State/CMainStateManager.h"
#include "Game/ChainedBehavior.h"
#include "Game/FlyStraightAnimator.h"
#include "Externals/MovableText/MovableText.h"
#include "BoardMenu.h"
#include "BoardMenuManager.h"
/*
TextButton::TextButton(const std::string& a_id, const std::string& a_text, BoardMenu* a_parent, const Ogre::Vector3& a_position) :
		BoardButton{ a_id, a_parent, a_position },
		m_hovering{ false },
		m_hoverAnimDuration{ 0.5f }
{
	BoardMenuManager* manager = getParent()->getParent();
	pugi::xml_node config = manager->getButtonConfig("callback_button");

	m_entity.reset(manager->cloneBaseWidget(a_id, "callback_button", a_text));
	m_entity->setScale(m_entity->getScale() * 0.8f);

	if (!config.empty())
	{
		m_hoverAnimDuration = config.attribute("anim_hover_duration").as_float(m_hoverAnimDuration);
	}
}

TextButton::TextButton(pugi::xml_node& a_node, BoardMenu* a_parent, const Ogre::Vector3& a_position) :
TextButton{
		a_node.attribute("id").as_string(),
		a_node.attribute("text").as_string(),
		a_parent,
		a_position }
{

}

TextButton::~TextButton()
{

}

void TextButton::init()
{
	if (m_entity)
	{
		m_entity->getGraphics()->getNode()->setVisible(true);
		m_entity->init();
	}
}

void TextButton::update(f64 dt)
{
	if (m_entity)
	{
		m_entity->update(dt);
	}
}

void TextButton::pause()
{
	if (m_entity)
	{
		m_entity->pause();
	}
}

void TextButton::resume()
{
	if (m_entity)
	{
		m_entity->resume();
	}
}

void TextButton::stop()
{
	if (m_entity)
	{
		m_entity->stop();
		m_entity->getGraphics()->getNode()->setVisible(false);
	}
}

void TextButton::animate(f64 a_duration, bool a_show)
{
	if (m_entity)
	{
		Ogre::Vector3 start, end;
		start = end = Ogre::Vector3::ZERO;

		if (a_show)
		{
			end = getPosition();
		}
		else
		{
			start = getPosition();
		}

		m_entity->addBehavior(new FlyStraightAnimator{a_duration, start, end});
	}
}

void TextButton::onActionChange()
{
	
	
}

void TextButton::onHover(bool a_hover)
{
	
	if (a_hover != m_hovering)
	{
		if (a_hover)
		{
			static_cast<Ogre::Entity*>(m_entity->getGraphics()->getNode()->getAttachedObject(0))->setMaterialName("button2_mat/on");

			const Ogre::Vector3 pos1{ m_position.x, m_position.y, m_position.z - 1.f };
			const Ogre::Vector3 pos2{ m_position.x, m_position.y, m_position.z + 1.f };

			m_entity->addBehavior(
				new ChainedBehavior({
					new FlyStraightAnimator{ .4, pos1, pos2 },
					new FlyStraightAnimator{ .4, pos2, pos1 },
			}, -1));
		}
		else
		{
			m_entity->removeAllBehaviors();
			m_entity->getGraphics()->getNode()->setPosition(m_position);
			static_cast<Ogre::Entity*>(m_entity->getGraphics()->getNode()->getAttachedObject(0))->setMaterialName("button2_mat/off");
		}

		m_hovering = a_hover;
	}
	
}

bool TextButton::hasEntity(GameEntity* a_entity) const
{
	return m_entity.get() == a_entity;
}

bool TextButton::keyUpdate(const std::string& a_key)
{

	if (a_key == "Return")
	{
		callOnChangeFunction();
	
	}
	return false;
}

bool TextButton::mouseUpdate(const std::string& a_button, GameEntity* a_entity)
{
	

	if (a_entity == m_entity.get() && a_button == "MOUSE_LB")
	{


		callOnChangeFunction();
		
	}

	return false;
}
*/