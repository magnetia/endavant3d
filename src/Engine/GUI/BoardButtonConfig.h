#ifndef EDV_GUI_BOARD_BUTTON_CONFIG_H_
#define EDV_GUI_BOARD_BUTTON_CONFIG_H_

#include <memory>
#include "Game/EntityBtPhysics.h"
#include "Game/EntityOgreGraphics.h"
#include "Core/CBasicTypes.h"

class Ogre::Vector3;
class BoardButtonConfig
{
public:
	BoardButtonConfig();
	std::string		m_buttonid;
	std::string		m_captiontext;
	std::shared_ptr<EntityOgreGraphics::Config> m_graphicsCfg;
	std::shared_ptr<EntityBtPhysics::Config>	m_physicsCfg;

	Ogre::Vector3	m_textPos;
	std::string		m_fontID;
	f32				m_anim_hover_duration;
	Ogre::Vector3	m_captionoffset;
	Ogre::Vector3	m_left_offset;
	Ogre::Vector3	m_value_offset;
	Ogre::Vector3	m_right_offset;
	Ogre::Vector3	m_meshes_offset;
	std::shared_ptr<BoardButtonConfig> clone();
};



#endif
