#include "InputButton.h"
#include "Core/CCoreEngine.h"
#include "Input/CInputManager.h"
#include "Options/OptionManager.h"
#include "Game/FlyStraightAnimator.h"
#include "Externals/OGRE/Ogre.h"
#include "BoardMenu.h"
#include "BoardMenuManager.h"
#include "Externals/MovableText/MovableText.h"
/*
// #todo: centralitzacio de textes
// #todo: molt codi repetit de OptionButton; pensar en com unificar els controls amb mes d'una entity

const f64 READ_INPUT_DELAY{ .3 };
const std::string CATEGORY{ "/controls" };
const InputButton::BaseTags InputButton::BASE_TAGS{ {
	{ "option_caption_button", "_caption" },
	{ "option_value_button", "_value" }
} };

InputButton::InputButton(const std::string& a_id, const std::string& a_caption, const std::string& a_option, BoardMenu* a_parent, const Ogre::Vector3& a_position) :
	BoardButton{ a_id, a_parent, a_position },
	m_option{ a_option },
	m_caption{ a_caption },
	m_mouseNotifId{ 0 },
	m_keybNotifId{ 0 },
	m_inputTimer{ CTimeManager::INVALID_TIMERID },
	m_meshesOffset{ 8, 0, 0 }
{
	BoardMenuManager* manager = getParent()->getParent();
	setParent(a_parent);

	for (u32 idx = 0; idx < BASE_TAGS.size(); idx++)
	{
		const GraphicsInfo& info = BASE_TAGS[idx];
		const std::string id = a_id + info.second;

		m_entities[idx].reset(manager->cloneBaseWidget(id, info.first, "-"));
	}

	BoardMenuManager::readVectorFromXML(manager->getButtonConfig("input_button").child("meshes_offset"), m_meshesOffset);
}

InputButton::InputButton(pugi::xml_node& a_node, BoardMenu* a_parent, const Ogre::Vector3& a_position) :
	InputButton{ 
		a_node.attribute("id").as_string(), 
		a_node.attribute("caption").as_string(), 
		a_node.attribute("option").as_string(),
		a_parent, 
		a_position }
{

}

void InputButton::init()
{
	m_inputTimer = CTimeManager::INVALID_TIMERID;

	for (u32 idx = 0; idx < BUTTON_MAX; idx++)
	{
		GameEntity* entity = m_entities[idx].get();

		entity->getGraphics()->getNode()->setVisible(true);
		entity->init();

		updateText(static_cast<Button>(idx), idx == BUTTON_CAPTION ? 
			m_caption : CCoreEngine::Instance().GetOptionManager().getOption(CATEGORY + "/" + m_option).as<std::string>());
	}
}

void InputButton::update(f64 dt)
{
	if (m_inputTimer)
	{
		if (CCoreEngine::Instance().GetTimerManager().IsEndTimer(m_inputTimer))
		{
			m_inputTimer = CTimeManager::INVALID_TIMERID;
			auto& mouse = CCoreEngine::Instance().GetInputManager().GetMouse();
			auto& keyboard = CCoreEngine::Instance().GetInputManager().GetKeyboard();

			// esto es C ?!
			m_mouseNotifId = mouse.NotifyNextChangeOfState(EV_MOUSEBUTTON_DOWN, std::bind(&InputButton::mouseCallback, this, std::placeholders::_1));
			m_keybNotifId = keyboard.NotifyNextKeyPressed(std::bind(&InputButton::keyboardCallback, this, std::placeholders::_1));
		}
	}

	for (auto& entityPtr : m_entities)
	{
		entityPtr->update(dt);
	}
}

void InputButton::pause()
{
	for (auto& entityPtr : m_entities)
	{
		entityPtr->pause();
	}
}

void InputButton::resume()
{
	for (auto& entityPtr : m_entities)
	{
		entityPtr->resume();
	}
}

void InputButton::stop()
{
	for (auto& entityPtr : m_entities)
	{
		entityPtr->stop();
		entityPtr->getGraphics()->getNode()->setVisible(false);
	}
}

void InputButton::animate(f64 a_duration, bool a_show)
{
	Ogre::Vector3 offset = Ogre::Vector3::ZERO;

	for (u32 idx = 0; idx < BUTTON_MAX; idx++)
	{
		Ogre::Vector3 start, end;
		start = end = Ogre::Vector3::ZERO;
		Ogre::Vector3 realPos = getPosition() + m_meshesOffset * static_cast<f32>(idx);

		if (a_show)
		{
			end = realPos;
		}
		else
		{
			start = realPos;
		}

		m_entities[idx]->addBehavior(new FlyStraightAnimator{ a_duration, start, end });
	}
}

bool InputButton::keyUpdate(const std::string& a_key)
{
	if (!m_inputTimer && !m_mouseNotifId && !m_keybNotifId)
	{
		if (a_key == "Return")
		{
			m_inputTimer = CCoreEngine::Instance().GetTimerManager().CreateTimer(READ_INPUT_DELAY);
		}
	}

	return false;
}

bool InputButton::mouseUpdate(const std::string& a_button, GameEntity* a_entity)
{
	if (!m_inputTimer && !m_mouseNotifId && !m_keybNotifId)
	{
		if (a_button == "MOUSE_LB")
		{
			if (a_entity == m_entities[BUTTON_INPUT].get())
			{
				m_inputTimer = CCoreEngine::Instance().GetTimerManager().CreateTimer(READ_INPUT_DELAY);
			}
		}
	}

	return false;
}

void InputButton::onHover(bool a_hover)
{
	// #todo
}

bool InputButton::hasEntity(GameEntity* a_entity) const
{
	bool gotIt = false;

	for (auto it = m_entities.begin(); !gotIt && it != m_entities.end(); it++)
	{
		if ((*it).get() == a_entity)
		{
			gotIt = true;
		}
	}

	return gotIt;
}

void InputButton::updateText(Button a_button, const std::string& a_text)
{
	// update visualization of value
	Ogre::SceneNode* entityNode = m_entities[a_button]->getGraphics()->getNode();

	if (Ogre::SceneNode* textNode = static_cast<Ogre::SceneNode*>(entityNode->getChild(entityNode->getName() + "_textNode")))
	{
		Ogre::MovableText* text = static_cast<Ogre::MovableText*>(textNode->getAttachedObject(0));
		text->setCaption(a_text);
	}
}

void InputButton::mouseCallback(const std::string& a_button)
{
	if (m_mouseNotifId)
	{
		CCoreEngine::Instance().GetInputManager().GetKeyboard().RemoveNotification(m_keybNotifId);
		setValue(a_button);
	}
}

void InputButton::keyboardCallback(const std::string& a_key)
{
	if (m_keybNotifId)
	{
		CCoreEngine::Instance().GetInputManager().GetMouse().RemoveNotification(m_mouseNotifId);
		setValue(a_key);
	}
}

void InputButton::setValue(const std::string& a_value)
{
	auto& options = CCoreEngine::Instance().GetOptionManager();

	// check that the same key is not assigned in the same category
	OptionManager::Category category = options.getCategory(CATEGORY);
	bool found = false;

	for (auto it = category.begin(); !found && it != category.end(); it++)
	{
		if (it->first != m_option && a_value == it->second.as<std::string>())
		{
			found = true;
		}
	}

	if (!found)
	{
		options.setOption(CATEGORY + "/" + m_option, a_value);
		updateText(BUTTON_INPUT, a_value);
	}
	
	m_mouseNotifId = 0;
	m_keybNotifId = 0;
}
*/