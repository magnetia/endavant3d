#ifndef EDV_GUI_BOARD_TEXT_H_
#define EDV_GUI_BOARD_TEXT_H_

#include "Utils/edvVector3.h"
#include <memory>
#include <functional>
#include "Externals/OGRE/Ogre.h"
#include "State/CStateManager.h"
#include "BoardButtonStates.h"
#include "Externals/MovableText/MovableText.h"
#include <string>


class BoardText
{
public:
	BoardText(const Ogre::Vector3& a_pos, const std::string & a_text, const std::string & a_idtext, const std::string & a_fontid, const Ogre::Vector3& a_color);
	Ogre::Vector3 m_pos;
	std::string	m_text;
	std::string	m_idtext;
	std::string	m_fontid;
	Ogre::Vector3 m_color;

	void show();
	void hide();
};


#endif
