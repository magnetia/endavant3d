#ifndef EDV_GUI_CHANGE_STATE_BUTTON_H_
#define EDV_GUI_CHANGE_STATE_BUTTON_H_

#include "BoardButton.h"
#include "BoardMenu.h"
#include "BoardButtonStates.h"
#include "BoardButtonConfig.h"

#include "Game\GameEntity.h"
#include "Game/FlyStraightAnimator.h"
#include "Game/RotationAnimator.h"
#include "Game/ChainedBehavior.h"
#include "Utils\edvVector3.h"

#include "Core/CCoreEngine.h"
#include "State/CMainStateManager.h"


class ChangeStateButton : public BoardButton
{
public:
	ChangeStateButton(std::shared_ptr<BoardButtonConfig> a_buttcfg, const std::string &a_buttid, std::shared_ptr<BoardMenu> a_BoardMenuAsociated, const std::string& a_stateMgrName, const std::string& a_nextStateName);
	ChangeStateButton(std::shared_ptr<BoardButtonConfig> a_buttcfg, const std::string &a_buttid,
		std::shared_ptr<BoardMenu> a_BoardMenuAsociated, const std::string& a_stateMgrName, const std::string& a_nextStateName,
		
		std::shared_ptr<BoardButtonIdleState> a_idlestate, std::shared_ptr<BoardButtonHoverState> a_hoverstate,
		std::shared_ptr<BoardButtonSelectedState>a_selectedstate, std::shared_ptr<BoardButtonFinishState> a_finishstate,
		std::shared_ptr<BoardButtonEndState> a_endstate
		);
	virtual ~ChangeStateButton();
	void ChangeState();

private:
	std::string m_statemgrname;
	std::string m_nextstatename;

};


class BoardButtonChangeStateIdleState : public BoardButtonIdleState
{
public:
	BoardButtonChangeStateIdleState() :BoardButtonIdleState() {};
	virtual ~BoardButtonChangeStateIdleState(){};
	virtual void Start() override
	{
		getBoardButton()->setTextCaption(getBoardButton()->getButtonConfig()->m_captiontext);
		getBoardButton()->getEntity()->getGraphics()->getNode()->setVisible(true);
	};

	virtual void Finish()override{  };
	virtual void Pause() override{};
	virtual void Resume() override{};


};


class BoardButtonChangeStateHoverState : public BoardButtonHoverState
{
public:
	BoardButtonChangeStateHoverState() :BoardButtonHoverState() {};
	virtual ~BoardButtonChangeStateHoverState(){};
	virtual void Start() override
	{
		auto initial_pos = getBoardButton()->getPosition();
		auto position_back = getBoardButton()->getPosition();
		position_back.z -= 1.5;
		
		getBoardButton()->getEntity()->addBehavior(new ChainedBehavior({
			new FlyStraightAnimator{ 0.15, initial_pos.to_OgreVector3(), position_back.to_OgreVector3() },
			new FlyStraightAnimator{ 0.15, position_back.to_OgreVector3(), initial_pos.to_OgreVector3() }
		}, -1)
		);
	};

	virtual void Finish() override
	{
		auto butt = getBoardButton();
		if (butt)
		{
			auto entity = butt->getEntity();
			if (entity)
			{
				entity->removeAllBehaviors();
				getBoardButton()->resetPosition();
			}
		}

	};




};

class BoardButtonChangeStateSelectedState : public BoardButtonSelectedState
{
public:
	BoardButtonChangeStateSelectedState() :BoardButtonSelectedState() {};
	virtual ~BoardButtonChangeStateSelectedState(){};
	virtual void Start() override
	{


		auto board_menu = getBoardButton()->getBoardMenu();
		auto curr_button = getBoardButton();
		auto initialpos = curr_button->getPosition();
		curr_button->setTextCaptionVisible(false);
		m_chainedbehavi = new RotationAnimator{ 0.2, Ogre::Degree{ 360.0 }, Ogre::Vector3{ 1.0, 0, 0 } };
		curr_button->getEntity()->addBehavior(m_chainedbehavi);

		//Faig que no es pugui seleccionar res mes
		board_menu->disableInput();

		for (auto &but : board_menu->getAllButtons())
		{
			//Faig que els altres botons canviin d'estat
			if (but->getName() != curr_button->getName())
			{
				but->ChangeButtonState(GUIBUTTON_FINISH);
			}
		}

	};


	virtual void Update(f64 dt) override
	{
		getBoardButton()->getEntity()->update(dt);
		if (m_chainedbehavi->finished())
		{
			getBoardButton()->ChangeButtonState(GUIBUTTON_FINISH);
		}

	}

	virtual void Finish() override
	{
		auto board_menu = getBoardButton()->getBoardMenu();
		board_menu->enableInput();
		getBoardButton()->setTextCaptionVisible(true);
	};

private:
	RotationAnimator*	m_chainedbehavi;
};

class BoardButtonChangeStateEndState : public BoardButtonEndState
{
public:
	BoardButtonChangeStateEndState() : BoardButtonEndState(){};
	virtual ~BoardButtonChangeStateEndState(){};
	virtual void Update(f64 dt) override
	{
		auto board_menu = getBoardButton()->getBoardMenu();
		auto curr_button = getBoardButton();

		//Si es el boto seleccionat
		if (board_menu->getButtonSelected()->getName() == curr_button->getName())
		{
			//Si tots estan en END
			auto counter = 0;
			for (auto &but : board_menu->getAllButtons())
			{
				if (but->GetButtonState() != GUIBUTTON_END)
				{
					counter++;
				}
			}

			if (!counter)
			{
				//Canvio d'estat
				auto curr_button = getBoardButton();
				std::shared_ptr<ChangeStateButton> change_button = std::dynamic_pointer_cast<ChangeStateButton>(curr_button);
				change_button->ChangeState();
			}

		}
		else
		{
			getBoardButton()->getEntity()->getGraphics()->getNode()->setVisible(false);
		}


	}


};


/*
class ChangeStateButton : public BoardButton
{
public:
	ChangeStateButton(	const std::string& a_id, 
						const std::string& a_stateMgrName,
						const std::string& a_nextStateName,	
						const std::string& a_text,
						BoardMenu* a_parent = nullptr,
						const Ogre::Vector3& a_position = {});

	ChangeStateButton(pugi::xml_node& a_node, BoardMenu* a_parent = nullptr, const Ogre::Vector3& a_position = {});
	~ChangeStateButton();

	void init() override;
	void update(f64 dt) override;
	void pause() override;
	void resume() override;
	void stop() override;
	void animate(f64 a_duration, bool a_show) override;
	void onActionChange() override;
	bool keyUpdate(const std::string& a_key) override;
	bool mouseUpdate(const std::string& a_button, GameEntity* a_entity) override;
	void onHover(bool a_hover) override;
	bool hasEntity(GameEntity* a_entity) const override;

private:
	typedef std::unique_ptr<GameEntity>	EntityPtr;

	std::string	m_nextStateName;
	std::string	m_stateMgrName;
	bool		m_hovering;
	EntityPtr	m_entity;
	f32			m_hoverAnimDuration;
};
*/
#endif

