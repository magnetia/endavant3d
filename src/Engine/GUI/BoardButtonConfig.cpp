#include "BoardButtonConfig.h"
#include "Externals/OGRE/Ogre.h"


BoardButtonConfig::BoardButtonConfig() :
m_textPos{ Ogre::Vector3::ZERO }, m_anim_hover_duration{ 0 }, m_captionoffset{ Ogre::Vector3::ZERO },
m_left_offset{ Ogre::Vector3::ZERO }, m_value_offset{ Ogre::Vector3::ZERO }, m_right_offset{ Ogre::Vector3::ZERO },
m_captiontext{ "" }
{
}

std::shared_ptr<BoardButtonConfig> BoardButtonConfig::clone()
{
	
	std::shared_ptr<BoardButtonConfig> new_buttconfig = std::make_shared< BoardButtonConfig>();
	new_buttconfig->m_graphicsCfg= std::make_shared<EntityOgreGraphics::Config>(*this->m_graphicsCfg);
	new_buttconfig->m_physicsCfg = std::make_shared<EntityBtPhysics::Config>(*this->m_physicsCfg);
	new_buttconfig->m_anim_hover_duration = this->m_anim_hover_duration;
	new_buttconfig->m_captionoffset = this->m_captionoffset;
	new_buttconfig->m_left_offset = this->m_captionoffset;
	new_buttconfig->m_value_offset = this->m_value_offset;
	new_buttconfig->m_right_offset = this->m_right_offset;
	new_buttconfig->m_meshes_offset = this->m_meshes_offset;
	new_buttconfig->m_captiontext = this->m_captiontext;
	new_buttconfig->m_fontID = this->m_fontID;
	new_buttconfig->m_textPos = this->m_textPos;
	return new_buttconfig;

}


/*
std::shared_ptr<MenuButtonConfig> MenuButtonConfig::clone(const std::string& a_newidgameentity)
{
	//Ogre::Vector3 cloneOrigin = { 0, -50, 0 };
	GameEntity* widget = nullptr;
	
	auto cur_pos = m_basebuttentity->getWorldPosition();
	widget = m_basebuttentity->clone(a_newidgameentity, cur_pos.to_OgreVector3());
	std::shared_ptr<GameEntity> entity_sharedptr(widget);

	//Creo el nou buttconfig
	std::shared_ptr<MenuButtonConfig> new_buttconfig = std::make_shared< MenuButtonConfig>(entity_sharedptr, m_textPos);
	new_buttconfig->m_anim_hover_duration = this->m_anim_hover_duration;
	new_buttconfig->m_captionoffset = this->m_captionoffset;
	new_buttconfig->m_left_offset = this->m_captionoffset;
	new_buttconfig->m_value_offset = this->m_value_offset;
	new_buttconfig->m_right_offset = this->m_right_offset;
	new_buttconfig->m_meshes_offset = this->m_meshes_offset;

	return new_buttconfig;
}
*/

/*GameEntity* BoardMenuManager::cloneBaseWidget(const std::string& a_id, const std::string& a_text) const
{
	GameEntity* widget = nullptr;
	auto it = m_baseWidgets.find(a_name);

	if (it != m_baseWidgets.end())
	{
		const BaseWidget& widgetInfo = it->second;

		widget = widgetInfo.widget->clone(a_id, m_cloneOrigin);

		if (!a_text.empty())
		{
			Ogre::SceneNode* entityNode = widget->getGraphics()->getNode();
			Ogre::SceneNode* textNode = entityNode->createChildSceneNode(a_id + "_textNode", widgetInfo.textPos);
			Ogre::MovableText* msg = new Ogre::MovableText{ a_id + "_text", a_text, m_fontName };

			msg->setTextAlignment(Ogre::MovableText::H_CENTER, Ogre::MovableText::V_ABOVE);
			textNode->attachObject(msg);
		}
	}

	return widget;
}*/