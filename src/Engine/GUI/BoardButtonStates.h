#ifndef EDV_GUI_BOARD_BUTTON_STATES_H_
#define EDV_GUI_BOARD_BUTTON_STATES_H_


#include <memory>
#include "State\CState.h"


#include <iostream>

class BoardButton;
class BoardMenu;

class BoardButtonState : public CState
{
public:
	BoardButtonState(	
		const std::string& aName = "BoardButtonStateUNDEFINED"
		) :CState(aName){};

	virtual ~BoardButtonState(){};

	virtual void Start() override{};
	virtual void Finish()override{};
	virtual void Pause() override{};
	virtual void Resume() override{};
	virtual void Update(f64 dt) override{};

	virtual void setVisible(const bool a_visible){};

	std::shared_ptr<BoardButton> getBoardButton(){ return m_ButtonAssociated.lock(); };

private:
	void setBoardButtonAssociated(std::shared_ptr<BoardButton> a_bbuttonassociated)  { m_ButtonAssociated = a_bbuttonassociated; }; friend class BoardButton;
	std::weak_ptr<BoardButton>	m_ButtonAssociated;


};

class BoardButtonIdleState : public BoardButtonState
{
public:
	const static std::string STATENAME;
	BoardButtonIdleState() :BoardButtonState( STATENAME){};
	virtual ~BoardButtonIdleState(){};
	virtual void Update(f64 dt) override;
};

class BoardButtonHoverState : public BoardButtonState
{
public:
	const static std::string STATENAME;

	BoardButtonHoverState() :BoardButtonState( STATENAME){};

	virtual ~BoardButtonHoverState(){};
	virtual void Update(f64 dt) override;

	
};

class BoardButtonSelectedState : public BoardButtonState
{
public:
	const static std::string STATENAME;
	BoardButtonSelectedState() :BoardButtonState(STATENAME){};
	virtual void Update(f64 dt) override;
	virtual ~BoardButtonSelectedState(){};
	
};


class FlyStraightAnimator;
class BoardButtonFinishState : public BoardButtonState
{
public:
	const static std::string STATENAME;
	BoardButtonFinishState() :BoardButtonState(STATENAME){};
	virtual void Update(f64 dt) override;
	virtual void Start() override;
	virtual void Finish() override;
	virtual ~BoardButtonFinishState(){};
private:
	FlyStraightAnimator* m_animation;
};


class BoardButtonEndState : public BoardButtonState
{
public:
	const static std::string STATENAME;
	BoardButtonEndState() :BoardButtonState(STATENAME){};
	virtual ~BoardButtonEndState(){};
	
};
#endif
