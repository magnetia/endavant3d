#include "ChangeStateButton.h"









ChangeStateButton::ChangeStateButton(std::shared_ptr<BoardButtonConfig> a_buttcfg, const std::string &a_buttid, std::shared_ptr<BoardMenu> a_BoardMenuAsociated,
	const std::string& a_stateMgrName, const std::string& a_nextStateName) :
			BoardButton(a_buttcfg, a_buttid, a_BoardMenuAsociated,
				std::make_shared<BoardButtonChangeStateIdleState>(), std::make_shared<BoardButtonChangeStateHoverState>(),
				std::make_shared<BoardButtonChangeStateSelectedState>(), std::make_shared<BoardButtonFinishState>(), std::make_shared<BoardButtonChangeStateEndState>())
{
	m_statemgrname = a_stateMgrName;
	m_nextstatename = a_nextStateName;

}


ChangeStateButton::ChangeStateButton(std::shared_ptr<BoardButtonConfig> a_buttcfg, const std::string &a_buttid,
	std::shared_ptr<BoardMenu> a_BoardMenuAsociated, const std::string& a_stateMgrName, const std::string& a_nextStateName,
	std::shared_ptr<BoardButtonIdleState> a_idlestate, std::shared_ptr<BoardButtonHoverState> a_hoverstate,
	std::shared_ptr<BoardButtonSelectedState> a_selectedstate, std::shared_ptr<BoardButtonFinishState> a_finishstate,
	std::shared_ptr<BoardButtonEndState> a_endstate
	) :
	BoardButton(a_buttcfg, a_buttid, a_BoardMenuAsociated,
	a_idlestate, a_hoverstate, a_selectedstate, a_finishstate,a_endstate)
	
{
	m_statemgrname = a_stateMgrName;
	m_nextstatename = a_nextStateName;
}

ChangeStateButton::~ChangeStateButton()
{
}

void ChangeStateButton::ChangeState()
{

	if (CStateManager* stateMgr = CCoreEngine::Instance().GetMainStateManager().GetStateManager(m_statemgrname))
	{
		if (m_nextstatename.empty())
		{
			stateMgr->PopState();
		}
		else
		{
			stateMgr->ChangeState(m_nextstatename);
		}
	}

}
/*
ChangeStateButton::ChangeStateButton(const std::string& a_id, const std::string& a_stateMgrName, const std::string& a_nextStateName, 
	const std::string& a_text, BoardMenu* a_parent, const Ogre::Vector3& a_position) :
		BoardButton{ a_id, a_parent, a_position },
		m_nextStateName{ a_nextStateName },
		m_stateMgrName{ a_stateMgrName },
		m_hovering{ false },
		m_hoverAnimDuration{ 0.5f }
{
	BoardMenuManager* manager = getParent()->getParent();
	pugi::xml_node config = manager->getButtonConfig("change_menu_button");

	m_entity.reset(manager->cloneBaseWidget(a_id, "change_menu_button", a_text));
	m_entity->setScale(m_entity->getScale() * 0.8f);

	if (!config.empty())
	{
		m_hoverAnimDuration = config.attribute("anim_hover_duration").as_float(m_hoverAnimDuration);
	}
}

ChangeStateButton::ChangeStateButton(pugi::xml_node& a_node, BoardMenu* a_parent, const Ogre::Vector3& a_position) :
	ChangeStateButton{ 
		a_node.attribute("id").as_string(),
		a_node.attribute("state_manager").as_string(),
		a_node.attribute("next_state").as_string(), 
		a_node.attribute("text").as_string(),
		a_parent,
		a_position }
{

}

ChangeStateButton::~ChangeStateButton()
{

}

void ChangeStateButton::init()
{
	if (m_entity)
	{
		m_entity->getGraphics()->getNode()->setVisible(true);
		m_entity->init();
	}
}

void ChangeStateButton::update(f64 dt)
{
	if (m_entity)
	{
		m_entity->update(dt);
	}
}

void ChangeStateButton::pause()
{
	if (m_entity)
	{
		m_entity->pause();
	}
}

void ChangeStateButton::resume()
{
	if (m_entity)
	{
		m_entity->resume();
	}
}

void ChangeStateButton::stop()
{
	if (m_entity)
	{
		m_entity->stop();
		m_entity->getGraphics()->getNode()->setVisible(false);
	}
}

void ChangeStateButton::animate(f64 a_duration, bool a_show)
{
	if (m_entity)
	{
		Ogre::Vector3 start, end;
		start = end = Ogre::Vector3::ZERO;

		if (a_show)
		{
			end = getPosition();
		}
		else
		{
			start = getPosition();
		}

		m_entity->addBehavior(new FlyStraightAnimator{a_duration, start, end});
	}
}

void ChangeStateButton::onActionChange()
{
	
	callOnChangeFunction();
	
	if (CStateManager* stateMgr = CCoreEngine::Instance().GetMainStateManager().GetStateManager(m_stateMgrName))
	{
		if (m_nextStateName.empty())
		{
			stateMgr->PopState();
		}
		else
		{
			stateMgr->ChangeState(m_nextStateName);
		}
	}


}

void ChangeStateButton::onHover(bool a_hover)
{
	if (a_hover != m_hovering)
	{
		if (a_hover)
		{
			static_cast<Ogre::Entity*>(m_entity->getGraphics()->getNode()->getAttachedObject(0))->setMaterialName("button2_mat/on");

			const Ogre::Vector3 pos1{ m_position.x, m_position.y, m_position.z - 1.f };
			const Ogre::Vector3 pos2{ m_position.x, m_position.y, m_position.z + 1.f };

			m_entity->addBehavior(
				new ChainedBehavior({
					new FlyStraightAnimator{ .4, pos1, pos2 },
					new FlyStraightAnimator{ .4, pos2, pos1 },
			}, -1));
		}
		else
		{
			m_entity->removeAllBehaviors();
			m_entity->getGraphics()->getNode()->setPosition(m_position);
			static_cast<Ogre::Entity*>(m_entity->getGraphics()->getNode()->getAttachedObject(0))->setMaterialName("button2_mat/off");
		}

		m_hovering = a_hover;
	}
}

bool ChangeStateButton::hasEntity(GameEntity* a_entity) const
{
	return m_entity.get() == a_entity;
}

bool ChangeStateButton::keyUpdate(const std::string& a_key)
{
	bool changeMenu = false;

	if (a_key == "Return")
	{
		changeMenu = true;
	}

	return changeMenu;
}

bool ChangeStateButton::mouseUpdate(const std::string& a_button, GameEntity* a_entity)
{
	bool changeMenu = false;

	if (a_entity == m_entity.get() && a_button == "MOUSE_LB")
	{
		changeMenu = true;
	}

	return changeMenu;
}
*/