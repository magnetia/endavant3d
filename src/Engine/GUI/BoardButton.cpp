#include "BoardButton.h"
#include "BoardButtonConfig.h"
#include "Game/GameEntity.h"
#include "Utils\edvVector3.h"

#include "Core\CCoreEngine.h"
#include "Physics\CPhysicsManager.h"


BoardButton::BoardButton(std::shared_ptr<BoardButtonConfig> a_buttcfg, const std::string &a_buttid, std::shared_ptr<BoardMenu> a_BoardMenuAsociated,
	std::shared_ptr<BoardButtonIdleState> a_idlebuttonstate,
	std::shared_ptr<BoardButtonHoverState> a_hoverstate,
	std::shared_ptr<BoardButtonSelectedState> a_selectedstate,
	std::shared_ptr<BoardButtonFinishState> a_finishstate ,
	std::shared_ptr<BoardButtonEndState> a_endstate 
	):
	m_buttstate{ GUIBUTTON_MAX_STATES }, m_statemanager{ a_buttid }, m_buttid{ a_buttid }, m_menuBoardAssociated{ a_BoardMenuAsociated }, m_buttoncfg{a_buttcfg},
	m_captionTextNode{nullptr}
{
	//register states
	if (!a_idlebuttonstate || !a_hoverstate || !a_selectedstate || !a_finishstate || !a_endstate)
		throw std::runtime_error("BoardButton: " + a_buttid + " State NULL!!");
	else
	{
		//Registro estats
		m_statemanager.RegisterState(a_idlebuttonstate.get());
		m_statemanager.RegisterState(a_hoverstate.get());
		m_statemanager.RegisterState(a_selectedstate.get());
		m_statemanager.RegisterState(a_finishstate.get());
		m_statemanager.RegisterState(a_endstate.get());

		//poso estats al graveyard
		m_graveyard.push_back(a_idlebuttonstate);
		m_graveyard.push_back(a_hoverstate);
		m_graveyard.push_back(a_selectedstate);
		m_graveyard.push_back(a_finishstate);
		m_graveyard.push_back(a_endstate);

		//Creo la entitat (boto)
		m_buttentity = std::make_shared<GameEntity>(*a_buttcfg->m_graphicsCfg.get());// , *a_buttcfg->m_physicsCfg.get());
		m_buttentity->getGraphics()->getNode()->setVisible(false);

		//Guardo posicio inicial
		m_initialposition = m_buttentity->getWorldPosition();


	}
}

BoardButton::~BoardButton()
{
	m_statemanager.Clear();
	if (m_captionTextNode)
	{
		delete m_captionTextNode;
	}
}

std::string BoardButton::getName() const
{
	return m_buttid;
}

void BoardButton::setVisible(const bool a_visible)
{
	m_buttentity->getGraphics()->getNode()->setVisible(a_visible);
}

void	BoardButton::ChangeButtonState(GUIBUTTON_STATE a_newstate)
{

	switch (a_newstate)
	{

	case GUIBUTTON_IDLE:
		if (GetButtonState() != GUIBUTTON_IDLE)
			m_statemanager.ChangeState(BoardButtonIdleState::STATENAME);
		break;
	case GUIBUTTON_HOVER:
		if (GetButtonState() != GUIBUTTON_HOVER)
			m_statemanager.ChangeState(BoardButtonHoverState::STATENAME);
		break;
	case GUIBUTTON_SELECTED:
		if (GetButtonState() != GUIBUTTON_SELECTED)
			m_statemanager.ChangeState(BoardButtonSelectedState::STATENAME);
		break;
	case GUIBUTTON_FINISH:
		if (GetButtonState() != GUIBUTTON_FINISH)
			m_statemanager.ChangeState(BoardButtonFinishState::STATENAME);
		break;
	case GUIBUTTON_END:
		if (GetButtonState() != GUIBUTTON_END)
			m_statemanager.ChangeState(BoardButtonEndState::STATENAME);
		break;
	
	}

	m_buttstate = a_newstate;
}

GUIBUTTON_STATE	BoardButton::GetButtonState()
{
	return m_buttstate;
}

void BoardButton::resetPosition() const
{
	m_buttentity->setWorldPosition(m_initialposition);
}

edv::Vector3f BoardButton::getPosition() const
{
	return m_buttentity->getWorldPosition();
}

void BoardButton::setTextCaption(const std::string &a_textcaption)
{
	if (m_captionTextNode==nullptr && !a_textcaption.empty())
	{
		Ogre::SceneNode* entityNode = m_buttentity->getGraphics()->getNode();
		auto zsize = m_buttentity->getGraphics()->getHalfSize().z()*2.0f ;
	
		Ogre::Vector3 position(0.0);// = getPosition().to_OgreVector3();
		position.z += zsize+0.01f;
		
		Ogre::SceneNode* textNode = entityNode->createChildSceneNode(getName() + "_textNode", m_buttoncfg->m_textPos + position);
		m_captionTextNode = new Ogre::MovableText{ getName() + "_text", a_textcaption, m_buttoncfg->m_fontID };
		m_captionTextNode->setTextAlignment(Ogre::MovableText::H_CENTER, Ogre::MovableText::V_CENTER);
		textNode->attachObject(m_captionTextNode);

	}

	if (!a_textcaption.empty())
	{
		m_captionTextNode->setCaption(a_textcaption);
	}

}

std::string BoardButton::getTextCaption() const
{
	if (!m_captionTextNode)
		return "";
	else
		return m_captionTextNode->getCaption();

}

void BoardButton::setTextCaptionVisible(const bool a_visiblestate)
{
	if (m_captionTextNode)
	{
		m_captionTextNode->setVisible(a_visiblestate);
	}
}

std::shared_ptr<BoardButtonConfig> BoardButton::getButtonConfig() const
{
	return m_buttoncfg;
}

bool BoardButton::IsMouseHovering()
{
	//Mirem colisio actual
	GameEntity* targetEntity = static_cast<GameEntity*>(CCoreEngine::Instance().GetPhysicsManager().RayTestMouseCurrentCamera());
	return (m_buttentity.get() == targetEntity);
}


void BoardButton::Start()
{
	for (auto state : m_statemanager.GetAllRegisteredStates())
		static_cast<BoardButtonState*>(state)->setBoardButtonAssociated(getshared());

	resetPosition();

	ChangeButtonState(GUIBUTTON_IDLE);
}

void BoardButton::Update(f64 dt)
{
	m_statemanager.Update(dt);
}

void BoardButton::Pause()
{

}


void BoardButton::Resume()
{

}

void BoardButton::Finish()
{
	m_buttentity->getGraphics()->getNode()->setVisible(false);
}



std::shared_ptr<GameEntity> BoardButton::getEntity() const
{
	return m_buttentity;
}

std::shared_ptr<BoardMenu> BoardButton::getBoardMenu() const
{
	return m_menuBoardAssociated.lock();
	//return m_menuBoardAssociated;
}

/*
BoardButton::BoardButton(const std::string &a_buttid, std::shared_ptr<MenuButtonConfig> a_menucfg, BoardMenu* a_parent = nullptr, const Ogre::Vector3& a_position = {}) :
m_buttid{ m_buttid },
	m_position{ a_position }
	
{
	setParent(a_parent);
}

BoardButton::~BoardButton()
{

}

const Ogre::Vector3& BoardButton::getPosition() const
{
	return m_position;
}


void BoardButton::setOnChangeFunction(const OnChangeFun& a_function)
{
	std::cout << "setOnChangeFunction: " << getButtonId() << std::endl;
	m_onchangefun = a_function;
}


void BoardButton::callOnChangeFunction()
{
	if (m_onchangefun)
	{

		std::cout << getButtonId() << " is Calling OnChangeFunction" << std::endl;
		m_onchangefun();
	}
}


std::string BoardButton::getButtonId()
{
	return m_buttid;
}

std::string BoardButton::getCaption()
{
	return m_caption;
}
std::string BoardButton::setCaption(const std::string &a_caption)
{
	m_caption = a_caption;
}
*/