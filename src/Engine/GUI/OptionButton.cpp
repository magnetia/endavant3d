#include "Core/CCoreEngine.h"
#include "Options/OptionManager.h"
#include "Game/FlyStraightAnimator.h"
#include "Utils/CConversions.h"
#include "Externals/OGRE/Ogre.h"
#include "Externals/MovableText/MovableText.h"
#include "OptionButton.h"
#include "BoardMenu.h"
#include "BoardMenuManager.h"
/*
// #todo: el text esta hardcodejadissim

const OptionButton::BaseTags OptionButton::BASE_TAGS{{
	{ "option_caption_button", "_caption" },
	{ "option_selector_button", "_left" }, 
	{ "option_value_button", "_value" },
	{ "option_selector_button", "_right" }
} };

OptionButton::OptionButton(pugi::xml_node& a_node, BoardMenu* a_parent, const Ogre::Vector3& a_position) :
	BoardButton{ "guarrada" , a_parent, a_position }, // hauria d'anar el id del bot�: const std::string id = a_node.attribute("id").as_string() + info.second; algo aixi 
	m_caption{ a_node.attribute("caption").as_string() },
	m_category{ a_node.attribute("category").as_string() },
	m_option{ a_node.attribute("option").as_string() },
	m_offsets{{ Ogre::Vector3{ 0, 0, 0 }, Ogre::Vector3{ 5, 0, 0 }, Ogre::Vector3{ 8.5, 0, 0 }, Ogre::Vector3{ 12, 0, 0 } }}
{
	BoardMenuManager* manager = getParent()->getParent();
	setParent(a_parent);

	for (u32 idx = 0; idx < BASE_TAGS.size(); idx++)
	{
		const GraphicsInfo& info = BASE_TAGS[idx];
		const std::string id = a_node.attribute("id").as_string() + info.second;

		m_entities[idx].reset(manager->cloneBaseWidget(id, info.first, 
			(idx == BUTTON_CAPTION || idx == BUTTON_VALUE)? "-" : ""));
	}

	pugi::xml_node config = manager->getButtonConfig("option_widget");
	
	if (!config.empty())
	{
		BoardMenuManager::readVectorFromXML(config.child("caption_offset"), m_offsets[BUTTON_CAPTION]);
		BoardMenuManager::readVectorFromXML(config.child("left_offset"), m_offsets[BUTTON_LEFT]);
		BoardMenuManager::readVectorFromXML(config.child("value_offset"), m_offsets[BUTTON_VALUE]);
		BoardMenuManager::readVectorFromXML(config.child("right_offset"), m_offsets[BUTTON_RIGHT]);
	}
}

void OptionButton::init()
{
	for (u32 idx = 0; idx < BUTTON_MAX; idx++)
	{
		GameEntity* entity = m_entities[idx].get();

		entity->getGraphics()->getNode()->setVisible(true);
		entity->init();

		if (idx == BUTTON_CAPTION || idx == BUTTON_VALUE)
		{
			updateText(static_cast<Button>(idx), idx == BUTTON_CAPTION ? m_caption : get());
		}
	}
}

void OptionButton::update(f64 dt)
{
	for (auto& entityPtr : m_entities)
	{
		entityPtr->update(dt);
	}
}

void OptionButton::pause()
{
	for (auto& entityPtr : m_entities)
	{
		entityPtr->pause();
	}
}

void OptionButton::resume()
{
	for (auto& entityPtr : m_entities)
	{
		entityPtr->resume();
	}
}

void OptionButton::stop()
{
	for (auto& entityPtr : m_entities)
	{
		entityPtr->stop();
		entityPtr->getGraphics()->getNode()->setVisible(false);
	}
}

void OptionButton::animate(f64 a_duration, bool a_show)
{
	for (u32 idx = 0; idx < BUTTON_MAX; idx++)
	{
		Ogre::Vector3 start, end, realPos = getPosition();
		start = end = Ogre::Vector3::ZERO;

		// #todo: cutreversion para la feria de Madrid
		switch (idx)
		{
		case BUTTON_CAPTION:
			realPos += { 0, 0, 0 };
			break;
		case BUTTON_LEFT:
			realPos += { 5., 0, 0 };
			break;
		case BUTTON_VALUE:
			realPos += { 8.5, 0, 0 };
			break;
		case BUTTON_RIGHT:
			realPos += { 12, 0, 0 };
			break;
		}

		if (a_show)
		{
			end = realPos;
		}
		else
		{
			start = realPos;
		}

		m_entities[idx]->addBehavior(new FlyStraightAnimator{a_duration, start, end});
	}
}

bool OptionButton::keyUpdate(const std::string& a_key)
{
	if (a_key == "Left")
	{
		prev();
	}
	else if (a_key == "Right")
	{
		next();
	}

	updateText(BUTTON_VALUE, get());

	return false;
}

void OptionButton::updateText(Button a_button, const std::string& a_text)
{
	// update visualization of value
	Ogre::SceneNode* entityNode = m_entities[a_button]->getGraphics()->getNode();
		
	if (Ogre::SceneNode* textNode = static_cast<Ogre::SceneNode*>(entityNode->getChild(entityNode->getName() + "_textNode")))
	{
		Ogre::MovableText* text = static_cast<Ogre::MovableText*>(textNode->getAttachedObject(0));
		text->setCaption(a_text);
	}
}

bool OptionButton::mouseUpdate(const std::string& a_button, GameEntity* a_entity)
{
	if (a_button == "MOUSE_LB")
	{
		if (a_entity == m_entities[BUTTON_LEFT].get())
		{
			prev();
		}
		else if (a_entity == m_entities[BUTTON_RIGHT].get())
		{
			next();
		}

		updateText(BUTTON_VALUE, get());
	}

	return false;
}

void OptionButton::onHover(bool a_hover)
{
	// #todo
}

bool OptionButton::hasEntity(GameEntity* a_entity) const
{
	bool gotIt = false;

	for (auto it = m_entities.begin(); !gotIt && it != m_entities.end(); it++)
	{
		if ((*it).get() == a_entity)
		{
			gotIt = true;
		}
	}

	return gotIt;
}

void OptionButton::applyCurrentValue() const
{
	const std::string optionPath = m_category + "/" + m_option;
	CCoreEngine::Instance().GetOptionManager().setOption(optionPath, get());
}

OptionButtonString::OptionButtonString(pugi::xml_node& a_node, BoardMenu* a_parent, const Ogre::Vector3& a_position, std::initializer_list<std::string> a_list) :
	OptionButton{ a_node, a_parent, a_position },
	m_values{ a_list },
	m_current{ m_values.end() }
{
	auto& options = CCoreEngine::Instance().GetOptionManager();
	const std::string optionPath = m_category + "/" + m_option;
	const std::string value = options.getOption(optionPath).as<std::string>();

	for (Values::iterator it = m_values.begin(); m_current == m_values.end() && it != m_values.end(); it++)
	{
		if ((*it) == value)
		{
			m_current = it;
		}
	}

	if (m_current == m_values.end())
	{
		m_current = m_values.begin();
		options.setOption(optionPath, (*m_current));
	}
}

void OptionButtonString::next()
{
	if (m_current != m_values.begin())
	{
		m_current--;
		applyCurrentValue();
	}
}

void OptionButtonString::prev()
{
	if (m_current + 1 != m_values.end())
	{
		m_current++;
		applyCurrentValue();
	}
}

std::string OptionButtonString::get() const
{
	std::string value;

	if (m_current != m_values.end())
	{
		value = *m_current;
	}

	return value;
}

OptionButtonNumeric::OptionButtonNumeric(pugi::xml_node& a_node, BoardMenu* a_parent, const Ogre::Vector3& a_position) :
	OptionButton{ a_node, a_parent, a_position}
{
	auto& options = CCoreEngine::Instance().GetOptionManager();
	const std::string optionPath = m_category + "/" + m_option;
	m_value = options.getOption(optionPath);

	m_min = a_node.attribute("min").as_int();
	m_max = a_node.attribute("max").as_int();
	m_default = a_node.attribute("default").as_int();
	m_inc = a_node.attribute("inc").as_int();

	if (m_min < m_max && m_default >= m_min  && m_default <= m_max)
	{
		if (m_value < m_min || m_value > m_max)
		{
			m_value = m_default;
			options.setOption(optionPath, m_value);
		}
	}
	else
	{
		throw std::runtime_error("invalid argument");
	}
}

void OptionButtonNumeric::next()
{
	if (m_value + m_inc <= m_max)
	{
		m_value += m_inc;
		applyCurrentValue();
	}
}

void OptionButtonNumeric::prev()
{
	if (m_value - m_inc >= m_min)
	{
		m_value -= m_inc;
		applyCurrentValue();
	}
}

std::string OptionButtonNumeric::get() const
{
	return to_string(m_value);
}
*/