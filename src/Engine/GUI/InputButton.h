#ifndef EDV_GUI_INPUT_BUTTON_H_
#define EDV_GUI_INPUT_BUTTON_H_

#include <array>
#include "BoardButton.h"
#include "Input/CInputMouse.h"
#include "Input/CInputKeyboard.h"
#include "Time/CTimeManager.h"
#include "Externals/pugixml/pugixml.hpp"
/*
class InputButton : public BoardButton
{
public:
	enum Button
	{
		BUTTON_CAPTION,
		BUTTON_INPUT,

		BUTTON_MAX
	};

	InputButton(const std::string& a_id, const std::string& a_caption, 
		const std::string& a_option, BoardMenu* a_parent = nullptr, const Ogre::Vector3& a_position = {});
	InputButton(pugi::xml_node& a_node, BoardMenu* a_parent = nullptr, const Ogre::Vector3& a_position = {});

	void init() override;
	void update(f64 dt) override;
	void pause() override;
	void resume() override;
	void stop() override;
	void animate(f64 a_duration, bool a_show) override;
	void onActionChange() override { }
	bool keyUpdate(const std::string& a_key) override;
	bool mouseUpdate(const std::string& a_button, GameEntity* a_entity) override;
	void onHover(bool a_hover) override;
	bool hasEntity(GameEntity* a_entity) const override;

private:
	typedef std::pair<std::string, std::string>		GraphicsInfo;
	typedef std::array<GraphicsInfo, BUTTON_MAX>	BaseTags;
	typedef std::unique_ptr<GameEntity>				EntityPtr;
	typedef std::array<EntityPtr, BUTTON_MAX>		Entities;

	void updateText(Button a_button, const std::string& a_text);
	void mouseCallback(const std::string& a_button);
	void keyboardCallback(const std::string& a_key);
	void setValue(const std::string& a_value);

	static const BaseTags BASE_TAGS;

	std::string						m_option;
	std::string						m_caption;
	CInputMouse::NotificationId		m_mouseNotifId;
	CInputKeyboard::NotificationId	m_keybNotifId;
	EV_TimerID						m_inputTimer;
	Entities						m_entities;
	Ogre::Vector3					m_meshesOffset;
};
*/
#endif
