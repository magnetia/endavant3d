/*#include <functional>


#include "Renderer/CRenderManager.h"

#include "Physics/COgreMotionState.h"
#include "Sound/CSoundManager.h"
#include "Game/FlyStraightAnimator.h"
#include "Externals/MovableText/MovableText.h"
#include "BoardMenuManager.h"
#include "BoardMenu.h"
#include "ChangeStateButton.h"
#include "OptionButton.h"
#include "InputButton.h"
#include "CallbackButton.h"
*/

//noves
#include "Input/CInputManager.h"
#include "Physics/CPhysicsManager.h"
#include "Core/CCoreEngine.h"
#include "BoardMenu.h"
#include "BoardMenuManager.h"
#include "BoardButton.h"
#include "Sound\CSoundManager.h"

BoardMenu::BoardMenu(const std::string &a_menuidstate, BoardMenuManager *a_menumgr ) :
CState{ a_menuidstate },
m_mouseActions{ { { "MOUSE_LB" }, { "MOUSE_MB" }, { "MOUSE_RB" } } },
m_keyboardActions{ { { "Up" }, { "Left" }, { "Down" }, { "Right" }, { "Return" } } },
m_mouseActive{ false },
m_inputEnabled{true},
m_menumgr{ a_menumgr }
{
}


std::shared_ptr<BoardButton> BoardMenu::getButtonSelected()
{
	return *m_buttonselected;

}

void BoardMenu::enableInput()
{
	m_inputEnabled = true;

}

BoardMenu::tBoardButtVec BoardMenu::getAllButtons()
{
	return m_menubuttons;
}

void BoardMenu::disableInput()
{
	m_inputEnabled = false;
}

void BoardMenu::AddButton(std::shared_ptr<BoardButton> a_newbutton)
{
	m_menubuttons.push_back(a_newbutton);
}

void BoardMenu::AddText(std::shared_ptr<BoardText> a_newtext)
{
	m_texts.push_back(a_newtext);
}

std::shared_ptr<BoardButton> BoardMenu::GetButton(const std::string &abuttname) const
{
	for (auto ibutt: m_menubuttons)
	{
		if (ibutt->getName() == abuttname)
			return ibutt;
	}

	std::shared_ptr<BoardButton> nulll;
	return nulll;

}

void BoardMenu::Start()
{
	//Assigno les accions
	auto& mouse = CCoreEngine::Instance().GetInputManager().GetMouse();
	auto& keyboard = CCoreEngine::Instance().GetInputManager().GetKeyboard();
	for (auto& mouseAction : m_mouseActions)
		mouseAction.actionId = mouse.InsertButtonAction(mouseAction.button);
	

	for (auto& keybdAction : m_keyboardActions)
		keybdAction.actionId = keyboard.InsertKeyAction(keybdAction.key);
	
	for (auto butt : m_menubuttons)
		butt->Start();

	//Assigno accions a gamepads
	auto vecGC = CCoreEngine::Instance().GetInputManager().GetGameController().getGameControllersIDs();
	if (!vecGC.empty())
	{
		auto gc = CCoreEngine::Instance().GetInputManager().GetGameController().getGameController(vecGC.at(0));
		gc.lock()->InsertKeyAction("Up", EDV_GAMECONTROLLER_BUTTON_DPAD_UP);
		gc.lock()->InsertKeyAction("Down", EDV_GAMECONTROLLER_BUTTON_DPAD_DOWN);
		gc.lock()->InsertKeyAction("Return", EDV_GAMECONTROLLER_BUTTON_A);
	}

	setVisible(true);
	m_mouseActive = false;
	//select el primer boto per si de cas
	if (m_menubuttons.size())
	{
		m_buttonselected = m_menubuttons.begin();
		(*m_buttonselected)->ChangeButtonState(GUIBUTTON_HOVER);
	}

	for (auto text : m_texts)
		text->show();
}

void BoardMenu::Finish()
{
	
	//DesAssigno les accions
	auto& mouse = CCoreEngine::Instance().GetInputManager().GetMouse();
	auto& keyboard = CCoreEngine::Instance().GetInputManager().GetKeyboard();
	for (auto& mouseAction : m_mouseActions)
		mouse.RemoveButtonAction(mouseAction.actionId);
	for (auto& keybdAction : m_keyboardActions)
		keyboard.RemoveKeyAction(keybdAction.actionId);


	for (auto butt : m_menubuttons)
		butt->Finish();

	for (auto text : m_texts)
		text->hide();
}

void BoardMenu::Pause() 
{
	for (auto butt : m_menubuttons)
		butt->Pause();
}

void BoardMenu::Resume() 
{
	for (auto butt : m_menubuttons)
		butt->Resume();
}

void BoardMenu::Update(f64 dt)
{
	//He apretat algun boto?
	HandleInput();
	

	//Update dels buttons
	for (auto butt : m_menubuttons)
		butt->Update(dt);
}


void BoardMenu::HandleInput()
{
	if (m_inputEnabled)
	{
		if (m_mouseActive)
		{
		
		}
		else
		{
			const bool useGC = CCoreEngine::Instance().GetOptionManager().getOption("controls/usegamepad");
			if (useGC)
			{
				auto vecGC = CCoreEngine::Instance().GetInputManager().GetGameController().getGameControllersIDs();
				if (!vecGC.empty())
				{
					auto gc = CCoreEngine::Instance().GetInputManager().GetGameController().getGameController(vecGC.at(0));
					if (gc.lock()->IsActionKeyDown("Up"))
					{
						hoverNextButton();
					}
					else if (gc.lock()->IsActionKeyDown("Down"))
					{
						hoverPreviousButton();
					}
					else if (gc.lock()->IsActionKeyDown("Return"))
					{
						selectCurrentButton();
					}

				}


			}
			else
			{
				auto& keyb = CCoreEngine::Instance().GetInputManager().GetKeyboard();
				for (auto& action : m_keyboardActions)
				{
					if (keyb.IsActionKeyDown(action.actionId))
					{
				
						if (action.key == "Up")
						{
							hoverNextButton();
						}
						else if (action.key == "Down")
						{
							hoverPreviousButton();
						}
						else if (action.key == "Return")
						{
							selectCurrentButton();
						}
						/*else if (m_selected != m_buttons.end())
						{
							m_selectionDone = (*m_selected)->keyUpdate(action.key);
						}*/
					}
				}
			}

		}
	}
}

void BoardMenu::hoverPreviousButton()
{
	if (!m_menubuttons.empty())
	{
		if (m_buttonselected != m_menubuttons.end())
		{
			//canvio idle el current boto
			(*m_buttonselected)->ChangeButtonState(GUIBUTTON_IDLE);

			if (m_buttonselected != m_menubuttons.end() - 1)
			{
				m_buttonselected++;
			}
			else
			{
				m_buttonselected = m_menubuttons.begin();
			}

			CCoreEngine::Instance().GetSoundManager().playSound("changebeep");
			(*m_buttonselected)->ChangeButtonState(GUIBUTTON_HOVER);
		}
	}
}

void BoardMenu::hoverNextButton()
{
	if (!m_menubuttons.empty())
	{
		if (m_buttonselected != m_menubuttons.end())
		{
			//canvio idle el current boto
			(*m_buttonselected)->ChangeButtonState(GUIBUTTON_IDLE);

			if (m_buttonselected != m_menubuttons.begin())
			{
				m_buttonselected--;
			}
			else
			{
				m_buttonselected = m_menubuttons.end() - 1;
			}

			CCoreEngine::Instance().GetSoundManager().playSound("changebeep");
			(*m_buttonselected)->ChangeButtonState(GUIBUTTON_HOVER);
		}
	}
}

void BoardMenu::selectCurrentButton()
{
	CCoreEngine::Instance().GetSoundManager().playSound("selectbeep");
	(*m_buttonselected)->ChangeButtonState(GUIBUTTON_SELECTED);
}


void BoardMenu::setVisible(const bool a_visible)
{
	for (auto butt : m_menubuttons)
	{
		butt->setVisible(a_visible);

	}
}

/*

BoardMenu::BoardMenu(pugi::xml_node& a_node, BoardMenuManager* a_parent, const std::shared_ptr<BoardButtonConfig>, const Ogre::Vector3& a_buttonsOffset, f32 a_animDuration) :
	CState{ a_node.attribute("id").as_string() },
	m_animSeq{ ANIM_NONE },
	m_animTimer{ CTimeManager::INVALID_TIMERID },
	m_inputActive{ false },
	m_mouseActions{{ { "MOUSE_LB" }, { "MOUSE_MB" }, { "MOUSE_RB" } }},
	m_keyboardActions{ { { "Up" }, { "Left" }, { "Down" }, { "Right" }, { "Return" } } },
	m_buttons{},
	m_selected{ m_buttons.begin() },
	m_selectionDone{ false },
	m_animDuration{ a_animDuration }
{
	// set parent menu manager
	setParent(a_parent);

	// add buttons
	Ogre::Vector3 origin{ 
		a_node.attribute("origin_x").as_float(0.f), 
		a_node.attribute("origin_y").as_float(6.5f), 
		a_node.attribute("origin_z").as_float(15.f) };

	for (pugi::xml_node buttonNode = a_node.first_child(); buttonNode; buttonNode = buttonNode.next_sibling())
	{
		ButtonPtr button;
		
		const std::string buttonNodeName{ buttonNode.name() };

		if (buttonNodeName == "change_menu_button")
		{
			button.reset( new ChangeStateButton{ buttonNode, this, origin });
		}
		else if (buttonNodeName == "callback_button")
		{
			button.reset(new CallbackButton{ buttonNode, this, origin });
		}
		else if (buttonNodeName == "option_widget")
		{
			const std::string type = buttonNode.attribute("type").as_string();

			if (type == "numeric")
			{
				button.reset( new OptionButtonNumeric{ buttonNode, this, origin });
			}
			else if (type == "boolean")
			{
				button.reset( new OptionButtonString{ buttonNode, this, origin, { "true", "false" } });
			}
			else if (type == "string")
			{
				// #todo: llegir llista proporcionada al xml
			}
		}
		else if (buttonNodeName == "input_button")
		{
			button.reset( new InputButton{ buttonNode, this, origin });
		}

		if (button)
		{
			addButton(std::move(button));
			
		}

		origin += a_buttonsOffset;
	}

	// add "back" button
	const std::string previousState = a_node.attribute("previous_state").as_string();

	if (!previousState.empty())
	{
		m_buttons.emplace_back(new ChangeStateButton{ "/" + GetName() + "/previous", getParent()->GetName(), 
			previousState, "Back", this, origin });
	}
}

void BoardMenu::addButton(ButtonPtr a_button)
{
	if (a_button)
		m_buttons.emplace_back(std::move(a_button));
	
}

BoardMenu::ButtonPtr BoardMenu::getButton(const std::string &a_butt_name)
{
	for (auto butt : m_buttons)
	{
		std::cout << butt->getName() << std::endl;

		if (butt->getName() == a_butt_name)
			return butt;

	}

	return nullptr;
		
}

void BoardMenu::Start()
{
	enableInput(true);

	for (auto& buttonPtr : m_buttons)
	{
		buttonPtr->init();
	}

	m_animSeq = ANIM_NONE;
	m_animTimer = CTimeManager::INVALID_TIMERID;
	m_selectionDone = false;

	nextSequence();
}

void BoardMenu::Update(f64 dt)
{
	updateSequence(dt);

	if (sequenceFinished())
	{
		nextSequence();
	}
	else
	{
		for (auto& buttonPtr : m_buttons)
		{
			buttonPtr->update(dt);
		}		
	}
}

void BoardMenu::Finish()
{
	for (auto& buttonPtr : m_buttons)
	{
		buttonPtr->stop();
	}

	enableInput(false);
}

void BoardMenu::Pause()
{
	for (auto& buttonPtr : m_buttons)
	{
		buttonPtr->pause();
	}

	enableInput(false);
}

void BoardMenu::Resume()
{
	enableInput(true);

	for (auto& buttonPtr : m_buttons)
	{
		buttonPtr->resume();
	}
}

void BoardMenu::enableInput(bool a_active)
{
	if (a_active != m_inputActive)
	{
		auto& mouse = CCoreEngine::Instance().GetInputManager().GetMouse();
		auto& keyboard = CCoreEngine::Instance().GetInputManager().GetKeyboard();

		if (a_active)
		{
			for (auto& mouseAction : m_mouseActions)
			{
				mouseAction.actionId = mouse.InsertButtonAction(mouseAction.button);
			}

			for (auto& keybdAction : m_keyboardActions)
			{
				keybdAction.actionId = keyboard.InsertKeyAction(keybdAction.key);
			}
		}
		else
		{
			for (auto& mouseAction : m_mouseActions)
			{
				mouse.RemoveButtonAction(mouseAction.actionId);
			}

			for (auto& keybdAction : m_keyboardActions)
			{
				keyboard.RemoveKeyAction(keybdAction.actionId);
			}
		}

		m_inputActive = a_active;
	}
}

void BoardMenu::handleKeyboardInput()
{
	if (!sequenceFinished())
	{
		auto& keyb = CCoreEngine::Instance().GetInputManager().GetKeyboard();

		for (auto& action : m_keyboardActions)
		{
			if (keyb.IsActionKeyDown(action.actionId))
			{
				// #todo: no comprovar totes les accions per cada accio, organitzar d'una altra manera
				if (action.key == "Up")
				{
					selectPreviousButton();
				}
				else if (action.key == "Down")
				{
					selectNextButton();
				}
				else if (m_selected != m_buttons.end())
				{
					m_selectionDone = (*m_selected)->keyUpdate(action.key);
				}
			}
		}
	}
}

void BoardMenu::handleMouseInput()
{
	if (!sequenceFinished())
	{
		auto& mouse = CCoreEngine::Instance().GetInputManager().GetMouse();

		// get entity mouse is pointing to
		auto screen_height = CCoreEngine::Instance().GetRenderManager().GetOgreViewport()->getActualHeight();
		auto screen_width = CCoreEngine::Instance().GetRenderManager().GetOgreViewport()->getActualWidth();
		Ogre::Camera *cam = CCoreEngine::Instance().GetRenderManager().GetOgreViewport()->getCamera();

		auto dir = cam->getDirection();
		Ogre::Real x = (f32)mouse.GetPosX() / screen_width;
		Ogre::Real y = (f32)mouse.GetPosY() / screen_height;

		Ogre::Ray ray = cam->getCameraToViewportRay(x, 1.0f - y);

		btVector3 origin = COgreMotionState::ogreVector3toBtVector3(ray.getOrigin());
		btVector3 dest = COgreMotionState::ogreVector3toBtVector3(ray.getPoint(1000.0));

		GameEntity* targetEntity = static_cast<GameEntity*>(CCoreEngine::Instance().GetPhysicsManager().RayTest(origin, dest));

		for (auto itButton = m_buttons.begin(); !m_selectionDone && itButton != m_buttons.end(); itButton++)
		{
			// check if mouse is pointing this button
			if ((*itButton)->hasEntity(targetEntity))
			{
				if (itButton != m_selected)
				{
					(*itButton)->onHover(true);
					m_selected = itButton;
				}

				// check mouse buttons
				for (auto itAction = m_mouseActions.begin(); !m_selectionDone && itAction != m_mouseActions.end(); itAction++)
				{
					if (mouse.IsActionButtonDown((*itAction).actionId))
					{
						m_selectionDone = (*itButton)->mouseUpdate((*itAction).button, targetEntity);
					}
				}
			}
			else
			{
				if (itButton != m_selected)
				{
					(*itButton)->onHover(false);
				}
			}
		}
	}
}

void BoardMenu::selectNextButton()
{
	if (!m_buttons.empty())
	{
		if (m_selected != m_buttons.end())
		{
			(*m_selected)->onHover(false);

			if (m_selected != m_buttons.end() - 1)
			{
				m_selected++;
			}
			else
			{
				m_selected = m_buttons.begin();
			}

			(*m_selected)->onHover(true);
		}
	}
}

void BoardMenu::selectPreviousButton()
{
	if (!m_buttons.empty())
	{
		if (m_selected != m_buttons.end())
		{
			(*m_selected)->onHover(false);

			if (m_selected != m_buttons.begin())
			{
				m_selected--;
			}
			else
			{
				m_selected = m_buttons.end() - 1;
			}

			(*m_selected)->onHover(true);
		}
	}
}

bool BoardMenu::sequenceFinished() const
{
	switch (m_animSeq)
	{
	case ANIM_SHOW_BUTTONS:
	case ANIM_HIDE_BUTTONS:
		return CCoreEngine::Instance().GetTimerManager().IsEndTimer(m_animTimer);
		break;

	case ANIM_SELECTION:
		return m_selectionDone;
		break;

	case ANIM_END:
	case ANIM_NONE:
	default:
		return false;
		break;
	}
}

void BoardMenu::nextSequence()
{
	m_animSeq = static_cast<AnimSequence>(static_cast<u32>(m_animSeq)+1U % static_cast<u32>(ANIM_MAX));

	switch (m_animSeq)
	{
	case ANIM_HIDE_BUTTONS:
		CCoreEngine::Instance().GetSoundManager().playSound("menu_beep");

		if (m_selected != m_buttons.end())
		{
			(*m_selected)->onHover(false);
		}
	case ANIM_SHOW_BUTTONS:
		showButtons(m_animSeq == ANIM_SHOW_BUTTONS);
		break;

	case ANIM_SELECTION:
		if (!m_buttons.empty())
		{
			m_selected = m_buttons.begin();
			(*m_selected)->onHover(true);
		}
		break;

	case ANIM_END:
		if (m_selected != m_buttons.end())
		{
			(*m_selected)->onActionChange();
		}
		break;

	case ANIM_NONE:
	default:
		break;
	}
}

void BoardMenu::updateSequence(f64 dt)
{
	switch (m_animSeq)
	{
	case ANIM_SELECTION:
		handleMouseInput();
		handleKeyboardInput();
		break;

	case ANIM_NONE:
	case ANIM_SHOW_BUTTONS:
	case ANIM_HIDE_BUTTONS:
	case ANIM_END:
	default:
		break;
	}
}

void BoardMenu::showButtons(bool a_show)
{
	m_animTimer = CCoreEngine::Instance().GetTimerManager().CreateTimer(m_animDuration);

	for (auto& buttonPtr : m_buttons)
	{
		buttonPtr->animate(m_animDuration, a_show);
	}
}

*/