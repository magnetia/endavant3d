#ifndef EDV_GUI_BOARD_MENU_MANAGER_H_
#define EDV_GUI_BOARD_MENU_MANAGER_H_

#include <memory>
#include <string>
#include <vector>
#include <unordered_map>

#include "BoardMenu.h"
#include "Externals/pugixml/pugixml.hpp"
#include "OGRE\OgreVector3.h"
#include "OGRE\OgreVector2.h"
#include "State\CMainStateManager.h"
#include "Ogre\Ogre.h"

class MenuFontCfg
{
public:
	std::string m_idfont;
	std::string m_fontname;
	std::string	m_fontsize;
};

class BoardButtonConfig;
class GameEntity;


class BoardMenuManager
{
public:
	~BoardMenuManager();
	BoardMenuManager();

	void	StartUp();
	void	ShutDown();
	void	Render();
	void 	Update(f64 dt);

	void load(const std::string& a_xmlPath);
	void unload();
	
	//GameEntity* cloneBaseWidget(const std::string& a_id, const std::string& a_name, const std::string& a_text = "") const;
	//FontConfig

	//ButtonConfig
	std::shared_ptr<BoardButtonConfig> getButtonConfig(const std::string& a_id);

	//Menus
	typedef std::shared_ptr<BoardMenu>						MenuPtr;
	
	void addBoardMenu(MenuPtr a_menuptr);
	MenuPtr getMenuBoard(const std::string &a_menuID);
	MenuPtr getcurrentMenuBoard();
	void	showMenuBoard(const std::string &a_menuID);

	void	setVisible(const bool a_visible);
	//Helpers
	static void readVectorFromXML(pugi::xml_node& a_node, Ogre::Vector2& a_vector);
	static void readVectorFromXML(pugi::xml_node& a_node, Ogre::Vector3& a_vector);

private:

	
	void LoadCamera(pugi::xml_node &boardMenuNode);
	void LoadBackground(pugi::xml_node &boardMenuNode);
	void LoadFontsConfig(pugi::xml_node &boardMenuNode);
	void LoadBaseButtonConfig(pugi::xml_node &boardMenuNode);
	void LoadMenus(pugi::xml_node &boardMenuNode);


	typedef std::map<std::string, std::shared_ptr<MenuFontCfg> > tFontCfgMap;
	tFontCfgMap m_fontscfgmap;

	typedef std::map<std::string, std::shared_ptr<BoardButtonConfig> > tButtonCfgMap;
	tButtonCfgMap m_buttoncfgmap;

	typedef std::vector<MenuPtr>							Menus;
	Menus				m_menus;

	Ogre::SceneNode*	m_parentNode;
	Ogre::Camera*		m_previousCamera;
	bool				m_mouseWasRelative;

	std::unique_ptr<Ogre::Rectangle2D>	m_backgroundrect;
	std::unique_ptr<GameEntity>			m_titleentity;
	CStateManager		m_menustatemanager;


	Ogre::Camera* m_menucamera;

};

#endif
