#ifndef EDV_GUI_BOARD_MENU_H_
#define EDV_GUI_BOARD_MENU_H_

#include <memory>
#include <string>
#include <vector>
#include "Utils\ChildOf.h"
#include "State/CState.h"
#include "BoardText.h"

class CState;
class BoardMenuManager;
class BoardButton;


class BoardMenu : public CState, public std::enable_shared_from_this<BoardMenu>
{
public:
	BoardMenu(const std::string &a_menuidstate, BoardMenuManager *a_menumgr);
	virtual ~BoardMenu(){};
	void AddButton(std::shared_ptr<BoardButton> a_newbutton);
	void AddText(std::shared_ptr<BoardText> a_newtext);
	std::shared_ptr<BoardButton> GetButton(const std::string &abuttname) const;
	void setVisible(const bool a_visible);
	// State
	void Start() override;
	void Finish() override;
	void Pause() override;
	void Resume() override;
	void Update(f64 dt) override;

	void enableInput();
	void disableInput();

	typedef std::vector < std::shared_ptr<BoardButton> > tBoardButtVec;
	tBoardButtVec getAllButtons();
	std::shared_ptr<BoardButton> getButtonSelected();


	typedef std::vector < std::shared_ptr<BoardText> > tBoardtext;

protected:
	BoardMenuManager *m_menumgr;

	std::shared_ptr<BoardMenu> getshared() { return shared_from_this(); }
	virtual void hoverPreviousButton();
	virtual void hoverNextButton();


	bool m_inputEnabled;
	tBoardButtVec m_menubuttons;

	tBoardButtVec::iterator m_buttonselected;

	tBoardtext m_texts;

	//Actions
	struct MouseAction
	{
		std::string		button;
		std::string		actionId;

		MouseAction(const std::string& a_button = "", const std::string& a_actionId = "") :
			button{ a_button }, actionId{ a_actionId } { }
	};

	struct KeyboardAction
	{
		std::string key;
		std::string actionId;

		KeyboardAction(const std::string& a_key = "", const std::string& a_actionId = "") :
			key{ a_key }, actionId{ a_actionId } { }
	};

	typedef std::vector<MouseAction>		MouseActions;
	typedef std::vector<KeyboardAction>		KeyboardActions;
	MouseActions		m_mouseActions;
	KeyboardActions		m_keyboardActions;
	bool m_mouseActive;
	void HandleInput();


	void selectCurrentButton();

};


/*

#include "Input/CInputMouse.h"
#include "Input/CInputKeyboard.h"
#include "Time/CTimeManager.h"
#include "Game/GameEntity.h"
#include "Externals/pugixml/pugixml.hpp"
class BoardMenu : public ChildOf<BoardMenuManager>, public CState
{
public:
	BoardMenu(pugi::xml_node& a_node, BoardMenuManager* a_parent, const std::shared_ptr<BoardButtonConfig>, const Ogre::Vector3& a_buttonsOffset, f32 a_animDuration);

	// State
	void Start() override;
	void Finish() override;
	void Pause() override;
	void Resume() override;
	void Update(f64 dt) override;

	typedef std::shared_ptr<BoardButton>	ButtonPtr;
	void addButton(ButtonPtr a_button);
	ButtonPtr getButton(const std::string &a_butt_name);

private:
	enum AnimSequence
	{
		ANIM_NONE,
		ANIM_SHOW_BUTTONS,
		ANIM_SELECTION,
		ANIM_HIDE_BUTTONS,
		ANIM_END,

		ANIM_MAX,
	};

	struct MouseAction
	{
		std::string		button;
		std::string		actionId;

		MouseAction(const std::string& a_button = "", const std::string& a_actionId = "") :
			button{ a_button }, actionId{ a_actionId } { }
	};

	struct KeyboardAction
	{
		std::string key;
		std::string actionId;

		KeyboardAction(const std::string& a_key = "", const std::string& a_actionId = "") :
			key{ a_key }, actionId{ a_actionId } { }
	};

	typedef std::vector<MouseAction>		MouseActions;
	typedef std::vector<KeyboardAction>		KeyboardActions;
	typedef std::vector<ButtonPtr>			Buttons;

	// input handling
	void enableInput(bool a_active);
	void handleKeyboardInput();
	void handleMouseInput();

	// button navigation
	void selectNextButton();
	void selectPreviousButton();

	// animations
	bool sequenceFinished() const;
	void nextSequence();
	void updateSequence(f64 dt);
	void showButtons(bool a_show);

	AnimSequence		m_animSeq;
	EV_TimerID			m_animTimer;
	bool				m_inputActive;
	MouseActions		m_mouseActions;
	KeyboardActions		m_keyboardActions;
	Buttons				m_buttons;
	Buttons::iterator	m_selected;
	bool				m_selectionDone;
	f32					m_animDuration;
};

*/
#endif