#ifndef EDV_GUI_OPTION_BUTTON_H_
#define EDV_GUI_OPTION_BUTTON_H_

#include <string>
#include <vector>
#include <array>
#include <initializer_list>
#include "BoardButton.h"
#include "Externals/pugixml/pugixml.hpp"
/*
class OptionButton : public BoardButton
{
public:
	enum Button
	{
		BUTTON_CAPTION,
		BUTTON_LEFT,
		BUTTON_VALUE,
		BUTTON_RIGHT,

		BUTTON_MAX
	};

	OptionButton(pugi::xml_node& a_node, BoardMenu* a_parent, const Ogre::Vector3& a_position = {});

	void init() override;
	void update(f64 dt) override;
	void pause() override;
	void resume() override;
	void stop() override;
	void animate(f64 a_duration, bool a_show) override;
	void onActionChange() override { }
	bool keyUpdate(const std::string& a_key) override;
	bool mouseUpdate(const std::string& a_button, GameEntity* a_entity) override;
	void onHover(bool a_hover) override;
	bool hasEntity(GameEntity* a_entity) const override;

protected:
	typedef std::pair<std::string, std::string>		GraphicsInfo;
	typedef std::array<GraphicsInfo, BUTTON_MAX>	BaseTags;
	typedef std::array<Ogre::Vector3, BUTTON_MAX>	MeshesOffsets;

	virtual void next() = 0;
	virtual void prev() = 0;
	virtual std::string get() const = 0;

	virtual void applyCurrentValue() const final;

	static const BaseTags BASE_TAGS;

	std::string		m_caption;
	std::string		m_category;
	std::string		m_option;
	MeshesOffsets	m_offsets;

private:
	typedef std::unique_ptr<GameEntity>				EntityPtr;
	typedef std::array<EntityPtr, BUTTON_MAX>		Entities;

	void updateText(Button a_button, const std::string& a_text);

	Entities	m_entities;
};

class OptionButtonString : public OptionButton
{
public:
	OptionButtonString(pugi::xml_node& a_node, BoardMenu* a_parent, const Ogre::Vector3& a_position = {}, std::initializer_list<std::string> a_list = {});
	virtual void next() override;
	virtual void prev() override;
	virtual std::string get() const override;

private:
	typedef std::vector<std::string> Values;

	Values				m_values;
	Values::iterator	m_current;
};

class OptionButtonNumeric : public OptionButton
{
public:
	OptionButtonNumeric(pugi::xml_node& a_node, BoardMenu* a_parent, const Ogre::Vector3& a_position = {});
	virtual void next() override;
	virtual void prev() override;
	virtual std::string get() const override;

private:
	s32 m_value;
	s32 m_min;
	s32 m_max;
	s32 m_default;
	s32 m_inc;
};

*/
#endif