#ifndef EDV_GUI_CallbackButton_H_
#define EDV_GUI_CallbackButton_H_

#include "BoardButton.h"
#include "Externals/pugixml/pugixml.hpp"
/*
class CallbackButton : public BoardButton
{
public:
	CallbackButton(const std::string& a_id,
						const std::string& a_text,
						BoardMenu* a_parent = nullptr,
						const Ogre::Vector3& a_position = {});

	CallbackButton(pugi::xml_node& a_node, BoardMenu* a_parent = nullptr, const Ogre::Vector3& a_position = {});
	~CallbackButton();

	void init() override;
	void update(f64 dt) override;
	void pause() override;
	void resume() override;
	void stop() override;
	void animate(f64 a_duration, bool a_show) override;
	void onActionChange() override;
	bool keyUpdate(const std::string& a_key) override;
	bool mouseUpdate(const std::string& a_button, GameEntity* a_entity) override;
	void onHover(bool a_hover) override;
	bool hasEntity(GameEntity* a_entity) const override;

private:
	typedef std::unique_ptr<GameEntity>	EntityPtr;

	bool		m_hovering;
	EntityPtr	m_entity;
	f32			m_hoverAnimDuration;
};
*/
#endif
