#include "BoardText.h"
#include <Core/CCoreEngine.h>
#include <Renderer\CRenderManager.h>
#include <Renderer\CRenderTextManager.h>


#include "Utils\edvVector3.h"



BoardText::BoardText(const Ogre::Vector3& a_pos, const std::string & a_text, const std::string & a_idtext, const std::string & a_fontid, const Ogre::Vector3& a_color)
{
	m_pos = a_pos;
	m_text = a_text;
	m_idtext = a_idtext;
	m_fontid = a_fontid;
	m_color = a_color;
}



void BoardText::show()
{
	CRenderTextManager* P = CCoreEngine::Instance().GetRenderManager().GetRenderTextManager();
	
	P->addTextBox(m_idtext, m_fontid, m_text, m_pos.x, m_pos.y, 1280, 720, Ogre::ColourValue(m_color.x, m_color.y, m_color.z, 1.0));
	
}

void BoardText::hide()
{
	CRenderTextManager* P = CCoreEngine::Instance().GetRenderManager().GetRenderTextManager();

	P->removeTextBox(m_idtext);
}