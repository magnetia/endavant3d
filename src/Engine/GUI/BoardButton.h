#ifndef EDV_GUI_BOARD_BUTTON_H_
#define EDV_GUI_BOARD_BUTTON_H_

#include "Utils/edvVector3.h"
#include <memory>
#include <functional>
#include "Externals/OGRE/Ogre.h"
#include "State/CStateManager.h"
#include "BoardButtonStates.h"
#include "Externals/MovableText/MovableText.h"


class BoardButtonConfig;
class GameEntity;
class BoardButtonIdleState;
class BoardButtonHoverState;
class BoardButtonSelectedState;
class BoardButtonState;
class BoardButtonEndState;
class BoardButtonFinishState;

class BoardMenu;



typedef enum
{
	GUIBUTTON_IDLE = 0,
	GUIBUTTON_HOVER,
	GUIBUTTON_SELECTED,
	GUIBUTTON_FINISH,
	GUIBUTTON_END,
	GUIBUTTON_MAX_STATES

}GUIBUTTON_STATE;


class BoardButton : std::enable_shared_from_this<BoardButton>
{
public:
	BoardButton(std::shared_ptr<BoardButtonConfig> a_buttcfg, const std::string &a_buttid, std::shared_ptr<BoardMenu> a_BoardMenuAsociated,
		std::shared_ptr<BoardButtonIdleState> a_idlebuttonstate,
		std::shared_ptr<BoardButtonHoverState> a_hoverstate,
		std::shared_ptr<BoardButtonSelectedState> a_selectedstate,
		std::shared_ptr<BoardButtonFinishState> a_finishstate,
		std::shared_ptr<BoardButtonEndState> a_endstate);

	std::string getName() const;
	void resetPosition() const;
	edv::Vector3f getPosition() const;
	virtual void setVisible(const bool a_visible);

	bool IsMouseHovering();
	std::shared_ptr<GameEntity> getEntity() const;
	std::shared_ptr<BoardMenu>  getBoardMenu() const;

	virtual ~BoardButton();

	virtual void Start();
	virtual void Finish();
	virtual void Pause();
	virtual void Resume();
	virtual void Update(f64 dt);
	
	//Button states
	void	ChangeButtonState(GUIBUTTON_STATE a_newstate);
	GUIBUTTON_STATE	GetButtonState();

	void setTextCaption(const std::string &a_textcaption);
	std::string getTextCaption() const;
	void setTextCaptionVisible(const bool a_visiblestate);
	std::shared_ptr<BoardButtonConfig> getButtonConfig() const;
protected:

	std::shared_ptr<BoardButton> getshared() { return shared_from_this(); }
	CStateManager	m_statemanager;

	GUIBUTTON_STATE	m_buttstate;
	std::string		m_buttid;
	std::shared_ptr<GameEntity>	m_buttentity;

	std::shared_ptr<BoardButtonConfig> m_buttoncfg;



private:
	std::weak_ptr<BoardMenu> m_menuBoardAssociated;

	//Cementiri per que no faci un delete quan surtin d'scope els shared_ptr... (tot es perque el StateManager gesitona pointers i no shared_ptrs..)
	std::vector<std::shared_ptr<BoardButtonState>> m_graveyard;

	edv::Vector3f m_initialposition;

	Ogre::MovableText* m_captionTextNode;
};



/*
class MenuButtonConfig
{
public:
	MenuButtonConfig(const std::shared_ptr<GameEntity> m_buttentity, const Ogre::Vector3 &a_textpos) :
		m_basebuttentity{ m_buttentity }, m_textPos{ a_textpos }, m_anim_hover_duration{ 0 }, m_captionoffset{ 0 },
		m_left_offset{ 0 }, m_value_offset{ 0}, m_right_offset{ 0 };

	std::shared_ptr<GameEntity>	m_basebuttentity;
	Ogre::Vector3	m_textPos;
	f32				m_anim_hover_duration;
	Ogre::Vector3	m_captionoffset;
	Ogre::Vector3	m_left_offset;
	Ogre::Vector3	m_value_offset;
	Ogre::Vector3	m_right_offset;
	Ogre::Vector3	m_meshes_offset;

	std::shared_ptr<MenuButtonConfig> cloneButtonCfg(const std::string& a_newidgameentity)
	{
		auto cloneOrigin = { 0, -50, 0 };
		GameEntity* widget = nullptr;
		widget = widgetInfo.widget->clone(a_id, cloneOrigin);
		auto newbuttoncfg = std::make_shared<MenuButtonConfig>();
	}

	GameEntity* BoardMenuManager::cloneBaseWidget(const std::string& a_id, const std::string& a_text) const
	{
		GameEntity* widget = nullptr;
		auto it = m_baseWidgets.find(a_name);

		if (it != m_baseWidgets.end())
		{
			const BaseWidget& widgetInfo = it->second;

			widget = widgetInfo.widget->clone(a_id, m_cloneOrigin);

			if (!a_text.empty())
			{
				Ogre::SceneNode* entityNode = widget->getGraphics()->getNode();
				Ogre::SceneNode* textNode = entityNode->createChildSceneNode(a_id + "_textNode", widgetInfo.textPos);
				Ogre::MovableText* msg = new Ogre::MovableText{ a_id + "_text", a_text, m_fontName };

				msg->setTextAlignment(Ogre::MovableText::H_CENTER, Ogre::MovableText::V_ABOVE);
				textNode->attachObject(msg);
			}
		}

		return widget;
	}
};
*/
/*

class BoardMenu;

class BoardButton : public ChildOf<BoardMenu>
{
public:
	BoardButton(const std::string &a_buttid, std::shared_ptr<MenuButtonConfig> a_menucfg, BoardMenu* a_parent = nullptr, const Ogre::Vector3& a_position);

	virtual ~BoardButton();

	virtual void init() = 0;
	virtual void update(f64 dt) = 0;
	virtual void pause() = 0;
	virtual void resume() = 0;
	virtual void stop() = 0;
	virtual void animate(f64 a_duration, bool a_show) = 0;
	virtual bool keyUpdate(const std::string& a_key) = 0;
	virtual bool mouseUpdate(const std::string& a_button, GameEntity* a_entity) = 0;
	virtual void onHover(bool a_hover) = 0;
	virtual void onActionChange() = 0;
	virtual bool hasEntity(GameEntity* a_entity) const = 0;

	typedef std::function<void() > OnChangeFun;
	void setOnChangeFunction(const OnChangeFun& a_function);
	void callOnChangeFunction();
	
		
	virtual const Ogre::Vector3& getPosition() const final;
	std::string getButtonId();
	

protected:


	Ogre::Vector3	m_position;
	std::string		m_buttid;


	//std::shared_ptr<MenuButtonConfig> a_menucfg;
	//OnChangeFun		m_onchangefun;
	//std::shared_ptr<GameEntity>	m_widgetentity;
};
*/
#endif
