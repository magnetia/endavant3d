#include "CInputGameController.h"
#include "Core/CCoreEngine.h"
#include "Core/CLogManager.h"
#include "Events/CEventCodes.h"
#include "SDL2\SDL.h"
CInputGameController::CInputGameController()
{

}

CInputGameController::~CInputGameController()
{
	ShutDown();
}

void CInputGameController::StartUp(void)
{


	if (SDL_InitSubSystem(SDL_INIT_GAMECONTROLLER ) < 0)
	{
		LOG(LOG_ERROR,LOGSUB_INPUT,"Couldn't initialize SDL_INIT_GAMECONTROLLER: %s\n", SDL_GetError());
		return;
		//TODO exception.
	}

	SDL_GameControllerEventState(SDL_ENABLE);

    /* Game controller events */
    AddEvent(EVENT_SDL, SDL_CONTROLLERDEVICEADDED);		/**< A new Game controller has been inserted into the system */
    AddEvent(EVENT_SDL, SDL_CONTROLLERDEVICEREMOVED);      /**< An opened Game controller has been removed */
    AddEvent(EVENT_SDL, SDL_CONTROLLERDEVICEREMAPPED);		/**< The controller mapping was updated */
    AddEvent(EVENT_SDL, SDL_CONTROLLERAXISMOTION);			/**< Game controller axis motion */
    AddEvent(EVENT_SDL, SDL_CONTROLLERBUTTONDOWN );		/**< Game controller button pressed */
    AddEvent(EVENT_SDL, SDL_CONTROLLERBUTTONUP);			/**< Game controller button released */

	RegisterEventManager();

	LoadAllControllers();
}

void CInputGameController::ShutDown(void)
{

	UnRegisterEventManager();
	SDL_QuitSubSystem(SDL_INIT_GAMECONTROLLER );
}

void CInputGameController::Update(f64 dt)
{
	for (auto gc : m_gamecontrollers)
		gc.second->Update(dt);

}

void CInputGameController::HandleEvent(const EVENT_TYPES a_Type, Uint32 a_Code, SDL_Event* a_Event)
{
	/*
	typedef struct SDL_ControllerAxisEvent
	{
	    Uint32 type;        //< ::SDL_CONTROLLERAXISMOTION
	    Uint32 timestamp;
	    SDL_JoystickID which; //< The joystick instance id
	    Uint8 axis;         //< The controller axis (SDL_GameControllerAxis)
	    Uint8 padding1;
	    Uint8 padding2;
	    Uint8 padding3;
	    Sint16 value;       //< The axis value (range: -32768 to 32767)
	    Uint16 padding4;
	} SDL_ControllerAxisEvent;



	//   \brief Game controller button event structure (event.cbutton.*)

	typedef struct SDL_ControllerButtonEvent
	{
	    Uint32 type;        //< ::SDL_CONTROLLERBUTTONDOWN or ::SDL_CONTROLLERBUTTONUP
	    Uint32 timestamp;
	    SDL_JoystickID which; //< The joystick instance id
	    Uint8 button;      ///< The controller button (SDL_GameControllerButton)
	    Uint8 state;        //< ::SDL_PRESSED or ::SDL_RELEASED
	    Uint8 padding1;
	    Uint8 padding2;
	} SDL_ControllerButtonEvent;



	 //  \brief Controller device event structure (event.cdevice.*)
	typedef struct SDL_ControllerDeviceEvent
	{
	    Uint32 type;        ///< ::SDL_CONTROLLERDEVICEADDED, ::SDL_CONTROLLERDEVICEREMOVED, or ::SDL_CONTROLLERDEVICEREMAPPED
	    Uint32 timestamp;
	    Sint32 which;       //< The joystick device index for the ADDED event, instance id for the REMOVED or REMAPPED event
	} SDL_ControllerDeviceEvent;
	*/

	//LOG( LOG_INFO, LOGSUB_INPUT, "Tipus: %d    Codi: %d ", a_Type,  a_Code);
	switch (a_Code)
	{
	case SDL_CONTROLLERDEVICEADDED	  	:
		LOG(LOG_INFO, LOGSUB_INPUT, "ID: %d SDL_CONTROLLERDEVICEADDED ", a_Event->cdevice.which);
		//LoadAllControllers();
		AddController(a_Event->cdevice.which);
		break;

	case SDL_CONTROLLERDEVICEREMOVED  	:
		LOG(LOG_INFO, LOGSUB_INPUT, "ID: %d SDL_CONTROLLERDEVICEREMOVED ", a_Event->cdevice.which);
		//LoadAllControllers();
		RemoveController( a_Event->cdevice.which);
		break;
	}

}

void CInputGameController::LoadAllControllers()
{

	CloseAllControllers();

	/*
	 *  The mapping format for joystick is:
	 *      bX - a joystick button, index X
	 *      hX.Y - hat X with value Y
	 *      aX - axis X of the joystick
	 *  Buttons can be used as a controller axis and vice versa.
	 *  */

	SDL_GameControllerAddMapping("7e050603000000000000504944564944,Nintendo Wii Classic Controller,a:b1,b:b0,y:b2,x:b3,start:b6,guide:b5,back:b4,dpup:b11,dpleft:b12,dpdown:b14,dpright:b13,lefttrigger:b7,righttrigger:b8,leftshoulder:b9,rightshoulder:b10,leftx:a0,lefty:a1,rightx:a2,righty:a3");
	SDL_GameControllerAddMapping("addef0be000000000000504944564944,Virtual Joystick,a:b1,b:b0,y:b2,x:b3,start:b6,guide:b5,back:b4,dpup:b11,dpleft:b12,dpdown:b14,dpright:b13,leftshoulder:b9,rightshoulder:b10,leftx:a0,lefty:a1,rightx:a2,righty:a3");

	LOG(LOG_INFO, LOGSUB_INPUT, "Searching Game Devices: ");
	for (auto device_id = 0 ; device_id < SDL_NumJoysticks(); ++device_id)
		AddController(device_id);



}

void CInputGameController::CloseAllControllers()
{
	m_gamecontrollers.clear();
}

void CInputGameController::RemoveController(GameController_id a_device_id)
{
	auto l_it = m_gamecontrollers.find(a_device_id);

	if (l_it != m_gamecontrollers.end())
		m_gamecontrollers.erase(l_it);
}

void CInputGameController::AddController(GameController_id a_device_id)
{
	//Retrieving Device GUID
	char l_guidbuf[33 /* 32 + 1 for \0 */];
	SDL_JoystickGetGUIDString(SDL_JoystickGetDeviceGUID(a_device_id), l_guidbuf, static_cast<int>(sizeof(l_guidbuf)));

	if (SDL_IsGameController(a_device_id))
	{
		LOG(LOG_INFO, LOGSUB_INPUT, "Device: { ID: %d | GUID: %s | Name: %s } ->  Supported Game Controller!", a_device_id, l_guidbuf, SDL_JoystickNameForIndex(a_device_id));
	}
	else
	{
		LOG(LOG_INFO, LOGSUB_INPUT, "Device: { ID: %d | GUID: %s | Name: %s  } ->  No Mapping Found for this Game Controller", a_device_id, l_guidbuf, SDL_JoystickNameForIndex(a_device_id));

		// Add a default mapping for this controller:
		std::string l_newmapping = std::string(l_guidbuf) + ",Default Controller #" + to_string(a_device_id) + ",a:b1,b:b2,y:b3,x:b0,start:b9,guide:b12,back:b8,dpup:h0.1,dpleft:h0.8,dpdown:h0.4,dpright:h0.2,leftshoulder:b4,rightshoulder:b5,leftstick:b10,rightstick:b11,leftx:a0,lefty:a1,rightx:a2,righty:a3,lefttrigger:b6,righttrigger:b7";
		LOG(LOG_INFO, LOGSUB_INPUT, "Device: { ID: %d | GUID: %s | Name: %s  } ->  Defaulting Mapping to: %s", a_device_id, l_guidbuf, SDL_JoystickNameForIndex(a_device_id), l_newmapping.c_str());
		SDL_GameControllerAddMapping(l_newmapping.c_str());
	}
	auto l_gc = std::make_shared<CGameController>(a_device_id, std::string(l_guidbuf));
	auto l_it = m_gamecontrollers.emplace(std::make_pair (a_device_id,l_gc));
	if (l_it.second)
		l_it.first->second.get()->Attach();

}


std::weak_ptr<CGameController> CInputGameController::getGameController(const GameController_id a_gc_id)
{
	std::weak_ptr < CGameController > ptr;
	for (auto gc : m_gamecontrollers)
	{
		if (gc.first == a_gc_id)
			ptr = gc.second;
	}
	return ptr;
	
	

}
std::vector<GameController_id> CInputGameController::getGameControllersIDs()
{
	std::vector<GameController_id> vec;
	for (auto gc : m_gamecontrollers)
		vec.push_back(gc.first);
	
	return vec;
}