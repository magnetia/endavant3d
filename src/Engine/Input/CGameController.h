#ifndef GAMECONTROLLER_H_
#define GAMECONTROLLER_H_

#include <SDL2/SDL_gamecontroller.h>
#include "Core/CBasicTypes.h"
#include "Events/CEventHandler.h"
#include "Events\CEventCodes.h"
#include "Utils/IdGenerator.h"
#include <array>
#include <string>

#include "OGRE\OgreVector2.h"

// AXIS AVAILABLE
typedef enum
{
    EDV_GAMECONTROLLER_AXIS_LEFTX        	= SDL_CONTROLLER_AXIS_LEFTX,
    EDV_GAMECONTROLLER_AXIS_LEFTY        	= SDL_CONTROLLER_AXIS_LEFTY,
    EDV_GAMECONTROLLER_AXIS_RIGHTX       	= SDL_CONTROLLER_AXIS_RIGHTX,
    EDV_GAMECONTROLLER_AXIS_RIGHTY       	= SDL_CONTROLLER_AXIS_RIGHTY,
    EDV_GAMECONTROLLER_AXIS_TRIGGERLEFT  	= SDL_CONTROLLER_AXIS_TRIGGERLEFT,
    EDV_GAMECONTROLLER_AXIS_TRIGGERRIGHT 	= SDL_CONTROLLER_AXIS_TRIGGERRIGHT,
    EDV_GAMECONTROLLER_AXIS_MAX          	= SDL_CONTROLLER_AXIS_MAX
} EDV_GameControllerAxis;

// BUTTONS AVAILABLE
typedef enum
{
	//EDV_GAMECONTROLLER_BUTTON_A                         = SDL_CONTROLLER_BUTTON_INVALID,
    EDV_GAMECONTROLLER_BUTTON_A                         = SDL_CONTROLLER_BUTTON_A,
    EDV_GAMECONTROLLER_BUTTON_B                         = SDL_CONTROLLER_BUTTON_B,
    EDV_GAMECONTROLLER_BUTTON_X                         = SDL_CONTROLLER_BUTTON_X,
    EDV_GAMECONTROLLER_BUTTON_Y                         = SDL_CONTROLLER_BUTTON_Y,
    EDV_GAMECONTROLLER_BUTTON_BACK                      = SDL_CONTROLLER_BUTTON_BACK,
    EDV_GAMECONTROLLER_BUTTON_GUIDE                     = SDL_CONTROLLER_BUTTON_GUIDE,
    EDV_GAMECONTROLLER_BUTTON_START                     = SDL_CONTROLLER_BUTTON_START,
    EDV_GAMECONTROLLER_BUTTON_LEFTSTICK                 = SDL_CONTROLLER_BUTTON_LEFTSTICK,
    EDV_GAMECONTROLLER_BUTTON_RIGHTSTICK                = SDL_CONTROLLER_BUTTON_RIGHTSTICK,
    EDV_GAMECONTROLLER_BUTTON_LEFTSHOULDER              = SDL_CONTROLLER_BUTTON_LEFTSHOULDER,
    EDV_GAMECONTROLLER_BUTTON_RIGHTSHOULDER             = SDL_CONTROLLER_BUTTON_RIGHTSHOULDER,
    EDV_GAMECONTROLLER_BUTTON_DPAD_UP                   = SDL_CONTROLLER_BUTTON_DPAD_UP,
    EDV_GAMECONTROLLER_BUTTON_DPAD_DOWN                 = SDL_CONTROLLER_BUTTON_DPAD_DOWN,
    EDV_GAMECONTROLLER_BUTTON_DPAD_LEFT                 = SDL_CONTROLLER_BUTTON_DPAD_LEFT,
    EDV_GAMECONTROLLER_BUTTON_DPAD_RIGHT                = SDL_CONTROLLER_BUTTON_DPAD_RIGHT,
    EDV_GAMECONTROLLER_BUTTON_MAX           			= SDL_CONTROLLER_BUTTON_MAX
} EDV_GameControllerButton;



class CGameController: public CEventHandler
{
	public:
		explicit CGameController(s32 a_device_id, const std::string & a_device_guid);
		~CGameController();
		void	Update(f64 dt);

		// CEventHandler
		void	HandleEvent(const EVENT_TYPES a_Type, Uint32 a_Code, SDL_Event* a_Event = NULL);

		/**
		 *  Open the game controller for use.
		 */
		void			Attach();

		/**
		 *  Closes the game controller for use.
		 */
		void			Dettach();

		/**
		 *  Returns true if the controller has been opened and currently connected, if not return false
		 */
		bool			IsAttached() const;

		
		void			AddMapping(	const std::string &);
		std::string		GetName()	const;
		std::string		GetGUID()	const;


		// Insert an action to the associated a_RawButton.
		bool	InsertKeyAction(const std::string &a_ActionName, const EDV_GameControllerButton &a_RawButton );
		bool	InsertKeyAction(const std::string &a_ActionName, const std::string &a_RawButton);

		// Insert an action to the associated a_RawButton. Action Name is generated.
		std::string InsertKeyAction(const EDV_GameControllerButton &a_RawButton);



		// Remove an action
		void	RemoveKeyAction(const std::string &a_ActionName);
		
		//Returns true if the key for the given action is pressed down / false if the key isn't
		bool	IsActionKeyPushed(const std::string &a_ActionName);

		//Returns true if the key for the given action is Key Up
		bool	IsActionKeyUp(const std::string &a_ActionName);

		//Returns true if the key for the given is first keydown
		bool	IsActionKeyDown(const std::string &a_ActionName);

		bool	IsActionButtonReleased(const std::string& a_ActionName);
		

		Ogre::Vector2 getAxisvectorNormalized(const EDV_GameControllerAxis &a_axisX, const EDV_GameControllerAxis &a_axisY);

	private:
		SDL_GameController	*m_pcontroller;
		s32					m_id;
		std::string			m_guid;

		typedef enum
		{
			EV_KEY_UP = 0,
			EV_KEY_DOWN,
			EV_KEY_PUSHREPEAT,
			EV_KEY_RELEASED

		}EV_KeyStatus;

		bool IsActionKeyStatus(const std::string& a_ActionName, EV_KeyStatus a_KeyStatus);

		//Contains all the registered input key actions with their current State/Info
		typedef std::map< std::string, EDV_GameControllerButton >	t_KeyActionsMap;
		t_KeyActionsMap		m_RegisteredKeyActions;


		//Contains all the registered input Buttons and their status EV_KeyStatus
		typedef std::map< EDV_GameControllerButton, EV_KeyStatus >			t_KeyScanStatusMap;
		t_KeyScanStatusMap	m_RegisteredInputButtons;
		IdGenerator<u64>	m_AutoIdGen;

		//Contains all the AXIS
		std::array<s16, EDV_GAMECONTROLLER_AXIS_MAX>	m_axis;


		s16 m_threshodlvalue;;

};

#endif /* GAMECONTROLLER_H_ */
