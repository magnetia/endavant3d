#ifndef CINPUTMANAGER_H_
#define CINPUTMANAGER_H_
#include "CInputMouse.h"
#include "CInputKeyboard.h"
#include "CInputJoystick.h"
#include "CInputGameController.h"

#include <string>

class CInputManager
{
public:
	~CInputManager();

	void LoadActionsXML(const std::string &a_FileName){};//TODO IMPLEMENT

	inline CInputMouse& 	GetMouse() { return m_MouseInput; }
	inline CInputKeyboard& 	GetKeyboard() { return m_KeyboardInput; }
	inline CInputGameController& GetGameController(){ return m_GCInput; }

	void	StartUp(void);
	void	ShutDown(void);
	void 	Update(f64 dt);

private:

	CInputManager(); friend class CCoreEngine;

	CInputMouse 	m_MouseInput;
	CInputKeyboard 	m_KeyboardInput;
	
	CInputGameController m_GCInput;



};

#endif /* CINPUTMANAGER_H_ */
