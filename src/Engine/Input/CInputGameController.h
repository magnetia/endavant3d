/*
 * CInputGameController.h
 *
 *  Created on: 25/07/2013
 *      Author: Dani
 */

#ifndef CINPUTGAMECONTROLLER_H_
#define CINPUTGAMECONTROLLER_H_

#include <SDL2/SDL_gamecontroller.h>
#include "Events/CEventHandler.h"

#include "Core/CBasicTypes.h"

#include "CGameController.h"


#include <vector>
#include <string>
#include <memory>
#include <map>

typedef s32 GameController_id;
class CInputGameController: public CEventHandler
{
	public:
		CInputGameController();
		virtual ~CInputGameController();

		// ISubSystem
		void	StartUp(void);
		void	ShutDown(void);
		void	Update(f64 dt);

		// CEventHandler
		void	HandleEvent(const EVENT_TYPES a_Type, Uint32 a_Code, SDL_Event* a_Event = NULL);

		std::weak_ptr<CGameController> getGameController(const GameController_id);
		std::vector<GameController_id> getGameControllersIDs();


	private:
		void	LoadAllControllers();
		void	CloseAllControllers();

		void	RemoveController(GameController_id a_joyindex);
		void	AddController(GameController_id a_joyindex);


		typedef std::map< GameController_id, std::shared_ptr<CGameController> > t_GameControllersVec;
		t_GameControllersVec	m_gamecontrollers;



};

#endif /* CINPUTGAMECONTROLLER_H_ */

