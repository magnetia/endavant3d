#ifndef CINPUT_MOUSE_H_
#define CINPUT_MOUSE_H_

#include "Events/CEventHandler.h"
#include "Externals/glm/glm.hpp"
#include "Utils/IdGenerator.h"

#include <functional>
#include <string>
#include <map>
#include <array>
#include <vector>

enum EV_MOUSE_BUTTON
{
	EV_MOUSE_LB = SDL_BUTTON_LEFT,
	EV_MOUSE_MB = SDL_BUTTON_MIDDLE,
	EV_MOUSE_RB = SDL_BUTTON_RIGHT,
	EV_MOUSE_X1 = SDL_BUTTON_X1,
	EV_MOUSE_X2 = SDL_BUTTON_X2,

	EV_MOUSE_MAXBUTTONS = 5
};

typedef enum
{
	EV_MOUSEBUTTON_UP = 0,
	EV_MOUSEBUTTON_DOWN,
	EV_MOUSEBUTTON_PUSHREPEAT,
	EV_MOUSEBUTTON_RELEASED

}EV_MOUSE_BUTTON_STATUS;

class CInputMouse : public CEventHandler
{
public:
	typedef std::function<void(const std::string&)>	CallbackFunc;
	typedef u64										NotificationId;

	CInputMouse();
	~CInputMouse();

	// Insert an action to the associated E_MOUSE
	bool	InsertButtonAction(const std::string &a_ActionName, const std::string &a_RawKeyName );

	// Insert an action to the associated E_MOUSE. The action identifier is generated and returned
	std::string InsertButtonAction(const std::string& a_RawKeyName);

	// Removes an action
	void	RemoveButtonAction(const std::string& a_ActionName);

	//Returns true if the key for the given action is pressed down / false if the key isn't
	bool	IsActionButtonPushed(const std::string &a_ActionName);

	//Returns true if the key for the given action is Key Up
	bool	IsActionButtonUp(const std::string &a_ActionName);

	//Returns true if the key for the given is first keydown
	bool	IsActionButtonDown(const std::string &a_ActionName);

	//Returns true if the key for the given is first keyup
	bool	IsActionButtonReleased(const std::string &a_ActionName);

	//Set Relative mouse behaviour: While the mouse is in relative mode, the cursor is hidden, 
	//and the driver will try to report continuous motion in the current window. 
	//Only relative motion events will be delivered, the mouse position will not change. 
	void	SetRelativeBehaviour(bool _status);

	// Returns the current status for relative behavior
	bool	HasRelativeBehaviour();

	// Registers a callable object that will be used on next mouse change of demanded state
	NotificationId	NotifyNextChangeOfState(EV_MOUSE_BUTTON_STATUS a_status, CallbackFunc a_func);

	// Removes a pending notification
	void	RemoveNotification(NotificationId a_id);

	//Getters
	inline s32			GetPosX() 		{return m_MousePosition.x;};
	inline s32			GetPosY() 		{return m_MousePosition.y;};
	inline glm::ivec2 	GetPos()		{return m_MousePosition;};
	inline s32			GetPosRelX() 	{return m_MouseRelativePosition.x;};
	inline s32			GetPosRelY() 	{return m_MouseRelativePosition.y;};
	inline glm::ivec2 	GetPosRel()		{return m_MouseRelativePosition;};
	//glm::ivec2 	GetPosFromCenter();
	//glm::ivec2 	GetPosFromCenter(s32 _centerX, s32 _centerY);



	//CEventHandler
	void HandleEvent(const EVENT_TYPES a_Type, Uint32 a_Code, SDL_Event* a_Event = NULL);

	void StartUp(void);
	void ShutDown(void);
	void Update(f64 dt);

private:

	bool IsActionButtonStatus(const std::string &a_ActionName, const EV_MOUSE_BUTTON_STATUS a_ButtonStatus);
	void MouseActionUpdate(EV_MOUSE_BUTTON a_MouseButton, bool a_ValueToUpdate);
	void CheckPendingNotifications(u32 a_MouseState);

	// Current button state
	std::array<EV_MOUSE_BUTTON_STATUS,EV_MOUSE_MAXBUTTONS> 	m_MouseButtonsState;

	// Current mouse position
	glm::ivec2		m_MousePosition;
	glm::ivec2		m_MouseRelativePosition;

	// Translation tables
	typedef	std::map<std::string, EV_MOUSE_BUTTON>	t_MapStringToMouseType;
	typedef	std::map<EV_MOUSE_BUTTON, std::string>	t_MapMouseTypeToString;

	t_MapStringToMouseType					m_translateStrToMouseType;
	t_MapMouseTypeToString					m_translateMouseTypeToStr;

	//Contains all the registered input mouse actions with their current State/Info
	typedef std::map< std::string, EV_MOUSE_BUTTON >	t_MouseActionsMap;
	t_MouseActionsMap		m_RegisteredMouseActions;
	IdGenerator<u64>		m_AutoIdGen;

	// for use with notifications of changes of state
	struct Notification
	{
		NotificationId			id;
		EV_MOUSE_BUTTON_STATUS	status;
		CallbackFunc			callback;
	};

	typedef std::vector<Notification>	PendingNotifications;
	PendingNotifications				m_pendingNotifications;
	IdGenerator<NotificationId>			m_NotifIdGen;

};

#endif
