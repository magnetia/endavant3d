#ifndef CINPUT_KEYBOARD_H_
#define CINPUT_KEYBOARD_H_

#include "SDL2/SDL.h"
#include "Core/CBasicTypes.h"
#include "Utils/IdGenerator.h"

#include <string>
//TODO the maps in this class should be unordered_maps
//#include <unordered_map>
//
// #todo: y si simplement registrem una classe i ens retornen un identificador en un u64 que fem servir per referir-nos a l'accio? l'associacio amb accions per xml podria estar en una altra capa
// typedef u64 t_keyActionId;
// t_keyActionId insertKeyAction(const std::string& a_rawKeyName);
#include <map>
#include <functional>
#include <vector>

class CInputKeyboard
{
public:
	typedef std::function<void(const std::string&)>	CallbackFunc;
	typedef u64										NotificationId;

	CInputKeyboard();
	~CInputKeyboard();

	// Insert an action to the associated RawKeyName. RawKeyName will use the SDL convention found here: wiki.libsdl.org/moin.fcg/SDL_Keycode
	bool	InsertKeyAction(const std::string &a_ActionName, const std::string &a_RawKeyName );

	// Insert an action to the associated RawKeyName. Action Name is generated.
	std::string InsertKeyAction(const std::string &a_RawKeyName);

	// Remove an action
	void	RemoveKeyAction(const std::string &a_ActionName);

	//Returns true if the key for the given action is pressed down / false if the key isn't
	bool	IsActionKeyPushed(const std::string &a_ActionName);

	//Returns true if the key for the given action is Key Up
	bool	IsActionKeyUp(const std::string &a_ActionName);

	//Returns true if the key for the given is first keydown
	bool	IsActionKeyDown(const std::string &a_ActionName);

	// Registers a callable object that will be used on next key status change of demanded state
	NotificationId	NotifyNextKeyPressed(CallbackFunc a_func);

	// Removes a pending notification
	void	RemoveNotification(NotificationId a_id);

	void StartUp(void);
	void ShutDown(void);
	void Update(f64 dt);


private:
	typedef enum
	{
		EV_KEY_UP = 0,
		EV_KEY_DOWN,
		EV_KEY_PUSHREPEAT

	}EV_KeyStatus;

	bool IsActionKeyStatus(const std::string& a_ActionName, EV_KeyStatus a_KeyStatus);
	void UpdateKeyActions();
	void CheckNotifications();

	//Mantains the state of the keyboard updated every frame
	const Uint8 *	m_KeyBoardState;

	//Contains all the registered input key actions with their current State/Info
	typedef std::map< std::string, SDL_Scancode >	t_KeyActionsMap;
	t_KeyActionsMap		m_RegisteredKeyActions;

	//Contains all the registered input Scancodes and their status EV_KeyStatus
	typedef std::map< SDL_Scancode, EV_KeyStatus >			t_KeyScanStatusMap;
	t_KeyScanStatusMap	m_RegisteredInputScancodes;
	IdGenerator<u64>	m_AutoIdGen;

	// for use with notifications of changes of state
	struct Notification
	{
		NotificationId	id;
		CallbackFunc	callback;
	};

	typedef std::vector<Notification>	PendingNotifications;
	PendingNotifications				m_pendingNotifications;
	IdGenerator<NotificationId>			m_NotifIdGen;
};

#endif
