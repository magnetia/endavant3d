#include "CGameController.h"
#include "Options\OptionManager.h"
#include "Core/CCoreEngine.h"
#include "Core/CLogManager.h"
//#include "SDL2/SDL_joystick.h"
#include <algorithm>


CGameController::CGameController(s32 a_device_id, const std::string & a_device_guid):
m_pcontroller(nullptr),
m_id(a_device_id),
m_guid(a_device_guid)
{
	m_axis.fill(0);

	LOG( LOG_INFO, LOGSUB_INPUT, "GameController CONSTRUCT: %d", a_device_id);

	AddEvent(EVENT_SDL, SDL_CONTROLLERDEVICEREMAPPED);		/**< The controller mapping was updated */
	AddEvent(EVENT_SDL, SDL_CONTROLLERAXISMOTION);			/**< Game controller axis motion */
	AddEvent(EVENT_SDL, SDL_CONTROLLERBUTTONDOWN);		/**< Game controller button pressed */
	AddEvent(EVENT_SDL, SDL_CONTROLLERBUTTONUP);			/**< Game controller button released */

	RegisterEventManager();

	//Calculo el threshold
	//-32768 to 32767 (sense el percentatge threshold)
	const f64 thresholdpercent = CCoreEngine::Instance().GetOptionManager().getOption("controls/gpadthresholdpercent");
	m_threshodlvalue = (s16)((static_cast<f64>(std::numeric_limits<s16>::max()) * thresholdpercent) / 100.0);

}

CGameController::~CGameController()
{
	LOG( LOG_INFO, LOGSUB_INPUT, "GameController DESTRUCT: %d", m_id);
	Dettach();

	if (m_pcontroller != nullptr)
		SDL_GameControllerClose(m_pcontroller);
}


void CGameController::HandleEvent(const EVENT_TYPES a_Type, u32 a_Code,SDL_Event* a_Event)
{

	switch (a_Code)
	{
		case SDL_CONTROLLERDEVICEREMAPPED:
			if (a_Event->cdevice.which == m_id)
			{
				LOG(LOG_INFO, LOGSUB_INPUT, "ID: %d SDL_CONTROLLERDEVICEREMAPPED ", a_Event->cdevice.which);

			}
			break;

		case SDL_CONTROLLERAXISMOTION:
			if (a_Event->caxis.which == m_id)
			{
				/*
				if (a_Event->caxis.axis != SDL_CONTROLLER_AXIS_INVALID)
				{

					if (a_Event->caxis.value > m_threshodlvalue)
					{
						m_axis[a_Event->caxis.axis] = a_Event->caxis.value - m_threshodlvalue;
						
					}
					else if (a_Event->caxis.value < (-m_threshodlvalue))
					{
						m_axis[a_Event->caxis.axis] = a_Event->caxis.value + m_threshodlvalue;
					}
					else
						m_axis[a_Event->caxis.axis] = 0;

					LOG(LOG_INFO, LOGSUB_INPUT, "ID: %d SDL_CONTROLLERAXISMOTION : AXIS: %d  Value: %d  ValueStored: %d", a_Event->caxis.which, a_Event->caxis.axis, a_Event->caxis.value, m_axis[a_Event->caxis.axis]);

				}*/
			}
			break;

		case SDL_CONTROLLERBUTTONDOWN:
			if (a_Event->cbutton.which == m_id)
			{
				LOG(LOG_INFO, LOGSUB_INPUT, "ID: %d SDL_CONTROLLERBUTTONDOWN: %d ", a_Event->cbutton.which, a_Event->cbutton.button);

				auto it_button = m_RegisteredInputButtons.find(static_cast<EDV_GameControllerButton>(a_Event->cbutton.button));
				if (it_button != m_RegisteredInputButtons.end())
				{
					if (it_button->second == EV_KEY_UP)
						it_button->second = EV_KEY_DOWN;
					else
						it_button->second = EV_KEY_PUSHREPEAT;
				}
			}
			break;

		case SDL_CONTROLLERBUTTONUP:
			if (a_Event->cbutton.which == m_id)
			{
				LOG(LOG_INFO, LOGSUB_INPUT, "ID: %d SDL_CONTROLLERBUTTONUP : %d ", a_Event->cbutton.which, a_Event->cbutton.button);
				auto it_button = m_RegisteredInputButtons.find(static_cast<EDV_GameControllerButton>(a_Event->cbutton.button));
				if (it_button != m_RegisteredInputButtons.end())
				{
					if (it_button->second == EV_KEY_PUSHREPEAT)
						it_button->second = EV_KEY_RELEASED;
					else
						it_button->second = EV_KEY_UP;
					
				}

			}
			break;
	}

}


void	CGameController::Update(f64 dt)
{
	for (auto &it_butt : m_RegisteredInputButtons)
	{
		if (it_butt.second == EV_KEY_DOWN)
			it_butt.second = EV_KEY_PUSHREPEAT;

		if (it_butt.second == EV_KEY_RELEASED)
			it_butt.second = EV_KEY_UP;
	}

	for (s32 axisindex = 0; axisindex < EDV_GAMECONTROLLER_AXIS_MAX; axisindex++)
	{
		s16 value = SDL_GameControllerGetAxis(m_pcontroller, static_cast<SDL_GameControllerAxis>(axisindex));
		if (value > m_threshodlvalue)
		{
			m_axis[axisindex] = value - m_threshodlvalue;
		}
		else if (value < (-m_threshodlvalue))
		{
			m_axis[axisindex] = value + m_threshodlvalue;
		}
		else
		{
			m_axis[axisindex] = 0;
		}
		//if (m_id == 0  && (axisindex == 1 || axisindex == 0))		LOG(LOG_INFO, LOGSUB_INPUT, "ID: %d m_AXIS[%d] =  %d", m_id, axisindex,m_axis[axisindex]);
	}
	

}

void CGameController::AddMapping(const std::string& a_mapping)
{
}

std::string CGameController::GetName() const
{
	return SDL_GameControllerName(m_pcontroller);
}

void CGameController::Attach()
{

	/**
	 *  Open a game controller for use.
	 *  The index passed as an argument refers to the N'th game controller on the system.
	 *  This index is the value which will identify this controller in future controller
	 *  events.
	 *
	 *  \return A controller identifier, or NULL if an error occurred.
	 */
	if (!IsAttached() )
	{
		m_pcontroller = SDL_GameControllerOpen(m_id);
		auto joy = SDL_GameControllerGetJoystick(m_pcontroller);
		m_id = SDL_JoystickInstanceID(joy);
		if (m_pcontroller == nullptr)
		{
			LOG( LOG_INFO, LOGSUB_INPUT, "GameController { ID: %d, GUID: %s, Name: %s } Can't Attach -> %s!!!",m_id,  m_guid.c_str(),  GetName().c_str(), SDL_GetError());
		}
		else
		{
			LOG( LOG_INFO, LOGSUB_INPUT, "GameController { ID: %d, GUID: %s, Name: %s } Attached!!!", m_id,m_guid.c_str(), GetName().c_str());
			LOG( LOG_INFO, LOGSUB_INPUT, "GameController { ID: %d, GUID: %s, Name: %s } Attached Mapping: %s", m_id,m_guid.c_str(), GetName().c_str(),SDL_GameControllerMapping(m_pcontroller));
		}

	}



}

void CGameController::Dettach()
{
	if (IsAttached())
		SDL_GameControllerClose(m_pcontroller);

}

bool CGameController::IsAttached() const
{
	return SDL_GameControllerGetAttached(m_pcontroller) > 0;
}

std::string CGameController::GetGUID() const
{
	return m_guid;
}

bool	CGameController::InsertKeyAction(const std::string &a_ActionName, const std::string &a_RawButton)
{
	const EDV_GameControllerButton sdl_butt = static_cast<EDV_GameControllerButton>(SDL_GameControllerGetButtonFromString(a_RawButton.c_str()));

	if (sdl_butt == SDL_CONTROLLER_BUTTON_INVALID)
		return false;
	else
	{
		return InsertKeyAction(a_ActionName, sdl_butt);
	}



}

bool CGameController::InsertKeyAction(const std::string &a_ActionName, const EDV_GameControllerButton &a_RawButton)
{

	//Check if that actionname exists if not exists add it to both maps
	if (m_RegisteredKeyActions.find(a_ActionName) == m_RegisteredKeyActions.end())
	{

		m_RegisteredKeyActions[a_ActionName] = a_RawButton;

		//Init the scancode to false (Keyup) if not exists
		if (m_RegisteredInputButtons.find(a_RawButton) == m_RegisteredInputButtons.end())
			m_RegisteredInputButtons[a_RawButton] = EV_KEY_UP;

		return true;
	}

	LOG( LOG_WARNING, LOGSUB_INPUT,"CInputKeyboard::InsertKeyAction ACTION-> Action: %s Exists!!  Button: %d",
		a_ActionName.c_str(), a_RawButton);

	return false;
}

std::string CGameController::InsertKeyAction(const EDV_GameControllerButton &a_RawButton)
{
	std::string actionId;

	do
	{
		actionId = to_string(m_AutoIdGen.nextId());
	} while (m_RegisteredKeyActions.find(actionId) != m_RegisteredKeyActions.end());

	InsertKeyAction(actionId, a_RawButton);

	return actionId;

}

void CGameController::RemoveKeyAction(const std::string &a_ActionName)
{
	m_RegisteredKeyActions.erase(a_ActionName);
}

bool	CGameController::IsActionKeyPushed(const std::string &a_ActionName)
{
	// tambe comprovem el KEY_DOWN, sino saltem 1 frame (i la tecla si k esta apretada)
	return IsActionKeyStatus(a_ActionName, EV_KEY_PUSHREPEAT) || IsActionKeyStatus(a_ActionName, EV_KEY_DOWN);
}

bool CGameController::IsActionKeyUp(const std::string& a_ActionName)
{
	return IsActionKeyStatus(a_ActionName, EV_KEY_UP);
}

bool CGameController::IsActionKeyDown(const std::string& a_ActionName)
{
	return IsActionKeyStatus(a_ActionName, EV_KEY_DOWN);
}

bool CGameController::IsActionKeyStatus(const std::string& a_ActionName, EV_KeyStatus a_KeyStatus)
{
	auto l_Action = m_RegisteredKeyActions.find(a_ActionName);

	if ( l_Action != m_RegisteredKeyActions.end())
	{
		if (m_RegisteredInputButtons[l_Action->second] == a_KeyStatus)
			return true;
		else
			return false;
	}

	//If not exists return false;
	return false;
}


bool CGameController::IsActionButtonReleased(const std::string& a_ActionName)
{
	return IsActionKeyStatus(a_ActionName, EV_KEY_RELEASED);
}



Ogre::Vector2 CGameController::getAxisvectorNormalized(const EDV_GameControllerAxis &a_axisX, const EDV_GameControllerAxis &a_axisY)
{
	Ogre::Vector2 vec;

	vec.x = m_axis[a_axisX];
	vec.y = m_axis[a_axisY];

	vec.normalise();
	return vec;


}
