#include "CInputManager.h"
#include "Core/CCoreEngine.h"
#include "Core/CLogManager.h"

CInputManager::CInputManager()
{

}

CInputManager::~CInputManager()
{

}

void CInputManager::StartUp()
{
	LOG( LOG_INFO, LOGSUB_INPUT,"Starting Up!");


	m_KeyboardInput.StartUp();

	m_MouseInput.StartUp();

	

	m_GCInput.StartUp();
}

void CInputManager::ShutDown()
{
	LOG( LOG_INFO, LOGSUB_INPUT,"Shutting Down!");

	m_GCInput.ShutDown();
	
	m_MouseInput.ShutDown();
	m_KeyboardInput.ShutDown();
}

void CInputManager::Update(f64 dt)
{
	m_KeyboardInput.Update(dt);
	m_MouseInput.Update(dt);
	m_GCInput.Update(dt);
	
}
