#include "SDL2/SDL.h"
#include "CInputMouse.h"
#include "Events/CEventCodes.h"
#include <algorithm>
#include "Core/CCoreEngine.h"
#include "Renderer/CRenderManager.h"

//For debug purposes
#include "Core/CLogManager.h"

CInputMouse::CInputMouse():
m_MouseButtonsState{ { EV_MOUSEBUTTON_UP, EV_MOUSEBUTTON_UP, EV_MOUSEBUTTON_UP } },
m_MousePosition(0,0),
m_MouseRelativePosition(0,0)
{
}

CInputMouse::~CInputMouse()
{

}

void CInputMouse::StartUp(void)
{
	AddEvent(EVENT_SDL, SDL_MOUSEMOTION);
	AddEvent(EVENT_SDL, SDL_MOUSEWHEEL);

	RegisterEventManager();

	// Create Translation tables
	m_translateStrToMouseType["MOUSE_LB"]	= EV_MOUSE_LB;
	m_translateStrToMouseType["MOUSE_MB"]	= EV_MOUSE_MB;
	m_translateStrToMouseType["MOUSE_RB"]	= EV_MOUSE_RB;
	m_translateStrToMouseType["MOUSE_X1"]	= EV_MOUSE_X1;
	m_translateStrToMouseType["MOUSE_X2"]	= EV_MOUSE_X2;

	m_translateMouseTypeToStr[EV_MOUSE_LB] = "MOUSE_LB";
	m_translateMouseTypeToStr[EV_MOUSE_MB] = "MOUSE_MB";
	m_translateMouseTypeToStr[EV_MOUSE_RB] = "MOUSE_RB";
	m_translateMouseTypeToStr[EV_MOUSE_X1] = "MOUSE_X1";
	m_translateMouseTypeToStr[EV_MOUSE_X2] = "MOUSE_X2";
}

void CInputMouse::ShutDown(void)
{
	UnRegisterEventManager();
}

void CInputMouse::Update(f64 dt)
{
	//Get current mouse state
	auto l_CurrMouseState = SDL_GetMouseState(&m_MousePosition.x, &m_MousePosition.y);
	SDL_GetRelativeMouseState(&m_MouseRelativePosition.x, &m_MouseRelativePosition.y);

	//Passo a coordenades abaix esquerra
	auto win_height = CCoreEngine::Instance().GetRenderManager().GetRenderWindow()->getHeight();
	m_MousePosition.y = -m_MousePosition.y + win_height - 1;  //PAso a coordenades abaix esquerra El -1 perque vagi de 0 , win_height-1
	m_MouseRelativePosition.y = -m_MouseRelativePosition.y;

	// Check if there are pending notifications
	CheckPendingNotifications(l_CurrMouseState);

	// Update all Mouse states
	for (u32 idx = 0; idx < m_MouseButtonsState.size(); idx++)
	{
		auto& buttonState = m_MouseButtonsState[idx];

		if (l_CurrMouseState & SDL_BUTTON(static_cast<EV_MOUSE_BUTTON>(idx)))
		{
			if (buttonState == EV_MOUSEBUTTON_UP)
				buttonState = EV_MOUSEBUTTON_DOWN;
			else
				buttonState = EV_MOUSEBUTTON_PUSHREPEAT;
		}
		else
		{
			if (buttonState == EV_MOUSEBUTTON_PUSHREPEAT)
				buttonState = EV_MOUSEBUTTON_RELEASED;
			else
				buttonState = EV_MOUSEBUTTON_UP;
		}
	}
}

bool CInputMouse::IsActionButtonPushed(const std::string& a_ActionName)
{
	return IsActionButtonStatus(a_ActionName,EV_MOUSEBUTTON_PUSHREPEAT) || IsActionButtonStatus(a_ActionName,EV_MOUSEBUTTON_DOWN);
}

bool CInputMouse::IsActionButtonUp(const std::string& a_ActionName)
{
	return IsActionButtonStatus(a_ActionName,EV_MOUSEBUTTON_UP);
}

bool CInputMouse::IsActionButtonDown(const std::string& a_ActionName)
{
	return IsActionButtonStatus(a_ActionName,EV_MOUSEBUTTON_DOWN);
}

bool CInputMouse::IsActionButtonReleased(const std::string& a_ActionName)
{
	return IsActionButtonStatus(a_ActionName, EV_MOUSEBUTTON_RELEASED);
}

bool	CInputMouse::IsActionButtonStatus(const std::string &a_ActionName, const EV_MOUSE_BUTTON_STATUS a_ButtonStatus)
{
	auto l_Action = m_RegisteredMouseActions.find(a_ActionName);

	if ( l_Action != m_RegisteredMouseActions.end())
	{
		if (m_MouseButtonsState[l_Action->second] == a_ButtonStatus)
			return true;
		else
			return false;
	}

	LOG( LOG_WARNING, LOGSUB_INPUT ,"IsActionActive: Action NOT Exists" );
	return false;
}



void CInputMouse::HandleEvent(const EVENT_TYPES a_Type, Uint32 a_Code, SDL_Event* a_Event)
{
	/*
	if (a_Event != NULL)
	{
		switch(a_Event->type)
		{
			case SDL_MOUSEMOTION:
			{
				m_MousePosition.x = a_Event->motion.x;
				m_MousePosition.y = a_Event->motion.y;
				m_MouseRelativePosition.x = a_Event->motion.xrel;
				m_MouseRelativePosition.y = a_Event->motion.yrel;
			}
			break;

		}

	}*/
}


void CInputMouse::SetRelativeBehaviour(bool a_enabled)
{
	SDL_SetRelativeMouseMode(static_cast<SDL_bool>(a_enabled));
}

bool CInputMouse::HasRelativeBehaviour()
{
	return SDL_GetRelativeMouseMode() ? true : false;
}

/*
glm::ivec2 	CInputMouse::GetPosFromCenter()
{
	auto win_height = CCoreEngine::Instance().GetRenderManager().GetRenderWindow()->getHeight();
	auto win_width = CCoreEngine::Instance().GetRenderManager().GetRenderWindow()->getWidth();

	//std::cout << "X: " << win_height<< " Y: " << win_width ;

	glm::ivec2 win_center(win_width / 2, win_height / 2);
	

	auto pos = GetPos() - win_center;

	
	pos.y = -pos.y;
	//std::cout << "	PosX: " << pos.x << " PosY: " << pos.y << std::endl;

	return pos;
}

glm::ivec2 	CInputMouse::GetPosFromCenter(s32 _centerX , s32 _centerY)
{
	//auto win_height = CCoreEngine::Instance().GetRenderManager().GetRenderWindow()->getHeight();
	//auto win_width = CCoreEngine::Instance().GetRenderManager().GetRenderWindow()->getWidth();

	//std::cout << "X: " << win_height<< " Y: " << win_width ;

	glm::ivec2 win_center(_centerX, _centerY);


	auto pos = GetPos() - win_center;


	pos.y = -pos.y;
	//std::cout << "	PosX: " << pos.x << " PosY: " << pos.y << std::endl;

	return pos;
}
*/
CInputMouse::NotificationId	CInputMouse::NotifyNextChangeOfState(EV_MOUSE_BUTTON_STATUS a_status, CallbackFunc a_func)
{
	NotificationId nextId = m_NotifIdGen.nextId();
	m_pendingNotifications.emplace_back(Notification{nextId, a_status, a_func});

	return  nextId;
}

void CInputMouse::RemoveNotification(NotificationId a_id)
{
	m_pendingNotifications.erase(
		std::remove_if(m_pendingNotifications.begin(), m_pendingNotifications.end(), 
			[&](const Notification& n){ return a_id == n.id; }), m_pendingNotifications.end());
}

bool CInputMouse::InsertButtonAction(const std::string &a_ActionName, const std::string &a_RawMouseButtonName )
{
	auto l_mouseinputenum = m_translateStrToMouseType.find(a_RawMouseButtonName);

	if ( l_mouseinputenum != m_translateStrToMouseType.end() )
	{
		//Check if that actionname exists if not exists add it to both maps
		if (m_RegisteredMouseActions.find(a_ActionName) == m_RegisteredMouseActions.end())
		{
			m_RegisteredMouseActions[a_ActionName] = l_mouseinputenum->second;
			return true;
		}
		else
		{
			LOG( LOG_WARNING, LOGSUB_INPUT ,"InsertButtonAction: Action Exists" );
			return false;
		}

	}
	else
	{
		LOG( LOG_ERROR, LOGSUB_INPUT ,"InsertButtonAction: Action Exists" );
		return false;
	}


}

std::string CInputMouse::InsertButtonAction(const std::string& a_RawKeyName)
{
	std::string actionId;

	do
	{
		actionId = to_string(m_AutoIdGen.nextId());
	} 
	while (m_RegisteredMouseActions.find(actionId) != m_RegisteredMouseActions.end());

	InsertButtonAction(actionId, a_RawKeyName);

	return actionId;
}

void CInputMouse::RemoveButtonAction(const std::string& a_ActionName)
{
	m_RegisteredMouseActions.erase(a_ActionName);
}

void CInputMouse::CheckPendingNotifications(u32 a_MouseState)
{
	m_pendingNotifications.erase(
		std::remove_if(m_pendingNotifications.begin(), m_pendingNotifications.end(),
		[&](const Notification& notif)
	{
		bool remove = false;

		if (a_MouseState == notif.status)
		{
			for (u32 idx = 0; idx < EV_MOUSE_MAXBUTTONS; idx++)
			{
				EV_MOUSE_BUTTON button = static_cast<EV_MOUSE_BUTTON>(idx);

				if (a_MouseState & SDL_BUTTON(button))
				{
					notif.callback(m_translateMouseTypeToStr[button]);
				}
			}

			remove = true;
		}

		return remove;
	}), m_pendingNotifications.end());
}
