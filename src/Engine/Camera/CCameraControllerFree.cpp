#include "Camera\CCameraControllerFree.h"
#include "Core/CCoreEngine.h"
#include "Input/CInputManager.h"
#include "Renderer/CRenderManager.h"

CCameraFree::CCameraFree(const std::string & a_camname, f32 a_linearvelocity ):
Camera(a_camname)
{
	//Jerarquia de transformacions
	m_CamYawNode = m_CamNode->createChildSceneNode();
	m_CamPitchNode = m_CamYawNode->createChildSceneNode();
	m_CamRollNode = m_CamPitchNode->createChildSceneNode();

	//Attach de la camera a lultima transformacio
	m_CamRollNode->attachObject(m_cam);

	auto &keyb = CCoreEngine::Instance().GetInputManager().GetKeyboard();
	auto &mouse = CCoreEngine::Instance().GetInputManager().GetMouse();

	keyb.InsertKeyAction("CAM_UP", "Space");
	keyb.InsertKeyAction("CAM_DOWN", "Left Ctrl");
	keyb.InsertKeyAction("CAM_SPEED", "Left Shift");

	keyb.InsertKeyAction("CAM_RIGHT", "D");
	keyb.InsertKeyAction("CAM_LEFT", "A");
	keyb.InsertKeyAction("CAM_FORWARD", "W");
	keyb.InsertKeyAction("CAM_BACKWARD", "S");

	keyb.InsertKeyAction("CAM_VEL_UP", "O");
	keyb.InsertKeyAction("CAM_VEL_DOWN", "L");

	SetLinearVelocity(a_linearvelocity);
	
	
}

CCameraFree::~CCameraFree()
{
	auto l_scene_mgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();
	l_scene_mgr->destroySceneNode(m_CamYawNode);
	l_scene_mgr->destroySceneNode(m_CamPitchNode);
	l_scene_mgr->destroySceneNode(m_CamRollNode);


	
}
void CCameraFree::OnActivate()
{
	auto &mouse = CCoreEngine::Instance().GetInputManager().GetMouse();
	mouse.SetRelativeBehaviour(true);

}

void CCameraFree::OnDeactivate()
{
	auto &mouse = CCoreEngine::Instance().GetInputManager().GetMouse();
	mouse.SetRelativeBehaviour(false);

}

void CCameraFree::Update(f64 dt)
{
	Ogre::Real distance = m_linear_vel * static_cast<f32>(dt);

	static s32 speed_multiplier = 0;
	static const s32 speed_multiplier_max = 6;
	if (speed_multiplier < speed_multiplier_max)
	{
		// Move camera upwards along to world's Y-axis.
		if (isActionKeyPushed("CAM_VEL_UP"))
			speed_multiplier++;

	}

	if (speed_multiplier >(-speed_multiplier_max))
	{
		// Move camera upwards along to world's Y-axis.
		if (isActionKeyPushed("CAM_VEL_DOWN"))
			speed_multiplier--;

	}

	if (speed_multiplier > 0)
		distance *= speed_multiplier;
	else if (speed_multiplier < 0)
		distance /= (-speed_multiplier);

	if (isActionKeyPushed("CAM_SPEED"))
		distance *= 3.0f;

	Ogre::Vector3 l_translateVector(Ogre::Real(0));

	// Move camera upwards along to world's Y-axis.
	if (isActionKeyPushed("CAM_UP"))
		m_CamNode->translate(Ogre::Vector3(0, distance, 0));


	// Move camera downwards along to world's Y-axis.
	if (isActionKeyPushed("CAM_DOWN"))
		m_CamNode->translate(Ogre::Vector3(0, -distance, 0));

	// Move camera forward.
	if (isActionKeyPushed("CAM_FORWARD"))
		l_translateVector.z = -(distance);

	// Move camera backward.
	if (isActionKeyPushed("CAM_BACKWARD"))
		l_translateVector.z = distance;

	// Move camera left.
	if (isActionKeyPushed("CAM_LEFT"))
		l_translateVector.x = -(distance);

	// Move camera right.
	if (isActionKeyPushed("CAM_RIGHT"))
		l_translateVector.x = distance;

	Ogre::Real pitchAngle;
	Ogre::Real pitchAngleSign;
	Ogre::Vector2 mouseRelPos{getMouseRelPosition()};

	Ogre::Real xrel = mouseRelPos.x / static_cast<Ogre::Real>(4.0);
	Ogre::Real yrel = mouseRelPos.y / static_cast<Ogre::Real>(4.0);

	Ogre::Degree xrot(-xrel);
	Ogre::Degree yrot(-yrel);

	//std::cout << "Mouse RelX: " << mouse.GetPosRelX() << " RelY: " << mouse.GetPosRelY() << " | dt: " << dt << " speed: " << m_speed << " distance: " << distance << std::endl;

	// Yaws the camera according to the mouse relative movement.
	m_CamYawNode->yaw(Ogre::Radian(xrot));

	// Pitches the camera according to the mouse relative movement.
	m_CamPitchNode->pitch(Ogre::Radian(yrot));

	// Translates the camera according to the translate vector which is
	// controlled by the keyboard arrows.
	//
	// NOTE: We multiply the mTranslateVector by the cameraPitchNode's
	// orientation quaternion and the cameraYawNode's orientation
	// quaternion to translate the camera accoding to the camera's
	// orientation around the Y-axis and the X-axis.
	m_CamNode->translate(m_CamYawNode->getOrientation() *
		m_CamPitchNode->getOrientation() *
		l_translateVector,
		Ogre::SceneNode::TS_LOCAL);

	// Angle of rotation around the X-axis.
	pitchAngle = (2 * Ogre::Degree(Ogre::Math::ACos(m_CamPitchNode->getOrientation().w)).valueDegrees());

	// Just to determine the sign of the angle we pick up above, the
	// value itself does not interest us.
	pitchAngleSign = m_CamPitchNode->getOrientation().x;

	// Limit the pitch between -90 degress and +90 degrees
	if (pitchAngle > 90.0f)
	{
		if (pitchAngleSign > 0)
			// Set orientation to 90 degrees on X-axis.
			m_CamPitchNode->setOrientation(Ogre::Quaternion(Ogre::Math::Sqrt(0.5f),
			Ogre::Math::Sqrt(0.5f), 0, 0));
		else if (pitchAngleSign < 0)
			// Sets orientation to -90 degrees on X-axis.
			m_CamPitchNode->setOrientation(Ogre::Quaternion(Ogre::Math::Sqrt(0.5f),
			-Ogre::Math::Sqrt(0.5f), 0, 0));
	}
}

void CCameraFree::SetLinearVelocity(f32 speed)
{
	m_linear_vel = speed;
}

f32 CCameraFree::GetLinearVelocity() const
{
	return m_linear_vel;
}

