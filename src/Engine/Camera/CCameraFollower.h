
#ifndef CCAMERA_FOLLOWER_H_
#define CCAMERA_FOLLOWER_H_

#include "Camera\Camera.h"

// MORE INFO: 
// http://www.ogre3d.org/tikiwiki/tiki-index.php?page=Creating+a+simple+first-person+camera+system&structure=Tutorials
class CCameraFollower : public Camera
{
public:
	
	CCameraFollower(const std::string & a_camname, Ogre::SceneNode *_nodetofollow);
	void Update(f64 dt);
	void SetNodeToFollow(Ogre::SceneNode * _node_to_folllow);
	virtual	~CCameraFollower();
	void OnActivate();
	
	void OnDeactivate();

private:
	void updateCameraGoal(Ogre::Real deltaYaw, Ogre::Real deltaPitch, Ogre::Real deltaZoom);

	Ogre::SceneNode *m_obj_tofollow;
	Ogre::SceneNode *m_CameraPivot;
	Ogre::SceneNode *m_CameraGoal;
	Ogre::Real	m_pivotPicth;
};


#endif