#include "Camera\CCameraFollower.h"
#include "Core\CCoreEngine.h"
#include "Renderer\CRenderManager.h"
#include "Input\CInputManager.h"


CCameraFollower::CCameraFollower(const std::string & a_camname, Ogre::SceneNode *_nodetofollow):
Camera(a_camname)
{
	SetNodeToFollow(_nodetofollow);
	
	// create a pivot at roughly the character's shoulder
	m_CameraPivot = m_cam->getSceneManager()->getRootSceneNode()->createChildSceneNode();
	// this is where the camera should be soon, and it spins around the pivot
	// @TODO passar a xml!
	m_CameraGoal = m_CameraPivot->createChildSceneNode(Ogre::Vector3(0, 0, 100));
	// this is where the camera actually is
	//m_CameraNode = _cam->getSceneManager()->getRootSceneNode()->createChildSceneNode();
	m_CamNode->setPosition(m_CameraPivot->getPosition() + m_CameraGoal->getPosition());

	m_CameraPivot->setFixedYawAxis(true);
	m_CameraGoal->setFixedYawAxis(true);
	m_CamNode->setFixedYawAxis(true);

	m_CamNode->attachObject(m_cam);
	m_pivotPicth = 0;

}

void CCameraFollower::updateCameraGoal(Ogre::Real deltaYaw, Ogre::Real deltaPitch, Ogre::Real deltaZoom)
{
	m_CameraPivot->yaw(Ogre::Degree(deltaYaw), Ogre::Node::TS_WORLD);

	// bound the pitch
	if (!(m_pivotPicth + deltaPitch > -10 && deltaPitch > 0) &&
		!(m_pivotPicth + deltaPitch < -60 && deltaPitch < 0))
	{
		m_CameraPivot->pitch(Ogre::Degree(deltaPitch), Ogre::Node::TS_LOCAL);
		m_pivotPicth += deltaPitch;
	}

	Ogre::Real dist = m_CameraGoal->_getDerivedPosition().squaredDistance(m_CameraPivot->_getDerivedPosition());
	Ogre::Real distChange = deltaZoom * dist;

	// bound the zoom
	if (!(dist + distChange < 8 && distChange < 0) &&
		!(dist + distChange > 25 && distChange > 0))
	{
		m_CameraGoal->translate(0, 0, distChange, Ogre::Node::TS_LOCAL);
	}
}

void CCameraFollower::SetNodeToFollow(Ogre::SceneNode * _node_to_folllow)
{

	m_obj_tofollow = _node_to_folllow;
}


CCameraFollower::~CCameraFollower()
{
	auto l_scene_mgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();

	l_scene_mgr->destroySceneNode(m_CameraPivot);
	l_scene_mgr->destroySceneNode(m_CameraGoal);

}

void CCameraFollower::OnActivate()
{
	auto &mouse = CCoreEngine::Instance().GetInputManager().GetMouse();
	mouse.SetRelativeBehaviour(true);

}

void CCameraFollower::OnDeactivate()
{
	auto &mouse = CCoreEngine::Instance().GetInputManager().GetMouse();
	mouse.SetRelativeBehaviour(false);
	
}


void CCameraFollower::Update(f64 dt)
{
	//Agafo la posicio actual de la camera
	//	m_CamNode->setPosition(0,300,0);
	//m_CamNode->lookAt(m_obj_tofollow->_getDerivedPosition(),);

	
	auto &mouse = CCoreEngine::Instance().GetInputManager().GetMouse();
	//updateCameraGoal(-0.50f * mouse.GetPosRelX(), -0.50f * mouse.GetPosRelY(), 0);


	// place the camera pivot roughly at the character's shoulder
	m_CameraPivot->setPosition(m_obj_tofollow->getPosition() + Ogre::Vector3::UNIT_Y * 1);
	// move the camera smoothly to the goal
	Ogre::Vector3 goalOffset = m_CameraGoal->_getDerivedPosition() - m_CamNode->getPosition();
	//m_CamNode->translate(goalOffset *static_cast<f32>(dt)* 9.0f);
	m_CamNode->translate(goalOffset);
	// always look at the pivot
	m_CamNode->lookAt(m_CameraPivot->_getDerivedPosition(), Ogre::Node::TS_WORLD);


}

