
#ifndef CAMERA_H_
#define CAMERA_H_
#include "Core/CBasicTypes.h"
#include "OGRE/Ogre.h"
#include "Game/GameEntity.h"
#include "Game/Behavior.h"


class Camera
{
public:
	Camera(const std::string & cam_name);
	
	virtual void Update(f64 dt);
	virtual void Render();
	virtual ~Camera();
	virtual void OnActivate(){};
	virtual void OnDeactivate(){};
	Ogre::Vector2 TransformWorldCoordToScreenCoord(const Ogre::Vector3 &_coord);
	Ogre::Vector3 TransformScreenCoordToWorldCoord(const Ogre::Vector2 &_coord);
	void setInputEnabled(bool a_enabled);
	bool isInputEnabled() const;
	
	const std::string& getName() const { return m_name; }
	Ogre::Camera * GetCamera() const { return m_cam; }

protected:
	bool isActionKeyPushed(const std::string& a_action) const;
	Ogre::Vector2 getMouseRelPosition() const;
	
	std::string		m_name;
	std::string		m_internalcamname;
	Ogre::Camera 	*m_cam;
	Ogre::SceneNode *m_CamNode;				//Situacio global respecte rootScene
	bool			m_inputEnabled;
};


#endif 
