
#ifndef CCAMERA_FREE_H_
#define CCAMERA_FREE_H_

#include "Camera/Camera.h"

// MORE INFO: 
// http://www.ogre3d.org/tikiwiki/tiki-index.php?page=Creating+a+simple+first-person+camera+system&structure=Tutorials
class CCameraFree : public Camera
{
public:

	CCameraFree(const std::string & a_camname, f32 a_linearvelocity = 100.0f);
	void Update(f64 dt);
	void SetLinearVelocity(f32 velocity);
	f32 GetLinearVelocity() const;

	void OnActivate();
	void OnDeactivate();
	virtual	~CCameraFree();

private:
	Ogre::SceneNode *m_CamYawNode;			//Angle Y, dreta esquerra
	Ogre::SceneNode *m_CamPitchNode;		//Angle X, amunt avall
	Ogre::SceneNode *m_CamRollNode;			//Angle Z, Roll

	f32	m_linear_vel;


};

#endif
