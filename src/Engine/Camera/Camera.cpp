#include "Camera/Camera.h"
#include "Core/CCoreEngine.h"
#include "Renderer/CRenderManager.h"
#include "Game/GameEntity.h"
#include "Input/CInputManager.h"


Camera::Camera(const std::string & cam_name) :
	m_name{ cam_name },
	m_internalcamname{ "CAM_" + m_name },
	m_cam{ CCoreEngine::Instance().GetRenderManager().GetSceneManager()->createCamera(m_internalcamname) },
	m_CamNode{ CCoreEngine::Instance().GetRenderManager().GetSceneManager()->getRootSceneNode()->createChildSceneNode() },
	m_inputEnabled{ true }
{
	//SETUP CAMERA
	m_cam->setNearClipDistance(0.1f); 
	m_cam->setFarClipDistance(10000);

	// Alter the camera aspect ratio to match the viewport
	m_cam->setAspectRatio(Ogre::Real(CCoreEngine::Instance().GetRenderManager().GetOgreViewport()->getActualWidth()) / Ogre::Real(CCoreEngine::Instance().GetRenderManager().GetOgreViewport()->getActualHeight()));
}

void Camera::Update(f64 dt)
{


}

void Camera::Render()
{
	
}

Camera::~Camera()
{
	auto l_scene_mgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();
	l_scene_mgr->destroyCamera(m_cam);
	l_scene_mgr->destroySceneNode(m_CamNode);


}

Ogre::Vector2 Camera::TransformWorldCoordToScreenCoord(const Ogre::Vector3 &_coord)
{
	Ogre::Vector3 point = m_cam->getProjectionMatrix() * (m_cam->getViewMatrix() * _coord);
	
	// coords de [0 , 1]
	Ogre::Vector2 screenpoint = Ogre::Vector2::ZERO;
	screenpoint.x = (point.x / 2) + 0.5f;
	screenpoint.y = (point.y / 2) + 0.5f;


	// paso a coordenades de pantalla reals
	auto width = CCoreEngine::Instance().GetRenderManager().GetOgreViewport()->getActualWidth();
	auto height = CCoreEngine::Instance().GetRenderManager().GetOgreViewport()->getActualHeight();
	screenpoint.x *= static_cast<Ogre::Real>(width);
	screenpoint.y *= static_cast<Ogre::Real>(height);

	return screenpoint;
	
}

Ogre::Vector3 Camera::TransformScreenCoordToWorldCoord(const Ogre::Vector2 &_coord)
{
	Ogre::Vector2 screenpoint(_coord);
	auto width = CCoreEngine::Instance().GetRenderManager().GetOgreViewport()->getActualWidth();
	auto height = CCoreEngine::Instance().GetRenderManager().GetOgreViewport()->getActualHeight();
	screenpoint.x /= static_cast<Ogre::Real>(width);
	screenpoint.y /= static_cast<Ogre::Real>(height);
	
	Ogre::Vector3 worldpoint(Ogre::Vector3::ZERO);
	worldpoint.x = (screenpoint.x - 0.5f) * 2;
	worldpoint.y = (screenpoint.y - 0.5f) * 2;

	worldpoint = ((m_cam->getProjectionMatrix() * m_cam->getViewMatrix()).inverse())*worldpoint;
	worldpoint.z = m_cam->getPosition().z;
	
	return worldpoint;

	//Ogre::Viewport* vp = CCoreEngine::Instance().GetRenderManager().GetOgreViewport();
	//f32 x = 2.0f * _coord.x / vp->getActualWidth() - 1;
	//f32 y = -2.0f * _coord.y / vp->getActualHeight() + 1;
	//Ogre::Matrix4 viewProjectionInverse = (m_cam->getProjectionMatrix() * m_cam->getViewMatrix()).inverse();

	//Ogre::Vector3 point3D{ x, y, 0.f };
	//return viewProjectionInverse * point3D;
}

void Camera::setInputEnabled(bool a_enabled)
{
	m_inputEnabled = a_enabled;
}

bool Camera::isInputEnabled() const
{
	return m_inputEnabled;
}

bool Camera::isActionKeyPushed(const std::string& a_action) const
{
	bool returnValue{ false };
	if(m_inputEnabled)
	{
		auto &keyb = CCoreEngine::Instance().GetInputManager().GetKeyboard();
		returnValue = keyb.IsActionKeyPushed(a_action);
	}

	return returnValue;
}

Ogre::Vector2 Camera::getMouseRelPosition() const
{
	auto &mouse = CCoreEngine::Instance().GetInputManager().GetMouse();
	Ogre::Vector2 returnValue{0.f, 0.f};
	
	if (m_inputEnabled)
	{
		returnValue.x = static_cast<Ogre::Real>(mouse.GetPosRelX());
		returnValue.y = static_cast<Ogre::Real>(mouse.GetPosRelY());
	}

	return returnValue;
}