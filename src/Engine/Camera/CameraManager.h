#ifndef CCAMERAMANAGER_H_
#define CCAMERAMANAGER_H_

#include <unordered_map>
#include <memory>
#include "Core/CBasicTypes.h"
#include "Renderer/CRenderManager.h"

class Camera;
class Behaviour;

class CameraManager
{
public:
	void	StartUp();
	void	ShutDown();
	void	Render();
	void 	Update(f64 dt);

	void	add_camera(Camera* camera);
	void	set_active_camera(const std::string & a_idcam);
	Camera *get_current_camera(){ return m_curcamera->second.get(); };

	void	destroy_all_cameras();

	~CameraManager();

private:
	typedef std::unique_ptr<Camera>						CameraPtr;
	typedef std::unordered_map<std::string, CameraPtr>	t_cameras;

	CameraManager(); friend class CCoreEngine;

	t_cameras 	m_cameras;
	t_cameras::iterator m_curcamera;
};




#endif 
