#include "Camera/Camera.h"
#include "Camera/CameraManager.h"
#include "Core/CCoreEngine.h"
#include "Renderer/CRenderManager.h"
#include "Game/Behavior.h"

CameraManager::CameraManager() :
	m_cameras{ },
	m_curcamera{ m_cameras.end() }
{

}

CameraManager::~CameraManager()
{
	
}

void CameraManager::StartUp()
{

}

void CameraManager::ShutDown() 
{
	destroy_all_cameras();
}

void CameraManager::Render() 
{

}

void CameraManager::Update(f64 dt)
{
	if (m_curcamera != m_cameras.end())
		m_curcamera->second->Update(dt);
}

void CameraManager::add_camera(Camera* camera)
{
	if (camera)
	{
		const std::string& cam_name = camera->getName();
		auto it = m_cameras.find(cam_name);

		if (it == m_cameras.end())
		{
			m_cameras.emplace_hint(it, t_cameras::value_type{ cam_name, std::move(CameraPtr{ camera }) });
		}
		else
		{
			throw std::runtime_error{ "duplicated camera id: " + cam_name };
		}
	}
}

void CameraManager::set_active_camera(const std::string & a_idcam)
{
	if (m_cameras.size())
	{
		auto found = m_cameras.find(a_idcam);
		if (found == m_cameras.end())
		{
			//TODO Camera no trobada

		}
		else 
		{
			//Trobada la camera
			if (m_curcamera != m_cameras.end())
			{
				m_curcamera->second->OnDeactivate();
			}

			m_curcamera = found;
			Ogre::Camera* const camera = m_curcamera->second->GetCamera();
			Ogre::Viewport* vp = CCoreEngine::Instance().GetRenderManager().GetOgreViewport();
			vp->setCamera(camera);
			m_curcamera->second->OnActivate();

		}
	}
	else
	{
		//TODO No hi ha cameres!
	}
	

}

void CameraManager::destroy_all_cameras()
{
	m_cameras.clear();
	m_curcamera = m_cameras.end();
}
