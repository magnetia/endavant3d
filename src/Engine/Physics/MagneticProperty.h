#ifndef EDV_PHY_MAGNET_PROPERTY_H_
#define EDV_PHY_MAGNET_PROPERTY_H_

#include <string>
#include "btBulletDynamicsCommon.h"
#include "Core/CBasicTypes.h"

class MagneticProperty
{
public:
	enum eMagneticCharge
	{
		MAGNET_TYPE_POSITIVE,
		MAGNET_TYPE_NEGATIVE,
		MAGNET_TYPE_NEUTRAL,
		MAGNET_TYPE_TOTAL
	};

	struct Config
	{
		eMagneticCharge charge;
		f32				force;
		f32				scope;
		f32				factor;

		Config(eMagneticCharge a_charge = MAGNET_TYPE_NEUTRAL, f32 a_force = 0.f, f32 a_scope = 0.f, f32 a_factor = 1.f) :
			charge{ a_charge }, force{ a_force }, scope{ a_scope }, factor{ a_factor }
		{}
	};

	static eMagneticCharge stringToCharge(const std::string& a_charge);

	MagneticProperty(const Config& a_config);
	MagneticProperty(eMagneticCharge a_charge = MAGNET_TYPE_NEUTRAL, f32 a_force = 0.f, f32 a_scope = 0.f);
	virtual ~MagneticProperty();

	void setCharge(eMagneticCharge a_magneticCharge);
	eMagneticCharge getCharge() const;
	void setForce(f32 a_force);
	f32 getForce() const;
	void setScope(f32 a_scope);
	f32 getScope() const;
	void setFactor(f32 a_factor);
	f32 getFactor() const;

	bool isActive() const;
	void switchPolarity();
	bool hasAttraction(eMagneticCharge a_charge) const;
	bool hasRepulsion(eMagneticCharge a_charge) const;

private:
	eMagneticCharge _charge;
	f32				_force;
	f32				_scope;
	f32				_factor;
};

#endif
