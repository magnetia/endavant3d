#ifndef EDV_CPHYSICS_MANAGER_H_
#define EDV_CPHYSICS_MANAGER_H_

#include <memory>
#include "btBulletDynamicsCommon.h"
#include "Core/CBasicTypes.h"
#include <functional>
#include <map>
#include <unordered_map>
#include <vector>
#include "Utils/IdGenerator.h"
#include "Utils/edvVector3.h"

// #todo: multiple worlds

class CBtDebugDrawer;
class OgreDebugDrawer;
class btGhostPairCallback;


static void EdvBulletTickCallback(btDynamicsWorld *a_world, btScalar a_timeStep);

class CPhysicsManager
{
public:
	static const u64 UNASSIGNED_COLLISION_ID = 0;

	enum	EdvDebugDrawModes
	{
		EDV_DBG_NoDebug					= btIDebugDraw::DBG_NoDebug,
		EDV_DBG_DrawWireframe			= btIDebugDraw::DBG_DrawWireframe,
		EDV_DBG_DrawAabb				= btIDebugDraw::DBG_DrawAabb,
		EDV_DBG_DrawFeaturesText		= btIDebugDraw::DBG_DrawFeaturesText,
		EDV_DBG_DrawContactPoints		= btIDebugDraw::DBG_DrawContactPoints,
		EDV_DBG_NoDeactivation			= btIDebugDraw::DBG_NoDeactivation,
		EDV_DBG_NoHelpText				= btIDebugDraw::DBG_NoHelpText,
		EDV_DBG_DrawText				= btIDebugDraw::DBG_DrawText,
		EDV_DBG_ProfileTimings			= btIDebugDraw::DBG_ProfileTimings,
		EDV_DBG_EnableSatComparison		= btIDebugDraw::DBG_EnableSatComparison,
		EDV_DBG_DisableBulletLCP		= btIDebugDraw::DBG_DisableBulletLCP,
		EDV_DBG_EnableCCD				= btIDebugDraw::DBG_EnableCCD,
		EDV_DBG_DrawConstraints			= btIDebugDraw::DBG_DrawConstraints,
		EDV_DBG_DrawConstraintLimits	= btIDebugDraw::DBG_DrawConstraintLimits,
		EDV_DBG_FastWireframe			= btIDebugDraw::DBG_FastWireframe,
		EDV_DBG_DrawNormals				= btIDebugDraw::DBG_DrawNormals,
		EDV_DBG_BT_MAX_DEBUG_DRAW_MODE	= btIDebugDraw::DBG_MAX_DEBUG_DRAW_MODE,
		EDV_DBG_MagneticFields
	};

	CPhysicsManager();

	void StartUp(void);
	void ShutDown(void);
	void Update(f64 dt);
	void updateEdvPhysics(f64 dt);
	void updateBulletPhysics(f64 dt);
	void Reset();
	//void AddBody(EntityBtPhysics* a_body);
	void AddRigidBody(btRigidBody* a_rigid_body);
	void RemoveRigidBody(btRigidBody* a_rigid_body);
	edv::Vector3f GetGravityLocation() const;
	void SetGravityLocation(const edv::Vector3f& a_gravity);
	void* RayTest(const edv::Vector3f& a_orig, const edv::Vector3f& a_dest);
	void* RayTestNotMe(const edv::Vector3f& a_orig, const edv::Vector3f& a_dest, btCollisionWorld::RayResultCallback& a_callback);
	void* RayTestMouseCurrentCamera();
	void SetDebugMode(u32 a_debug_mode);
	void SwitchDebugModeEnabled();
	void BulletTickCallback(btScalar a_timeStep);

	typedef std::function<void(btScalar)> tEdvPhysicsTickCallback;
	void RegisterBulletTickCallback(const std::string& a_name, const tEdvPhysicsTickCallback& a_function);
	std::string RegisterBulletTickCallback(const tEdvPhysicsTickCallback& a_function);
	void UnRegisterBulletTickCallback(const std::string& a_name);
	btDiscreteDynamicsWorld* getWorld() const;

	static const btVector3 DEFAULT_GRAVITY_LOC;



	OgreDebugDrawer											*m_debugdrawerogre;
private:
	//typedef std::list<EntityBtPhysics*> Bodies;

	void CheckCollisionNotifications();

	std::unique_ptr<btBroadphaseInterface>					m_broadphaseInterface;
	std::unique_ptr<btGhostPairCallback>					m_internalGhostPairCallback;
	std::unique_ptr<btDefaultCollisionConfiguration>		m_defaultCollisionConfiguration;
	std::unique_ptr<btCollisionDispatcher>					m_collisionDispatcher;
	std::unique_ptr<btSequentialImpulseConstraintSolver>	m_solver;
	std::unique_ptr<btDiscreteDynamicsWorld>				m_dynamicsWorld;
	std::unique_ptr<btIDebugDraw>							m_btDebugDrawer;
	EdvDebugDrawModes										m_last_debug_mode;
	EdvDebugDrawModes										m_current_debug_mode;
	edv::Vector3f											m_gravityLocation;

	typedef std::map<std::string, tEdvPhysicsTickCallback> tBulletTickCallbakcsMap;
	tBulletTickCallbakcsMap									m_tick_callbacks;
	IdGenerator<u64>										m_idGenerator;

	//Bodies m_enabledBodies;
	//Bodies m_disabledBodies;
};

#endif
