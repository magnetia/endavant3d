#include "CPhysicsManager.h"
#include "BulletOgreDebugDrawer.h"
#include "Core/CCoreEngine.h"
#include "Renderer/CRenderManager.h"
#include "Options/OptionManager.h"
#include <BulletCollision/CollisionDispatch/btGhostObject.h>
#include "btResultCallback.hpp"
#include "Input/CInputManager.h"
#include "Input/CInputMouse.h"
#include "Renderer/CRenderManager.h"
#include "Physics/COgreMotionState.h"
#include "Game/GameEntity.h"
#include "Game/Actor.h"

const btVector3 CPhysicsManager::DEFAULT_GRAVITY_LOC(0, -10, 0);

void EdvBulletTickCallback(btDynamicsWorld *a_world, btScalar a_timeStep)
{
	CPhysicsManager *w = static_cast<CPhysicsManager *>(a_world->getWorldUserInfo());
	w->BulletTickCallback(a_timeStep);
}

CPhysicsManager::CPhysicsManager() :
	m_broadphaseInterface(nullptr),
	m_defaultCollisionConfiguration(nullptr),
	m_collisionDispatcher(nullptr),
	m_solver(nullptr),
	m_dynamicsWorld(nullptr),
	m_btDebugDrawer(nullptr),
	m_last_debug_mode(EDV_DBG_NoDebug),
	m_current_debug_mode(EDV_DBG_NoDebug),
	m_gravityLocation(DEFAULT_GRAVITY_LOC)
{

}

void CPhysicsManager::StartUp(void)
{
	m_broadphaseInterface.reset(new btDbvtBroadphase);
	// To register a "ghost pair callback" for the ghost object to be updated every simulation tick
	m_internalGhostPairCallback.reset(new btGhostPairCallback());
	m_broadphaseInterface->getOverlappingPairCache()->setInternalGhostPairCallback(m_internalGhostPairCallback.get());

	m_defaultCollisionConfiguration.reset(new btDefaultCollisionConfiguration);
	m_collisionDispatcher.reset(new btCollisionDispatcher(m_defaultCollisionConfiguration.get()));

	m_solver.reset(new btSequentialImpulseConstraintSolver);

	m_dynamicsWorld.reset(new btDiscreteDynamicsWorld(	m_collisionDispatcher.get(), 
														m_broadphaseInterface.get(), 
														m_solver.get(),
														m_defaultCollisionConfiguration.get()));

	SetGravityLocation(DEFAULT_GRAVITY_LOC);

	// Debug drawing
	auto sm = CCoreEngine::Instance().GetRenderManager().GetSceneManager();
	m_debugdrawerogre = new OgreDebugDrawer(sm);
	m_btDebugDrawer.reset(m_debugdrawerogre);
	m_dynamicsWorld->setDebugDrawer(m_btDebugDrawer.get());
	m_dynamicsWorld->getDebugDrawer()->setDebugMode(btIDebugDraw::DBG_NoDebug);

	// Tick callback
	m_dynamicsWorld->setInternalTickCallback(EdvBulletTickCallback, static_cast<void*>(this));
}

void CPhysicsManager::ShutDown(void)
{

}

void CPhysicsManager::Update(f64 dt)
{
	updateEdvPhysics(dt);
	updateBulletPhysics(dt);
}

void CPhysicsManager::updateEdvPhysics(f64 dt)
{
	// Upate Endavant Physics	
}

void CPhysicsManager::updateBulletPhysics(f64 dt)
{
	//-- Explanation of "stepSimulation" method --//
	// http://bulletphysics.org/mediawiki-1.5.8/index.php/Stepping_The_World
	// Parameters:
	// - timeStep: amount of time to step the simulation by. Typically you're going to be passing it the time since you last called it.
	// - maxSubSteps: maximum number of steps that Bullet is allowed to take each time you call it.
	// - fixedTimeStep: size of that internal step.
	// [ timeStep < maxSubSteps * fixedTimeStep ]
	// We suppose that minimum framerate is 30 fps and maximum is 400 fps. The fixedTimeStep is 1/60 (by default).
	// 400 fps	-> 1/400 < maxSubsteps * 1/60; 6/40 < maxSubsteps
	// 30 fps	-> 1/30 < maxSubsteps * 1/60; 2 < maxSubsteps
	// So, maxSubsteps = 3.
	//--//
	
	const btScalar timeStep = static_cast<f32>(dt);
	const btScalar fixedTimeStep = 1.f/60.f;
	const u32 maxSubSteps = 3;

	m_dynamicsWorld->stepSimulation(timeStep, maxSubSteps, fixedTimeStep);
	m_dynamicsWorld->debugDrawWorld();
}

void CPhysicsManager::Reset()
{
	if (m_dynamicsWorld)
	{
		int numConstraints = m_dynamicsWorld->getNumConstraints();
		for (int i = 0; i<numConstraints; i++)
		{
			m_dynamicsWorld->getConstraint(0)->setEnabled(true);
		}
		int numObjects = m_dynamicsWorld->getNumCollisionObjects();

		btCollisionObjectArray copyArray = m_dynamicsWorld->getCollisionObjectArray();

		for (int i = 0; i<numObjects; i++)
		{
			btCollisionObject* colObj = copyArray[i];
			btRigidBody* body = btRigidBody::upcast(colObj);
			if (body)
			{
				if (body->getMotionState())
				{
					btDefaultMotionState* myMotionState = (btDefaultMotionState*)body->getMotionState();
					myMotionState->m_graphicsWorldTrans = myMotionState->m_startWorldTrans;
					body->setCenterOfMassTransform(myMotionState->m_graphicsWorldTrans);
					colObj->setInterpolationWorldTransform(myMotionState->m_startWorldTrans);
					colObj->forceActivationState(ACTIVE_TAG);
					colObj->activate();
					colObj->setDeactivationTime(0);
				}
				//removed cached contact points (this is not necessary if all objects have been removed from the dynamics world)
				if (m_dynamicsWorld->getBroadphase()->getOverlappingPairCache())
					m_dynamicsWorld->getBroadphase()->getOverlappingPairCache()->cleanProxyFromPairs(colObj->getBroadphaseHandle(), m_dynamicsWorld->getDispatcher());

				btRigidBody* body = btRigidBody::upcast(colObj);
				if (body && !body->isStaticObject())
				{
					btRigidBody::upcast(colObj)->setLinearVelocity(btVector3(0, 0, 0));
					btRigidBody::upcast(colObj)->setAngularVelocity(btVector3(0, 0, 0));
				}
			}

		}

		///reset some internal cached data in the broadphase
		m_dynamicsWorld->getBroadphase()->resetPool(m_dynamicsWorld->getDispatcher());
		m_dynamicsWorld->getConstraintSolver()->reset();

		// reset gravity location
		SetGravityLocation(DEFAULT_GRAVITY_LOC);
	}
}

void CPhysicsManager::AddRigidBody(btRigidBody* a_rigid_body)
{
	if (m_dynamicsWorld)
	{
		m_dynamicsWorld->addRigidBody(a_rigid_body);
	}
}

void CPhysicsManager::RemoveRigidBody(btRigidBody* a_rigid_body)
{
	if (m_dynamicsWorld)
	{
		m_dynamicsWorld->removeRigidBody(a_rigid_body);
	}
}

edv::Vector3f CPhysicsManager::GetGravityLocation() const
{
	return m_gravityLocation;
}

void CPhysicsManager::SetGravityLocation(const edv::Vector3f& a_gravity)
{
	if (m_dynamicsWorld)
	{
		m_dynamicsWorld->setGravity(a_gravity.to_btVector3());
		m_gravityLocation = a_gravity;
	}
}

void* CPhysicsManager::RayTestMouseCurrentCamera()
{
	auto& mouse = CCoreEngine::Instance().GetInputManager().GetMouse();

	// get entity mouse is pointing to
	auto screen_height = CCoreEngine::Instance().GetRenderManager().GetOgreViewport()->getActualHeight();
	auto screen_width = CCoreEngine::Instance().GetRenderManager().GetOgreViewport()->getActualWidth();
	Ogre::Camera *cam = CCoreEngine::Instance().GetRenderManager().GetOgreViewport()->getCamera();

	auto dir = cam->getDirection();
	Ogre::Real x = (f32)mouse.GetPosX() / screen_width;
	Ogre::Real y = (f32)mouse.GetPosY() / screen_height;

	Ogre::Ray ray = cam->getCameraToViewportRay(x, 1.0f - y);

	btVector3 origin = COgreMotionState::ogreVector3toBtVector3(ray.getOrigin());
	btVector3 dest = COgreMotionState::ogreVector3toBtVector3(ray.getPoint(1000.0));

	return RayTest(origin, dest);
}

void* CPhysicsManager::RayTest(const edv::Vector3f& a_orig, const edv::Vector3f& a_dest)
{
	void* hit = nullptr;

	if (m_dynamicsWorld)
	{
		btVector3 orig{ a_orig.to_btVector3() }, dest{ a_dest.to_btVector3() };
		btCollisionWorld::ClosestRayResultCallback callback{ orig, dest };
		m_dynamicsWorld->rayTest(orig, dest, callback);

		if (callback.hasHit())
		{
			hit = callback.m_collisionObject->getUserPointer();
		}
	}

	return hit;
}

void* CPhysicsManager::RayTestNotMe(const edv::Vector3f& a_orig, const edv::Vector3f& a_dest, btCollisionWorld::RayResultCallback& a_callback)
{
	void* hit = nullptr;

	if (m_dynamicsWorld)
	{
		btVector3 orig{ a_orig.to_btVector3() }, dest{ a_dest.to_btVector3() };
		m_dynamicsWorld->rayTest(orig, dest, a_callback);

		if (a_callback.hasHit())
		{
			hit = a_callback.m_collisionObject->getUserPointer();
		}
	}

	return hit;
}

void CPhysicsManager::SetDebugMode(u32 a_debug_mode)
{
	if (m_dynamicsWorld)
	{
		u32 newBtDebugMode = (a_debug_mode < EDV_DBG_BT_MAX_DEBUG_DRAW_MODE) ? a_debug_mode : EDV_DBG_NoDebug;
		m_dynamicsWorld->getDebugDrawer()->setDebugMode(newBtDebugMode);

		m_last_debug_mode = m_current_debug_mode;
		m_current_debug_mode = static_cast<EdvDebugDrawModes>(a_debug_mode);
	}
}

void CPhysicsManager::SwitchDebugModeEnabled()
{
	if (m_current_debug_mode != EDV_DBG_NoDebug)
	{
		SetDebugMode(EDV_DBG_NoDebug);
	}
	else
	{
		SetDebugMode(m_last_debug_mode);
	}
}

void CPhysicsManager::BulletTickCallback(btScalar a_timeStep)
{
	for (auto it = m_tick_callbacks.begin(); it != m_tick_callbacks.end(); it++)
	{
		it->second(a_timeStep);
	}

	CheckCollisionNotifications();
}

void CPhysicsManager::RegisterBulletTickCallback(const std::string& a_name, const tEdvPhysicsTickCallback& a_function)
{
	m_tick_callbacks[a_name] = a_function;
}

std::string CPhysicsManager::RegisterBulletTickCallback(const tEdvPhysicsTickCallback& a_function)
{
	std::string id = std::to_string(m_idGenerator.nextId());

	m_tick_callbacks[id] = a_function;

	return id;
}

void CPhysicsManager::UnRegisterBulletTickCallback(const std::string& a_name)
{
	m_tick_callbacks.erase(a_name);
}

void CPhysicsManager::CheckCollisionNotifications()
{
	s32 numManifolds = m_dynamicsWorld->getDispatcher()->getNumManifolds();

	for (s32 i = 0; i<numManifolds; i++)
	{
		btPersistentManifold* contactManifold = m_dynamicsWorld->getDispatcher()->getManifoldByIndexInternal(i);
		void* obA = contactManifold->getBody0()->getUserPointer();
		void* obB = contactManifold->getBody1()->getUserPointer();

		if (obA == obB)
			continue;

		int numContacts = contactManifold->getNumContacts();

		for (int j = 0; j<numContacts; j++)
		{
			btManifoldPoint& pt = contactManifold->getContactPoint(j);
			btScalar distance = pt.getDistance();

			if (distance < 0.f && abs(distance) > 0.01f)
			{
				if (GameEntity* const entityA = static_cast<GameEntity*>(obA))
				{
					if (Actor* const actorA = entityA->getActor())
					{
						actorA->addCollision({ 
							static_cast<GameEntity*>(obB), 
							(pt.getPositionWorldOnA() + pt.getPositionWorldOnB()) / 2.f,
							pt.m_normalWorldOnB 
						});
					}
				}

				if (GameEntity* const entityB = static_cast<GameEntity*>(obB))
				{
					if (Actor* const actorB = entityB->getActor())
					{
						actorB->addCollision({
							static_cast<GameEntity*>(obA),
							(pt.getPositionWorldOnA() + pt.getPositionWorldOnB()) / 2.f,
							pt.m_normalWorldOnB
						});
					}
				}
			}
		}
	}
}

btDiscreteDynamicsWorld* CPhysicsManager::getWorld() const
{
	return m_dynamicsWorld.get();
}
