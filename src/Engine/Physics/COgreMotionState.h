#ifndef EDV_COGRE_MOTION_STATE
#define EDV_COGRE_MOTION_STATE

#include <vector>
#include "btBulletDynamicsCommon.h"
#include "OGRE/Ogre.h"
#include "Core/CBasicTypes.h"

class COgreMotionState : public btDefaultMotionState
{
public:
	static btVector3 ogreVector3toBtVector3(const Ogre::Vector3& a_ogreVect);
	static Ogre::Vector3 btVector3toOgreVector3(const btVector3& a_btVect);
	static btQuaternion ogreQuaternionToBtQuaternion(const Ogre::Quaternion& a_ogreQuaternion);
	static Ogre::Quaternion btQuaternionToOgreQuaternion(const btQuaternion& a_btQuaternion);

	COgreMotionState(Ogre::SceneNode* a_node, const btVector3& a_translation = { 0.f, 0.f, 0.f }, 
		const btQuaternion& a_rotation = { 0.f, 0.f, 0.f, 1.f }, const btTransform& a_startTrans = btTransform::getIdentity());

	void attachNode(Ogre::SceneNode* a_node);
	void removeNode();
	Ogre::SceneNode* getNode() const;
	void getWorldTransform(btTransform &a_worldTrans) const override;
	void setWorldTransform(const btTransform &a_worldTrans) override;

	const btVector3& getTranslation() const;

protected:
	Ogre::SceneNode*	m_node;
	btVector3			m_translation;
	btQuaternion		m_rotation;
};

#endif
