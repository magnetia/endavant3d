#include "MagneticProperty.h"
#include <Renderer/CRenderDebug.h>
#include <OGRE/OgreVector3.h>
#include <OGRE/OgreColourValue.h>

MagneticProperty::eMagneticCharge MagneticProperty::stringToCharge(const std::string& a_charge)
{
	std::string param;
	std::transform(a_charge.begin(), a_charge.end(), std::back_inserter(param), ::tolower);

	return { param == "positive" ? MAGNET_TYPE_POSITIVE : (param == "negative" ? MAGNET_TYPE_NEGATIVE : MAGNET_TYPE_NEUTRAL) };
}

MagneticProperty::MagneticProperty(const MagneticProperty::Config& a_config) :
_charge{ a_config.charge }, _force{ a_config.force }, _scope{ a_config.scope }, _factor{ a_config.factor }
{
}

MagneticProperty::MagneticProperty(eMagneticCharge a_charge, f32 a_force, f32 a_scope) :
_charge{ a_charge }, _force{ a_force }, _scope{ a_scope }
{
}

MagneticProperty::~MagneticProperty()
{
}

void MagneticProperty::setCharge(eMagneticCharge a_magneticCharge)
{
	_charge = a_magneticCharge;
}

MagneticProperty::eMagneticCharge MagneticProperty::getCharge() const
{
	return _charge;
}

void MagneticProperty::setForce(f32 a_force)
{
	_force = a_force;
}

f32 MagneticProperty::getForce() const
{
	return _force;
}

void MagneticProperty::setScope(f32 a_scope)
{
	_scope = a_scope;
}

f32 MagneticProperty::getScope() const
{
	return _scope;
}

void MagneticProperty::setFactor(f32 a_factor)
{
	_factor = a_factor;
}

f32 MagneticProperty::getFactor() const
{
	return _factor;
}

bool MagneticProperty::isActive() const
{
	return (_charge != MAGNET_TYPE_NEUTRAL);
}

void MagneticProperty::switchPolarity()
{
	if (isActive())
	{
		_charge = (_charge == MAGNET_TYPE_POSITIVE) ? MAGNET_TYPE_NEGATIVE : MAGNET_TYPE_POSITIVE;
	}
}

bool MagneticProperty::hasAttraction(eMagneticCharge a_charge) const
{
	bool return_value = false;
	if (isActive())
	{
		return_value = _charge != a_charge;
	}
	return return_value;
}

bool MagneticProperty::hasRepulsion(eMagneticCharge a_charge) const
{
	bool return_value = false;
	if (isActive())
	{
		return_value = _charge == a_charge;
	}
	return return_value;
}