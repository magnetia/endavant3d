#include "COgreKinematicMotionState.h"

COgreKinematicMotionState::COgreKinematicMotionState(Ogre::SceneNode* a_node, const btVector3& a_translation, 
	const btQuaternion& a_rotation, const btTransform& a_startTrans) :
	COgreMotionState(a_node, a_translation, a_rotation, a_startTrans)
{
}

void COgreKinematicMotionState::getWorldTransform(btTransform &a_worldTrans) const
{
	// synchronizes graphic world to physics
	if (m_node)
	{
		const btVector3 origin = COgreMotionState::ogreVector3toBtVector3(m_node->_getDerivedPosition()) + m_translation;
		const btQuaternion rotation = COgreMotionState::ogreQuaternionToBtQuaternion(m_node->_getDerivedOrientation()) + m_rotation;
		a_worldTrans.setOrigin(origin);
		a_worldTrans.setRotation(rotation);
	}
}

void COgreKinematicMotionState::setWorldTransform(const btTransform &a_worldTrans)
{
	// sets graphics position
	if (m_node)
	{
		btTransform transform = a_worldTrans;
		const btVector3 origin = transform.getOrigin();
		const btQuaternion rotation = transform.getRotation();
		m_node->setPosition(COgreMotionState::btVector3toOgreVector3(origin));
		m_node->setOrientation(COgreMotionState::btQuaternionToOgreQuaternion(rotation));
	}
}
