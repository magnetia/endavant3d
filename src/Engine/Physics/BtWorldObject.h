#ifndef EDV_PHY_BT_WORLD_OBJECT_H_
#define EDV_PHY_BT_WORLD_OBJECT_H_

#include <string>
#include <vector>
#include "btBulletDynamicsCommon.h"

class BtWorldObject
{
public:
	virtual ~BtWorldObject() { }

	virtual btVector3 getPosition() const = 0;
	virtual btQuaternion getOrientation() const = 0;
	virtual btVector3 getWorldPosition() const = 0;
	virtual btQuaternion getWorldOrientation() const = 0;
	virtual std::vector<btVector3> getVertices() const = 0;
	virtual btVector3 getHalfSize() const = 0;
	virtual btVector3 btGetScale() const = 0;
	virtual btStridingMeshInterface* createStridingMeshInterface() = 0;
	virtual btMotionState* createMotionState(const btVector3& a_translation, const btQuaternion& a_rotation, bool a_kinemtic = false) = 0;
	virtual BtWorldObject* createWorldObject(void* a_param) = 0;
	virtual std::string getObjectId() const = 0;
};

#endif
