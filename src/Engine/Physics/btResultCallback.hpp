#ifndef EDV_PHY_BT_RESULT_CALLBACK_H_
#define EDV_PHY_BT_RESULT_CALLBACK_H_

#include "btBulletCollisionCommon.h"
#include "BulletCollision/CollisionDispatch/btCollisionWorld.h"
#include <vector>

class btClosestNotMeConvexResultCallback : public btCollisionWorld::ClosestConvexResultCallback
{
public:
	btClosestNotMeConvexResultCallback(void* a_me, const btVector3& a_up, btScalar a_minSlopeDot)
		: btCollisionWorld::ClosestConvexResultCallback(btVector3(0.0, 0.0, 0.0), btVector3(0.0, 0.0, 0.0)),
		m_me{ a_me },
		m_up{ a_up },
		m_minSlopeDot{ a_minSlopeDot }
	{
	}

	virtual	btScalar addSingleResult(btCollisionWorld::LocalConvexResult& convexResult, bool normalInWorldSpace) override
	{
		if (convexResult.m_hitCollisionObject->getUserPointer() == m_me)
			return btScalar(1.0);

		if (!convexResult.m_hitCollisionObject->hasContactResponse())
			return btScalar(1.0);

		btVector3 hitNormalWorld;
		if (normalInWorldSpace)
		{
			hitNormalWorld = convexResult.m_hitNormalLocal;
		}
		else
		{
			///need to transform normal into worldspace
			hitNormalWorld = convexResult.m_hitCollisionObject->getWorldTransform().getBasis()*convexResult.m_hitNormalLocal;
		}

		btScalar dotUp = m_up.dot(hitNormalWorld);
		if (dotUp < m_minSlopeDot) {
			return btScalar(1.0);
		}

		return ClosestConvexResultCallback::addSingleResult(convexResult, normalInWorldSpace);
	}

protected:
	void*	m_me;
	const btVector3		m_up;
	btScalar			m_minSlopeDot;
};

class btMultipleClosestNotMeConvexResultCallback : public btClosestNotMeConvexResultCallback
{
public:
	std::vector<btCollisionWorld::LocalConvexResult> results;
	
	btMultipleClosestNotMeConvexResultCallback(void* a_me, const btVector3& a_up, btScalar a_minSlopeDot)
		: btClosestNotMeConvexResultCallback(a_me, a_up, a_minSlopeDot)
	{
	}

	virtual	btScalar addSingleResult(btCollisionWorld::LocalConvexResult& convexResult, bool normalInWorldSpace) override
	{
		// only add result if it is valid
		if (btClosestNotMeConvexResultCallback::addSingleResult(convexResult, normalInWorldSpace) != 1.0)
		{
			if (!normalInWorldSpace)
			{
				///need to transform normal into worldspace
				convexResult.m_hitNormalLocal = m_hitCollisionObject->getWorldTransform().getBasis()*convexResult.m_hitNormalLocal;
			}
			results.push_back(convexResult);
		}
		// Erwin Coumans: "The user needs to return 0 for first hit, hitFraction for closest hit, and 1 for all hits in the AddSingleResult callback."
		return 1.0;
	}
};


class btClosestNotMeRayResultCallback : public btCollisionWorld::ClosestRayResultCallback
{
public:
	btClosestNotMeRayResultCallback(void* a_me, const btVector3& a_orig, const btVector3& a_dest, int a_collision_flags = 0) :
		btCollisionWorld::ClosestRayResultCallback(a_orig, a_dest),
		m_me(a_me),
		m_collision_flags(a_collision_flags)
	{
	}

	virtual btScalar addSingleResult(btCollisionWorld::LocalRayResult& rayResult, bool normalInWorldSpace)
	{
		if ((rayResult.m_collisionObject)->getUserPointer() == m_me)
			return 1.0;

		if ((rayResult.m_collisionObject->getCollisionFlags() & m_collision_flags) != 0)
			return 1.0;

		return ClosestRayResultCallback::addSingleResult(rayResult, normalInWorldSpace);
	}
protected:
	void* m_me;
	int m_collision_flags;
};

#endif