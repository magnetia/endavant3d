#include "COgreMotionState.h"

btVector3 COgreMotionState::ogreVector3toBtVector3(const Ogre::Vector3& a_ogreVect)
{
	btVector3 btVect;

	btVect.setX(a_ogreVect.x);
	btVect.setY(a_ogreVect.y);
	btVect.setZ(a_ogreVect.z);

	return btVect;
}

Ogre::Vector3 COgreMotionState::btVector3toOgreVector3(const btVector3& a_btVect)
{
	Ogre::Vector3 ogreVect;

	ogreVect.x = a_btVect.x();
	ogreVect.y = a_btVect.y();
	ogreVect.z = a_btVect.z();

	return ogreVect;
}

btQuaternion COgreMotionState::ogreQuaternionToBtQuaternion(const Ogre::Quaternion& a_ogreQuaternion)
{
	btQuaternion btQ;

	btQ.setW(a_ogreQuaternion.w);
	btQ.setX(a_ogreQuaternion.x);
	btQ.setY(a_ogreQuaternion.y);
	btQ.setZ(a_ogreQuaternion.z);

	return btQ;
}

Ogre::Quaternion COgreMotionState::btQuaternionToOgreQuaternion(const btQuaternion& a_btQuaternion)
{
	Ogre::Quaternion ogreQ;

	ogreQ.w = a_btQuaternion.w();
	ogreQ.x = a_btQuaternion.x();
	ogreQ.y = a_btQuaternion.y();
	ogreQ.z = a_btQuaternion.z();

	return ogreQ;
}

COgreMotionState::COgreMotionState(Ogre::SceneNode* a_node, const btVector3& a_translation, const btQuaternion& a_rotation, 
	const btTransform& a_startTrans) :
		btDefaultMotionState(a_startTrans),
		m_node{ nullptr },
		m_translation{ a_translation },
		m_rotation{ a_rotation }
{
	attachNode(a_node);
}

void COgreMotionState::attachNode(Ogre::SceneNode* a_node)
{
	m_node = a_node;

	if (m_node)
	{
		m_graphicsWorldTrans.setIdentity();
		m_graphicsWorldTrans.setOrigin(ogreVector3toBtVector3(m_node->_getDerivedPosition()) + m_translation);
		m_graphicsWorldTrans.setRotation(ogreQuaternionToBtQuaternion(m_node->getOrientation()) * m_rotation);
		m_startWorldTrans = m_graphicsWorldTrans;
	}
}

void COgreMotionState::removeNode()
{
	m_graphicsWorldTrans.setIdentity();
	m_startWorldTrans.setIdentity();
	m_node = nullptr;
}

Ogre::SceneNode* COgreMotionState::getNode() const
{
	return m_node;
}

void COgreMotionState::getWorldTransform(btTransform &a_worldTrans) const
{
	btDefaultMotionState::getWorldTransform(a_worldTrans);
	const btVector3 origin = a_worldTrans.getOrigin();
	const btQuaternion rotation = a_worldTrans.getRotation();
	a_worldTrans.setOrigin(origin + m_translation);
	a_worldTrans.setRotation(rotation * m_rotation);
}

void COgreMotionState::setWorldTransform(const btTransform &a_worldTrans)
{
	if (m_node)
	{
		btTransform transform = a_worldTrans;
		const btVector3 origin = transform.getOrigin();
		const btQuaternion rotation = transform.getRotation();
		transform.setOrigin(origin - m_translation);
		transform.setRotation(rotation * m_rotation.inverse());

		btDefaultMotionState::setWorldTransform(transform);
		btQuaternion rot = m_graphicsWorldTrans.getRotation();
		m_node->setOrientation(rot.w(), rot.x(), rot.y(), rot.z());
		btVector3 pos = m_graphicsWorldTrans.getOrigin();
		m_node->setPosition(pos.x(), pos.y(), pos.z());
	}
}

const btVector3& COgreMotionState::getTranslation() const
{
	return m_translation;
}
