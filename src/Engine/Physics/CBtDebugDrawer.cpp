#include "CBtDebugDrawer.h"
#include <Renderer/CRenderDebug.h>
#include <OGRE\OgreVector3.h>
#include <OGRE\OgreColourValue.h>
#include "LinearMath\btVector3.h"

CBtDebugDrawer::CBtDebugDrawer() :
	mDebugDrawMode(DBG_NoDebug)
{
}

CBtDebugDrawer::~CBtDebugDrawer()
{
}

void CBtDebugDrawer::drawLine(const btVector3 &from, const btVector3 &to, const btVector3 &color)
{
	Ogre::Vector3 start(from.x(), from.y(), from.z()), end(to.x(), to.y(), to.z());
	Ogre::ColourValue colour(color.x(), color.y(), color.z());

	CRenderDebug::getSingleton().drawLine(start, end, colour);
}

void CBtDebugDrawer::setDebugMode(int debugMode)
{
	mDebugDrawMode = static_cast<DebugDrawModes>(debugMode);
}

int CBtDebugDrawer::getDebugMode() const
{
	return mDebugDrawMode;
}