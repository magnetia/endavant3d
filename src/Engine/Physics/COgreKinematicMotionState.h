#ifndef EDV_COGRE_KINEMATIC_MOTION_STATE
#define EDV_COGRE_KINEMATIC_MOTION_STATE

#include "COgreMotionState.h"

class COgreKinematicMotionState : public COgreMotionState
{
public:
	COgreKinematicMotionState(Ogre::SceneNode* a_node, const btVector3& a_translation = { 0.f, 0.f, 0.f },
		const btQuaternion& a_rotation = { 0.f, 0.f, 0.f, 1.f }, const btTransform& a_startTrans = btTransform::getIdentity());

	void getWorldTransform(btTransform &a_worldTrans) const override;
	void setWorldTransform(const btTransform &a_worldTrans) override;
};

#endif
