
#ifndef CRENDER_TEXT_MANAGER_H_
#define CRENDER_TEXT_MANAGER_H_
#include <string>
#include "OGRE/Ogre.h"
#include "OGRE/Overlay/OgreOverlaySystem.h"
#include "Core\CBasicTypes.h"



class CRenderTextManager
{

public:
	CRenderTextManager();
	~CRenderTextManager();
	
	static const std::string EDV_DEFAULTFONT_ID;
	static const std::string EDV_DEFAULTFONT_FILENAME;

	void addTextBox(const std::string& ID, const std::string &a_fontID, const std::string& text, f32 x, f32 y, f32 width, f32 height, const Ogre::ColourValue& color=Ogre::ColourValue());

	void removeTextBox(const std::string& ID);
	void createTTFFont(const std::string & a_fontID, const std::string &a_fontfilename, const std::string&  a_fontsize = "32");
	void removeAlltext();
	void setText(const std::string& ID, const std::string& Text);
	const std::string getText(const std::string& ID);

private:
	void CreateTTFFont(const std::string & a_fontname);
	friend class CRenderManager;
	Ogre::OverlayManager*    m_overlayMgr;
	Ogre::Overlay*           m_overlay;
	Ogre::OverlayContainer*  m_panel;
};

#endif /* CWINDOW_H_ */
