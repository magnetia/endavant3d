#ifndef CRENDERMANAGER_H_
#define CRENDERMANAGER_H_

#include <vector>
#include "Core/CLogManager.h"
#include "Utils/CXMLParserPUGI.h"
#include "Utils/CConversions.h"
#include "OGRE/Ogre.h"
#include "OGRE/OgreMaterialManager.h"
#include "CRenderTextManager.h"
#include "Time/CTimeManager.h"
#include "Utils/IdGenerator.h"
#include "OGRE/OgreFrameListener.h"
#include "SDL2/SDL_syswm.h"
#include "SDL2/SDL.h"
namespace Ogre
{
//	class Root;
//	class RenderWindow;
	class OverlaySystem;
//	class SceneManager;
//	class Vector3;
//	class Mesh;
//	class Quaternion;
}

class CScene;


class CRenderManager: public Ogre::FrameListener
{
	public:
		//Framelistener
		bool frameStarted(const Ogre::FrameEvent& evt) override;
		bool frameEnded(const Ogre::FrameEvent& evt) override;
		bool frameRenderingQueued(const Ogre::FrameEvent& evt) override;

		f32 m_elapsedframetime;

		enum eSceneFormat
		{
			SCENE_FORMAT_DOTSCENE,
		};

		struct Compositor
		{
			typedef u32 Id;
			typedef IdGenerator<Id> IdGen;

			std::string							name;
			Ogre::MaterialManager::Listener*	listener;

			Compositor(const std::string& a_name = "", Ogre::MaterialManager::Listener* a_listener = nullptr) :
				name{ a_name }, listener{ a_listener } { }
		};

		bool	StartUp(void);
		void	ShutDown(void);
		void	Update(f64 dt);
		void	Render(void);
		
		~CRenderManager();
		Ogre::Root* GetOgreRoot(){ return m_OgreRoot; };
		Ogre::SceneManager* GetSceneManager(){ return m_sceneMgr; };
		Ogre::RenderWindow* GetRenderWindow(){ return m_OgreWindow; };
		Ogre::OverlaySystem* GetOgreOverlaySystem(){ return mOverlaySystem; };
		Ogre::Viewport*		GetOgreViewport() { return m_viewport; };
		CRenderTextManager*	GetRenderTextManager() { return m_txtmgr; };
		void addScene(eSceneFormat a_format, const std::string& a_filename);
		void getMeshInformation(Ogre::Mesh* mesh, size_t &vertex_count, std::vector<Ogre::Vector3> &vertices, size_t &index_count, std::vector<unsigned int> &indices, 
			const Ogre::Vector3 &position = Ogre::Vector3::ZERO, const Ogre::Quaternion &orient = Ogre::Quaternion::IDENTITY, const Ogre::Vector3 &scale = Ogre::Vector3::UNIT_SCALE);
		Compositor::Id addCompositor(const std::string& a_name, Ogre::MaterialManager::Listener* a_listener = nullptr);
		void removeCompositor(Compositor::Id a_id);

		void SwitchRenderMode();
		void SwitchRenderInformation();

		Ogre::Vector2 getTextureSize(const std::string& aTextureName) const;

	private:
		typedef std::unordered_map<Compositor::Id, Compositor>	Compositors;

		CRenderManager();	friend class CCoreEngine;

		bool	InitOgre();

		
		//OGRE needs
		Ogre::Root*					m_OgreRoot;		//!< Pointer to the Root singleton
		Ogre::RenderWindow			*m_OgreWindow;	//!< Ogre controlled window
		Ogre::OverlaySystem			*mOverlaySystem;
		Ogre::SceneManager			*m_sceneMgr;
		// set viewport
		Ogre::Viewport				*m_viewport;	//!< Viewport del joc
		CRenderTextManager			*m_txtmgr;

		SDL_Window					*m_SDLWinhndlr;
	
		void				UpdateDebugMode();
		EV_TimerID			m_timerupdatedebug;
		bool				m_debugmode;
		Compositor::IdGen	m_compositorIdGen;
		Compositors			m_compositors;
};

#endif /* RENDERMANAGER_H_ */
