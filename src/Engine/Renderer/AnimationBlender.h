#include "Ogre.h"

class AnimationBlender
{
public:
	enum BlendingTransition
	{
		BlendSwitch,			// stop source and start dest
		BlendWhileAnimating,	// cross fade, blend source animation out while blending destination animation in
		BlendThenAnimate		// blend source to first frame of dest, when done, start dest anim
	};

	AnimationBlender(Ogre::Entity* a_entity);
	~AnimationBlender();

	void init(const Ogre::String& a_animation);
	void blend(const Ogre::String& a_animation, BlendingTransition a_transition, Ogre::Real a_duration);
	void addTime(Ogre::Real a_time);
	Ogre::Real getProgress() { return mTimeleft / mDuration; }
	Ogre::AnimationState *getSource() { return mSource; }
	Ogre::AnimationState *getTarget() { return mTarget; }
	void stop();
	bool isCompleted() const;

private:
	Ogre::Entity*			mEntity;
	Ogre::AnimationState*	mSource;
	Ogre::AnimationState*	mTarget;

	BlendingTransition		mTransition;

	Ogre::Real				mTimeleft;
	Ogre::Real				mDuration;
	bool					mCompleted;
};