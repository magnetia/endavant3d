#include "AnimationBlender.h"

AnimationBlender::AnimationBlender(Ogre::Entity *entity) : 
	mEntity(entity),
	mSource(nullptr),
	mTarget(nullptr),
	mTransition(BlendSwitch),
	mTimeleft(0.f),
	mDuration(0.f),
	mCompleted(false)
{
}

AnimationBlender::~AnimationBlender()
{
	stop();
}

void AnimationBlender::init(const Ogre::String& a_animation)
{
	mSource = mEntity->getAnimationState(a_animation);
	Ogre::AnimationStateSet *set = mEntity->getAllAnimationStates();
	Ogre::AnimationStateIterator it = set->getAnimationStateIterator();
	while (it.hasMoreElements())
	{
		Ogre::AnimationState *anim = it.getNext();
		if (anim != mSource)
		{
			anim->setEnabled(false);
		}
	}
	
	if (mSource)
	{
		mSource->setEnabled(true);
		mSource->setWeight(1);
	}

	mTimeleft = 0;
	mDuration = 1;
	mTarget = nullptr;
	mCompleted = false;
}

void AnimationBlender::blend(const Ogre::String& a_animation, BlendingTransition a_transition, Ogre::Real a_duration)
{
	if (a_transition == AnimationBlender::BlendSwitch)
	{
		if (mSource != nullptr)
		{
			mSource->setEnabled(false);
		}
		mSource = mEntity->getAnimationState(a_animation);
		mSource->setEnabled(true);
		mSource->setWeight(1);
		mSource->setTimePosition(0);
		mTimeleft = 0;
	}
	else
	{
		Ogre::AnimationState *newTarget = mEntity->getAnimationState(a_animation);
		if (mTimeleft > 0)
		{
			// oops, weren't finished yet
			if (newTarget == mTarget)
			{
				// nothing to do! (ignoring duration here)
			}
			else if (newTarget == mSource)
			{
				// going back to the source state, so let's switch
				mSource = mTarget;
				mTarget = newTarget;
				mTimeleft = mDuration - mTimeleft; // i'm ignoring the new duration here
			}
			else
			{
				// ok, newTarget is really new, so either we simply replace the target with this one, or
				// we make the target the new source
				if (mTimeleft < mDuration * 0.5)
				{
					// simply replace the target with this one
					mTarget->setEnabled(false);
					mTarget->setWeight(0);
				}
				else
				{
					// old target becomes new source
					mSource->setEnabled(false);
					mSource->setWeight(0);
					mSource = mTarget;
				}
				mTarget = newTarget;
				
				if (mTarget)
				{
					mTarget->setEnabled(true);
					mTarget->setWeight(1.f - mTimeleft / mDuration);
					mTarget->setTimePosition(0);
				}
			}
		}
		else
		{
			mTransition = a_transition;
			mTimeleft = mDuration = a_duration;
			mTarget = newTarget;
			mTarget->setEnabled(true);
			mTarget->setWeight(0);
			mTarget->setTimePosition(0);
		}
	}
}

void AnimationBlender::addTime(Ogre::Real a_time)
{
	if (mSource && mTarget)
	{
		if (mTimeleft > 0)
		{
			mTimeleft -= a_time;
			if (mTimeleft <= 0)
			{
				// finish blending
				mSource->setEnabled(false);
				mTarget->setEnabled(true);
				stop();
			}
			else
			{
				// still blending, advance weights
				mSource->setWeight(mTimeleft / mDuration);
				mTarget->setWeight(1.f - mTimeleft / mDuration);
				if (mTransition == AnimationBlender::BlendWhileAnimating)
					mTarget->addTime(a_time);
			}
		}
		mSource->addTime(a_time);
	}
}

bool AnimationBlender::isCompleted() const
{
	return mCompleted;
}

void AnimationBlender::stop()
{
	if (mSource)
		mSource->setWeight(1);

	if (mTarget)
		mTarget->setWeight(1);

	mTarget = nullptr;
	mTimeleft = mDuration = 0.f;
	mCompleted = true;
}