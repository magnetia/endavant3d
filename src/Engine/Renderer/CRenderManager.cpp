#include "CRenderManager.h"
#include "Core/CCoreEngine.h"
#include "Time/CTimeManager.h"


// OGRE
#include "OGRE/Ogre.h"
#include "OGRE/Overlay/OgreOverlaySystem.h"
// DotScene loader
#include "Externals/DotScene/DotSceneLoader.h"
#include "CRenderDebug.h"
#include "Physics\CPhysicsManager.h"
#include "Physics\BulletOgreDebugDrawer.h"

bool CRenderManager::frameStarted(const Ogre::FrameEvent& evt)
{
	return true;
}

bool CRenderManager::frameRenderingQueued(const Ogre::FrameEvent& evt)
{
	m_elapsedframetime = evt.timeSinceLastFrame;

	CRenderDebug::getSingletonPtr()->clear();

	CCoreEngine::Instance().GetPhysicsManager().m_debugdrawerogre->frameStarted(evt);
	CCoreEngine::Instance().Update();
	CCoreEngine::Instance().GetPhysicsManager().m_debugdrawerogre->frameEnded(evt);

	CRenderDebug::getSingletonPtr()->build();

	return CCoreEngine::Instance().IsRunning();
}


bool CRenderManager::frameEnded(const Ogre::FrameEvent& evt)
{
	return true;
}


CRenderManager::CRenderManager():
m_debugmode{ false }, m_timerupdatedebug{ CTimeManager::INVALID_TIMERID }
{
	LOG( LOG_INFO, LOGSUB_VIDEO,"Object Created!");
}

CRenderManager::~CRenderManager()
{
	LOG( LOG_INFO, LOGSUB_VIDEO,"Object Destroyed!");
}

bool CRenderManager::StartUp()
{
	LOG( LOG_INFO, LOGSUB_VIDEO,"Starting Up!");
	
	
	return InitOgre();

}


bool CRenderManager::InitOgre()
{

   	m_OgreRoot = new Ogre::Root("", "Config\\ogre.cfg");
	
#ifdef _DEBUG
	m_OgreRoot->loadPlugin("RenderSystem_Direct3D9_d");
	m_OgreRoot->loadPlugin("RenderSystem_GL_d");
	//m_OgreRoot->loadPlugin("Plugin_BSPSceneManager_d");
	//m_OgreRoot->loadPlugin("Plugin_PCZSceneManager_d");
	//m_OgreRoot->loadPlugin("Plugin_OctreeZone_d");
	m_OgreRoot->loadPlugin("Plugin_OctreeSceneManager_d");
	m_OgreRoot->loadPlugin("Plugin_ParticleFX_d");
	//m_OgreRoot->loadPlugin("Plugin_CgProgramManager_d");
#else
	m_OgreRoot->loadPlugin("RenderSystem_Direct3D9");
	m_OgreRoot->loadPlugin("RenderSystem_GL");
	m_OgreRoot->loadPlugin("Plugin_OctreeSceneManager");
	m_OgreRoot->loadPlugin("Plugin_ParticleFX");
	//m_OgreRoot->loadPlugin("Plugin_CgProgramManager");
#endif


    mOverlaySystem = new Ogre::OverlaySystem();
	
	//-------------------------------------------------------------------------------------

	// configure
	// Show the configuration dialog and initialise the system
	// You can skip this and use root.restoreConfig() to load configuration
	// settings if you were sure there are valid ones saved in ogre.cfg
	if (m_OgreRoot->restoreConfig() || m_OgreRoot->showConfigDialog())
	{
		// If returned true, user clicked OK so initialise
		// Here we choose to let the system create a default rendering window by passing 'true'
		m_OgreWindow = m_OgreRoot->initialise(true, "MAGNETIA");
	}
	else
	{
		return false;
	}

	//Agafo input
	//HWND hwnd;
	//m_OgreWindow->getCustomAttribute("HWND", &hwnd);
	HWND Data = 0;
	m_OgreWindow->getCustomAttribute("WINDOW", &Data);
	m_SDLWinhndlr = SDL_CreateWindowFrom((void*)Data);
	SDL_SetWindowGrab(m_SDLWinhndlr, SDL_TRUE);
	
	//No he tingut collons a trobar una manera amb SDL_ShowCursor ni res multiplataforma...
	ShowCursor(false);
	
//	SDL_SysWMinfo sysInfo;
//	SDL_VERSION( &sysInfo.version );
//	if ( SDL_GetWindowWMInfo( m_Window->GetWindowHandler(), &sysInfo ) <= 0 )
//	{
//		// TODO
//		// printf("%s : %d\n", SDL_GetError(), sysInfo.window);
//		assert( false );
//	}
//
//	unsigned long hWnd;
//#ifdef SDL_VIDEO_DRIVER_WINDOWS
//	hWnd = reinterpret_cast<unsigned long>( sysInfo.info.win.window );
//#else
//	hWnd = sysInfo.info.x11.window;
//#endif
//
//	Ogre::String strWindowHandle = Ogre::StringConverter::toString( hWnd );
//
//	Ogre::NameValuePairList misc;
//	misc["externalWindowHandle"] = strWindowHandle;
//
//	m_OgreWindow = m_OgreRoot->createRenderWindow(a_WindowCaption, a_WindowWidth, a_WindowHeight, a_Fullscreen, &misc);
	m_OgreWindow->setVisible(true);
	
	// create the scene and add the overlay system to it
	m_sceneMgr = m_OgreRoot->createSceneManager("OctreeSceneManager");
	m_sceneMgr->addRenderQueueListener(mOverlaySystem);

	//Creo una camera inicial per crear un viewport inicial (no fer res amb aquesta camera)
	Ogre::Camera * l_cam = m_sceneMgr->createCamera("EDV_MAINCAMERA");
	m_viewport = m_OgreWindow->addViewport(l_cam);
	m_viewport->setBackgroundColour(Ogre::ColourValue(0.0, 0.0, 0.0));


	// Alter the camera aspect ratio to match the viewport
	l_cam->setAspectRatio(Ogre::Real(m_viewport->getActualWidth()) / Ogre::Real(m_viewport->getActualHeight()));
	
	//Creem el manager de text render
	m_txtmgr = new CRenderTextManager();

	// Render Debug
	new CRenderDebug(m_sceneMgr, 0.5f);

	return true;
}


const std::string ID_DEBUG_TXT{ "RENDER_DBG_TXT" };
const std::string ID_DEBUG_TXT2{ "RENDER_DBG_TXT2" };
const std::string ID_DEBUG_TXT3{ "RENDER_DBG_TXT3" };
const std::string ID_DEBUG_TXT4{ "RENDER_DBG_TXT4" };
const std::string ID_DEBUG_TXT5{ "RENDER_DBG_TXT5" };
void	CRenderManager::SwitchRenderInformation()
{
	m_debugmode = !m_debugmode;
	if (m_debugmode)
	{
		f32 height(static_cast<f32>(m_viewport->getActualHeight()));
		m_txtmgr->addTextBox(ID_DEBUG_TXT, CRenderTextManager::EDV_DEFAULTFONT_ID, "", 0, height-150, 800, 600, Ogre::ColourValue(0.0, 1.0, 0.0, 1.0));
		m_txtmgr->addTextBox(ID_DEBUG_TXT2, CRenderTextManager::EDV_DEFAULTFONT_ID, "", 0, height-120, 800, 600, Ogre::ColourValue(0.0, 1.0, 0.0, 1.0));
		m_txtmgr->addTextBox(ID_DEBUG_TXT3, CRenderTextManager::EDV_DEFAULTFONT_ID, "", 0, height-90, 800, 600, Ogre::ColourValue(0.0, 1.0, 0.0, 1.0));
		m_txtmgr->addTextBox(ID_DEBUG_TXT4, CRenderTextManager::EDV_DEFAULTFONT_ID, "", 0, height-60, 800, 600, Ogre::ColourValue(0.0, 1.0, 0.0, 1.0));
		m_txtmgr->addTextBox(ID_DEBUG_TXT5, CRenderTextManager::EDV_DEFAULTFONT_ID, "", 0, height-30, 800, 600, Ogre::ColourValue(0.0, 1.0, 0.0, 1.0));
		
	}
	else
	{
		m_txtmgr->removeTextBox(ID_DEBUG_TXT);
		m_txtmgr->removeTextBox(ID_DEBUG_TXT2);
		m_txtmgr->removeTextBox(ID_DEBUG_TXT3);
		m_txtmgr->removeTextBox(ID_DEBUG_TXT4);
		m_txtmgr->removeTextBox(ID_DEBUG_TXT5);
	
	}


}
void	CRenderManager::UpdateDebugMode()
{
	static bool first = true;
	if (first)
	{
		m_timerupdatedebug = CCoreEngine::Instance().GetTimerManager().CreateTimer(1.0, true);
		first = false;
	}

	if (m_debugmode && CCoreEngine::Instance().GetTimerManager().IsEndTimer(m_timerupdatedebug))
	{
		Ogre::RenderWindow::FrameStats l_framestat = m_OgreWindow->getStatistics();
		
		m_txtmgr->setText(ID_DEBUG_TXT,"Triangle Count: " + to_string(l_framestat.triangleCount));
		m_txtmgr->setText(ID_DEBUG_TXT2, "FPS: " + to_string(to_string(CCoreEngine::Instance().GetTimerManager().GetFPS()))  );
		m_txtmgr->setText(ID_DEBUG_TXT3, "Deltatime: " + to_string(CCoreEngine::Instance().GetTimerManager().GetElapsedTimeSeconds()));
		m_txtmgr->setText(ID_DEBUG_TXT4, "Batch count: " + to_string(l_framestat.batchCount));
		m_txtmgr->setText(ID_DEBUG_TXT5, "Current Camera ID: " + m_sceneMgr->getCurrentViewport()->getCamera()->getName());
		
	}

	
}
void	CRenderManager::ShutDown()
{
	LOG( LOG_INFO, LOGSUB_VIDEO,"ShutDown!  ");

	SDL_DestroyWindow(m_SDLWinhndlr);
	SDL_QuitSubSystem(SDL_INIT_VIDEO);

	delete CRenderDebug::getSingletonPtr();
	delete m_txtmgr;
}

void CRenderManager::Render(void)
{
//	auto abans = SDL_GetTicks();
//	CRenderDebug::getSingletonPtr()->build();
//	m_OgreRoot->renderOneFrame();
//	CRenderDebug::getSingletonPtr()->clear();
//	auto despres = SDL_GetTicks();
//	LOG(LOG_INFO, LOGSUB_VIDEO, "Temps de render: %d  ", despres-abans);

}

void CRenderManager::Update(f64 dt)
{
	UpdateDebugMode();
}

void CRenderManager::addScene(eSceneFormat a_format, const std::string& a_filename)
{
	if (m_sceneMgr)
	{
		switch (a_format)
		{
		case SCENE_FORMAT_DOTSCENE:
			{
				DotSceneLoader loader;
				loader.parseDotScene(a_filename, "General", m_sceneMgr);
			}
			break;
		}
	}
}

void CRenderManager::getMeshInformation(Ogre::Mesh* mesh, size_t &vertex_count, std::vector<Ogre::Vector3> &vertices,
	size_t &index_count, std::vector<unsigned int> &indices,
	const Ogre::Vector3 &position,
	const Ogre::Quaternion &orient, 
	const Ogre::Vector3 &scale)
{
	vertex_count = index_count = 0;

	bool added_shared = false;
	size_t current_offset = vertex_count;
	size_t shared_offset = vertex_count;
	size_t next_offset = vertex_count;
	size_t index_offset = index_count;
	size_t prev_vert = vertex_count;
	size_t prev_ind = index_count;

	// Calculate how many vertices and indices we're going to need
	for (int i = 0; i < mesh->getNumSubMeshes(); i++)
	{
		Ogre::SubMesh* submesh = mesh->getSubMesh(i);

		// We only need to add the shared vertices once
		if (submesh->useSharedVertices)
		{
			if (!added_shared)
			{
				Ogre::VertexData* vertex_data = mesh->sharedVertexData;
				vertex_count += vertex_data->vertexCount;
				added_shared = true;
			}
		}
		else
		{
			Ogre::VertexData* vertex_data = submesh->vertexData;
			vertex_count += vertex_data->vertexCount;
		}

		// Add the indices
		Ogre::IndexData* index_data = submesh->indexData;
		index_count += index_data->indexCount;
	}

	// Allocate space for the vertices and indices
	for (; vertices.size() != vertex_count + 1; vertices.push_back(Ogre::Vector3()));
	for (; indices.size() != index_count + 1; indices.push_back(unsigned int()));

	added_shared = false;

	// Run through the submeshes again, adding the data into the arrays
	for (int i = 0; i < mesh->getNumSubMeshes(); i++)
	{
		Ogre::SubMesh* submesh = mesh->getSubMesh(i);

		Ogre::VertexData* vertex_data = submesh->useSharedVertices ? mesh->sharedVertexData : submesh->vertexData;
		if ((!submesh->useSharedVertices) || (submesh->useSharedVertices && !added_shared))
		{
			if (submesh->useSharedVertices)
			{
				added_shared = true;
				shared_offset = current_offset;
			}

			const Ogre::VertexElement* posElem = vertex_data->vertexDeclaration->findElementBySemantic(Ogre::VES_POSITION);
			Ogre::HardwareVertexBufferSharedPtr vbuf = vertex_data->vertexBufferBinding->getBuffer(posElem->getSource());
			unsigned char* vertex = static_cast<unsigned char*>(vbuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
			Ogre::Real* pReal;

			for (size_t j = 0; j < vertex_data->vertexCount; ++j, vertex += vbuf->getVertexSize())
			{
				posElem->baseVertexPointerToElement(vertex, &pReal);

				Ogre::Vector3 pt;

				pt.x = (*pReal++);
				pt.y = (*pReal++);
				pt.z = (*pReal++);

				pt = (orient * (pt * scale)) + position;

				vertices[current_offset + j].x = pt.x;
				vertices[current_offset + j].y = pt.y;
				vertices[current_offset + j].z = pt.z;
			}
			vbuf->unlock();
			next_offset += vertex_data->vertexCount;
		}

		Ogre::IndexData* index_data = submesh->indexData;

		size_t numTris = index_data->indexCount / 3;
		unsigned short* pShort;
		unsigned int* pInt;
		Ogre::HardwareIndexBufferSharedPtr ibuf = index_data->indexBuffer;
		bool use32bitindexes = (ibuf->getType() == Ogre::HardwareIndexBuffer::IT_32BIT);
		if (use32bitindexes) pInt = static_cast<unsigned int*>(ibuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
		else pShort = static_cast<unsigned short*>(ibuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));

		for (size_t k = 0; k < numTris; ++k)
		{
			size_t offset = (submesh->useSharedVertices) ? shared_offset : current_offset;

			unsigned int vindex = use32bitindexes ? *pInt++ : *pShort++;
			indices[index_offset + 0] = vindex + offset;
			vindex = use32bitindexes ? *pInt++ : *pShort++;
			indices[index_offset + 1] = vindex + offset;
			vindex = use32bitindexes ? *pInt++ : *pShort++;
			indices[index_offset + 2] = vindex + offset;

			index_offset += 3;
		}
		ibuf->unlock();
		current_offset = next_offset;
	}
}


void CRenderManager::SwitchRenderMode()
{
	
	Ogre::PolygonMode mode = m_viewport->getCamera()->getPolygonMode();

	switch (mode)
	{
	case Ogre::PM_POINTS:
		m_viewport->getCamera()->setPolygonMode(Ogre::PM_WIREFRAME);
		break;
	case Ogre::PM_WIREFRAME:
		m_viewport->getCamera()->setPolygonMode(Ogre::PM_SOLID);
		break;
	case Ogre::PM_SOLID:
		m_viewport->getCamera()->setPolygonMode(Ogre::PM_POINTS);
		break;
	default:
		m_viewport->getCamera()->setPolygonMode(Ogre::PM_SOLID);
		break;
	}

}

CRenderManager::Compositor::Id CRenderManager::addCompositor(const std::string& a_name, Ogre::MaterialManager::Listener* a_listener)
{
	if (!a_name.empty())
	{
		auto& compMgr = Ogre::CompositorManager::getSingleton();

		compMgr.addCompositor(m_viewport, a_name);
		compMgr.setCompositorEnabled(m_viewport, a_name, true);

		if (a_listener)
		{
			Ogre::MaterialManager::getSingleton().addListener(a_listener);
		}

		const Compositor::Id nextId = m_compositorIdGen.nextId();
		m_compositors[nextId] = { a_name, a_listener };

		return nextId;
	}
	else
	{
		throw std::runtime_error{ "invalid parameter" };
	}
}

void CRenderManager::removeCompositor(Compositor::Id a_id)
{
	auto it = m_compositors.find(a_id);

	if (it != m_compositors.end())
	{
		const Compositor& compositor = it->second;

		if (compositor.listener)
		{
			Ogre::MaterialManager::getSingleton().removeListener(compositor.listener);
		}

		Ogre::CompositorManager::getSingleton().removeCompositor(m_viewport, compositor.name);
		m_compositors.erase(it);
	}
}


Ogre::Vector2 CRenderManager::getTextureSize(const std::string& aTextureName) const
{
	auto texturePtr = Ogre::TextureManager::getSingleton().getByName(aTextureName, "General");
	bool loaded(false);
	if (!texturePtr.get())
	{
		texturePtr = Ogre::TextureManager::getSingleton().load(aTextureName, "General");
		loaded = true;
	}
	Ogre::Vector2 size{ static_cast<f32>(texturePtr->getSrcWidth()), static_cast<f32>(texturePtr->getSrcHeight()) };
	
	if (loaded)
	{
		texturePtr->unload();
	}

	return size;
}
