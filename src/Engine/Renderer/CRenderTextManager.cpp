#include "Renderer\CRenderTextManager.h"
#include "Core/CCoreEngine.h"
#include "Core/CLogManager.h"
#include "Core\exceptions.h"
#include "Options/Optionmanager.h"


const std::string CRenderTextManager::EDV_DEFAULTFONT_ID = "SystemFontID";
const std::string CRenderTextManager::EDV_DEFAULTFONT_FILENAME = "SystemFont.ttf";

CRenderTextManager::CRenderTextManager()
{
	m_overlayMgr = Ogre::OverlayManager::getSingletonPtr();

	m_overlay = m_overlayMgr->create("CRenderTextManagerOverlay");
	m_panel = static_cast<Ogre::OverlayContainer*>(m_overlayMgr->createOverlayElement("Panel", "CRenderTextManagerPanel"));
	m_panel->setDimensions(1, 1);
	m_panel->setPosition(0, 0);

	m_overlay->add2D(m_panel);
	m_overlay->setZOrder(CCoreEngine::Instance().GetOptionManager().getOption("overlay/z_order/RenderTextManager"));

	m_overlay->show();

	//createTTFFont(EDV_DEFAULTFONT_ID, EDV_DEFAULTFONT_FILENAME);
}

CRenderTextManager::~CRenderTextManager()
{

}

void CRenderTextManager::createTTFFont(const std::string & a_fontID, const std::string &a_fontfilename, const std::string & a_fontsize)
{

	Ogre::FontManager *fontMgr = Ogre::FontManager::getSingletonPtr();

	if (!fontMgr->resourceExists(a_fontID))
	{
		Ogre::FontPtr  font = fontMgr->create(a_fontID, "General");
		font->setParameter("type", "truetype");
		font->setParameter("source", a_fontfilename);
		font->setParameter("size", a_fontsize);
		font->setParameter("resolution", "96");
		font->load();
		
	}



}

void CRenderTextManager::addTextBox(const std::string& ID, const std::string &a_fontID,const std::string& text, f32 x, f32 y, f32 width, f32 height, const Ogre::ColourValue& color)
{
	Ogre::FontManager *fontMgr = Ogre::FontManager::getSingletonPtr();

	//Load the default font if not loaded (Only for the default!!!)
	if (a_fontID == EDV_DEFAULTFONT_ID)
	{
		if (!fontMgr->resourceExists(a_fontID))
			createTTFFont(EDV_DEFAULTFONT_ID, EDV_DEFAULTFONT_FILENAME);
	}
	

	if (fontMgr->resourceExists(a_fontID))
	{

		auto fntptr = fontMgr->getByName(a_fontID);

		//La borrem si existeix, per aplicar-li nova configuracio
		removeTextBox(ID);

		Ogre::OverlayElement* textBox = m_overlayMgr->createOverlayElement("TextArea", ID);
		textBox->setDimensions(width, height);
		textBox->setMetricsMode(Ogre::GMM_PIXELS);
		textBox->setPosition(x, y);
		textBox->setWidth(width);
		textBox->setHeight(height);
		textBox->setParameter("font_name", a_fontID);
		auto ffff = fntptr->getParameter("size");
		textBox->setParameter("char_height", ffff);
		textBox->setParameter("size", ffff);
		auto res = fntptr->getParameter("resolution");
		textBox->setParameter("resolution", res);
		textBox->setColour(color);
		textBox->setCaption(text);
	

		m_panel->addChild(textBox);
		
	}
	else
		throw edv::core::exc::fatal_error("font ID: " + a_fontID + " not exists");
}

void CRenderTextManager::removeTextBox(const std::string& ID)
{
	if (m_overlayMgr->hasOverlayElement(ID))
	{
		m_panel->removeChild(ID);
		m_overlayMgr->destroyOverlayElement(ID);
	}
}

void CRenderTextManager::setText(const std::string& ID, const std::string& Text)
{
	Ogre::OverlayElement* textBox = m_overlayMgr->getOverlayElement(ID);
	textBox->setCaption(Text);

}
const std::string CRenderTextManager::getText(const std::string& ID)
{
	Ogre::OverlayElement* textBox = m_overlayMgr->getOverlayElement(ID);
	return textBox->getCaption();

}

void CRenderTextManager::removeAlltext()
{
	auto child_iterator = m_panel->getChildIterator();
	std::vector<std::string> l_elements;

	while (child_iterator.hasMoreElements())
		l_elements.push_back(child_iterator.getNext()->getName());
	
	for (auto & element : l_elements)
		removeTextBox(element);

}