#include "Util.h"
#include "Core/CCoreEngine.h"

Ogre::Vector3 luaToOgreVector3(const luabind::object& a_object)
{
	const f32 x = luabind::object_cast<f32>(a_object["x"]);
	const f32 y = luabind::object_cast<f32>(a_object["y"]);
	const f32 z = luabind::object_cast<f32>(a_object["z"]);

	return{ x, y, z };
}

Ogre::Quaternion luaToOgreQuaternion(const luabind::object& a_object)
{
	const f32 w = luabind::object_cast<f32>(a_object["w"]);
	const f32 x = luabind::object_cast<f32>(a_object["x"]);
	const f32 y = luabind::object_cast<f32>(a_object["y"]);
	const f32 z = luabind::object_cast<f32>(a_object["z"]);

	return{ w, x, y, z };
}

luabind::object ogreVector3ToLua(const Ogre::Vector3& a_vect)
{
	luabind::adl::object obj = CCoreEngine::Instance().GetScriptManager().createTable();

	obj["x"] = a_vect.x;
	obj["y"] = a_vect.y;
	obj["z"] = a_vect.z;

	return obj;
}

luabind::object ogreVector2ToLua(const Ogre::Vector2& a_vect)
{
	luabind::object obj = CCoreEngine::Instance().GetScriptManager().createTable();

	obj["x"] = a_vect.x;
	obj["y"] = a_vect.y;

	return obj;
}

luabind::object ogreQuaternionToLua(const Ogre::Quaternion& a_quat)
{
	luabind::object obj = CCoreEngine::Instance().GetScriptManager().createTable();;

	obj["w"] = a_quat.w;
	obj["x"] = a_quat.x;
	obj["y"] = a_quat.y;
	obj["z"] = a_quat.z;

	return obj;
}

btVector3 luaToBtVector3(const luabind::object& a_obj)
{
	btVector3 vect;

	vect.setX(luabind::object_cast<f32>(a_obj["x"]));
	vect.setY(luabind::object_cast<f32>(a_obj["y"]));
	vect.setZ(luabind::object_cast<f32>(a_obj["z"]));

	return vect;
}

luabind::object btVector3ToLua(const btVector3& a_vect)
{
	luabind::object obj;

	obj["x"] = a_vect.x();
	obj["y"] = a_vect.y();
	obj["z"] = a_vect.z();

	return obj;
}
