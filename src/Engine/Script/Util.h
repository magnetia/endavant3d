#ifndef EDV_SCRIPT_UTIL_H_
#define EDV_SCRIPT_UTIL_H_

#include <btBulletDynamicsCommon.h>
#include "Script/ScriptManager.h"
#include "Externals/OGRE/Ogre.h"

Ogre::Vector3 luaToOgreVector3(const luabind::object& a_object);
Ogre::Quaternion luaToOgreQuaternion(const luabind::object& a_object);
luabind::object ogreVector3ToLua(const Ogre::Vector3& a_vect);
luabind::object ogreVector2ToLua(const Ogre::Vector2& a_vect);
luabind::object ogreQuaternionToLua(const Ogre::Quaternion& a_quat);
btVector3 luaToBtVector3(const luabind::object& a_obj);
luabind::object btVector3ToLua(const btVector3& a_vect);

#endif
