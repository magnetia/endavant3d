#include <sstream>
#include <algorithm>
#include "Script/Bindings/LuaOgreEntityBinding.h"
#include "Script/Bindings/LuaSceneNodeBinding.h"
#include "Script/Bindings/LuaRigidBodyBinding.h"
#include "Script/Bindings/EntityOgreGraphicsBinding.h"
#include "Script/Bindings/EntityBtPhysicsBinding.h"
#include "Script/Bindings/BehaviorBinding.h"
#include "Script/Bindings/GameEntityBinding.h"
#include "Script/Bindings/ActorBinding.h"
#include "Script/Bindings/GameManagerBinding.h"
#include "Script/Bindings/edvVectorBinding.h"
#include "Script/Bindings/MagneticPropertyBinding.h"
#include "Script/Bindings/LevelBinding.h"
#include "Script/ScriptManager.h"

ScriptManager::ScriptManager() :
	m_L{ luaL_newstate() }
{
	luaL_openlibs(m_L);
	luabind::set_error_callback(luabindErrorHandler);
	luabind::open(m_L);
}

ScriptManager::~ScriptManager()
{
	lua_close(m_L);
	m_L = nullptr;
}

void ScriptManager::startUp()
{
	// register builtin bindings
	registerScope(toLuaClass<LuaOgreEntity>());
	registerScope(toLuaClass<LuaSceneNode>());
	registerScope(toLuaClass<LuaRigidBody>());
	registerScope(toLuaClass<EntityOgreGraphics>());
	registerScope(toLuaClass<EntityBtPhysics>());
	registerScope(toLuaClass<Behavior>());
	registerScope(toLuaClass<GameEntity>());
	registerScope(toLuaClass<Actor>());
	registerScope(toLuaClass<GameManager>());
	registerScope(toLuaClass<edv::Vector3f>());
	registerScope(toLuaClass<MagneticProperty>());
	registerScope(toLuaClass<Level>());
}

void ScriptManager::shutDown()
{

}

void ScriptManager::update(f64 dt)
{

}

// #todo: descarregar scripts
void ScriptManager::loadScript(const std::string& a_path)
{
	luaL_dofile(m_L, a_path.c_str());
}

void ScriptManager::registerScope(luabind::scope a_scope)
{
	if (m_L)
	{
		luabind::module(m_L)[a_scope];
	} 
}

luabind::object ScriptManager::createTable()
{
	return luabind::newtable(m_L);
}

luabind::object ScriptManager::getGlobal(const std::string& a_name)
{
	luabind::object obj;

	if (m_L)
	{
		return luabind::globals(m_L)[a_name];
	}

	return obj;
}

void ScriptManager::unloadGlobal(const std::string& a_name)
{
	if (m_L)
	{
		luabind::globals(m_L)[a_name] = luabind::nil;
	}
}

// taken from http://stackoverflow.com/questions/10768610/lualuabind-no-error-information-at-top-of-stack-after-runtime-error //////////////////////////////////////
void luabindErrorHandler(lua_State* L)
{
	// log the error message
	luabind::object msg(luabind::from_stack(L, -1));
	std::ostringstream str;
	str << "lua> run-time error: " << msg;
	std::cout << std::endl << std::endl << str.str() << std::endl;

	// log the callstack
	std::string traceback = luabind::call_function<std::string>(luabind::globals(L)["debug"]["traceback"]);
	traceback = std::string("lua> ") + traceback;
	std::cout << traceback.c_str() << std::endl;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
