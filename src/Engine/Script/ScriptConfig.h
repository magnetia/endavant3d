#ifndef EDV_SCRIPT_SCRIPT_CONFIG_H_
#define EDV_SCRIPT_SCRIPT_CONFIG_H_

#include <string>

struct ScriptConfig
{
	std::string	path;
	std::string	scope;

	ScriptConfig(const std::string& a_path = "", const std::string& a_scope = "");
	bool empty() const;
};

#endif
