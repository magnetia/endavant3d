#ifndef EDV_SCRIPT_ENTITY_BT_PHYSICS_BINDING_H_
#define EDV_SCRIPT_ENTITY_BT_PHYSICS_BINDING_H_

#include "Game/EntityBtPhysics.h"
#include "Script/ScriptManager.h"
#include "Script/Wrappers/LuaRigidBody.h"

LuaRigidBody getLuaRigidBody(EntityBtPhysics* a_physics);

template <> inline luabind::scope toLuaClass<EntityBtPhysics>()
{
	using namespace luabind;
	return
		class_<EntityBtPhysics>("EntityBtPhysics")

			.enum_("CollisionShape")
			[
				value("COLLISION_SHAPE_NONE", 0),
				value("COLLISION_SHAPE_SPHERE", 1),
				value("COLLISION_SHAPE_BOX", 2),
				value("COLLISION_SHAPE_HULL", 3),
				value("COLLISION_SHAPE_TRIANGLE_MESH", 4),
				value("COLLISION_SHAPE_CAPSULE", 5)
			]

			.enum_("CollisionDetection")
				[
					value("COLLISION_DETECTION_NONE", 0),
					value("COLLISION_DETECTION_DISABLE", 1),
					value("COLLISION_DETECTION_GHOST", 2),
					value("COLLISION_DETECTION_MANIFOLDS", 3),
					value("COLLISION_DETECTION_TRIGGER", 4)
				]

			.def("isKinematic", &EntityBtPhysics::isKinematic)
			.def("getMass", &EntityBtPhysics::getMass)
			.def("getCollisionShape", &EntityBtPhysics::getCollisionShape)
			.def("getCollisionDetectionMode", &EntityBtPhysics::getCollisionDetectionMode)
			.def("getMagneticProperty", &EntityBtPhysics::getMagneticProperty)
			.def("has2DPhysics", &EntityBtPhysics::has2DPhysics)
			.def("hasTriggerCollision", &EntityBtPhysics::hasTriggerCollision)
			.def("isStatic", &EntityBtPhysics::isStatic)
			.def("setCollisionEnabled", &EntityBtPhysics::setCollisionEnabled);
}

#endif
