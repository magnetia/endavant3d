#ifndef EDV_SCRIPT_BEHAVIOR_BINDING_H_
#define EDV_SCRIPT_BEHAVIOR_BINDING_H_

#include "Game/Behavior.h"
#include "Script/ScriptManager.h"
#include "Utils/stuff.h"

template <> inline luabind::scope toLuaClass<Behavior>()
{
	pointer_to_member<GameEntity*, Behavior>::type getParentPtr{ &Behavior::getParent };

	using namespace luabind;
	return
		class_<Behavior>("Behavior")
			.def("getParent", getParentPtr);
}

#endif
