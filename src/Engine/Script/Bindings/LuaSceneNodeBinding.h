#ifndef EDV_SCRIPT_LUA_SCENE_NODE_BINDING_H_
#define EDV_SCRIPT_LUA_SCENE_NODE_BINDING_H_

#include "Script/ScriptManager.h"
#include "Script/Wrappers/LuaSceneNode.h"

template <> inline luabind::scope toLuaClass<LuaSceneNode>()
{
	using namespace luabind;
	return
		class_<LuaSceneNode>("LuaSceneNode")
			.def("getName", &LuaSceneNode::getName)
			.def("setDerivedPosition", &LuaSceneNode::setDerivedPosition)
			.def("setDerivedOrientation", &LuaSceneNode::setDerivedOrientation)
			.def("setPosition", &LuaSceneNode::setPosition)
			.def("setOrientation", &LuaSceneNode::setOrientation)
			.def("setVisible", &LuaSceneNode::setVisible)
			.def("getDerivedPosition", &LuaSceneNode::getDerivedPosition)
			.def("getDerivedOrientation", &LuaSceneNode::getDerivedOrientation)
			.def("getPosition", &LuaSceneNode::getPosition)
			.def("getOrientation", &LuaSceneNode::getOrientation)
			.def("hasVisibleObjects", &LuaSceneNode::hasVisibleObjects);
}

#endif
