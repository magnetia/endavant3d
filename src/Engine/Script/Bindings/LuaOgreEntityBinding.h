#ifndef EDV_SCRIPT_LUA_OGRE_ENTITY_BINDING_H_
#define EDV_SCRIPT_LUA_OGRE_ENTITY_BINDING_H_

#include "Script/ScriptManager.h"
#include "Script/Wrappers/LuaOgreEntity.h"

template <> inline luabind::scope toLuaClass<LuaOgreEntity>()
{
	using namespace luabind;
	return
		class_<LuaOgreEntity>("LuaOgreEntity")
			.def("setMaterialName", &LuaOgreEntity::setMaterialName)
			.def("getFrame", &LuaOgreEntity::getFrame)
			.def("setFrame", &LuaOgreEntity::setFrame)
			.def("getNumFrames", &LuaOgreEntity::getNumFrames);
}

#endif
