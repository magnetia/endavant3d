#include "ActorBinding.h"

void applyMove(Actor* a_actor, f32 a_x, f32 a_y, f32 a_z)
{
	a_actor->applyMove(edv::Vector3f(a_x, a_y, a_z));
}

void applyForce(Actor* a_actor, f32 a_x, f32 a_y, f32 a_z)
{
	a_actor->applyForce(edv::Vector3f(a_x, a_y, a_z));
}

void applyImpulse(Actor* a_actor, f32 a_x, f32 a_y, f32 a_z)
{
	a_actor->applyImpulse(edv::Vector3f(a_x, a_y, a_z));
}

void setGravity(Actor* a_actor, f32 a_x, f32 a_y, f32 a_z)
{
	a_actor->setGravity(edv::Vector3f(a_x, a_y, a_z));
}