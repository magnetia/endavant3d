#ifndef EDV_SCRIPT_GAME_MANAGER_H_
#define EDV_SCRIPT_GAME_MANAGER_H_

#include "Script/ScriptManager.h"
#include "Game/GameManager.h"
#include "Utils/stuff.h"

class GameEntity;

GameManager* getGameManager();
GameEntity* createEntity(const std::string& a_type, const std::string& a_name, f32 x, f32 y);
GameEntity* getPlayer();

template <> inline luabind::scope toLuaClass<GameManager>()
{
	pointer_to_member<void, GameManager, GameEntity*>::type destroyEntity{ &GameManager::destroyEntity };
	pointer_to_member<void, GameManager, const std::string&>::type destroyEntityByName{ &GameManager::destroyEntity };

	using namespace luabind;
	return
		def("getGameManager", &getGameManager),
		def("getPlayer", &getPlayer),
		class_<GameManager>("GameManager")
			.def("getEntity", &GameManager::getEntity)
			.def("getLevel", &GameManager::getLevel)
			.def("getCurrentLevel", &GameManager::getCurrentLevel)
			.def("registerPlayer", &GameManager::registerPlayer)
			.def("createEntity", &createEntity)
			.def("destroyEntity", destroyEntity)
			.def("destroyEntityByName", destroyEntityByName);
}

#endif
