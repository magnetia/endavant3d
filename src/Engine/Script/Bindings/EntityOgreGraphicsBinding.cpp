#include "EntityOgreGraphicsBinding.h"

LuaSceneNode getLuaNode(EntityOgreGraphics* a_graphics)
{
	return LuaSceneNode(a_graphics->getNode());
}

LuaSceneNode getParticleSystemNode(EntityOgreGraphics* a_graphics, const char* a_particleName)
{
	std::string particleName{ a_particleName };
	return LuaSceneNode(a_graphics->getParticleSystemNode(particleName));
}

LuaOgreEntity getMainEntity(EntityOgreGraphics* a_graphics)
{
	return LuaOgreEntity(a_graphics->getMainEntity());
}
