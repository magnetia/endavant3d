#ifndef EDV_SCRIPT_GAME_ENTITY_BINDING_H_
#define EDV_SCRIPT_GAME_ENTITY_BINDING_H_

#include "Script/ScriptManager.h"
#include "Game/GameEntity.h"

bool sameEntity(const GameEntity& lhs, const GameEntity& rhs);
luabind::object	getWorldPosition(GameEntity* a_game_entity);
luabind::object	getScreenPosition(GameEntity* a_game_entity);
luabind::object	getScale(GameEntity* a_game_entity);
luabind::object	getHalfSize(GameEntity* a_game_entity);
luabind::object	getScaledHalfSize(GameEntity* a_game_entity);
void			setWorldPosition(GameEntity* a_game_entity, f32 a_x, f32 a_y, f32 a_z);
void			setScale(GameEntity* a_entity, f32 a_x, f32 a_y, f32 a_z);

template <> inline luabind::scope toLuaClass<GameEntity>()
{
	using namespace luabind;
	return
		class_<GameEntity>("GameEntity")
		.def("distanceToEntity", &GameEntity::distanceToEntity)
		.def("angleToEntity", &GameEntity::angleToEntity)
		.def("canSeeEntity", &GameEntity::canSeeEntity)
		.def("getName", &GameEntity::getName)
		.def("getGraphics", &GameEntity::getGraphics)
		.def("getPhysics", &GameEntity::getPhysics)
		.def("getActor", &GameEntity::getActor)
		.def("sameEntity", &sameEntity)
		.def("getWorldPosition", &getWorldPosition)
		.def("getScreenPosition", &getScreenPosition)
		.def("getScale", &getScale)
		.def("setWorldPosition", &setWorldPosition)
		.def("setScale", &setScale)
		.def("getHalfSize", &getHalfSize)
		.def("getScaledHalfSize", &getScaledHalfSize);
}

#endif
