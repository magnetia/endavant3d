#ifndef EDV_SCRIPT_ENTITY_OGRE_GRAPHICS_H_
#define EDV_SCRIPT_ENTITY_OGRE_GRAPHICS_H_

#include "Game/EntityOgreGraphics.h"
#include "Script/ScriptManager.h"
#include "Script/Wrappers/LuaSceneNode.h"
#include "Script/Wrappers/LuaOgreEntity.h"

LuaSceneNode getLuaNode(EntityOgreGraphics* a_graphics);
LuaSceneNode getParticleSystemNode(EntityOgreGraphics* a_graphics, const char* a_particleName);
LuaOgreEntity getMainEntity(EntityOgreGraphics* a_graphics);

template <> inline luabind::scope toLuaClass<EntityOgreGraphics>()
{
	using namespace luabind;
	return
		class_<EntityOgreGraphics>("EntityOgreGraphics")
			.def("getNode", &getLuaNode)
			.def("getParticleSystemNode", &getParticleSystemNode)
			.def("getMainEntity", &getMainEntity);
}


#endif
