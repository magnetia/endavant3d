#include "Core/CCoreEngine.h"
#include "Game/GameEntity.h"
#include "Script/Util.h"
#include "GameManagerBinding.h"

GameManager* getGameManager()
{
	return &CCoreEngine::Instance().GetGameManager();
}

GameEntity* createEntity(const std::string& a_type, const std::string& a_name, f32 x, f32 y)
{
	return CCoreEngine::Instance().GetGameManager().createEntity(a_type, a_name, Ogre::Vector3{ x, y, 0 });
}

GameEntity* getPlayer()
{
	return CCoreEngine::Instance().GetGameManager().getPlayer();
}
