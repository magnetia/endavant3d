#ifndef EDV_SCRIPT_LUA_RIGID_BODY_BINDING_H_
#define EDV_SCRIPT_LUA_RIGID_BODY_BINDING_H_

#include "Script/ScriptManager.h"
#include "Script/Wrappers/LuaRigidBody.h"

template <> inline luabind::scope toLuaClass<LuaRigidBody>()
{
	using namespace luabind;
	return
		class_<LuaRigidBody>("LuaRigidBody")
			.enum_("BT_STATE")
			[
				value("ACTIVE_TAG", 1),
				value("ISLAND_SLEEPING", 2),
				value("WANTS_DEACTIVATION", 3),
				value("DISABLE_DEACTIVATION", 4),
				value("DISABLE_SIMULATION", 5)
			]

			.def("setLinearVelocity", &LuaRigidBody::setLinearVelocity)
			.def("getLinearVelocity", &LuaRigidBody::getLinearVelocity)
			.def("forceActivationState", &LuaRigidBody::forceActivationState)
			.def("applyCentralForce", &LuaRigidBody::applyCentralForce);
}

#endif
