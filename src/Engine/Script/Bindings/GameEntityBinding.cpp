#include "GameEntityBinding.h"
#include "Script/Util.h"
#include "Core/CCoreEngine.h"
#include "Script/ScriptManager.h"

bool sameEntity(const GameEntity& lhs, const GameEntity& rhs)
{
	return &lhs == &rhs;
}

luabind::object	getWorldPosition(GameEntity* a_game_entity)
{
	Ogre::Vector3 position = a_game_entity->getWorldPosition().to_OgreVector3();
	return ogreVector3ToLua(position);
}

luabind::object	getScreenPosition(GameEntity* a_game_entity)
{
	Ogre::Vector2 position = a_game_entity->getScreenPosition();
	return ogreVector2ToLua(position);
}

luabind::object	getScale(GameEntity* a_game_entity)
{
	Ogre::Vector3 scale = a_game_entity->getScale().to_OgreVector3();
	return ogreVector3ToLua(scale);
}

luabind::object	getHalfSize(GameEntity* a_game_entity)
{
	Ogre::Vector3 halfSize = a_game_entity->getHalfSize().to_OgreVector3();
	return ogreVector3ToLua(halfSize);
}

luabind::object	getScaledHalfSize(GameEntity* a_game_entity)
{
	Ogre::Vector3 scaledHalfSize = a_game_entity->getScaledHalfSize().to_OgreVector3();
	return ogreVector3ToLua(scaledHalfSize);
}

void setWorldPosition(GameEntity* a_game_entity, f32 a_x, f32 a_y, f32 a_z)
{
	a_game_entity->setWorldPosition(Ogre::Vector3(a_x, a_y, a_z));
}

void setScale(GameEntity* a_entity, f32 a_x, f32 a_y, f32 a_z)
{
	a_entity->setScale({ a_x, a_y, a_z });
}
