#ifndef EDV_SCRIPT_LEVEL_BINDING_H_
#define EDV_SCRIPT_LEVEL_BINDING_H_

#include "Game/Level.h"
#include "Script/ScriptManager.h"

template <> inline luabind::scope toLuaClass<Level>()
{
	using namespace luabind;
	return
		class_<Level>("Level");
}

#endif
