#ifndef EDV_SCRIPT_EDV_VECTOR_BINDING_H_
#define EDV_SCRIPT_EDV_VECTOR_BINDING_H_

#include <Script/ScriptManager.h>
#include "Utils/edvVector3.h"

template <> inline luabind::scope toLuaClass<edv::Vector3f>()
{
	using namespace luabind;
	return
		class_<edv::Vector3f>("edvVector3f")
			.def_readwrite("x", &edv::Vector3f::x)
			.def_readwrite("y", &edv::Vector3f::y)
			.def_readwrite("z", &edv::Vector3f::z);
}

#endif
