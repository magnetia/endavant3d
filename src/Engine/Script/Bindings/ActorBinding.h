#ifndef EDV_SCRIPT_ACTOR_BINDING_H_
#define EDV_SCRIPT_ACTOR_BINDING_H_

#include "Script/ScriptManager.h"
#include "Game/Actor.h"

void applyMove(Actor* a_actor, f32 a_x, f32 a_y, f32 a_z);
void applyForce(Actor* a_actor, f32 a_x, f32 a_y, f32 a_z);
void applyImpulse(Actor* a_actor, f32 a_x, f32 a_y, f32 a_z);
void setGravity(Actor* a_actor, f32 a_x, f32 a_y, f32 a_z);

template <> inline luabind::scope toLuaClass<Actor>()
{
	using namespace luabind;
	return
		class_<Actor>("Actor")
				.def("init", &Actor::init)
				.def("stop", &Actor::stop)
				.def("stateElapsed", &Actor::stateElapsed)
				.def("getState", &Actor::getState)
				.def("setState", &Actor::setState)
				.def("setLife", &Actor::setLife)
				.def("setMaxLife", &Actor::setMaxLife)
				.def("decreaseLife", &Actor::decreaseLife)
				.def("kill", &Actor::kill)
				.def("getLife", &Actor::getLife)
				.def("getMaxLife", &Actor::getMaxLife)
				.def("applyMove", &applyMove)
				.def("applyForce", &applyForce)
				.def("applyImpulse", &applyImpulse)
				.def("setGravity", &setGravity)
				.def("getVelocity", &Actor::getVelocity)
				.def("resetVelocity", &Actor::resetVelocity)
				.def("isOnGround", &Actor::isOnGround)
				.def("isOnCeiling", &Actor::isOnCeiling);
}

#endif
