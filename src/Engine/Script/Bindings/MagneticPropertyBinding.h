#ifndef EDV_SCRIPT_MAGNETIC_PROPERTY_BINDING_H_
#define EDV_SCRIPT_MAGNETIC_PROPERTY_BINDING_H_

#include "Script/ScriptManager.h"
#include "Physics/MagneticProperty.h"

template <> inline luabind::scope toLuaClass<MagneticProperty>()
{
	using namespace luabind;
	return
		class_<MagneticProperty>("MagneticProperty")
			.enum_("MagneticCharge")
			[
				value("MAGNET_TYPE_POSITIVE", 0),
				value("MAGNET_TYPE_NEGATIVE", 1),
				value("MAGNET_TYPE_NEUTRAL", 2)
			]
	
		.property("charge", &MagneticProperty::getCharge, &MagneticProperty::setCharge)
		.property("force", &MagneticProperty::getForce, &MagneticProperty::setForce)
		.property("scope", &MagneticProperty::getScope, &MagneticProperty::setScope)
		.def("isActive", &MagneticProperty::isActive)
		.def("switchPolarity", &MagneticProperty::switchPolarity)
		.def("hasAttraction", &MagneticProperty::hasAttraction)
		.def("hasRepulsion", &MagneticProperty::hasRepulsion);
}

#endif
