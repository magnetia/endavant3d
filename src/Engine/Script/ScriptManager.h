#ifndef EDV_SCRIPT_SCRIPT_MANAGER_H_
#define EDV_SCRIPT_SCRIPT_MANAGER_H_

// needs to have dll-interface. Si als programadors de luabind
// no els preocupa aquest warning, a mi tampoc.
#pragma warning(disable: 4251)

#include <string>
#include <vector>
#include "Externals/lua/lua.hpp" 
#include "Externals/luabind/luabind.hpp"
#include "Externals/luabind/object.hpp"
#include "Core/CBasicTypes.h"
#include "Utils/CSingleton.h"

void luabindErrorHandler(lua_State* L);
template <typename T> luabind::scope toLuaClass();

class ScriptManager
{
public:
	ScriptManager();
	~ScriptManager();

	void startUp();
	void shutDown();
	void update(f64 dt);

	void loadScript(const std::string& a_path); // #todo: especificar scope? provar si posan-t'ho segueix funcionant call_function
	template <typename T, typename... Args> T callFunction(const std::string& a_name, Args... args);
	template <typename... Args>	void callProcedure(const std::string& a_name, Args... args);
	template <typename T> void registerGlobal(const std::string& a_id, const T& a_value);
	luabind::object getGlobal(const std::string& a_name);
	void unloadGlobal(const std::string& a_name);

	void registerScope(luabind::scope a_scope);
	luabind::object createTable();

private:
	typedef std::vector<std::string> LoadedScripts;	

	lua_State*		m_L;
};

template <typename T, typename... Args>
T ScriptManager::callFunction(const std::string& a_name, Args... args)
{
	if (m_L)
	{
		return luabind::call_function<T>(m_L, a_name.c_str(), args...);
	}
	
	return {};
}

template <typename... Args>
void ScriptManager::callProcedure(const std::string& a_name, Args... args)
{
	if (m_L)
	{
		luabind::call_function<void>(m_L, a_name.c_str(), args...);
	}
}

template <typename T> 
void registerGlobal(const std::string& a_id, const T& a_value)
{
	if (m_L)
	{
		luabind::globals(m_L)[a_id] = a_value;
	}
}

template <typename T>
luabind::scope toLuaClass()
{
	static_assert(0, "you must write a binding for this type");
}

#endif
