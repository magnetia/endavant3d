#include "ScriptConfig.h"

ScriptConfig::ScriptConfig(const std::string& a_path, const std::string& a_scope) :
	path{ a_path }, scope{ a_scope } 
{

}

bool ScriptConfig::empty() const
{
	return path.empty() && scope.empty();
}
