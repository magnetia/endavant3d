#ifndef EDV_SCRIPT_LUA_SCENE_NODE_H_
#define EDV_SCRIPT_LUA_SCENE_NODE_H_

#include "OGRE/Ogre.h"
#include "Script/ScriptManager.h"

class LuaSceneNode
{
public:
	LuaSceneNode(Ogre::SceneNode* a_node);

	void reset(Ogre::SceneNode* a_node = nullptr);
	Ogre::SceneNode* get();
	std::string getName();

	void setDerivedPosition(const luabind::object& a_pos);
	void setDerivedOrientation(const luabind::object& a_rot);
	void setPosition(const luabind::object& a_pos);
	void setOrientation(const luabind::object& a_rot);
	void setVisible(bool a_visible);
	luabind::object getDerivedPosition() const;
	luabind::object getDerivedOrientation() const;
	luabind::object getPosition() const;
	luabind::object getOrientation() const;
	bool hasVisibleObjects() const;

private:
	Ogre::SceneNode* m_node;
};

#endif
