#include "Core/CBasicTypes.h"
#include "Script/Util.h"
#include "LuaSceneNode.h"

LuaSceneNode::LuaSceneNode(Ogre::SceneNode* a_node) :
	m_node{ a_node }
{

}

void LuaSceneNode::reset(Ogre::SceneNode* a_node)
{
	m_node = a_node;
}

Ogre::SceneNode* LuaSceneNode::get()
{
	return m_node;
}

std::string LuaSceneNode::getName()
{
	return m_node->getName();
}

void LuaSceneNode::setDerivedPosition(const luabind::object& a_pos)
{
	if (m_node)
	{
		m_node->_setDerivedPosition(luaToOgreVector3(a_pos));
	}
}

void LuaSceneNode::setDerivedOrientation(const luabind::object& a_rot)
{
	if (m_node)
	{
		m_node->_setDerivedOrientation(luaToOgreQuaternion(a_rot));
	}
}

void LuaSceneNode::setPosition(const luabind::object& a_pos)
{
	if (m_node)
	{
		m_node->setPosition(luaToOgreVector3(a_pos));
	}
}

void LuaSceneNode::setOrientation(const luabind::object& a_rot)
{
	if (m_node)
	{
		m_node->setOrientation(luaToOgreQuaternion(a_rot));
	}
}

void LuaSceneNode::setVisible(bool a_visible)
{
	if (m_node)
	{
		m_node->setVisible(a_visible);
	}
}

luabind::object LuaSceneNode::getDerivedPosition() const
{
	if (m_node)
	{
		return ogreVector3ToLua(m_node->_getDerivedPosition());
	}

	return {};
}

luabind::object LuaSceneNode::getDerivedOrientation() const
{
	if (m_node)
	{
		return ogreQuaternionToLua(m_node->_getDerivedOrientation());
	}

	return {};
}

luabind::object LuaSceneNode::getPosition() const
{
	if (m_node)
	{
		return ogreVector3ToLua(m_node->getPosition());
	}

	return {};
}

luabind::object LuaSceneNode::getOrientation() const
{
	if (m_node)
	{
		return ogreQuaternionToLua(m_node->getOrientation());
	}

	return {};
}

bool LuaSceneNode::hasVisibleObjects() const
{
	bool visible = false;

	if (m_node)
	{
		auto it = m_node->getAttachedObjectIterator();

		while (!visible && it.hasMoreElements())
		{
			visible = visible && static_cast<Ogre::Entity*>(it.getNext())->isVisible();
		}
	}

	return visible;
}
