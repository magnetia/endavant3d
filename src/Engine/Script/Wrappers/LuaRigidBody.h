#ifndef EDV_SCRIPT_LUA_RIGID_BODY_H_
#define EDV_SCRIPT_LUA_RIGID_BODY_H_

#include "Core/CBasicTypes.h"
#include "Script/Util.h"

// #todo: afegir funcions a mesura que les necessitem

class LuaRigidBody
{
public:
	LuaRigidBody(btRigidBody* a_body);

	void setLinearVelocity(const luabind::object& a_linVel);
	luabind::object getLinearVelocity() const;
	void forceActivationState(s32 a_state);
	void applyCentralForce(const luabind::object& a_force);

private:
	btRigidBody* m_body;
};

#endif
