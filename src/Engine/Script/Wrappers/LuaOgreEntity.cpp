#include "Core/CBasicTypes.h"
#include "Script/Util.h"
#include "LuaOgreEntity.h"

LuaOgreEntity::LuaOgreEntity(Ogre::Entity* a_entity) :
	m_entity{ a_entity }
{

}

void LuaOgreEntity::setMaterialName(const std::string& a_material_name)
{
	if (m_entity)
	{
		m_entity->setMaterialName(a_material_name);
	}
}

u32 LuaOgreEntity::getFrame() const
{
	u32 frame{ 0 };
	if (m_entity)
	{
		frame = m_entity->getSubEntity(0)->getMaterial()->getTechnique(0)->getPass(0)->getTextureUnitState(0)->getCurrentFrame();
	}
	return frame;
}

void LuaOgreEntity::setFrame(const u32 a_frame)
{
	if (m_entity)
	{
		m_entity->getSubEntity(0)->getMaterial()->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setCurrentFrame(a_frame);
	}
}

u32 LuaOgreEntity::getNumFrames() const
{
	u32 frames{ 0 };
	if (m_entity)
	{
		frames = m_entity->getSubEntity(0)->getMaterial()->getTechnique(0)->getPass(0)->getTextureUnitState(0)->getNumFrames();
	}
	return frames;
}
