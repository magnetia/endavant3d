#include "Script/Util.h"
#include "LuaRigidBody.h"

LuaRigidBody::LuaRigidBody(btRigidBody* a_body) :
	m_body{ a_body }
{

}

void LuaRigidBody::setLinearVelocity(const luabind::object& a_linVel)
{
	if (m_body)
	{
		m_body->setLinearVelocity(luaToBtVector3(a_linVel));
	}
}

luabind::object LuaRigidBody::getLinearVelocity() const
{
	if (m_body)
	{
		return btVector3ToLua(m_body->getLinearVelocity());
	}

	return {};
}

void LuaRigidBody::forceActivationState(s32 a_state)
{
	if (m_body)
	{
		m_body->forceActivationState(a_state);
	}
}

void LuaRigidBody::applyCentralForce(const luabind::object& a_force)
{
	if (m_body)
	{
		m_body->applyCentralForce(luaToBtVector3(a_force));
	}
}
