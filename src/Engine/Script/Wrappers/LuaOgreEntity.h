#ifndef EDV_SCRIPT_LUA_OGRE_ENTITY_H_
#define EDV_SCRIPT_LUA_OGRE_ENTITY_H_

#include "OGRE/Ogre.h"
#include "Script/ScriptManager.h"

class LuaOgreEntity
{
public:
	LuaOgreEntity(Ogre::Entity* a_node);

	void setMaterialName(const std::string& a_material_name);
	u32 getFrame() const;
	void setFrame(const u32 a_frame);
	u32 getNumFrames() const;

private:
	Ogre::Entity* m_entity;
};

#endif
