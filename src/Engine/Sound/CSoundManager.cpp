#include <algorithm>
#include "CSoundManager.h"
#include "Core/CCoreEngine.h"
#include "Core/CLogManager.h"
#include "Resources/CResourceManager.h"
#include "Resources/CSoundResource.h"
#include "Resources/CTrackResource.h"
#include "Utils/tree_xml_utils.h"

const std::string AUDIO_CONFIG_XML_PATH("./Config/audio_options.xml");
const s32 SDL_REQUEST_AVAILABLE_CHANNEL = -1;
const s32 CSoundManager::UNASSIGNED_CHANNEL_ID = SDL_REQUEST_AVAILABLE_CHANNEL;
const s32 CSoundManager::MAX_VOLUME = MIX_MAX_VOLUME;
const f32 FMAX_VOLUME = static_cast<f32>(CSoundManager::MAX_VOLUME);
const std::string SOUND_VOLUME_OPTION_PATH{ "audio/sound_volume" };
const std::string MUSIC_VOLUME_OPTION_PATH{ "audio/music_volume" };

CSoundManager::CSoundManager() :
	m_PreloadedResources{ },
	m_Channels{ },
	m_Music{ },
	m_config{ },
	m_soundVolume{ 1.f },
	m_musicVolume{ 1.f },
	m_callbackIds{ },
	m_availableChannels{ },
	m_channels_mutex{ }
{

}

CSoundManager::~CSoundManager()
{

}

void CSoundManager::StartUp(void)
{
	loadConfiguration();
	initOptions();
	initializeMixer();

	LOG(LOG_INFO, LOGSUB_SOUND, "StartUp");
}

void CSoundManager::ShutDown(void)
{
	stopAllSounds();
	stopMusic();
	releasePreloadedResources();
	stopOptions();
	shutdownMixer();

	LOG(LOG_INFO, LOGSUB_SOUND, "ShutDown");
}

void CSoundManager::Update(f64 dt)
{

}

void CSoundManager::reloadConfiguration()
{
	// reload configuration without re-initializing SDL_Mixer or reloading resources from disk
	stopAllSounds();
	stopMusic();
	stopOptions();

	loadConfiguration();
	initOptions();
}

void CSoundManager::preload(E_RESOURCE_TYPE a_resourceType, const std::string& a_resourceId)
{
	_preload(a_resourceType, a_resourceId);
}

void CSoundManager::preloadSound(const std::string& a_soundId)
{
	preload(RESOURCE_TYPE_SOUND, getResourceIdFromConfig(RESOURCE_TYPE_SOUND, a_soundId));
}

void CSoundManager::preloadMusic(const std::string& a_trackId)
{
	preload(RESOURCE_TYPE_TRACK, getResourceIdFromConfig(RESOURCE_TYPE_TRACK, a_trackId));
}

void CSoundManager::release(const std::string& a_resourceId)
{
	auto it = m_PreloadedResources.find(a_resourceId);

	if (it != m_PreloadedResources.end())
	{
		CResource* resource = it->second.get();

		resource->drop();

		if (!resource->getReferenceCount())
		{
			m_PreloadedResources.erase(it);
		}
	}
	else
	{
		LOG(LOG_WARNING, LOGSUB_SOUND, "Resource %s not preloaded", a_resourceId.c_str());
	}
}

void CSoundManager::releaseSound(const std::string& a_soundId)
{
	release(getResourceIdFromConfig(RESOURCE_TYPE_SOUND, a_soundId));
}

void CSoundManager::releaseMusic(const std::string& a_trackId)
{
	release(getResourceIdFromConfig(RESOURCE_TYPE_TRACK, a_trackId));
}

CSoundManager::tOptions CSoundManager::getSoundOptions(const std::string& a_soundId)
{
	return m_config.try_get_tree("sounds/" + a_soundId);
}

CSoundManager::t_ChannelId CSoundManager::playSound(const std::string& a_soundId, tCallback a_callback)
{
	AudioConfig soundConfig = m_config.try_get_tree("sounds/" + a_soundId);

	if (!soundConfig.empty())
	{
		const std::string resourceId = soundConfig["resource"].as<std::string>();

		if (!resourceId.empty())
		{
			const s32 volume = soundConfig.try_get("volume", MAX_VOLUME);
			const s32 loops = soundConfig.try_get("loops", 0);
			const s32 fadeInMs = soundConfig.try_get("fade_in_ms", 0);
			const s32 fadeOutMs = soundConfig.try_get("fade_out_ms", 0);

			CResource* sound = _preload(RESOURCE_TYPE_SOUND, resourceId);
			Mix_Chunk* const chunk = static_cast<Mix_Chunk*>(sound->getResource());
			t_ChannelId channel = UNASSIGNED_CHANNEL_ID;

			channel = reserveChannel();
			Mix_Volume(channel, static_cast<s32>(volume * m_soundVolume));

			if (fadeInMs)
			{				
				Mix_FadeInChannel(channel, chunk, loops, fadeInMs);
			}
			else
			{
				Mix_PlayChannel(channel, chunk, loops);
			}

			ChannelInfo* const info = &m_Channels[channel];
			info->channel_id = channel;
			info->volume = volume;
			info->loops = loops;
			info->fadeOutMs = fadeOutMs;
			info->muted = false;
			info->resource = sound;
			info->callback = a_callback;

			return channel;
		}
		else
		{
			throw std::runtime_error{ "sound with id \"" + a_soundId + "\" does not specify a resource id" };
		}
	}
	else
	{
		throw std::runtime_error{ "unknown sound id: \"" + a_soundId + "\"" };
	}
}

void CSoundManager::setSoundVolume(t_ChannelId a_Id, s32 a_Volume)
{
	if (a_Volume >= 0 && a_Volume <= MAX_VOLUME)
	{
		ChannelInfo* const info = getPlayingChannelInfo(a_Id);

		if (info)
		{
			info->volume = a_Volume;
			Mix_Volume(a_Id, static_cast<s32>(a_Volume * m_soundVolume));
		}
	}
}

void CSoundManager::pauseSound(t_ChannelId a_Id)
{
	ChannelInfo* const info = getPlayingChannelInfo(a_Id);

	if (info)
	{
		Mix_Pause(a_Id);
	}
}

void CSoundManager::resumeSound(t_ChannelId a_Id)
{
	ChannelInfo* const info = getPlayingChannelInfo(a_Id);

	if (info)
	{
		Mix_Resume(a_Id);
	}
}

void CSoundManager::stopSound(t_ChannelId a_Id)
{
	ChannelInfo* const info = getPlayingChannelInfo(a_Id);

	if (info)
	{
		if (info->fadeOutMs)
		{
			Mix_FadeOutChannel(a_Id, info->fadeOutMs);
		}
		else
		{
			Mix_HaltChannel(a_Id);
		}
	}
}

void CSoundManager::stopAllSounds()
{
	Mix_Resume(-1);
	Mix_HaltChannel(-1);
	resetAvailableChannels();
}

s32 CSoundManager::getSoundVolume(t_ChannelId a_Id)
{
	s32 volume = 0;
	ChannelInfo* const info = getPlayingChannelInfo(a_Id);

	if (info)
	{
		volume = info->volume;
	}

	return volume;
}

void CSoundManager::playMusic(const std::string& a_trackId)
{
	stopMusic();

	AudioConfig trackConfig = m_config.try_get_tree("tracks/" + a_trackId);

	if (!trackConfig.empty())
	{
		const std::string resourceId = trackConfig["resource"].as<std::string>();

		if (!resourceId.empty())
		{
			const s32 fadeInMs = trackConfig.try_get("fade_in_ms", 0);
			m_Music.volume = trackConfig.try_get("volume", MAX_VOLUME).as<s32>();
			m_Music.loops = trackConfig.try_get("loops", 0);
			m_Music.fadeOutMs = trackConfig.try_get("fade_out_ms", 0);
			m_Music.muted = false;
			m_Music.resource = _preload(RESOURCE_TYPE_TRACK, resourceId);

			Mix_VolumeMusic(static_cast<s32>(m_Music.volume * m_musicVolume));
			Mix_Music* musicTrack = static_cast<Mix_Music*>(m_Music.resource->getResource());

			if (fadeInMs)
			{
				Mix_FadeInMusic(musicTrack, m_Music.loops, fadeInMs);
			}
			else
			{
				Mix_PlayMusic(musicTrack, m_Music.loops);
			}
		}
		else
		{
			throw std::runtime_error{ "track with id \"" + a_trackId + "\" does not specify a resource id" };
		}
	}
	else
	{
		throw std::runtime_error{ "unknown track id: \"" + a_trackId + "\"" };
	}	
}

void CSoundManager::setMusicVolume(s32 a_Volume)
{
	if (m_Music.resource && a_Volume >= 0 && a_Volume <= MAX_VOLUME)
	{
		m_Music.volume = a_Volume;
		Mix_VolumeMusic(static_cast<s32>(a_Volume * m_musicVolume));
	}
}

void CSoundManager::pauseMusic()
{
	if (m_Music.resource)
	{
		Mix_PauseMusic();
	}
}

void CSoundManager::resumeMusic()
{
	if (m_Music.resource)
	{
		Mix_ResumeMusic();
	}
}

void CSoundManager::stopMusic()
{
	if (m_Music.resource)
	{
		// #todo: fer servir una callback tambe per quan acaba la musica, de manera
		// que poguem alliberar el recurs lliurement
		s32 refCount = m_Music.resource->getReferenceCount();

		// if we are going to unload the resource, we must halt music immediatly
		if (m_Music.fadeOutMs && refCount > 1)
		{
			Mix_FadeOutMusic(m_Music.fadeOutMs);
		}
		else
		{
			Mix_HaltMusic();
		}

		m_Music.resource->drop();
		m_Music.resource = nullptr;
	}
}

s32 CSoundManager::getMusicVolume()
{
	s32 volume = 0;

	if (m_Music.resource)
	{
		volume = m_Music.volume;
	}

	return volume;
}

void CSoundManager::optionChanged(const std::string& a_path, const edv::any& a_newValue)
{
	if (a_path == SOUND_VOLUME_OPTION_PATH)
	{
		m_soundVolume = a_newValue.as<f32>() / 100.f;

		for (u32 channel = 0; channel < m_Channels.size(); channel++)
		{
			ChannelInfo& channelInfo = m_Channels[channel];

			Mix_Volume(channel, static_cast<s32>(channelInfo.volume * m_soundVolume));
		}
	}
	else if (a_path == MUSIC_VOLUME_OPTION_PATH)
	{
		m_musicVolume = a_newValue.as<f32>() / 100.f;

		Mix_VolumeMusic(static_cast<s32>(m_Music.volume * m_musicVolume));
	}
}

//void CSoundManager::mute(bool unmute = false)
//{
//
//}

void CSoundManager::channelFinished(s32 a_Channel)
{
	CCoreEngine::Instance().GetSoundManager().channelFinishedInternal(a_Channel);
}

void CSoundManager::loadConfiguration()
{
	// parse xml file
	m_config.clear();
	pugi::xml_document document;

	if (pugi::xml_parse_result result = document.load_file(AUDIO_CONFIG_XML_PATH.c_str()))
	{
		edv::tree_from_xml(m_config, document.child("AudioConfiguration"));
	}
}

void CSoundManager::initializeMixer()
{
	// default values if not specified in xml file
	s32 frequency = MIX_DEFAULT_FREQUENCY, channels = MIX_DEFAULT_CHANNELS, chunk_size = 1024, mixing_channels = MIX_CHANNELS;
	const /* we're using ogg here, motherfuckers. */ s32 flags = MIX_INIT_OGG;

	if (m_config.path_exists("options"))
	{
		const AudioConfig::tree_type& options = m_config.get_tree("options");

		frequency = options.try_get("frequency", frequency);
		channels = options.try_get("channels", channels);
		chunk_size = options.try_get("chunk_size", chunk_size);
		mixing_channels = options.try_get("mixing_channels", mixing_channels);
	}

	if (flags == Mix_Init(flags) &&
		Mix_OpenAudio(frequency, MIX_DEFAULT_FORMAT, channels, chunk_size) != -1 &&
		Mix_AllocateChannels(mixing_channels) == mixing_channels)
	{
		Mix_ChannelFinished(channelFinished);
		m_Channels.resize(mixing_channels);
		resetAvailableChannels();
	}
	else
	{
		throw std::runtime_error("error initializing sound subsystem: " + std::string(Mix_GetError()));
	}
}

void CSoundManager::shutdownMixer()
{
	Mix_CloseAudio();
	Mix_Quit();
}

void CSoundManager::initOptions()
{
	// load volume options
	auto& options = CCoreEngine::Instance().GetOptionManager();

	m_soundVolume = options.getOption(SOUND_VOLUME_OPTION_PATH).as<f32>() / 100.f;
	m_musicVolume = options.getOption(MUSIC_VOLUME_OPTION_PATH).as<f32>() / 100.f;

	// add variable change notification
	auto functor = std::bind(&CSoundManager::optionChanged, this, std::placeholders::_1, std::placeholders::_2);
	m_callbackIds.push_back(options.registerCallback(SOUND_VOLUME_OPTION_PATH, functor));
	m_callbackIds.push_back(options.registerCallback(MUSIC_VOLUME_OPTION_PATH, functor));
}

void CSoundManager::stopOptions()
{
	// unregister option manager callbacks
	auto& options = CCoreEngine::Instance().GetOptionManager();

	for (auto callbackId : m_callbackIds)
	{
		options.unregisterCallback(callbackId);
	}

	m_callbackIds.clear();
}

void CSoundManager::channelFinishedInternal(s32 a_Channel)
{
	// this function is called from the thread created by SDL_mixer
	// no mutex should be needed as m_Channels doesn't change size

	ChannelInfo* const info = getPlayingChannelInfo(a_Channel);

	if (info)
	{
		// reset channel_id
		{
			LockGuard lock{ m_channels_mutex };
			m_availableChannels.push_back(info->channel_id);
			info->channel_id = UNASSIGNED_CHANNEL_ID;
		}

		// drop reference to sound resource
		info->resource->drop();
		info->resource = nullptr;

		if (info->callback)
		{
			tCallback callback = info->callback;

			info->callback = {};
			callback(a_Channel);
		}
	}
}

void CSoundManager::releasePreloadedResources()
{
	for (auto it = m_PreloadedResources.begin(); it != m_PreloadedResources.end(); it++)
	{
		CResource* resource = it->second.get();

		while (resource->getReferenceCount())
		{
			resource->drop();
		}
	}

	m_PreloadedResources.clear();
}

CSoundManager::ChannelInfo* CSoundManager::getPlayingChannelInfo(t_ChannelId a_Channel)
{
	ChannelInfo* info = nullptr;

	if (a_Channel > -1 && a_Channel < static_cast<s32>(m_Channels.size()))
	{
		if (m_Channels[a_Channel].resource)
		{
			info = &m_Channels[a_Channel];
		}
	}

	return info;
}

std::string CSoundManager::getResourceIdFromConfig(E_RESOURCE_TYPE a_resourceType, const std::string& a_soundId)
{
	std::string resourceId;
	std::string path;

	switch (a_resourceType)
	{
	case RESOURCE_TYPE_SOUND:
		path = "sounds/";
		break;
	case RESOURCE_TYPE_TRACK:
		path = "tracks/";
		break;
	default:
		break;
	}

	AudioConfig config = m_config.try_get_tree(path + a_soundId);

	if (!config.empty())
	{
		resourceId = config["resource"].as<std::string>();
	}

	return resourceId;
}

CResource* CSoundManager::_preload(E_RESOURCE_TYPE a_resourceType, const std::string& a_resourceId)
{
	CResource* resource = nullptr;
	auto it = m_PreloadedResources.find(a_resourceId);

	if (it == m_PreloadedResources.end())
	{
		const std::string path = m_config["resources/" + a_resourceId].as<std::string>();

		if (!path.empty())
		{
			switch (a_resourceType)
			{
			case RESOURCE_TYPE_SOUND:
				resource = new CSoundResource{ a_resourceId, path };
				break;
			case RESOURCE_TYPE_TRACK:
				resource = new CTrackResource{ a_resourceId, path };
				break;
			default:
				throw std::runtime_error{ "unknown resource type" };
				break;
			}

			resource->grab();
			m_PreloadedResources.insert(it, { a_resourceId, ResourcePtr{ resource } });
		}
		else
		{
			throw std::runtime_error{ "resource \"" + a_resourceId + "\" not found or missing path" };
		}
	}
	else
	{
		resource = it->second.get();
		resource->grab();
	}

	return resource;
}

CSoundManager::t_ChannelId CSoundManager::reserveChannel()
{
	LockGuard lock{ m_channels_mutex };
	t_ChannelId channel{ m_availableChannels.back() };

	m_availableChannels.pop_back();

	return channel;
}

void CSoundManager::resetAvailableChannels()
{
	LockGuard lock{ m_channels_mutex };
	const s32 channelsSize{ static_cast<s32>(m_Channels.size()) };
	s32 channel{ channelsSize - 1 };

	m_availableChannels.resize(channelsSize);

	for (s32 channel_index = 0; channel_index < channelsSize; channel_index++)
	{
		m_availableChannels[channel_index] = channel;
		m_Channels[channel_index].channel_id = UNASSIGNED_CHANNEL_ID;
		channel--;
	}
}

void CSoundManager::printAvailableChannels()
{
	std::string available_channels;

	for (AvailableChannels::const_iterator it = m_availableChannels.begin(); it != m_availableChannels.end(); it++)
	{
		available_channels += edv::any(*it).as<std::string>() + ", ";
	}

	LOG(LOG_DEVEL, LOGSUB_SOUND, "Total Channels: %u \nAvailable Channels: %s\n", m_availableChannels.size(), available_channels.c_str());
}
