#ifndef CSOUNDMANAGER_H_
#define CSOUNDMANAGER_H_

#include <string>
#include <vector>
#include <functional>
#include <unordered_map>
#include <memory>
#include <mutex>
#include "SDL2/SDL_mixer.h"
#include "Core/CBasicTypes.h"
#include "Resources/CResource.h"
#include "Options/OptionManager.h"
#include "Utils/any.h"
#include "Utils/tree.h"

//	#todo: millorar:
//		estudiar la opcio de fer servir shared pointers en comptes del comptador de referencies de CResource

class CResource;
class CSoundResource;
class CTrackResource;

class CSoundManager
{
public:
	typedef s32									t_ChannelId;
	typedef std::function<void(t_ChannelId)>	tCallback;
	typedef edv::tree<edv::any>					tOptions;

	static const s32 UNASSIGNED_CHANNEL_ID;
	static const s32 MAX_VOLUME;

	CSoundManager();
	virtual ~CSoundManager();

	void StartUp(void);
	void ShutDown(void);
	void Update(f64 dt);

	void reloadConfiguration();

	void preload(E_RESOURCE_TYPE a_resourceType, const std::string& a_resourceId);
	void preloadSound(const std::string& a_soundId);
	void preloadMusic(const std::string& a_trackId);
	void release(const std::string& a_resourceId);
	void releaseSound(const std::string& a_soundId);
	void releaseMusic(const std::string& a_trackId);

	tOptions getSoundOptions(const std::string& a_soundId);
	t_ChannelId playSound(const std::string& a_soundId, tCallback a_callback = {});
	void setSoundVolume(t_ChannelId a_Id, s32 a_Volume);
	void pauseSound(t_ChannelId a_Id);
	void resumeSound(t_ChannelId a_Id);
	void stopSound(t_ChannelId a_Id);
	void stopAllSounds();
	s32 getSoundVolume(t_ChannelId a_Id);

	void playMusic(const std::string& a_trackId);
	void setMusicVolume(s32 a_Volume);
	void pauseMusic();
	void resumeMusic();
	void stopMusic();
	s32 getMusicVolume();

	void optionChanged(const std::string& a_path, const edv::any& a_newValue);

//#todo:
//	void mute(bool unmute = false);

private:
	struct ChannelInfo
	{
		t_ChannelId	channel_id;
		s32			volume;
		s32			loops;
		s32			fadeOutMs;
		bool		muted;
		CResource*	resource;
		tCallback	callback;

		ChannelInfo() : 
			channel_id(0), volume{ 0 }, loops{ 1 }, fadeOutMs{ 0 }, muted{ false }, resource{ nullptr }, callback{} { }
		ChannelInfo(t_ChannelId id, s32 v, s32 l, s32 fo, CResource* r, tCallback c) :
			channel_id(id), volume{ v }, loops{ l }, fadeOutMs{ fo }, muted{ false }, resource{ nullptr }, callback{ c } { }
	};

	typedef std::unique_ptr<CResource>						ResourcePtr;
	typedef std::unordered_map<std::string, ResourcePtr>	Resources;
	typedef std::vector<ChannelInfo>						t_Channels;
	typedef edv::tree<edv::any>								AudioConfig;
	typedef std::list<OptionManager::CallbackId>			CallbackIds;
	typedef std::vector<t_ChannelId>						AvailableChannels;
	typedef std::lock_guard<std::mutex>						LockGuard;

	static void channelFinished(s32 a_Channel);

	void loadConfiguration();
	void initializeMixer();
	void shutdownMixer();
	void initOptions();
	void stopOptions();
	void channelFinishedInternal(s32 a_Channel);
	void releasePreloadedResources();
	ChannelInfo* getPlayingChannelInfo(t_ChannelId a_Channel);
	std::string getResourceIdFromConfig(E_RESOURCE_TYPE a_resourceType, const std::string& a_soundId);
	CResource* _preload(E_RESOURCE_TYPE a_resourceType, const std::string& a_resourceId);
	t_ChannelId reserveChannel();
	void resetAvailableChannels();

	// #debug:
	void printAvailableChannels();

	Resources			m_PreloadedResources;
	t_Channels			m_Channels;
	ChannelInfo			m_Music;
	AudioConfig			m_config;
	f32					m_soundVolume;
	f32					m_musicVolume;
	CallbackIds			m_callbackIds;
	AvailableChannels	m_availableChannels;
	std::mutex			m_channels_mutex;
};

#endif
