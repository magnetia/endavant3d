#ifndef EDV_OPTIONS_OPTION_MANAGER_H_
#define EDV_OPTIONS_OPTION_MANAGER_H_

#include <memory>
#include <string>
#include <vector>
#include <unordered_map>
#include "Utils/any.h"
#include "Utils/tree.h"
#include "Utils/IdGenerator.h"

namespace pugi
{
	class xml_node;
}

class OptionManager
{
public:
	typedef edv::tree<edv::any>											Options;
	typedef std::pair<std::string, edv::any>							OptionPair;
	typedef std::vector<OptionPair>										Category;
	typedef std::function<void(const std::string&, const edv::any&)>	Callback;
	typedef u64															CallbackId;

	void startUp(void);
	void shutDown(void);
	void reload();
	void update(f64 dt);

	void loadFile(const std::string& a_filePath = DEFAULT_OPTIONS_PATH);
	void saveFile(const std::string& a_filePath = DEFAULT_OPTIONS_PATH);

	const edv::any& getOption(const std::string& a_path) const;
	void setOption(const std::string& a_path, const edv::any& a_value);
	Category getCategory(const std::string& a_path);

	CallbackId registerCallback(const std::string& a_path, const Callback& a_callback);
	void unregisterCallback(CallbackId a_id);

	static const std::string DEFAULT_OPTIONS_PATH;

private:
	struct CallbackInfo;

	typedef std::list<CallbackInfo>								CallbackInfoList;
	typedef std::unordered_map<std::string, CallbackInfoList>	Callbacks;

	struct CallbackInfo
	{
		u64			id;
		Callback	callback;

		CallbackInfo(u64 a_id, const Callback& a_callback) :
			id{ a_id }, callback{ a_callback } { }
	};

	Options				m_options;
	Callbacks			m_callbacks;
	IdGenerator<u64>	m_callbackIdGenerator;
};

#endif
