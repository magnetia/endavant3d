#include "Core/CCoreEngine.h"
#include "Core/CLogManager.h"
#include "Script/ScriptManager.h"
#include "Utils/tree_xml_utils.h"
#include "Externals/pugixml/pugixml.hpp"
#include "OptionManager.h"

const std::string OptionManager::DEFAULT_OPTIONS_PATH = "Config/game_options.xml";

void OptionManager::startUp(void)
{
	loadFile();
}

void OptionManager::shutDown(void)
{

}

void OptionManager::reload()
{
	shutDown();
	startUp();
}

void OptionManager::update(f64 dt)
{

}

void OptionManager::loadFile(const std::string& a_filePath)
{
	edv::tree_from_xml_file(m_options, a_filePath, "GameOptions");
}

void OptionManager::saveFile(const std::string& a_filePath)
{
	edv::tree_to_xml_file(a_filePath, "GameOptions", m_options);
}

const edv::any& OptionManager::getOption(const std::string& a_path) const
{
	try
	{
		return m_options.get(a_path);
	}
	catch (const Options::tree_exception& e)
	{
		const std::string error = "error getting option [" + a_path + "]: " + e.what();
		LOG(LOG_ERROR, LOGSUB_ENGINE, error.c_str());

		throw std::runtime_error{error};
	}
}

void OptionManager::setOption(const std::string& a_path, const edv::any& a_value)
{
	edv::any& option{ m_options.add_path(a_path, a_value) };

	// notify option change
	auto it = m_callbacks.find(a_path);

	if (it != m_callbacks.end())
	{
		const CallbackInfoList& optionCallbacks = it->second;

		for (auto& callbackInfo : optionCallbacks)
		{
			const Callback& callback = callbackInfo.callback;

			if (callback)
			{
				callback(a_path, option);
			}
		}
	}
}

OptionManager::Category OptionManager::getCategory(const std::string& a_path)
{
	Category category;

	try
	{
		Options::tree_type& tree = m_options.get_tree_ref(a_path);
		Options::keys_type optionNames = tree.keys();

		for (const auto& optionName : optionNames)
		{
			OptionPair op;
			op.first = optionName;
			op.second = tree[optionName];

			category.push_back(op);
		}
	}
	catch (const Options::tree_exception& e)
	{
		const std::string error = "error getting option [" + a_path + "]: " + e.what();
		LOG(LOG_ERROR, LOGSUB_ENGINE, error.c_str());

		throw std::runtime_error{ error };
	}

	return category;
}

OptionManager::CallbackId OptionManager::registerCallback(const std::string& a_path, const Callback& a_callback)
{
	const CallbackId id = m_callbackIdGenerator.nextId();

	m_callbacks[a_path].push_back(CallbackInfo{ id, a_callback });

	return id;
}

void OptionManager::unregisterCallback(CallbackId a_id)
{
	for (auto it = m_callbacks.begin(); it != m_callbacks.end(); it++)
	{
		CallbackInfoList& optionCallbacks = it->second;
		bool found = false;

		for (auto it_c = optionCallbacks.begin(); !found && it_c != optionCallbacks.end(); )
		{
			const CallbackInfo& callbackInfo = (*it_c);

			if (callbackInfo.id == a_id)
			{
				it_c = optionCallbacks.erase(it_c);
				found = true;
			}
			else
			{
				it_c++;
			}
		}
	}
}
