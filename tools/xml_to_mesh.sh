#!/bin/bash
#

PATH_TO_OGRE_XML_CONVERTER=''
PATH_TO_OGRE_XML_UPGRADER=''
PATH_TO_GAME_MEDIA_DIRECTORY=''

for item in `ls | grep "mesh.xml$"`; do
	$PATH_TO_OGRE_XML_CONVERTER "$item"
	$PATH_TO_OGRE_XML_UPGRADER "$item"
done

for item in `ls | grep ".material$"`; do
	cp "$item" ${PATH_TO_GAME_MEDIA_DIRECTORY}/graphics/materials
done

for item in `ls | grep ".mesh$"`; do
	cp "$item" ${PATH_TO_GAME_MEDIA_DIRECTORY}/graphics/models
done
