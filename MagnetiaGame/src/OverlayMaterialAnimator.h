#ifndef MAGNETIA_GAME_OVERLAY_MATERIAL_ANIMATOR_H_
#define MAGNETIA_GAME_OVERLAY_MATERIAL_ANIMATOR_H_

#include <string>
#include <Core/CBasicTypes.h>

namespace Ogre
{
	class OverlayContainer;
}

class OverlayMaterialAnimator
{
public:
	OverlayMaterialAnimator(Ogre::OverlayContainer& a_overlayContainer, const std::string& a_newMaterialName, f64 a_duration);
	~OverlayMaterialAnimator();
	void start();
	void stop();
	f64 elapsed() const;
	bool isActive() const;
	void update(f64 dt);

private:
	Ogre::OverlayContainer*		m_overlayContainer;
	std::string					m_basicMaterialName;
	std::string					m_newMaterialName;
	f64							m_duration;
	f64							m_startTimestamp;
};

#endif