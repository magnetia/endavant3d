#include "CMainGameState.h"
#include <Core/CCoreEngine.h>
#include <Renderer/CRenderManager.h>
#include <Renderer/CRenderDebug.h>
#include <Physics/CPhysicsManager.h>
#include <Input/CInputManager.h>
#include <State/CMainStateManager.h>
#include <Game/GameManager.h>
#include <Game/GameEntity.h>
#include "PausedGameState.h"
#include "MainMenuState.h"
#include "GameLevelSelector.h"

#include "Actors/Player.h"
#include "Actors/Nik.h"
#include "Actors/Clashnik.h"
#include "Actors/Minsect.h"
#include "Actors/Toweronic.h"
#include "Actors/Platform.h"
#include "Actors/Magnet.h"
#include "Actors/NeutralBall.h"
#include "Actors/Aplasta.h"
#include "Actors/LethalBarrier.h"
#include "Actors/Smashnik.h"
#include "Actors/Impulse.h"
#include "Actors/UnstablePlatform.h"
#include "Actors/FallingPlatform.h"
#include "Actors/Spaceship.h"
#include "Actors/Boss.h"
#include "Actors/Mine.h"
#include "Actors/Missile.h"
#include "Actors/TriggerGameEnd.h"

#include "Bindings/NikBinding.h"
#include "Bindings/ClashnikBinding.h"
#include "Bindings/MinsectBinding.h"
#include "Bindings/ToweronicBinding.h"
#include "Bindings/ToweronicProjectileBinding.h"
#include "Bindings/NeutralBallBinding.h"
#include "Bindings/MagnetiaActorBinding.h"
#include "Bindings/GameLevelBinding.h"
#include "Bindings/AplastaBinding.h"
#include "Bindings/LethalBarrierBinding.h"
#include "Bindings/SmashnikBinding.h"
#include "Bindings/ImpulseBinding.h"
#include "Bindings/UnstablePlatformBinding.h"
#include "Bindings/FallingPlatformBinding.h"
#include "Bindings/PlayerBinding.h"
#include "Bindings/BossBinding.h"
#include "Bindings/MineBinding.h"
#include "Bindings/TriggerGameEndBinding.h"

// #todo: passar coses d'aqui al nivell?

CMainGameState::CMainGameState() :
	CState("MainGameState")
{
	// Set debug mode and disable it
	auto& physics = CCoreEngine::Instance().GetPhysicsManager();
	physics.SetDebugMode(CPhysicsManager::EDV_DBG_DrawWireframe);
	physics.SwitchDebugModeEnabled();

	auto& keyboard = CCoreEngine::Instance().GetInputManager().GetKeyboard();
	auto& optionMgr = CCoreEngine::Instance().GetOptionManager();

	if (optionMgr.getOption("game/development").as<bool>())
	{
		keyboard.InsertKeyAction("RenderInfo", "F1");
		keyboard.InsertKeyAction("RenderPolyMode", "F2");
		keyboard.InsertKeyAction("BulletDebug", "F3");
		keyboard.InsertKeyAction("ReloadMapRaw", "F5");
		
		keyboard.InsertKeyAction("Glow", "0");
		
		keyboard.InsertKeyAction("ScenarioVisible", "1");
		keyboard.InsertKeyAction("ActorsVisible", "2");
		keyboard.InsertKeyAction("GodMode", "G");
	}
	
	keyboard.InsertKeyAction("ReloadMap", "F6");
	keyboard.InsertKeyAction("Pause", "Pause");
	keyboard.InsertKeyAction("SelectLevel", "F7");

	// Register components that will appear in the level
	auto& game = CCoreEngine::Instance().GetGameManager();
	game.registerDefaultActor(new MagnetiaActor::Factory);
	game.registerComponent(GameManager::COMPONENT_TYPE_ACTOR, "Nik", new Nik::Factory);
	game.registerComponent(GameManager::COMPONENT_TYPE_ACTOR, "Clashnik", new Clashnik::Factory);
	game.registerComponent(GameManager::COMPONENT_TYPE_ACTOR, "Minsect", new Minsect::Factory);
	game.registerComponent(GameManager::COMPONENT_TYPE_ACTOR, "Toweronic", new Toweronic::Factory);
	game.registerComponent(GameManager::COMPONENT_TYPE_ACTOR, "ToweronicProjectile", new ToweronicProjectile::Factory);
	game.registerComponent(GameManager::COMPONENT_TYPE_ACTOR, "Platform", new Platform::Factory);
	game.registerComponent(GameManager::COMPONENT_TYPE_ACTOR, "Magnet", new Magnet::Factory);
	game.registerComponent(GameManager::COMPONENT_TYPE_ACTOR, "NeutralBall", new NeutralBall::Factory);
	game.registerComponent(GameManager::COMPONENT_TYPE_ACTOR, "Aplasta", new Aplasta::Factory);
	game.registerComponent(GameManager::COMPONENT_TYPE_ACTOR, "LethalBarrier", new LethalBarrier::Factory);
	game.registerComponent(GameManager::COMPONENT_TYPE_ACTOR, "Smashnik", new Smashnik::Factory);
	game.registerComponent(GameManager::COMPONENT_TYPE_ACTOR, "Impulse", new Impulse::Factory);
	game.registerComponent(GameManager::COMPONENT_TYPE_ACTOR, "UnstablePlatform", new UnstablePlatform::Factory);
	game.registerComponent(GameManager::COMPONENT_TYPE_ACTOR, "FallingPlatform", new FallingPlatform::Factory);
	game.registerComponent(GameManager::COMPONENT_TYPE_ACTOR, "Spaceship", new Spaceship::Factory);
	game.registerComponent(GameManager::COMPONENT_TYPE_ACTOR, "Boss", new Boss::Factory);
	game.registerComponent(GameManager::COMPONENT_TYPE_ACTOR, "Mine", new Mine::Factory);
	game.registerComponent(GameManager::COMPONENT_TYPE_ACTOR, "Missile", new Missile::Factory);
	game.registerComponent(GameManager::COMPONENT_TYPE_ACTOR, "TriggerGameEnd", new TriggerGameEnd::Factory);

	// #todo: actors register bindings automatically
	// Register bindings
	auto& script = CCoreEngine::Instance().GetScriptManager();
	script.registerScope(toLuaClass<MagnetiaActor>());
	script.registerScope(toLuaClass<Player>());
	script.registerScope(toLuaClass<Nik>());
	script.registerScope(toLuaClass<Clashnik>());
	script.registerScope(toLuaClass<Minsect>());
	script.registerScope(toLuaClass<Toweronic>());
	script.registerScope(toLuaClass<ToweronicProjectile>());
	script.registerScope(toLuaClass<NeutralBall>());
	script.registerScope(toLuaClass<Aplasta>());
	script.registerScope(toLuaClass<LethalBarrier>());
	script.registerScope(toLuaClass<Smashnik>());
	script.registerScope(toLuaClass<Impulse>());
	script.registerScope(toLuaClass<GameLevel>());
	script.registerScope(toLuaClass<UnstablePlatform>());
	script.registerScope(toLuaClass<FallingPlatform>());
	script.registerScope(toLuaClass<Boss>());
	script.registerScope(toLuaClass<Mine>());
	script.registerScope(toLuaClass<TriggerGameEnd>());
}

CMainGameState::~CMainGameState()
{

}

void CMainGameState::Start()
{
	// select first level
	auto levels = GameLevelSelector::Pointer()->getLevels();
	GameLevelSelector::Pointer()->loadLevel(GameLevelSelector::Pointer()->m_levelid);
}

void CMainGameState::Finish()
{
	GameLevelSelector::Pointer()->unloadLevel();
	auto scene_mgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();
}

void CMainGameState::Pause()
{
	GameLevelSelector::Pointer()->pause();
}

void CMainGameState::Resume()
{
	GameLevelSelector::Pointer()->resume();
}

void CMainGameState::Update(f64 dt)
{
			
	GameLevelSelector::Pointer()->update(dt);

	if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("ReloadMapRaw"))
	{
		GameLevelSelector::Pointer()->reloadLevelRaw();
	}

	else if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("ReloadMap"))
	{
		GameLevelSelector::Pointer()->reloadLevel();
	}

	else if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("RenderInfo"))
	{
		//Show Render information Debug
		auto &render_mgr = CCoreEngine::Instance().GetRenderManager();
		render_mgr.SwitchRenderInformation();
	}

	else if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("RenderPolyMode"))
	{
		//Show Render information Debug
		auto &render_mgr = CCoreEngine::Instance().GetRenderManager();
		render_mgr.SwitchRenderMode();
	}

	else if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("BulletDebug"))
	{
		CCoreEngine::Instance().GetPhysicsManager().SwitchDebugModeEnabled();
	}

	else if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("Pause"))
	{
		CCoreEngine::Instance().GetMainStateManager().PushState(PausedGameState::Pointer());
	}

	else if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("ScenarioVisible"))
	{
		static bool scenarioVisible = false;
		auto &game = CCoreEngine::Instance().GetGameManager();
		static_cast<GameLevel*>(game.getCurrentLevel())->setVisibleLevelChildSceneNode(GameLevel::eLevelNode::SCENARIO_NODE, scenarioVisible);
		scenarioVisible = !scenarioVisible;
	}

	else if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("ActorsVisible"))
	{
		static bool actorsVisible = false;
		auto &game = CCoreEngine::Instance().GetGameManager();
		static_cast<GameLevel*>(game.getCurrentLevel())->setVisibleLevelChildSceneNode(GameLevel::eLevelNode::ACTORS_NODE, actorsVisible);
		actorsVisible = !actorsVisible;
	}

	else if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("GodMode"))
	{
		static bool goodMode = true;
		auto &game = CCoreEngine::Instance().GetGameManager();
		static_cast<Player*>(game.getPlayer()->getActor())->setGodMode(goodMode);
		goodMode = !goodMode;
	}

	else if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("SelectLevel"))
	{
		MainMenuState::Pointer()->showMenu("/MainMenu/MenuSelectLevel");
		CCoreEngine::Instance().GetMainStateManager().ChangeState(MainMenuState::Pointer());
	}
	
	else
	{
		auto &game = CCoreEngine::Instance().GetGameManager();
		auto currentLevel = static_cast<GameLevel*>(game.getCurrentLevel());
		if (currentLevel && currentLevel->isLevelWon())
		{
			GameLevelSelector::LevelList levelList = GameLevelSelector::Pointer()->getLevels();
			GameLevelSelector::LevelList::iterator currentPos = std::find(levelList.begin(), levelList.end(), GameLevelSelector::Pointer()->m_levelid);

			if (currentPos != levelList.end())
			{
				currentPos++;

				if (currentPos != levelList.end())
				{
					GameLevelSelector::Pointer()->m_levelid = (*currentPos);
					CCoreEngine::Instance().GetMainStateManager().ChangeState(this);
				}
				else
				{
					MainMenuState::Pointer()->showMenu("/MainMenu/MenuSelectLevel");
					CCoreEngine::Instance().GetMainStateManager().ChangeState(MainMenuState::Pointer());
				}
			}
		}
	}
}
