#ifndef MAGNETIA_ACTOR_H_
#define MAGNETIA_ACTOR_H_

#include <Script/ScriptManager.h>
#include "Actors/MagnetiaActor.h"

template <> inline luabind::scope toLuaClass<MagnetiaActor>()
{
	using namespace luabind;
	return
		class_<MagnetiaActor, bases<Actor>>("MagnetiaActor")
			.def("setOrientation", &MagnetiaActor::setOrientation)
			.def("getOrientation", &MagnetiaActor::getOrientation);
}

#endif
