#ifndef EDV_SCRIPT_BOSS_BINDING_H_
#define EDV_SCRIPT_BOSS_BINDING_H_

#include <Script/ScriptManager.h>
#include "Actors/Boss.h"

template <> inline luabind::scope toLuaClass<Boss>()
{
	using namespace luabind;
	return
		class_<Boss, bases<MagnetiaActor>>("Boss")
		;
}

#endif
