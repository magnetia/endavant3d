#include "Script/Util.h"
#include "ImpulseBinding.h"

luabind::object	getImpulseVector(Impulse* a_impulse)
{
	if (a_impulse)
	{
		return ogreVector3ToLua(a_impulse->getImpulseVector().to_OgreVector3());
	}
	else
	{
		throw std::runtime_error{ "invalid parameter" };
	}
}
