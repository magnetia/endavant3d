#ifndef MAGNETIA_Mine_BINDING_H_
#define MAGNETIA_Mine_BINDING_H_

#include <Script/ScriptManager.h>
#include "Actors/Mine.h"

template <> inline luabind::scope toLuaClass<Mine>()
{
	using namespace luabind;
	return
		class_<Mine, bases<MagnetiaActor>>("Mine")
			.def("explode", &Mine::explode)
			.def("getBossEntity", &Mine::getBossEntity);
}

#endif
