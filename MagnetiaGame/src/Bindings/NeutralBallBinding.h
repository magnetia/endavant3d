#ifndef MAGNETIA_NEUTRAL_BALL_BINDING_H_
#define MAGNETIA_NEUTRAL_BALL_BINDING_H_

#include <Script/ScriptManager.h>
#include "Actors/NeutralBall.h"

template <> inline luabind::scope toLuaClass<NeutralBall>()
{
	using namespace luabind;
	return
		class_<NeutralBall, bases<MagnetiaActor>>("NeutralBall")
			.def("getAnimationDuration", &NeutralBall::getAnimationDuration)
			.def("playGetAnimation", &NeutralBall::playGetAnimation)
			.def("isPlayingAnimation", &NeutralBall::isPlayingAnimation);
}

#endif
