#ifndef EDV_SCRIPT_FALLING_PLATFORM_BINDING_H_
#define EDV_SCRIPT_FALLING_PLATFORM_BINDING_H_

#include <Script/ScriptManager.h>
#include "Actors/FallingPlatform.h"

template <> inline luabind::scope toLuaClass<FallingPlatform>()
{
	using namespace luabind;
	return
		class_<FallingPlatform, bases<MagnetiaActor>>("FallingPlatform")
			.def("playFalling", &FallingPlatform::playFalling)
			.def("getTimeInActive", &FallingPlatform::getTimeInActive)
			.def("getTimeInFalling", &FallingPlatform::getTimeInFalling)
			.def("getTimeInInactive", &FallingPlatform::getTimeInInactive);
}

#endif
