#ifndef MAGNETIA_TOWERONIC_PROJECTILE_BINDING_H_
#define MAGNETIA_TOWERONIC_PROJECTILE_BINDING_H_

#include <Script/ScriptManager.h>
#include "Actors/ToweronicProjectile.h"

namespace ToweronicProjectileBinding
{
	s32 getCharge(ToweronicProjectile* a_projectile);
}

template <> inline luabind::scope toLuaClass<ToweronicProjectile>()
{
	using namespace luabind;
	return
		class_<ToweronicProjectile, bases<MagnetiaActor>>("ToweronicProjectile")
			.def("destroy", &ToweronicProjectile::destroy)
			.def("getToweronicEntity", &ToweronicProjectile::getToweronicEntity)
			.def("setInactive", &ToweronicProjectile::setInactive)
			.def("grow", &ToweronicProjectile::grow)
			.def("getCharge", &ToweronicProjectileBinding::getCharge);
}

#endif
