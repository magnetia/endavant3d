#ifndef MAGNETIA_TRIGGER_GAME_END_BINDING_H_
#define MAGNETIA_TRIGGER_GAME_END_BINDING_H_

#include <Script/ScriptManager.h>
#include "Actors/TriggerGameEnd.h"

template <> inline luabind::scope toLuaClass<TriggerGameEnd>()
{
	using namespace luabind;
	return
		class_<TriggerGameEnd, bases<MagnetiaActor>>("TriggerGameEnd")
			.enum_("Substate")
			[
				value("SUBSTATE_WAITING", 0),
				value("SUBSTATE_FADE_OFF", 1),
				value("SUBSTATE_STORY", 2)
			]

			.def("getSubstate", &TriggerGameEnd::getSubstate)
			.def("startStory", &TriggerGameEnd::startStory);
}

#endif
