#include <Script/Util.h>
#include "AplastaBinding.h"

luabind::object	getOriginPosition(Aplasta* a_aplasta)
{
	return ogreVector3ToLua(a_aplasta->getOriginPosition().to_OgreVector3());
}

luabind::object	getOnGroundPosition(Aplasta* a_aplasta)
{
	return ogreVector3ToLua(a_aplasta->getGroundPosition().to_OgreVector3());
}
