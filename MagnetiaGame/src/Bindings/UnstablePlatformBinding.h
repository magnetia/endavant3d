#ifndef EDV_SCRIPT_UNSTABLE_PLATFORM_BINDING_H_
#define EDV_SCRIPT_UNSTABLE_PLATFORM_BINDING_H_

#include <Script/ScriptManager.h>
#include "Actors/UnstablePlatform.h"

template <> inline luabind::scope toLuaClass<UnstablePlatform>()
{
	using namespace luabind;
	return
		class_<UnstablePlatform, bases<MagnetiaActor>>("UnstablePlatform")
			.def("playAdvise", &UnstablePlatform::playAdvise)
			.def("getTimeInActive", &UnstablePlatform::getTimeInActive)
			.def("getTimeInAdvise", &UnstablePlatform::getTimeInAdvise)
			.def("getTimeInInactive", &UnstablePlatform::getTimeInInactive);
}

#endif
