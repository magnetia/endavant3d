#ifndef EDV_SCRIPT_LETHAL_BARRIER_BINDING_H_
#define EDV_SCRIPT_LETHAL_BARRIER_BINDING_H_

#include <Script/ScriptManager.h>
#include "Actors/LethalBarrier.h"

template <> inline luabind::scope toLuaClass<LethalBarrier>()
{
	using namespace luabind;
	return
		class_<LethalBarrier, bases<MagnetiaActor>>("LethalBarrier")
			.def("getOffTime", &LethalBarrier::getOffTime)
			.def("getOnTime", &LethalBarrier::getOnTime)
			.def("showRay", &LethalBarrier::showRay)
			.def("isShowingRay", &LethalBarrier::isShowingRay);
}

#endif
