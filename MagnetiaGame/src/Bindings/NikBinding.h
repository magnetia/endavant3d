#ifndef MAGNETIA_NIK_BINDING_H_
#define MAGNETIA_NIK_BINDING_H_

#include <Script/ScriptManager.h>
#include "Actors/Nik.h"

template <> inline luabind::scope toLuaClass<Nik>()
{
	using namespace luabind;
	return
		class_<Nik, bases<Player>>("Nik")
		.def("setRayRadius", &Nik::setRayRadius);

}

#endif
