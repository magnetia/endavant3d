#ifndef MAGNETIA_GAME_LEVEL_BINDING_H_
#define MAGNETIA_GAME_LEVEL_BINDING_H_

#include <Script/ScriptManager.h>
#include "GameLevel.h"

template <> inline luabind::scope toLuaClass<GameLevel>()
{
	using namespace luabind;
	return
		class_<GameLevel, bases<Level>>("GameLevel")
			.def("destroyItem", &GameLevel::destroyItem)
			.def("destroyEnemy", &GameLevel::destroyEnemy)
			.def("setPlayerAtEnd", &GameLevel::setPlayerAtEnd)
			.def("isPlayerAtEnd", &GameLevel::isPlayerAtEnd)
			.def("getCurrentNeutralBalls", &GameLevel::getCurrentNeutralBalls);
}

#endif
