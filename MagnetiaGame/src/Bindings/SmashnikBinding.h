#ifndef EDV_SCRIPT_SMASHNIK_BINDING_H_
#define EDV_SCRIPT_SMASHNIK_BINDING_H_

#include <Script/ScriptManager.h>
#include "Actors/Smashnik.h"

template <> inline luabind::scope toLuaClass<Smashnik>()
{
	using namespace luabind;
	return
		class_<Smashnik, bases<MagnetiaActor>>("Smashnik")
			.def("startUp", &Smashnik::startUp)
			.def("shutdown", &Smashnik::shutdown)
			.def("onGround", &Smashnik::onGround)
			.def("collidingLeft", &Smashnik::collidingLeft)
			.def("collidingRight", &Smashnik::collidingRight)
			.def("move", &Smashnik::move)
			.def("getGravity", &Smashnik::getGravity)
			.def("finishAnimation", &Smashnik::finishAnimation)
			.def("showParticles", &Smashnik::showParticles)
			.def("checkOrientation", &Smashnik::checkOrientation);
}

#endif
