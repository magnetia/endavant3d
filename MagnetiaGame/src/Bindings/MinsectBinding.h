#ifndef MAGNETIA_MINSECT_BINDING_H_
#define MAGNETIA_MINSECT_BINDING_H_

#include <Script/ScriptManager.h>
#include "Actors/Minsect.h"

template <> inline luabind::scope toLuaClass<Minsect>()
{
	using namespace luabind;
	return
		class_<Minsect, bases<MagnetiaActor>>("Minsect")
			.def("show", &Minsect::show)
			.def("attack", &Minsect::attack)
			.def("dieAnimation", &Minsect::dieAnimation);
}

#endif
