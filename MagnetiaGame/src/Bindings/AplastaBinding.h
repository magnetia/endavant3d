#ifndef EDV_SCRIPT_APLASTA_BINDING_H_
#define EDV_SCRIPT_APLASTA_BINDING_H_

#include <Script/ScriptManager.h>
#include "Actors/Aplasta.h"

luabind::object	getOriginPosition(Aplasta* a_aplasta);
luabind::object	getOnGroundPosition(Aplasta* a_aplasta);

template <> inline luabind::scope toLuaClass<Aplasta>()
{
	using namespace luabind;
	return
		class_<Aplasta, bases<MagnetiaActor>>("Aplasta")
			.def("goUp", &Aplasta::goUp)
			.def("goDown", &Aplasta::goDown)
			.def("getUpWaitTime", &Aplasta::getUpWaitTime)
			.def("getDownTime", &Aplasta::getDownTime)
			.def("getDownWaitTime", &Aplasta::getDownWaitTime)
			.def("getUpTime", &Aplasta::getUpTime)
			.def("getChangingTime", &Aplasta::getChangingTime)
			.def("getOriginPosition", &getOriginPosition)
			.def("getGroundPosition", &getOnGroundPosition)
			.def("playCrushAnimation", &Aplasta::playCrushAnimation);
}

#endif
