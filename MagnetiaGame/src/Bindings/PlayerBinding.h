#ifndef MAGNETIA_GAME_PLAYER_BINDING_H_
#define MAGNETIA_GAME_PLAYER_BINDING_H_

#include <Script/ScriptManager.h>
#include "Actors/Player.h"

template <> inline luabind::scope toLuaClass<Player>()
{
	using namespace luabind;
	return
		class_<Player, bases<MagnetiaActor>>("Player")
		.def("getNeutralizerBalls", &Player::getNeutralizerBalls)
		.def("setNeutralizerBalls", &Player::setNeutralizerBalls);
}

#endif
