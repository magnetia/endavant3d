#ifndef MAGNETIA_IMPULSE_BINDING_H_
#define MAGNETIA_IMPULSE_BINDING_H_

#include <Script/ScriptManager.h>
#include "Actors/Impulse.h"

luabind::object	getImpulseVector(Impulse* a_impulse);

template <> inline luabind::scope toLuaClass<Impulse>()
{
	using namespace luabind;
	return
		class_<Impulse, bases<MagnetiaActor>>("Impulse")
			.def("getImpulseVector", &getImpulseVector)
			.def("ejectPlayer", &Impulse::ejectPlayer);
}

#endif
