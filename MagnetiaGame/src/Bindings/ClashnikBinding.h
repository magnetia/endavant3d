#ifndef EDV_SCRIPT_CLASHNIK_BINDING_H_
#define EDV_SCRIPT_CLASHNIK_BINDING_H_

#include <Script/ScriptManager.h>
#include "Actors/Clashnik.h"

template <> inline luabind::scope toLuaClass<Clashnik>()
{
	using namespace luabind;
	return
		class_<Clashnik, bases<MagnetiaActor>>("Clashnik")
			.def("setSpeed", &Clashnik::setSpeed)
			.def("setOrientation", &Clashnik::setOrientation)
			.def("getSpeed", &Clashnik::getSpeed)
			.def("getClashnikOrientation", &Clashnik::getClashnikOrientation)
			.def("isStuck", &Clashnik::isStuck);
}

#endif
