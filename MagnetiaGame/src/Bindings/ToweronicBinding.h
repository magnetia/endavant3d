#ifndef EDV_SCRIPT_TOWERONIC_BINDING_H_
#define EDV_SCRIPT_TOWERONIC_BINDING_H_

#include <Script/ScriptManager.h>
#include "Actors/Toweronic.h"

template <> inline luabind::scope toLuaClass<Toweronic>()
{
	using namespace luabind;
	return
		class_<Toweronic, bases<MagnetiaActor>>("Toweronic")
			.def("target", &Toweronic::target)
			.def("targetPlayer", &Toweronic::targetPlayer)
			.def("shoot", &Toweronic::shoot)
			.def("shootPlayer", &Toweronic::shootPlayer)
			.def("canSeePlayer", &Toweronic::canSeePlayer)
			.def("getActionRadius", &Toweronic::getActionRadius);
}

#endif
