#include <Game/GameEntity.h>
#include "ToweronicProjectileBinding.h"

namespace ToweronicProjectileBinding
{
	s32 getCharge(ToweronicProjectile* a_projectile)
	{
		s32 charge = static_cast<s32>(MagneticProperty::MAGNET_TYPE_NEUTRAL);

		if (a_projectile)
		{
			charge = static_cast<s32>(a_projectile->getCharge());
		}

		return charge;
	}
}
