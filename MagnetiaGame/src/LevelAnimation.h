#ifndef MAGNETIA_GAME_LEVEL_ANIMATION_H_
#define MAGNETIA_GAME_LEVEL_ANIMATION_H_

#include <memory>
#include <vector>
#include <string>
#include <Core/CBasicTypes.h>
#include <Panel.h>

class LevelAnimation
{
public:
	LevelAnimation(const std::string& a_name);
	virtual ~LevelAnimation();

	virtual void start() final;
	virtual void update(f64 dt);
	virtual bool finished() const;

protected:
	virtual void startImpl() = 0;
	virtual void onAnimationStart(Panel::Animation* a_animation) { }
	virtual void onAnimationEnd(Panel::Animation* a_animation) { }

	typedef std::unique_ptr<Panel>	PanelPtr;
	typedef std::vector<PanelPtr>	Panels;

	void addPanel(Panel* a_panel);
	void createFont(const std::string& a_name, const std::string& a_type, const std::string& a_source, s32 a_size, s32 a_resolution);

	std::string		m_name;
	Ogre::Overlay*	m_overlay;
	Panels			m_panels;
};

#endif
