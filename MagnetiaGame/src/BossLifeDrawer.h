#ifndef MAGNETIA_GAME_BOSS_LIFE_DRAWER_H_
#define MAGNETIA_GAME_BOSS_LIFE_DRAWER_H_

#include <memory>
#include <Core/CBasicTypes.h>
#include <Ogre.h>
#include "Panel.h"
#include "HudElement.h"

class Boss;

class BossLifeDrawer : public HudElement
{
public:
	BossLifeDrawer();
	~BossLifeDrawer();

	virtual void start() override;
	virtual void update(f64 dt) override;
	virtual void stop() override;
	virtual tPanelsVector getPanels() const override;

private:
	typedef std::unique_ptr<Panel>	PanelPtr;

	bool			m_running;
	PanelPtr		m_lifePanel;
	s32				m_lastLifeValue;
	Boss*			m_boss;
	Ogre::Vector2	m_barMaxSize;
	Ogre::Vector2	m_startPosition;
};

#endif
