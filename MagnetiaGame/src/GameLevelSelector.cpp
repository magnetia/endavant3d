#include "GameLevelSelector.h"
#include <Core/CLogManager.h>
#include <Options/OptionManager.h>
#include <Sound/CSoundManager.h>
#include <Utils/CXMLParserPUGI.h>
#include <Game/GameManager.h>

#include <Renderer/CRenderManager.h>
#include "GameLevelTiledParser.h"



GameLevelSelector::GameLevelSelector()
{
	
	m_tiled_parser = std::make_shared<GameLevelTiledParser>();
	m_tiled_parser->ParseAllLevels();

	
}

GameLevelSelector::~GameLevelSelector()
{
	
}

GameLevelSelector::LevelList GameLevelSelector::getLevels() const
{
	LevelList ret;
	for (auto level : m_tiled_parser->GetAllLevelsXMLInfo())
		ret.push_back(level->name);
		
	return ret;
}

GameLevelTiledParser::tGameLevelXMLInfo GameLevelSelector::getLevelsXMLinfo() const
{
	return m_tiled_parser->GetAllLevelsXMLInfo();
}

void GameLevelSelector::loadLevel(const std::string& a_level_name)
{
	if (!m_tiled_parser->ExistsLevel(a_level_name))
	{
		throw std::runtime_error(a_level_name + " not exists!");
	}
	else
	{
		// Guardem el nom del nivell actual
		std::string lastLevelName;
		if (m_current_level)
		{
			lastLevelName = m_current_level->first;
			unloadLevel();
		}

		// Creo l'objecte del nivell
		auto levelobj = std::make_shared<GameLevel>(m_tiled_parser->GetLevelRawInfo(a_level_name));
		auto cur_lvl = std::make_pair(a_level_name, levelobj);
		m_current_level = std::make_shared<tCurLevelInfo>(cur_lvl);

		if (!m_tiled_parser->isLevelSelectable(m_current_level->first))
		{
			m_tiled_parser->setLevelSelectable(m_current_level->first, true);
			m_tiled_parser->saveGameLevelsInfoToXML();
		}

		// Mirem si hem de canviar de musica
		if (lastLevelName != m_current_level->first)
		{
			if (!lastLevelName.empty())
			{
				const GameLevelTiledParser::tGameLevelRawInfo& oldLevelRawInfo{ m_tiled_parser->GetLevelRawInfo(lastLevelName) };
				if (oldLevelRawInfo)
				{
					CCoreEngine::Instance().GetSoundManager().stopMusic();
				}
			}
			
			const GameLevelTiledParser::tGameLevelRawInfo& newLevelRawInfo{ m_tiled_parser->GetLevelRawInfo(m_current_level->first) };
			CCoreEngine::Instance().GetSoundManager().playMusic(newLevelRawInfo->m_backgroundmusic);
		}
	}
}

std::shared_ptr<const GameLevel> GameLevelSelector::getCurrentLevel() const
{
	if (m_current_level)
		return m_current_level->second;
	else
	{
		std::shared_ptr<const GameLevel> ptr;
		return ptr;
	}

}

void GameLevelSelector::reloadLevel()
{
	CCoreEngine::Instance().GetPhysicsManager().Reset();
	auto lvlname = m_current_level->first;
	loadLevel(lvlname);
}

void GameLevelSelector::reloadLevelRaw()
{

	auto lvlname = m_current_level->first;
	m_tiled_parser->ParseLevel(lvlname); //Llegim de nou el fitxer de tiled
	
	// reload confiruation
	CCoreEngine::Instance().GetOptionManager().reload();
	CCoreEngine::Instance().GetSoundManager().reloadConfiguration();
	CCoreEngine::Instance().GetGameManager().reloadEntityConfiguration();
	
	CCoreEngine::Instance().GetPhysicsManager().Reset();
	
	unloadLevel();
	loadLevel(lvlname);
}

void GameLevelSelector::unloadLevel()
{
	if (m_current_level)
	{
		CCoreEngine::Instance().GetGameManager().unregisterLevel(m_current_level->first);
		m_current_level.reset();
	}
	
}

void GameLevelSelector::pause()
{
	if (m_current_level)
		m_current_level->second->pause();
}

void GameLevelSelector::resume()
{
	if (m_current_level)
		m_current_level->second->resume();
}

void GameLevelSelector::update(f64 dt)
{
	if (m_current_level)
		m_current_level->second->update(dt);

	if (m_current_level->second->isLevelLost())
	{
		// Restart level
		reloadLevel();
	}
}

