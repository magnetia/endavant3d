#include <Core/CCoreEngine.h>
#include <Core/CLogManager.h>
#include <Camera/CCameraControllerFree.h>
#include <Camera/CCameraFollower.h>
#include <Camera/MagnetiaCamera.h>
#include <Camera/CameraManager.h>
#include <Input/CInputManager.h>
#include <Sound/CSoundManager.h>
#include <Game/GameEntity.h>
#include <Game/GameManager.h>
#include <Game/AlphaTransitionAnimator.h>
#include <Game/ChainedBehavior.h>
#include <Externals/Glow/GlowMaterialListener.h>
#include "GameLevel.h"
#include "StartAnimation.h"
#include "LoseAnimation.h"
#include "WinAnimation.h"
#include "Actors/Nik.h"

#include "Timer.h"
#include "Counter.h"
#include "BossLifeDrawer.h"
#include "Help.h"


//Tile and tile layer properties
const static std::string GAMELEVEL_LAYER_TRANSLATIONZ_TAG{ "translationZ" };

const static std::string GAMELEVEL_TYPE_TAG{ "type" };
const static std::string GAMELEVEL_SUBTYPE_TAG{ "subtype" };

const static std::string GAMELEVEL_TYPE_STATICMESH_TAG{ "static_mesh" };
const static std::string GAMELEVEL_TYPE_MESH{ "level_mesh" };
const static std::string GAMELEVEL_MESH_TAG{ "mesh" };
const static std::string GAMELEVEL_ROTATIONX_TAG{ "rotationX" };
const static std::string GAMELEVEL_ROTATIONY_TAG{ "rotationY" };
const static std::string GAMELEVEL_ROTATIONZ_TAG{ "rotationZ" };
const static std::string GAMELEVEL_SCALEX_TAG{ "scaleX" };
const static std::string GAMELEVEL_SCALEY_TAG{ "scaleY" };
const static std::string GAMELEVEL_SCALEZ_TAG{ "scaleZ" };
const static std::string GAMELEVEL_MATERIAL_TAG{ "material" };

const static std::string GAMELEVEL_TYPE_ACTOR_TAG{ "actor" };
const static std::string GAMELEVEL_ENTITY_ID_TAG{ "entityID" };
const static std::string GAMELEVEL_ACTOR_SUBTYPE_ENEMY_TAG{ "enemy" };
const static std::string GAMELEVEL_ACTOR_SUBTYPE_PLAYER_TAG{ "player" };
const static std::string GAMELEVEL_ACTOR_SUBTYPE_ITEM_TAG{ "item" };

//Object layer properties
const static std::string GAMELEVEL_OBJ_COLLISIONS_LAYER_TAG{ "collision" };


static const std::string IDCAMERA_FREE("FreeCamera");
static const std::string IDCAMERA_PLAYER("PlayerCamera");
static const std::string IDCAMERA_GAME("GameCamera");

const static std::string IDNEUTRALLBALL{ "item" };

// Sound options
const static std::string SOUND_VOLUME_OPTION_PATH{ "audio/sound_volume" };
const static std::string MUSIC_VOLUME_OPTION_PATH{ "audio/music_volume" };
const static u32 MIN_SOUND_VOLUME{ 0 };
const static u32 MAX_SOUND_VOLUME{ 100 };

const static u32 MAX_ENTITIES_PER_STATIC_MESH{ 500 };

GameLevel::GameLevel(const GameLevelTiledParser::tGameLevelRawInfo &a_lvlinfo) :
	m_tiledRawinfo{ a_lvlinfo },
	m_level_node{ nullptr },
	m_hud{ },
	m_player{ nullptr },
	m_player_at_end{ false },
	m_levelState{ LEVEL_STATE_NONE }
{
	init();
	load();
	CreateHud();
	CreateCameras();
	CreateCompositors();

	//Play de la musica d'inici
	CCoreEngine::Instance().GetSoundManager().playSound("start_level");
}

GameLevel::~GameLevel()
{
	CCoreEngine::Instance().GetSoundManager().stopAllSounds();
	DestroyCompositors();
	DestroyCameras();
	unload();
	deinit();
}

void GameLevel::init()
{
	CCoreEngine::Instance().GetGameManager().registerLevel(m_tiledRawinfo->m_mapname, this);

	m_animations = Animations{{ nullptr, new StartAnimation, nullptr, new LoseAnimation, new WinAnimation, nullptr, nullptr }};

	m_level_nodes.fill(nullptr);

	auto& keyboard = CCoreEngine::Instance().GetInputManager().GetKeyboard();
	auto& optionMgr = CCoreEngine::Instance().GetOptionManager();
	
	if (optionMgr.getOption("game/development").as<bool>())
	{
		keyboard.InsertKeyAction("ChangeCam", "C");
		keyboard.InsertKeyAction("SetInputEnableCam", "V");
		keyboard.InsertKeyAction("VolumeUp", "Keypad +");
		keyboard.InsertKeyAction("VolumeDown", "Keypad -");
	}

	auto scene_mgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();

	//Creem la iluminacio del nivell
	//ambient light
	scene_mgr->destroyAllLights();
	scene_mgr->setAmbientLight(Ogre::ColourValue(0.5f,0.5f,0.5f,1.0f));
	//scene_mgr->setAmbientLight(Ogre::ColourValue::Black);
	scene_mgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
	Ogre::Light* directionalLight = scene_mgr->createLight("directionalLight");
	directionalLight->setType(Ogre::Light::LT_DIRECTIONAL);
	
//	scene_mgr->setAmbientLight(Ogre::ColourValue{ 0.5f, 0.5f, 0.5f, 1.f });

	directionalLight->setDiffuseColour(Ogre::ColourValue(.50, .50, 0.5));
	directionalLight->setSpecularColour(Ogre::ColourValue(.50, .50, 0.5));
	directionalLight->setDirection(Ogre::Vector3(0.5, -1, -1 ));
}

void GameLevel::deinit()
{
	for (auto* animation : m_animations)
	{
		if (animation)
			delete animation;
	}

	CCoreEngine::Instance().GetGameManager().unregisterLevel(this);
}

void GameLevel::load()
{
	CreateLevel();
	CreateSkyBox();

	// build static geometry
	for (u32 i = 0; i < m_level_static_geometry.size(); i++)
	{
		m_level_static_geometry.at(i)->setCastShadows(true);
		m_level_static_geometry.at(i)->build();
		m_level_static_geometry.at(i)->setVisible(true);
	}
	

	m_levelState = LEVEL_STATE_START_ANIMATION;

	if (auto* animation = m_animations[m_levelState])
	{
		animation->start();
	}
}



void GameLevel::unload()
{	
	DestroyLevel();
	
	// Destruim la geometria estatica
	auto scene_mgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();
	for (u32 i = 0; i < m_level_static_geometry.size(); i++)
	{
		scene_mgr->destroyStaticGeometry("ScenarioGeometry" + to_string(i)+ m_tiledRawinfo->m_mapname);
	}
	scene_mgr->setSkyBox(false, "skybox_level1");

	for (auto &meshrscname : m_meshresourcestoremove)
	{
		Ogre::MeshManager::getSingleton().remove(meshrscname);
	}
}



void GameLevel::CreateSkyBox()
{
	if (!m_tiledRawinfo->m_skyboxmaterialname.empty())
	{
		auto scene_mgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();
		scene_mgr->setSkyBox(true, m_tiledRawinfo->m_skyboxmaterialname);

		/*Ogre::ParticleSystem* sunPartic = scene_mgr->getParticleSystem("particles/test");
		Ogre::ParticleSystem* sunParticle = scene_mgr->createParticleSystem("testingparticle", "Atomic");
		Ogre::SceneNode* particleNode = scene_mgr->getRootSceneNode()->createChildSceneNode("ParticleTestingNode");
		particleNode->attachObject(sunParticle);*/
	}
}

void GameLevel::CreateHud()
{
	m_hudTimer.reset(new Timer);
	m_hud.addElement(m_hudTimer.get());
	
	if (getTotalNeutralBalls() > 0U)
	{
		m_hudCounter.reset(new Counter);
		m_hud.addElement(m_hudCounter.get());
	}

	if (CCoreEngine::Instance().GetGameManager().getBoss())
	{
		m_hudBossLife.reset(new BossLifeDrawer);
		m_hud.addElement(m_hudBossLife.get());
	}

	if (m_tiledRawinfo.get()->m_tutorial)
	{
		m_hudHelp.reset(new Help);
		m_hud.addElement(m_hudHelp.get());
	}

	m_hud.init();
}

void GameLevel::CreateCameras()
{	
	//Si no hi ha jugador no creem la camera del jugador
	if (getPlayerSceneNode() != nullptr)
	{
		auto& camMgr = CCoreEngine::Instance().GetCameraManager();

		camMgr.add_camera(new CCameraFree{ IDCAMERA_FREE });
		//camMgr.add_camera(new CCameraFollower{ IDCAMERA_PLAYER, GetPlayerSceneNode() });
		camMgr.add_camera(new MagnetiaCamera{ IDCAMERA_GAME });

		camMgr.set_active_camera(IDCAMERA_GAME);
	}
}

void GameLevel::CreateCompositors()
{
	// #todo: xml de nivells
	//m_compositors.push_back(CCoreEngine::Instance().GetRenderManager().addCompositor("Glow", new GlowMaterialListener));

	//m_compositors.push_back(CCoreEngine::Instance().GetRenderManager().addCompositor("B&W"));
	//m_compositors.push_back(CCoreEngine::Instance().GetRenderManager().addCompositor("Old Movie"));
	//m_compositors.push_back(CCoreEngine::Instance().GetRenderManager().addCompositor("Bloom"));
}

void GameLevel::DestroyCompositors()
{
	auto& renderMgr = CCoreEngine::Instance().GetRenderManager();

	for (auto compositorId : m_compositors)
	{
		renderMgr.removeCompositor(compositorId);
	}

	m_compositors.clear();
}

u32 GameLevel::getCurrentNeutralBalls() const
{
	u32 neutrallBalls = 0;
	tActorEntitiesMap::const_iterator it = m_actorsmap.find(IDNEUTRALLBALL);
	if (it != m_actorsmap.end())
	{
		neutrallBalls = (*it).second.size();
	}
	return neutrallBalls;
}

u32 GameLevel::getTotalNeutralBalls() const
{
	u32 neutrallBalls{ 0 };
	if (m_player)
		neutrallBalls = static_cast<Nik*>(m_player->getActor())->getNeutralizerBalls() + getCurrentNeutralBalls();
	return neutrallBalls;
}

Ogre::SceneNode* GameLevel::getLevelSceneNode()
{
	return m_level_node;
}

Ogre::SceneNode* GameLevel::getPlayerSceneNode()
{
	if (m_player != nullptr)
		return m_player->getGraphics()->getNode();
	else
		return nullptr;
}

Ogre::SceneNode* GameLevel::getLevelChildSceneNode(eLevelNode aLevelNode)
{
	return m_level_nodes[aLevelNode];
}

void GameLevel::setVisibleLevelChildSceneNode(eLevelNode aLevelNode, bool aVisible)
{
	if (aLevelNode == SCENARIO_NODE)
	{
		for (u32 i = 0; i < m_level_static_geometry.size(); i++)
		{
			m_level_static_geometry.at(i)->setVisible(aVisible);
		}
	}
	else if (m_level_nodes[aLevelNode])
	{
		m_level_nodes[aLevelNode]->setVisible(aVisible);
	}
}

void GameLevel::DestroyCameras()
{
	CCoreEngine::Instance().GetCameraManager().destroy_all_cameras();
}

void GameLevel::update(f64 dt)
{
	if (m_levelState == LEVEL_STATE_GAME || m_levelState == LEVEL_STATE_WIN_ANIMATION)
	{
		if (m_levelState == LEVEL_STATE_GAME)
		{
			auto& keyb = CCoreEngine::Instance().GetInputManager().GetKeyboard();
			if (keyb.IsActionKeyDown("ChangeCam"))
			{
				static bool cam = false;
				cam = !cam;
				if (!cam)
				{
					CCoreEngine::Instance().GetCameraManager().set_active_camera(IDCAMERA_GAME);
					CCoreEngine::Instance().GetCameraManager().get_current_camera()->setInputEnabled(false);
					if (m_player)
					{
						static_cast<Player*>(m_player->getActor())->setInputEnabled(true);
					}
				}
				else
				{
					CCoreEngine::Instance().GetCameraManager().set_active_camera(IDCAMERA_FREE);
					CCoreEngine::Instance().GetCameraManager().get_current_camera()->setInputEnabled(true);
					if (m_player)
					{
						static_cast<Player*>(m_player->getActor())->setInputEnabled(false);
					}
				}
			}
			else if (keyb.IsActionKeyDown("SetInputEnableCam"))
			{
				bool inputEnabled = CCoreEngine::Instance().GetCameraManager().get_current_camera()->isInputEnabled();
				CCoreEngine::Instance().GetCameraManager().get_current_camera()->setInputEnabled(!inputEnabled);
				if (m_player)
				{
					static_cast<Player*>(m_player->getActor())->setInputEnabled(inputEnabled);
				}
			}
			else if (keyb.IsActionKeyPushed("VolumeUp"))
			{
				u32 volume = CCoreEngine::Instance().GetOptionManager().getOption(MUSIC_VOLUME_OPTION_PATH).as<u32>();
				if (volume < MAX_SOUND_VOLUME)
				{
					volume++;
					CCoreEngine::Instance().GetOptionManager().setOption(MUSIC_VOLUME_OPTION_PATH, volume);
				}
			}
			else if (keyb.IsActionKeyPushed("VolumeDown"))
			{
				u32 volume = CCoreEngine::Instance().GetOptionManager().getOption(MUSIC_VOLUME_OPTION_PATH).as<u32>();
				if (volume > MIN_SOUND_VOLUME)
				{
					volume--;
					CCoreEngine::Instance().GetOptionManager().setOption(MUSIC_VOLUME_OPTION_PATH, volume);
				}
			}

			//Actualitzo les entitats (sense el player)
			for (auto &entitygroup : m_actorsmap)
			{
				for (auto &entity : entitygroup.second)
				{
					if (entity.second != m_player)
						entity.second->update(dt);
				}
			}

			// actualitzem el player
			if (m_player)
				m_player->update(dt);

			// destroy entities
			removePendingEntities();

			if (isPlayerDied())
			{
				//m_levelState = LEVEL_STATE_LOST;
				m_levelState = LEVEL_STATE_LOSE_ANIMATION;

				if (LevelAnimation* const animation = m_animations[m_levelState])
				{
					animation->start();
				}
			}
			else if (isPlayerWon())
			{
				//m_levelState = LEVEL_STATE_WON;
				m_levelState = LEVEL_STATE_WIN_ANIMATION;
				m_hud.stop();

				CCoreEngine::Instance().GetSoundManager().playSound("end_level");
				if (LevelAnimation* const animation = m_animations[m_levelState])
				{
					animation->start();
				}
			}
		}

		m_hud.update(dt);
	}

	if (auto* animation = m_animations[m_levelState])
	{
		animation->update(dt);

		if (animation->finished())
		{
			switch (m_levelState)
			{
			case LEVEL_STATE_START_ANIMATION:
				m_levelState = LEVEL_STATE_GAME;
				break;
			case LEVEL_STATE_LOSE_ANIMATION:
				m_levelState = LEVEL_STATE_LOST;
				break;
			case LEVEL_STATE_WIN_ANIMATION:
				m_levelState = LEVEL_STATE_WON;
				break;

			case LEVEL_STATE_LOST:
			case LEVEL_STATE_WON:
				break;
			}
		}
	}
}

void GameLevel::pause()
{
	CCoreEngine::Instance().GetSoundManager().pauseMusic();

	for (auto &entitygroup : m_actorsmap)
		for (auto &entity : entitygroup.second)
			entity.second->pause();

}

void GameLevel::resume()
{
	CCoreEngine::Instance().GetSoundManager().resumeMusic();
	
	for (auto &entitygroup : m_actorsmap)
		for (auto &entity : entitygroup.second)
			entity.second->resume();
	

}

void GameLevel::destroyItem(const std::string& a_name)
{
	m_entitiesToRemove[GAMELEVEL_ACTOR_SUBTYPE_ITEM_TAG] = a_name;
}

void GameLevel::destroyEnemy(const std::string& a_name)
{
	m_entitiesToRemove[GAMELEVEL_ACTOR_SUBTYPE_ENEMY_TAG] = a_name;
}


bool GameLevel::isLevelEnded() const
{
	// FINAL NIVELL
	// - No queden boles per recollir i el jugador ha arribat al final del nivell.
	// - El jugador ha mort.
	return ((getCurrentNeutralBalls() == 0) && m_player_at_end) || (m_player && (m_player->getActor()->getLife() == 0));
}

bool GameLevel::isPlayerDied() const
{
	return (m_player && (m_player->getActor()->getLife() == 0));
}

bool GameLevel::isPlayerWon() const
{
	return ((getCurrentNeutralBalls() == 0) && m_player_at_end);
}


void GameLevel::setPlayerAtEnd(bool a_player_at_end)
{
	m_player_at_end = a_player_at_end;
}

bool GameLevel::isPlayerAtEnd() const
{
	return m_player_at_end;
}

bool GameLevel::isLevelLost() const
{
	return (m_levelState == LEVEL_STATE_LOST);
}

bool GameLevel::isLevelWon() const
{
	return (m_levelState == LEVEL_STATE_WON);
}




void GameLevel::CreateLevel()
{
	m_static_entities_count = 0;
	// Create Ogre nodes
	auto scene_mgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();
	m_level_node = scene_mgr->getRootSceneNode()->createChildSceneNode(m_tiledRawinfo->m_mapname + "_node");

	m_level_nodes[SCENARIO_NODE]		= m_level_node->createChildSceneNode(m_tiledRawinfo->m_mapname + "_scenario_node");
	m_level_nodes[ACTORS_NODE]			= m_level_node->createChildSceneNode(m_tiledRawinfo->m_mapname + "_actors_node");
	m_level_nodes[COLLISION_NODE]		= m_level_node->createChildSceneNode(m_tiledRawinfo->m_mapname + "_collision_node");
	m_level_nodes[HUD_NODE]				= m_level_node->createChildSceneNode(m_tiledRawinfo->m_mapname + "_hud_node");

	//Object Layers
	for (auto objectlayer : m_tiledRawinfo->m_layersobjects)
	{
		auto objectlayername = objectlayer.first;
		//Si es la layer de collision
		if (objectlayername == GAMELEVEL_OBJ_COLLISIONS_LAYER_TAG)
			CreateLevelCollisions(objectlayer.second);
	}

	//Tile Layers
	for (auto layer : m_tiledRawinfo->m_layers)
	{
		auto layerobj = layer.second;

		for (auto tile : layerobj->m_tileIds)
		{
			auto tileproperties = tile.second;
			auto tileposition = tile.first;
			if (!GameLevelPropertyExists(GAMELEVEL_TYPE_TAG, *tileproperties))
				throw std::runtime_error("No tileconfig tag:" + GAMELEVEL_TYPE_TAG + " Found !!!!!");

			//Agafo el tipus de la tile
			auto tiletype = GetGameLevelProperty(GAMELEVEL_TYPE_TAG, *tileproperties);

			//depen del tipus de tile fem quelcom
			if (tiletype == GAMELEVEL_TYPE_STATICMESH_TAG)
				CreateLevelStaticMesh(tileposition, tileproperties, layerobj);
			else if (tiletype == GAMELEVEL_TYPE_MESH)
				CreateLevelMesh(tileposition, tileproperties, layerobj);
			else if (tiletype == GAMELEVEL_TYPE_ACTOR_TAG)
				CreateLevelActor(tileposition, tileproperties, layerobj);
		}
	}
}



void GameLevel::CreateLevelStaticMesh(const u32 a_tileposition, const std::shared_ptr<const tGameLevelPropertiesMap> a_tileproperties, const std::shared_ptr<const GameLevelLayer> a_layer)
{
	if (!GameLevelPropertyExists(GAMELEVEL_TYPE_TAG, *a_tileproperties))
		throw std::runtime_error("No tileconfig tag:" + GAMELEVEL_TYPE_TAG + " Found !!!!!");

	//busco la posicio de la tile (0.0) abaix a la esquerra.
	s32 columna = a_tileposition % m_tiledRawinfo->m_tilecolumnscount;
	s32 fila = m_tiledRawinfo->m_tilerowscount - ((static_cast<s32>(a_tileposition) / m_tiledRawinfo->m_tilecolumnscount) + 1);

	// Calculem la posicio de la tile al MIG de la tile
	Ogre::Vector3 posvec(static_cast<Ogre::Real>(columna)* m_tiledRawinfo->m_tiledimensions + (m_tiledRawinfo->m_tiledimensions / 2.f), static_cast<Ogre::Real>(fila)* m_tiledRawinfo->m_tiledimensions + (m_tiledRawinfo->m_tiledimensions / 2.f), 0.0f);
	
	
	//Si la layer te la propietat de translation
	if (GameLevelPropertyExists(GAMELEVEL_LAYER_TRANSLATIONZ_TAG, a_layer->m_layerGlobalProperties))
	{
		auto translationZvalue = GetGameLevelProperty(GAMELEVEL_LAYER_TRANSLATIONZ_TAG, a_layer->m_layerGlobalProperties);
		posvec.z += static_cast<Ogre::Real>(StringToNumber(translationZvalue) * m_tiledRawinfo->m_tiledimensions);
	}


	//Rotacio
	edv::any rotax(GetGameLevelProperty(GAMELEVEL_ROTATIONX_TAG, *a_tileproperties));
	edv::any rotay(GetGameLevelProperty(GAMELEVEL_ROTATIONY_TAG, *a_tileproperties));
	edv::any rotaz(GetGameLevelProperty(GAMELEVEL_ROTATIONZ_TAG, *a_tileproperties));

	Ogre::Quaternion quatx(Ogre::Degree(rotax.as<f32>()), Ogre::Vector3(1, 0, 0));
	Ogre::Quaternion quaty(Ogre::Degree(rotay.as<f32>()), Ogre::Vector3(0, 1, 0));
	Ogre::Quaternion quatz(Ogre::Degree(rotaz.as<f32>()), Ogre::Vector3(0, 0, 1));

	f32 scalex(GameLevelPropertyExists(GAMELEVEL_SCALEX_TAG, *a_tileproperties) ? edv::any(GetGameLevelProperty(GAMELEVEL_SCALEX_TAG, *a_tileproperties)) : 1.f);
	f32 scaley(GameLevelPropertyExists(GAMELEVEL_SCALEY_TAG, *a_tileproperties) ? edv::any(GetGameLevelProperty(GAMELEVEL_SCALEY_TAG, *a_tileproperties)) : 1.f);
	f32 scalez(GameLevelPropertyExists(GAMELEVEL_SCALEZ_TAG, *a_tileproperties) ? edv::any(GetGameLevelProperty(GAMELEVEL_SCALEZ_TAG, *a_tileproperties)) : 1.f);

	Ogre::Quaternion quatrotation = quatx * quaty * quatz;
	Ogre::Vector3 scaleConfig{ scalex, scaley, scalez };
	std::string GameEntityName = "GraphicalPiece " + to_string(a_layer->m_name) + " " + to_string(fila) + " " + to_string(columna);
	EntityOgreGraphics::Config GraphicalConfig(GameEntityName, posvec, quatrotation, true, GetGameLevelProperty(GAMELEVEL_MESH_TAG, *a_tileproperties), nullptr, GetGameLevelProperty(GAMELEVEL_MATERIAL_TAG, *a_tileproperties), scaleConfig);

	// Create entity
	auto scene_mgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();
	Ogre::Entity* entity = scene_mgr->createEntity(GraphicalConfig.nodeName, GraphicalConfig.filePath);
	if (!GraphicalConfig.material.empty())
	{
		entity->setMaterialName(GraphicalConfig.material);
	}
	Ogre::Vector3 position(GraphicalConfig.initialPos);
	Ogre::Quaternion rotation(GraphicalConfig.initialRot);
	Ogre::Vector3 scale(GraphicalConfig.scale);

	if (m_static_entities_count == 0 || m_static_entities_count > MAX_ENTITIES_PER_STATIC_MESH)
	{
		addStaticMesh();
		m_static_entities_count = 0;
	}

	m_level_static_geometry.back()->addEntity(entity, position, rotation, scale);
	m_static_entities_count++;

	scene_mgr->destroyEntity(entity);
}

void GameLevel::CreateLevelMesh(const u32 a_tileposition, const std::shared_ptr<const tGameLevelPropertiesMap> a_tileproperties, const std::shared_ptr<const GameLevelLayer> a_layer)
{
	if (!GameLevelPropertyExists(GAMELEVEL_TYPE_TAG, *a_tileproperties))
		throw std::runtime_error("No tileconfig tag:" + GAMELEVEL_TYPE_TAG + " Found !!!!!");

	//busco la posicio de la tile (0.0) abaix a la esquerra.
	s32 columna = a_tileposition % m_tiledRawinfo->m_tilecolumnscount;
	s32 fila = m_tiledRawinfo->m_tilerowscount - ((static_cast<s32>(a_tileposition) / m_tiledRawinfo->m_tilecolumnscount) + 1);

	// Calculem la posicio de la tile al MIG de la tile
	Ogre::Vector3 posvec(static_cast<Ogre::Real>(columna)* m_tiledRawinfo->m_tiledimensions + (m_tiledRawinfo->m_tiledimensions / 2.f), static_cast<Ogre::Real>(fila)* m_tiledRawinfo->m_tiledimensions + (m_tiledRawinfo->m_tiledimensions / 2.f), 0.0f);


	//Si la layer te la propietat de translation
	if (GameLevelPropertyExists(GAMELEVEL_LAYER_TRANSLATIONZ_TAG, a_layer->m_layerGlobalProperties))
	{
		auto translationZvalue = GetGameLevelProperty(GAMELEVEL_LAYER_TRANSLATIONZ_TAG, a_layer->m_layerGlobalProperties);
		posvec.z += static_cast<Ogre::Real>(StringToNumber(translationZvalue) * m_tiledRawinfo->m_tiledimensions);
	}


	//Rotacio
	edv::any rotax(GetGameLevelProperty(GAMELEVEL_ROTATIONX_TAG, *a_tileproperties));
	edv::any rotay(GetGameLevelProperty(GAMELEVEL_ROTATIONY_TAG, *a_tileproperties));
	edv::any rotaz(GetGameLevelProperty(GAMELEVEL_ROTATIONZ_TAG, *a_tileproperties));

	Ogre::Quaternion quatx(Ogre::Degree(rotax.as<f32>()), Ogre::Vector3(1, 0, 0));
	Ogre::Quaternion quaty(Ogre::Degree(rotay.as<f32>()), Ogre::Vector3(0, 1, 0));
	Ogre::Quaternion quatz(Ogre::Degree(rotaz.as<f32>()), Ogre::Vector3(0, 0, 1));

	f32 scalex(GameLevelPropertyExists(GAMELEVEL_SCALEX_TAG, *a_tileproperties) ? edv::any(GetGameLevelProperty(GAMELEVEL_SCALEX_TAG, *a_tileproperties)) : 1.f);
	f32 scaley(GameLevelPropertyExists(GAMELEVEL_SCALEY_TAG, *a_tileproperties) ? edv::any(GetGameLevelProperty(GAMELEVEL_SCALEY_TAG, *a_tileproperties)) : 1.f);
	f32 scalez(GameLevelPropertyExists(GAMELEVEL_SCALEZ_TAG, *a_tileproperties) ? edv::any(GetGameLevelProperty(GAMELEVEL_SCALEZ_TAG, *a_tileproperties)) : 1.f);

	Ogre::Quaternion quatrotation = quatx * quaty * quatz;
	Ogre::Vector3 scaleConfig{ scalex, scaley, scalez };
	std::string GameEntityName = "GraphicalPiece " + to_string(a_layer->m_name) + " " + to_string(fila) + " " + to_string(columna);
	EntityOgreGraphics::Config GraphicalConfig(GameEntityName, posvec, quatrotation, true, GetGameLevelProperty(GAMELEVEL_MESH_TAG, *a_tileproperties), nullptr, GetGameLevelProperty(GAMELEVEL_MATERIAL_TAG, *a_tileproperties), scaleConfig);
	
	// Create entity
	auto scene_mgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();
	Ogre::Entity* entity = scene_mgr->createEntity(GraphicalConfig.nodeName, GraphicalConfig.filePath);
	if (!GraphicalConfig.material.empty())
	{
		entity->setMaterialName(GraphicalConfig.material);
	}
	Ogre::Vector3 position(GraphicalConfig.initialPos);
	Ogre::Quaternion rotation(GraphicalConfig.initialRot);
	Ogre::Vector3 scale(GraphicalConfig.scale);
	
	// creem una mesh estatica
	addStaticMesh();

	m_level_static_geometry.back()->addEntity(entity, position, rotation, scale);
	scene_mgr->destroyEntity(entity);

}

void GameLevel::CreateLevelActor(const u32 a_tileposition, const std::shared_ptr<const tGameLevelPropertiesMap> a_tileproperties, const std::shared_ptr<const GameLevelLayer> a_layer)
{

	if (!GameLevelPropertyExists(GAMELEVEL_ENTITY_ID_TAG, *a_tileproperties))
		throw std::runtime_error("No Actor tileconfig tag:" + GAMELEVEL_ENTITY_ID_TAG + " Found !!!!!");

	if (!GameLevelPropertyExists(GAMELEVEL_SUBTYPE_TAG, *a_tileproperties))
		throw std::runtime_error("No Actor tileconfig tag:" + GAMELEVEL_SUBTYPE_TAG + " Found !!!!!");
	
	//busco la posicio de la tile (0.0) abaix a la esquerra.
	s32 columna = a_tileposition % m_tiledRawinfo->m_tilecolumnscount;
	s32 fila = m_tiledRawinfo->m_tilerowscount - ((static_cast<s32>(a_tileposition) / m_tiledRawinfo->m_tilecolumnscount) + 1);

	// Calculem la posicio de la tile al MIG de la tile
	Ogre::Vector3 posvec(static_cast<Ogre::Real>(columna)* m_tiledRawinfo->m_tiledimensions + (m_tiledRawinfo->m_tiledimensions / 2.f), static_cast<Ogre::Real>(fila)* m_tiledRawinfo->m_tiledimensions + (m_tiledRawinfo->m_tiledimensions / 2.f), 0.0f);


	auto subtype		= GetGameLevelProperty(GAMELEVEL_SUBTYPE_TAG, *a_tileproperties);
	auto actorentityid	= GetGameLevelProperty(GAMELEVEL_ENTITY_ID_TAG, *a_tileproperties);

	auto& game = CCoreEngine::Instance().GetGameManager();
	auto p_entity = game.createEntity(actorentityid, "Actor"+ a_layer->m_name + subtype + to_string(fila) + to_string(columna), posvec, Ogre::Quaternion::IDENTITY, m_level_nodes[ACTORS_NODE]);
	
	
	if (subtype == GAMELEVEL_ACTOR_SUBTYPE_PLAYER_TAG)
	{	
		if (m_player == nullptr)
			m_player = p_entity;
		else
			throw std::runtime_error("Two player start tiles in the same map?!");
		game.registerPlayer(p_entity);
	}
	
	m_actorsmap[subtype][p_entity->getName()] = p_entity;

}

void GameLevel::CreateLevelCollisions(const std::shared_ptr<const GameLevelPolyObjectsLayer> a_objlayer)
{
	f32 translationZ = 0.f;

	//Si la layer te la propietat de translation
	if (GameLevelPropertyExists(GAMELEVEL_LAYER_TRANSLATIONZ_TAG, a_objlayer->m_globalProperties))
	{
		auto translationZvalue = GetGameLevelProperty(GAMELEVEL_LAYER_TRANSLATIONZ_TAG, a_objlayer->m_globalProperties);
		translationZ = static_cast<Ogre::Real>(StringToNumber(translationZvalue) * m_tiledRawinfo->m_tiledimensions);
	}
	
	auto idmanualobj = 0;
	for (auto polyobject : a_objlayer->m_polyobjects )
	{
		// Crearem una mesh d'Ogre per a passar-la a Bullet
		
		auto scene_mgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();
		std::string manualObjectName = m_tiledRawinfo->m_mapname + a_objlayer->m_name + to_string(idmanualobj);
		idmanualobj++;
		Ogre::ManualObject* object = scene_mgr->createManualObject(manualObjectName);
		object->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_TRIANGLE_STRIP);
			
		auto index = 0;
		for (auto point : polyobject.second->m_points)
		{
			Ogre::Vector3 pos(point.first, point.second, (m_tiledRawinfo->m_tiledimensions / 2.f) + translationZ);
			// front vertex
			object->position(pos);
			object->index(index++);

			// back vertex
			pos.z = static_cast<btScalar>(-m_tiledRawinfo->m_tiledimensions / 2.f) + translationZ;
			object->position(pos);
			object->index(index++);
		}
		object->end();

		
		// Creem el recurs de la mesh
		object->convertToMesh(manualObjectName);
		//Em guardo el resource per petarlo despres
		m_meshresourcestoremove.push_back(manualObjectName);

		EntityOgreGraphics::Config GraphicalConfig("PhysicPiece" + object->getName(), Ogre::Vector3::ZERO, Ogre::Quaternion::IDENTITY, true, manualObjectName, m_level_nodes[COLLISION_NODE]);
		EntityBtPhysics::Config PhysicalConfig(0.0f, false, {}, false, { EntityBtPhysics::COLLISION_SHAPE_TRIANGLE_MESH });

		GameEntity* Piece = new GameEntity(GraphicalConfig, PhysicalConfig);
		
		// Amaguem la visibilitat de la mesh
		Piece->getGraphics()->getNode()->setVisible(false);
		
		// Indiquem el flag de colisio estatic
		Piece->getPhysics()->getRigidBody()->setCollisionFlags(Piece->getPhysics()->getRigidBody()->getCollisionFlags() | btCollisionObject::CF_STATIC_OBJECT);
		
		m_actorsmap[a_objlayer->m_name][Piece->getName()] = Piece;

		scene_mgr->destroyManualObject(object);
	}
}

GameLevel::BossPathPoints GameLevel::getBossPathPoints(const std::string& a_layerName)
{
	BossPathPoints bossPath;
	auto it = m_tiledRawinfo->m_layersobjects.find(a_layerName);

	if (it != m_tiledRawinfo->m_layersobjects.end())
	{
		Ogre::Real z = 0;
		auto layer = it->second;

		if (GameLevelPropertyExists(GAMELEVEL_LAYER_TRANSLATIONZ_TAG, layer->m_globalProperties))
		{
			auto translationZvalue = GetGameLevelProperty(GAMELEVEL_LAYER_TRANSLATIONZ_TAG, layer->m_globalProperties);
			z += static_cast<Ogre::Real>(StringToNumber(translationZvalue) * m_tiledRawinfo->m_tiledimensions);
		}

		for (auto polyobject : layer->m_polyobjects)
		{
			for (auto point : polyobject.second->m_points)
			{
				bossPath.push_back(edv::Vector3f{ point.first, point.second, z });
			}
		}
	}

	return bossPath;
}

void GameLevel::DestroyLevel()
{
	//Peto totes les instancies d'entitats
	auto& game = CCoreEngine::Instance().GetGameManager();
	for (auto &entitygroup : m_actorsmap)
	{
		for (auto &entity : entitygroup.second)
		{
			auto ptrentity = game.getEntity(entity.first);

			//Si no esta registrada delete
			if (ptrentity == nullptr)
				delete entity.second;
			else
				game.destroyEntity(entity.second);
		}
	}
	m_actorsmap.clear();

	m_level_node->removeAndDestroyAllChildren();
	m_level_node->getParent()->removeChild(m_level_node);
	auto* sceneMgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();
	sceneMgr->destroySceneNode(m_level_node);
	m_level_node = nullptr;
	m_level_nodes.fill(nullptr);
}

void GameLevel::removePendingEntities()
{
	for (auto entity : m_entitiesToRemove)
	{
		auto type = entity.first;
		auto name = entity.second;
		auto typeit = m_actorsmap.find(type);
		if (typeit != m_actorsmap.end())
		{
			auto itentity = typeit->second.find(name);
			if (itentity != typeit->second.end())
			{
				CCoreEngine::Instance().GetGameManager().destroyEntity(itentity->second);
				typeit->second.erase(itentity);
			}
		}
	}
	m_entitiesToRemove.clear();
}

Ogre::StaticGeometry* GameLevel::addStaticMesh()
{
	auto scene_mgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();
	Ogre::StaticGeometry * plevel_static_geometry = scene_mgr->createStaticGeometry("ScenarioGeometry" + to_string(m_level_static_geometry.size()) + m_tiledRawinfo->m_mapname);
	m_level_static_geometry.push_back(plevel_static_geometry);
	return plevel_static_geometry;
}