#ifndef MAGNETIA_INTRO_PAUSED_STATE_H_
#define MAGNETIA_INTRO_PAUSED_STATE_H_

#include "IntroPausedState.h"

#include <State/CState.h>
#include <Utils/CSingleton.h>

class IntroPausedState : public CState, public CSingleton<IntroPausedState>
{
public:
	IntroPausedState();
	~IntroPausedState();

	void Start() override;
	void Finish() override;
	void Pause() override;
	void Resume() override;
	void Update(f64 dt) override;

private:
	f64 m_pauseTime;
	f64	m_startTimestamp;
};

#endif
