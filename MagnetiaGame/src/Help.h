#ifndef MAGNETIA_GAME_HELP_H_
#define MAGNETIA_GAME_HELP_H_

#include <Core/CBasicTypes.h>
#include <memory>
#include <OGRE/Ogre.h>
#include "HudElement.h"
#include "Time/ITimerCallback.h"

class Panel;

class Help : public HudElement
{
public:
	Help();
	~Help();

	virtual void start() override;
	virtual void update(f64 dt) override;
	virtual void stop() override;
	virtual tPanelsVector getPanels() const override;

private:

	void show();
	void hide();

	bool		m_running;
	bool		m_showing;
	typedef std::unique_ptr<Panel>						tPanelPtr;
	tPanelPtr		m_panel;
	Ogre::Vector2	m_hidePosition;
	Ogre::Vector2	m_initialPosition;
	Ogre::Vector2	m_showPosition;
	Ogre::Vector2	m_dimensions;
	EV_TimerID		m_showTimer;
};

#endif
