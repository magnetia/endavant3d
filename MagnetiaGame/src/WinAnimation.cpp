#include <Core/CCoreEngine.h>
#include <Renderer/CRenderManager.h>
#include <Options/OptionManager.h>
#include "WinAnimation.h"
#include "Fader.h"

WinAnimation::WinAnimation() :
	LevelAnimation{ "WinAnimation" },
	m_fader{ new Fader{ "Overlays/FadeWin", "White", nullptr } }
{
	createFont("WinAnimationFont", "truetype", "Aero.ttf", 32, 96);
}

WinAnimation::~WinAnimation()
{
	Ogre::FontManager::getSingleton().destroyResourcePool("WinAnimationFont");
}

void WinAnimation::startImpl()
{
	Ogre::Viewport* vp = CCoreEngine::Instance().GetRenderManager().GetOgreViewport();
	f32 totalWidth = static_cast<f32>(vp->getActualWidth()), totalHeight = static_cast<f32>(vp->getActualHeight());

	// background panel
	const Ogre::Vector2 backgroundPanelDimensions{ totalWidth, totalHeight / 4.f }, backgroundPanelStart{ -totalWidth, totalHeight / 4.f };
	const Ogre::Vector2 backgroundPanelPos{ 0, totalHeight / 2.f - backgroundPanelDimensions.y / 2.f };

	std::vector<Panel::Animation*> backgroundAnimations{
		//new Panel::Animation{ 3, backgroundPanelStart, backgroundPanelStart },
		//new Panel::Animation{ 0.3, backgroundPanelStart, backgroundPanelPos },
		//new Panel::Animation{ 0.5, backgroundPanelPos, backgroundPanelPos },

		// ray anim texture panel
		new Panel::Animation{ 5., backgroundPanelPos, backgroundPanelPos },
		//new Panel::Animation{ 3., backgroundPanelStart, backgroundPanelStart },
		//new Panel::Animation{ 0.5, backgroundPanelPos, backgroundPanelStart },
	};

	Panel::Text* backgroundPanelText = new Panel::Text{
		"LevelWonText",
		"WinAnimationFont",
		CCoreEngine::Instance().GetOptionManager().getOption("timer/level_passed_text"),
		{ (totalWidth / 2.f) - 245, backgroundPanelDimensions.y / 2.f - 200 /*(totalHeight / 2.f) - 50*/ }, // 28/08/2014 so SHUT THE FUCK UP!!
		Ogre::ColourValue::Red
	};

	Panel::TextsParam backgroundPanelTexts{
		Panel::TextPairParam{ "backgroundPanelText", backgroundPanelText }
	};

	addPanel(new Panel{ "WinAnimation_WinBackground", backgroundPanelStart, backgroundPanelDimensions, {}, backgroundAnimations, { .5, "WinAnimation_WinBackground", false } });

	Ogre::Vector2 winPanelTextDimensions{ totalWidth / 2.f, totalHeight / 4.f }, winPanelTextStart{ 0, -winPanelTextDimensions.y };
	const Ogre::Vector2 winPanelTextPos{ 0.f, totalHeight / 2.f - 50 };

	std::vector<Panel::Animation*> winPanelTextAnimations{
		new Panel::Animation{ 2., winPanelTextStart, winPanelTextStart },
		new Panel::Animation{ .5, winPanelTextStart, winPanelTextPos },
	};
	
	addPanel(new Panel{ "WinAnimation_Text", winPanelTextStart, winPanelTextDimensions, backgroundPanelTexts, winPanelTextAnimations, {} });

	m_fader->startFadeOut(2.);
}

void WinAnimation::update(f64 dt)
{
	LevelAnimation::update(dt);
	m_fader->fade(dt);
}