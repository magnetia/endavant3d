#ifndef MAGNETIA_GAME_START_ANIMATION_H_
#define MAGNETIA_GAME_START_ANIMATION_H_

#include <memory>
#include <vector>
#include <Core/CBasicTypes.h>
#include "LevelAnimation.h"

class StartAnimation : public LevelAnimation
{
public:
	StartAnimation();
	~StartAnimation();

private:
	void startImpl() override;
};

#endif
