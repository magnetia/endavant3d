#ifndef BOARD_MENU_LVL_SELECTION_H_
#define BOARD_MENU_LVL_SELECTION_H_

#include <Core/CBasicTypes.h>
#include <Utils/CXMLParserPUGI.h>
#include <map>
#include <memory>
#include "OGRE/Ogre.h"
#include "OGRE/Overlay/OgreOverlaySystem.h"

#include "GUI\BoardButtonStates.h"
#include "GUI\BoardMenu.h"
#include "GUI\BoardButtonConfig.h"
#include "GUI\ChangeStateButton.h"
class BoardButtonChangeStateLevelIdleState;
class BoardButtonChangeStateLevelHoverState;
class BoardButtonChangeStateLevelSelectedState;
class BoardButtonChangeStateLevelFinishState;
class BoardButtonChangeStateLevelEndState; 

class ChangeStateLevelSelectionButton : public ChangeStateButton
{
public:
	typedef enum E_BUTT_POSITION
	{
		E_OUTSIDEUP_POSITION = 0,
		E_VERYUP_POSITION,
		E_UP_POSITION,
		E_MIDDLE_POSITION,
		E_LOW_POSITION,
		E_VERYLOW_POSITION,
		E_OUTSIDELOW_POSITION
	};

	ChangeStateLevelSelectionButton(std::shared_ptr<BoardButtonConfig> a_buttcfg, const std::string &a_buttid, 
		std::shared_ptr<BoardMenu> a_BoardMenuAsociated, const std::string& a_stateMgrName, const std::string& a_nextStateName,
		ChangeStateLevelSelectionButton::E_BUTT_POSITION a_initialposition);

	virtual ~ChangeStateLevelSelectionButton(){};
	
	const static Ogre::Vector3 VERYUP_POSITION;
	const static Ogre::Vector3 UP_POSITION;
	const static Ogre::Vector3 MIDDLE_POSITION;
	const static Ogre::Vector3 LOW_POSITION;
	const static Ogre::Vector3 VERYLOW_POSITION;


	

	void setPositionState(E_BUTT_POSITION a_pos);
	Ogre::Vector3 getPositionByState(E_BUTT_POSITION a_pos);
	E_BUTT_POSITION getPositionState();
private:
	E_BUTT_POSITION m_buttpositioninternal;

};



class BoardMenuLvlSelection: public BoardMenu
{
public:

	
	BoardMenuLvlSelection(BoardMenuManager *a_menumgr);
	virtual ~BoardMenuLvlSelection();

	void CreateButtons();
protected:
	void hoverPreviousButton() override;
	void hoverNextButton() override;

};

#endif
