#include "OverlayMaterialAnimator.h"
#include <OGRE/Overlay/OgreOverlayContainer.h>
#include <Core/CCoreEngine.h>
#include <Time/CTimeManager.h>

OverlayMaterialAnimator::OverlayMaterialAnimator(Ogre::OverlayContainer& a_overlayContainer, const std::string& a_newMaterialName, f64 a_duration) :
	m_overlayContainer{ &a_overlayContainer },
	m_basicMaterialName{ a_overlayContainer.getMaterialName() },
	m_newMaterialName{ a_newMaterialName },
	m_duration{ a_duration },
	m_startTimestamp{ 0. }
{
	// Preload textures
	m_overlayContainer->setMaterialName(m_newMaterialName);
	m_overlayContainer->setMaterialName(m_basicMaterialName);
}

OverlayMaterialAnimator::~OverlayMaterialAnimator()
{
}

void OverlayMaterialAnimator::start()
{
	m_startTimestamp = CCoreEngine::Instance().GetTimerManager().GetTotalTimeSeconds();
	m_overlayContainer->setMaterialName(m_newMaterialName);
}

void OverlayMaterialAnimator::stop()
{
	m_overlayContainer->setMaterialName(m_basicMaterialName);
	m_startTimestamp = 0.;
}

f64 OverlayMaterialAnimator::elapsed() const
{
	const f64 now = CCoreEngine::Instance().GetTimerManager().GetTotalTimeSeconds();
	return (now - m_startTimestamp);
}

bool OverlayMaterialAnimator::isActive() const
{
	return (m_startTimestamp != 0.);
}

void OverlayMaterialAnimator::update(f64 dt)
{
	if (isActive() && elapsed() >= m_duration)
	{
		stop();
	}
}