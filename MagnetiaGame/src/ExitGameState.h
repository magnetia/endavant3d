#ifndef MAGNETIA_EXIT_GAME_STATE_H_
#define MAGNETIA_EXIT_GAME_STATE_H_

#include <Core/CCoreEngine.h>
#include <State/CState.h>
#include <Utils/CSingleton.h>

class ExitGameState : public CState, public CSingleton<ExitGameState>
{
public:
	ExitGameState() : CState("ExitGameState") { }

	void Start() override { CCoreEngine::Instance().StopCore(); }
	void Finish() override { }
	void Pause() override { }
	void Resume() override { }
	void Update(f64 dt) override { }

private:
};

#endif

