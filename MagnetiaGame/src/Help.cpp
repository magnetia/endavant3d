#include "Help.h"
#include "Panel.h"
#include <Core/CCoreEngine.h>
#include <Renderer/CRenderManager.h>
#include <Input/CInputManager.h>

Help::Help() :
	m_running{ false },
	m_showing{ false },
	m_hidePosition{ 0.f, 0.f },
	m_initialPosition{ 0.f, 0.f },
	m_showPosition{ 0.f, 0.f },
	m_dimensions{ 0.f, 0.f },
	m_showTimer{ CTimeManager::INVALID_TIMERID }
{
	Ogre::Viewport* const vp{ CCoreEngine::Instance().GetRenderManager().GetOgreViewport() };
	const f32 totalWidth{ static_cast<f32>(vp->getActualWidth()) }, totalHeight{ static_cast<f32>(vp->getActualHeight()) };

	const Ogre::Vector2 textureSize{ CCoreEngine::Instance().GetRenderManager().getTextureSize("HUD_controls.png") };
	m_dimensions = { textureSize.x, textureSize.y };
	m_hidePosition = { -m_dimensions.x*0.86f, totalHeight / 2.f - m_dimensions.y / 2.f + 0.02f*m_dimensions.y };
	m_initialPosition = { -m_dimensions.x*0.76f, m_hidePosition.y };
	m_showPosition = { -m_dimensions.x*0.16f, m_hidePosition.y };
}

Help::~Help()
{
}

void Help::start()
{
	if (!m_running)
	{
		m_running = true;

		std::vector<Panel::Animation*> panelAnimations{
			new Panel::Animation{ 0.5, m_hidePosition, m_initialPosition }
		};
		m_panel.reset(new Panel{ "HUD_help", m_hidePosition, m_dimensions, {}, panelAnimations, TextureAnimation::Config{ 0.5, "HUD_help", true } });

		CCoreEngine::Instance().GetInputManager().GetKeyboard().InsertKeyAction("Help", "H");
	}
}

void Help::update(f64 dt)
{
	if (m_panel)
	{
		m_panel->update(dt);
		if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("Help"))
		{
			if (m_panel->finished())
			{
				if (m_showing)
				{
					hide();
				}
				else
				{
					show();
					m_showTimer = CCoreEngine::Instance().GetTimerManager().CreateTimer(8.);
				}
			}
		}
		else if (m_running && m_showing && CCoreEngine::Instance().GetTimerManager().IsEndTimer(m_showTimer))
		{
			CCoreEngine::Instance().GetTimerManager().KillTimer(m_showTimer);
			hide();
		}
	}
}

void Help::stop()
{
	if (m_running)
	{
		m_running = false;
		if (m_showing)
		{
			m_panel->finishAnimations();
			hide();
		}
	};
}

HudElement::tPanelsVector Help::getPanels() const
{
	return { m_panel.get() };
}

void Help::show()
{
	if (!m_showing)
	{
		m_showing = true;

		const bool textureAnimationActived(m_panel->getTextureAnimation()->isActive());
		const Ogre::Vector2 startPosition{ textureAnimationActived ? m_initialPosition : m_hidePosition };

		std::vector<Panel::Animation*> panelAnimations{
			new Panel::Animation{ 0.5, startPosition, m_showPosition }
		};

		m_panel->setAnimations(panelAnimations);
		
		if (textureAnimationActived)
		{
			m_panel->getTextureAnimation()->stop();
		}
	}
}

void Help::hide()
{
	if (m_showing)
	{
		m_showing = false;

		std::vector<Panel::Animation*> panelAnimations{
			new Panel::Animation{ 0.5, m_showPosition, m_hidePosition }
		};

		m_panel->setAnimations(panelAnimations);
	}
}