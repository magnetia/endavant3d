#ifndef MAGNETIA_GAME_HUD_ELEMENT_H_
#define MAGNETIA_GAME_HUD_ELEMENT_H_

#include <Core/CBasicTypes.h>

class Panel;

class HudElement
{
public:
	virtual void start() = 0;
	virtual void update(f64 dt) = 0;
	virtual void stop() = 0;
	typedef std::vector<Panel*> tPanelsVector;
	virtual tPanelsVector getPanels() const = 0;
};

#endif