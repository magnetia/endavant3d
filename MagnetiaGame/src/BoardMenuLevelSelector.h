#ifndef BoardMenuLevelSelector_H_
#define BoardMenuLevelSelector_H_

#include <memory>
#include <string>
#include <vector>
#include "State/CState.h"
#include "Input/CInputMouse.h"
#include "Input/CInputKeyboard.h"
#include "Time/CTimeManager.h"
#include "Game/GameEntity.h"
#include "Externals/pugixml/pugixml.hpp"
#include "GUI/BoardButton.h"
#include "GUI/BoardMenu.h"

// #todo: crear en una altra banda del mon pq no interfereixi
// o be pensar si volem que "sobrevoli" el mon

class BoardMenuManager;

class BoardMenuLevelSelector : public BoardMenu
{
public:
	BoardMenuLevelSelector(pugi::xml_node& a_node, BoardMenuManager* a_parent, const Ogre::Vector3& a_buttonsOffset, f32 a_animDuration);

	// State
	void Start() override;
	void Finish() override;
	void Pause() override;
	void Resume() override;
	void Update(f64 dt) override;

private:
	
};

#endif
