#include "BoardMenuLvlSelection.h"
#include "GUI/BoardMenuManager.h"
#include "GUI/ChangeStateButton.h"
#include "GameLevel.h"

#include "GameLevelSelector.h"
#include "Core/CCoreEngine.h"
#include "Sound\CSoundManager.h"


#include "GameLevelSelector.h"

/// ESTATS BOTO




class ChangeStateLevelSelectionButton;


class BoardButtonChangeStateLevelIdleState : public BoardButtonChangeStateIdleState
{
public:
	BoardButtonChangeStateLevelIdleState() :BoardButtonChangeStateIdleState() {};
	virtual ~BoardButtonChangeStateLevelIdleState(){};
	virtual void Start() override
	{
		getBoardButton()->setTextCaption(getBoardButton()->getButtonConfig()->m_captiontext);

		std::shared_ptr<ChangeStateLevelSelectionButton> change_button = std::dynamic_pointer_cast<ChangeStateLevelSelectionButton>(getBoardButton());
		if (change_button->getPositionState() == ChangeStateLevelSelectionButton::E_OUTSIDEUP_POSITION ||
			change_button->getPositionState() == ChangeStateLevelSelectionButton::E_OUTSIDELOW_POSITION)
			getBoardButton()->getEntity()->getGraphics()->getNode()->setVisible(false);
		else
			getBoardButton()->getEntity()->getGraphics()->getNode()->setVisible(true);


		change_button->getEntity()->setWorldPosition(change_button->getPositionByState(change_button->getPositionState()));

	};
	
	

	virtual void Finish()override
	{
		auto butt = getBoardButton();
		if (butt)
		{
			auto entity = butt->getEntity();
			if (entity)
			{
				entity->removeAllBehaviors();
		//		getBoardButton()->resetPosition();
			}
		}
	};
	virtual void Pause() override{};
	virtual void Resume() override{};
	

};


class BoardButtonChangeStateLevelHoverState : public BoardButtonChangeStateHoverState
{
private:
	FlyStraightAnimator * m_behavicheck;
	void makeMovement(std::shared_ptr<BoardButton> button, ChangeStateLevelSelectionButton::E_BUTT_POSITION end)
	{
		
		auto but = std::dynamic_pointer_cast<ChangeStateLevelSelectionButton>(button);
		auto initial_pos = button->getPosition();
		auto position_end = but->getPositionByState(end);
		
		m_behavicheck = new FlyStraightAnimator{ 0.15, initial_pos.to_OgreVector3(), position_end };
		button->getEntity()->addBehavior(m_behavicheck);
		but->setPositionState(end);
		but->getEntity()->getGraphics()->getNode()->setVisible(true);
	
	}
public:
	BoardButtonChangeStateLevelHoverState() :BoardButtonChangeStateHoverState() {};
	virtual ~BoardButtonChangeStateLevelHoverState(){};
	virtual void Start() override
	{
		getBoardButton()->getBoardMenu()->disableInput();

		auto board_menu = getBoardButton()->getBoardMenu();
		auto current_button = getBoardButton();

		auto buttons = board_menu->getAllButtons();

		//Miro en quina posicio queda el hover
		auto buttonhoverposition = 0;
		for (auto butt : buttons)
		{
			//Miro si es un altre boto
			if (butt->getName() != current_button->getName())
				buttonhoverposition++;
			else
				break;
		}

		//Current hovering buttons
		makeMovement(buttons.at(buttonhoverposition), ChangeStateLevelSelectionButton::E_MIDDLE_POSITION);
		/*auto hover_but = std::dynamic_pointer_cast<ChangeStateLevelSelectionButton>(buttons.at(buttonhoverposition));
		hover_but->setPositionState(ChangeStateLevelSelectionButton::E_MIDDLE_POSITION);
		hover_but->getEntity()->getGraphics()->getNode()->setVisible(true);
		hover_but->getEntity()->setWorldPosition(hover_but->getPositionByState(hover_but->getPositionState()));*/
		
		
		{
			auto up = buttonhoverposition - 1;
			auto veryup = buttonhoverposition - 2;
			auto low = buttonhoverposition + 1;
			auto verylow = buttonhoverposition + 2;

			if (up >= 0)
			{
				makeMovement(buttons.at(up), ChangeStateLevelSelectionButton::E_UP_POSITION);
			}
						
			if (veryup >= 0)
			{
				makeMovement(buttons.at(veryup), ChangeStateLevelSelectionButton::E_VERYUP_POSITION);
				

			}
			

			for (auto i = 0; i < veryup; i++)
			{
				auto but = std::dynamic_pointer_cast<ChangeStateLevelSelectionButton>(buttons.at(i));
				but->setPositionState(ChangeStateLevelSelectionButton::E_OUTSIDEUP_POSITION);
				but->getEntity()->getGraphics()->getNode()->setVisible(false);
				but->getEntity()->setWorldPosition(but->getPositionByState(but->getPositionState()));

			}


			if (low < static_cast<s32>(buttons.size()))
			{
				makeMovement(buttons.at(low), ChangeStateLevelSelectionButton::E_LOW_POSITION);
			}
			

			if (verylow < static_cast<s32>(buttons.size()))
			{
				makeMovement(buttons.at(verylow), ChangeStateLevelSelectionButton::E_VERYLOW_POSITION);

			}
			
			for (auto i = verylow + 1; i < static_cast<s32>(buttons.size()); i++)
			{
				auto but = std::dynamic_pointer_cast<ChangeStateLevelSelectionButton>(buttons.at(i));
				but->setPositionState(ChangeStateLevelSelectionButton::E_OUTSIDELOW_POSITION);
				but->getEntity()->getGraphics()->getNode()->setVisible(false);
				but->getEntity()->setWorldPosition(but->getPositionByState(but->getPositionState()));
			}

		}

		

	

		
		
	};

	virtual void Finish() override
	{
		
		auto butt = getBoardButton();
		if (butt)
		{
			auto entity = butt->getEntity();
			if (entity)
			{
				entity->removeAllBehaviors();
				getBoardButton()->resetPosition();
			}
		}
		
	};

	virtual void Update(f64 dt) override
	{
		//hack per els altres
		if (m_behavicheck != nullptr)
		{
			if (m_behavicheck->finished())
			{
				getBoardButton()->getBoardMenu()->enableInput();
				m_behavicheck = nullptr;


				//Moviment de vaibe
				getBoardButton()->getEntity()->removeAllBehaviors();
				auto initial_pos = getBoardButton()->getPosition();
				auto position_back = getBoardButton()->getPosition();
				position_back.z -= 1.5;
				getBoardButton()->getEntity()->addBehavior(new ChainedBehavior({
					new FlyStraightAnimator{ 0.15, initial_pos.to_OgreVector3(), position_back.to_OgreVector3() },
					new FlyStraightAnimator{ 0.15, position_back.to_OgreVector3(), initial_pos.to_OgreVector3() }
				}, -1)
				);

			}

		}

	
		
		getBoardButton()->getEntity()->update(dt);
		
	}

};

class BoardButtonChangeStateLevelSelectedState : public BoardButtonChangeStateSelectedState
{
public:
	BoardButtonChangeStateLevelSelectedState() :BoardButtonChangeStateSelectedState() {};
	virtual ~BoardButtonChangeStateLevelSelectedState(){};
	

private:
	RotationAnimator*	m_chainedbehavi;
};
class BoardButtonChangeStateLevelFinishState : public BoardButtonFinishState
{
public:
	BoardButtonChangeStateLevelFinishState() : BoardButtonFinishState(){};
	virtual ~BoardButtonChangeStateLevelFinishState(){};
};

class BoardButtonChangeStateLevelEndState : public BoardButtonChangeStateEndState
{
public:
	BoardButtonChangeStateLevelEndState() : BoardButtonChangeStateEndState(){};
	virtual ~BoardButtonChangeStateLevelEndState(){};

	virtual void Update(f64 dt) override
	{
		auto board_menu = getBoardButton()->getBoardMenu();
		auto curr_button = getBoardButton();

		//Si es el boto seleccionat
		if (board_menu->getButtonSelected()->getName() == curr_button->getName())
		{
			//Si tots estan en END
			auto counter = 0;
			for (auto &but : board_menu->getAllButtons())
			{
				if (but->GetButtonState() != GUIBUTTON_END)
				{
					counter++;
				}
			}

			if (!counter)
			{

				//TODO nivell a carregar

				//Canvio d'estat
				auto curr_button = getBoardButton();
				std::shared_ptr<ChangeStateButton> change_button = std::dynamic_pointer_cast<ChangeStateButton>(curr_button);
				GameLevelSelector::Pointer()->m_levelid = change_button->getName();
				change_button->ChangeState();
			}

		}
		else
		{
			getBoardButton()->getEntity()->getGraphics()->getNode()->setVisible(false);
		}


	}

	
};

///////////////////////// BOTO
const  Ogre::Vector3 ChangeStateLevelSelectionButton::VERYUP_POSITION{ -15.0f, 4.0f, 1.0f };
const  Ogre::Vector3 ChangeStateLevelSelectionButton::UP_POSITION{ -8.0f, 0.0f, 1.0f };
const  Ogre::Vector3 ChangeStateLevelSelectionButton::MIDDLE_POSITION{ -5.0f, -4.0f, 1.0f };
const  Ogre::Vector3 ChangeStateLevelSelectionButton::LOW_POSITION{ -8.0f, -8.0f, 1.0f };
const  Ogre::Vector3 ChangeStateLevelSelectionButton::VERYLOW_POSITION{ -15.0f, -12.0f, 1.0f };

ChangeStateLevelSelectionButton::ChangeStateLevelSelectionButton(std::shared_ptr<BoardButtonConfig> a_buttcfg, const std::string &a_buttid,
	std::shared_ptr<BoardMenu> a_BoardMenuAsociated, const std::string& a_stateMgrName, const std::string& a_nextStateName, ChangeStateLevelSelectionButton::E_BUTT_POSITION a_initialposition) :
ChangeStateButton(a_buttcfg, a_buttid, a_BoardMenuAsociated, a_stateMgrName, a_nextStateName, std::make_shared<BoardButtonChangeStateLevelIdleState>(),
std::make_shared<BoardButtonChangeStateLevelHoverState>(), std::make_shared<BoardButtonChangeStateLevelSelectedState>(),
std::make_shared<BoardButtonChangeStateLevelFinishState>(), std::make_shared<BoardButtonChangeStateLevelEndState>())

{
	setPositionState(a_initialposition);
	getEntity()->setWorldPosition(getPositionByState(a_initialposition));

}

Ogre::Vector3 ChangeStateLevelSelectionButton::getPositionByState(E_BUTT_POSITION a_pos)
{
		
	switch (a_pos)
	{
	case E_OUTSIDEUP_POSITION:
		return VERYUP_POSITION;
		break;
	case E_VERYUP_POSITION:
		return VERYUP_POSITION;
		break;
	case E_UP_POSITION:
		return UP_POSITION;
		break;
	case E_MIDDLE_POSITION:
		return MIDDLE_POSITION;
		break;
	case E_LOW_POSITION:
		return LOW_POSITION;
		break;
	case E_VERYLOW_POSITION:
		return VERYLOW_POSITION;
		break;
	case E_OUTSIDELOW_POSITION:
		return VERYLOW_POSITION;
		break;
	default:
		return VERYLOW_POSITION;
		break;
	}
}


ChangeStateLevelSelectionButton::E_BUTT_POSITION ChangeStateLevelSelectionButton::getPositionState()
{
	return m_buttpositioninternal;
}
void ChangeStateLevelSelectionButton::setPositionState(ChangeStateLevelSelectionButton::E_BUTT_POSITION a_pos)
{
	m_buttpositioninternal = a_pos;
}

/////////////////////// BOARD MENU



static const std::string BOARD_NAME("/MainMenu/MenuSelectLevel");
BoardMenuLvlSelection::BoardMenuLvlSelection(BoardMenuManager *a_menumgr) :
BoardMenu(BOARD_NAME, a_menumgr)
{
}

BoardMenuLvlSelection::~BoardMenuLvlSelection()
{
}


void BoardMenuLvlSelection::hoverPreviousButton()
{
	if (!m_menubuttons.empty())
	{
		if (m_buttonselected != m_menubuttons.end())
		{
			if (m_buttonselected != m_menubuttons.end() - 1)
			{
				CCoreEngine::Instance().GetSoundManager().playSound("changebeep");
				(*m_buttonselected)->ChangeButtonState(GUIBUTTON_IDLE);
				m_buttonselected++;
				(*m_buttonselected)->ChangeButtonState(GUIBUTTON_HOVER);
			}
			
			
		}
	}
}

void BoardMenuLvlSelection::hoverNextButton()
{
	if (!m_menubuttons.empty())
	{
		if (m_buttonselected != m_menubuttons.end())
		{
			if (m_buttonselected != m_menubuttons.begin())
			{
				CCoreEngine::Instance().GetSoundManager().playSound("changebeep");
				(*m_buttonselected)->ChangeButtonState(GUIBUTTON_IDLE);
				m_buttonselected--;
				(*m_buttonselected)->ChangeButtonState(GUIBUTTON_HOVER);
			}
		}
	}
}



void BoardMenuLvlSelection::CreateButtons()
{
	//Creo el menu dels nivells
	auto levels = GameLevelSelector::Instance().getLevelsXMLinfo();
	
	
	auto indexbutt = 0;
	//Creo el boto Back del principi
	//{
	//	auto boardbuttoncfg = m_menumgr->getButtonConfig("change_menu_button")->clone();
	//	boardbuttoncfg->m_graphicsCfg->nodeName += "LevelUPBack";
	//	boardbuttoncfg->m_captiontext = "Back";
	//	auto newbutton = std::make_shared<ChangeStateLevelSelectionButton>(boardbuttoncfg, "LevelUPBack", getshared(), "BoardMenuManager", "/MainMenu/MainMenu", ChangeStateLevelSelectionButton::E_UP_POSITION);
	//	AddButton(newbutton);
	//}


	//Afegeixo els botons dels nivells
	for (auto level : levels)
	{
		if (level->isselectable)
		{
			//auto levels_board = CCoreEngine::Instance().GetBoardMenuManager().getMenuBoard("/MainMenu/MenuSelectLevel");
		
			auto boardbuttoncfg = m_menumgr->getButtonConfig("change_menu_button")->clone();
			boardbuttoncfg->m_graphicsCfg->nodeName += level->name;
			boardbuttoncfg->m_captiontext = level->name;

			ChangeStateLevelSelectionButton::E_BUTT_POSITION position = ChangeStateLevelSelectionButton::E_OUTSIDELOW_POSITION;
			switch (indexbutt)
			{
			case 0:
				position = ChangeStateLevelSelectionButton::E_MIDDLE_POSITION;
				break;
			case 1:
				position = ChangeStateLevelSelectionButton::E_LOW_POSITION;
				break;
			case 2:
				position = ChangeStateLevelSelectionButton::E_VERYLOW_POSITION;
				break;
			default:
				break;
			}
			
			auto newbutton = std::make_shared<ChangeStateLevelSelectionButton>(boardbuttoncfg, level->name, getshared(), "MainStateManager", "MainGameState", position);
			
			AddButton(newbutton);

			//y += -5.0f;
			indexbutt++;
		}

	}


	//Creo el boto Back del final
	{
		ChangeStateLevelSelectionButton::E_BUTT_POSITION position = ChangeStateLevelSelectionButton::E_OUTSIDELOW_POSITION;
		switch (indexbutt)
		{
		case 0:
			position = ChangeStateLevelSelectionButton::E_MIDDLE_POSITION;
			break;
		case 1:
			position = ChangeStateLevelSelectionButton::E_LOW_POSITION;
			break;
		case 2:
			position = ChangeStateLevelSelectionButton::E_VERYLOW_POSITION;
			break;
		default:

			break;
		}
	
		auto boardbuttoncfg = m_menumgr->getButtonConfig("change_menu_button")->clone();
		boardbuttoncfg->m_graphicsCfg->nodeName += "LevelLOWBack";
		boardbuttoncfg->m_captiontext = "Back";
		auto newbutton = std::make_shared<ChangeStateLevelSelectionButton>(boardbuttoncfg, "LevelLOWBack", getshared(), "BoardMenuManager", "/MainMenu/MainMenu", position);
		AddButton(newbutton);
	
	}
}