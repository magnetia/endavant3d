#include "Counter.h"
#include "Panel.h"
#include <OGRE/Overlay/OgreOverlaySystem.h>
#include <Core/CCoreEngine.h>
#include <Renderer/CRenderManager.h>
#include "OverlayMaterialAnimator.h"
#include <Game/GameManager.h>
#include "GameLevel.h"

const static std::string COUNTER_FONT_ID("counterFont0");
const static std::string COUNTER_FONT_SIZE("36");
const static std::string NUMBERS_BALL_TXTBOX("HUD_TXTNUMBALL");
const static Ogre::Real MATERIAL_BALL_X(7.0);
const static Ogre::Real MATERIAL_BALL_X_NUMBERS(170.0);
const static Ogre::Real MATERIAL_BALL_Y_NUMBERS(75.0);
const static Ogre::Real MATERIAL_BALL_X_TITLE(78.0);
const static Ogre::Real MATERIAL_BALL_Y_TITLE(35.0);


Counter::Counter() :
	m_running{ false },
	m_ballspanelAnimator{ nullptr },
	m_playerNeutralBalls{ 0 }
{
	Ogre::FontManager& fontManager = Ogre::FontManager::getSingleton();
	Ogre::FontPtr counterFont0 = fontManager.create(COUNTER_FONT_ID, "General");

	counterFont0->setParameter("type", "truetype");
	counterFont0->setParameter("source", "Aero.ttf");
	counterFont0->setParameter("size", COUNTER_FONT_SIZE);
	counterFont0->setParameter("resolution", "72");
}

Counter::~Counter()
{
	Ogre::FontManager::getSingleton().destroyResourcePool(COUNTER_FONT_ID);
}

void Counter::start()
{
	if (!m_running)
	{
		m_running = true;

		// initialize graphics
		Ogre::Viewport* const vp{ CCoreEngine::Instance().GetRenderManager().GetOgreViewport() };
		const f32 totalWidth{ static_cast<f32>(vp->getActualWidth()) }, totalHeight{ static_cast<f32>(vp->getActualHeight()) };
		const f32 fontCharHeight{ 32.f };

		const Ogre::Vector2 counterTextureSize{ CCoreEngine::Instance().GetRenderManager().getTextureSize("HUD.png") };
		const Ogre::Vector2 counterDimensions{ counterTextureSize.x, counterTextureSize.y };
		const Ogre::Vector2 counterStart{ -counterDimensions.x, 0 }, counterPos{ MATERIAL_BALL_X, counterStart.y };

		std::vector<Panel::Animation*> counterPanelAnimations{
			new Panel::Animation{ 0.5, counterStart, counterPos }
		};

		Panel::Text* counterCaptionText = new Panel::Text{
			"CounterCaptionText",
			COUNTER_FONT_ID,
			"NeutroBalls",
			{ MATERIAL_BALL_X_TITLE, MATERIAL_BALL_Y_TITLE },
			Ogre::ColourValue::White,
			fontCharHeight,
		};

		Panel::Text* counterText = new Panel::Text{
			"Counter",
			COUNTER_FONT_ID,
			"0",
			{ MATERIAL_BALL_X_NUMBERS, MATERIAL_BALL_Y_NUMBERS },
			Ogre::ColourValue::White,
			fontCharHeight,
			Ogre::TextAreaOverlayElement::Alignment::Center
		};

		Panel::TextsParam counterPanelTexts{
			Panel::TextPairParam{ "CounterCaption", counterCaptionText },
			Panel::TextPairParam{ "Counter", counterText }
		};

		m_counterPanel.reset(new Panel{ "HUD_neutralBalls", counterStart, counterDimensions, counterPanelTexts, counterPanelAnimations });

		m_ballspanelAnimator.reset(new OverlayMaterialAnimator{ *m_counterPanel->getOgrePanel(), "HUD_neutralBalls_glow", .3 });
	}
}

void Counter::update(f64 dt)
{
	// update graphics
	if (m_counterPanel)
	{
		if(auto currentLevel = static_cast<GameLevel*>(CCoreEngine::Instance().GetGameManager().getCurrentLevel()))
		{
			const auto totalNeutralBalls = currentLevel->getTotalNeutralBalls();
			const auto currentNeutralBalls = currentLevel->getCurrentNeutralBalls();
			const auto playerNeutralBalls = totalNeutralBalls - currentNeutralBalls;
			std::string message = to_string(playerNeutralBalls) + "/" + to_string(totalNeutralBalls);
			
			Panel::Text* const text = m_counterPanel->getText("Counter");
			if (text)
			{
				text->setCaption(message);

				if (playerNeutralBalls != m_playerNeutralBalls)
				{
					m_ballspanelAnimator->start();
					m_playerNeutralBalls = playerNeutralBalls;
				}
			}
		}
		m_counterPanel->update(dt);
	
	}

	if (m_ballspanelAnimator)
	{
		m_ballspanelAnimator->update(dt);
	}
}

void Counter::stop()
{
	if (m_running)
	{
		m_running = false;
		m_ballspanelAnimator->stop();
		
		const Ogre::Vector2 counterTextureSize{ CCoreEngine::Instance().GetRenderManager().getTextureSize("HUD.png") };
		const Ogre::Vector2 counterDimensions{ counterTextureSize.x, counterTextureSize.y };
		const Ogre::Vector2 counterStart{ -counterDimensions.x, 0 }, counterPos{ MATERIAL_BALL_X, counterStart.y };

		std::vector<Panel::Animation*> counterPanelAnimations{
			new Panel::Animation{ 0.5, counterPos, counterStart }
		};

		m_counterPanel->setAnimations(counterPanelAnimations);
	}
}

HudElement::tPanelsVector Counter::getPanels() const
{
	return { m_counterPanel.get() };
}