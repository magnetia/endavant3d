#include "Timer.h"
#include "Panel.h"
#include <OGRE/Overlay/OgreOverlaySystem.h>
#include <Core/CCoreEngine.h>
#include <Options/Optionmanager.h>
#include <Utils/tree_xml_utils.h>
#include <Utils/CConversions.h>
#include <Renderer/CRenderManager.h>
#include <Game/GameManager.h>
#include "GameLevelSelector.h"

const static std::string HUD_RECORDS_FILE("Config/records.xml");

Timer::Timer() :
m_running{ false },
m_timerPanel{ nullptr },
m_recordPanel{ nullptr },
m_counters{ { 0, 0, 0 } },
m_recordCounters{ { 0, 0, 0 } },
m_records{},
m_recordPanelActive{ false }
{
	Ogre::FontManager& fontManager = Ogre::FontManager::getSingleton();
	Ogre::FontPtr timerFont1 = fontManager.create("TimerFont1", "General");

	timerFont1->setParameter("type", "truetype");
	timerFont1->setParameter("source", "Aero.ttf");
	timerFont1->setParameter("size", "32");
	timerFont1->setParameter("resolution", "72");

	timerFont1->load();

	Ogre::FontPtr timerFont2 = fontManager.create("TimerFont2", "General");

	timerFont2->setParameter("type", "truetype");
	timerFont2->setParameter("source", "LiberationMono-BoldItalic.ttf");
	timerFont2->setParameter("size", "32");
	timerFont2->setParameter("resolution", "72");

	timerFont2->load();

	Ogre::FontPtr recordFont = fontManager.create("RecordFont", "General");

	recordFont->setParameter("type", "truetype");
	recordFont->setParameter("source", "Aero.ttf");
	recordFont->setParameter("size", "32");
	recordFont->setParameter("resolution", "72");

	recordFont->load();

	// load records file
	edv::tree_from_xml_file(m_records, HUD_RECORDS_FILE, "Records");
}

Timer::~Timer()
{
	Ogre::FontManager::getSingleton().destroyResourcePool("TimerFont");

	// save records file
	edv::tree_to_xml_file(HUD_RECORDS_FILE, "Records", m_records);
}

void Timer::start()
{
	if (!m_running)
	{
		m_running = true;
		
		// #debug:
		OptionManager& optionManager{ CCoreEngine::Instance().GetOptionManager() };

		// initialize timer
		resetCounters();

		// initialize graphics
		Ogre::Viewport* const vp{ CCoreEngine::Instance().GetRenderManager().GetOgreViewport() };
		const f32 totalWidth{ static_cast<f32>(vp->getActualWidth()) }, totalHeight{ static_cast<f32>(vp->getActualHeight()) };
		const f32 fontCharHeight{ 32 };

		const Ogre::Vector2 timerTextureSize{ CCoreEngine::Instance().GetRenderManager().getTextureSize("HUDtemps.png") };
		const Ogre::Vector2 timerDimensions{ timerTextureSize.x, timerTextureSize.y };
		const Ogre::Vector2 timerStart{ totalWidth, 0 }, timerPos{ totalWidth - timerDimensions.x, timerStart.y };

		std::vector<Panel::Animation*> timerPanelAnimations{
			new Panel::Animation{ 0.5, timerStart, timerPos }
		};

		Panel::Text* timerCaptionText = new Panel::Text{
			"TimerCaptionText",
			"TimerFont1",
			"Time",
			{ 0, 0 },
			Ogre::ColourValue::White,
			fontCharHeight,
		};

		Panel::Text* timerText = new Panel::Text{
			"TimerText",
			"TimerFont2",
			countersToString(m_counters),
			{ 0, 0 },
			Ogre::ColourValue::White,
			fontCharHeight,
			Ogre::TextAreaOverlayElement::Alignment::Center
		};

		Panel::TextsParam timerPanelTexts{
			Panel::TextPairParam{ "TimerCaption", timerCaptionText },
			Panel::TextPairParam{ "Timer", timerText }
		};

		m_timerPanel.reset(new Panel{ "HUD_temps", timerStart, timerDimensions, timerPanelTexts, timerPanelAnimations });
		m_timerPanel->getText("TimerCaption")->getTextArea()->setPosition(
			optionManager.getOption("development/timer_caption_pos_x"),
			optionManager.getOption("development/timer_caption_pos_y")
			);
		m_timerPanel->getText("Timer")->getTextArea()->setPosition(
			optionManager.getOption("development/timer_pos_x"),
			optionManager.getOption("development/timer_pos_y")
			);


		//const f32 captionX{ (timerTextureSize.x / 2) - (timerCaptionText->getTextArea()->getWidth() / 2) };
		//const f32 timerX{ (timerTextureSize.x / 2) - (timerText->getTextArea()->getWidth() / 2) };
		//timerCaptionText->getTextArea()->setPosition(captionX - 4, 30);
		//timerText->getTextArea()->setPosition(timerX - 4, 75);

		// record panel
		if (m_records.path_exists(GameLevelSelector::Pointer()->m_levelid))
		{
			loadRecordCounters();

			const Ogre::Vector2 recordTextureSize{ CCoreEngine::Instance().GetRenderManager().getTextureSize("HUDrecord.png") };
			const Ogre::Vector2 recordDimensions{ recordTextureSize.x, recordTextureSize.y };
			const Ogre::Vector2 recordStart{ totalWidth, timerDimensions.y - 25 }, recordPos{ totalWidth - recordDimensions.x, timerDimensions.y - 25 };
			const f32 recordCharHeight{ optionManager.getOption("development/record_char_height") };

			Panel::AnimationsParam recordPanelAnimations{
				new Panel::Animation{ 0.5, recordStart, recordPos }
			};

			Panel::Text* recordCaptionText = new Panel::Text{
				"RecordCaptionText",
				"RecordFont",
				"Record",
				{ 0, 0 },
				Ogre::ColourValue::White,
				recordCharHeight
			};

			Panel::Text* recordText = new Panel::Text{
				"",
				"TimerFont2",
				countersToString(m_recordCounters),
				{ 0, 0 },
				Ogre::ColourValue::White,
				recordCharHeight,
				Ogre::TextAreaOverlayElement::Alignment::Center
			};

			Panel::TextsParam recordPanelTexts{
				Panel::TextPairParam{ "RecordCaption", recordCaptionText },
				Panel::TextPairParam{ "Record", recordText }
			};

			m_recordPanel.reset(new Panel{ "HUD_record", recordStart, recordDimensions, recordPanelTexts, recordPanelAnimations });
			m_recordPanel->getText("RecordCaption")->getTextArea()->setPosition(
				optionManager.getOption("development/record_caption_pos_x"),
				optionManager.getOption("development/record_caption_pos_y")
				);
			m_recordPanel->getText("Record")->getTextArea()->setPosition(
				optionManager.getOption("development/record_pos_x"),
				optionManager.getOption("development/record_pos_y")
				);

			m_recordPanelActive = true;
		}
	}
}

void Timer::update(f64 dt)
{
	// update timer
	Counters::value_type& millis = m_counters[MILLISECONDS];

	millis += dt * 1000;

	if (millis > 999)
	{
		Counters::value_type& seconds = m_counters[SECONDS];

		millis = 0;
		seconds++;

		if (seconds > 59)
		{
			Counters::value_type& minutes = m_counters[MINUTES];

			seconds = 0;
			minutes++;
		}
	}

	// update graphics
	if (m_timerPanel)
	{
		Panel::Text* const text = m_timerPanel->getText("Timer");

		if (text && m_running)
		{
			text->setCaption(countersToString(m_counters));
		}

		m_timerPanel->update(dt);
	}

	if (m_recordPanel)
	{
		if (m_running)
		{
			const Counters::value_type currentTotal = getCountersTotal(m_counters);
			const Counters::value_type recordTotal = getCountersTotal(m_recordCounters);

			if (m_recordPanelActive && recordTotal < currentTotal)
			{
				Ogre::Viewport* const vp = CCoreEngine::Instance().GetRenderManager().GetOgreViewport();
				const f32 totalWidth{ static_cast<f32>(vp->getActualWidth()) }, totalHeight{ static_cast<f32>(vp->getActualHeight()) };
				const Ogre::Vector2 timerDimensions{ m_timerPanel ? m_timerPanel->getDimensions() : Ogre::Vector2::ZERO };
				const Ogre::Vector2 recordStart{ totalWidth - m_recordPanel->getDimensions().x, timerDimensions.y - 25 }, recordPos{ totalWidth, timerDimensions.y - 25 };

				std::vector<Panel::Animation*> recordPanelAnimations{
					new Panel::Animation{ 0.45, recordStart, recordPos }
				};

				m_recordPanel->setAnimations(recordPanelAnimations);
				m_recordPanelActive = false;
			}
		}

		m_recordPanel->update(dt);
	}
}

void Timer::stop()
{
	if (m_running)
	{
		m_running = false;

		updateRecord();
		resetCounters();

		if (m_timerPanel)
		{
			const Ogre::Vector2 timerDimensions{ m_timerPanel->getDimensions() };
			Ogre::Viewport* vp = CCoreEngine::Instance().GetRenderManager().GetOgreViewport();
			f32 totalWidth{ static_cast<f32>(vp->getActualWidth()) }, totalHeight{ static_cast<f32>(vp->getActualHeight()) };
			const Ogre::Vector2 timerStart{ totalWidth - timerDimensions.x, 0 };
			const Ogre::Vector2 timerPos{ (totalWidth / 2) - (timerDimensions.x / 2), (totalHeight / 2) - (timerDimensions.y / 2) };

			std::vector<Panel::Animation*> timerPanelAnimations{
				new Panel::Animation{ 1.5, timerStart, timerPos },
				new Panel::Animation{ 1.5, timerPos, timerPos }
			};

			m_timerPanel->setAnimations(timerPanelAnimations);

			if (m_recordPanelActive)
			{
				const Ogre::Vector2 recordStart{ totalWidth - m_recordPanel->getDimensions().x, timerDimensions.y - 25 };
				const Ogre::Vector2 recordPos{ (totalWidth / 2) - (m_recordPanel->getDimensions().x / 2), timerPos.y - (timerDimensions.y / 2) - (m_recordPanel->getDimensions().y / 2) - 35 };

				std::vector<Panel::Animation*> recordPanelAnimations{
					new Panel::Animation{ 1.5, recordStart, recordPos },
					new Panel::Animation{ 1.5, recordPos, recordPos }
				};

				m_recordPanel->setAnimations(recordPanelAnimations);
			}
		}
	}
}

HudElement::tPanelsVector Timer::getPanels() const
{
	return{ m_timerPanel.get(), m_recordPanel.get() };
}

void Timer::resetCounters()
{
	Counters::value_type& millis = m_counters[MILLISECONDS];
	Counters::value_type& seconds = m_counters[SECONDS];
	Counters::value_type& minutes = m_counters[MINUTES];

	millis = seconds = minutes = 0;
}

void Timer::updateRecord()
{
	if (Level* const currentLevel = CCoreEngine::Instance().GetGameManager().getCurrentLevel())
	{
		const std::string& levelName = GameLevelSelector::Pointer()->m_levelid;
		bool newRecord = false;

		if (m_records.path_exists(levelName))
		{
			const Counters::value_type currentTotal = getCountersTotal(m_counters);
			const Counters::value_type recordTotal = getCountersTotal(m_recordCounters);

			if (currentTotal < recordTotal)
			{
				m_records.get_ref(levelName) = countersToString(m_counters, true);
				newRecord = true;
			}
		}
		else
		{
			m_records.emplace_back({ levelName, countersToString(m_counters, true) });
			newRecord = true;
		}

		OptionManager& optionManager = CCoreEngine::Instance().GetOptionManager();
		optionManager.setOption("timer/level_passed_text", newRecord ? "New Record!" : "Level Complete");
		optionManager.setOption("timer/new_record", newRecord ? "true" : "false");
	}
}

std::string Timer::countersToString(const Counters& a_counters, bool a_printAll) const
{
	std::stringstream ss;
	ss << std::setfill('0') << std::setw(3) << std::to_string(static_cast<s32>(a_counters[MILLISECONDS]));
	const std::string millisStr{ ":" + ss.str() };

	edv::clear_stream(ss);
	ss << std::setfill('0') << std::setw(2) << std::to_string(static_cast<s32>(a_counters[SECONDS]));
	const std::string secondsStr{ ss.str() };

	const Counters::value_type minutes{ a_counters[MINUTES] };
	std::string minutesStr;

	if (a_printAll || minutes > 0)
	{
		edv::clear_stream(ss);
		ss << std::setfill('0') << std::setw(2) << std::to_string(static_cast<s32>(minutes));
		minutesStr = ss.str() + ":";
	}

	return minutesStr + secondsStr + millisStr;
}

Timer::Counters::value_type Timer::getCountersTotal(const Counters& a_counters) const
{
	return
		(a_counters[MINUTES] * 60 * 1000) +
		(a_counters[SECONDS] * 1000) +
		a_counters[MILLISECONDS];
}

void Timer::loadRecordCounters()
{
	// vector of string from one string
	std::vector<std::string> recordStr;
	recordStr.reserve(TIMER_COUNTER_MAX);

	StringExplode(m_records[GameLevelSelector::Pointer()->m_levelid], recordStr, ':');
	std::reverse(recordStr.begin(), recordStr.end());

	// Counters from vector of string
	if (recordStr.size() == m_recordCounters.size())
	{
		for (u32 idx = 0; idx < recordStr.size(); idx++)
		{
			m_recordCounters[idx] = edv::any{ recordStr[idx] }.as<Counters::value_type>();
		}
	}
}