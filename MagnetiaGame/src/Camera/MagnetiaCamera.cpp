#include <Core/CCoreEngine.h>
#include <Input/CInputManager.h>
#include <Options/OptionManager.h>
#include <Game/GameManager.h>
#include <Renderer/CRenderManager.h>
#include "MagnetiaCamera.h"

MagnetiaCamera::MagnetiaCamera(const std::string& a_name) :
	Camera{ a_name },
	m_zIncrement{ CCoreEngine::Instance().GetOptionManager().getOption("camera/z") },
	m_topBorder{ CCoreEngine::Instance().GetOptionManager().getOption("camera/borders/top") },
	m_bottomBorder{ CCoreEngine::Instance().GetOptionManager().getOption("camera/borders/bottom") },
	m_leftBorder{ CCoreEngine::Instance().GetOptionManager().getOption("camera/borders/left") },
	m_rightBorder{ CCoreEngine::Instance().GetOptionManager().getOption("camera/borders/right") },
	m_maxSpeed{ 0 },
	m_minTightness{ CCoreEngine::Instance().GetOptionManager().getOption("camera/min_tightness") },
	m_currentTightness{ m_minTightness },
	m_smoothFactor{ CCoreEngine::Instance().GetOptionManager().getOption("camera/smooth_factor") },
	m_borderMargin{ CCoreEngine::Instance().GetOptionManager().getOption("camera/borders/margin") },
	m_facingLeft{ false },
	m_repositioning{ false }
{
	const Ogre::Vector3 playerPosition{ CCoreEngine::Instance().GetGameManager().getPlayer()->getWorldPosition().to_OgreVector3() };
	const Ogre::Vector3 cameraPosition{ playerPosition.x, playerPosition.y, playerPosition.z + m_zIncrement };

	m_CamNode->attachObject(m_cam);
	m_CamNode->_setDerivedPosition(cameraPosition);
	m_cam->lookAt(playerPosition);
}

MagnetiaCamera::~MagnetiaCamera()
{

}

void MagnetiaCamera::Update(f64 dt)
{
				//const Ogre::Vector3 playerPosition{ CCoreEngine::Instance().GetGameManager().getPlayer()->getWorldPosition().to_OgreVector3() };
				//const Ogre::Vector3 cameraPosition{ playerPosition.x, playerPosition.y, playerPosition.z + m_zIncrement };
				//m_CamNode->_setDerivedPosition(cameraPosition);
				//m_cam->lookAt(playerPosition);
				//return;

	Ogre::Viewport* const vp = CCoreEngine::Instance().GetRenderManager().GetOgreViewport();
	GameEntity* const playerEntity = CCoreEngine::Instance().GetGameManager().getPlayer();
	const f32 leftBorder = m_facingLeft ? vp->getActualWidth() - m_rightBorder : m_leftBorder;
	const f32 rightBorder = m_facingLeft ? vp->getActualWidth() - m_leftBorder : m_rightBorder;
	const f32 topBorder = (vp->getActualHeight() - m_topBorder) / vp->getActualHeight();
	const f32 bottomBorder = m_bottomBorder / vp->getActualHeight();
	const edv::Vector3f leftBorderWorld = m_cam->getCameraToViewportRay(leftBorder, 0).getPoint(-1 * m_zIncrement);
	const edv::Vector3f rightBorderWorld = m_cam->getCameraToViewportRay(rightBorder, 0).getPoint(-1 * m_zIncrement);
	const edv::Vector3f topBorderWorld = m_cam->getCameraToViewportRay(0, topBorder).getPoint(-1 * m_zIncrement);
	const edv::Vector3f bottomBorderWorld = m_cam->getCameraToViewportRay(0, bottomBorder).getPoint(-1 * m_zIncrement);
	const edv::Vector3f playerWorldPosition = playerEntity->getWorldPosition();
	//const MagnetiaActor::Orientation currentOrientation = static_cast<MagnetiaActor*>(playerEntity->getActor())->getOrientation();
	edv::Vector3f movement;

	//LOG(LOG_DEVEL, LOGSUB_ENGINE, "screenBorders: %d %d", leftBorder, rightBorder);
	//LOG(LOG_DEVEL, LOGSUB_ENGINE, "leftBorderWorld: %f %f %f", leftBorderWorld.x, leftBorderWorld.y, leftBorderWorld.z);
	//LOG(LOG_DEVEL, LOGSUB_ENGINE, "rightBorderWorld: %f %f %f", rightBorderWorld.x, rightBorderWorld.y, rightBorderWorld.z);
	//LOG(LOG_DEVEL, LOGSUB_ENGINE, "playerWorldPosition: %f %f %f", playerWorldPosition.x, playerWorldPosition.y, playerWorldPosition.z);
	//LOG(LOG_DEVEL, LOGSUB_ENGINE, "facing left: %s", m_facingLeft? "true" : "false");
	//LOG(LOG_DEVEL, LOGSUB_ENGINE, "repositioning: %s", m_repositioning? "true" : "false");

	if (m_repositioning && m_currentTightness < 1.)
	{
		m_currentTightness = std::min(m_currentTightness + static_cast<f32>(dt) / m_smoothFactor, 1.f);
		//LOG(LOG_DEVEL, LOGSUB_ENGINE, "current tightness: %f", m_currentTightness);
	}

	if (m_facingLeft)
	{
		if (playerWorldPosition.x > leftBorderWorld.x - m_borderMargin && playerWorldPosition.x < leftBorderWorld.x + m_borderMargin)
		{
			m_repositioning = false;
		}
		else if (playerWorldPosition.x < leftBorderWorld.x)
		{
			movement.x = -1 * (leftBorderWorld.x - playerWorldPosition.x) * (m_repositioning ? m_currentTightness : 1);
		}
		else if (playerWorldPosition.x > rightBorderWorld.x + m_borderMargin)
		{
			m_repositioning = true;
			//LOG(LOG_DEVEL, LOGSUB_ENGINE, "repositioning: TRUE");
			m_currentTightness = m_minTightness;
			m_facingLeft = false;
		}
	}
	else
	{
		if (playerWorldPosition.x < rightBorderWorld.x + m_borderMargin && playerWorldPosition.x > rightBorderWorld.x - m_borderMargin)
		{
			m_repositioning = false;
		}
		else if (playerWorldPosition.x > rightBorderWorld.x + m_borderMargin)
		{
			movement.x = (playerWorldPosition.x - rightBorderWorld.x) * (m_repositioning ? m_currentTightness : 1);
		}
		else if (playerWorldPosition.x < leftBorderWorld.x - m_borderMargin)
		{
			m_repositioning = true;
			//LOG(LOG_DEVEL, LOGSUB_ENGINE, "repositioning: TRUE");
			m_currentTightness = m_minTightness;
			m_facingLeft = true;
		}
	}

	//LOG(LOG_DEVEL, LOGSUB_ENGINE, "playerWorldPosition: %f %f %f", playerWorldPosition.x, playerWorldPosition.y, playerWorldPosition.z);

	if (playerWorldPosition.y > topBorderWorld.y)
	{
		movement.y = playerWorldPosition.y - topBorderWorld.y;
	}
	else if (playerWorldPosition.y < bottomBorderWorld.y)
	{
		movement.y = -1 * (bottomBorderWorld.y - playerWorldPosition.y);
	}

	if (!movement.isZero())
	{
		m_CamNode->translate(movement.to_OgreVector3());
	}
}

void MagnetiaCamera::OnActivate()
{
	auto &mouse = CCoreEngine::Instance().GetInputManager().GetMouse();
	mouse.SetRelativeBehaviour(true);
}

void MagnetiaCamera::OnDeactivate()
{
	auto &mouse = CCoreEngine::Instance().GetInputManager().GetMouse();
	mouse.SetRelativeBehaviour(false);
}
