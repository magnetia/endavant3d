#ifndef MAGNETIA_CAMERA_MAGNETIA_CAMERA_H_
#define MAGNETIA_CAMERA_MAGNETIA_CAMERA_H_

#include <Camera/Camera.h>
#include <Utils/edvVector3.h>
#include "Actors/MagnetiaActor.h"

class MagnetiaCamera : public Camera
{
public:
	MagnetiaCamera(const std::string& a_name);
	~MagnetiaCamera();

	void Update(f64 dt) override;
	void OnActivate() override;
	void OnDeactivate() override;

private:
	f32				m_zIncrement;
	f32				m_topBorder;
	f32				m_bottomBorder;
	f32				m_leftBorder;
	f32				m_rightBorder;
	f32				m_maxSpeed;
	f32				m_minTightness;
	f32				m_currentTightness;
	f32				m_smoothFactor;
	f32				m_borderMargin;

	bool			m_facingLeft;
	bool			m_repositioning;
};

#endif
