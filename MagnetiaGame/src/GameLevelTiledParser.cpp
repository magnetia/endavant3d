#include "GameLevelTiledParser.h"
#include "Core/CLogManager.h"
#include "Core/CCoreEngine.h"
#include "Renderer/CRenderManager.h"
#include "OGRE/OgreMeshManager.h"
#include "Utils/tree_xml_utils.h"
#include "Utils/CConversions.h"
#include <algorithm>

// Tiled.tmx tags
static const char map_tag[] = "map";
static const char width_tag[] = "width";
static const char height_tag[] = "height";
static const char tilewidth_tag[] = "tilewidth";
static const char tileheight_tag[] = "tileheight";
static const char tileset_tag[] = "tileset";
static const char tilesetcfgfile_tag[] = "tilesetconfigfile";
static const char id_tag[] = "id";
static const char tile_tag[] = "tile";
static const char properties_tag[] = "properties";
static const char property_tag[] = "property";
static const char name_tag[] = "name";
static const char type_tag[] = "type";
static const char value_tag[] = "value";
static const char layer_tag[] = "layer";
static const char data_tag[] = "data";
static const char gid_tag[] = "gid";
static const char objectgroup_tag[] = "objectgroup";
static const char object_tag[] = "object";
static const char x_tag[] = "x";
static const char y_tag[] = "y";
static const char polyline_tag[] = "polyline";
static const char points_tag[] = "points";
static const char polygon_tag[] = "polygon";
static const char ellipse_tag[] = "ellipse";
static const char mesh_property[] = "mesh";
static const char scale_property[] = "tiledimensions";
static const char type_property[] = "type";
static const char subtype_property[] = "subtype";
static const char firstgid_tag[] = "firstgid";

//Descripcio de l'XML
const static std::string LEVELS_DESCRIPTION_XML{ "Config/levels.xml" };
const static std::string LEVELS_TAG_XML{ "levels" };
const static std::string LEVEL_TAG_XML{ "level" };
const static std::string LEVEL_NAME_TAG_XML{ "name" };
const static std::string LEVEL_SELECTABLE_TAG_XML{ "selectable" };
const static std::string LEVEL_TILED_TAG_XML{ "tiled_path" };
const static std::string LEVEL_MUSIC_TAG_XML{ "background_music" };
const static std::string LEVEL_SKYBOX_TAG_XML{ "skybox" };

// Tilesets configuration files
static const std::string TILESET_CONFIG_MAINNODE{ "TilesetConfig" };

std::string GetGameLevelProperty(const std::string &a_propertytosearch, const tGameLevelPropertiesMap &a_prop_map)
{
	auto it = a_prop_map.find(a_propertytosearch);
	if (it != a_prop_map.end())
		return it->second;
	else
		return "";
}

bool	GameLevelPropertyExists(const std::string &a_propertytosearch, const tGameLevelPropertiesMap &a_prop_map)
{
	auto it = a_prop_map.find(a_propertytosearch);
	if (it != a_prop_map.end())
		return true;
	else
		return false;
}




GameLevelTiledParser::GameLevelTiledParser()
{
}

void GameLevelTiledParser::ParseAllLevels()
{
	LoadGameLevelsInfoFromXML();
	LoadTiledMaps();
}

GameLevelTiledParser::tGameLevelXMLInfo GameLevelTiledParser::GetAllLevelsXMLInfo()
{
	return m_levelsXMLinfo;
}

void	GameLevelTiledParser::ParseLevel(const std::string &a_lvlname)
{
	bool found = false;
	std::shared_ptr<GameLevelXMLInfo> xmlinfo;
	for (auto &mapXMLinfo : m_levelsXMLinfo)
	{
		if (mapXMLinfo->name == a_lvlname)
		{
			found = true;
			xmlinfo = mapXMLinfo;
		}
	}

	if (!found)
		throw std::runtime_error("ParseLevel Level: " + a_lvlname + " Not Found!!!");
	

	

	auto lvlinfobuffer = std::make_shared<GameLevelRawInfo>();

	lvlinfobuffer->m_backgroundmusic = xmlinfo->background_music;
	lvlinfobuffer->m_mapname = xmlinfo->name;
	lvlinfobuffer->m_tiledfilepath = xmlinfo->tiled_pathfilename;
	lvlinfobuffer->m_skyboxmaterialname = xmlinfo->skyboxmaterialname;
	lvlinfobuffer->m_tutorial = xmlinfo->tutorial;

	auto itmap = m_levelsinfo.find(lvlinfobuffer->m_mapname);

	if (itmap != m_levelsinfo.end())
	{
		//Si ja hi ha un, peto l'estructura i el torno a llegir
		itmap->second.reset();
	}


	m_levelsinfo[lvlinfobuffer->m_mapname] = lvlinfobuffer;

	LoadTiledMap(a_lvlname);
}

u32		GameLevelTiledParser::GetLevelsCount()
{
	return m_levelsinfo.size();
}

GameLevelTiledParser::tGameLevelRawInfo GameLevelTiledParser::GetLevelRawInfo(const std::string &a_lvlname)
{
	auto it = m_levelsinfo.find(a_lvlname);
	if (it == m_levelsinfo.end())
		return std::make_shared<GameLevelRawInfo>();
	else
		return it->second;
	
}

/*
tGameLevelInfoRawMap GameLevelTiledParser::GetAllLevelsRawInfo()
{
	return m_levelsinfo;
}

*/

void GameLevelTiledParser::setLevelSelectable(const std::string& a_level_name, bool a_selectable)
{
	for (auto& level_info : m_levelsXMLinfo)
	{
		if (level_info->name == a_level_name)
		{
			level_info->isselectable = a_selectable;
		}
	}
}

bool GameLevelTiledParser::isLevelSelectable(const std::string& a_level_name) const
{
	bool selectable = false;

	for (auto& level_info : m_levelsXMLinfo)
	{
		if (level_info->name == a_level_name)
		{
			selectable = level_info->isselectable;
		}
	}

	return selectable;
}

void GameLevelTiledParser::saveGameLevelsInfoToXML() const
{
	edv::tree<edv::any> levels;

	for (auto& level_info : m_levelsXMLinfo)
	{
		edv::tree<edv::any> level{ level_info->id };

		level.emplace_back({ "name", level_info->name });
		level.emplace_back({ "tiled_path", level_info->tiled_pathfilename });
		level.emplace_back({ "skybox", level_info->skyboxmaterialname });
		level.emplace_back({ "selectable", level_info->isselectable });
		level.emplace_back({ "tutorial", level_info->tutorial });
		level.emplace_back({ "background_music", level_info->background_music });

		levels.emplace_back(std::move(level));
	}

	edv::tree_to_xml_file(LEVELS_DESCRIPTION_XML, "Levels", levels);
}

void GameLevelTiledParser::LoadGameLevelsInfoFromXML()
{
	m_levelsXMLinfo.clear();

	edv::tree<edv::any> xml_info;
	edv::tree<edv::any>::keys_type level_keys;

	edv::tree_from_xml_file(xml_info, LEVELS_DESCRIPTION_XML, "Levels");
	level_keys = xml_info.keys();

	for (const edv::tree<edv::any>::keys_type::value_type& key : level_keys)
	{
		auto lvl_xmlinfo = std::make_shared<GameLevelXMLInfo>();
		lvl_xmlinfo->id = key;
		lvl_xmlinfo->name = xml_info[key + "/name"].as<std::string>();
		lvl_xmlinfo->tiled_pathfilename = xml_info[key + "/tiled_path"].as<std::string>();
		lvl_xmlinfo->background_music = xml_info[key + "/background_music"].as<std::string>();
		lvl_xmlinfo->isselectable = xml_info[key + "/selectable"];
		lvl_xmlinfo->skyboxmaterialname = xml_info[key + "/skybox"].as<std::string>();
		lvl_xmlinfo->tutorial = xml_info[key + "/tutorial"].as<bool>();

		if (std::find(m_levelsXMLinfo.begin(), m_levelsXMLinfo.end(), lvl_xmlinfo) == m_levelsXMLinfo.end())
		{
			m_levelsXMLinfo.push_back(lvl_xmlinfo);
		}
		else
		{
			throw std::runtime_error(lvl_xmlinfo->name + " exists!");
		}
	}
}

void GameLevelTiledParser::LoadTiledMaps()
{
	//Descarreguem tots els mapes
	m_levelsinfo.clear();

	// Per cada mapa carregat de l'XML carrego un mapa
	for (auto &mapXMLinfo : m_levelsXMLinfo)
	{
		ParseLevel(mapXMLinfo->name);
	}

}



void GameLevelTiledParser::LoadTiledMap(const std::string &a_mapname)
{
	auto curr_info = m_levelsinfo.find(a_mapname);
	if (curr_info == m_levelsinfo.end())
	{
		throw std::runtime_error("LoadTiledMap map: " + a_mapname + "not exists");
	}
	else
	{
		auto curr_mapinfo = curr_info->second;

		// Check del fitxer de tiled si existeix
		pugi::xml_document tiledxml_doc;
		pugi::xml_parse_result result = tiledxml_doc.load_file(curr_mapinfo->m_tiledfilepath.c_str());
		if (!result)
		{
			LOG(LOG_ERROR, LOGSUB_PARSER, "[ %s ] not found!", curr_mapinfo->m_tiledfilepath.c_str());
			throw std::runtime_error(curr_mapinfo->m_tiledfilepath + " Not FOUND!!!!");
			return;
		}
	
		// map attributes
		pugi::xml_node map_node = tiledxml_doc.child(map_tag);
		
		curr_mapinfo->m_tilecolumnscount = map_node.attribute(width_tag).as_int();
		curr_mapinfo->m_tilerowscount = map_node.attribute(height_tag).as_int();
		f32 l_tilewidth = map_node.attribute(tilewidth_tag).as_float();
		f32 l_tileheight = map_node.attribute(tileheight_tag).as_float();

		// map properties
		pugi::xml_node properties_node = map_node.child(properties_tag);
		pugi::xml_node property_node = properties_node.child(property_tag);
		for (; property_node; property_node = property_node.next_sibling(property_tag))
		{
			curr_mapinfo->m_globalproperties[property_node.attribute(name_tag).as_string()] = property_node.attribute(value_tag).as_string();
		}

		curr_mapinfo->m_tiledimensions = static_cast<float>((curr_mapinfo->m_globalproperties.find(scale_property) != curr_mapinfo->m_globalproperties.end()) ? atof(curr_mapinfo->m_globalproperties.at(scale_property).c_str()) : 10.);

		// Carreguem Layers
		pugi::xml_node layer_node = map_node.child(layer_tag);
		for (; layer_node; layer_node = layer_node.next_sibling(layer_tag))
		{
			// Guardo opcions de layer globals
			auto levellayerbuffer = std::make_shared<GameLevelLayer>();
			levellayerbuffer->m_name = layer_node.attribute(name_tag).as_string();
			//levellayerbuffer->m_width = layer_node.attribute(width_tag).as_int();
			//levellayerbuffer->m_height = layer_node.attribute(height_tag).as_int();

			
			// Guardo Properties de layer globals
			pugi::xml_node properties_node = layer_node.child(properties_tag);
			pugi::xml_node property_node = properties_node.child(property_tag);
			for (; property_node; property_node = property_node.next_sibling(property_tag))
				levellayerbuffer->m_layerGlobalProperties[property_node.attribute(name_tag).as_string()] = property_node.attribute(value_tag).as_string();
			
			s32 firstguid = -1;
			std::string tilesetXMLfile;

			// Carrego el tileset de la layer
			auto tilesetcfg_it = levellayerbuffer->m_layerGlobalProperties.find(tileset_tag);
			if (tilesetcfg_it == levellayerbuffer->m_layerGlobalProperties.end())
			{
				throw std::runtime_error("Layer " + levellayerbuffer->m_name + " can't find 'tileset' config tag in global layer properties");
			}
			else
			{
				// Bussco quin firstguid i el fitxer que haig de carregar del tileset te la tileset en concret
				pugi::xml_node tileset_node = map_node.child(tileset_tag);
				
				for (; tileset_node && firstguid == -1; tileset_node = tileset_node.next_sibling(tileset_tag))
				{
					//Comprovo si es el tileset de la layer
					if (tileset_node.attribute(name_tag).as_string() == tilesetcfg_it->second)
					{
						firstguid = tileset_node.attribute(firstgid_tag).as_uint();
						if (firstguid == -1)
							throw std::runtime_error("Layer " + levellayerbuffer->m_name + " can't find tileset: " + tilesetcfg_it->second + " in Tiled map file");

						// Agafo les propietats globals del Tileset (el tilesetconfigfile nomes ) //TODO
						pugi::xml_node properties_node = tileset_node.child(properties_tag);
						pugi::xml_node property_node = properties_node.child(property_tag);
						for (; property_node; property_node = property_node.next_sibling(property_tag))
						{
							//levellayerbuffer->m_layerGlobalProperties[property_node.attribute(name_tag).as_string()] = property_node.attribute(value_tag).as_string();
							std::string s = property_node.attribute(name_tag).as_string();
							if (!std::string(tilesetcfgfile_tag).compare(s))
								tilesetXMLfile = property_node.attribute(value_tag).as_string();

						}

						if (tilesetXMLfile.empty())
							throw std::runtime_error("Layer " + levellayerbuffer->m_name + " can't find tag: " + tilesetcfgfile_tag + " in tileset " + tilesetcfg_it->second );
					}

				}

				

				// carrego el XML de configuracio de les tiles
				pugi::xml_document document;
				pugi::xml_parse_result result = document.load_file(tilesetXMLfile.c_str());
				if (!result)
					throw std::runtime_error("Layer " + levellayerbuffer->m_name + " can't load tileset config file: " + tilesetcfg_it->second);
				else
				{
					edv::tree<edv::any> tilesetconfig;
					edv::tree_from_xml(tilesetconfig, document.child(TILESET_CONFIG_MAINNODE.c_str()));
					
					auto tileid_keys = tilesetconfig.keys();
					for (auto key : tileid_keys)
					{
						auto tile_properties_tree = tilesetconfig.get_tree(key);
						auto tile_properties_keys = tile_properties_tree.keys();
						auto properties_buffer = std::make_shared<tGameLevelPropertiesMap>();
						for (auto property : tile_properties_keys)
						{
							if (properties_buffer->find(property) == properties_buffer->end())
								properties_buffer->emplace(property, tile_properties_tree[property].as<std::string>());
							else
								throw std::runtime_error("In " + tilesetXMLfile + " Node name:  " + key + " Property: " + property + " Is Duplicated !!");
						}

						u32 key_num = StringToNumber(std::string(key));
						if (levellayerbuffer->m_layerTileIDProperties.find(key_num + firstguid) == levellayerbuffer->m_layerTileIDProperties.end())
						{
							levellayerbuffer->m_layerTileIDProperties.emplace(key_num + firstguid, std::move(properties_buffer));
						}
						else
							throw std::runtime_error("In " + tilesetXMLfile + " Node name:  " + key + " Is Duplicated !!");
					}
				}

				


			}


		

			// Carrego les tiles
			pugi::xml_node data_node = layer_node.child(data_tag);
			pugi::xml_node tile_node = data_node.child(tile_tag);
			for (u32 id_tile = 0; tile_node; tile_node = tile_node.next_sibling(tile_tag))
			{
				auto id_tilesetcfgid = tile_node.attribute(gid_tag).as_uint();
				if (id_tilesetcfgid != 0)
				{
					//Busco les propietats de la tile segons les propietats del tileset XML
					auto it_tileProperties = levellayerbuffer->m_layerTileIDProperties.find(id_tilesetcfgid);
					if (it_tileProperties == levellayerbuffer->m_layerTileIDProperties.end())
						throw std::runtime_error("In Layer: " + levellayerbuffer->m_name + " Tile number: " + to_string(id_tile - firstguid) + " with gid: " + to_string(id_tilesetcfgid) + " Not found in tilesetcfgXMLfile: " + tilesetXMLfile);
					else
					{
						auto my_pair = std::make_pair(id_tile, it_tileProperties->second);
						levellayerbuffer->m_tileIds.push_back(my_pair);
					}


				}
				id_tile++;
			}

			//Afegeixo la layer a la informacio del mapa
			curr_mapinfo->m_layers.emplace(levellayerbuffer->m_name, std::move(levellayerbuffer) );
		
			

		}


		// Despres de carregar les layers les layers objectgroup (linies, cubs, etc)
		pugi::xml_node objectgroup_node = map_node.child(objectgroup_tag);
		for (; objectgroup_node; objectgroup_node = objectgroup_node.next_sibling(objectgroup_tag))
		{
			auto object_group = std::make_shared<GameLevelPolyObjectsLayer>();
			
			object_group->m_name = objectgroup_node.attribute(name_tag).as_string();
			object_group->m_width = objectgroup_node.attribute(width_tag).as_int();
			object_group->m_height = objectgroup_node.attribute(height_tag).as_int();

			pugi::xml_node properties = objectgroup_node.child(properties_tag);
			pugi::xml_node property_node = properties.child(property_tag);

			
			for (; property_node; property_node = property_node.next_sibling(property_tag))
				object_group->m_globalProperties[property_node.attribute(name_tag).as_string()] = property_node.attribute(value_tag).as_string();
			
			// TODO POSAR ELS QUADRATS TODO TODO AQUI UN SWITCH DEPEN DE SI ES UNA POLYLINE O UN QUADRAT
			pugi::xml_node object_node = objectgroup_node.child(object_tag);
			u32 objectUid = 0;
			for (; object_node; object_node = object_node.next_sibling(object_tag), objectUid++)
			{
				auto object_poly = std::make_shared<GameLevelPolyObject>();
				
				std::string name = object_node.attribute(name_tag).as_string();
				object_poly->m_name = name.empty() ? "PhysicsObject" + to_string(objectUid) : name;
				object_poly->m_type = object_node.attribute(type_tag).as_string();

				f32 pointx = (object_node.attribute(x_tag).as_float() / l_tilewidth) * curr_mapinfo->m_tiledimensions;
				f32 pointy = (object_node.attribute(y_tag).as_float() / l_tileheight) * curr_mapinfo->m_tiledimensions;
				pointy = (curr_mapinfo->m_tilerowscount * curr_mapinfo->m_tiledimensions) - pointy;  //Situo el eix y abaix per fer 0.0 abaix esquerra

				std::string buffer = object_node.child(polyline_tag).attribute(points_tag).as_string();
				std::vector<std::string> points;
				std::istringstream is(buffer);
				std::string s;
				while (std::getline(is, s, ' '))
				{
					f32 buffx;
					f32 buffy;
					std::istringstream iss(s);
					iss >> buffx;
					std::getline(iss, s, ',');
					iss >> buffy;
										
					buffx = (buffx / l_tilewidth) * curr_mapinfo->m_tiledimensions;
					buffx += pointx;
					buffy = -(buffy / l_tileheight) * curr_mapinfo->m_tiledimensions;
					buffy += pointy;

					auto my_pair = std::make_pair(buffx, buffy);
					object_poly->m_points.push_back(my_pair);
				}
				object_group->m_polyobjects.emplace(object_poly->m_name, std::move(object_poly));
							
			}

			curr_mapinfo->m_layersobjects.emplace(object_group->m_name, std::move(object_group));
			
		}
	}
}

bool GameLevelTiledParser::ExistsLevel(const std::string &a_lvlname)
{
	auto found = m_levelsinfo.find(a_lvlname);

	if (found == m_levelsinfo.end())
		return false;
	else
		return true;

}
/*
const std::vector<const std::string> GameLevelTiledParser::GetAllLevelIDs() const
{
	std::vector<const std::string> vec;
	for (auto map : m_levelsinfo)
		vec.push_back(map.first);
	
	return vec;
}
*/

	