#ifndef MAGNETIA_PAUSED_GAME_STATE_H_
#define MAGNETIA_PAUSED_GAME_STATE_H_

#include "State/CState.h"
#include "Utils/CSingleton.h"
#include "GUI/BoardMenuManager.h"

class PausedGameState : public CState, public CSingleton<PausedGameState>
{
public:
	PausedGameState();
	~PausedGameState();

	void Start() override;
	void Finish() override;
	void Pause() override;
	void Resume() override;
	void Update(f64 dt) override;

};

#endif
