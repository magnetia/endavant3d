#ifndef CMAINGAMESTATE_H_
#define CMAINGAMESTATE_H_

#include <memory>
#include <State/CState.h>
#include <Utils/CSingleton.h>
#include <Camera/CCameraControllerFree.h>
#include <Camera/CCameraFollower.h>

class GameLevelSelector;

class CMainGameState : public CState, public CSingleton<CMainGameState>
{
public:
	CMainGameState();
	~CMainGameState();

	void Start() override;
	void Finish() override;
	void Pause() override;
	void Resume() override;
	void Update(f64 dt) override;
};

#endif
