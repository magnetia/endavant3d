#include <Core/CCoreEngine.h>
#include <Options/OptionManager.h>
#include <Renderer/CRenderManager.h>
#include <OGRE/Overlay/OgreFontManager.h>
#include <Utils/any.h>
#include "GameLevelSelector.h"
#include "LevelAnimation.h"

LevelAnimation::LevelAnimation(const std::string& a_name) :
	m_name{ a_name }, 
	m_overlay{ nullptr },
	m_panels{ }
{
	m_overlay = Ogre::OverlayManager::getSingleton().create(a_name);
}

LevelAnimation::~LevelAnimation()
{
	Ogre::OverlayManager& overlayManager = Ogre::OverlayManager::getSingleton();

	for (Panels::const_iterator it = m_panels.begin(); it != m_panels.end(); it++)
	{
		Panel* const panel = it->get();

		m_overlay->remove2D(panel->getOgrePanel());
	}

	overlayManager.destroy(m_overlay);
}

void LevelAnimation::start()
{
	m_panels.clear();

	startImpl();

	// configure and show overlay
	m_overlay->setZOrder(CCoreEngine::Instance().GetOptionManager().getOption("overlay/z_order/LevelAnimation"));
	m_overlay->show();
}

void LevelAnimation::update(f64 dt)
{
	for (auto& panel : m_panels)
	{
		panel->update(dt);
	}
}

bool LevelAnimation::finished() const
{
	bool returnValue{ true };
	
	for (auto& panel : m_panels)
	{
		returnValue = returnValue && panel->finished();
	}

	return returnValue;
}

void LevelAnimation::addPanel(Panel* a_panel)
{
	m_panels.emplace_back(a_panel);
	m_overlay->add2D(a_panel->getOgrePanel());
}

void LevelAnimation::createFont(const std::string& a_name, const std::string& a_type, const std::string& a_source, s32 a_size, s32 a_resolution)
{
	Ogre::FontManager& fontManager = Ogre::FontManager::getSingleton();

	if (!fontManager.resourceExists(a_name))
	{
		Ogre::FontPtr font = fontManager.create(a_name, "General");

		font->setParameter("type", a_type);
		font->setParameter("source", a_source);
		font->setParameter("size", edv::any{ a_size });
		font->setParameter("resolution", edv::any{ a_resolution });
		font->setParameter("character_spacer", edv::any{ 25 });

		font->load();
	}
}
