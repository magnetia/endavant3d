#ifndef GAME_WORLD_H_
#define GAME_WORLD_H_

#include <Core/CBasicTypes.h>
#include <Renderer/CRenderManager.h>
#include <Utils/CXMLParserPUGI.h>


#include <Game/GameEntity.h>
#include <Game/Level.h>
#include <map>
#include <memory>
#include <vector>
#include <array>
#include "GameLevelHUD.h"
#include "GameLevelTiledParser.h"

#include "LevelAnimation.h"

class Time;
class Counter;
class BossLifeDrawer;
class Help;

class GameLevel : public Level
{
public:

	enum eLevelNode
	{
		SCENARIO_NODE,
		ACTORS_NODE,
		COLLISION_NODE,
		HUD_NODE,
		LEVEL_NODES_NUMBER
	};

	GameLevel(const GameLevelTiledParser::tGameLevelRawInfo &a_lvlinfo);
	~GameLevel();

	void load();
	void unload();
	void pause();
	void resume();
	void update(f64 dt);
	Ogre::SceneNode*	getLevelSceneNode();
	Ogre::SceneNode *	getPlayerSceneNode();
	Ogre::SceneNode*	getLevelChildSceneNode(eLevelNode aLevelNode);
	void				setVisibleLevelChildSceneNode(eLevelNode aLevelNode, bool aVisible);
	u32					getCurrentNeutralBalls() const;

	void destroyItem(const std::string& a_name);
	void destroyEnemy(const std::string& a_name);
	bool isLevelEnded() const;
	void setPlayerAtEnd(bool a_player_at_end);
	bool isPlayerAtEnd() const;
	bool isPlayerDied() const;
	bool isPlayerWon() const;
	bool isLevelLost() const;
	bool isLevelWon() const;

	u32 getTotalNeutralBalls() const;
	typedef std::vector<edv::Vector3f> BossPathPoints;
	BossPathPoints getBossPathPoints(const std::string& a_layerName);

private:
	enum eLevelState
	{
		LEVEL_STATE_NONE,
		LEVEL_STATE_START_ANIMATION,
		LEVEL_STATE_GAME,
		LEVEL_STATE_LOSE_ANIMATION,
		LEVEL_STATE_WIN_ANIMATION,
		LEVEL_STATE_LOST,
		LEVEL_STATE_WON,

		LEVEL_STATE_MAX,
	};
	
	// Generacio del nivell
	GameLevelTiledParser::tGameLevelRawInfo m_tiledRawinfo;
	void CreateLevel();
	void CreateLevelStaticMesh(const u32 a_tileposition, const std::shared_ptr<const tGameLevelPropertiesMap> a_tileproperties, const std::shared_ptr<const GameLevelLayer> a_layer);
	void CreateLevelMesh(const u32 a_tileposition, const std::shared_ptr<const tGameLevelPropertiesMap> a_tileproperties, const std::shared_ptr<const GameLevelLayer> a_layer);
	void CreateLevelActor(const u32 a_tileposition, const std::shared_ptr<const tGameLevelPropertiesMap> a_tileproperties, const std::shared_ptr<const GameLevelLayer> a_layer);
	void CreateLevelCollisions(const std::shared_ptr<const GameLevelPolyObjectsLayer> a_objlayer);
	typedef std::list<CRenderManager::Compositor::Id>	Compositors;

	void DestroyLevel();


	void init();
	void deinit();


	void CreateSkyBox();
	void CreateHud();
	void CreateCameras();
	void DestroyCameras();
	void CreateCompositors();
	void DestroyCompositors();
	
	void removePendingEntities();
	Ogre::StaticGeometry* addStaticMesh();

	LevelAnimation* getCurrentAnimation() const;

	
	Ogre::SceneNode*									m_level_node;
	std::array<Ogre::SceneNode*, LEVEL_NODES_NUMBER>	m_level_nodes;

	typedef std::map<std::string, GameEntity*> tGameEntitiesMap;


	// game world static geometry
	std::size_t m_static_entities_count;
	std::vector<Ogre::StaticGeometry*> m_level_static_geometry;

	// level actors entities map
	typedef std::map<std::string, tGameEntitiesMap > tActorEntitiesMap;
	tActorEntitiesMap			m_actorsmap;
	GameEntity*					m_player;
	std::map<std::string, std::string>	m_entitiesToRemove;

	
	// physical level game entities
	GameLevelHUD				m_hud;
	Compositors					m_compositors;

	//MERDA
	std::vector<std::string> m_meshresourcestoremove;

	eLevelState					m_levelState;
	bool						m_player_at_end;

	typedef std::array<LevelAnimation*, LEVEL_STATE_MAX>	Animations;
	Animations					m_animations;

	// Hud Elements
	typedef std::unique_ptr<Timer>			TimerPtr;
	typedef std::unique_ptr<Counter>		CounterPtr;
	typedef std::unique_ptr<BossLifeDrawer>	BossLifeDrawerPtr;
	typedef std::unique_ptr<Help>			HelpPtr;

	TimerPtr					m_hudTimer;
	CounterPtr					m_hudCounter;
	BossLifeDrawerPtr			m_hudBossLife;
	HelpPtr						m_hudHelp;
};

#endif
