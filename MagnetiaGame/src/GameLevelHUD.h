#ifndef MAGNETIA_GAME_HUD_H_
#define MAGNETIA_GAME_HUD_H_

#include <memory>
#include <map>
#include <array>
#include <OGRE/Ogre.h>
#include <OGRE/Overlay/OgreOverlaySystem.h>
#include <Core/CBasicTypes.h>
#include <Utils/CXMLParserPUGI.h>
#include <Utils/tree.h>
#include <Utils/any.h>
#include "Panel.h"
#include "Timer.h"
#include "Counter.h"
#include "BossLifeDrawer.h"

class HudElement;

class GameLevelHUD
{
public:
	GameLevelHUD();
	~GameLevelHUD();

	void addElement(HudElement* a_hudElement);

	void init();
	void stop();

	void update(f64 dt);

private:
	Ogre::Overlay*          m_overlay;
	typedef std::vector<HudElement*> tHudElementsVector;
	tHudElementsVector m_hudElements;
};

#endif
