#include <functional>
#include "Core/CCoreEngine.h"
#include "Input/CInputManager.h"
#include "Renderer/CRenderManager.h"
#include "Physics/CPhysicsManager.h"
#include "Physics/COgreMotionState.h"
#include "Sound/CSoundManager.h"
#include "Game/FlyStraightAnimator.h"
#include "Externals/MovableText/MovableText.h"
#include "BoardMenuManager.h"
#include "BoardMenuLevelSelector.h"
#include "ChangeStateButton.h"
#include "OptionButton.h"
#include "InputButton.h"
#include "Game\GameManager.h"

// #todo: millorable en molts aspectes

BoardMenuLevelSelector::BoardMenuLevelSelector(pugi::xml_node& a_node, BoardMenuManager* a_parent, const Ogre::Vector3& a_buttonsOffset, f32 a_animDuration) :
BoardMenu(a_node, a_parent, a_buttonsOffset, a_animDuration)
{

	// add buttons
	Ogre::Vector3 origin{
		a_node.attribute("origin_x").as_float(0.f),
		a_node.attribute("origin_y").as_float(6.5f),
		a_node.attribute("origin_z").as_float(15.f) };

	
	auto levels = CCoreEngine::Instance().GetGameManager().getLevelsRegistered();

	

	

}

void BoardMenuLevelSelector::Start()
{
	BoardMenu::Start();
}

void BoardMenuLevelSelector::Update(f64 dt)
{
	BoardMenu::Update(dt);
}

void BoardMenuLevelSelector::Finish()
{
	BoardMenu::Finish();

	
	
}

void BoardMenuLevelSelector::Pause()
{
	BoardMenu::Finish();
}

void BoardMenuLevelSelector::Resume()
{
	BoardMenu::Finish();
}
