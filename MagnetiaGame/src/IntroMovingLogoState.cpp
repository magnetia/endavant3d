#include <Core/CCoreEngine.h>
#include <Renderer/CRenderManager.h>
#include <State/CMainStateManager.h>
#include <Time/CTimeManager.h>
#include <Options/OptionManager.h>
#include <Sound/CSoundManager.h>
#include "IntroMovingLogoState.h"

IntroMovingLogoState::IntroMovingLogoState() :
	CState{ "IntroMovingLogoState" },
	m_duration{ 0. },
	m_startTimestamp{ 0 },
	m_panel{ nullptr },
	m_newLogoPanel{ nullptr },
	m_overlay{ nullptr },
	m_soundPoint{ 0. },
	m_soundPlayed{ false }
{

}

IntroMovingLogoState::~IntroMovingLogoState()
{

}

void IntroMovingLogoState::Start()
{
	OptionManager& optionManager{ CCoreEngine::Instance().GetOptionManager() };

	m_duration = optionManager.getOption("isc/" + GetName() + "/in/duration");
	m_startTimestamp = CCoreEngine::Instance().GetTimerManager().GetTotalTimeSeconds();
	m_overlay = Ogre::OverlayManager::getSingleton().getByName("IntroStateOverlay");
	m_soundPoint = m_duration * 0.25;
	m_soundPlayed = false;

	// old logo panel
	CRenderManager& renderManager{ CCoreEngine::Instance().GetRenderManager() };
	Ogre::Viewport* const vp{ renderManager.GetOgreViewport() };
	Ogre::Vector2 textureSize{ renderManager.getTextureSize(optionManager.getOption("isc/" + GetName() + "/in/texture_name").as<std::string>()) };
	textureSize /= 2;
	f32 totalWidth{ static_cast<f32>(vp->getActualWidth()) }, totalHeight{ static_cast<f32>(vp->getActualHeight()) };
	const Ogre::Vector2 logoPanelStart{ (totalWidth / 2.f) - (textureSize.x / 2.f), (totalHeight / 2.f) - (textureSize.y / 2.f) };
	const Ogre::Vector2 logoPanelEnd{ logoPanelStart.x + textureSize.y * 4, logoPanelStart.y };

	std::vector<Panel::Animation*> logoAnimations{
		new Panel::Animation{ m_soundPoint, logoPanelStart, logoPanelEnd },
		new Panel::Animation{ m_duration - m_soundPoint, logoPanelEnd, logoPanelEnd }
	};

	const std::string materialName{ optionManager.getOption("isc/" + GetName() + "/in/material_name").as<std::string>() };
	m_panel.reset(new Panel{ materialName, logoPanelStart, textureSize, {}, logoAnimations });
	m_overlay->add2D(m_panel->getOgrePanel());

	// new logo panel
	Ogre::Vector2 newTextureSize{ renderManager.getTextureSize(optionManager.getOption("isc/" + GetName() + "/in/new_texture_name").as<std::string>()) };
	const Ogre::Vector2 newLogoPanelEnd{ (totalWidth / 2.f) - (newTextureSize.x / 2.f), (totalHeight / 2.f) - (newTextureSize.y / 2.f) };
	const Ogre::Vector2 newLogoPanelStart{ logoPanelEnd.x - newTextureSize.x * 4, newLogoPanelEnd.y };

	std::vector<Panel::Animation*> newLogoAnimations{
		new Panel::Animation{ m_soundPoint, newLogoPanelStart, newLogoPanelEnd },
		new Panel::Animation{ m_duration - m_soundPoint, newLogoPanelEnd, newLogoPanelEnd }
	};

	const std::string newMaterialName{ optionManager.getOption("isc/" + GetName() + "/in/new_material_name").as<std::string>() };
	m_newLogoPanel.reset(new Panel{ newMaterialName, newLogoPanelStart, newTextureSize, {}, newLogoAnimations });

	m_overlay->add2D(m_newLogoPanel->getOgrePanel());
	m_overlay->show();
}

void IntroMovingLogoState::Finish()
{
	m_startTimestamp = 0;

	if (m_overlay)
	{
		if (m_panel)
		{
			m_overlay->remove2D(m_panel->getOgrePanel());
			m_panel.reset();
		}

		if (m_newLogoPanel)
		{
			m_overlay->remove2D(m_newLogoPanel->getOgrePanel());
			m_newLogoPanel.reset();
		}
	}
}

void IntroMovingLogoState::Pause()
{
	// warning - not implemented!
}

void IntroMovingLogoState::Resume()
{
	// warning - not implemented!
}

void IntroMovingLogoState::Update(f64 dt)
{
	const f64 now{ CCoreEngine::Instance().GetTimerManager().GetTotalTimeSeconds() };
	const f64 elapsed{ now - m_startTimestamp };

	if (elapsed >= m_duration)
	{
		CCoreEngine::Instance().GetMainStateManager().PopState();
	}
	else
	{
		if (!m_soundPlayed)
		{
			if (elapsed >= m_soundPoint)
			{
				//CCoreEngine::Instance().GetSoundManager().playSound("intro_impact");
				m_soundPlayed = true;
			}
		}

		if (m_panel)
		{
			m_panel->update(dt);
		}
		
		if (m_newLogoPanel)
		{
			m_newLogoPanel->update(dt);
		}
	}
}
