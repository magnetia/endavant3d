#ifndef MAGNETIA_GAME_PANEL_H_
#define MAGNETIA_GAME_PANEL_H_

#include <memory>
#include <vector>
#include <OGRE/Ogre.h>
#include <OGRE/Overlay/OgreOverlaySystem.h>
#include <Core/CBasicTypes.h>
#include <Utils/edvVector3.h>
#include <Utils/ChildOf.h>
#include <Game/TextureAnimation.h>

class Panel
{
public:
	class Animation : public ChildOf<Panel>
	{
	public:
		Animation(f64 a_duration, const Ogre::Vector2& a_origin, const Ogre::Vector2& a_destination);
		~Animation();

		void start(Panel* a_panel);
		void update(f64 dt);
		void finish();
		bool finished() const;

	private:
		f64						m_duration;
		Ogre::Vector2			m_destination;
		Ogre::Vector2			m_vector;
		f64						m_movementFactor;
		Ogre::Vector2			m_position;
		f64						m_elapsed;
	};

	class Text
	{
	public:
		Text(const std::string& a_id, const std::string& a_fontName, const std::string& a_caption, const Ogre::Vector2& a_position,
			const Ogre::ColourValue& a_colour, f32 a_charHeight = 64.f, 
			Ogre::TextAreaOverlayElement::Alignment a_alignment = Ogre::TextAreaOverlayElement::Left);
		~Text();

		void setCaption(const std::string& a_caption);
		const std::string& getCaption() const;
		Ogre::TextAreaOverlayElement* getTextArea() const;

	private:
		std::string						m_fontName;
		Ogre::TextAreaOverlayElement*	m_textArea;
		std::string						m_caption;
		Ogre::Vector2					m_dimensions;
		Ogre::Vector2					m_position;
	};

	typedef std::unique_ptr<Animation>		AnimationPtr;
	typedef std::unique_ptr<Text>			TextPtr;
	typedef std::pair<std::string, TextPtr>	TextPair;
	typedef std::pair<std::string, Text*>	TextPairParam;
	typedef std::vector<AnimationPtr>		Animations;
	typedef std::vector<TextPair>			Texts;
	typedef std::vector<Animation*>			AnimationsParam;
	typedef std::vector<TextPairParam>		TextsParam;

	Panel(const std::string& a_name, const Ogre::Vector2& a_position, const Ogre::Vector2& a_dimensions, const TextsParam& a_texts = {},
		const AnimationsParam& a_animations = {}, const TextureAnimation::Config& a_textureAnimationConfig = {});
	~Panel();

	void update(f64 dt);
	Ogre::OverlayContainer* getOgrePanel() const;
	void finishAnimations();
	bool finished() const;
	void setAnimations(const AnimationsParam& a_animations = {});
	Text* getText(const std::string& a_textId) const;
	TextsParam getTexts() const;
	const Ogre::Vector2& getDimensions() const;
	TextureAnimation* getTextureAnimation() const;
	void setDimensions(const Ogre::Vector2& a_dimensions);

private:
	void addAnimations(const AnimationsParam& a_animations);
	void playCurrentAnimation();

	std::string							m_name;
	Animations							m_animations;
	Animations::const_iterator			m_currentAnimation;
	Ogre::OverlayContainer*				m_panel;
	Texts								m_texts;
	Ogre::Vector2						m_dimensions;
	std::unique_ptr<TextureAnimation>	m_textureAnimation;
};

#endif
