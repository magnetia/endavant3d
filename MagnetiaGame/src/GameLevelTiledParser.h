#ifndef TILED_PARSER_H_
#define TILED_PARSER_H_

#include "Core/CBasicTypes.h"
#include "Utils/CXMLParserPUGI.h"
#include "Game/EntityOgreGraphics.h"
#include "Game/EntityBtPhysics.h"
#include <map>
#include <unordered_map>
#include "Utils/any.h"
#include "Utils/tree.h"
#include <memory>

namespace Ogre
{
	class ManualObject;
};



typedef std::map<std::string, std::string> tGameLevelPropertiesMap;
typedef std::map<u32, std::shared_ptr<tGameLevelPropertiesMap> > tIDTileProperties;

//Helpers
std::string GetGameLevelProperty(const std::string &a_propertytosearch, const tGameLevelPropertiesMap &a_prop_map);
bool	GameLevelPropertyExists(const std::string &a_propertytosearch, const tGameLevelPropertiesMap &a_prop_map);

class GameLevelPolyObject
{
public:
	std::string	m_name;
	std::string m_type;

	std::vector<std::pair<f32, f32>> m_points;
};



class GameLevelPolyObjectsLayer
{
public:

	std::string	m_name;
	s32			m_width;
	s32			m_height;
	
	tGameLevelPropertiesMap m_globalProperties;
	typedef std::map<std::string, std::shared_ptr<GameLevelPolyObject> > tGameLevelPolyObjectVec;
	tGameLevelPolyObjectVec m_polyobjects;
	
};


class GameLevelLayer
{
public:
	std::string m_name;
	//s32 m_width;
	//s32 m_height;
	

	tGameLevelPropertiesMap m_layerGlobalProperties;

	//Propietats de les tiles per id
	tIDTileProperties		m_layerTileIDProperties;
	
	//vector amb totes les tiles i les seves propietats
	typedef std::vector<std::pair< u32, std::shared_ptr<tGameLevelPropertiesMap> > > tGameLevelTilesInfo;
	tGameLevelTilesInfo m_tileIds;





};

class GameLevelRawInfo
{
public:
	std::string m_mapname;
	std::string m_backgroundmusic;
	std::string m_tiledfilepath;
	std::string m_skyboxmaterialname;
	bool		m_tutorial;
	s32 m_tilecolumnscount;
	s32 m_tilerowscount;

	
	f32 m_tiledimensions;  //dimensio d'aresta de la tile ja en coordenades del joc

	tGameLevelPropertiesMap m_globalproperties;

	typedef std::unordered_map<std::string, std::shared_ptr<GameLevelLayer> > tGameLevelLayersMap;
	tGameLevelLayersMap	m_layers;

	typedef std::unordered_map<std::string, std::shared_ptr<GameLevelPolyObjectsLayer> > tGameLevelObjectLayerMap;
	tGameLevelObjectLayerMap m_layersobjects;

	



};




class GameLevelTiledParser
{

public:

	GameLevelTiledParser();
	void	ParseAllLevels();
	void	ParseLevel(const std::string &a_lvlname);
	bool	ExistsLevel(const std::string &a_lvlname);
	u32		GetLevelsCount();
	typedef std::shared_ptr<const GameLevelRawInfo> tGameLevelRawInfo;
	tGameLevelRawInfo GetLevelRawInfo(const std::string &a_lvlname);
	//const std::vector<const std::string> GetAllLevelIDs() const;

	// Map xml file
	struct GameLevelXMLInfo
	{
		std::string id;
		std::string name;				//Identificador unic del nivell
		std::string tiled_pathfilename;	//ruta de l'arxiu a carregar de tiled
		std::string background_music;	//pista d'audio que sonara de fons
		bool isselectable;				//Si el nivell estara disponible per jugarlo (per defecte true)
		std::string skyboxmaterialname;	//Identificador del material del skybox
		bool tutorial;					//Indica si es un nivell de tutorial
	};
	typedef std::vector<std::shared_ptr<GameLevelXMLInfo>> tGameLevelXMLInfo;
	tGameLevelXMLInfo GetAllLevelsXMLInfo();

	void setLevelSelectable(const std::string& a_level_name, bool a_selectable);
	bool isLevelSelectable(const std::string& a_level_name) const;
	void saveGameLevelsInfoToXML() const;

	void LoadGameLevelsInfoFromXML();
	void LoadTiledMaps();

private:
	tGameLevelXMLInfo m_levelsXMLinfo;

	// Tiled Map
	typedef std::map<std::string, std::shared_ptr<GameLevelRawInfo> > tGameLevelInfoMap;
	tGameLevelInfoMap m_levelsinfo;
	void LoadTiledMap(const std::string &a_mapname);


};




#endif