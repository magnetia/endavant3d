#ifndef MAGNETIA_GAME_TIMER_H_
#define MAGNETIA_GAME_TIMER_H_

#include <Core/CBasicTypes.h>
#include <memory>
#include <array>
#include <Utils/tree.h>
#include <Utils/any.h>
#include <OGRE/Ogre.h>
#include "HudElement.h"

class Panel;

class Timer : public HudElement
{
public:
	Timer();
	~Timer();

	virtual void start() override;
	virtual void update(f64 dt) override;
	virtual void stop() override;
	virtual tPanelsVector getPanels() const override;

private:
	enum TimerCounter
	{
		MILLISECONDS,
		SECONDS,
		MINUTES,

		TIMER_COUNTER_MAX
	};

	typedef std::unique_ptr<Panel>				PanelPtr;
	typedef std::array<f64, TIMER_COUNTER_MAX>	Counters;
	typedef edv::tree<edv::any>					Records;

	void resetCounters();
	void updateRecord();
	std::string countersToString(const Counters& a_counters, bool a_printAll = false) const;
	Counters::value_type getCountersTotal(const Counters& a_counters) const;
	void loadRecordCounters();

	bool		m_running;
	PanelPtr	m_timerPanel;
	PanelPtr	m_recordPanel;
	Counters	m_counters;
	Counters	m_recordCounters;
	Records		m_records;
	bool		m_recordPanelActive;
	std::string m_recordsFilename;
};

#endif