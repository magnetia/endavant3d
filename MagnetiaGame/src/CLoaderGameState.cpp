#include "CLoaderGameState.h"
#include "MainMenuState.h"
#include <Core/CCoreEngine.h>
#include <State/CMainStateManager.h>
#include "GUI/BoardMenuManager.h"
#include <Renderer/CRenderManager.h>
#include <Time/CTimeManager.h>

CLoaderGameState::CLoaderGameState() :
	CState{"CLoaderGameState"},
	m_timestamp{ 0. },
	m_substate{ NONE }
{
	m_parentNode = CCoreEngine::Instance().GetRenderManager().GetSceneManager()->getRootSceneNode()->createChildSceneNode("LogosNode");
	m_parentNode->setVisible(false);

	Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().create("logos_mat", "General");
	material->getTechnique(0)->getPass(0)->createTextureUnitState("logos.png");
	material->getTechnique(0)->getPass(0)->setLightingEnabled(false);
	material->getTechnique(0)->getPass(0)->setSceneBlending(Ogre::SceneBlendType::SBT_TRANSPARENT_ALPHA);
	
	m_backgroundrect.reset(new Ogre::Rectangle2D(true));
	m_backgroundrect->setCorners(-1.0, 1.0, 1.0, -1.0);
	m_backgroundrect->setMaterial("logos_mat");
	m_backgroundrect->setRenderQueueGroup(Ogre::RENDER_QUEUE_BACKGROUND);
	Ogre::AxisAlignedBox aabInf;
	aabInf.setInfinite();
	m_backgroundrect->setBoundingBox(aabInf);
	m_backgroundrect->setVisible(true);
	m_parentNode->attachObject(m_backgroundrect.get());

	m_fader.reset(new Fader{ "Overlays/FadeInOut", "Black", this });
}

void CLoaderGameState::Start()
{
	m_parentNode->setVisible(true);
	m_fader->startFadeIn(1.5f);
	m_substate = FADE_IN;
}

CLoaderGameState::~CLoaderGameState()
{
	
}

void CLoaderGameState::Finish()
{
	m_parentNode->setVisible(false);
	m_fader->stop();
	m_substate = NONE;
}

void CLoaderGameState::Pause()
{

}

void CLoaderGameState::Resume()
{

}

void CLoaderGameState::Update(f64 dt)
{
	switch (m_substate)
	{
		case FADE_IN:
		case FADE_OUT:
		{
			if (m_fader)
			{
				m_fader->fade(dt);
			}
		}
		break;

		case SHOW_LOGOS:
		{
			const f64 now = CCoreEngine::Instance().GetTimerManager().GetTotalTimeSeconds();
			if (now - m_timestamp > 1.8f)
			{
				m_fader->startFadeOut(0.8f);
				m_substate = FADE_OUT;
			}
		}
		break;
	}
}

void CLoaderGameState::fadeInCallback()
{
	m_timestamp = CCoreEngine::Instance().GetTimerManager().GetTotalTimeSeconds();
	m_substate = SHOW_LOGOS;
}

void CLoaderGameState::fadeOutCallback()
{
	changeToNextState();
}

void CLoaderGameState::changeToNextState()
{
	CCoreEngine::Instance().GetMainStateManager().ChangeState(MainMenuState::Pointer());
	MainMenuState::Pointer()->showMenu("/MainMenu/MainMenu");
}