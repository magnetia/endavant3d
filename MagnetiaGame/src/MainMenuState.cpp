#include <Core/CCoreEngine.h>
#include <State/CMainStateManager.h>
#include <Options/OptionManager.h>
#include <Sound/CSoundManager.h>
#include "CMainGameState.h"
#include "MainMenuState.h"
#include "GameLevelSelector.h"
#include <iostream>
#include "BoardMenuLvlSelection.h"
#include "GUI/BoardMenuManager.h"
#include "GUI/ChangeStateButton.h"
#include "GameLevel.h"
#include "GUI\BoardButtonConfig.h"
#include "GameLevelSelector.h"

const std::string STATE_NAME = "MainMenuState";

const std::string BACKGROUND_MUSIC = "menu_background";

const std::string MENULEVEL_ID = "MenuLevel";

MainMenuState::MainMenuState() :
	CState{ STATE_NAME }
	
{
	
	m_menuMgr.reset(new BoardMenuManager());

	const std::string MENUS_XML_CONFIG = "media/menus/main_menu.xml";
	m_menuMgr->load(MENUS_XML_CONFIG);
	auto board = std::make_shared<BoardMenuLvlSelection>(m_menuMgr.get());
	board->CreateButtons();

	m_menuMgr->addBoardMenu(board);

	CCoreEngine::Instance().GetSoundManager().preloadMusic(BACKGROUND_MUSIC);
}

MainMenuState::~MainMenuState()
{
	CCoreEngine::Instance().GetSoundManager().releaseMusic(BACKGROUND_MUSIC);
}

void MainMenuState::Start()
{

	const bool skipToGame = CCoreEngine::Instance().GetOptionManager().getOption("game/skip_to_game");
	if (skipToGame)
	{
		CCoreEngine::Instance().GetMainStateManager().ChangeState(CMainGameState::Pointer());
	}
	else
	{
		//Carrego els menus
		CCoreEngine::Instance().GetSoundManager().playMusic(BACKGROUND_MUSIC);
	}
}

void MainMenuState::Finish()
{
	CCoreEngine::Instance().GetSoundManager().stopMusic();
	//CCoreEngine::Instance().GetBoardMenuManager().
	//m_menuMgr->unload();// NO VA VA  APETAR :D
	m_menuMgr->setVisible(false);
}

void MainMenuState::Pause()
{
	//CCoreEngine::Instance().GetBoardMenuManager().getMenuBoard();
	//m_menuMgr->pause();

}

void MainMenuState::Resume()
{
	//m_menuMgr->Resume();

}

void MainMenuState::Update(f64 dt)
{
	m_menuMgr->Update(dt);
	
}

void MainMenuState::showMenu(const std::string& a_menuId)
{
	if (m_menuMgr)
	{
		m_menuMgr->showMenuBoard(a_menuId);
	}
}
