#include <Core/CCoreEngine.h>
#include "GameLevelSelector.h"
#include "StartAnimation.h"

StartAnimation::StartAnimation() :
	LevelAnimation{ "StartAnimation" }
{
	createFont("StartAnimationFont", "truetype", "Aero.ttf", 36, 96);
}

StartAnimation::~StartAnimation()
{
	Ogre::FontManager::getSingleton().destroyResourcePool("LevelAnimationFont");
}

void StartAnimation::startImpl()
{
	Ogre::Viewport* vp = CCoreEngine::Instance().GetRenderManager().GetOgreViewport();
	f32 totalWidth = static_cast<f32>(vp->getActualWidth()), totalHeight = static_cast<f32>(vp->getActualHeight());

	// background panel
	const Ogre::Vector2 backgroundPanelDimensions{ totalWidth, totalHeight }, backgroundPanelStart{ 0.f, -totalHeight };
	const Ogre::Vector2 backgroundPanelPos{ 0, 0 };

	std::vector<Panel::Animation*> backgroundAnimations{
		new Panel::Animation{ 0.5, backgroundPanelStart, backgroundPanelPos },
		new Panel::Animation{ 1.5, backgroundPanelPos, backgroundPanelPos },
		new Panel::Animation{ 0.2, backgroundPanelPos, backgroundPanelStart }
	};

	addPanel(new Panel{ "StartAnimation_Background", backgroundPanelStart, backgroundPanelDimensions, {}, backgroundAnimations });

	// red panel
	const Ogre::Vector2 redPanelDimensions{ totalWidth, totalHeight / 4 }, redPanelStart{ -totalWidth, 0 };
	const Ogre::Vector2 redPanelPos{ 0, 0 };

	std::vector<Panel::Animation*> redAnimations{
		//new Panel::Animation{ 0.5, redPanelStart, redPanelPos },
		//new Panel::Animation{ 1.5, redPanelPos, redPanelPos },
		//new Panel::Animation{ 0.2, redPanelPos, redPanelStart }

		// ray anim texture panel
		new Panel::Animation{ 2.25, redPanelPos, redPanelPos },
		new Panel::Animation{ 0.1, redPanelPos, redPanelStart }
	};

	addPanel(new Panel{ "StartAnimation_Red", redPanelStart, redPanelDimensions, {}, redAnimations, { 2., "StartAnimation_Red", false } });

	// blue panel
	const Ogre::Vector2 bluePanelDimensions{ totalWidth, totalHeight / 4 }, bluePanelStart{ totalWidth, totalHeight - bluePanelDimensions.y };
	const Ogre::Vector2 bluePanelPos{ 0, bluePanelStart.y };

	std::vector<Panel::Animation*> blueAnimations{
		//new Panel::Animation{ 0.5, bluePanelStart, bluePanelPos },
		//new Panel::Animation{ 1.5, bluePanelPos, bluePanelPos },
		//new Panel::Animation{ 0.2, bluePanelPos, bluePanelStart }
		
		// ray anim texture panel
		new Panel::Animation{ 2.25, bluePanelPos, bluePanelPos },
		new Panel::Animation{ 0.1, bluePanelPos, bluePanelStart }
	};

	addPanel(new Panel{ "StartAnimation_Blue", bluePanelStart, bluePanelDimensions, {}, blueAnimations, { 2., "StartAnimation_Blue", false } });

	// upper text panel
	Ogre::Vector2 upperPanelDimensions{ totalWidth / 2, totalHeight / 4 }, upperPanelStart{ 100, -upperPanelDimensions.y };
	const Ogre::Vector2 upperPanelPos{ upperPanelStart.x, 0 };
	const Ogre::Vector2 upperPanelEnd{ -upperPanelDimensions.x, upperPanelPos.y };

	std::vector<Panel::Animation*> upperPanelAnimations{
		new Panel::Animation{ 0.5, upperPanelStart, upperPanelStart },
		new Panel::Animation{ 0.7, upperPanelStart, upperPanelPos },
		new Panel::Animation{ 0.6, upperPanelPos, upperPanelPos },
		new Panel::Animation{ 0.1, upperPanelPos, upperPanelEnd }
	};

	Panel::Text* upperPanelText = new Panel::Text{ 
		"StartAnimationUpperText", 
		"StartAnimationFont", 
		GameLevelSelector::Pointer()->m_levelid,
		{ 0, 45 }, 
		Ogre::ColourValue::White 
	};

	Panel::TextsParam upperPanelTexts{
		Panel::TextPairParam{ "upperPanelText", upperPanelText }
	};

	addPanel(new Panel{ 
		"StartAnimation_UpperText", 
		upperPanelStart,
		upperPanelDimensions,
		upperPanelTexts,
		upperPanelAnimations
	});

	// lower text panel
	const Ogre::Vector2 lowerPanelDimensions{ totalWidth / 2, totalHeight / 4 }, lowerPanelStart{ totalWidth - 500, totalHeight };
	const Ogre::Vector2 lowerPanelPos{ lowerPanelStart.x, totalHeight - lowerPanelDimensions.y };
	const Ogre::Vector2 lowerPanelEnd{ totalWidth, lowerPanelPos.y };

	std::vector<Panel::Animation*> lowerPanelAnimations{
		new Panel::Animation{ 0.5, lowerPanelStart, lowerPanelStart },
		new Panel::Animation{ 0.7, lowerPanelStart, lowerPanelPos },
		new Panel::Animation{ 0.6, lowerPanelPos, lowerPanelPos },
		new Panel::Animation{ 0.1, lowerPanelPos, lowerPanelEnd }
	};

	Panel::Text* const lowerPanelText = new Panel::Text{
		"StartAnimationLowerText", 
		"StartAnimationFont", 
		"magnetia", 
		{ 0, 45 }, 
		Ogre::ColourValue::White
	};

	Panel::TextsParam lowerPanelTexts{
		Panel::TextPairParam{ "lowerPanelText", lowerPanelText }
	};

	addPanel(new Panel{
		"StartAnimation_LowerText",
		lowerPanelStart,
		lowerPanelDimensions,
		lowerPanelTexts,
		lowerPanelAnimations
	});
}
