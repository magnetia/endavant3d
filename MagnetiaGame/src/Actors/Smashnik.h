#ifndef MAGNETIA_GAME_SMASHNIK_H_
#define MAGNETIA_GAME_SMASHNIK_H_

#include <Sound/CSoundManager.h>
#include <Time/ITimerCallback.h>
#include <Time/CTimeManager.h>
#include <Utils/Factory.h>
#include "MagnetiaActor.h"

class RotationAnimator;

class Smashnik : public MagnetiaActor, ITimerCallback
{
public:
	class Factory : public Actor::factory_type
	{
	public:
		Actor* createElement(const Actor::factory_param_type& a_config) override;
	};

	Smashnik(s32 a_maxLife = 0, const ScriptConfig& a_scriptConfig = {}, f32 a_downMargin = 0.f, f32 a_gravity = 0.f);
	~Smashnik();
	Actor* clone() override;

	void startUp(f64 a_animTime);
	void shutdown();
	void move(f32 a_xOffset, f32 a_yOffset);
	bool onGround() const;
	bool collidingLeft() const;
	bool collidingRight() const;
	f32 getGravity() const;
	void finishAnimation();
	void showParticles(bool a_show);
	void checkOrientation(bool a_reset);

	void CallBack(EV_TimerID a_ID) override;

protected:
	void initImpl() override;
	void updateImpl(f64 dt) override;
	void stopImpl() override;

private:
	enum SmashOrientation
	{
		SMASH_LEFT,
		SMASH_RIGHT,
		SMASH_FRONT,
	};

	void updateCollisions();
	bool checkOnGround();
	bool checkLeftCollision();
	bool checkRightCollision();
	void updateSizeAndPosition();

	edv::Vector3f				m_compoundPosition;
	edv::Vector3f				m_compoundHalfSize;
	edv::Vector3f				m_smashnikHalfSize;
	bool						m_started;
	f32							m_downMargin;
	f32							m_gravity;
	edv::Vector3f				m_currentMovement;
	edv::Vector3f				m_linearVelocity;
	bool						m_onGround;
	bool						m_collidingLeft;
	bool						m_collidingRight;
	Ogre::SceneNode*			m_leftParticles;
	Ogre::SceneNode*			m_rightParticles;
	SmashOrientation			m_smashOrientation;
	EV_TimerID					m_rotationTimerId;
	RotationAnimator*			m_currentRotator;
	CSoundManager::t_ChannelId	m_channelId;
};

#endif
