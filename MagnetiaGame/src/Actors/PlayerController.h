#ifndef EDV_GAME_PLAYER_CONTROLLER_H_
#define EDV_GAME_PLAYER_CONTROLLER_H_

#include <Game/KinematicController.h>

class PlayerController : public KinematicController
{
public:
	
	struct sControllerConfig : sMovementConfig
	{
		f32				height_check_margin;
		f32				acceleration;
		f32				deceleration;
		f32				ground_max_velocity;
		f32				air_friction;
		f32				air_acceleration;
		f32				magnetic_acceleration;
		sControllerConfig(f32 a_height_check_margin = 0.f, f32 a_acceleration = 0.f, f32 a_deceleration = 0.f, const edv::Vector3f& a_gravity = { 0.f, -9.8f, 0.f }, f32 a_restitution = 0.1f, f32 a_sliding = 0.1f, f32 a_friction = 0.f,
			f32 a_ground_max_velocity = 0.f, f32 a_air_friction = 0.f, f32 a_air_movement_factor = 0.f, f32 a_magnetic_acceleration = 0.f ) :
			sMovementConfig{ a_gravity, a_restitution, a_sliding, a_friction }, 
			height_check_margin{ a_height_check_margin }, acceleration{ a_acceleration }, deceleration{ a_deceleration }, ground_max_velocity{ a_ground_max_velocity }, air_friction{ a_air_friction }, air_acceleration{ a_air_movement_factor },
			magnetic_acceleration{ a_magnetic_acceleration }
		{ }
	};
	
	
	PlayerController(const sControllerConfig& a_controller_config = {});
	void applyMove(const edv::Vector3f& a_vector) override;
	void applyForce(const edv::Vector3f& a_vector) override;
	edv::Vector3f applyFriction(const edv::Vector3f& a_velocity) override;
	virtual void updateVelocityOnCollision(edv::Vector3f& a_velocity, const edv::Vector3f& a_normal_hit, f32 a_tangentFactor, f32 a_normalFactor) override;
	virtual bool checkOnGround(GameCollisions& a_collisionInfo) override;
	virtual bool checkOnCeiling(GameCollisions& a_collisionInfo) override;
	void startMagnetism(const edv::Vector3f& a_vector);
	void stopMagnetism();
	bool hasMagnetism() const;
	f32 getMovementSpeedFactor() const;
	void checkPosition(GameCollisions& a_collisionInfo) override;
	void repositionOnGround(const edv::Vector3f& a_hitPointWorld);
	void repositionOnCeiling();

private:
	enum ePosition
	{
		STATE_POSITION_GROUND,
		STATE_POSITION_AIR,
	};
	
	sControllerConfig		m_controller_config;
	bool					m_magnetism;
	edv::Vector3f			m_groundVector;
	const f32				m_heightRayFactor;
	const f32				m_widthRayFactor;
	f32						m_heightCheckMargin;
	const f32				m_heightMargin;
	ePosition				m_currentStatePosition;

};

#endif
