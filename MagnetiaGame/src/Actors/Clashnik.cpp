#include "Core/CCoreEngine.h"
#include "Game/GameManager.h"
#include "Game/GameEntity.h"
#include "Game/KinematicController.h"
#include "Clashnik.h"

Actor* Clashnik::Factory::createElement(const Actor::factory_param_type& a_config)
{
	return new Clashnik{ a_config["max_life"], { a_config["script/path"], a_config["script/scope"] } };
}

Clashnik::Clashnik(s32 a_maxLife, const ScriptConfig& a_scriptConfig) :
	MagnetiaActor{ a_maxLife, a_scriptConfig },
	m_speed{ 0.f },
	m_askedPosition{}
{

}

Clashnik::~Clashnik()
{
	
}

Actor* Clashnik::clone()
{
	return new Clashnik{ getMaxLife(), getScriptConfig() };
}

void Clashnik::initImpl()
{
	MagnetiaActor::initImpl();

	m_askedPosition = getParent()->getWorldPosition();
}

void Clashnik::updateImpl(f64 dt)
{
	if (m_movement && m_speed != 0.f)
	{
		const edv::Vector3f move = { m_speed, 0.f, 0.f };
		m_askedPosition = getParent()->getWorldPosition() + move;
		m_movement->applyMove(move);
	}
}

void Clashnik::setSpeed(f32 a_speed)
{
	if (a_speed > 0.f && a_speed < 0.001f)
	{
		a_speed = 0;
	}

	m_speed = a_speed;
}

void Clashnik::setOrientation(u32 a_orientation)
{
	Orientation value = static_cast<Orientation>(a_orientation);

	if (value < FACING_INVALID)
	{
		m_orientation = value;
	}
	else
	{
		throw std::runtime_error{"invalid orientation value"};
	}	
}

f32 Clashnik::getSpeed() const
{
	return m_speed;
}

u32 Clashnik::getClashnikOrientation() const
{
	return m_orientation;
}

ActorMovement* Clashnik::createMovement()
{
	return new KinematicController;
}

bool Clashnik::isStuck()
{
	return m_askedPosition != getParent()->getWorldPosition();
}
