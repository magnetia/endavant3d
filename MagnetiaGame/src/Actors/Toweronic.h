#ifndef MAGNETIA_TOWERONIC_H_
#define MAGNETIA_TOWERONIC_H_

#include <list>
#include "Game/GameManager.h"
#include "MagnetiaActor.h"
#include "Utils/Factory.h"
#include "ToweronicProjectile.h"

class Toweronic : public MagnetiaActor
{
public:
	class Factory : public Actor::factory_type
	{
	public:
		Actor* createElement(const Actor::factory_param_type& a_config) override;
	};

	struct Config
	{
		f32			actionRadius;
		std::string projectileName;
		f32			projectileOffset;
		std::string	shootSound;

		Config(f32 a_actionRadius = 50.f, const std::string& a_projectileName = "", f32 a_projectileOffset = 0.f, const std::string& a_shootSound = "") :
			actionRadius(a_actionRadius), projectileName(a_projectileName), projectileOffset(a_projectileOffset), shootSound(a_shootSound)
		{
		};
	};

	Toweronic(s32 a_maxLife = 0, const ScriptConfig& a_scriptConfig = {}, Config a_config = Config{});
	~Toweronic();
	Actor* clone() override;
	void clear() override;

	void target(const edv::Vector3f& a_vector);
	void shoot(const edv::Vector3f& a_vector);
	void removeProjectile(ToweronicProjectile* a_projectile);
	void targetPlayer();
	void shootPlayer();
	bool canSeePlayer() const;
	f32 getActionRadius() const;

protected:
	void initImpl() override;
	void updateImpl(f64 dt) override;
	void stopImpl() override;

private:
	typedef std::pair<std::string, GameEntity*>	EntityInfo;
	typedef std::list<EntityInfo>				Projectiles;
	typedef std::list<GameEntity*>				ProjectilesToRemove;

	std::string			m_projectileName;
	f32					m_projectileOffset;
	Projectiles			m_projectiles;
	ProjectilesToRemove	m_projectilesToRemove;
	std::string			m_projectileSound;
	Ogre::Bone*			m_cannon_bone;
	f32					m_actionRadius;
};

#endif
