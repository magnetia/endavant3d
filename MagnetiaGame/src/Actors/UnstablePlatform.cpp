#include "UnstablePlatform.h"
#include <Game/GameEntity.h>
#include <Game/ChainedBehavior.h>
#include <Game/AlphaTransitionAnimator.h>

Actor* UnstablePlatform::Factory::createElement(const Actor::factory_param_type& a_config)
{
	return new UnstablePlatform{ 
			{ a_config["script/path"], a_config["script/scope"] },
			a_config["time_in_active"],
			a_config["time_in_advise"],
			a_config["time_in_inactive"]
	};
}


UnstablePlatform::UnstablePlatform(const ScriptConfig& a_scriptConfig, f64 a_timeInActive, f64 a_timeInAdvise, f64 a_timeInInactive) :
	MagnetiaActor{ 0, a_scriptConfig },
	m_timeInActive{ a_timeInActive },
	m_timeInAdvise{ a_timeInAdvise },
	m_timeInInactive{ a_timeInInactive }
{
}

void UnstablePlatform::playAdvise()
{
	const u32 AnimLoops = 3;
	getParent()->addBehavior(
		new ChainedBehavior(
			{
				new AlphaTransitionAnimator{ (m_timeInAdvise / AnimLoops)/ 2.f, 1.f, .5f },
				new AlphaTransitionAnimator{ (m_timeInAdvise / AnimLoops) / 2.f, .5f, 1.f },
			},
			AnimLoops
		)
		
	);
}

f64 UnstablePlatform::getTimeInActive() const
{
	return m_timeInActive;
}

f64 UnstablePlatform::getTimeInAdvise() const
{
	return m_timeInAdvise;
}

f64 UnstablePlatform::getTimeInInactive() const
{
	return m_timeInInactive;
}

void UnstablePlatform::initImpl()
{
	getParent()->getGraphics()->getNode()->setVisible(true);
	getParent()->removeAllBehaviors();
	getParent()->getPhysics()->setCollisionEnabled(true);
}

void UnstablePlatform::stopImpl()
{
	getParent()->getGraphics()->getNode()->setVisible(false);
	getParent()->getPhysics()->setCollisionEnabled(false);
}