#ifndef MAGNETIA_GAME_BOSS_WAIT_STATE_H_
#define MAGNETIA_GAME_BOSS_WAIT_STATE_H_

#include "BossState.h"

class BossEscapeState;

class BossWaitState : public BossState
{
public:
	BossWaitState(Boss* a_boss, f32 a_distance, BossEscapeState* a_escapeState);

	void start() override;
	void update(f64 dt) override;
	void stop() override;
	bool finished() override;
	edv::Vector3f getStartPoint() const override;

private:
	f32					m_distance;
	bool				m_finished;
	BossEscapeState*	m_escapeState; // guarrada maxima, campanya!
};

#endif
