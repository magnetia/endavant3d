#ifndef MAGNETIA_GAME_LETHAL_BARRIER_H_
#define MAGNETIA_GAME_LETHAL_BARRIER_H_

#include <Sound/CSoundManager.h>
#include "Actors/MagnetiaActor.h"

class LethalBarrier : public MagnetiaActor
{
public:
	// Factory
	class Factory : public Actor::factory_type
	{
	public:
		Actor* createElement(const Actor::factory_param_type& a_config) override;
	};

	LethalBarrier(const ScriptConfig& a_scriptConfig = {}, f64 a_offTime = 0, f64 a_onTime = 0, const edv::Vector3f& a_direction = {}, f32 a_sound_range = 0.f, 
		const std::string& a_sphereMaterialName = "", const std::string& a_rayMaterialName = "");
	~LethalBarrier();
	Actor* clone() override;
	void clear();

	f64 getOffTime() const;
	f64 getOnTime() const;
	void showRay(bool a_show);
	bool isShowingRay() const;

private:
	struct ArcPoint
	{
		edv::Vector3f		point;
		Ogre::SceneNode*	node;
		Ogre::Entity*		sphere;

		ArcPoint(const edv::Vector3f& a_point = {}, Ogre::SceneNode* a_node = nullptr, Ogre::Entity* a_sphere = nullptr);
		void clear();
	};

	void initImpl() override;
	void updateImpl(f64 dt) override;

	f64						m_offTime;
	f64						m_onTime;
	edv::Vector3f			m_directVect;
	edv::Vector3f			m_direction;
	Ogre::BillboardChain*	m_bbChain;
	Ogre::SceneNode*		m_node;
	Ogre::ManualObject*		m_collisionWorldObject;
	ArcPoint				m_arcPoint1;
	ArcPoint				m_arcPoint2;
	CSoundManager::t_ChannelId	m_soundChannelId;
	f32						m_sound_range;
	std::string				m_sphereMaterialName;
	std::string				m_rayMaterialName;
};

#endif
