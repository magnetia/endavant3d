#include <Physics/MagneticProperty.h>
#include <Core/CCoreEngine.h>
#include <Renderer/CRenderManager.h>
#include <Physics/CPhysicsManager.h>
#include <Physics/btResultCallback.hpp>
#include <Sound/CSoundManager.h>
#include <Game/GameManager.h>
#include <Game/GameEntity.h>
#include "Toweronic.h"

Actor* Toweronic::Factory::createElement(const Actor::factory_param_type& a_config)
{
	return new Toweronic { 
		a_config["max_life"].as<s32>(), 
		{ a_config["script/path"], a_config["script/scope"] },
		{
			a_config["action_radius"],
			a_config["projectile_name"],
			a_config["projectile_offset"],
			a_config["projectile_sound"]
		}
	};
}

Toweronic::Toweronic(s32 a_maxLife, const ScriptConfig& a_scriptConfig, Toweronic::Config a_config) :
		MagnetiaActor{ a_maxLife, a_scriptConfig },
		m_projectileName{ a_config.projectileName },
		m_projectileOffset{ a_config.projectileOffset },
		m_projectileSound{ a_config.shootSound },
		m_cannon_bone{ nullptr },
		m_actionRadius{ a_config.actionRadius }
{
	
}

Toweronic::~Toweronic()
{
}

Actor* Toweronic::clone()
{
	return new Toweronic { 
		getMaxLife(), 
		getScriptConfig(), 
		{ m_actionRadius, m_projectileName, m_projectileOffset, m_projectileSound }
	};
}

void Toweronic::clear()
{
	for (auto entityInfo : m_projectiles)
	{
		CCoreEngine::Instance().GetGameManager().destroyEntity(entityInfo.first);
	}

	m_projectiles.clear();
	m_projectilesToRemove.clear();
	m_cannon_bone = nullptr;
}

void Toweronic::target(const edv::Vector3f& a_vector)
{
	auto& game = CCoreEngine::Instance().GetGameManager();

	if (GameEntity* player = game.getPlayer())
	{
		if (m_cannon_bone)
		{
			// compute angle of cannon
			edv::Vector3f cannon_vector{ a_vector };
			edv::Vector3f inital_vector{ 0.f, 1.f, 0.f };
			f32 dotProduct = inital_vector.dotProduct(cannon_vector);
			f32 angle = std::acos(dotProduct / (inital_vector.length()*cannon_vector.length()));

			// Angle control
			if (cannon_vector.x > 0)
			{
				angle *= -1;
			}

			m_cannon_bone->resetToInitialState();
			m_cannon_bone->rotate(Ogre::Quaternion{ Ogre::Radian(angle), Ogre::Vector3{ 0.f, 1.f, 0.f } });
		}
	}
}

void Toweronic::shoot(const edv::Vector3f& a_vector)
{
	edv::Vector3f movementVector{ a_vector };
	movementVector.normalise();

	// create projectile instance
	auto& game = CCoreEngine::Instance().GetGameManager();
	edv::Vector3f towerPos = getParent()->getWorldPosition();
	MagneticProperty::eMagneticCharge charge = rand() % 2 ? MagneticProperty::MAGNET_TYPE_POSITIVE : MagneticProperty::MAGNET_TYPE_NEGATIVE;
	const edv::Vector3f projectilePos = towerPos + (movementVector * m_projectileOffset);
	const std::string projectileName = charge == MagneticProperty::MAGNET_TYPE_POSITIVE ? m_projectileName + "Positive" : m_projectileName + "Negative";
	EntityInfo projectileData{ game.createEntity(projectileName, projectilePos.to_OgreVector3()) };
	GameEntity* const projectileEntity = projectileData.second;

	// configure projectile		
	ToweronicProjectile* const projectile = static_cast<ToweronicProjectile*>(projectileEntity->getActor());
	projectile->configure(this, movementVector, charge);

	// so we can keep track of on-screen projectiles
	m_projectiles.push_back(projectileData);

	// play some sound
	CCoreEngine::Instance().GetSoundManager().playSound(m_projectileSound);
}

void Toweronic::removeProjectile(ToweronicProjectile* a_projectile)
{
	m_projectilesToRemove.push_back(a_projectile->getParent());
}

void Toweronic::targetPlayer()
{
	auto& game = CCoreEngine::Instance().GetGameManager();

	if (GameEntity* player = game.getPlayer())
	{
		edv::Vector3f movementVector{ player->getWorldPosition() - getParent()->getWorldPosition() };
		movementVector.normalise();
		target(movementVector);
	}
}

void Toweronic::shootPlayer()
{
	auto& game = CCoreEngine::Instance().GetGameManager();

	if (GameEntity* player = game.getPlayer())
	{
		edv::Vector3f movementVector{ player->getWorldPosition() - getParent()->getWorldPosition() };
		movementVector.normalise();

		// target and...
		target(movementVector);
		// FIRE!
		shoot(movementVector);
	}
}

bool Toweronic::canSeePlayer() const
{
	bool canSee = false;

	if (GameEntity* const player = CCoreEngine::Instance().GetGameManager().getPlayer())
	{
		const edv::Vector3f playerWorldPos = player->getWorldPosition();
		const edv::Vector3f towerWorldPos = getParent()->getWorldPosition();
		const edv::Vector3f towerHalfSize = getParent()->getScaledHalfSize();
		const edv::Vector3f originUp{ towerWorldPos.x, towerWorldPos.y + towerHalfSize.y, towerWorldPos.z };
		const edv::Vector3f originDown{ towerWorldPos.x, towerWorldPos.y - towerHalfSize.y, towerWorldPos.z };

		btClosestNotMeRayResultCallback callbackUp{
			getParent(),
			originUp.to_btVector3(),
			playerWorldPos.to_btVector3(),
			EntityBtPhysics::EdvCollisionFlags::CF_TRIGGER | btCollisionObject::CF_NO_CONTACT_RESPONSE
		};

		btClosestNotMeRayResultCallback callbackDown{
			getParent(),
			originDown.to_btVector3(),
			playerWorldPos.to_btVector3(),
			EntityBtPhysics::EdvCollisionFlags::CF_TRIGGER | btCollisionObject::CF_NO_CONTACT_RESPONSE
		};

		GameEntity* targetUp = static_cast<GameEntity*>(CCoreEngine::Instance().GetPhysicsManager().RayTestNotMe(originUp, playerWorldPos, callbackUp));
		GameEntity* targetDown = static_cast<GameEntity*>(CCoreEngine::Instance().GetPhysicsManager().RayTestNotMe(originDown, playerWorldPos, callbackDown));

		canSee = targetUp == player || targetDown == player;
	}

	return canSee;
}

f32 Toweronic::getActionRadius() const
{
	return m_actionRadius;
}

void Toweronic::initImpl()
{
	MagnetiaActor::initImpl();

	CCoreEngine::Instance().GetSoundManager().preloadSound(m_projectileSound);
	
	m_cannon_bone = EntityOgreGraphics::getBone(static_cast<Ogre::Entity*>(getParent()->getGraphics()->getNode()->getAttachedObject(0)), "cano_jnt");
	if (m_cannon_bone)
	{
		m_cannon_bone->setManuallyControlled(true);
	}
}

void Toweronic::updateImpl(f64 dt)
{
	// remove projectiles that had collided
	m_projectiles.erase(std::remove_if(m_projectiles.begin(), m_projectiles.end(),
		[&](const EntityInfo& p)
		{ 
			bool remove = false;

			for (auto it = m_projectilesToRemove.begin(); !remove && it != m_projectilesToRemove.end(); )
			{
				GameEntity* const entity = (*it);

				if (entity == p.second)
				{
					CCoreEngine::Instance().GetGameManager().destroyEntity(p.first);
					it = m_projectilesToRemove.erase(it);
					remove = true;
				}
				else
				{
					it++;
				}
			}

			return remove;
		}), m_projectiles.end());

	m_projectilesToRemove.clear();

	// update projectiles
	for (auto it = m_projectiles.begin(); it != m_projectiles.end(); it++)
	{
		GameEntity* const projectile = it->second;
		projectile->update(dt);
	}
}

void Toweronic::stopImpl()
{
	CCoreEngine::Instance().GetSoundManager().releaseSound(m_projectileSound);
}
