#include <Game/GameEntity.h>
#include <Game/ChainedBehavior.h>
#include <Game/FlyStraightAnimator.h>
#include <Game/NullAnimator.h>
#include "Platform.h"

Actor* Platform::Factory::createElement(const Actor::factory_param_type& a_config)
{
	return new Platform{
		a_config["duration"],
		{ a_config["offset/x"], a_config["offset/y"], a_config["offset/z"] }, 
		{ a_config["script/path"], a_config["script/scope"] }
	};
}

Platform::Platform(f64 a_duration, const edv::Vector3f& a_offset, const ScriptConfig& a_scriptConfig) :
	MagnetiaActor{ 0, a_scriptConfig },
	m_duration{ a_duration },
	m_offset{ a_offset },
	m_origin{ },
	m_dest{ }
{

}

Actor* Platform::clone()
{
	return new Platform{ m_duration, m_offset, getScriptConfig() };
}

void Platform::initImpl()
{
	MagnetiaActor::initImpl();

	m_origin = getParent()->getWorldPosition();
	m_dest = m_origin + m_offset;
	const f64 stepDuration = m_duration / 4;

	getParent()->addBehavior(
		new ChainedBehavior(
			{
				new FlyStraightAnimator{ stepDuration, m_origin.to_OgreVector3(), m_dest.to_OgreVector3() },
				new NullAnimator{ stepDuration },
				new FlyStraightAnimator{ stepDuration, m_dest.to_OgreVector3(), m_origin.to_OgreVector3() },
				new NullAnimator{ stepDuration }
			},
			-1
		)
	);
}

void Platform::updateImpl(f64 dt)
{

}
