#include "Player.h"
#include "Core\CCoreEngine.h"
#include "Sound\CSoundManager.h"
Player::Player(s32 a_maxLife, const ScriptConfig& a_scriptConfig) :
	MagnetiaActor{ a_maxLife, a_scriptConfig },
	m_neutralizerBallCount{ 0 },
	m_godMode{ false },
	m_inputEnabled{ true }
{
}

Player::~Player()
{
}

void Player::setNeutralizerBalls(s32 a_countballs)
{
	m_neutralizerBallCount = a_countballs;	
}

s32	Player::getNeutralizerBalls() const
{
	return m_neutralizerBallCount;
}

void Player::setGodMode(bool a_active)
{
	m_godMode = a_active;
}

bool Player::isGodMode() const
{
	return m_godMode;
}

void Player::setInputEnabled(bool a_enabled)
{
	m_inputEnabled = a_enabled;
}

bool Player::isInputEnabled() const
{
	return m_inputEnabled;
}