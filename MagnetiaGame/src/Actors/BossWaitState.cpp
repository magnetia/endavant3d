#include <Core/CCoreEngine.h>
#include <Game/GameManager.h>
#include "Boss.h"
#include "BossEscapeState.h"
#include "BossWaitState.h"

BossWaitState::BossWaitState(Boss* a_boss, f32 a_distance, BossEscapeState* a_escapeState) :
	BossState{ a_boss },
	m_distance{ a_distance },
	m_finished{ false },
	m_escapeState{ a_escapeState }
{

}

void BossWaitState::start()
{
	m_finished = false;
}

void BossWaitState::update(f64 dt)
{
	if (!m_finished)
	{
		// campanya maxima
		if (m_escapeState)
		{
			m_escapeState->update(dt);
		}

		if (GameEntity* const player = CCoreEngine::Instance().GetGameManager().getPlayer())
		{
			if (m_boss->getParent()->distanceToEntity(player) < m_distance)
			{
				if (m_boss->getParent()->canSeeEntity(player))
				{
					m_finished = true;

					if (m_escapeState)
					{
						m_escapeState->stop();
					}
				}
			}
		}
	}
}

void BossWaitState::stop()
{
	m_finished = true;
}

bool BossWaitState::finished()
{
	return m_finished;
}

edv::Vector3f BossWaitState::getStartPoint() const
{
	return {};
}
