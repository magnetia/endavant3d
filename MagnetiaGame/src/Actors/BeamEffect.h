#ifndef MAGNETIA_BEAM_EFFECT_H_
#define MAGNETIA_BEAM_EFFECT_H_

#include <Core/CBasicTypes.h>
#include <Externals/OGRE/Ogre.h>

class GameEntity;

class BeamEffect
{
public:
	BeamEffect(const std::string& a_id = "", const edv::Vector3f& a_direction = {}, f64 a_duration = 0, 
		const Ogre::ColourValue& a_color = Ogre::ColourValue{ 0.f, 0.f, 0.f, 1.f }, 
		const edv::Vector3f& a_position = {}, const Ogre::Quaternion& a_orientation = {});
	
	~BeamEffect();

	void update(f64 dt);
	std::string getId() const;

private:
	Ogre::SceneNode*			m_node;
	Ogre::Animation*			m_animation;
	Ogre::AnimationState*		m_state;
	Ogre::BillboardSet*			m_bbs;
	Ogre::Billboard*			m_flare;
	Ogre::Light*				m_light;
	Ogre::ColourValue			m_color;
	Ogre::RibbonTrail*			m_trail;
	edv::Vector3f				m_position;
	Ogre::Quaternion			m_orientation;
	edv::Vector3f				m_direction;
	f64							m_duration;
	f64							m_startTimestamp;
	std::string					m_id;
};

#endif
