#ifndef MAGNETIA_MAGNETIA_ACTOR_H_
#define MAGNETIA_MAGNETIA_ACTOR_H_

#include <Game/Actor.h>
#include <Utils/edvVector3.h>
#include <Physics/MagneticProperty.h>

struct MagneticRayInfo
{
	GameEntity*							originEntity;
	GameEntity*							targetEntity;
	edv::Vector3f						originPos;
	edv::Vector3f						direction;
	edv::Vector3f						worldPoint;
	f32									force;
	MagneticProperty::eMagneticCharge	charge;

	MagneticRayInfo(GameEntity* a_originEntity = nullptr, GameEntity* a_targetEntity = nullptr, const edv::Vector3f& a_originPos = {}, const edv::Vector3f& a_direction = {}, const edv::Vector3f& a_worldPoint = {},
		f32 a_force = 0.f, MagneticProperty::eMagneticCharge a_charge = MagneticProperty::MAGNET_TYPE_NEUTRAL) :
		originEntity{ a_originEntity }, targetEntity{ a_targetEntity }, originPos{ a_originPos }, direction{ a_direction }, worldPoint{ a_worldPoint }, force{ a_force }, charge{ a_charge } { }

	void clear()
	{
		originEntity = nullptr;
		targetEntity = nullptr;
		originPos = edv::Vector3f{ 0.f, 0.f, 0.f };
		direction = edv::Vector3f{ 0.f, 0.f, 0.f };
		worldPoint = edv::Vector3f{ 0.f, 0.f, 0.f };
		force = 0.f;
		charge = MagneticProperty::MAGNET_TYPE_NEUTRAL;
	}
};

class MagnetiaActor : public Actor
{
public:
	// high level orientation layer
	enum Orientation
	{
		FACING_LEFT,
		FACING_RIGHT,
	};

	enum Direction
	{
		DIRECTION_FORWARD,
		DIRECTION_BACK,
	};

	// Factory
	class Factory : public Actor::factory_type
	{
	public:
		Actor* createElement(const Actor::factory_param_type& a_config) override;
	};

	MagnetiaActor(s32 a_maxLife = 0, const ScriptConfig& a_scriptConfig = {});
	Actor* clone() override;

	void applyMove(const edv::Vector3f & a_vector) override;
	//void applyForce(const edv::Vector3f & a_vector) override;

	virtual void onMagneticRayHit(const MagneticRayInfo& a_rayInfo);
	virtual void onMagneticRayTarget(const MagneticRayInfo& a_rayInfo);
	virtual void onMagneticRayUnTarget();
	virtual void setOrientation(Orientation a_orientation) final;
	virtual Orientation getOrientation() const final;
	virtual Direction getDirection() const final;
	virtual void setDirection(Direction a_direction) final;

protected:
	void initImpl() override;
	void updateImpl(f64 dt) override;
	void stopImpl() override;
	virtual void checkFacing();
	virtual void checkDirection();

	Ogre::Quaternion	m_previousRotation;
	Orientation			m_orientation;
	Direction			m_direction;
};

#endif
