#ifndef MAGNETIA_TOWERONIC_PROJECTILE_H_
#define MAGNETIA_TOWERONIC_PROJECTILE_H_

#include <Utils/edvVector3.h>
#include <Game/ScaleAnimator.h>
#include "MagnetiaActor.h"

class Toweronic;

class ToweronicProjectile : public MagnetiaActor
{
public:
	class Factory : public Actor::factory_type
	{
	public:
		Actor* createElement(const Actor::factory_param_type& a_config) override;
	};

	ToweronicProjectile(const ScriptConfig& a_scriptConfig = {}, f32 a_speed = 0.f);
	Actor* clone() override;

	void configure(Toweronic* a_toweronic, const edv::Vector3f& a_vector, MagneticProperty::eMagneticCharge a_charge);
	void destroy();
	GameEntity* getToweronicEntity() const;
	void setInactive(f32 a_animTime);
	void grow(f32 a_increment, f32 a_time);
	MagneticProperty::eMagneticCharge getCharge() const;

protected:
	void initImpl() override;
	void updateImpl(f64 dt) override;

	f32									m_speed;
	edv::Vector3f						m_vector;
	Toweronic*							m_toweronic;
	MagneticProperty::eMagneticCharge	m_charge;
	ScaleAnimator*						m_currentScaleAnim;
};

#endif
