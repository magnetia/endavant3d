#include <Core/CCoreEngine.h>
#include <Options/OptionManager.h>
#include "TriggerGameEnd.h"

Actor* TriggerGameEnd::Factory::createElement(const Actor::factory_param_type& a_config)
{
	return new TriggerGameEnd{
		a_config["max_life"].as<s32>(),
		ScriptConfig{ a_config["script/path"], a_config["script/scope"] }
	};
}

TriggerGameEnd::TriggerGameEnd(s32 a_maxLife, const ScriptConfig& a_scriptConfig) :
	MagnetiaActor{ a_maxLife, a_scriptConfig },
	m_currentSubstate{ SUBSTATE_WAITING },
	m_fader{ new Fader{ "Overlays/FadeWin", "FadeGameEndMaterial", this } },
	m_panel{ nullptr },
	m_overlay{ Ogre::OverlayManager::getSingleton().create("TriggerGameEndOverlay") }
{
	Ogre::FontManager& fontManager = Ogre::FontManager::getSingleton();

	if (!fontManager.resourceExists("GameEndFont"))
	{
		Ogre::FontPtr font = fontManager.create("GameEndFont", "General");

		font->setParameter("type", "truetype");
		font->setParameter("source", "Aero.ttf");
		font->setParameter("size", "32");
		font->setParameter("resolution", "96");

		font->load();
	}
}

TriggerGameEnd::~TriggerGameEnd()
{
	Ogre::FontManager::getSingleton().destroyResourcePool("GameEndFont");

	if (m_overlay)
	{
		if (m_panel)
		{
			m_overlay->remove2D(m_panel->getOgrePanel());
		}

		Ogre::OverlayManager::getSingleton().destroy(m_overlay);
	}
}

Actor* TriggerGameEnd::clone()
{
	return new TriggerGameEnd{ getMaxLife(), getScriptConfig() };
}

void TriggerGameEnd::fadeInCallback()
{

}

void TriggerGameEnd::fadeOutCallback()
{
	m_currentSubstate = SUBSTATE_STORY;
}

TriggerGameEnd::Substate TriggerGameEnd::getSubstate() const
{
	return m_currentSubstate;
}

void TriggerGameEnd::startStory()
{
	m_currentSubstate = SUBSTATE_FADE_OFF;
	m_fader->startFadeOut(2.f);

	Panel::Text* gameEndText = new Panel::Text{
		"GameEndText",
		"GameEndFont",
		"Bla bla bla bla bla bla\nbla bla bla bla",
		{ 0, 0 },
		Ogre::ColourValue::Black
	};

	Panel::TextsParam gameEndTexts{
		Panel::TextPairParam{ "EndStory", gameEndText }
	};

	m_panel.reset(new Panel{ "FadeGameEndMaterial", { 0, 0 }, { 500, 500 }, gameEndTexts, { } });

	m_overlay->add2D(m_panel->getOgrePanel());

	// configure and show overlay
	m_overlay->setZOrder(CCoreEngine::Instance().GetOptionManager().getOption("overlay/z_order/GameEndStory"));
	m_overlay->show();
}

void TriggerGameEnd::updateImpl(f64 dt)
{
	switch (m_currentSubstate)
	{
	case SUBSTATE_WAITING:
		break;
	case SUBSTATE_FADE_OFF:
		m_fader->fade(dt);
		break;
	case SUBSTATE_STORY:
		updateStory(dt);
		break;
	}
}

void TriggerGameEnd::updateStory(f64 dt)
{
	if (m_panel)
	{
		m_panel->update(dt);
	}
}
