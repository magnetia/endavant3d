#include "Aplasta.h"
#include <Core/CCoreEngine.h>
#include <Physics/CPhysicsManager.h>
#include <Physics/btResultCallback.hpp>
#include <Game/GameManager.h>
#include <Game/GameEntity.h>
#include <Game/FlyStraightAnimator.h>
#include <Game/ParticleSystemAnimator.h>
#include <Game/DistanceSound.h>

#include <Core/CLogManager.h>

Actor* Aplasta::Factory::createElement(const Actor::factory_param_type& a_config)
{
	return new Aplasta{ 
		{ a_config["script/path"], a_config["script/scope"] }, 
		a_config["up_wait_time"], 
		a_config["down_time"], 
		a_config["down_wait_time"], 
		a_config["up_time"],
		a_config["changing_time"],
		a_config["particle_system_crush"],
		a_config["crush_sound"],
		a_config["sound_range"],
	};
}

Aplasta::Aplasta(const ScriptConfig& a_scriptConfig, f64 a_upWaitTime, f64 a_downTime, f64 a_downWaitTime, f64 a_upTime, f64 a_changingTime,
	const std::string& a_particleSystemAnimationId, const std::string& a_crushSoundId, f32 a_soundRange) :
	MagnetiaActor{ 0, a_scriptConfig },
	m_originPosition{ },
	m_onGroundPosition{ },
	m_upWaitTime{ a_upWaitTime },
	m_downTime{ a_downTime },
	m_downWaitTime{ a_downWaitTime },
	m_upTime{ a_upTime },
	m_changingTime{ a_changingTime },
	m_currentMovementBehavior{ nullptr },
	m_particleSystemAnimationId{ a_particleSystemAnimationId },
	m_crushSoundId{ a_crushSoundId },
	m_soundRange{ a_soundRange }
{

}

Aplasta::~Aplasta()
{

}

Actor* Aplasta::clone()
{
	return new Aplasta{ getScriptConfig(), m_upWaitTime, m_downTime, m_downWaitTime, m_upTime, m_changingTime, m_particleSystemAnimationId, m_crushSoundId, m_soundRange };
}

void Aplasta::goDown()
{
	// move downwards
	getParent()->removeBehavior(m_currentMovementBehavior);
	m_currentMovementBehavior = new FlyStraightAnimator{ m_downTime, m_originPosition.to_OgreVector3(), m_onGroundPosition.to_OgreVector3() };
	getParent()->addBehavior(m_currentMovementBehavior);
}

void Aplasta::goUp()
{
	// move upwards
	getParent()->removeBehavior(m_currentMovementBehavior);
	m_currentMovementBehavior = new FlyStraightAnimator{ m_upTime, m_onGroundPosition.to_OgreVector3(), m_originPosition.to_OgreVector3() };
	getParent()->addBehavior(m_currentMovementBehavior);
}

void Aplasta::initImpl()
{
	MagnetiaActor::initImpl();

	// ray end point
	const edv::Vector3f worldPos = getParent()->getWorldPosition();
	edv::Vector3f dest{ worldPos.x, worldPos.y + 1000, worldPos.z };

	// ray cast to find the ceiling
	btClosestNotMeRayResultCallback callbackCeiling{
		getParent(),
		worldPos.to_btVector3(),
		dest.to_btVector3(),
		EntityBtPhysics::EdvCollisionFlags::CF_TRIGGER | btCollisionObject::CF_NO_CONTACT_RESPONSE
	};

	CCoreEngine::Instance().GetPhysicsManager().RayTestNotMe(worldPos, dest, callbackCeiling);

	if (callbackCeiling.hasHit())
	{
		m_originPosition = edv::Vector3f{
			callbackCeiling.m_hitPointWorld.x(),
			callbackCeiling.m_hitPointWorld.y() - getParent()->getScaledHalfSize().y,
			callbackCeiling.m_hitPointWorld.z()
		};
	}
	else
	{
		throw std::runtime_error{ "no ceiling found for " + getParent()->getName() };
	}

	// ray cast to find the ground
	dest.y = worldPos.y - 1000;

	btClosestNotMeRayResultCallback callbackGround{
		getParent(),
		worldPos.to_btVector3(),
		dest.to_btVector3(),
		EntityBtPhysics::EdvCollisionFlags::CF_TRIGGER | btCollisionObject::CF_NO_CONTACT_RESPONSE
	};

	CCoreEngine::Instance().GetPhysicsManager().RayTestNotMe(worldPos, dest, callbackGround);

	if (callbackGround.hasHit())
	{
		m_onGroundPosition = edv::Vector3f{ 
			callbackGround.m_hitPointWorld.x(), 
			callbackGround.m_hitPointWorld.y() + getParent()->getScaledHalfSize().y, 
			callbackGround.m_hitPointWorld.z()
		};
	}
	else
	{
		throw std::runtime_error{ "no ground found for " + getParent()->getName() };
	}

	// set aplasta on top position
	getParent()->setWorldPosition(m_originPosition);
}

f64 Aplasta::getUpWaitTime() const
{
	return m_upWaitTime;
}

f64 Aplasta::getDownTime() const
{
	return m_downTime;
}

f64 Aplasta::getDownWaitTime() const
{
	return m_downWaitTime;
}

f64 Aplasta::getUpTime() const
{
	return m_upTime;
}

f64 Aplasta::getChangingTime() const
{
	return m_changingTime;
}

const edv::Vector3f& Aplasta::getOriginPosition() const
{
	return m_originPosition;
}

const edv::Vector3f& Aplasta::getGroundPosition() const
{
	return m_onGroundPosition;
}

void Aplasta::playCrushAnimation()
{
	// right particle
	{
		std::string particleSystemName{ getParent()->getName() + "Aplasta_smoke_right" };
		getParent()->getGraphics()->removeParticleSystem(particleSystemName);
		getParent()->getGraphics()->addParticleSystem(particleSystemName, EntityOgreGraphics::ParticleConfig{ "", m_particleSystemAnimationId, "", { getParent()->getScaledHalfSize().x, -getParent()->getScaledHalfSize().y, 0.f } });
		Ogre::ParticleSystem* ps = static_cast<Ogre::ParticleSystem*>(getParent()->getGraphics()->getParticleSystemNode(particleSystemName)->getAttachedObject(0));
		ps->getEmitter(0)->setDirection({ 1.f, 0.f, 0.f });
	}

	// left particle
	{
		std::string particleSystemName{ getParent()->getName() + "Aplasta_smoke_left" };
		getParent()->getGraphics()->removeParticleSystem(particleSystemName);
		getParent()->getGraphics()->addParticleSystem(particleSystemName, EntityOgreGraphics::ParticleConfig{ "", m_particleSystemAnimationId, "", { -getParent()->getScaledHalfSize().x, -getParent()->getScaledHalfSize().y, 0.f } });
		Ogre::ParticleSystem* ps = static_cast<Ogre::ParticleSystem*>(getParent()->getGraphics()->getParticleSystemNode(particleSystemName)->getAttachedObject(0));
		ps->getEmitter(0)->setDirection({ -1.f, 0.f, 0.f });
	}

	// sound
	GameEntity* const player{ CCoreEngine::Instance().GetGameManager().getPlayer() };

	if (player)
	{
		if (getParent()->distanceToEntity(player) <= m_soundRange)
		{
			CCoreEngine::Instance().GetSoundManager().playSound(m_crushSoundId);
		}
	}
}
