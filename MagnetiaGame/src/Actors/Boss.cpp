#include <Core/CCoreEngine.h>
#include <Game/GameManager.h>
#include <Game/FlyStraightAnimator.h>
#include "GameLevel.h"
#include "BossEscapeState.h"
#include "BossWaitState.h"
#include "BossTransitionState.h"
#include "BossConfrontState.h"
#include "Actors/Boss.h"

Actor* Boss::Factory::createElement(const Actor::factory_param_type& a_config)
{
	PathsParam paths;
	Actor::factory_param_type pathsTree = a_config.try_get_tree("escape/paths");

	if (!pathsTree.empty())
	{
		// #todo:aeroport edv::tree<T>::iterator
		Actor::factory_param_type::keys_type pathKeys = pathsTree.keys();

		for (const auto& key : pathKeys)
		{
			paths.push_back(pathsTree[key]);
		}
	}

	return new Boss{ 
		a_config["max_life"], 
		ScriptConfig{ a_config["script/path"], a_config["script/scope"] },
		paths,
		edv::range64f{ a_config["escape/min_acceleration"], a_config["escape/max_acceleration"] },
		edv::range64f{ a_config["escape/min_mine_drop_time"], a_config["escape/max_mine_drop_time"] },
		a_config["escape/acceleration_radius"],
		edv::range32f{ a_config["confront/y_variation/min"], a_config["confront/y_variation/max"] },
		edv::range64f{ a_config["confront/duration/min"], a_config["confront/duration/max"] },
		edv::range32{ a_config["confront/shoots/min"], a_config["confront/shoots/max"] },
		a_config["wait/distance"]
	};
}

Boss::Boss(
		s32 a_maxLife,
		const ScriptConfig& a_scriptConfig,
		const PathsParam& a_pathsName,
		const edv::range64f& a_acceleration,
		const edv::range64f& a_mineDropTime,
		f64 a_accelRadius,
		const edv::range32f& a_confrontVariationY,
		const edv::range64f& a_confrontMoveDuration,
		const edv::range32& m_confrontShoots,
		f32 a_waitDistance) :
	MagnetiaActor{ a_maxLife, a_scriptConfig },
	m_pathsName{ a_pathsName },
	m_acceleration{ a_acceleration },
	m_mineDropTime{ a_mineDropTime },
	m_accelRadius{ a_accelRadius },
	m_confrontVariationY{ a_confrontVariationY },
	m_confrontMoveDuration{ a_confrontMoveDuration },
	m_waitDistance{ a_waitDistance },
	m_states{ },
	m_currentState{ }
{
	for (PathsParam::const_iterator it = m_pathsName.begin(); it != m_pathsName.end(); it++)
	{
		const std::string& pathName = (*it);
		BossEscapeState* const escapeState{ new BossEscapeState{ this, pathName, m_acceleration, m_mineDropTime, m_accelRadius } };
		
		m_states.emplace_back(escapeState);
		m_states.emplace_back(new BossWaitState{ this, m_waitDistance, escapeState });
		m_states.emplace_back(new BossConfrontState{ this, m_confrontVariationY, m_confrontMoveDuration, m_confrontShoots });
		m_states.emplace_back(new BossTransitionState{ this });
	}	
}

Boss::~Boss()
{
	
}

Actor* Boss::clone()
{
	return new Boss{ 
		getMaxLife(), 
		getScriptConfig(), 
		m_pathsName, 
		m_acceleration, 
		m_mineDropTime, 
		m_accelRadius,
		m_confrontVariationY,
		m_confrontMoveDuration
	};
}

void Boss::kill()
{
	decreaseLife(1);

	if (getLife() == 0)
	{
		GameLevel* const currentLevel{ static_cast<GameLevel*>(CCoreEngine::Instance().GetGameManager().getCurrentLevel()) };

		if (!currentLevel->isPlayerAtEnd())
		{
			currentLevel->setPlayerAtEnd(true);
		}
	}
}

edv::Vector3f Boss::getNextStartPoint() const
{
	edv::Vector3f nextStartPoint;

	if (!m_states.empty())
	{
		States::const_iterator nextState{ m_currentState + 1 == m_states.end() ? m_states.begin() : m_currentState + 1 };
		
		nextStartPoint = (*nextState)->getStartPoint();
	}

	return nextStartPoint;
}

void Boss::initImpl()
{
	CCoreEngine::Instance().GetGameManager().registerBoss(getParent());

	if (!m_states.empty())
	{
		m_currentState = m_states.begin();
		(*m_currentState)->start();
	}
}

void Boss::updateImpl(f64 dt)
{
	MagnetiaActor::updateImpl(dt);

	if (!m_states.empty())
	{
		(*m_currentState)->update(dt);

		if ((*m_currentState)->finished())
		{
			m_currentState++;

			if (m_currentState == m_states.end())
			{
				m_currentState = m_states.begin();
			}

			(*m_currentState)->start();
		}
	}
}

void Boss::stopImpl()
{
	for (StatePtr& state : m_states)
	{
		state->stop();
	}

	CCoreEngine::Instance().GetGameManager().unregisterBoss();
}
