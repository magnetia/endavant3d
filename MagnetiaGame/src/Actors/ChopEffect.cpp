#include "ChopEffect.h"
#include <Core/CCoreEngine.h>
#include <Game/GameManager.h>
#include <cmath>

ChopEffect::ChopEffect(const edv::Vector3f& a_origin, const tEntityIds& a_entitiesIds, u32 a_count, f32 a_force, const edv::Vector3f& a_gravity)
{
	auto &game = CCoreEngine::Instance().GetGameManager();
	edv::Vector3f pos{ a_origin };
	for (u32 i = 0; i < a_count; i++)
	{
		auto entityId = a_entitiesIds.at(std::rand() % a_entitiesIds.size());
		f32 angle = static_cast<f32>((std::acos(-1)*2.f / a_count)*i+1); // acos(-1) = PI
		edv::Vector3f dir{ std::cos(angle), std::sin(angle), 0.f };
		auto entity = game.createEntity(entityId, (pos + dir*3.f).to_OgreVector3()).second;
		entity->getPhysics()->getRigidBody()->applyCentralImpulse((dir*a_force).to_btVector3());
		f32 scaleFactor = (std::rand() / (static_cast <f32> (RAND_MAX))) / 2.f + 0.5f;
		entity->setScale( { scaleFactor, scaleFactor, 1.f });
		entity->getPhysics()->getRigidBody()->setGravity(a_gravity.to_btVector3());
		m_entities.push_back(entity);
	}
}

ChopEffect::ChopEffect(const edv::Vector3f& a_origin, const tEntityIds& a_entitiesIds, f32 a_force, const edv::Vector3f& a_gravity, const Ogre::Quaternion& a_rotation)
{
	auto &game = CCoreEngine::Instance().GetGameManager();
	edv::Vector3f pos{ a_origin };
	u32 count{ a_entitiesIds.size() };
	for (u32 i = 0; i < count; i++)
	{
		auto entityId = a_entitiesIds.at(i);
		f32 HalfPi = static_cast <f32> (std::acos(-1) / 2.f);
		f32 angle = (std::rand() / (static_cast <f32> (RAND_MAX))*HalfPi) + (i % 4)*HalfPi;
		edv::Vector3f dir{ std::cos(angle), std::sin(angle), (std::rand() / static_cast <f32> (RAND_MAX))*2.f - 1.f };
		auto entity = game.createEntity(entityId, (pos + dir*3.f).to_OgreVector3(), a_rotation).second;
		entity->getPhysics()->getRigidBody()->applyImpulse((dir*a_force).to_btVector3(), btVector3{ (std::rand() / static_cast <f32> (RAND_MAX))*2.f, (std::rand() / static_cast <f32> (RAND_MAX))*2.f, (std::rand() / static_cast <f32> (RAND_MAX))*2.f });
		entity->getPhysics()->getRigidBody()->setGravity(a_gravity.to_btVector3());
		m_entities.push_back(entity);
	}
}

ChopEffect::~ChopEffect()
{
	auto &game = CCoreEngine::Instance().GetGameManager();
	for (auto& entity : m_entities)
	{
		game.destroyEntity(entity);
	}
}