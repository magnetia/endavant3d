#ifndef MAGNETIA_GAME_BOSS_STATE_H_
#define MAGNETIA_GAME_BOSS_STATE_H_

#include <Core/CBasicTypes.h>
#include <Utils/edvVector3.h>

class Boss;

class BossState
{
public:
	BossState(Boss* a_boss) : m_boss{ a_boss } { }
	virtual ~BossState() { }

	virtual void start() = 0;
	virtual void update(f64 dt) = 0;
	virtual void stop() = 0;
	virtual bool finished() = 0;
	virtual edv::Vector3f getStartPoint() const = 0;

protected:
	Boss* m_boss;
};

#endif
