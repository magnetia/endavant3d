#ifndef MAGNETIA_NIK_H_
#define MAGNETIA_NIK_H_

#include <Game/MouseListener.h>
#include "Player.h"
#include "PlayerController.h"
#include "ForceRay.h"

class ParticleSystemAnimator;
class ChopEffect;
class SkeletonDebug;

class Nik : public Player
{
public:
	// Factory
	class Factory : public Actor::factory_type
	{
	public:
		Actor* createElement(const Actor::factory_param_type& a_config) override;
	};

	struct Config
	{
		std::string left_arm_bone_name;
		std::string right_arm_bone_name;
		std::string left_ray_origin_bone_name;
		std::string right_ray_origin_bone_name;
		std::string init_animation_name;
		std::string idle_animation_name;
		f64 idle_animation_trigger_time;
		std::string run_left_animation_name;
		std::string run_right_animation_name;
		f32 run_animation_speed_factor;
		std::string fly_forward_animation_name;
		std::string fly_backward_animation_name;
		std::string falling_animation_name;
		f64 blending_animation_time;
		f32 death_animation_force;
		edv::Vector3f death_animation_gravity;

		Config(const std::string& a_left_arm_bone_name = "", const std::string& a_right_arm_bone_name = "", const std::string& a_left_ray_origin_bone_name = "", 
			const std::string& a_right_ray_origin_bone_name = "", const std::string& a_init_animation_name = "", const std::string& a_idle_animation_name = "", f64 a_idle_animation_trigger_time = 0.,
			const std::string& a_run_left_animation_name = "", const std::string& a_run_right_animation_name = "", f32 a_run_animation_speed_factor = 1.f, const std::string& a_fly_forward_animation_name = "",
			const std::string& a_fly_backward_animation_name = "", const std::string& a_falling_animation_name = "", f64 a_blending_animation_time = 0., f32 a_death_animation_force = 0.f, const edv::Vector3f& a_death_animation_gravity = {}) :
			left_arm_bone_name{ a_left_arm_bone_name }, right_arm_bone_name{ a_right_arm_bone_name }, left_ray_origin_bone_name{ a_left_ray_origin_bone_name }, right_ray_origin_bone_name{ a_right_ray_origin_bone_name },
			init_animation_name{ a_init_animation_name }, idle_animation_name{ a_idle_animation_name }, idle_animation_trigger_time{ a_idle_animation_trigger_time }, run_left_animation_name{ a_run_left_animation_name }, run_right_animation_name{ a_run_right_animation_name },
			run_animation_speed_factor{ a_run_animation_speed_factor }, fly_forward_animation_name{ a_fly_forward_animation_name }, fly_backward_animation_name{ a_fly_backward_animation_name }, falling_animation_name{ a_falling_animation_name }, blending_animation_time{ a_blending_animation_time },
			death_animation_force{ a_death_animation_force }, death_animation_gravity{ a_death_animation_gravity }
		{}
	};
	
	Nik(s32 a_maxLife = 0, const ScriptConfig& a_scriptConfig = {}, const PlayerController::sControllerConfig& a_controller_config = {},
		const ForceRay::RayConfig& a_rayConfig = {}, const Config& a_config = {});
	~Nik();
	Actor* clone() override;

	void setnverseMagneticControl(bool a_value);
	void OnMouseDown(MouseListener::MouseButton a_button);
	void OnMouseUp(MouseListener::MouseButton a_button);
	void OnMouseHover(bool a_hover);

	void	setRayRadius(f32 a_radius);

	void applyMove(const edv::Vector3f & a_vector) override;
	void kill() override;
	void setGodMode(bool a_active) override;
	void setInputEnabled(bool a_enabled) override;

protected:
	void initImpl() override;
	void updateImpl(f64 dt) override;
	void stopImpl() override;
	void clear() override;

	ActorMovement* createMovement() override;

private:
	enum eStates
	{
		STATE_NONE,
		STATE_IDLE,
		STATE_IDLE_TO_WALK,
		STATE_WALK,
		STATE_WALK_TO_IDLE,
		STATE_IDLE_TO_FLY,
		STATE_WALK_TO_FLY,
		STATE_FLY,
		STATE_FLY_TO_WALK,
		STATE_DEATH
	};

	void startMagnetism(MagneticProperty::eMagneticCharge a_charge, const edv::Vector3f& a_vector);
	void finishMagnetism();
	void stopAllAnimations();
	void target();
	void shoot();
	void checkFacingByRay();
	void checkFacing() override;

	typedef std::vector<Behavior::Id> tBevaviorsIds;
	tBevaviorsIds						m_inputBehaviorIds;
	ForceRay							m_rayforce;
	MagneticProperty::eMagneticCharge	m_magnetic_configuraion[2];
	eStates								m_lastState;
	PlayerController::sControllerConfig	m_movement_config;
	MagneticRayInfo						m_magneticRayInfo;
	Config								m_config;

	// Animations
	std::string							m_current_animation;
	Ogre::Bone*							m_left_arm_bone;
	Ogre::Bone*							m_right_arm_bone;
	Ogre::Bone*							m_left_ray_origin_bone;
	Ogre::Bone*							m_right_ray_origin_bone;

	ParticleSystemAnimator*				m_death_particles;
	ChopEffect*							m_death_effect;
	
	// Debug
	std::unique_ptr<SkeletonDebug>		m_skeleton_debug;
};

#endif
