#ifndef MAGNETIA_PLATFORM_H_
#define MAGNETIA_PLATFORM_H_

#include "MagnetiaActor.h"

class Platform : public MagnetiaActor
{
public:
	class Factory : public Actor::factory_type
	{
	public:
		Actor* createElement(const Actor::factory_param_type& a_config) override;
	};

	Platform(f64 a_duration = 0.f, const edv::Vector3f& a_offset = {}, const ScriptConfig& a_scriptConfig = {});
	Actor* clone() override;

private:
	void initImpl() override;
	void updateImpl(f64 dt) override;

	f64				m_duration;
	edv::Vector3f	m_offset;
	edv::Vector3f	m_origin;
	edv::Vector3f	m_dest;
};

#endif
