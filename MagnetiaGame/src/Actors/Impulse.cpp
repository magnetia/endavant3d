#include <functional>
#include <Core/CCoreEngine.h>
#include <Game/GameManager.h>
#include <Game/GameEntity.h>
#include "Impulse.h"

Actor* Impulse::Factory::createElement(const Actor::factory_param_type& a_config)
{
	return new Impulse{ 
		ScriptConfig{ a_config["script/path"], a_config["script/scope"] }, 
		edv::Vector3f{ a_config["impulse/x"], a_config["impulse/y"], a_config["impulse/z"] },
		a_config["sound"]
	};
}

Impulse::Impulse(const ScriptConfig& a_scriptConfig, const edv::Vector3f& a_impulseVector, const std::string& a_soundId) :
	MagnetiaActor{ 0, a_scriptConfig },
	m_impulseVector{ a_impulseVector },
	m_soundId{ a_soundId },
	m_channelId{ CSoundManager::UNASSIGNED_CHANNEL_ID }
{
	if (!m_soundId.empty())
	{
		CCoreEngine::Instance().GetSoundManager().preloadSound(m_soundId);
	}
}

Impulse::~Impulse()
{
	if (!m_soundId.empty())
	{
		CCoreEngine::Instance().GetSoundManager().releaseSound(m_soundId);
	}
}

Actor* Impulse::clone()
{
	return new Impulse{ getScriptConfig(), m_impulseVector, m_soundId };
}

const edv::Vector3f& Impulse::getImpulseVector() const
{
	return m_impulseVector;
}

void Impulse::ejectPlayer()
{
	GameEntity* const player{ CCoreEngine::Instance().GetGameManager().getPlayer() };

	if (player)
	{
		Actor* const playerActor{ player->getActor() };

		if (playerActor)
		{
			playerActor->resetVelocity();
			playerActor->applyImpulse(m_impulseVector);

			if (!m_soundId.empty() && m_channelId == CSoundManager::UNASSIGNED_CHANNEL_ID)
			{
				auto functor = std::bind(&Impulse::soundFinished, this, std::placeholders::_1);
				m_channelId = CCoreEngine::Instance().GetSoundManager().playSound("impulse_sound", functor);
			}
		}
	}
}

void Impulse::soundFinished(CSoundManager::t_ChannelId a_channel)
{
	m_channelId = CSoundManager::UNASSIGNED_CHANNEL_ID;
}

void Impulse::initImpl()
{
	getParent()->getGraphics()->getNode()->setVisible(false);
}
