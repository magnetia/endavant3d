#ifndef MAGNETIA_GAME_APLASTA_H_
#define MAGNETIA_GAME_APLASTA_H_

#include "MagnetiaActor.h"

class Behavior;

class Aplasta : public MagnetiaActor
{
public:
	// Factory
	class Factory : public Actor::factory_type
	{
	public:
		Actor* createElement(const Actor::factory_param_type& a_config) override;
	};

	Aplasta(const ScriptConfig& a_scriptConfig = {}, f64 a_upWaitTime = 0, f64 a_downTime = 0, f64 a_downWaitTime = 0, f64 a_upTime = 0, f64 a_changingTime = 0,
		const std::string& a_particleSystemAnimationId = "", const std::string& a_crushSoundId = "", f32 a_rangeSound = 0.f);
	~Aplasta();
	Actor* clone() override;

	void goDown();
	void goUp();
	f64 getUpWaitTime() const;
	f64 getDownTime() const;
	f64 getDownWaitTime() const;
	f64 getUpTime() const;
	f64 getChangingTime() const;
	const edv::Vector3f& getOriginPosition() const;
	const edv::Vector3f& getGroundPosition() const;
	void playCrushAnimation();

protected:
	void initImpl() override;

private:
	edv::Vector3f	m_originPosition;
	edv::Vector3f	m_onGroundPosition;
	f64				m_upWaitTime;
	f64				m_downTime;
	f64				m_downWaitTime;
	f64				m_upTime;
	f64				m_changingTime;
	Behavior*		m_currentMovementBehavior;
	std::string		m_particleSystemAnimationId;
	std::string		m_crushSoundId;
	f32				m_soundRange;
};

#endif
