#ifndef EDV_GAME_SPACESHIP_CONTROLLER_H_
#define EDV_GAME_SPACESHIP_CONTROLLER_H_

#include <Game/KinematicController.h>

class SpaceshipController : public KinematicController
{
public:

	struct sControllerConfig : sMovementConfig
	{
		
		f32				X_acceleration;
		f32				Y_acceleration;
		f32				max_velocity;
		sControllerConfig(const edv::Vector3f& a_gravity = { 0.f, -9.8f, 0.f }, f32 a_restitution = 0.1f, f32 a_sliding = 0.1f, f32 a_friction = 0.f,
			f32 a_X_acceleration = 0.f, f32 a_Y_acceleration = 0.f, f32 max_velocity = 0.f) :
			sMovementConfig{ a_gravity, a_restitution, a_sliding, a_friction },
			X_acceleration{ a_X_acceleration }, Y_acceleration{ a_Y_acceleration }, max_velocity{ max_velocity }
		{ }
	};


	SpaceshipController(const sControllerConfig& a_controller_config = {});
	void applyMove(const edv::Vector3f& a_vector) override;
	void applyForce(const edv::Vector3f& a_vector) override;
	edv::Vector3f applyFriction(const edv::Vector3f& a_velocity) override;
	virtual void updateVelocityOnCollision(edv::Vector3f& a_velocity, const edv::Vector3f& a_normal_hit, f32 a_tangentFactor, f32 a_normalFactor) override;

private:
	
	sControllerConfig		m_controller_config;
};

#endif
