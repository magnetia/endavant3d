#include <Core/CCoreEngine.h>
#include <Renderer/CRenderManager.h>
#include <Options/OptionManager.h>
#include <Sound/CSoundManager.h>
#include <Game/GameEntity.h>
#include <Game/ParticleSystemAnimator.h>
#include "NeutralBall.h"

Actor* NeutralBall::Factory::createElement(const Actor::factory_param_type& a_config)
{
	return new NeutralBall{ 
		a_config["max_life"].as<s32>(), 
		ScriptConfig{ a_config["script/path"], a_config["script/scope"] },
		a_config["animation_duration"],
		edv::Vector3f{ a_config["animation_scale/x"], a_config["animation_scale/y"], a_config["animation_scale/z"] }
	};
}

NeutralBall::NeutralBall(s32 a_maxLife, const ScriptConfig& a_scriptConfig, f64 a_animationDuration, const edv::Vector3f& a_animationScale) :
	MagnetiaActor{ a_maxLife, a_scriptConfig },
	m_animationDuration{ a_animationDuration },
	m_animationScale{ a_animationScale},
	m_playingAnimation{ false }
{

}

NeutralBall::~NeutralBall()
{

}

Actor* NeutralBall::clone()
{
	return new NeutralBall{ getMaxLife(), getScriptConfig(), m_animationDuration, m_animationScale };
}

f64 NeutralBall::getAnimationDuration() const
{
	return m_animationDuration;
}

bool NeutralBall::isPlayingAnimation() const
{
	return m_playingAnimation;
}

void NeutralBall::playGetAnimation()
{
	if (!m_playingAnimation)
	{
		getParent()->getGraphics()->getParticleSystemNode(getParent()->getName() + "neutroball_particle")->setVisible(false);

		CCoreEngine::Instance().GetSoundManager().playSound("pick_neutroball");

		getParent()->addBehavior(new ParticleSystemAnimator{ getParent()->getName() + "getAnimation", 
			m_animationDuration * 0.5, "NeutroBallGet", "", {}, m_animationScale, ParticleSystemAnimator::ON_STOP_DISABLE_EMITTERS });
		m_playingAnimation = true;
	}
}

void NeutralBall::initImpl()
{
	getParent()->removeAllBehaviors();
	m_playingAnimation = false;
}
