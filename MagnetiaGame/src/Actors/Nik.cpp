#include <utility>
#include "Game/GameEntity.h"
#include "Nik.h"
#include "Input/CInputManager.h"
#include "Input/CInputMouse.h"
#include "Core/CCoreEngine.h"
#include "Camera/CameraManager.h"
#include "Camera/Camera.h"
#include "PlayerController.h"
#include "OgreAnimation.h"
#include <Game/ParticleSystemAnimator.h>
#include <Game/GameManager.h>
#include "ChopEffect.h"

// #debug:
#include <iostream>
#include <Core/CCoreEngine.h>
#include "Renderer/CRenderDebug.h"
#include <Game/AlphaTransitionAnimator.h>
#include <Game/ChainedBehavior.h>

#include "Core/CCoreEngine.h"
#include "Core/CLogManager.h"
#include <Renderer/SkeletonDebug.h>

Actor* Nik::Factory::createElement(const Actor::factory_param_type& a_config)
{
	return new Nik{
		a_config["max_life"],
		{ a_config["script/path"], a_config["script/scope"] },
		{
			{ a_config["movement/height_check_margin"] },
			{ a_config["movement/acceleration"] },
			{ a_config["movement/deceleration"] },
			{ a_config["movement/gravity/x"], a_config["movement/gravity/y"], a_config["movement/gravity/z"] }, a_config["movement/restitution"], a_config["movement/sliding"], a_config["movement/friction"],
			{ a_config["movement/ground_max_velocity"] },
			a_config["movement/air_friction"], a_config["movement/air_acceleration"],
			{ a_config["movement/magnetic_acceleration"] }
		},
		ForceRay::RayConfig{ a_config["ray_force/radius"].as<f32>(), a_config["ray_force/displ_factor"].as<f32>(), a_config["ray_force/sound"],
			a_config["ray_force/ray_particles_node"], a_config["ray_force/pos_fx_particles_node_name"], a_config["ray_force/neg_fx_particles_node_name"]
			, a_config["ray_force/raiggran_pos_node_name"], a_config["ray_force/raiggran_neg_node_name"], a_config["ray_force/raiggranstart_pos_node_name"], a_config["ray_force/raiggranstart_neg_node_name"]
			, a_config["ray_force/raiggranfinal_pos_node_name"], a_config["ray_force/raiggranfinal_neg_node_name"], a_config["ray_force/raig_pointer_node_name"]
			}
		,
		Nik::Config{
				a_config["configuration/left_arm_bone_name"], a_config["configuration/right_arm_bone_name"], a_config["configuration/left_ray_origin_bone_name"], a_config["configuration/right_ray_origin_bone_name"], 
				a_config["configuration/init_animation_name"], a_config["configuration/idle_animation_name"], a_config["configuration/idle_animation_trigger_time"], a_config["configuration/run_left_animation_name"], 
				a_config["configuration/run_right_animation_name"], a_config["configuration/run_animation_speed_factor"], a_config["configuration/fly_forward_animation_name"], a_config["configuration/fly_backward_animation_name"], 
				a_config["configuration/falling_animation_name"], a_config["configuration/blending_animation_time"], a_config["configuration/death_animation_force"], { a_config["configuration/death_animation_gravity/x"], 
				a_config["configuration/death_animation_gravity/y"], a_config["configuration/death_animation_gravity/z"] }
			}
	};
}

Nik::Nik(s32 a_maxLife, const ScriptConfig& a_scriptConfig, const PlayerController::sControllerConfig& a_controller_config, const ForceRay::RayConfig& a_rayConfig, const Config& a_config) :
	Player{ a_maxLife, a_scriptConfig },
	m_rayforce{ a_rayConfig },
	m_movement_config{ a_controller_config },
	m_config{ a_config },
	m_current_animation{ m_config.init_animation_name },
	m_left_arm_bone{ nullptr },
	m_right_arm_bone{ nullptr },
	m_left_ray_origin_bone{ nullptr },
	m_right_ray_origin_bone{ nullptr },
	m_death_particles{ nullptr },
	m_death_effect{ nullptr }
{
	m_rayforce.setParent(this);

	m_state = STATE_FLY;
	m_lastState = STATE_NONE;
	setnverseMagneticControl(false);
}

Nik::~Nik()
{

}

Actor* Nik::clone()
{
	// #todo: passar ray config
	return new Nik{ getMaxLife(), getScriptConfig(), m_movement_config, {}, m_config };
}

void Nik::setnverseMagneticControl(bool a_value)
{
	m_magnetic_configuraion[0] = a_value ? MagneticProperty::MAGNET_TYPE_POSITIVE : MagneticProperty::MAGNET_TYPE_NEGATIVE;
	m_magnetic_configuraion[1] = !a_value ? MagneticProperty::MAGNET_TYPE_POSITIVE : MagneticProperty::MAGNET_TYPE_NEGATIVE;
}

void Nik::initImpl()
{
	MagnetiaActor::initImpl();

	Ogre::Entity* nikEntity = getParent()->getGraphics()->getMainEntity();
	m_left_arm_bone = EntityOgreGraphics::getBone(nikEntity, m_config.left_arm_bone_name);
	if (m_left_arm_bone)
	{
		m_left_arm_bone->setManuallyControlled(true);
	}

	m_right_arm_bone = EntityOgreGraphics::getBone(nikEntity, m_config.right_arm_bone_name);
	if (m_right_arm_bone)
	{
		m_right_arm_bone->setManuallyControlled(true);
	}
	m_rayforce.init();

	m_left_ray_origin_bone = EntityOgreGraphics::getBone(nikEntity, m_config.left_ray_origin_bone_name);
	m_right_ray_origin_bone = EntityOgreGraphics::getBone(nikEntity, m_config.right_ray_origin_bone_name);

	// Input behaviors
	const Behavior::Id mouseBehavior = getParent()->addBehavior(new MouseListener(std::bind(&Nik::OnMouseDown, this, std::placeholders::_1), std::bind(&Nik::OnMouseUp, this, std::placeholders::_1), std::bind(&Nik::OnMouseHover, this, std::placeholders::_1)));
	const Behavior::Id controllerBehavior = 2LL; // Controller behavior (XML)
	m_inputBehaviorIds.push_back(mouseBehavior);
	m_inputBehaviorIds.push_back(controllerBehavior);

	m_skeleton_debug.reset(new SkeletonDebug{ getParent()->getGraphics()->getMainEntity(), CCoreEngine::Instance().GetRenderManager().GetSceneManager(), 0.3f});
	//m_skeleton_debug->showNames(true);
	//m_skeleton_debug->showAxes(true);
	//m_skeleton_debug->showBones(true);
}

void Nik::updateImpl(f64 dt)
{
	if (m_state != STATE_DEATH)
	{
		target();
		shoot();
		m_rayforce.update(dt);
		checkFacingByRay();
	}

	switch(m_state)
	{
	case STATE_IDLE:
	{
		if (m_lastState != STATE_IDLE)
		{
			stopAllAnimations();
			m_current_animation = m_config.init_animation_name;
			m_lastState = STATE_IDLE;
		}
		else
		{
			if (m_movement.get())
			{
				if (m_movement->onGround() && m_movement->hasMovement())
				{
					setState(STATE_IDLE_TO_WALK);
				}
				else if (!m_movement->onGround())
				{
					setState(STATE_IDLE_TO_FLY);
				}
				else
				{
					if (stateElapsed() > m_config.idle_animation_trigger_time)
					{
						const std::string& animationName{ m_config.idle_animation_name };
						if (auto* anim = getParent()->getGraphics()->getAnimation(animationName))
						{
							if (!anim->getEnabled())
							{
								anim->setTimePosition(0.f);
								anim->setEnabled(true);
								m_current_animation = animationName;
							}
							else if (anim->hasEnded())
							{
								anim->setEnabled(false);
								setState(STATE_IDLE);
								m_current_animation = m_config.init_animation_name;
							}
						}
					}
				}
			}
		}
	}
	break;

	case STATE_IDLE_TO_WALK:
	{
		if (m_lastState != STATE_IDLE_TO_WALK)
		{
			stopAllAnimations();
			const std::string& animationName{ getOrientation() == FACING_LEFT ? m_config.run_left_animation_name : m_config.run_right_animation_name };
			getParent()->getGraphics()->blendAnimations(m_current_animation, animationName, AnimationBlender::BlendWhileAnimating, static_cast<f32>(m_config.blending_animation_time));
			m_current_animation = animationName;
			m_lastState = STATE_IDLE_TO_WALK;
		}
		else
		{
			if (stateElapsed() > m_config.blending_animation_time)
			{
				setState(STATE_WALK);
			}
		}
	}
	break;
	
	case STATE_WALK:
	{
		if (m_lastState != STATE_WALK)
		{
			const std::string& animationName{ getOrientation() == FACING_LEFT ? m_config.run_left_animation_name : m_config.run_right_animation_name };
			if (auto* anim = getParent()->getGraphics()->getAnimation(animationName))
			{
				anim->setEnabled(true);
			}
			m_current_animation = animationName;
			m_lastState = STATE_WALK;
		}
		else
		{
			if (m_movement.get())
			{
				f32 speed{ static_cast<PlayerController*>(m_movement.get())->getMovementSpeedFactor() };
				const f32 minSpeed{ 0.2f };
				speed = (std::abs(speed) < minSpeed) ? ((speed > 0.f) ? minSpeed : -minSpeed) : speed;
				speed *= ((getDirection() == DIRECTION_FORWARD) ? 1.f : -1.f)*m_config.run_animation_speed_factor;
				getParent()->getGraphics()->setAnimationSpeed(m_current_animation, speed);

				if (m_movement->onGround() && !m_movement->hasMovement())
				{
					setState(STATE_WALK_TO_IDLE);
				}
				else if (!m_movement->onGround())
				{
					setState(STATE_WALK_TO_FLY);
				}
			}
		}
	}
	break;

	case STATE_WALK_TO_IDLE:
	{
		if (m_lastState != STATE_WALK_TO_IDLE)
		{
			const std::string& animationName{ m_config.init_animation_name };
			getParent()->getGraphics()->blendAnimations(m_current_animation, animationName, AnimationBlender::BlendWhileAnimating, static_cast<f32>(m_config.blending_animation_time));
			m_current_animation = animationName;
			m_lastState = STATE_WALK_TO_IDLE;
		}
		else
		{
			if (stateElapsed() > m_config.blending_animation_time)
			{
				setState(STATE_IDLE);
			}
		}
	}
	break;

	case STATE_IDLE_TO_FLY:
	{
		if (m_lastState != STATE_IDLE_TO_FLY)
		{
			if (m_movement.get() && static_cast<PlayerController*>(m_movement.get())->hasMagnetism())
			{
				edv::Vector3f velocity{ m_movement->getVelocity() };
				const std::string& animationName{ getOrientation() == FACING_RIGHT ? (velocity.x > 0.f ? m_config.fly_forward_animation_name : m_config.fly_backward_animation_name) : (velocity.x < 0.f ? m_config.fly_forward_animation_name : m_config.fly_backward_animation_name) };
				getParent()->getGraphics()->blendAnimations(m_current_animation, animationName, AnimationBlender::BlendWhileAnimating, static_cast<f32>(m_config.blending_animation_time));
				m_current_animation = animationName;
			}
			m_lastState = STATE_IDLE_TO_FLY;
		}
		else
		{
			if (stateElapsed() > m_config.blending_animation_time)
			{
				setState(STATE_FLY);
			}
		}
	}
	break;

	case STATE_WALK_TO_FLY:
	{
		if (m_lastState != STATE_WALK_TO_FLY)
		{
			if (m_movement.get() && static_cast<PlayerController*>(m_movement.get())->hasMagnetism())
			{
				edv::Vector3f velocity{ m_movement->getVelocity() };
				const std::string& animationName{ getOrientation() == FACING_RIGHT ? (velocity.x > 0.f ? m_config.fly_forward_animation_name : m_config.fly_backward_animation_name) : (velocity.x < 0.f ? m_config.fly_forward_animation_name : m_config.fly_backward_animation_name) };
				getParent()->getGraphics()->blendAnimations(m_current_animation, animationName, AnimationBlender::BlendWhileAnimating, static_cast<f32>(m_config.blending_animation_time));
				m_current_animation = animationName;
			}
			m_lastState = STATE_WALK_TO_FLY;
		}
		else
		{
			if (stateElapsed() > m_config.blending_animation_time)
			{
				setState(STATE_FLY);
			}
		}
	}
	break;

	case STATE_FLY:
	{
		if (m_lastState != STATE_FLY)
		{
			m_lastState = STATE_FLY;
		}
		else
		{
			if (m_movement.get())
			{
				if (m_movement->onGround())
				{
					setState(STATE_FLY_TO_WALK);
				}
				else
				{
					edv::Vector3f velocity{ m_movement->getVelocity() };
					if (static_cast<PlayerController*>(m_movement.get())->hasMagnetism())
					{
						const std::string& animationName{ getOrientation() == FACING_RIGHT ? (velocity.x > 0.f ? m_config.fly_forward_animation_name : m_config.fly_backward_animation_name) : (velocity.x < 0.f ? m_config.fly_forward_animation_name : m_config.fly_backward_animation_name) };
						if (m_current_animation != animationName)
						{
							getParent()->getGraphics()->blendAnimations(m_current_animation, animationName, AnimationBlender::BlendWhileAnimating, static_cast<f32>(m_config.blending_animation_time));
							m_current_animation = animationName;
						}
					}
					else if (velocity.y < 0.f && m_current_animation != m_config.falling_animation_name)
					{
						const std::string& animationName{ m_config.falling_animation_name };
						getParent()->getGraphics()->blendAnimations(m_current_animation, animationName, AnimationBlender::BlendWhileAnimating, static_cast<f32>(m_config.blending_animation_time));
						m_current_animation = animationName;
					}
				}
			}
		}
	}
	break;

	case STATE_FLY_TO_WALK:
	{	
		if (m_lastState != STATE_FLY_TO_WALK)
		{
			const std::string& animationName{ getOrientation() == FACING_LEFT ? m_config.run_left_animation_name : m_config.run_right_animation_name };
			getParent()->getGraphics()->blendAnimations(m_current_animation, animationName, AnimationBlender::BlendWhileAnimating, static_cast<f32>(m_config.blending_animation_time));
			m_current_animation = animationName;
			m_lastState = STATE_FLY_TO_WALK;
		}
		else
		{
			if (stateElapsed() > m_config.blending_animation_time)
			{
				setState(STATE_WALK);
			}
		}
	}
	break;
	
	case STATE_DEATH:
	{
		if (m_death_particles)
		{
			if (m_death_particles->finished())
			{
				getParent()->removeBehavior(m_death_particles);
				m_life = 0;
			}
		}
		else
		{
			m_life = 0;
		}
	}
	break;
	}
	
	if(m_skeleton_debug.get())
		m_skeleton_debug->update();
}

void Nik::stopImpl()
{
	m_rayforce.stop();
	m_inputBehaviorIds.clear();
}

void Nik::clear()
{
	m_rayforce.clear();
	m_left_arm_bone = nullptr;
	m_right_arm_bone = nullptr;
	m_left_ray_origin_bone = nullptr;
	m_right_ray_origin_bone = nullptr;

	if (m_death_effect)
	{
		delete m_death_effect;
	}
	m_death_effect = nullptr;
}

ActorMovement* Nik::createMovement()
{
	ActorMovement* movement = new PlayerController{ m_movement_config };
	return movement;
}

void Nik::OnMouseDown(MouseListener::MouseButton a_button)
{
	u32 control_index = static_cast<u32>(a_button);
	
	// Apply magnetism with left/right mouse button
	if (control_index < 2)
	{
		GameEntity* target_entity = m_magneticRayInfo.targetEntity;
		bool validEntity = (target_entity != nullptr);
		if (validEntity)
		{
			validEntity = validEntity && (CCoreEngine::Instance().GetGameManager().hasEntity(target_entity) && target_entity->getPhysics()->getMagneticProperty());
		}
		if (validEntity)
		{
			// Check attraction
			if (target_entity->getPhysics()->getMagneticProperty()->hasAttraction(m_magnetic_configuraion[control_index]))
			{
				edv::Vector3f direction = m_rayforce.getDirection();
				direction *= target_entity->getPhysics()->getMagneticProperty()->getFactor();
				startMagnetism(m_magnetic_configuraion[control_index], direction);
			}
			else if (target_entity->getPhysics()->getMagneticProperty()->hasRepulsion(m_magnetic_configuraion[control_index]))
			{
				edv::Vector3f direction = -m_rayforce.getDirection();
				direction *= target_entity->getPhysics()->getMagneticProperty()->getFactor();
				startMagnetism(m_magnetic_configuraion[control_index], direction);
			}
			else
			{
				finishMagnetism();
			}

			// Notify hit
			MagnetiaActor* magnetia_actor = static_cast<MagnetiaActor*>(target_entity->getActor());
			if (magnetia_actor)
			{
				// not uses force @TODO eliminar
				m_magneticRayInfo.force = 0.f;
				m_magneticRayInfo.charge = m_magnetic_configuraion[control_index];
				magnetia_actor->onMagneticRayHit(m_magneticRayInfo);
			}
		}
		else
		{
			finishMagnetism();
		}

		// change ray state
		ForceRay::eStates new_ray_state = ForceRay::STATE_NEUTRAL;
		if (m_magnetic_configuraion[control_index] == MagneticProperty::MAGNET_TYPE_POSITIVE)
		{
			new_ray_state = ForceRay::STATE_POSITIVE;
		}
		else
		{
			new_ray_state = ForceRay::STATE_NEGATIVE;
		}
		m_rayforce.setState(new_ray_state);
	}
}

void Nik::OnMouseUp(MouseListener::MouseButton a_button)
{
	u32 control_index = static_cast<u32>(a_button);
	if (control_index < 2)
	{
		finishMagnetism();
		// change ray state
		m_rayforce.setState(ForceRay::STATE_NEUTRAL);
	}
}

void Nik::OnMouseHover(bool a_hover)
{

}

void Nik::startMagnetism(MagneticProperty::eMagneticCharge a_charge, const edv::Vector3f& a_vector)
{
	// movement
	if (!a_vector.isZero() && m_movement.get())
	{
		static_cast<PlayerController*>(m_movement.get())->startMagnetism(a_vector);
	}
}

void Nik::finishMagnetism()
{
	// movement
	if (m_movement.get())
	{
		static_cast<PlayerController*>(m_movement.get())->stopMagnetism();
	}
}

void	Nik::setRayRadius(f32 a_radius)
{
	m_rayforce.setRadius(a_radius);
}

void Nik::stopAllAnimations()
{
	getParent()->getGraphics()->stopAllAnimations();
	m_current_animation = m_config.init_animation_name;
}

void Nik::target()
{
	if (m_left_arm_bone)
		m_left_arm_bone->resetToInitialState();

	if (m_right_arm_bone)
		m_right_arm_bone->resetToInitialState();
	
	Ogre::Bone* armBone = (getOrientation() == FACING_RIGHT)? m_left_arm_bone : m_right_arm_bone;
	if (armBone)
	{
		// compute angle of cannon
		edv::Vector3f inital_vector{ 0.f, -1.f, 0.f };
		f32 angle = m_rayforce.getAngleFromAxis(inital_vector);
		armBone->rotate(Ogre::Quaternion{ Ogre::Radian(angle), Ogre::Vector3{ 1.f, 0.f, 0.f } });
	}
}

void Nik::shoot()
{
	MagneticRayInfo lastRayInfo = m_magneticRayInfo;
	m_magneticRayInfo.clear();
	Ogre::Entity* mainEntity = getParent()->getGraphics()->getMainEntity();
	Ogre::Vector3 mainEntityPosition = getParent()->getWorldPosition().to_OgreVector3();
	const edv::Vector3f graphical_origin = (getOrientation() == FACING_RIGHT) ? EntityOgreGraphics::getBoneWorldPosition(mainEntity, m_left_ray_origin_bone) : EntityOgreGraphics::getBoneWorldPosition(mainEntity, m_right_ray_origin_bone);
	const edv::Vector3f physical_origin{ edv::Vector3f{ graphical_origin.x, graphical_origin.y, mainEntityPosition.z } };
	m_rayforce.setGraphicalOrigin(graphical_origin);
	m_rayforce.setLogicalOrigin(physical_origin);
	const bool hit = m_rayforce.shoot(m_magneticRayInfo);

	if (hit)
	{
		// Notify target
		MagnetiaActor* magnetia_actor = static_cast<MagnetiaActor*>(m_magneticRayInfo.targetEntity->getActor());
		if (magnetia_actor)
			magnetia_actor->onMagneticRayTarget(m_magneticRayInfo);
	}

	if (lastRayInfo.targetEntity &&  lastRayInfo.targetEntity != m_magneticRayInfo.targetEntity)
	{
		if (CCoreEngine::Instance().GetGameManager().hasEntity(lastRayInfo.targetEntity))
		{
			// Notify untarget
			GameEntity* last_target_entity = lastRayInfo.targetEntity;
			MagnetiaActor* magnetia_actor = static_cast<MagnetiaActor*>(last_target_entity->getActor());
			if (magnetia_actor)
				magnetia_actor->onMagneticRayUnTarget();
		}
	}
}

void Nik::applyMove(const edv::Vector3f & a_vector)
{
	MagnetiaActor::applyMove(a_vector);
}

void Nik::kill()
{
	if (!isGodMode())
	{
		if (m_state != STATE_DEATH)
		{
			getParent()->removeAllBehaviors();
			const std::string particleNodeName = "Custom/DeathExplosion";
			m_death_particles = new ParticleSystemAnimator{ "death_particles", 2.5f, particleNodeName };
			getParent()->addBehavior(m_death_particles);
			getParent()->getGraphics()->getNode()->setVisible(false);
			const s32 collisionFlags = getParent()->getPhysics()->getRigidBody()->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE;
			getParent()->getPhysics()->getRigidBody()->setCollisionFlags(collisionFlags);
			getParent()->getPhysics()->getGhost()->setCollisionFlags(collisionFlags);
			f32 force{ m_config.death_animation_force + m_config.death_animation_force*(m_movement.get()? static_cast<PlayerController*>(m_movement.get())->getMovementSpeedFactor() : 1.f) };
			m_death_effect = new ChopEffect{ getParent()->getWorldPosition(), { "Death_Fragment_Antenna", "Death_Fragment_Body", "Death_Fragment_Glasses", "Death_Fragment_LeftArm", "Death_Fragment_LeftLeg", "Death_Fragment_LeftFoot", 
				"Death_Fragment_RightArm", "Death_Fragment_RightLeg", "Death_Fragment_RightFoot" }, force,
				m_config.death_animation_gravity, getParent()->getWorldOrientation()};
			setState(STATE_DEATH);

			// play death sounds
			CSoundManager& soundManager{ CCoreEngine::Instance().GetSoundManager() };

			soundManager.playSound("nik_death_explosion");
			soundManager.playSound("nik_death_pieces");
			MagnetiaActor::stop();
		}
	}
}

void Nik::setGodMode(bool a_active)
{
	Player::setGodMode(a_active);
	static ChainedBehavior* alphaAnimator{ nullptr };
	if (a_active)
	{
		alphaAnimator = new ChainedBehavior(
			{
				new AlphaTransitionAnimator{ 0.5f, 1.f, .75f },
				new AlphaTransitionAnimator{ 0.5f, .75f, 1.f },
			},
			-1
		);
		getParent()->addBehavior(alphaAnimator);
	}
	else
	{
		getParent()->removeBehavior(alphaAnimator);
	}
}

void Nik::setInputEnabled(bool a_enabled)
{
	Player::setInputEnabled(a_enabled);

	if (a_enabled)
	{
		for (auto bh : m_inputBehaviorIds)
		{
			getParent()->getBehavior(bh)->resume();
		}
	}
	else
	{
		for (auto bh : m_inputBehaviorIds)
		{
			getParent()->getBehavior(bh)->pause();
		}
	}
}

void Nik::checkFacingByRay()
{
	const f32 xRayScope = m_rayforce.getScope().x;
	Ogre::Quaternion rotation{ Ogre::Quaternion::IDENTITY };
	if (getOrientation() == FACING_RIGHT && xRayScope < 0)
	{
		setOrientation(FACING_LEFT);
		rotation = Ogre::Quaternion{ Ogre::Degree{ -90.f }, { 0.f, 1.f, 0.f } };
	}
	else if (getOrientation() == FACING_LEFT && xRayScope > 0)
	{
		setOrientation(FACING_RIGHT);
		rotation = Ogre::Quaternion{ Ogre::Degree{ 90.f }, { 0.f, 1.f, 0.f } };
	}
	
	if (rotation != Ogre::Quaternion::IDENTITY)
	{
		// udpate rotation and arm positions
		getParent()->getGraphics()->getNode()->setOrientation(rotation);
		target();
		const std::string& animationSourceName{ !(getOrientation() == FACING_LEFT) ? m_config.run_left_animation_name : m_config.run_right_animation_name };
		const std::string& animationTargetName{ (getOrientation() == FACING_LEFT) ? m_config.run_left_animation_name : m_config.run_right_animation_name };
		
		auto animationSoruce = getParent()->getGraphics()->getAnimation(animationSourceName);
		auto animationTarget = getParent()->getGraphics()->getAnimation(animationTargetName);
		if (animationSoruce && animationTarget)
		{
			if (animationSoruce->getEnabled())
			{
				getParent()->getGraphics()->stopBlendAnimations();
				animationSoruce->setEnabled(false);
				auto time = animationSoruce->getTimePosition();
				animationTarget->setTimePosition(time);
				animationTarget->setEnabled(true);
				m_current_animation = animationTargetName;
			}
		}
	}
}

void Nik::checkFacing()
{
	// Nothing to do... Force Ray has this power! 
}
