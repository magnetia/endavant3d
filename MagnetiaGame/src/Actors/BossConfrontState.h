#ifndef MAGNETIA_GAME_BOSS_CONFRONT_STATE_H_
#define MAGNETIA_GAME_BOSS_CONFRONT_STATE_H_

#include <memory>
#include <vector>
#include <Utils/generic.hpp>
#include "BossState.h"

class TimedBehavior;
class GameEntity;

class BossConfrontState : public BossState
{
public:
	BossConfrontState(Boss* a_boss, const edv::range32f& a_variationY, const edv::range64f& a_duration, const edv::range32& a_shoots);
	~BossConfrontState();

	void start() override;
	void update(f64 dt) override;
	void stop() override;
	bool finished() override;
	edv::Vector3f getStartPoint() const override;

private:
	typedef std::vector<GameEntity*> Missiles;

	void getLimits();
	void shoot();

	u32				m_remainingShoots;
	TimedBehavior*	m_currentAnimation;
	Missiles		m_missiles;
	edv::range32f	m_variationY;
	edv::range64f	m_duration;
	edv::range32	m_shoots;
	edv::Vector3f	m_topLimit;
	edv::Vector3f	m_bottomLimit;
	bool			m_lastLimitedUp;
	bool			m_lastLimitedDown;
};

#endif
