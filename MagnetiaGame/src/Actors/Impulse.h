#ifndef MAGNETIA_GAME_IMPULSE_H_
#define MAGNETIA_GAME_IMPULSE_H_

#include <Sound/CSoundManager.h>
#include "MagnetiaActor.h"

class Impulse : public MagnetiaActor
{
public:
	class Factory : public Actor::factory_type
	{
	public:
		Actor* createElement(const Actor::factory_param_type& a_config) override;
	};

	Impulse(const ScriptConfig& a_scriptConfig = {}, const edv::Vector3f& a_forceVector = {}, const std::string& a_soundId = "");
	~Impulse();
	Actor* clone() override;

	const edv::Vector3f& getImpulseVector() const;
	void ejectPlayer();

	void soundFinished(CSoundManager::t_ChannelId a_channel);

protected:
	void initImpl() override;

private:
	edv::Vector3f				m_impulseVector;
	CSoundManager::t_ChannelId	m_channelId;
	std::string					m_soundId;
};

#endif
