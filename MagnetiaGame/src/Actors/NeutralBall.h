#ifndef MAGNETIA_GAME_NEUTRAL_BALL_H_
#define MAGNETIA_GAME_NEUTRAL_BALL_H_

#include "MagnetiaActor.h"

class NeutralBall : public MagnetiaActor
{
public:
	// Factory
	class Factory : public Actor::factory_type
	{
	public:
		Actor* createElement(const Actor::factory_param_type& a_config) override;
	};

	NeutralBall(s32 a_maxLife = 0, const ScriptConfig& a_scriptConfig = {}, f64 a_animationDuration = 0., const edv::Vector3f& a_animationScale = {});
	~NeutralBall();
	Actor* clone() override;

	f64 getAnimationDuration() const;
	bool isPlayingAnimation() const;
	void playGetAnimation();

private:
	void initImpl() override;

	f64				m_animationDuration;
	edv::Vector3f	m_animationScale;
	bool			m_playingAnimation;
};

#endif
