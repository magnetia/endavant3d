#include <Game/ParticleSystemAnimator.h>
#include <Game/GameEntity.h>
#include <Utils/IdGenerator.h>
#include "Mine.h"

namespace {	IdGenerator<u64> idGenerator; } // campanya

Actor* Mine::Factory::createElement(const Actor::factory_param_type& a_config)
{
	return new Mine{ a_config["max_life"].as<s32>(), { a_config["script/path"], a_config["script/scope"] } };
}

Mine::Mine(s32 a_maxLife, const ScriptConfig& a_scriptConfig) :
	MagnetiaActor{ a_maxLife, a_scriptConfig },
	m_boss{ nullptr },
	m_animation{ nullptr },
	m_finished{ false }
{

}

Mine::~Mine()
{

}

Actor* Mine::clone()
{
	return new Mine{ getMaxLife(), getScriptConfig() };
}

void Mine::explode()
{
	if (!m_animation)
	{
		m_animation = new ParticleSystemAnimator{ "death_particles_mine" + std::to_string(idGenerator.nextId()), 2.5f, "Custom/DeathExplosion" };
		getParent()->addBehavior(m_animation);
		getParent()->getPhysics()->clear();
	}
}

GameEntity* Mine::getBossEntity() const
{
	return m_boss;
}

bool Mine::finished() const
{
	return m_finished;
}

void Mine::setBossEntity(GameEntity* a_boss)
{
	m_boss = a_boss;
}

void Mine::updateImpl(f64 dt)
{
	if (!m_finished && m_animation && m_animation->finished())
	{
		m_finished = true;
	}
}
