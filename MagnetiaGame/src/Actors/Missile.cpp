#include <Core/CCoreEngine.h>
#include <Game/FlyStraightAnimator.h>
#include <Game/ParticleSystemAnimator.h>
#include <Game/GameEntity.h>
#include <Game/GameManager.h>
#include <Utils/IdGenerator.h>
#include "Missile.h"

namespace { IdGenerator<u64> idGenerator; } // campanya

Actor* Missile::Factory::createElement(const Actor::factory_param_type& a_config)
{
	return new Missile{ 
		a_config["max_life"].as<s32>(),
		ScriptConfig{ a_config["script/path"], a_config["script/scope"] },
		edv::range64f{ a_config["duration/min"], a_config["duration/max"] },
		edv::Vector3f{ a_config["direction/x"], a_config["direction/y"], a_config["direction/z"] }
	};
}

Missile::Missile(s32 a_maxLife, const ScriptConfig& a_scriptConfig, const edv::range64f& a_duration, const edv::Vector3f& a_direction) :
	MagnetiaActor{ a_maxLife, a_scriptConfig },
	m_duration{ a_duration },
	m_direction{ a_direction },
	m_currentAnimation{ nullptr },
	m_owner{ nullptr },
	m_target{ nullptr },
	m_colliding{ false },
	m_finished{ false }
{

}

Missile::~Missile()
{

}

Actor* Missile::clone()
{
	return new Missile{ getMaxLife(), getScriptConfig(), m_duration, m_direction };
}

void Missile::setDuration(const edv::range64f& a_duration)
{
	m_duration = a_duration;
}

void Missile::setDirectionVect(const edv::Vector3f& a_direction)
{
	m_direction = a_direction;

	if (m_currentAnimation)
	{
		getParent()->removeBehavior(m_currentAnimation);
	}

	const edv::Vector3f position{ getParent()->getWorldPosition() };

	m_currentAnimation = new FlyStraightAnimator{
		m_duration.random(),
		position,
		position + m_direction
	};

	getParent()->addBehavior(m_currentAnimation);
}

const edv::range64f& Missile::getDuration() const
{
	return m_duration;
}

const edv::Vector3f& Missile::getDirectionVect() const
{
	return m_direction;
}

bool Missile::finished() const
{
	return m_finished;
}

void Missile::setOwner(GameEntity* a_owner)
{
	m_owner = a_owner;
}

void Missile::setTarget(GameEntity* a_target)
{
	m_target = a_target;
}

void Missile::onCollision(const CollisionInfo& a_collisionInfo)
{
	if (!m_finished)
	{ 
		if (m_colliding)
		{
			if (m_currentAnimation && m_currentAnimation->finished())
			{
				m_finished = true;
			}
		}
		else
		{
			if (m_target == a_collisionInfo.target)
			{
				m_target->getActor()->kill();
			}

			if (m_owner != a_collisionInfo.target)
			{
				if (m_currentAnimation)
				{
					getParent()->removeBehavior(m_currentAnimation);
				}

				m_currentAnimation = new ParticleSystemAnimator{ "death_particles_missile" + std::to_string(idGenerator.nextId()), 2.5f, "Custom/DeathExplosion" };
				getParent()->addBehavior(m_currentAnimation);
				getParent()->getGraphics()->getNode()->setVisible(false);
				getParent()->getPhysics()->clear();

				m_colliding = true;
			}
		}
	}
}

void Missile::initImpl()
{
	MagnetiaActor::initImpl();

	const edv::Vector3f position{ getParent()->getWorldPosition() };

	m_currentAnimation = new FlyStraightAnimator{
		m_duration.random(),
		position,
		position + m_direction
	};

	getParent()->addBehavior(m_currentAnimation);
}

void Missile::updateImpl(f64 dt)
{
	MagnetiaActor::updateImpl(dt);

	if (m_currentAnimation && m_currentAnimation->finished())
	{
		getParent()->getGraphics()->getNode()->setVisible(false);
		getParent()->removeBehavior(m_currentAnimation);
		m_currentAnimation = nullptr;
	}
}
