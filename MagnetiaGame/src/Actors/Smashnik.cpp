#include <Core/CCoreEngine.h>
#include <Physics/CPhysicsManager.h>
#include <Physics/btResultCallback.hpp>
#include <Renderer/CRenderManager.h>
#include <Game/GameEntity.h>
#include <Game/FlyStraightAnimator.h>
#include <Game/RotationAnimator.h>
#include <Game/GameManager.h>
#include "Smashnik.h"

// #todo: rotaciones
#include <Core/CLogManager.h>

Actor* Smashnik::Factory::createElement(const Actor::factory_param_type& a_config)
{
	return new Smashnik{ 
		a_config["max_life"], 
		ScriptConfig{ a_config["script/path"], a_config["script/scope"] }, 
		a_config["down_margin"],
		a_config["gravity"]
	};
}

Smashnik::Smashnik(s32 a_maxLife, const ScriptConfig& a_scriptConfig, f32 a_downMargin, f32 a_gravity) :
	MagnetiaActor{ a_maxLife, a_scriptConfig },
	m_compoundPosition{ },
	m_compoundHalfSize{ },
	m_smashnikHalfSize{ },
	m_started{ false },
	m_downMargin{ a_downMargin },
	m_gravity{ a_gravity },
	m_currentMovement{ },
	m_linearVelocity{ },
	m_onGround{ false },
	m_collidingLeft{ false },
	m_collidingRight{ false },
	m_leftParticles{ nullptr },
	m_rightParticles{ nullptr },
	m_smashOrientation{ SMASH_FRONT },
	m_rotationTimerId{ CTimeManager::INVALID_TIMERID },
	m_currentRotator{ nullptr },
	m_channelId{ CSoundManager::UNASSIGNED_CHANNEL_ID }
{

}

Smashnik::~Smashnik()
{

}

Actor* Smashnik::clone()
{
	return new Smashnik{ getMaxLife(), getScriptConfig(), m_downMargin };
}

void Smashnik::initImpl()
{
	MagnetiaActor::initImpl();

	GameEntity* const entity = getParent();
	const std::string entityName = entity->getName();

	m_smashnikHalfSize = entity->getScaledHalfSize();
	m_leftParticles = entity->getGraphics()->getParticleSystemNode(entityName + "propulsion1");
	m_rightParticles = entity->getGraphics()->getParticleSystemNode(entityName + "propulsion2");

	if (!m_leftParticles || !m_rightParticles)
	{
		throw std::runtime_error{ "Particles missing for smashnik " + entityName };
	}

	showParticles(false);

	updateSizeAndPosition();
	updateCollisions();
}

void Smashnik::updateImpl(f64 dt)
{
	MagnetiaActor::updateImpl(dt);

	if (!m_currentMovement.isZero())
	{
		GameEntity* const entity = getParent();
		EntityBtPhysics* const physics = entity->getPhysics();

		if (f32 mass = physics->getMass())
		{
			m_linearVelocity += (m_currentMovement)* (1.f / mass) * static_cast<f32>(dt);
			m_linearVelocity *= physics->getRigidBody()->getLinearFactor();
		}

		if (!m_linearVelocity.isZero())
		{
			btTransform from_transform, to_transform;
			from_transform.setIdentity(); to_transform.setIdentity();
			from_transform.setOrigin(m_compoundPosition.to_btVector3());

			const f32 delta_time(static_cast<f32>(dt));
			const btVector3 ang_velocity(0.f, 0.f, 0.f);
			btTransformUtil::integrateTransform(from_transform, m_linearVelocity.to_btVector3(), ang_velocity, delta_time, to_transform);
			btVector3 targetPosition = to_transform.getOrigin();

			if (((targetPosition.x() < m_compoundPosition.x) && m_collidingLeft) || ((targetPosition.x() > m_compoundPosition.x) && m_collidingRight))
			{
				targetPosition.setX(m_compoundPosition.x);
				m_linearVelocity.x = 0;
			}

			if (m_onGround && targetPosition.y() < m_compoundPosition.y)
			{
				targetPosition.setY(m_compoundPosition.y);
				m_linearVelocity.y = 0;
			}

			edv::Vector3f new_position = targetPosition * physics->getRigidBody()->getLinearFactor();
			m_compoundPosition = new_position;
			new_position.y = new_position.y + m_compoundHalfSize.y - m_smashnikHalfSize.y;
			entity->setWorldPosition(new_position);
		}

		m_currentMovement = {};
	}

	if (m_linearVelocity.length() < 0.01f)
	{
		m_linearVelocity.setZero();
	}

	updateCollisions();
}

void Smashnik::stopImpl()
{
	if (m_rotationTimerId != CTimeManager::INVALID_TIMERID)
	{
		CCoreEngine::Instance().GetTimerManager().KillTimer(m_rotationTimerId);
		m_rotationTimerId = CTimeManager::INVALID_TIMERID;
	}

	if (m_channelId != CSoundManager::UNASSIGNED_CHANNEL_ID)
	{
		CCoreEngine::Instance().GetSoundManager().stopSound(m_channelId);
		m_channelId = CSoundManager::UNASSIGNED_CHANNEL_ID;
	}
}

void Smashnik::startUp(f64 a_animTime)
{
	const f32 offsetY = m_downMargin / 2;
	const edv::Vector3f currentPosition = getParent()->getWorldPosition();
	const edv::Vector3f targetPosition{ currentPosition.x, currentPosition.y + offsetY, currentPosition.z };

	getParent()->addBehavior(new FlyStraightAnimator{ a_animTime, currentPosition.to_OgreVector3(), targetPosition.to_OgreVector3() });

	m_linearVelocity.setZero();

	m_started = true;
}

void Smashnik::shutdown()
{
	m_linearVelocity.setZero();
	m_started = false;
}

void Smashnik::move(f32 a_xOffset, f32 a_yOffset)
{
	m_currentMovement += edv::Vector3f{ a_xOffset, a_yOffset, 0 };
}

bool Smashnik::onGround() const
{
	return m_onGround;
}

bool Smashnik::collidingLeft() const
{
	return m_collidingLeft;
}

bool Smashnik::collidingRight() const
{
	return m_collidingRight;
}

f32 Smashnik::getGravity() const
{
	return m_gravity;
}

void Smashnik::finishAnimation()
{
	getParent()->removeAllBehaviors();
	updateSizeAndPosition();
}

void Smashnik::showParticles(bool a_show)
{
	if (a_show)
	{
		if (m_channelId == CSoundManager::UNASSIGNED_CHANNEL_ID)
		{
			m_channelId = CCoreEngine::Instance().GetSoundManager().playSound("smashnik_propulsion");
		}
	}
	else
	{
		if (m_channelId != CSoundManager::UNASSIGNED_CHANNEL_ID)
		{
			CCoreEngine::Instance().GetSoundManager().stopSound(m_channelId);
			m_channelId = CSoundManager::UNASSIGNED_CHANNEL_ID;
		}
	}

	m_leftParticles->setVisible(a_show);
	m_rightParticles->setVisible(a_show);
}

void Smashnik::checkOrientation(bool a_reset)
{
	// campanya!!!!

	// #todo: configurable
	static const f64 animTime = 0.5;

	if (!m_currentRotator)
	{
		const edv::Vector3f playerPos = CCoreEngine::Instance().GetGameManager().getPlayer()->getWorldPosition();
		SmashOrientation nextOrientation = m_smashOrientation;

		if (a_reset)
		{
			nextOrientation = SMASH_FRONT;
		}
		else
		{
			if (playerPos.x < m_compoundPosition.x)
			{
				nextOrientation = SMASH_LEFT;
			}
			else if (playerPos.x > m_compoundPosition.x)
			{
				nextOrientation = SMASH_RIGHT;
			}
		}

		if (m_smashOrientation != nextOrientation)
		{
			Ogre::Degree rotate;

			switch (m_smashOrientation)
			{
			case SMASH_LEFT:
				switch (nextOrientation)
				{
				case SMASH_FRONT:
					rotate = 90;
					break;
				case SMASH_RIGHT:
					rotate = 180;
					break;
				}
				break;

			case SMASH_RIGHT:
				switch (nextOrientation)
				{
				case SMASH_FRONT:
					rotate = -90;
					break;
				case SMASH_LEFT:
					rotate = -180;
					break;
				}
				break;

			case SMASH_FRONT:
				switch (nextOrientation)
				{
				case SMASH_LEFT:
					rotate = -90;
					break;
				case SMASH_RIGHT:
					rotate = 90;
					break;
				}
				break;
			}

			// rotate
			m_smashOrientation = nextOrientation;
			m_currentRotator = new RotationAnimator{ animTime, rotate, Ogre::Vector3{ 0.f, 1.f, 0.f } };
			m_rotationTimerId = CCoreEngine::Instance().GetTimerManager().CreateTimer(animTime, false, this);

			getParent()->addBehavior(m_currentRotator);
		}
	}
}

void Smashnik::CallBack(EV_TimerID a_ID)
{
	if (m_currentRotator)
	{
		getParent()->removeBehavior(m_currentRotator);
		m_currentRotator = nullptr;
		m_rotationTimerId = CTimeManager::INVALID_TIMERID;
	}
}

void Smashnik::updateCollisions()
{
	m_onGround = checkOnGround();
	m_collidingLeft = checkLeftCollision();
	m_collidingRight = checkRightCollision();
}

void Smashnik::updateSizeAndPosition()
{
	const edv::Vector3f smashnikWorldPos = getParent()->getWorldPosition();

	m_compoundHalfSize = edv::Vector3f{ m_smashnikHalfSize.x, m_smashnikHalfSize.y + (m_started ? (m_downMargin / 2) : 0), m_smashnikHalfSize.z };
	m_compoundPosition = edv::Vector3f{ smashnikWorldPos.x, (smashnikWorldPos.y + m_smashnikHalfSize.y) - m_compoundHalfSize.y, smashnikWorldPos.z };
}

bool Smashnik::checkOnGround()
{
	const edv::Vector3f leftPosition{ m_compoundPosition.x - m_compoundHalfSize.x, m_compoundPosition.y, m_compoundPosition.z };
	const edv::Vector3f rightPosition{ m_compoundPosition.x + m_compoundHalfSize.x, m_compoundPosition.y, m_compoundPosition.z };
	const edv::Vector3f destLeft{ leftPosition.x, m_compoundPosition.y - m_compoundHalfSize.y, m_compoundPosition.z };
	const edv::Vector3f destRight{ rightPosition.x, m_compoundPosition.y - m_compoundHalfSize.y, m_compoundPosition.z };

	btClosestNotMeRayResultCallback callbackLeft{
		getParent(),
		leftPosition.to_btVector3(),
		destLeft.to_btVector3(),
		EntityBtPhysics::EdvCollisionFlags::CF_TRIGGER | btCollisionObject::CF_NO_CONTACT_RESPONSE
	};

	btClosestNotMeRayResultCallback callbackRight{
		getParent(),
		rightPosition.to_btVector3(),
		destRight.to_btVector3(),
		EntityBtPhysics::EdvCollisionFlags::CF_TRIGGER | btCollisionObject::CF_NO_CONTACT_RESPONSE
	};

	CCoreEngine::Instance().GetPhysicsManager().RayTestNotMe(leftPosition, destLeft, callbackLeft);
	CCoreEngine::Instance().GetPhysicsManager().RayTestNotMe(rightPosition, destRight, callbackRight);

	return callbackLeft.hasHit() || callbackRight.hasHit();
}

bool Smashnik::checkLeftCollision()
{
	// #todo: fer funcionar amb compound.y i compound_half_size.y
	const edv::Vector3f smashPos = getParent()->getWorldPosition();

	const edv::Vector3f upPosition{ m_compoundPosition.x, smashPos.y + m_smashnikHalfSize.y, m_compoundPosition.z };
	const edv::Vector3f downPosition{ m_compoundPosition.x, smashPos.y - m_smashnikHalfSize.y, m_compoundPosition.z };
	const edv::Vector3f destUp{ m_compoundPosition.x - m_compoundHalfSize.x, upPosition.y, m_compoundPosition.z };
	const edv::Vector3f destDown{ m_compoundPosition.x - m_compoundHalfSize.x, downPosition.y, m_compoundPosition.z };

	btClosestNotMeRayResultCallback callbackUp{
		getParent(),
		upPosition.to_btVector3(),
		destUp.to_btVector3(),
		EntityBtPhysics::EdvCollisionFlags::CF_TRIGGER | btCollisionObject::CF_NO_CONTACT_RESPONSE
	};

	btClosestNotMeRayResultCallback callbackDown{
		getParent(),
		downPosition.to_btVector3(),
		destDown.to_btVector3(),
		EntityBtPhysics::EdvCollisionFlags::CF_TRIGGER | btCollisionObject::CF_NO_CONTACT_RESPONSE
	};

	CCoreEngine::Instance().GetPhysicsManager().RayTestNotMe(upPosition, destUp, callbackUp);
	CCoreEngine::Instance().GetPhysicsManager().RayTestNotMe(downPosition, destDown, callbackDown);

	return callbackUp.hasHit() || callbackDown.hasHit();
}

bool Smashnik::checkRightCollision()
{
	// #todo: fer funcionar amb compound.y i compound_half_size.y
	const edv::Vector3f smashPos = getParent()->getWorldPosition();

	const edv::Vector3f upPosition{ m_compoundPosition.x, smashPos.y + m_smashnikHalfSize.y, m_compoundPosition.z };
	const edv::Vector3f downPosition{ m_compoundPosition.x, smashPos.y - m_smashnikHalfSize.y, m_compoundPosition.z };
	const edv::Vector3f destUp{ m_compoundPosition.x + m_compoundHalfSize.x, upPosition.y, m_compoundPosition.z };
	const edv::Vector3f destDown{ m_compoundPosition.x + m_compoundHalfSize.x, downPosition.y, m_compoundPosition.z };

	btClosestNotMeRayResultCallback callbackUp{
		getParent(),
		upPosition.to_btVector3(),
		destUp.to_btVector3(),
		EntityBtPhysics::EdvCollisionFlags::CF_TRIGGER | btCollisionObject::CF_NO_CONTACT_RESPONSE
	};

	btClosestNotMeRayResultCallback callbackDown{
		getParent(),
		downPosition.to_btVector3(),
		destDown.to_btVector3(),
		EntityBtPhysics::EdvCollisionFlags::CF_TRIGGER | btCollisionObject::CF_NO_CONTACT_RESPONSE
	};

	CCoreEngine::Instance().GetPhysicsManager().RayTestNotMe(upPosition, destUp, callbackUp);
	CCoreEngine::Instance().GetPhysicsManager().RayTestNotMe(downPosition, destDown, callbackDown);

	return callbackUp.hasHit() || callbackDown.hasHit();
}

