#include <Core/CCoreEngine.h>
#include <Renderer/CRenderManager.h>
#include <Time/CTimeManager.h>
#include <Game/GameEntity.h>
#include "BeamEffect.h"

BeamEffect::BeamEffect(const std::string& a_id, const edv::Vector3f& a_direction, f64 a_duration, const Ogre::ColourValue& a_color, const edv::Vector3f& a_position, const Ogre::Quaternion& a_orientation) :
	m_node{ nullptr },
	m_animation{ nullptr },
	m_id{ a_id },
	m_state{ nullptr },
	m_bbs{ nullptr },
	m_flare{ nullptr },
	m_light{ nullptr },
	m_color{ a_color },
	m_trail{ nullptr },
	m_position{ a_position },
	m_orientation{ a_orientation },
	m_direction{ a_direction },
	m_duration{ a_duration },
	m_startTimestamp{ 0 }
{
	// add some variation
	if (!(rand() % 25))
	{
		//std::vector<Ogre::ColourValue> colors({ 
		//	Ogre::ColourValue{ 1.f, 1.f, 1.f, 1.f }, 
		//	m_color == Ogre::ColourValue::Red ? Ogre::ColourValue{ 1.f, 0.96f, 0.93f, 1.f } : Ogre::ColourValue{ 0.f, 1.f, 1.f, 1.f }
		//	 });

		//m_color = colors[rand() % colors.size()];

		m_color = Ogre::ColourValue{ 1.f, 1.f, 1.f, 1.f };
	}
	else
	{
		const Ogre::ColourValue altColor = 
			m_color == Ogre::ColourValue::Red ? Ogre::ColourValue{ 1.f, 0.5f, 0.f, 1.f } : Ogre::ColourValue{ 0.17f, 0.46f, 1.f, 1.f };

		if (!(rand() % 20))
		{
			m_color = altColor;
		}
	}

		// save timestamp
	m_startTimestamp = CCoreEngine::Instance().GetTimerManager().GetTotalTimeSeconds();

	// create path
	Ogre::Vector3 offset1{ -m_direction.y, m_direction.x, m_direction.z }, offset2{ m_direction.y, -m_direction.x, m_direction.z };
	offset1 *= .07f;
	offset2 *= .07f;

	std::vector<Ogre::Vector3> axis{ Ogre::Vector3::UNIT_X, Ogre::Vector3::UNIT_Y, Ogre::Vector3::UNIT_Z };
	Ogre::Degree rot{ static_cast<f32>(rand() % 361) };
	offset1 = Ogre::Quaternion{ rot, axis[rand() % 3] } *offset1;
	offset2 = Ogre::Quaternion{ rot, axis[rand() % 3] } *offset2;

	std::vector<edv::Vector3f> points{ 5, m_position };
	points[1] += (m_direction * .25f) + offset1;
	points[2] += m_direction * .5f;
	points[3] += m_direction * .75f + offset2;
	points[4] += m_direction;

	// create node
	Ogre::SceneManager* sceneMgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();
	m_node = sceneMgr->getRootSceneNode()->createChildSceneNode();
	m_node->_setDerivedPosition(m_position.to_OgreVector3());
	m_node->_setDerivedOrientation(m_orientation);

	// create track animation
	m_animation = sceneMgr->createAnimation(m_id, static_cast<Ogre::Real>(m_duration));
	m_animation->setInterpolationMode(Ogre::Animation::IM_SPLINE);

	Ogre::NodeAnimationTrack* track = nullptr;
	track = m_animation->createNodeTrack(1, m_node);

	const Ogre::Real duration = static_cast<Ogre::Real>(m_duration);
	track->createNodeKeyFrame(0)->setTranslate(points[0].to_OgreVector3());
	track->createNodeKeyFrame(duration * .25f)->setTranslate(points[1].to_OgreVector3());
	track->createNodeKeyFrame(duration * .5f)->setTranslate(points[2].to_OgreVector3());
	track->createNodeKeyFrame(duration * .75f)->setTranslate(points[3].to_OgreVector3());
	track->createNodeKeyFrame(duration)->setTranslate(points[4].to_OgreVector3());

	// create animation state
	m_state = sceneMgr->createAnimationState(m_id);
	m_state->setEnabled(true);

	// create trail
	m_trail = sceneMgr->createRibbonTrail();
	sceneMgr->getRootSceneNode()->attachObject(m_trail);
	m_trail->setMaterialName("Examples/LightRibbonTrail");
	m_trail->setTrailLength(100);
	m_trail->setInitialColour(0, m_color);
	m_trail->setInitialWidth(0, 1.2f);
	m_trail->setColourChange(0, 0.5, 0.5, 0.5, 0.5);
	m_trail->addNode(m_node);

	// create light
	//m_light = sceneMgr->createLight();
	//m_light->setDiffuseColour(m_color);
	//m_node->attachObject(m_light);

	// create flare
	//m_bbs = sceneMgr->createBillboardSet(1);
	//m_bbs->setDefaultDimensions(2, 2);
	//m_flare = m_bbs->createBillboard(Ogre::Vector3::ZERO, m_color);
	//m_bbs->setMaterialName("Examples/Flare");
	//m_node->attachObject(m_bbs);
}

BeamEffect::~BeamEffect()
{
	Ogre::SceneManager* sceneMgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();

	//m_bbs->removeBillboard(m_flare);
	//sceneMgr->destroyBillboardSet(m_bbs);
	//sceneMgr->destroyLight(m_light);
	sceneMgr->destroyAnimationState(m_state->getAnimationName());
	m_animation->destroyNodeTrack(1);
	sceneMgr->destroyAnimation(m_animation->getName());
	m_trail->removeNode(m_node);
	sceneMgr->destroyRibbonTrail(m_trail);
	sceneMgr->getRootSceneNode()->removeChild(m_node);
}

void BeamEffect::update(f64 dt)
{
	// #todo: sense logica de pausat l'animacio esta malament
	const f64 now = CCoreEngine::Instance().GetTimerManager().GetTotalTimeSeconds();

	if (now - m_startTimestamp < m_duration)
	{
		m_state->addTime(static_cast<Ogre::Real>(dt));
	}
}

std::string BeamEffect::getId() const
{
	return m_id;
}
