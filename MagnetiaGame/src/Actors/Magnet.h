#ifndef MAGNETIA_MAGNET_H_
#define MAGNETIA_MAGNET_H_

#include "MagnetiaActor.h"

class Magnet : public MagnetiaActor
{
public:
	class Factory : public Actor::factory_type
	{
	public:
		Actor* createElement(const Actor::factory_param_type& a_config) override;
	};

	Magnet(const f64 a_animDuration = 0., const std::string& a_animGlowTexture = "", const edv::Vector3f& a_animInitialLevel = {},
		const edv::Vector3f& a_animFinalLevel = {});
	~Magnet();
	Actor* clone() override;

private:
	void initImpl() override;
	void updateImpl(f64 dt) override;

	f64				m_animDuration;
	std::string		m_animGlowTexture;
	edv::Vector3f	m_animInitialLevel;
	edv::Vector3f	m_animFinalLevel;
};

#endif
