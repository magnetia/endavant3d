#ifndef MAGNETIA_GAME_BOSS_H_
#define MAGNETIA_GAME_BOSS_H_

#include <memory>
#include <string>
#include <vector>
#include "MagnetiaActor.h"
#include "BossState.h"

class Boss : public MagnetiaActor
{
public:
	typedef std::vector<std::string> PathsParam;

	// Factory
	class Factory : public Actor::factory_type
	{
	public:
		Actor* createElement(const Actor::factory_param_type& a_config) override;
	};

	Boss(
		s32 a_maxLife = 0, 
		const ScriptConfig& a_scriptConfig = {}, 
		const PathsParam& a_pathsName = {}, 
		const edv::range64f& a_acceleration = {},
		const edv::range64f& a_mineDropTime = {},
		f64 a_accelRadius = 25.f,
		const edv::range32f& a_confrontVariationY = {},
		const edv::range64f& a_confrontMoveDuration = {},
		const edv::range32& a_confrontShoots = {},
		f32 a_waitDistance = 0.f);
	~Boss();
	Actor* clone() override;
	void kill() override;

	edv::Vector3f getNextStartPoint() const;

private:
	typedef std::unique_ptr<BossState>	StatePtr;
	typedef std::vector<StatePtr>		States;

	void initImpl() override;
	void updateImpl(f64 dt) override;
	void stopImpl() override;
	
	PathsParam				m_pathsName;
	edv::range64f			m_acceleration;
	edv::range64f			m_mineDropTime;
	f64						m_accelRadius;
	edv::range32f			m_confrontVariationY;
	edv::range64f			m_confrontMoveDuration;
	edv::range32			m_confrontShoots;
	f32						m_waitDistance;
	States					m_states;
	States::const_iterator	m_currentState;
};

#endif
