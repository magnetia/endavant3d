#include <Core/CCoreEngine.h>
#include <Game/GameManager.h>
#include <Game/GameEntity.h>
#include "Missile.h"
#include "Spaceship.h"
#include "SpaceshipController.h"
#include <Game/AlphaTransitionAnimator.h>
#include <Game/ChainedBehavior.h>

Actor* Spaceship::Factory::createElement(const Actor::factory_param_type& a_config)
{
	return new Spaceship {
		a_config["max_life"],
		{ a_config["script/path"], a_config["script/scope"] },
		{
			{ a_config["movement/gravity/x"], a_config["movement/gravity/y"], a_config["movement/gravity/z"] }, a_config["movement/restitution"], a_config["movement/sliding"], a_config["movement/friction"],
			{ a_config["movement/X_acceleration"] },
			{ a_config["movement/Y_acceleration"] },
			{ a_config["movement/max_velocity"] }
		}
	};
}

Spaceship::Spaceship(s32 a_maxLife, const ScriptConfig& a_scriptConfig, const SpaceshipController::sControllerConfig& a_controller_config) :
	Player{ a_maxLife, a_scriptConfig },
	m_movement_config{ a_controller_config },
	m_shooting{ false }
{

}

Spaceship::~Spaceship()
{

}

Actor* Spaceship::clone()
{
	return new Spaceship{ getMaxLife(), getScriptConfig() };
}

void Spaceship::applyMove(const edv::Vector3f& a_vector)
{
	MagnetiaActor::applyMove(a_vector);
}

void Spaceship::kill()
{
	if (!isGodMode())
	{
		MagnetiaActor::kill();
	}
}

void Spaceship::onCollision(const CollisionInfo& a_collisionInfo)
{
	if (!a_collisionInfo.target->getActor())
	{
		kill();
	}
}

void Spaceship::initImpl()
{
	MagnetiaActor::initImpl();

	// Input behaviors
	const Behavior::Id mouseBehavior = getParent()->addBehavior(new MouseListener(std::bind(&Spaceship::onMouseDown, this, std::placeholders::_1), std::bind(&Spaceship::onMouseUp, this, std::placeholders::_1), std::bind(&Spaceship::onMouseHover, this, std::placeholders::_1)));
	const Behavior::Id controllerBehavior = 2LL; // Controller behavior (XML)
	m_inputBehaviorIds.push_back(mouseBehavior);
	m_inputBehaviorIds.push_back(controllerBehavior);
}

void Spaceship::onMouseDown(MouseListener::MouseButton a_button)
{
	if (!m_shooting)
	{
		// shoot
		GameManager::EntityInfo info{ CCoreEngine::Instance().GetGameManager().createEntity("NikMissile", getParent()->getWorldPosition().to_OgreVector3()) };
		GameEntity* const missileEntity{ info.second };
		Missile* const missileActor{ static_cast<Missile*>(info.second->getActor()) };

		missileActor->setOwner(getParent());
		missileActor->setTarget(CCoreEngine::Instance().GetGameManager().getBoss());
		// #todo:
		missileActor->setDirectionVect({ 1000.f, 0.f, 0.f });
		//missileActor->setDirectionVect({ getOrientation() == FACING_LEFT ? -1000.f : 1000.f, 0.f, 0.f });

		m_missiles.push_back(missileEntity);
		m_shooting = true;
	}
}

void Spaceship::onMouseUp(MouseListener::MouseButton a_button)
{
	if (m_shooting)
	{
		m_shooting = false;
	}
}

void Spaceship::onMouseHover(bool a_hover)
{

}

void Spaceship::setGodMode(bool a_active)
{
	Player::setGodMode(a_active);
	static ChainedBehavior* alphaAnimator{ nullptr };
	if (a_active)
	{
		alphaAnimator = new ChainedBehavior(
		{
			new AlphaTransitionAnimator{ 0.5f, 1.f, .75f },
			new AlphaTransitionAnimator{ 0.5f, .75f, 1.f },
		},
		-1
		);
			getParent()->addBehavior(alphaAnimator);
	}
	else
	{
		getParent()->removeBehavior(alphaAnimator);
	}
}

void Spaceship::setInputEnabled(bool a_enabled)
{
	Player::setInputEnabled(a_enabled);
	
	if (a_enabled)
	{
		for (auto bh : m_inputBehaviorIds)
		{
			getParent()->getBehavior(bh)->resume();
		}
	}
	else
	{
		for (auto bh : m_inputBehaviorIds)
		{
			getParent()->getBehavior(bh)->pause();
		}
	}
}

void Spaceship::updateImpl(f64 dt)
{
	GameManager& gameManager = CCoreEngine::Instance().GetGameManager();

	// update missiles
	for (Missiles::iterator it = m_missiles.begin(); it != m_missiles.end();)
	{
		GameEntity* const entity = (*it);

		entity->update(dt);

		if (static_cast<Missile*>(entity->getActor())->finished())
		{
			gameManager.destroyEntity(entity->getName());
			it = m_missiles.erase(it);
		}
		else
		{
			it++;
		}
	}
}

void Spaceship::stopImpl()
{
	GameManager& gameManager = CCoreEngine::Instance().GetGameManager();

	for (GameEntity* const entity : m_missiles)
	{
		gameManager.destroyEntity(entity->getName());
	}

	m_missiles.clear();
	m_inputBehaviorIds.clear();
}

void Spaceship::clear()
{

}

ActorMovement* Spaceship::createMovement()
{
	return new SpaceshipController{ m_movement_config };
}

void Spaceship::updateFacing()
{
	//Ogre::Quaternion rotation{ Ogre::Quaternion::IDENTITY };
	//if (getOrientation() == FACING_RIGHT)
	//{
	//	rotation = Ogre::Quaternion{ Ogre::Degree{ 90.f }, { 0.f, 1.f, 0.f } };
	//}
	//else
	//{
	//	rotation = Ogre::Quaternion{ Ogre::Degree{ -90.f }, { 0.f, 1.f, 0.f } };
	//}
	//
	//getParent()->getGraphics()->getNode()->setOrientation(rotation);
}

void Spaceship::checkFacing()
{
	//const f32 xRayScope = m_rayforce.getScope().x;
	//if (getOrientation() == FACING_RIGHT && xRayScope < 0)
	//{
	//	setOrientation(FACING_LEFT);
	//}
	//else if (getOrientation() == FACING_LEFT && xRayScope > 0)
	//{
	//	setOrientation(FACING_RIGHT);
	//}
	//
	//updateFacing();
}
