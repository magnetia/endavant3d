#ifndef MAGNETIA_GAME_PLAYER_H_
#define MAGNETIA_GAME_PLAYER_H_

#include "MagnetiaActor.h"

class ParticleSystemAnimator;
class ChopEffect;

class Player : public MagnetiaActor
{
public:
	Player(s32 a_maxLife = 0, const ScriptConfig& a_scriptConfig = {});
	~Player();

	virtual void	setNeutralizerBalls(s32 a_countballs);
	virtual s32		getNeutralizerBalls() const;
	virtual void	setGodMode(bool a_active);
	virtual bool	isGodMode() const;
	virtual void	setInputEnabled(bool a_enabled);
	virtual bool	isInputEnabled() const;

private:
	//Inventori
	s32									m_neutralizerBallCount;
	bool								m_godMode;
	bool								m_inputEnabled;
};

#endif
