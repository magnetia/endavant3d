#ifndef MAGNETIA_GAME_MINSECT_H_
#define MAGNETIA_GAME_MINSECT_H_

#include <memory>
#include <Sound/CSoundManager.h>
#include "MagnetiaActor.h"

class ChopEffect;
class Behavior;

class Minsect : public MagnetiaActor
{
public:
	// Factory
	class Factory : public Actor::factory_type
	{
	public:
		Actor* createElement(const Actor::factory_param_type& a_config) override;
	};

	Minsect(s32 a_maxLife = 0, const ScriptConfig& a_scriptConfig = {}, f32 a_soundRadius = 30.f);
	~Minsect();
	Actor* clone() override;

	void show(f32 a_duration);
	void attack(f32 a_duration);
	void dieAnimation(f32 a_duration);

private:
	bool						m_dying;
	std::unique_ptr<ChopEffect>	m_death_effect;
	f32							m_soundRadius;
};

#endif
