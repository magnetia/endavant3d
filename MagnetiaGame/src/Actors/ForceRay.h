#ifndef MAGNETIA_FORCE_RAY_H_
#define MAGNETIA_FORCE_RAY_H_

#include <memory>
#include <Core/CBasicTypes.h>
#include <Time/CTimeManager.h>
#include <Sound/CSoundManager.h>
#include <Utils/edvVector3.h>
#include <Utils/ChildOf.h>
#include <Utils/IdGenerator.h>
#include <Externals/OGRE/Ogre.h>
#include "BeamEffect.h"

class Nik;

class ForceRay : public ChildOf<Nik>
{
public:
	typedef IdGenerator<u32> IdGen;

	enum eStates
	{
		STATE_DISABLE,
		STATE_NEUTRAL,
		STATE_POSITIVE,
		STATE_NEGATIVE,
	};

	struct RayConfig
	{
		const f32				radius;
		const f32				displ_factor;
		const std::string		sound;

		const std::string		ray_fx;
		const std::string		pos_fx;
		const std::string		neg_fx;

		const std::string		raypos_fx;
		const std::string		rayneg_fx;


		const std::string		rayposstart_fx;
		const std::string		raynegstart_fx;

		const std::string		rayposfinal_fx;
		const std::string		raynegfinal_fx;

		const std::string		raypointer_fx;

		RayConfig(f32 a_radius = 50.f, f32 a_displ_factor = 1.f, const std::string& a_sound = "", const std::string& a_ray_fx = "", 
			const std::string& a_pos_fx = "", const std::string& a_neg_fx = "", const std::string& a_raypos_fx = "", const std::string& a_rayneg_fx = "", const std::string& a_rayposstart_fx = "", const std::string& a_raynegstart_fx = ""
			, const std::string& a_rayposfinal_fx = "", const std::string& a_raynegfinal_fx = "", const std::string & a_raypointer = "") :
			radius{ a_radius }, displ_factor{ a_displ_factor }, sound{ a_sound }, ray_fx{ a_ray_fx }, pos_fx{ a_pos_fx }, neg_fx{ a_neg_fx }, raypos_fx{ a_raypos_fx }, rayneg_fx{ a_rayneg_fx },
			rayposstart_fx{ a_rayposstart_fx }, raynegstart_fx{ a_raynegstart_fx },
			rayposfinal_fx{ a_rayposfinal_fx }, raynegfinal_fx{ a_raynegfinal_fx },
			raypointer_fx{a_raypointer}


		{}
	};

	ForceRay(const RayConfig& a_rayConfig = RayConfig{});
	~ForceRay();

	void setLogicalOrigin(const edv::Vector3f& a_position);
	void setGraphicalOrigin(const edv::Vector3f& a_position);
	void update(f64 dt);
	edv::Vector3f getDirection() const;
	edv::Vector3f getScope() const;
	void setScope(const edv::Vector3f& a_scope);
	edv::Vector3f getLimitedDirection() const;
	void setState(eStates a_state);
	void setRadius(f32 a_radius);
	f32 getAngleFromAxis(const edv::Vector3f& a_axis);

	void init();
	void stop();
	void clear();

	bool shoot(MagneticRayInfo& a_magnetic_ray_info);
	void startParticlesShootFx(const MagneticRayInfo& a_magnetic_ray_info);
	void stopParticlesShootFx(Ogre::SceneNode* a_particles_node);

private:
	typedef std::unique_ptr<BeamEffect>				BeamEffectPtr;
	typedef std::pair<EV_TimerID, BeamEffectPtr>	BeamEffectData;
	typedef std::vector<BeamEffectData>				BeamEffects;

	edv::Vector3f			m_logical_origin;
	edv::Vector3f			m_graphical_origin;
	edv::Vector3f			m_direction;
	edv::Vector3f			m_limited_direction;
	edv::Vector3f			m_hit_worldposition;
	bool					m_has_hit;
	eStates					m_state;
	f32						m_radius;
	f32						m_displ_factor;
	RayConfig				m_rayConfig;
	Ogre::SceneNode*		m_ray_particlesNode;
	Ogre::SceneNode*		m_posfx_particlesNode;
	Ogre::SceneNode*		m_negfx_particlesNode;

	Ogre::SceneNode*		m_raypos_particlesNode;
	Ogre::SceneNode*		m_rayneg_particlesNode;

	Ogre::SceneNode*		m_rayposstart_particlesNode;
	Ogre::SceneNode*		m_raynegstart_particlesNode;

	Ogre::SceneNode*		m_rayposfinal_particlesNode;
	Ogre::SceneNode*		m_raynegfinal_particlesNode;

	Ogre::SceneNode*		m_raypointer_particlesNode;

	//
	Ogre::RibbonTrail*			m_trail;
	BeamEffects					m_effects;
	IdGen						m_effectIdGenerator;
	Ogre::ColourValue			m_oldColor;
	CSoundManager::t_ChannelId	m_soundChannelId;
	bool						m_useGC;
	

	// #todo: configurable
	static const u32 MAX_EFFECTS = 200;
};

#endif
