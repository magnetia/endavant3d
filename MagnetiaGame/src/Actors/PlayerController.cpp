#include "PlayerController.h"
#include <limits>
#include <Core/CCoreEngine.h>
#include <Physics/btResultCallback.hpp>
#include <Physics/CPhysicsManager.h>
#include <Game/GameEntity.h>
#include <Renderer/CRenderDebug.h>

#include <Core/CLogManager.h>

PlayerController::PlayerController(const sControllerConfig& a_controller_config) :
	m_controller_config{ a_controller_config },
	m_magnetism{ false },
	m_groundVector{ 0.f, 0.f, 0.f },
	m_heightRayFactor{ 2.f },
	m_widthRayFactor{ .5f },
	m_heightCheckMargin{ a_controller_config.height_check_margin },
	m_heightMargin{ .5f },
	m_currentStatePosition{ STATE_POSITION_AIR }
{
	setGravity(m_controller_config.gravity);
	setRestitution(m_controller_config.restitution);
	setSliding(m_controller_config.sliding);
	setFriction(m_controller_config.friction);
}

void PlayerController::applyMove(const edv::Vector3f& a_vector)
{
	edv::Vector3f movement{ 0.f, 0.f, 0.f };
	if ((a_vector.x > 0 && m_on_right) || (a_vector.x < 0 && m_on_left))
		return;
	
	if (m_on_ground)
	{
		// ground max velocity
		if (!m_linear_velocity.isZero() && m_linear_velocity.length() > m_controller_config.ground_max_velocity && (m_linear_velocity.x * movement.x) >= 0.f)
		{
			return;
		}

		// ground movement
		if (a_vector.x != 0 && a_vector.y == 0 && a_vector.z == 0)
		{
			movement = (a_vector.x > 0.f) ? m_groundVector : -m_groundVector;
		}
		else
		{
			movement = a_vector;
		}

		// check deceleration
		if ((m_linear_velocity.x * movement.x) < 0.f)
		{
			movement *= m_controller_config.deceleration;
		}
		else
		{
			movement *= m_controller_config.acceleration;
		}
	}
	else
	{
		movement = a_vector*m_controller_config.air_acceleration;
	}
	KinematicController::applyMove(movement);
}

void PlayerController::applyForce(const edv::Vector3f& a_vector)
{
	edv::Vector3f movement{ a_vector };
	movement.x = ((movement.x > 0 && m_on_right) || (movement.x < 0 && m_on_left)) ? 0.f : movement.x;
	//movement.y = ((movement.y < 0 && m_on_ground)) ? 0.f : movement.y;
	movement.y = ((movement.y < 0 && m_on_ground || movement.y > 0 && m_on_ceiling)) ? 0.f : movement.y;
	KinematicController::applyForce(movement);
}

edv::Vector3f PlayerController::applyFriction(const edv::Vector3f& a_velocity)
{
	if (!m_movement && !m_magnetism)
	{
		if (m_on_ground)
		{
			return KinematicController::applyFriction(a_velocity);
		}
		else
		{
			return (-a_velocity)*m_controller_config.air_friction;
		}
	}
	else
	{
		return edv::Vector3f{ 0.f, 0.f, 0.f };
	}
}

void PlayerController::updateVelocityOnCollision(edv::Vector3f& a_velocity, const edv::Vector3f& a_normal_hit, f32 a_tangentFactor, f32 a_normalFactor)
{
	if (m_currentStatePosition == STATE_POSITION_GROUND)
	{
		// Wall
		if (abs(a_normal_hit.x) > 0.9f)
		{
			a_velocity.setZero();
		}
		else
		{
			KinematicController::updateVelocityOnCollision(a_velocity, a_normal_hit, a_tangentFactor, a_normalFactor);
		}
	}
	else if (m_currentStatePosition == STATE_POSITION_AIR)
	{
		// Ceiling
		if (a_normal_hit.y < 0.f)
		{
			repositionOnCeiling();
			KinematicController::updateVelocityOnCollision(a_velocity, a_normal_hit, a_tangentFactor, a_normalFactor);
		}
		// Ground
		else if (a_normal_hit.y > 0.9f)
		{
			a_velocity.setZero();
		}
		else
		{
			KinematicController::updateVelocityOnCollision(a_velocity, a_normal_hit, a_tangentFactor, a_normalFactor);
		}
	}
	else
	{
		KinematicController::updateVelocityOnCollision(a_velocity, a_normal_hit, a_tangentFactor, a_normalFactor);
	}

	//std::cout << a_normal_hit << std::endl;
}

bool PlayerController::checkOnGround(GameCollisions& a_collisionInfo)
{
	bool returnValue{ false };
	GameEntity* entity = getParent()->getParent();
	btScalar height = entity->getHalfSize().y;
	btScalar width = entity->getHalfSize().x;

	// left point
	edv::Vector3f orig{ m_current_position.x - (width* m_widthRayFactor), m_current_position.y, m_current_position.z };
	edv::Vector3f dest{ orig.x, orig.y - (height * m_heightRayFactor), orig.z };
	//CRenderDebug::getSingleton().drawLine(orig.to_OgreVector3(), dest.to_OgreVector3(), Ogre::ColourValue::Blue);
	btClosestNotMeRayResultCallback leftCallback{ entity, orig.to_btVector3(), dest.to_btVector3(), EntityBtPhysics::EdvCollisionFlags::CF_TRIGGER | btCollisionObject::CF_NO_CONTACT_RESPONSE };
	GameEntity* left_target_entity = static_cast<GameEntity*>(CCoreEngine::Instance().GetPhysicsManager().RayTestNotMe(orig, dest, leftCallback));
	if (leftCallback.hasHit())
	{
		if (orig.y - leftCallback.m_hitPointWorld.y() < (height + m_heightCheckMargin))
		{
			returnValue = true;
		}
	}
			
	// right point
	orig = edv::Vector3f(m_current_position.x + (width*m_widthRayFactor), m_current_position.y, m_current_position.z);
	dest = edv::Vector3f(orig.x, orig.y - (height * m_heightRayFactor), orig.z);
	//CRenderDebug::getSingleton().drawLine(orig.to_OgreVector3(), dest.to_OgreVector3(), Ogre::ColourValue::Red);
	btClosestNotMeRayResultCallback rightCallback = { entity, orig.to_btVector3(), dest.to_btVector3(), EntityBtPhysics::EdvCollisionFlags::CF_TRIGGER | btCollisionObject::CF_NO_CONTACT_RESPONSE };
	GameEntity* right_target_entity = static_cast<GameEntity*>(CCoreEngine::Instance().GetPhysicsManager().RayTestNotMe(orig, dest, rightCallback));
	if (rightCallback.hasHit())
	{
		if (orig.y - rightCallback.m_hitPointWorld.y() < (height + m_heightCheckMargin))
		{
			returnValue |= true;
		}
	}

	// compute ground vector
	if (rightCallback.hasHit() && leftCallback.hasHit())
	{
		m_groundVector = rightCallback.m_hitPointWorld - leftCallback.m_hitPointWorld;
	}
	else
	{
		m_groundVector = rightCallback.m_rayFromWorld - leftCallback.m_rayFromWorld;
	}

	m_groundVector.normalise();

	// center point
	orig = m_current_position;
	dest = edv::Vector3f{ orig.x, orig.y - height*m_heightRayFactor, orig.z };
	//CRenderDebug::getSingleton().drawLine(orig.to_OgreVector3(), dest.to_OgreVector3(), Ogre::ColourValue::Red);
	btClosestNotMeRayResultCallback centerCallback{ entity, orig.to_btVector3(), dest.to_btVector3(), EntityBtPhysics::EdvCollisionFlags::CF_TRIGGER | btCollisionObject::CF_NO_CONTACT_RESPONSE };
	GameEntity* center_target_entity = static_cast<GameEntity*>(CCoreEngine::Instance().GetPhysicsManager().RayTestNotMe(orig, dest, centerCallback));
	if (centerCallback.hasHit())
	{
		if (orig.y - centerCallback.m_hitPointWorld.y() < (height + m_heightCheckMargin))
		{
			returnValue |= true;
		}
	}

	// reposition on ground
	if ( returnValue && !m_impulse && (!m_magnetism || (m_magnetism && m_total_acceleartion.y <= 0.f)) )
	{
		const edv::Vector3f ground_hit_point = centerCallback.hasHit() ? centerCallback.m_hitPointWorld : leftCallback.hasHit() ? leftCallback.m_hitPointWorld : rightCallback.m_hitPointWorld;
		repositionOnGround(ground_hit_point);

		// notify ground collision
		const edv::Vector3f ground_hit_normal = centerCallback.hasHit() ? centerCallback.m_hitNormalWorld : leftCallback.hasHit() ? leftCallback.m_hitNormalWorld : rightCallback.m_hitNormalWorld;
		GameEntity* target_entity = centerCallback.hasHit() ? center_target_entity : leftCallback.hasHit() ? left_target_entity : right_target_entity;
		a_collisionInfo.push_back({ target_entity, ground_hit_point, ground_hit_normal });
	}

	return returnValue;
}

bool PlayerController::checkOnCeiling(GameCollisions& a_collisionInfo)
{
	bool returnValue{ false };
	GameEntity* entity = getParent()->getParent();
	btScalar height = entity->getHalfSize().y;

	edv::Vector3f orig{ m_current_position };
	edv::Vector3f dest{ orig.x, orig.y + height * m_heightRayFactor, orig.z };
	//CRenderDebug::getSingleton().drawLine(orig.to_OgreVector3(), dest.to_OgreVector3(), Ogre::ColourValue::Blue);
	btClosestNotMeRayResultCallback leftCallback{ entity, orig.to_btVector3(), dest.to_btVector3(), EntityBtPhysics::EdvCollisionFlags::CF_TRIGGER | btCollisionObject::CF_NO_CONTACT_RESPONSE };
	GameEntity* target_entity = static_cast<GameEntity*>(CCoreEngine::Instance().GetPhysicsManager().RayTestNotMe(orig, dest, leftCallback));
	if (leftCallback.hasHit())
	{
		if (leftCallback.m_hitPointWorld.y() - orig.y < (height + m_heightMargin))
		{
			returnValue = true;
		}
	}
	return returnValue;
}

void PlayerController::startMagnetism(const edv::Vector3f& a_vector)
{
	edv::Vector3f acceleration = a_vector*m_controller_config.magnetic_acceleration;
	applyForce(acceleration);
	m_magnetism = true;
}

void PlayerController::stopMagnetism()
{
	m_magnetism = false;
}

bool PlayerController::hasMagnetism() const
{
	return m_magnetism;
}

f32 PlayerController::getMovementSpeedFactor() const
{
	f32 speedFactor{ 0.f };
	f32 maxGroundVelocity{ m_controller_config.ground_max_velocity };
	if (m_on_ground && maxGroundVelocity > 0.f)
	{
		speedFactor = m_linear_velocity.x / maxGroundVelocity;
	}
	return speedFactor;
}

void PlayerController::checkPosition(GameCollisions& a_collisionInfo)
{
	KinematicController::checkPosition(a_collisionInfo);

	switch (m_currentStatePosition)
	{
	case STATE_POSITION_GROUND:
		if (!m_on_ground)
		{
			m_heightCheckMargin *= 0.2f;
			m_currentStatePosition = STATE_POSITION_AIR;
		}
		break;

	case STATE_POSITION_AIR:
		if (m_on_ground)
		{
			m_linear_velocity.y = 0.f;
			m_heightCheckMargin = m_controller_config.height_check_margin;
			m_currentStatePosition = STATE_POSITION_GROUND;
		}
		break;
	}
	
	//static edv::Vector3f position(m_current_position);
	//if (position != m_current_position)
	//{
	//	position = m_current_position;
	//	std::cout << "pos" << m_current_position << std::endl;
	//	std::cout << "vel" << m_linear_velocity << std::endl;
	//}
}

void PlayerController::repositionOnGround(const edv::Vector3f& a_hitPointWorld)
{
	GameEntity* entity = getParent()->getParent();
	btScalar height = entity->getHalfSize().y;
	setPosition(edv::Vector3f{ m_current_position.x, a_hitPointWorld.y + height + m_heightMargin, m_current_position.z });
}

void PlayerController::repositionOnCeiling()
{
	GameEntity* entity = getParent()->getParent();
	btScalar height = entity->getHalfSize().y;

	edv::Vector3f orig{ m_current_position };
	edv::Vector3f dest{ orig.x, orig.y + height*m_heightRayFactor, orig.z };
	//CRenderDebug::getSingleton().drawLine(orig.to_OgreVector3(), dest.to_OgreVector3(), Ogre::ColourValue::Red);
	btClosestNotMeRayResultCallback callback{ entity, orig.to_btVector3(), dest.to_btVector3(), EntityBtPhysics::EdvCollisionFlags::CF_TRIGGER | btCollisionObject::CF_NO_CONTACT_RESPONSE };
	GameEntity* target_entity = static_cast<GameEntity*>(CCoreEngine::Instance().GetPhysicsManager().RayTestNotMe(orig, dest, callback));
	if (callback.hasHit())
	{
		setPosition(edv::Vector3f{ m_current_position.x, callback.m_hitPointWorld.y() - height - m_heightMargin, m_current_position.z });
	}
}

