#include <Core/CCoreEngine.h>
#include <Physics/CPhysicsManager.h>
#include <Physics/btResultCallback.hpp>
#include <Renderer/CRenderManager.h>
#include <Game/GameEntity.h>
#include <Game/GameManager.h>
#include "LethalBarrier.h"

#include <Renderer/CRenderDebug.h>

Actor* LethalBarrier::Factory::createElement(const Actor::factory_param_type& a_config)
{
	return new LethalBarrier{
		{ a_config["script/path"], a_config["script/scope"] },
		a_config["off_time"],
		a_config["on_time"],
		{ a_config["direction/x"], a_config["direction/y"], a_config["direction/z"] },
		a_config["sound_range"],
		a_config["sphere_material"],
		a_config["ray_material"],
	};
}

LethalBarrier::LethalBarrier(const ScriptConfig& a_scriptConfig, f64 a_offTime, f64 a_onTime, const edv::Vector3f& a_direction, f32 a_sound_range, const std::string& a_sphereMaterialName, const std::string& a_rayMaterialName) :
	MagnetiaActor{ 0, a_scriptConfig },
	m_offTime{ a_offTime },
	m_onTime{ a_onTime },
	m_directVect{ a_direction },
	m_bbChain{ nullptr },
	m_node{ nullptr },
	m_collisionWorldObject{ nullptr },
	m_arcPoint1{ },
	m_arcPoint2{ },
	m_soundChannelId{ CSoundManager::UNASSIGNED_CHANNEL_ID },
	m_sound_range{ a_sound_range },
	m_sphereMaterialName{ a_sphereMaterialName },
	m_rayMaterialName{ a_rayMaterialName }
{
	// cutreversion para la feria de Madrid
	CCoreEngine::Instance().GetSoundManager().preloadSound("lethalbarrier_sound");
}

LethalBarrier::~LethalBarrier()
{
	clear();
}

Actor* LethalBarrier::clone()
{
	return new LethalBarrier{ getScriptConfig(), m_offTime, m_onTime, m_directVect };
}

void LethalBarrier::clear()
{
	Ogre::SceneManager* const sceneMgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();

	if (m_collisionWorldObject)
	{
		sceneMgr->destroyManualObject(m_collisionWorldObject);
		m_collisionWorldObject = nullptr;
	}

	m_arcPoint1.clear();
	m_arcPoint2.clear();

	if (m_bbChain)
	{
		sceneMgr->destroyBillboardChain(m_bbChain);
		m_bbChain = nullptr;
	}

	if (m_node)
	{
		sceneMgr->destroySceneNode(m_node);
		m_node = nullptr;
	}

	CCoreEngine::Instance().GetSoundManager().releaseSound("lethalbarrier_sound");
}

f64 LethalBarrier::getOffTime() const
{
	return m_offTime;
}

f64 LethalBarrier::getOnTime() const
{
	return m_onTime;
}

void LethalBarrier::showRay(bool a_show)
{
	if (m_bbChain && a_show != m_bbChain->isVisible())
	{
		m_bbChain->setVisible(a_show);
	}

	GameEntity* const player = CCoreEngine::Instance().GetGameManager().getPlayer();

	if (a_show)
	{
		edv::Vector3f position{ (m_arcPoint1.node->getPosition() + m_arcPoint2.node->getPosition())/2.f };
		f32 dist{ (position - player->getWorldPosition()).length() };
		if ( dist < m_sound_range)
		{
			m_soundChannelId = CCoreEngine::Instance().GetSoundManager().playSound("lethalbarrier_sound");
		}
	}
	else if (m_soundChannelId != CSoundManager::UNASSIGNED_CHANNEL_ID)
	{
		CCoreEngine::Instance().GetSoundManager().stopSound(m_soundChannelId);
	}
}

bool LethalBarrier::isShowingRay() const
{
	bool showing = false;

	if (m_bbChain)
	{
		showing = m_bbChain->isVisible();
	}

	return showing;
}

void LethalBarrier::initImpl()
{
	// update Magnetia Actor
	MagnetiaActor::initImpl();

	// update Lethal Barrier
	// get first point
	const edv::Vector3f orig = getParent()->getWorldPosition();
	edv::Vector3f dest{ orig.x + (m_directVect.x * -1000), orig.y + (m_directVect.y * -1000), orig.z + (m_directVect.z * -1000) };

	btCollisionWorld::ClosestRayResultCallback callbackPoint1{
		orig.to_btVector3(),
		dest.to_btVector3(),
	};

	CCoreEngine::Instance().GetPhysicsManager().RayTestNotMe(orig, dest, callbackPoint1);

	if (callbackPoint1.hasHit())
	{
		m_arcPoint1.point = edv::Vector3f{
			callbackPoint1.m_hitPointWorld.x(),
			callbackPoint1.m_hitPointWorld.y(),
			callbackPoint1.m_hitPointWorld.z()
		};
	}
	else
	{
		throw std::runtime_error{ "point1 not found for " + getParent()->getName() };
	}

	// get second point
	dest = edv::Vector3f{ orig.x + (m_directVect.x * 1000), orig.y + (m_directVect.y * 1000), orig.z + (m_directVect.z * 1000) };

	btCollisionWorld::ClosestRayResultCallback callbackPoint2{
		orig.to_btVector3(),
		dest.to_btVector3(),
	};

	CCoreEngine::Instance().GetPhysicsManager().RayTestNotMe(orig, dest, callbackPoint2);

	if (callbackPoint2.hasHit())
	{
		m_arcPoint2.point = edv::Vector3f{
			callbackPoint2.m_hitPointWorld.x(),
			callbackPoint2.m_hitPointWorld.y(),
			callbackPoint2.m_hitPointWorld.z()
		};
	}
	else
	{
		throw std::runtime_error{ "point2 not found for " + getParent()->getName() };
	}

	// create billboard chain
	m_direction = m_arcPoint2.point - m_arcPoint1.point;
	Ogre::SceneManager* const sceneMgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();

	m_bbChain = sceneMgr->createBillboardChain();
	m_bbChain->setMaterialName(m_rayMaterialName);
	m_bbChain->setVisible(false);

	m_node = sceneMgr->getRootSceneNode()->createChildSceneNode();
	m_node->attachObject(m_bbChain);

	Ogre::Vector3 elementPosition = m_arcPoint1.point.to_OgreVector3();
	f32 elementWidth = 1.5;
	f32 texCoord = 0;
	Ogre::Vector3 step = m_direction.to_OgreVector3() / 19;

	for (int i = 0; i < 20; i++)
	{
		texCoord = static_cast<f32>(i);
		const Ogre::BillboardChain::Element elem{ elementPosition, elementWidth, texCoord, Ogre::ColourValue::White, Ogre::Quaternion::IDENTITY };
		m_bbChain->addChainElement(0, elem);

		elementPosition += step;
	}

	// create spheres
	m_arcPoint1.sphere = sceneMgr->createEntity(getParent()->getName() + "_sphere1", Ogre::SceneManager::PT_SPHERE);
	m_arcPoint1.sphere->getMesh()->buildEdgeList();
	m_arcPoint1.sphere->setMaterialName(m_sphereMaterialName);
	m_arcPoint1.node = sceneMgr->getRootSceneNode()->createChildSceneNode(m_arcPoint1.point.to_OgreVector3());
	m_arcPoint1.node->attachObject(m_arcPoint1.sphere);
	m_arcPoint1.node->setScale({ 0.05f, 0.05f, 0.05f });

	m_arcPoint2.sphere = sceneMgr->createEntity(getParent()->getName() + "_sphere2", Ogre::SceneManager::PT_SPHERE);
	m_arcPoint2.sphere->getMesh()->buildEdgeList();
	m_arcPoint2.sphere->setMaterialName(m_sphereMaterialName);
	m_arcPoint2.node = sceneMgr->getRootSceneNode()->createChildSceneNode(m_arcPoint2.point.to_OgreVector3());
	m_arcPoint2.node->attachObject(m_arcPoint2.sphere);
	m_arcPoint2.node->setScale({ 0.05f, 0.05f, 0.05f });

	// create mesh used to define collision zone
	const edv::Vector3f factor{ 2.25f * !m_directVect.x, 2.25f * !m_directVect.y, 2.25f * !m_directVect.z };

	const Ogre::Vector3 
		pt1UpFront{ m_arcPoint1.point.x + factor.x, m_arcPoint1.point.y + factor.y, m_arcPoint1.point.z + factor.z },
		pt1UpBack{ m_arcPoint1.point.x + factor.x, m_arcPoint1.point.y + factor.y, m_arcPoint1.point.z - factor.z },
		pt1DownFront{ m_arcPoint1.point.x - factor.x, m_arcPoint1.point.y - factor.y, m_arcPoint1.point.z + factor.z },
		pt1DownBack{ m_arcPoint1.point.x - factor.x, m_arcPoint1.point.y - factor.y, m_arcPoint1.point.z - factor.z },
		pt2UpFront{ m_arcPoint2.point.x + factor.x, m_arcPoint2.point.y + factor.y, m_arcPoint2.point.z + factor.z },
		pt2UpBack{ m_arcPoint2.point.x + factor.x, m_arcPoint2.point.y + factor.y, m_arcPoint2.point.z - factor.z },
		pt2DownFront{ m_arcPoint2.point.x - factor.x, m_arcPoint2.point.y - factor.y, m_arcPoint2.point.z + factor.z },
		pt2DownBack{ m_arcPoint2.point.x - factor.x, m_arcPoint2.point.y - factor.y, m_arcPoint2.point.z - factor.z };

	auto* scene_mgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();

	const std::string manualObjectName = getParent()->getName() + "_barrierCollision";
	m_collisionWorldObject = scene_mgr->createManualObject(manualObjectName);
	m_collisionWorldObject->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_TRIANGLE_STRIP);

	auto index = 0;
	for (auto point : { pt1UpBack, pt1UpFront, pt2UpBack, pt2UpFront, pt2DownBack, pt2DownFront, pt1DownBack, pt1DownFront })
	{
		m_collisionWorldObject->position(point);
		m_collisionWorldObject->index(index++);
	}
	m_collisionWorldObject->end();

	Ogre::MeshPtr mesh = m_collisionWorldObject->convertToMesh(manualObjectName);

	// set procedural graphics
	EntityOgreGraphics* const graphics = getParent()->getGraphics();
	std::string entityName = getParent()->getName();

	graphics->clear();

	graphics->init(
		EntityOgreGraphics::Config{ 
			entityName,
			Ogre::Vector3::ZERO, 
			Ogre::Quaternion::IDENTITY, 
			true, 
			manualObjectName 
		}
	);

	graphics->getNode()->setVisible(false);

	// set procedural physics
	EntityBtPhysics* const physics = getParent()->getPhysics();

	physics->clear();

	physics->init(
		getParent()->getGraphics(), 
		EntityBtPhysics::Config{ 
			0.0f, 
			false, 
			{}, 
			false, 
			EntityBtPhysics::CollisionConfig{
				EntityBtPhysics::COLLISION_SHAPE_TRIANGLE_MESH, 
				EntityBtPhysics::COLLISION_DETECTION_TRIGGER }
		}
	);
}

void LethalBarrier::updateImpl(f64 dt)
{
	MagnetiaActor::updateImpl(dt);

	Ogre::Vector3 elementPosition = m_arcPoint1.point.to_OgreVector3();
	Ogre::Vector3 step = m_direction.to_OgreVector3() / 19;

	for (int i = 0; i < 20; i++)
	{
		Ogre::BillboardChain::Element elem = m_bbChain->getChainElement(0, i);
		Ogre::Vector3 randomVector{ Ogre::Vector3::ZERO };
		const f32 ceil = 1.5;

		randomVector = Ogre::Vector3{
			(static_cast<f32>(rand()) / RAND_MAX) * ceil * (rand() % 2 ? 1.f : -1.f),
			(static_cast<f32>(rand()) / RAND_MAX) * ceil * (rand() % 2 ? 1.f : -1.f),
			(static_cast<f32>(rand()) / RAND_MAX) * ceil * (rand() % 2 ? 1.f : -1.f)
		};

		elem.position = step.normalisedCopy().crossProduct(randomVector) + elementPosition;
		m_bbChain->updateChainElement(0, i, elem);			
		elementPosition += step;
	}
}

LethalBarrier::ArcPoint::ArcPoint(const edv::Vector3f& a_point, Ogre::SceneNode* a_node, Ogre::Entity* a_sphere) :
	point{ a_point }, 
	node{ a_node }, 
	sphere{ a_sphere } 
{

}

void LethalBarrier::ArcPoint::clear()
{
	Ogre::SceneManager* const sceneMgr = CCoreEngine::Instance().GetRenderManager().GetSceneManager();

	if (sphere)
	{
		sceneMgr->destroyEntity(sphere);
		sphere = nullptr;
	}

	if (node)
	{
		sceneMgr->destroySceneNode(node);
		node = nullptr;
	}

	point = {};
}
