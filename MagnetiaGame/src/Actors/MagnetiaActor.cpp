#include <Core/CCoreEngine.h>
#include <Script/ScriptManager.h>
#include <Game/GameEntity.h>
#include "MagnetiaActor.h"

Actor* MagnetiaActor::Factory::createElement(const Actor::factory_param_type& a_config)
{
	return new MagnetiaActor{ a_config["max_life"].as<s32>(), { a_config["script/path"], a_config["script/scope"] } };
}

MagnetiaActor::MagnetiaActor(s32 a_maxLife, const ScriptConfig& a_scriptConfig) :
	Actor{ a_maxLife, a_scriptConfig },
	m_orientation{ FACING_RIGHT },
	m_previousRotation{ Ogre::Quaternion::IDENTITY },
	m_direction{ DIRECTION_FORWARD }
{

}

Actor* MagnetiaActor::clone()
{
	return new MagnetiaActor(getMaxLife(), getScriptConfig());
}

void MagnetiaActor::applyMove(const edv::Vector3f & a_vector)
{
	Actor::applyMove(a_vector);
	checkFacing();
	checkDirection();
}

//void MagnetiaActor::applyForce(const edv::Vector3f & a_vector)
//{
//
//}

void MagnetiaActor::onMagneticRayHit(const MagneticRayInfo& a_rayInfo)
{
	if (!m_scriptConfig.empty())
	{
		CCoreEngine::Instance().GetScriptManager().callProcedure(
			m_scriptConfig.scope + "OnMagneticRayHit", 
			getParent(),
			a_rayInfo.originEntity,
			a_rayInfo.originPos,
			a_rayInfo.direction,
			a_rayInfo.worldPoint,
			a_rayInfo.force, 
			a_rayInfo.charge);
	}
}

void MagnetiaActor::onMagneticRayTarget(const MagneticRayInfo& a_rayInfo)
{
	if (!m_scriptConfig.empty())
	{
		CCoreEngine::Instance().GetScriptManager().callProcedure(
			m_scriptConfig.scope + "OnMagneticRayTarget",
			getParent(),
			a_rayInfo.originEntity,
			a_rayInfo.originPos,
			a_rayInfo.direction,
			a_rayInfo.worldPoint);
	}
}

void MagnetiaActor::onMagneticRayUnTarget()
{
	if (!m_scriptConfig.empty())
	{
	CCoreEngine::Instance().GetScriptManager().callProcedure(
		m_scriptConfig.scope + "OnMagneticRayUnTarget",
		getParent()
		);
	}
}

void MagnetiaActor::setOrientation(Orientation a_orientation)
{
	m_orientation = a_orientation;
}

MagnetiaActor::Orientation MagnetiaActor::getOrientation() const
{
	return m_orientation;
}

MagnetiaActor::Direction MagnetiaActor::getDirection() const
{
	return m_direction;
}

void MagnetiaActor::setDirection(MagnetiaActor::Direction a_direction)
{
	m_direction = a_direction;
}

void MagnetiaActor::initImpl()
{
	m_previousRotation = getParent()->getGraphics()->getNode()->_getDerivedOrientation();
}

void MagnetiaActor::updateImpl(f64 dt)
{

}

void MagnetiaActor::stopImpl()
{

}

void MagnetiaActor::checkFacing()
{
	const Ogre::Quaternion currRotation = getParent()->getGraphics()->getNode()->_getDerivedOrientation();

	if (m_previousRotation != currRotation)
	{
		m_orientation = m_orientation == FACING_LEFT ? FACING_RIGHT : FACING_LEFT;
	}

	m_previousRotation = currRotation;
}

void MagnetiaActor::checkDirection()
{
	if (m_movement.get() && m_movement->getVelocity().x >= 0 && getOrientation() == FACING_RIGHT || m_movement.get() && m_movement->getVelocity().x <= 0 && getOrientation() == FACING_LEFT)
	{
		m_direction = DIRECTION_FORWARD;
	}
	else
	{
		m_direction = DIRECTION_BACK;
	}
}
