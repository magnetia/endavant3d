#ifndef MAGNETIA_GAME_MINE_H_
#define MAGNETIA_GAME_MINE_H_

#include "MagnetiaActor.h"

class TimedBehavior;

class Mine : public MagnetiaActor
{
public:
	// Factory
	class Factory : public Actor::factory_type
	{
	public:
		Actor* createElement(const Actor::factory_param_type& a_config) override;
	};

	Mine(s32 a_maxLife = 0, const ScriptConfig& a_scriptConfig = {});
	~Mine();
	Actor* clone() override;

	void explode();
	GameEntity* getBossEntity() const;
	bool finished() const;

	void setBossEntity(GameEntity* a_boss);

private:
	void updateImpl(f64 dt) override;

	GameEntity*		m_boss;
	TimedBehavior*	m_animation;
	bool			m_finished;
};

#endif
