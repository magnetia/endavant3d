#include <Core/CCoreEngine.h>
#include <Game/GameManager.h>
#include <Game/FlyStraightAnimator.h>
#include <Game/GameEntity.h>
#include "Boss.h"
#include "Missile.h"
#include "BossConfrontState.h"

#include <Core/CLogManager.h>

BossConfrontState::BossConfrontState(Boss* a_boss, const edv::range32f& a_variationY, const edv::range64f& a_duration, const edv::range32& a_shoots) :
	BossState{a_boss},
	m_remainingShoots{ 0 },
	m_currentAnimation{ nullptr },
	m_missiles{ },
	m_variationY{ a_variationY },
	m_duration{ a_duration },
	m_shoots{ a_shoots },
	m_topLimit{ },
	m_bottomLimit{ },
	m_lastLimitedUp{ false },
	m_lastLimitedDown{ false }
{

}

BossConfrontState::~BossConfrontState()
{
	
}

void BossConfrontState::start()
{
	getLimits();
	m_remainingShoots = m_shoots.random();

	shoot();
}

void BossConfrontState::update(f64 dt)
{
	GameManager& gameManager = CCoreEngine::Instance().GetGameManager();

	// update missiles
	for (Missiles::iterator it = m_missiles.begin(); it != m_missiles.end(); )
	{
		GameEntity* const entity = (*it);

		entity->update(dt);

		if (static_cast<Missile*>(entity->getActor())->finished())
		{
			gameManager.destroyEntity(entity->getName());
			it = m_missiles.erase(it);
		}
		else
		{
			it++;
		}
	}

	// shooting and movement
	if (m_currentAnimation && m_currentAnimation->finished())
	{
		m_boss->getParent()->removeBehavior(m_currentAnimation);
		m_currentAnimation = nullptr;

		shoot();
	}
}

void BossConfrontState::stop()
{
	GameManager& gameManager = CCoreEngine::Instance().GetGameManager();

	for (GameEntity* const entity : m_missiles)
	{
		gameManager.destroyEntity(entity->getName());
	}

	m_missiles.clear();

	if (m_currentAnimation)
	{
		m_currentAnimation->stop();
		m_boss->getParent()->removeBehavior(m_currentAnimation);
		m_currentAnimation = nullptr;
	}
	
	m_remainingShoots = 0;
}

bool BossConfrontState::finished()
{
	return m_remainingShoots == 0;
}

edv::Vector3f BossConfrontState::getStartPoint() const
{
	return {};
}

void BossConfrontState::getLimits()
{
	// campanya
	const edv::Vector3f scaledHalfSize{ m_boss->getParent()->getScaledHalfSize() };

	{
		const edv::Vector3f orig = m_boss->getParent()->getWorldPosition();
		edv::Vector3f dest{ orig.x, orig.y - 1000, orig.z };

		btCollisionWorld::ClosestRayResultCallback callbackPoint1{
			orig.to_btVector3(),
			dest.to_btVector3(),
		};

		CCoreEngine::Instance().GetPhysicsManager().RayTestNotMe(orig, dest, callbackPoint1);

		if (callbackPoint1.hasHit())
		{
			m_topLimit = edv::Vector3f{
				callbackPoint1.m_hitPointWorld.x(),
				callbackPoint1.m_hitPointWorld.y() + scaledHalfSize.y,
				callbackPoint1.m_hitPointWorld.z()
			};
		}
		else
		{
			throw std::runtime_error{ "point1 not found for " + m_boss->getParent()->getName() };
		}
	}
	{
		const edv::Vector3f orig = m_boss->getParent()->getWorldPosition();
		edv::Vector3f dest{ orig.x, orig.y + 1000, orig.z };

		btCollisionWorld::ClosestRayResultCallback callbackPoint1{
			orig.to_btVector3(),
			dest.to_btVector3(),
		};

		CCoreEngine::Instance().GetPhysicsManager().RayTestNotMe(orig, dest, callbackPoint1);

		if (callbackPoint1.hasHit())
		{
			m_bottomLimit = edv::Vector3f{
				callbackPoint1.m_hitPointWorld.x(),
				callbackPoint1.m_hitPointWorld.y() - scaledHalfSize.y,
				callbackPoint1.m_hitPointWorld.z()
			};
		}
		else
		{
			throw std::runtime_error{ "point2 not found for " + m_boss->getParent()->getName() };
		}
	}
}

void BossConfrontState::shoot()
{
	if (m_remainingShoots - 1 > 0)
	{
		m_remainingShoots--;
		GameEntity* const bossEntity{ m_boss->getParent() };
		
		// shoot
		GameManager::EntityInfo info{ CCoreEngine::Instance().GetGameManager().createEntity("Missile", m_boss->getParent()->getWorldPosition().to_OgreVector3()) };
		GameEntity* const missileEntity{ info.second };		
		Missile* const missileActor{ static_cast<Missile*>(info.second->getActor()) };

		missileActor->setOwner(m_boss->getParent());
		missileActor->setTarget(CCoreEngine::Instance().GetGameManager().getPlayer());
		// #todo:
		missileActor->setDirectionVect({ -1000.f, 0.f, 0.f });
		//missileActor->setDirectionVect({ m_boss->getOrientation() == MagnetiaActor::FACING_LEFT ? -1000.f : 1000.f, 0.f, 0.f });
		m_missiles.push_back(missileEntity);

		// move
		f64 nextDuration{ m_duration.random() };
		f32 nextVariationY{ m_variationY.random() };
		
		if ((m_lastLimitedUp && nextVariationY < 0) || (m_lastLimitedDown && nextVariationY > 0))
		{
			nextVariationY *= -1.f;
			m_lastLimitedUp = false;
			m_lastLimitedDown = false;
		}

		edv::Vector3f nextPosition{ bossEntity->getWorldPosition() + edv::Vector3f{ 0, nextVariationY, 0 } };

		if (nextPosition.y <= m_topLimit.y)
		{
			nextPosition.y = m_topLimit.y;
			m_lastLimitedUp = true;
			m_lastLimitedDown = false;
		}
		else if (nextPosition.y >= m_bottomLimit.y)
		{
			nextPosition.y = m_bottomLimit.y;
			m_lastLimitedUp = false;
			m_lastLimitedDown = true;
		}

		m_currentAnimation = new FlyStraightAnimator{ nextDuration, bossEntity->getWorldPosition(), nextPosition };
		bossEntity->addBehavior(m_currentAnimation);
	}
	else
	{
		stop();
	}
}
