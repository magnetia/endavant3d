#ifndef MAGNETIA_GAME_BOSS_TRANSITION_STATE_H_
#define MAGNETIA_GAME_BOSS_TRANSITION_STATE_H_

#include <vector>
#include "BossState.h"

class TimedBehavior;

class BossTransitionState : public BossState
{
public:
	BossTransitionState(Boss* a_boss);

	void start() override;
	void update(f64 dt) override;
	void stop() override;
	bool finished() override;
	edv::Vector3f getStartPoint() const override;

private:
	TimedBehavior*	m_movementAnimator;
	TimedBehavior*	m_alphaAnimator;
};

#endif
