#ifndef MAGNETIA_UNSTABLE_PLATFORM_H_
#define MAGNETIA_UNSTABLE_PLATFORM_H_

#include "MagnetiaActor.h"

class UnstablePlatform : public MagnetiaActor
{
public:
	class Factory : public Actor::factory_type
	{
	public:
		Actor* createElement(const Actor::factory_param_type& a_config) override;
	};

	UnstablePlatform(const ScriptConfig& a_scriptConfig = {}, f64 a_timeInActive = 0.f, f64 a_timeInAdvise = 0.f, f64 a_timeInInactive = 0.f);
	void playAdvise();
	f64 getTimeInActive() const;
	f64 getTimeInAdvise() const;
	f64 getTimeInInactive() const;

protected:
	void initImpl() override;
	void stopImpl() override;

private:
	f64 m_timeInActive;
	f64 m_timeInAdvise;
	f64 m_timeInInactive;
};

#endif
