#ifndef MAGNETIA_GAME_CLASHNIK_H_
#define MAGNETIA_GAME_CLASHNIK_H_

#include "Utils/Factory.h"
#include "MagnetiaActor.h"

class Clashnik : public MagnetiaActor
{
public:
	// Factory
	class Factory : public Actor::factory_type
	{
	public:
		Actor* createElement(const Actor::factory_param_type& a_config) override;
	};

	enum Orientation
	{
		FACING_LEFT,
		FACING_RIGHT,

		FACING_INVALID
	};

	Clashnik(s32 a_maxLife = 0, const ScriptConfig& a_scriptConfig = {});
	~Clashnik();
	Actor* clone() override;

	void setSpeed(f32 a_speed);
	void setOrientation(u32 a_orientation);
	f32 getSpeed() const;
	u32 getClashnikOrientation() const;
	bool isStuck();

protected:
	void initImpl() override;
	void updateImpl(f64 dt) override;
	ActorMovement* createMovement() override;

private:
	f32				m_speed;
	Orientation		m_orientation;
	edv::Vector3f	m_askedPosition;
};

#endif
