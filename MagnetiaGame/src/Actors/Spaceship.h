#ifndef MAGNETIA_GAME_SPACESHIP_H_
#define MAGNETIA_GAME_SPACESHIP_H_

#include <Input/CInputMouse.h>
#include <Game/MouseListener.h>
#include "Player.h"
#include "SpaceshipController.h"

class Spaceship : public Player
{
public:
	// Factory
	class Factory : public Actor::factory_type
	{
	public:
		Actor* createElement(const Actor::factory_param_type& a_config) override;
	};

	Spaceship(s32 a_maxLife = 0, const ScriptConfig& a_scriptConfig = {}, const SpaceshipController::sControllerConfig& a_controller_config = {});
	~Spaceship();
	Actor* clone() override;

	void applyMove(const edv::Vector3f& a_vector) override;
	void kill() override;
	void onCollision(const CollisionInfo& a_collisionInfo) override;

	void onMouseDown(MouseListener::MouseButton a_button);
	void onMouseUp(MouseListener::MouseButton a_button);
	void onMouseHover(bool a_hover);
	void setGodMode(bool a_active) override;
	void setInputEnabled(bool a_enabled) override;

protected:
	void initImpl() override;
	void updateImpl(f64 dt) override;
	void stopImpl() override;
	void clear() override;

	ActorMovement* createMovement() override;

private:
	typedef std::vector<GameEntity*> Missiles;

	enum eStates
	{
		STATE_NONE,
	};

	void updateFacing();
	void checkFacing() override;

	typedef std::vector<Behavior::Id> tBevaviorsIds;
	tBevaviorsIds							m_inputBehaviorIds;
	SpaceshipController::sControllerConfig	m_movement_config;
	bool									m_shooting;
	Missiles								m_missiles;
};

#endif
