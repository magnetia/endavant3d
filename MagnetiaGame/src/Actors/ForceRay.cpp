#include <Core/CCoreEngine.h>
#include <Input/CInputManager.h>
#include <Renderer/CRenderManager.h>
#include <Renderer/CRenderDebug.h>
#include <Camera/CameraManager.h>
#include <Camera/Camera.h>
#include <Game/GameEntity.h>
#include <Physics/CPhysicsManager.h>
#include <Physics/btResultCallback.hpp>
#include "Nik.h"
#include "ForceRay.h"


ForceRay::ForceRay(const RayConfig& a_rayConfig) :
	m_logical_origin{ edv::Vector3f{ 0.f, 0.f, 0.f } },
	m_graphical_origin{ edv::Vector3f{ 0.f, 0.f, 0.f } },
	m_state{ STATE_NEUTRAL },
	m_radius{ a_rayConfig.radius },
	m_displ_factor{ a_rayConfig.displ_factor },
	m_rayConfig{ a_rayConfig },
	m_ray_particlesNode{ nullptr },
	m_posfx_particlesNode{ nullptr },
	m_negfx_particlesNode{ nullptr },
	m_oldColor{ Ogre::ColourValue::White },
	m_soundChannelId{ CSoundManager::UNASSIGNED_CHANNEL_ID },
	m_useGC{ CCoreEngine::Instance().GetOptionManager().getOption("controls/usegamepad") }
{
	m_direction = edv::Vector3f(0, -m_radius, 0); //Inicia el raig a la dreta minim
	m_limited_direction = m_direction;
	m_has_hit = false;
	m_effects.reserve(MAX_EFFECTS);
}

ForceRay::~ForceRay()
{

}

void ForceRay::setLogicalOrigin(const edv::Vector3f& a_position)
{
	m_logical_origin = a_position;
}

void ForceRay::setGraphicalOrigin(const edv::Vector3f& a_position)
{
	m_graphical_origin = a_position;
}

void ForceRay::update(f64 dt)
{
	auto screen_coords_player = CCoreEngine::Instance().GetCameraManager().get_current_camera()->TransformWorldCoordToScreenCoord(m_logical_origin.to_OgreVector3());
	
	Ogre::Vector2 direction(0.0f);
	if (getParent()->isInputEnabled())
	{
		if (m_useGC)
		{
			auto vecGC = CCoreEngine::Instance().GetInputManager().GetGameController().getGameControllersIDs();
			if (!vecGC.empty())
			{
				auto gc = CCoreEngine::Instance().GetInputManager().GetGameController().getGameController(vecGC.at(0));
				direction = gc.lock()->getAxisvectorNormalized(EDV_GAMECONTROLLER_AXIS_RIGHTY, EDV_GAMECONTROLLER_AXIS_RIGHTX);
				direction.y = -direction.y;
				//	LOG(LOG_INFO, LOGSUB_INPUT, " X: %f     Y: %f \n", direction.x, direction.y);
			}
		}
		else
		{

			auto posmouserel = CCoreEngine::Instance().GetInputManager().GetMouse().GetPosRel();
			if (posmouserel.x != 0)
			{
				m_direction.x += posmouserel.x*m_displ_factor;
			}

			if (posmouserel.y != 0)
			{
				m_direction.y += posmouserel.y*m_displ_factor;
			}
			direction.x = m_direction.x;
			direction.y = m_direction.y;
		}
	}

	//Si no ens estem movent (lenght == 0) la direccio del raig queda igual!
	if (direction.length())
	{
		// Project point to sphere: [ sphere(center C, radius R), point P, find sphere point SP ] -> [ SP = C + R*(P-C)/(norm(P-C)) ]
		m_direction.x = m_radius*direction.x / direction.length();
		m_direction.y = m_radius*direction.y / direction.length();
	}

	//std::cout << "Mouse X: " << mouse_vec.x << " Y: " << mouse_vec.y << " Origin X: " << origen.x << " Y: " << origen.y << " Desti X: " << desti.x << "Y: " << desti.y << std::endl;
	Ogre::ColourValue color = Ogre::ColourValue::ZERO;
	switch (m_state)
	{
	case ForceRay::STATE_DISABLE:
		color = Ogre::ColourValue::Black;
		m_raypos_particlesNode->setVisible(false);
		m_rayneg_particlesNode->setVisible(false);
		m_raynegstart_particlesNode->setVisible(false);
		m_raynegfinal_particlesNode->setVisible(false);
		m_rayposstart_particlesNode->setVisible(false);
		m_rayposfinal_particlesNode->setVisible(false);
		m_raypointer_particlesNode->setVisible(false);
		break;
	case ForceRay::STATE_NEUTRAL:
		color = Ogre::ColourValue::White;
		m_raypos_particlesNode->setVisible(false);	
		m_rayneg_particlesNode->setVisible(false);
		m_raynegstart_particlesNode->setVisible(false);
		m_raynegfinal_particlesNode->setVisible(false);

		m_rayposstart_particlesNode->setVisible(false);
		m_rayposfinal_particlesNode->setVisible(false);
		m_raypointer_particlesNode->setVisible(true);
		break;
	case ForceRay::STATE_POSITIVE:
		color = Ogre::ColourValue::Blue;
		m_raypos_particlesNode->setVisible(true);
		m_rayposstart_particlesNode->setVisible(true);
		if (m_has_hit)
			m_rayposfinal_particlesNode->setVisible(true);
		else
			m_rayposfinal_particlesNode->setVisible(false);

		m_rayneg_particlesNode->setVisible(false);
		m_raynegstart_particlesNode->setVisible(false);
		m_raynegfinal_particlesNode->setVisible(false);
		m_raypointer_particlesNode->setVisible(false);
		break;
	case ForceRay::STATE_NEGATIVE:
		color = Ogre::ColourValue::Red;
		m_rayneg_particlesNode->setVisible(true);
		m_raynegstart_particlesNode->setVisible(true);
		if (m_has_hit)
			m_raynegfinal_particlesNode->setVisible(true);
		else
			m_raynegfinal_particlesNode->setVisible(false);

		m_raypos_particlesNode->setVisible(false);
		m_rayposstart_particlesNode->setVisible(false);
		m_rayposfinal_particlesNode->setVisible(false);
		m_raypointer_particlesNode->setVisible(false);
		break;
	default:
		break;
	}
	m_has_hit = false;

	//Recalculo el time to live de les particules del raig.
	//RAIG GRAN
	Ogre::ParticleSystem* ps = static_cast<Ogre::ParticleSystem*>(m_rayneg_particlesNode->getAttachedObject(0));
	Ogre::ParticleEmitter* emitter = ps->getEmitter(0);
	//std::cout << "time: " << m_limited_direction.length() / emitter->getParticleVelocity() << "x: " << m_limited_direction.x << " y: " << m_limited_direction.y << std::endl;
	emitter->setTimeToLive(m_radius / emitter->getParticleVelocity());
	emitter->setMinTimeToLive(m_radius / emitter->getParticleVelocity());
	emitter->setMaxTimeToLive(m_radius / emitter->getParticleVelocity());
	m_rayneg_particlesNode->setPosition(m_graphical_origin.to_OgreVector3());
	m_rayneg_particlesNode->_setDerivedOrientation(Ogre::Quaternion{ Ogre::Radian(-std::atan2(m_direction.x, m_direction.y)), { 0.f, 0.f, 1.f } });

	ps = static_cast<Ogre::ParticleSystem*>(m_raypos_particlesNode->getAttachedObject(0));
	emitter = ps->getEmitter(0);
	
	emitter->setTimeToLive(m_radius / emitter->getParticleVelocity());
	emitter->setMinTimeToLive(m_radius / emitter->getParticleVelocity());
	emitter->setMaxTimeToLive(m_radius / emitter->getParticleVelocity());
	m_raypos_particlesNode->setPosition(m_graphical_origin.to_OgreVector3());
	m_raypos_particlesNode->_setDerivedOrientation(Ogre::Quaternion{ Ogre::Radian(-std::atan2(m_direction.x, m_direction.y)), { 0.f, 0.f, 1.f } });

	//RAIG INICI DEL RAIG
	m_raynegstart_particlesNode->setPosition(m_graphical_origin.to_OgreVector3());
	m_raynegstart_particlesNode->_setDerivedOrientation(Ogre::Quaternion{ Ogre::Radian(-std::atan2(m_direction.x, m_direction.y)), { 0.f, 0.f, 1.f } });
	m_rayposstart_particlesNode->setPosition(m_graphical_origin.to_OgreVector3());
	m_rayposstart_particlesNode->_setDerivedOrientation(Ogre::Quaternion{ Ogre::Radian(-std::atan2(m_direction.x, m_direction.y)), { 0.f, 0.f, 1.f } });
	
	m_raypointer_particlesNode->setPosition(m_graphical_origin.to_OgreVector3());
	m_raypointer_particlesNode->_setDerivedOrientation(Ogre::Quaternion{ Ogre::Radian(-std::atan2(m_direction.x, m_direction.y)), { 0.f, 0.f, 1.f } });


	//RAIG FINAL DEL RAIG
	m_raynegfinal_particlesNode->setPosition(m_hit_worldposition.to_OgreVector3());
	m_raynegfinal_particlesNode->_setDerivedOrientation(Ogre::Quaternion{ Ogre::Radian(-std::atan2(m_direction.x, m_direction.y)), { 0.f, 0.f, 1.f } });
	m_rayposfinal_particlesNode->setPosition(m_hit_worldposition.to_OgreVector3());
	m_rayposfinal_particlesNode->_setDerivedOrientation(Ogre::Quaternion{ Ogre::Radian(-std::atan2(m_direction.x, m_direction.y)), { 0.f, 0.f, 1.f } });


	// update effects
	if ((color == Ogre::ColourValue::Red || color == Ogre::ColourValue::Blue) && m_effects.size() < MAX_EFFECTS)
	{
		// #todo: duracio configurable
		const f64 duration = 0.3;
		GameEntity* gameEntity = getParent()->getParent();

		/*m_effects.push_back(BeamEffectData{ 
			CCoreEngine::Instance().GetTimerManager().CreateTimer(duration),
			BeamEffectPtr{ new BeamEffect{ { gameEntity->getName() + "_beam_effect" + std::to_string(m_effectIdGenerator.nextId()) }, m_limited_direction, duration, color, m_centerpos.to_OgreVector3(), gameEntity->getWorldOrientation() } }
		});*/
	}
	
	// #debug
	else
	{
		//CRenderDebug::getSingleton().drawLine(m_graphical_origin.to_OgreVector3(), (m_graphical_origin + m_limited_direction).to_OgreVector3(), Ogre::ColourValue::White, 0.5f);
	}
	//--//

	auto& timeMgr = CCoreEngine::Instance().GetTimerManager();

	for (auto it = m_effects.begin(); it != m_effects.end(); )
	{
		const BeamEffectData& beamData = (*it);

		if (timeMgr.IsEndTimer(beamData.first))
		{
			timeMgr.KillTimer(beamData.first);
			it = m_effects.erase(it);
		}
		else
		{
			beamData.second->update(dt);
			it++;
		}
	}

	if (m_oldColor != color)
	{
		const bool InShooting{ (m_oldColor == Ogre::ColourValue::Red || m_oldColor == Ogre::ColourValue::Blue) };
		const bool ToShooting{ (color == Ogre::ColourValue::Red || color == Ogre::ColourValue::Blue) };

		if (InShooting && !ToShooting)
		{
			CCoreEngine::Instance().GetSoundManager().stopSound(m_soundChannelId);
		}
		else if (!InShooting && ToShooting)
		{
			m_soundChannelId = CCoreEngine::Instance().GetSoundManager().playSound(m_rayConfig.sound);
		}
	}

	m_oldColor = color;
}

edv::Vector3f ForceRay::getDirection() const
{
	auto dir = m_direction;
	dir.normalise();

	return dir;
}

edv::Vector3f ForceRay::getScope() const
{
	return m_direction;
}

void ForceRay::setScope(const edv::Vector3f& a_scope)
{
	m_direction = a_scope;
}

edv::Vector3f ForceRay::getLimitedDirection() const
{
	return m_limited_direction;
}

void ForceRay::setState(eStates a_state)
{
	m_state = a_state;
}

void ForceRay::setRadius(f32 a_radius)
{
	m_radius = a_radius;
}

f32 ForceRay::getAngleFromAxis(const edv::Vector3f& a_axis)
{
	edv::Vector3f ray_vector{ getDirection() };
	edv::Vector3f other_vector{ a_axis };
	f32 dotProduct = other_vector.dotProduct(ray_vector);
	f32 angle = std::acos(dotProduct / (ray_vector.length()*other_vector.length()));
	
	return angle;
}

void ForceRay::init()
{
	
	GameEntity* parentEntity = getParent()->getParent();

	//Particules del raig gran pos i neg.
	m_raypos_particlesNode = parentEntity->getGraphics()->getParticleSystemNode(parentEntity->getName() + m_rayConfig.raypos_fx);
	m_rayneg_particlesNode = parentEntity->getGraphics()->getParticleSystemNode(parentEntity->getName() + m_rayConfig.rayneg_fx);
	m_rayposstart_particlesNode = parentEntity->getGraphics()->getParticleSystemNode(parentEntity->getName() + m_rayConfig.rayposstart_fx);
	m_raynegstart_particlesNode = parentEntity->getGraphics()->getParticleSystemNode(parentEntity->getName() + m_rayConfig.raynegstart_fx);
	m_rayposfinal_particlesNode = parentEntity->getGraphics()->getParticleSystemNode(parentEntity->getName() + m_rayConfig.rayposfinal_fx);
	m_raynegfinal_particlesNode = parentEntity->getGraphics()->getParticleSystemNode(parentEntity->getName() + m_rayConfig.raynegfinal_fx);
	m_raypointer_particlesNode	= parentEntity->getGraphics()->getParticleSystemNode(parentEntity->getName() + m_rayConfig.raypointer_fx);

	if (m_raypos_particlesNode) m_raypos_particlesNode->setVisible(false);
	if (m_rayneg_particlesNode) m_rayneg_particlesNode->setVisible(false);
	if (m_rayposstart_particlesNode) m_rayposstart_particlesNode->setVisible(false);
	if (m_raynegstart_particlesNode) m_raynegstart_particlesNode->setVisible(false);
	if (m_rayposfinal_particlesNode) m_rayposfinal_particlesNode->setVisible(false);
	if (m_raynegfinal_particlesNode) m_raynegfinal_particlesNode->setVisible(false);
	if (m_raypointer_particlesNode) m_raypointer_particlesNode->setVisible(false);
	
	// initialize ray particles
	std::string particleNodeName = parentEntity->getName() + m_rayConfig.ray_fx;
	Ogre::SceneNode* particleRaySystemNode = parentEntity->getGraphics()->getParticleSystemNode(particleNodeName);
	particleNodeName = parentEntity->getName() + m_rayConfig.pos_fx;
	Ogre::SceneNode* particlePosFxSystemNode = parentEntity->getGraphics()->getParticleSystemNode(particleNodeName);
	particleNodeName = parentEntity->getName() + m_rayConfig.neg_fx;
	Ogre::SceneNode* particleNegFxSystemNode = parentEntity->getGraphics()->getParticleSystemNode(particleNodeName);


	// configure particles
	if (particleRaySystemNode)
	{
		m_ray_particlesNode = particleRaySystemNode;
		m_ray_particlesNode->setVisible(true);
	}

	if (particlePosFxSystemNode)
	{
		m_posfx_particlesNode = particlePosFxSystemNode;
		m_posfx_particlesNode->setVisible(false);
	}

	if (particleNegFxSystemNode)
	{
		m_negfx_particlesNode = particleNegFxSystemNode;
		m_negfx_particlesNode->setVisible(false);
	}

	// configure effects
	m_effects.clear();

	// #todo: configurable
	// sound
	CCoreEngine::Instance().GetSoundManager().preloadSound(m_rayConfig.sound);
}

void ForceRay::stop()
{
	CCoreEngine::Instance().GetSoundManager().stopSound(m_soundChannelId);
	CCoreEngine::Instance().GetSoundManager().releaseSound(m_rayConfig.sound);

	auto& timeMgr = CCoreEngine::Instance().GetTimerManager();

	for (auto& effectData : m_effects)
	{
		timeMgr.KillTimer(effectData.first);
	}

	if (m_ray_particlesNode)
	{
		m_ray_particlesNode->setVisible(false);
		m_ray_particlesNode = nullptr;
	}

	if (m_posfx_particlesNode)
	{
		m_posfx_particlesNode->setVisible(false);
		m_posfx_particlesNode = nullptr;
	}

	if (m_negfx_particlesNode)
	{
		m_negfx_particlesNode->setVisible(false);
		m_negfx_particlesNode = nullptr;
	}

	if (m_rayneg_particlesNode)
	{
		m_rayneg_particlesNode->setVisible(false);
		m_rayneg_particlesNode = nullptr;
	}

	if (m_raypos_particlesNode)
	{
		m_raypos_particlesNode->setVisible(false);
		m_raypos_particlesNode = nullptr;
	}

	if (m_rayposstart_particlesNode)
	{
		m_rayposstart_particlesNode->setVisible(false);
		m_rayposstart_particlesNode = nullptr;
	}
	if (m_raynegstart_particlesNode)
	{
		m_raynegstart_particlesNode->setVisible(false);
		m_raynegstart_particlesNode = nullptr;
	}

	if (m_rayposfinal_particlesNode)
	{
		m_rayposfinal_particlesNode->setVisible(false);
		m_rayposfinal_particlesNode = nullptr;
	}
	if (m_raynegfinal_particlesNode)
	{
		m_raynegfinal_particlesNode->setVisible(false);
		m_raynegfinal_particlesNode = nullptr;
	}

	if (m_raypointer_particlesNode)
	{
		m_raypointer_particlesNode->setVisible(false);
		m_raypointer_particlesNode = nullptr;
	}

	m_effects.clear();
	m_effectIdGenerator.reset();
}

void ForceRay::clear()
{
}

bool ForceRay::shoot(MagneticRayInfo& a_magnetic_ray_info)
{
	a_magnetic_ray_info.clear();
	GameEntity* const parentEntity = getParent()->getParent();
	edv::Vector3f logicalOrig(m_logical_origin);
	edv::Vector3f graphicalOrig(m_graphical_origin);
	edv::Vector3f dest((logicalOrig + m_direction).x, (logicalOrig + m_direction).y, parentEntity->getWorldPosition().z);
	btClosestNotMeRayResultCallback callback{ parentEntity, logicalOrig.to_btVector3(), dest.to_btVector3(), EntityBtPhysics::EdvCollisionFlags::CF_TRIGGER | btCollisionObject::CF_NO_CONTACT_RESPONSE };
	GameEntity* target_entity = static_cast<GameEntity*>(CCoreEngine::Instance().GetPhysicsManager().RayTestNotMe(logicalOrig, dest, callback));
	edv::Vector3f newDirection{ dest - graphicalOrig };

	const bool hit = callback.hasHit();
	m_has_hit = hit;
	if (hit)
	{
		m_hit_worldposition = edv::Vector3f(callback.m_hitPointWorld);
		newDirection = { (edv::Vector3f(callback.m_hitPointWorld) - graphicalOrig) };
		a_magnetic_ray_info.originEntity = parentEntity;
		a_magnetic_ray_info.targetEntity = target_entity;
		a_magnetic_ray_info.originPos = graphicalOrig;
		a_magnetic_ray_info.direction = newDirection;
		a_magnetic_ray_info.worldPoint = callback.m_hitPointWorld;
		a_magnetic_ray_info.charge = (target_entity->getPhysics()->getMagneticProperty()) ? target_entity->getPhysics()->getMagneticProperty()->getCharge() : MagneticProperty::eMagneticCharge::MAGNET_TYPE_NEUTRAL;
	}

	if (hit)
	{
		// apply particles!
		MagneticProperty::eMagneticCharge  charge = a_magnetic_ray_info.charge;
		if (charge == MagneticProperty::eMagneticCharge::MAGNET_TYPE_POSITIVE || charge == MagneticProperty::eMagneticCharge::MAGNET_TYPE_NEGATIVE)
		{
			startParticlesShootFx(a_magnetic_ray_info);
		}
		else
		{
			stopParticlesShootFx(m_posfx_particlesNode);
			stopParticlesShootFx(m_negfx_particlesNode);
		}
	}
	else
	{
		stopParticlesShootFx(m_posfx_particlesNode);
		stopParticlesShootFx(m_negfx_particlesNode);
	}

	m_limited_direction = newDirection;
	return hit;
}

void ForceRay::startParticlesShootFx(const MagneticRayInfo& a_magnetic_ray_info)
{
	MagneticProperty::eMagneticCharge  charge{ a_magnetic_ray_info.charge };
	Ogre::SceneNode* particles_node = nullptr;
	if (charge == MagneticProperty::eMagneticCharge::MAGNET_TYPE_POSITIVE)
	{
		particles_node = m_posfx_particlesNode;
		stopParticlesShootFx(m_negfx_particlesNode);
	}
	else if (charge == MagneticProperty::eMagneticCharge::MAGNET_TYPE_NEGATIVE)
	{
		particles_node = m_negfx_particlesNode;
		stopParticlesShootFx(m_posfx_particlesNode);
	}

	if (particles_node)
	{
		if (Ogre::ParticleSystem* ps = static_cast<Ogre::ParticleSystem*>(particles_node->getAttachedObject(0)))
		{
			const u32 numEmitters = ps->getNumEmitters();
			ps->setEmitting(true);

			for (u32 emitterIdx = 0; emitterIdx < numEmitters; emitterIdx++)
			{
				if (Ogre::ParticleEmitter* const emitter = ps->getEmitter(emitterIdx))
				{
					Ogre::Vector3 direction{ -a_magnetic_ray_info.direction.to_OgreVector3() };
					emitter->setDirection(direction);
				}
			}
		}
		particles_node->_setDerivedPosition(a_magnetic_ray_info.worldPoint.to_OgreVector3());
		particles_node->setVisible(true);
	}
}

void ForceRay::stopParticlesShootFx(Ogre::SceneNode* a_particles_node)
{
	Ogre::SceneNode* particles_node = a_particles_node;
	if (particles_node)
	{
		particles_node->setVisible(false);
		if (Ogre::ParticleSystem* ps = static_cast<Ogre::ParticleSystem*>(particles_node->getAttachedObject(0)))
		{
			ps->setEmitting(false);
		}
	}
}