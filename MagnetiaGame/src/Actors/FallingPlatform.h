#ifndef MAGNETIA_FALLING_PLATFORM_H_
#define MAGNETIA_FALLING_PLATFORM_H_

#include "MagnetiaActor.h"

class FallingPlatform : public MagnetiaActor
{
public:
	class Factory : public Actor::factory_type
	{
	public:
		Actor* createElement(const Actor::factory_param_type& a_config) override;
	};

	FallingPlatform(const ScriptConfig& a_scriptConfig = {}, f64 a_timeInActive = 0.f, f64 a_timeInFalling = 0.f, f64 a_timeInInactive = 0.f, f32 a_fallingMass = 0.f);
	void playFalling();
	f64 getTimeInActive() const;
	f64 getTimeInFalling() const;
	f64 getTimeInInactive() const;

protected:
	void initImpl() override;
	void stopImpl() override;

private:
	f64 m_timeInActive;
	f64 m_timeInFalling;
	f64 m_timeInInactive;
	f32	m_fallingMass;
};

#endif
