#ifndef MAGNETIA_GAME_TRIGGER_GAME_END_H_
#define MAGNETIA_GAME_TRIGGER_GAME_END_H_

#include <memory>
#include "MagnetiaActor.h"
#include "Fader.h"
#include "Panel.h"

class TriggerGameEnd : public MagnetiaActor, public FaderCallback
{
public:
	// Factory
	class Factory : public Actor::factory_type
	{
	public:
		Actor* createElement(const Actor::factory_param_type& a_config) override;
	};

	enum Substate
	{
		SUBSTATE_WAITING,
		SUBSTATE_FADE_OFF,
		SUBSTATE_STORY
	};

	TriggerGameEnd(s32 a_maxLife = 0, const ScriptConfig& a_scriptConfig = {});
	~TriggerGameEnd();
	Actor* clone() override;
	void fadeInCallback() override;
	void fadeOutCallback() override;

	Substate getSubstate() const;
	void startStory();

private:
	typedef std::unique_ptr<Fader>	FaderPtr;
	typedef std::unique_ptr<Panel>	PanelPtr;

	void updateImpl(f64 dt) override;
	void updateStory(f64 dt);

	Substate		m_currentSubstate;
	FaderPtr		m_fader;
	PanelPtr		m_panel;
	Ogre::Overlay*	m_overlay;
};

#endif
