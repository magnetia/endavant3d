#include <Game/GameEntity.h>
#include <Game/FlyStraightAnimator.h>
#include <Game/AlphaTransitionAnimator.h>
#include <Game/NullAnimator.h>
#include <Game/ChainedBehavior.h>
#include "Boss.h"
#include "BossTransitionState.h"

BossTransitionState::BossTransitionState(Boss* a_boss) :
	BossState{ a_boss },
	m_movementAnimator{ nullptr },
	m_alphaAnimator{ nullptr }
{

}

void BossTransitionState::start()
{
	if (GameEntity* const entity = m_boss->getParent())
	{
		m_movementAnimator = new FlyStraightAnimator{ 3.5f, entity->getWorldPosition(), m_boss->getNextStartPoint() };
		m_alphaAnimator = new ChainedBehavior{
			new AlphaTransitionAnimator{ 1, 1.f, 0.1f },
			new NullAnimator{ 1.5f },
			new AlphaTransitionAnimator{ 1, 0.1f, 1 }
		};

		entity->addBehavior(m_movementAnimator);
		entity->addBehavior(m_alphaAnimator);
	}
}

void BossTransitionState::update(f64 dt)
{
	GameEntity* const entity{ m_boss->getParent() };

	if (m_movementAnimator && m_movementAnimator->finished())
	{
		entity->removeBehavior(m_movementAnimator);
		m_movementAnimator = nullptr;

		if (m_alphaAnimator)
		{
			m_alphaAnimator->stop();
			entity->removeBehavior(m_alphaAnimator);
			m_alphaAnimator = nullptr;
		}
	}
}

void BossTransitionState::stop()
{
	GameEntity* const entity{ m_boss->getParent() };

	if (m_movementAnimator)
	{
		m_movementAnimator->stop();
		entity->removeBehavior(m_movementAnimator);
		m_movementAnimator = nullptr;
	}

	if (m_alphaAnimator)
	{
		m_alphaAnimator->stop();
		entity->removeBehavior(m_alphaAnimator);
		m_alphaAnimator = nullptr;
	}
}

bool BossTransitionState::finished()
{
	return m_movementAnimator ? m_movementAnimator->finished() : true;
}

edv::Vector3f BossTransitionState::getStartPoint() const
{
	return {};
}
