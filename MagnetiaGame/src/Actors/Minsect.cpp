#include <Core/CCoreEngine.h>
#include "Minsect.h"
#include <Physics/CPhysicsManager.h>
#include <Physics/COgreMotionState.h>
#include <Game/GameManager.h>
#include <Game/GameEntity.h>
#include <Game/AlphaTransitionAnimator.h>
#include <Game/FlyStraightAnimator.h>
#include <Game/RotationAnimator.h>
#include <Game/ParticleSystemAnimator.h>
#include <Game/DistanceSound.h>
#include "ChopEffect.h"

// #debug
#include <Renderer/CRenderDebug.h>

Actor* Minsect::Factory::createElement(const Actor::factory_param_type& a_config)
{
	return new Minsect{ 
		a_config["max_life"].as<s32>(), 
		ScriptConfig{ a_config["script/path"], a_config["script/scope"] },
		a_config["sound_radius"]
	};
}

Minsect::Minsect(s32 a_maxLife, const ScriptConfig& a_scriptConfig, f32 a_soundRadius) :
	MagnetiaActor{ a_maxLife, a_scriptConfig },
	m_dying{ false },
	m_death_effect{ },
	m_soundRadius{ a_soundRadius }
{
	CCoreEngine::Instance().GetSoundManager().preloadSound("minsect_death");
}

Minsect::~Minsect()
{
	CCoreEngine::Instance().GetSoundManager().releaseSound("minsect_death");
}

Actor* Minsect::clone()
{
	return new Minsect{ getMaxLife(), getScriptConfig(), m_soundRadius };
}

void Minsect::show(f32 a_duration)
{
	getParent()->addBehavior(new AlphaTransitionAnimator{ a_duration });
	getParent()->addBehavior(new DistanceSound{ "minsect_buzz", m_soundRadius });
}

void Minsect::attack(f32 a_duration)
{	
	const edv::Vector3f playerPosition = CCoreEngine::Instance().GetGameManager().getPlayer()->getWorldPosition();
	const edv::Vector3f minsectPosition = getParent()->getWorldPosition();

	// movement
	edv::Vector3f dir = playerPosition - getParent()->getWorldPosition();
	f32 length = dir.normalise();
	edv::Vector3f p2 = getParent()->getWorldPosition() + (dir * (length * 2));

	getParent()->addBehavior(new FlyStraightAnimator{ 
		a_duration, 
		getParent()->getWorldPosition().to_OgreVector3(), 
		p2.to_OgreVector3()
	});

	// rotation
	Ogre::Degree rotation{ 0 };

	if (playerPosition.x < minsectPosition.x)
	{
		rotation = -90;
	}
	else if (playerPosition.x > minsectPosition.x)
	{
		rotation = 90;
	}

	if (rotation != Ogre::Degree{ 0 })
	{
		getParent()->addBehavior(new RotationAnimator{ a_duration / 4, rotation, Ogre::Vector3{ 0.f, 1.f, 0.f } });
	}
}

void Minsect::dieAnimation(f32 a_duration)
{
	if (!m_dying)
	{
		MagnetiaActor::stop();
		getParent()->removeAllBehaviors();
		const std::string particleNodeName = "Particle/MinsectExplosion";
		getParent()->addBehavior(new ParticleSystemAnimator{ getParent()->getName() + "death_particles_minsect", a_duration, particleNodeName });
		getParent()->getGraphics()->getNode()->setVisible(false);
		getParent()->getPhysics()->clear();
		m_death_effect.reset(new ChopEffect{ getParent()->getWorldPosition(), { "Death_Minsect_Fragment", "Death_Minsect_Fragment2" }, 4.f, {0.f, -1.5f, 0.f} });
		CCoreEngine::Instance().GetSoundManager().playSound("minsect_death");

		m_dying = true;
	}
}
