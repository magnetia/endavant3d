#ifndef MAGNETIA_GAME_BOSS_SCAPE_STATE_H_
#define MAGNETIA_GAME_BOSS_SCAPE_STATE_H_

#include <memory>
#include <Utils/edvVector3.h>
#include <Utils/generic.hpp>
#include "Mine.h"
#include "BossState.h"

class BossEscapeState : public BossState
{
public:
	BossEscapeState(
		Boss* a_boss,
		const std::string& a_pathName,
		const edv::range64f& a_acceleration,
		const edv::range64f& a_mineDropTime,
		f64 a_accelRadius);
	~BossEscapeState();

	void start() override;
	void update(f64 dt) override;
	void stop() override;
	bool finished() override;
	edv::Vector3f getStartPoint() const override;

private:
	typedef std::vector<edv::Vector3f>	PathPoints;
	typedef std::vector<GameEntity*>	Mines;

	void advance(f64 dt);
	void updateAcceleration();
	void updateMines(f64 dt);

	edv::range64f				m_acceleration;
	edv::range64f				m_mineDropTime;
	f64							m_accelRadius;
	f64							m_currentAcc;
	PathPoints					m_path;
	PathPoints::const_iterator	m_currentPoint;
	edv::Vector3f				m_currentOffset;
	edv::Vector3f				m_linearVelocity;
	f64							m_mineCounter;
	Mines						m_mines;
};

#endif
