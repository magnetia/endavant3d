#ifndef MAGNETIA_CHOP_EFFECT_H_
#define MAGNETIA_CHOP_EFFECT_H_

#include <Core/CBasicTypes.h>
#include <vector>
#include <Utils/edvVector3.h>

class GameEntity;

class ChopEffect
{
public:
	typedef std::vector<std::string> tEntityIds;
	ChopEffect(const edv::Vector3f& a_origin, const tEntityIds& a_entitiesIds, u32 a_count, f32 a_force, const edv::Vector3f& a_gravity = { 0.f, -9.8f, 0.f});
	ChopEffect(const edv::Vector3f& a_origin, const tEntityIds& a_entitiesIds, f32 a_force, const edv::Vector3f& a_gravity = { 0.f, -9.8f, 0.f }, const Ogre::Quaternion& a_rotation = Ogre::Quaternion::IDENTITY);
	~ChopEffect();

private:
	typedef std::vector<GameEntity*> tEntityVector;
	tEntityVector m_entities;
};

#endif
