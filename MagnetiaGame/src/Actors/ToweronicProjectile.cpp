#include <Game/ParticleSystemAnimator.h>
#include "Toweronic.h"
#include "ToweronicProjectile.h"

Actor* ToweronicProjectile::Factory::createElement(const Actor::factory_param_type& a_config)
{
	return new ToweronicProjectile{{ a_config["script/path"], a_config["script/scope"] }, a_config["speed"]};
}

ToweronicProjectile::ToweronicProjectile(const ScriptConfig& a_scriptConfig, f32 a_speed) :
	MagnetiaActor{ 0, a_scriptConfig },
	m_speed{ a_speed },
	m_vector{ },
	m_toweronic{ nullptr },
	m_charge{ MagneticProperty::MAGNET_TYPE_NEUTRAL },
	m_currentScaleAnim{ nullptr }
{

}

Actor* ToweronicProjectile::clone()
{
	return new ToweronicProjectile{ getScriptConfig(), m_speed };
}

void ToweronicProjectile::configure(Toweronic* a_toweronic, const edv::Vector3f& a_vector, MagneticProperty::eMagneticCharge a_charge)
{
	m_toweronic = a_toweronic;
	m_vector = a_vector;
	m_charge = a_charge;
}

void ToweronicProjectile::destroy()
{
	if (m_toweronic)
	{
		m_toweronic->removeProjectile(this);
	}
}

GameEntity* ToweronicProjectile::getToweronicEntity() const
{
	GameEntity* entity = nullptr;

	if (m_toweronic)
	{
		entity = m_toweronic->getParent();
	}

	return entity;
}

void ToweronicProjectile::setInactive(f32 a_animTime)
{
	getParent()->getPhysics()->clear();

	const std::string particleNodeName = "Particle/ToweronicProjectileExplosion";
	getParent()->addBehavior(new ParticleSystemAnimator{ getParent()->getName() + "death_particles_toweronic_projectile", a_animTime, particleNodeName });
	getParent()->getGraphics()->getNode()->setVisible(false);
}

void ToweronicProjectile::grow(f32 a_increment, f32 a_time)
{
	// remove old scaling options
	if (m_currentScaleAnim)
	{
		getParent()->removeBehavior(m_currentScaleAnim);
		m_currentScaleAnim = nullptr;
	}

	// add new scaling options
	edv::Vector3f finalScale = getParent()->getScale();
	finalScale *= a_increment;
	
	m_currentScaleAnim = new ScaleAnimator{ a_time, getParent()->getScale(), finalScale };
	getParent()->addBehavior(m_currentScaleAnim);
}

MagneticProperty::eMagneticCharge ToweronicProjectile::getCharge() const
{
	return m_charge;
}

void ToweronicProjectile::initImpl()
{
	MagnetiaActor::initImpl();
}

void ToweronicProjectile::updateImpl(f64 dt)
{
	edv::Vector3f projectilePos = getParent()->getWorldPosition();
	projectilePos += m_vector * m_speed * static_cast<f32>(dt);

	getParent()->setWorldPosition(projectilePos);
}
