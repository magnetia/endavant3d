#ifndef MAGNETIA_GAME_MISSILE_H_
#define MAGNETIA_GAME_MISSILE_H_

#include "MagnetiaActor.h"

class TimedBehavior;

class Missile : public MagnetiaActor
{
public:
	// Factory
	class Factory : public Actor::factory_type
	{
	public:
		Actor* createElement(const Actor::factory_param_type& a_config) override;
	};

	Missile(s32 a_maxLife = 0, const ScriptConfig& a_scriptConfig = {}, const edv::range64f& a_duration = {}, const edv::Vector3f& a_direction = {});
	~Missile();
	Actor* clone() override;

	void setDuration(const edv::range64f& a_duration);
	void setDirectionVect(const edv::Vector3f& a_direction);
	const edv::range64f& getDuration() const;
	const edv::Vector3f& getDirectionVect() const;
	void setOwner(GameEntity* a_owner);
	void setTarget(GameEntity* a_target);
	bool finished() const;

	void onCollision(const CollisionInfo& a_collisionInfo) override;

private:
	void initImpl() override;
	void updateImpl(f64 dt) override;

	edv::range64f	m_duration;
	edv::Vector3f	m_direction;
	TimedBehavior*	m_currentAnimation;
	GameEntity*		m_owner;
	GameEntity*		m_target;
	bool			m_colliding;
	bool			m_finished;
};

#endif
