#include "FallingPlatform.h"
#include <Game/GameEntity.h>
#include <Game/ChainedBehavior.h>
#include <Game/AlphaTransitionAnimator.h>
#include <Core/CCoreEngine.h>
#include <Game/GameManager.h>

Actor* FallingPlatform::Factory::createElement(const Actor::factory_param_type& a_config)
{
	return new FallingPlatform{
			{ a_config["script/path"], a_config["script/scope"] },
			a_config["time_in_active"],
			a_config["time_in_falling"],
			a_config["time_in_inactive"],
			a_config["falling_mass"]
	};
}


FallingPlatform::FallingPlatform(const ScriptConfig& a_scriptConfig, f64 a_timeInFalling, f64 a_timeInAdvise, f64 a_timeInInactive, f32 a_fallingMass) :
	MagnetiaActor{ 0, a_scriptConfig },
	m_timeInActive{ a_timeInFalling },
	m_timeInFalling{ a_timeInAdvise },
	m_timeInInactive{ a_timeInInactive },
	m_fallingMass{ a_fallingMass }
{
}

void FallingPlatform::playFalling()
{
	auto* phyisics = getParent()->getPhysics();
	const auto collisionShape = phyisics->getCollisionShape();

	phyisics->clear();
	phyisics->init(
		getParent()->getGraphics(),
		EntityBtPhysics::Config{
		m_fallingMass,
		false,
		{},
		true,
		EntityBtPhysics::CollisionConfig{ collisionShape }
		}
	);
}

f64 FallingPlatform::getTimeInActive() const
{
	return m_timeInActive;
}

f64 FallingPlatform::getTimeInFalling() const
{
	return m_timeInFalling;
}

f64 FallingPlatform::getTimeInInactive() const
{
	return m_timeInInactive;
}

void FallingPlatform::initImpl()
{
	getParent()->getGraphics()->getNode()->setVisible(true);
	getParent()->getPhysics()->setCollisionEnabled(true);
}

void FallingPlatform::stopImpl()
{
	auto* phyisics = getParent()->getPhysics();
	const auto collisionShape = phyisics->getCollisionShape();

	phyisics->clear();

	phyisics->init(
		getParent()->getGraphics(),
		EntityBtPhysics::Config{
		0.f,
		false,
		{},
		true,
		EntityBtPhysics::CollisionConfig{ collisionShape }
		}
	);

	getParent()->getGraphics()->getNode()->setVisible(false);
	getParent()->getPhysics()->setCollisionEnabled(false);
}