#include <limits>
#include <Core/CCoreEngine.h>
#include <Game/GameManager.h>
#include "GameLevel.h"
#include "Boss.h"
#include "BossEscapeState.h"

BossEscapeState::BossEscapeState(
		Boss* a_boss, const std::string& a_pathName, 
		const edv::range64f& a_acceleration, 
		const edv::range64f& a_mineDropTime, 
		f64 a_accelRadius) :
	BossState{ a_boss },
	m_acceleration{ a_acceleration },
	m_mineDropTime{ a_mineDropTime },
	m_accelRadius{ a_accelRadius },
	m_currentAcc{ m_acceleration.min },
	m_path{},
	m_currentPoint{},
	m_currentOffset{},
	m_mineCounter{},
	m_mines{}
{
	if (GameLevel* const level = static_cast<GameLevel*>(CCoreEngine::Instance().GetGameManager().getCurrentLevel()))
	{
		m_path = level->getBossPathPoints(a_pathName);
	}
}

BossEscapeState::~BossEscapeState()
{
	GameManager& gameManager = CCoreEngine::Instance().GetGameManager();

	for (GameEntity* const entity : m_mines)
	{
		gameManager.destroyEntity(entity->getName());
	}
}

void BossEscapeState::start()
{
	m_linearVelocity.setZero();
	m_mineCounter = m_mineDropTime.random();
	m_currentPoint = m_path.begin();

	if (!m_path.empty())
	{
		m_boss->getParent()->setWorldPosition((*m_currentPoint));
	}
}

void BossEscapeState::update(f64 dt)
{
	updateMines(dt);
	updateAcceleration();
	advance(dt);	
}

void BossEscapeState::stop()
{
	GameManager& gameManager{ CCoreEngine::Instance().GetGameManager() };

	// update existing mines
	for (Mines::iterator it = m_mines.begin(); it != m_mines.end(); it++)
	{
		GameEntity* const entity = (*it);
		gameManager.destroyEntity(entity);
	}

	m_mines.clear();
}

bool BossEscapeState::finished()
{
	return m_currentPoint == m_path.end() || (m_currentPoint + 1) == m_path.end();
}

edv::Vector3f BossEscapeState::getStartPoint() const
{
	return m_path.empty() ? edv::Vector3f{} : (*m_path.begin());
}

void BossEscapeState::advance(f64 dt)
{
	const edv::Vector3f currPosition{ m_boss->getParent()->getWorldPosition() };
	edv::Vector3f nextPosition{ currPosition };
	f32 totalDistance{ static_cast<f32>(m_currentAcc * dt) };

	while (!finished() && totalDistance != 0)
	{
		const edv::Vector3f& pointA{ *m_currentPoint };
		const edv::Vector3f& pointB{ *(m_currentPoint + 1) };
		edv::Vector3f currentDir{ pointB - currPosition };

		currentDir.normalise();
		m_linearVelocity += totalDistance * currentDir.to_btVector3();
		m_linearVelocity *= m_boss->getParent()->getPhysics()->getRigidBody()->getLinearFactor();

		btTransform from_transform, to_transform;
		to_transform.setIdentity(); from_transform.setIdentity();

		from_transform.setOrigin(nextPosition.to_btVector3());

		const f32 delta_time{ static_cast<f32>(dt) };
		const btVector3 ang_velocity{ 0.f, 0.f, 0.f };
		btTransformUtil::integrateTransform(from_transform, m_linearVelocity.to_btVector3(), ang_velocity, delta_time, to_transform);
		const btVector3& targetPosition = to_transform.getOrigin();

		const f32 distanceMove{ btVector3{ targetPosition - nextPosition.to_btVector3() }.length() };
		const f32 distanceMax{ edv::Vector3f{ pointB - nextPosition }.length() };

		if (distanceMove < distanceMax)
		{
			nextPosition = targetPosition;
			totalDistance = 0;
		}
		else
		{
			nextPosition = pointB;
			totalDistance -= distanceMax;
			m_currentPoint++;

			if (!finished())
			{
				const edv::Vector3f& pointA{ *m_currentPoint };
				const edv::Vector3f& pointB{ *(m_currentPoint + 1) };
				edv::Vector3f nextDir{ pointB - pointA };

				nextDir.normalise();
				m_linearVelocity = m_linearVelocity.length() * nextDir;
			}
		}
	}

	if (!m_linearVelocity.isZero() && m_linearVelocity.squaredLength() < 0.1f)
	{
		m_linearVelocity.setZero();
	}

	m_boss->getParent()->setWorldPosition(nextPosition);
}

void BossEscapeState::updateAcceleration()
{
	if (GameEntity* const player = CCoreEngine::Instance().GetGameManager().getPlayer())
	{
		const f32 distance = m_boss->getParent()->distanceToEntity(player);

		if (distance <= m_accelRadius)
		{
			m_currentAcc = ((distance / m_accelRadius) * (m_acceleration.max - m_acceleration.min)) + m_acceleration.min;
		}
		else
		{
			m_currentAcc = m_acceleration.min;
		}
	}
}

void BossEscapeState::updateMines(f64 dt)
{
	GameManager& gameManager{ CCoreEngine::Instance().GetGameManager() };

	// update existing mines
	for (Mines::iterator it = m_mines.begin(); it != m_mines.end(); )
	{
		GameEntity* const entity = (*it);

		entity->update(dt);

		if (static_cast<Mine*>(entity->getActor())->finished())
		{
			gameManager.destroyEntity(entity);
			it = m_mines.erase(it);
		}
		else
		{
			it++;
		}
	}

	// test if a new mine must be dropped
	m_mineCounter -= dt;

	if (!finished() && m_mineCounter <= 0.)
	{
		GameManager::EntityInfo info = gameManager.createEntity("Mine", m_boss->getParent()->getWorldPosition().to_OgreVector3());
		GameEntity* const mineEntity{ info.second };
		Mine* const mineActor{ static_cast<Mine*>(mineEntity->getActor()) };

		mineActor->setBossEntity(m_boss->getParent());
		m_mines.push_back(info.second);
		m_mineCounter = m_mineDropTime.random();
	}
}
