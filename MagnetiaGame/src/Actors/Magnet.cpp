#include <Game/ChainedBehavior.h>
#include <Game/GlowAnimator.h>
#include <Game/GameEntity.h>
#include "Magnet.h"

Actor* Magnet::Factory::createElement(const Actor::factory_param_type& a_config)
{
	return new Magnet{ 
		a_config["anim_duration"], 
		a_config["anim_glow_texture"], 
		{ a_config["anim_initial_level/x"], a_config["anim_initial_level/y"], a_config["anim_initial_level/z"] } ,
		{ a_config["anim_final_level/x"], a_config["anim_final_level/y"], a_config["anim_final_level/z"] }
	};
}

Magnet::Magnet(const f64 a_animDuration, const std::string& a_animGlowTexture, const edv::Vector3f& a_animInitialLevel, 
	const edv::Vector3f& a_animFinalLevel) :
	MagnetiaActor{ 0, {} },
	m_animDuration{ a_animDuration },
	m_animGlowTexture{ a_animGlowTexture },
	m_animInitialLevel{ a_animInitialLevel },
	m_animFinalLevel{ a_animFinalLevel }
{

}

Magnet::~Magnet()
{

}

Actor* Magnet::clone()
{
	return new Magnet{ m_animDuration, m_animGlowTexture, m_animInitialLevel, m_animFinalLevel };
}

void Magnet::initImpl()
{
	MagnetiaActor::initImpl();

	// @deprecated
	//const f64 step = m_animDuration / 2;	
	//
	//getParent()->addBehavior(new ChainedBehavior({
	//	new GlowAnimator{ step, m_animGlowTexture, m_animInitialLevel, m_animFinalLevel },
	//	new GlowAnimator{ step, m_animGlowTexture, m_animFinalLevel, m_animInitialLevel }
	//}, -1));
}

void Magnet::updateImpl(f64 dt)
{

}
