#include "SpaceshipController.h"

SpaceshipController::SpaceshipController(const sControllerConfig& a_controller_config) :
m_controller_config{ a_controller_config }
{
	setGravity(m_controller_config.gravity);
	setRestitution(m_controller_config.restitution);
	setSliding(m_controller_config.sliding);
	setFriction(m_controller_config.friction);
}

void SpaceshipController::applyMove(const edv::Vector3f& a_vector)
{
	edv::Vector3f movement{ 0.f, 0.f, 0.f };
	
	// check walls
	if ((a_vector.x > 0 && m_on_right) || (a_vector.x < 0 && m_on_left) || (a_vector.y < 0 && m_on_ground) || (a_vector.y > 0 && m_on_ceiling))
		return;

	// max velocity
	if ((m_linear_velocity.x * movement.x) >= 0.f && !m_linear_velocity.isZero() && m_linear_velocity.length() > m_controller_config.max_velocity)
	{
		return;
	}

	movement = edv::Vector3f{ a_vector.x*m_controller_config.X_acceleration, a_vector.y*m_controller_config.Y_acceleration, a_vector.z };
	
	KinematicController::applyMove(movement);
}

void SpaceshipController::applyForce(const edv::Vector3f& a_vector)
{
	edv::Vector3f movement{ a_vector };
	
	// check walls
	movement.x = ((movement.x > 0 && m_on_right) || (movement.x < 0 && m_on_left)) ? 0.f : movement.x;
	movement.y = ((movement.y < 0 && m_on_ground)) ? 0.f : movement.y;
	
	KinematicController::applyForce(movement);
}

edv::Vector3f SpaceshipController::applyFriction(const edv::Vector3f& a_velocity)
{
	if (!m_movement)
	{
		return KinematicController::applyFriction(a_velocity);
	}
	else
	{
		return edv::Vector3f{ 0.f, 0.f, 0.f };
	}
}

void SpaceshipController::updateVelocityOnCollision(edv::Vector3f& a_velocity, const edv::Vector3f& a_normal_hit, f32 a_tangentFactor, f32 a_normalFactor)
{
	/*if (m_currentStatePosition == STATE_POSITION_GROUND)
	{
		// Wall
		if (abs(a_normal_hit.x) > 0.9f)
		{
			a_velocity.setZero();
		}
		else
		{
			KinematicController::updateVelocityOnCollision(a_velocity, a_normal_hit, a_tangentFactor, a_normalFactor);
		}
	}
	else if (m_currentStatePosition == STATE_POSITION_AIR)
	{
		// Ceiling
		if (a_normal_hit.y < 0.f)
		{
			repositionOnCeiling();
			a_velocity.setZero();
		}
		// Ground
		else if (a_normal_hit.y > 0.9f)
		{
			a_velocity.setZero();
		}
		else
		{
			KinematicController::updateVelocityOnCollision(a_velocity, a_normal_hit, a_tangentFactor, a_normalFactor);
		}
	}
	else
	{
		KinematicController::updateVelocityOnCollision(a_velocity, a_normal_hit, a_tangentFactor, a_normalFactor);
	}

	//std::cout << a_normal_hit << std::endl;*/
}

