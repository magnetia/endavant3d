#ifndef MAGNETIA_INTRO_FALLING_LOGO_STATE_H_
#define MAGNETIA_INTRO_FALLING_LOGO_STATE_H_

#include <State/CState.h>
#include <Utils/CSingleton.h>
#include <Externals/OGRE/Ogre.h>
#include "Panel.h"

class IntroFallingLogoState : public CState, public CSingleton<IntroFallingLogoState>
{
public:
	IntroFallingLogoState();
	~IntroFallingLogoState();

	void Start() override;
	void Finish() override;
	void Pause() override;
	void Resume() override;
	void Update(f64 dt) override;

private:
	typedef std::unique_ptr<Panel>	PanelPtr;

	f64				m_duration;
	f64				m_startTimestamp;
	PanelPtr		m_panel;
	Ogre::Overlay*	m_overlay;
	f64				m_soundPoint;
	bool			m_soundPlayed;
};

#endif
