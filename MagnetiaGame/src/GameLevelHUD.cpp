#include "GameLevelHUD.h"
#include <Core/CCoreEngine.h>
#include <Options/Optionmanager.h>
#include <Renderer/CRenderManager.h>

const static std::string OVERLAYNAME("HUD");

GameLevelHUD::GameLevelHUD() :
	m_overlay{ nullptr }
{
	Ogre::OverlayManager& overlayManager = Ogre::OverlayManager::getSingleton();
	// Create an overlay
	m_overlay = overlayManager.create(OVERLAYNAME);
}

GameLevelHUD::~GameLevelHUD()
{
	Ogre::OverlayManager& overlayManager = Ogre::OverlayManager::getSingleton();
	overlayManager.destroy(OVERLAYNAME);
}

void GameLevelHUD::addElement(HudElement* a_hudElement)
{
	if (a_hudElement && m_overlay)
	{
		a_hudElement->start();
		auto& Panels = a_hudElement->getPanels();
		for (auto panel : Panels)
		{
			if (panel)
			{
				if (Ogre::OverlayContainer* const overlayContainer = panel->getOgrePanel())
				{
					m_overlay->add2D(overlayContainer);
				}
			}
		}
	
		m_hudElements.push_back(a_hudElement);
	}
}

void GameLevelHUD::init()
{
	// Show the overlay
	m_overlay->setZOrder(CCoreEngine::Instance().GetOptionManager().getOption("overlay/z_order/HUD"));
	m_overlay->show();
}

void GameLevelHUD::stop()
{
	for (auto hudElement : m_hudElements)
	{
		hudElement->stop();
	}
}

void GameLevelHUD::update(f64 dt)
{
	for (auto hudElement : m_hudElements)
	{
		hudElement->update(dt);
	}
}
