#ifndef CLOADERGAMESTATE_H_
#define CLOADERGAMESTATE_H_

#include <State/CState.h>
#include <Utils/CSingleton.h>
#include <memory>
#include "Fader.h"

namespace Ogre
{
	class Rectangle2D;
	class SceneNode;
}

class CLoaderGameState : public CState, public CSingleton<CLoaderGameState>, public FaderCallback
{
public:
	CLoaderGameState();
	~CLoaderGameState();

	void Start() override;
	void Finish() override;
	void Pause() override;
	void Resume() override;
	void Update(f64 dt) override;

private:
	virtual void fadeInCallback(void) override;
	virtual void fadeOutCallback(void) override;
	void changeToNextState();

	enum eSubstate
	{
		NONE,
		FADE_IN,
		SHOW_LOGOS,
		FADE_OUT,
	};

	eSubstate							m_substate;
	std::unique_ptr<Ogre::Rectangle2D>	m_backgroundrect;
	Ogre::SceneNode*					m_parentNode;
	std::unique_ptr<Fader>				m_fader;
	f64									m_timestamp;
};

#endif

