#ifndef MAGNETIA_INTRO_STATE_H_
#define MAGNETIA_INTRO_STATE_H_

#include <State/CState.h>
#include <Utils/CSingleton.h>

class IntroState : public CState, public CSingleton<IntroState>
{
public:
	IntroState();
	~IntroState();

	void Start() override;
	void Finish() override;
	void Pause() override;
	void Resume() override;
	void Update(f64 dt) override;

private:
	enum Step
	{
		STEP_NONE,
		STEP_FALLING_SDL,
		STEP_FALLING_OGRE,
		STEP_FALLING_BULLET,
		STEP_FALLING_LUA,
		STEP_MOVING_ENDAVANT,
		STEP_END
	};

	void changeToStep(Step a_step);

	Step	m_currentStep;
};

#endif
