#ifndef CMAINMENUSTATE_H_
#define CMAINMENUSTATE_H_

#include "State/CState.h"
#include "Utils/CSingleton.h"
#include <memory>
class BoardMenuManager;

class MainMenuState : public CState, public CSingleton<MainMenuState>
{
public:
	MainMenuState();
	~MainMenuState();

	void Start() override;
	void Finish() override;
	void Pause() override;
	void Resume() override;
	void Update(f64 dt) override;
	void showMenu(const std::string& a_menuId);

private:
	std::unique_ptr<BoardMenuManager> m_menuMgr;

};

#endif
