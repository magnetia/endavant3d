#include <Core/CCoreEngine.h>
#include <State/CMainStateManager.h>
#include "MainMenuState.h"
#include "ExitGameState.h"
#include "CMainGameState.h"
#include "IntroState.h"

#undef main

int main(int argc, char **argv)
{
	// hide console
	HWND hWnd = GetConsoleWindow();
	ShowWindow(hWnd, SW_HIDE);
	
	// core initialization
	auto& core = CCoreEngine::Instance();
	if (!core.StartUp())
	{
		return 1;
	}

	// register states for name access
	auto& mainStateMgr = core.GetMainStateManager();
	mainStateMgr.RegisterState(MainMenuState::Pointer());
	mainStateMgr.RegisterState(ExitGameState::Pointer());
	mainStateMgr.RegisterState(CMainGameState::Pointer());
	mainStateMgr.RegisterState(IntroState::Pointer());

	// change to first state and endavant!!
	mainStateMgr.ChangeState(IntroState::Pointer());
	core.Run();

	// cleanup
	core.ShutDown();

	return 0;
}
