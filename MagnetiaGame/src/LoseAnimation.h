#ifndef MAGNETIA_GAME_LOSE_ANIMATION_H_
#define MAGNETIA_GAME_LOSE_ANIMATION_H_

#include <memory>
#include <Core/CBasicTypes.h>
#include "Fader.h"
#include "LevelAnimation.h"

class LoseAnimation : public FaderCallback, public LevelAnimation
{
public:
	LoseAnimation();
	~LoseAnimation();
	
	void update(f64 dt) override;
	bool finished() const override;
	virtual void fadeInCallback(void) override;
	virtual void fadeOutCallback(void) override;

private:
	void startImpl();

	std::unique_ptr<Fader>	m_fader;
	bool					m_finished;
};

#endif
