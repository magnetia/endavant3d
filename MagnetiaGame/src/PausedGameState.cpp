#include "PausedGameState.h"
#include <State/CMainStateManager.h>
#include <Core/CCoreEngine.h>
#include <Input/CInputManager.h>


const std::string XML_CONFIG = "media/menus/pause_menu.xml";

PausedGameState::PausedGameState() :
	CState{ "PausedGameState" }
	//m_menuMgr{ "PauseMenuManager" }
{

}

PausedGameState::~PausedGameState()
{

}

void PausedGameState::Start()
{
//	m_menuMgr.load(XML_CONFIG);
}

void PausedGameState::Finish()
{
	//m_menuMgr.unload();
}

void PausedGameState::Pause()
{
//	if (auto* menu = m_menuMgr.TopState())
//	{
//	//	menu->Pause();
//	}
}

void PausedGameState::Resume()
{
/*	if (auto* menu = m_menuMgr.TopState())
	{
		menu->Resume();
	}*/
}

void PausedGameState::Update(f64 dt)
{
	//m_menuMgr.Update(dt);
	if (CCoreEngine::Instance().GetInputManager().GetKeyboard().IsActionKeyDown("Pause"))
	{
		CCoreEngine::Instance().GetMainStateManager().PopState();
	}
}
