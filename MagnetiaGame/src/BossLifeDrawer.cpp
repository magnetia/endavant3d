#include <Core/CCoreEngine.h>
#include <Renderer/CRenderManager.h>
#include <Game/GameManager.h>
#include "Actors/Boss.h"
#include "BossLifeDrawer.h"

BossLifeDrawer::BossLifeDrawer() :
	m_running{ false },
	m_lifePanel{ },
	m_lastLifeValue{ 0 },
	m_boss{ nullptr },
	m_barMaxSize{ },
	m_startPosition{ }
{

}

BossLifeDrawer::~BossLifeDrawer()
{

}

void BossLifeDrawer::start()
{
	if (!m_running)
	{
		m_running = true;

		if (GameEntity* const bossEntity = CCoreEngine::Instance().GetGameManager().getBoss())
		{
			m_boss = static_cast<Boss*>(bossEntity->getActor());
			m_lastLifeValue = m_boss->getLife();
		}

		Ogre::Viewport* const vp{ CCoreEngine::Instance().GetRenderManager().GetOgreViewport() };
		const Ogre::Vector2 lifeStart{ (1.f / 6.f) * vp->getActualWidth(), vp->getActualHeight()*1.f }, lifePos{ lifeStart.x, 0.9f*lifeStart.y };

		std::vector<Panel::Animation*> lifePanelAnimations{
			new Panel::Animation{ 0.5, lifeStart, lifePos }
		};


		m_startPosition = lifePos;
		m_barMaxSize = Ogre::Vector2{ (2.f / 3.f) * vp->getActualWidth(), 0.05f * vp->getActualHeight() };
		m_lifePanel.reset(new Panel{ "BossLifeDrawer", m_startPosition, m_barMaxSize, {}, lifePanelAnimations, {} });
	}
}

void BossLifeDrawer::update(f64 dt)
{
	if (m_lifePanel)
	{
		if (m_boss)
		{
			const s32 currentLife{ m_boss->getLife() };

			if (currentLife != m_lastLifeValue)
			{
				const f32 factor{ static_cast<f32>(currentLife) / m_boss->getMaxLife() };
				const Ogre::Vector2 currentDimensions{ m_barMaxSize.x * factor, m_barMaxSize.y };
				const f32 offset{ m_barMaxSize.x - currentDimensions.x };

				m_lifePanel->getOgrePanel()->setDimensions(currentDimensions.x, currentDimensions.y);
				m_lifePanel->getOgrePanel()->setPosition(m_startPosition.x + offset, m_startPosition.y);

				m_lastLifeValue = currentLife;
			}
		}

		m_lifePanel->update(dt);
	}
}

void BossLifeDrawer::stop()
{
	if (m_running)
	{
		m_running = false;
		m_lifePanel.reset();
		m_lastLifeValue = 0;
		m_boss = nullptr;
		m_barMaxSize = {};
		m_startPosition = {};
	}
}

HudElement::tPanelsVector BossLifeDrawer::getPanels() const
{
	return { m_lifePanel.get() };
}
