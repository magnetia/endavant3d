#include "LoseAnimation.h"
#include "Fader.h"

LoseAnimation::LoseAnimation() :
	LevelAnimation{ "LoseAnimation" },
	m_fader{ new Fader("Overlays/FadeInOut", "Black", this) },
	m_finished{ false }
{
}

LoseAnimation::~LoseAnimation()
{
}

void LoseAnimation::startImpl()
{
	m_finished = false;
	m_fader->startFadeOut(2.f);
}

void LoseAnimation::update(f64 dt)
{
	LevelAnimation::update(dt);
	m_fader->fade(dt);
}

bool LoseAnimation::finished() const
{
	return m_finished;
}

void LoseAnimation::fadeInCallback(void)
{
	// not used...
}

void LoseAnimation::fadeOutCallback(void)
{
	m_finished = true;
}
