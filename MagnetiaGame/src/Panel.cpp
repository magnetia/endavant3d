#include "Panel.h"

Panel::Panel(
		const std::string& a_name, 
		const Ogre::Vector2& a_position, 
		const Ogre::Vector2& a_dimensions,
		const TextsParam& a_texts,
		const AnimationsParam& a_animations,
		const TextureAnimation::Config& a_textureAnimationConfig) :
	m_name{ a_name },
	m_animations{ },
	m_currentAnimation{},
	m_panel{ static_cast<Ogre::OverlayContainer*>(Ogre::OverlayManager::getSingleton().createOverlayElement("Panel", m_name)) },
	m_texts{ },
	m_dimensions{ a_dimensions }
{
	// configure panel
	m_panel->setMetricsMode(Ogre::GMM_PIXELS);
	m_panel->setPosition(a_position.x, a_position.y);
	m_panel->setDimensions(m_dimensions.x, m_dimensions.y);
	m_panel->setMaterialName(m_name);
	m_panel->show();

	// add texts
	for (const TextPairParam& textPair : a_texts )
	{
		const std::string& textId{ textPair.first };
		Text* const text{ textPair.second };

		m_texts.push_back({ textId, TextPtr{ textPair.second } });
		m_panel->addChild(text->getTextArea());
	}

	// add animations
	addAnimations(a_animations);

	// start playing animations
	m_currentAnimation = m_animations.begin();
	playCurrentAnimation();

	if (a_textureAnimationConfig.IsInitialized())
	{
		m_textureAnimation.reset( new TextureAnimation{ a_textureAnimationConfig } );
		m_textureAnimation->start();
	}
}

Panel::~Panel()
{
	Ogre::OverlayManager& overlayManager = Ogre::OverlayManager::getSingleton();

	for (const TextPair& textPair : m_texts)
	{
		overlayManager.destroyOverlayElement(textPair.second->getTextArea());
	}

	if (m_panel)
	{
		overlayManager.destroyOverlayElement(m_panel);
	}
}

void Panel::update(f64 dt)
{
	if (m_currentAnimation != m_animations.end())
	{
		(*m_currentAnimation)->update(dt);

		if ((*m_currentAnimation)->finished())
		{
			m_currentAnimation++;
			playCurrentAnimation();
		}
	}

	if (m_textureAnimation.get())
	{
		m_textureAnimation->update(dt);
	}
}

Ogre::OverlayContainer* Panel::getOgrePanel() const
{
	return m_panel;
}

void Panel::finishAnimations()
{
	while (m_currentAnimation != m_animations.end())
	{
		(*m_currentAnimation)->finish();
		m_currentAnimation++;
	}
}

bool Panel::finished() const
{
	bool returnValue{ true };

	for (auto& animation : m_animations)
	{
		returnValue = returnValue && animation->finished();
	}

	return returnValue;
}

void Panel::setAnimations(const AnimationsParam& a_animations)
{
	finishAnimations();

	m_animations.clear();
	addAnimations(a_animations);
	m_currentAnimation = m_animations.begin();

	playCurrentAnimation();
}

void Panel::playCurrentAnimation()
{
	if (m_currentAnimation != m_animations.end())
	{
		(*m_currentAnimation)->start(this);
	}
}

Panel::Text* Panel::getText(const std::string& a_textId) const
{
	Text* text{ nullptr };
	const Texts::const_iterator it{ std::find_if(m_texts.begin(), m_texts.end(), [&](const TextPair& t){ return t.first == a_textId; }) };

	if (it != m_texts.end())
	{
		text = it->second.get();
	}

	return text;
}

Panel::TextsParam Panel::getTexts() const
{
	TextsParam texts;

	std::transform(m_texts.begin(), m_texts.end(), std::back_inserter(texts), [](const TextPair& t){ return TextPairParam{ t.first, t.second.get() }; });

	return texts;
}

const Ogre::Vector2& Panel::getDimensions() const
{
	return m_dimensions;
}

TextureAnimation* Panel::getTextureAnimation() const
{
	return m_textureAnimation.get();
}

void Panel::setDimensions(const Ogre::Vector2& a_dimensions)
{
	if (m_panel)
	{
		m_dimensions = a_dimensions;
		m_panel->setDimensions(m_dimensions.x, m_dimensions.y);
	}
}

void Panel::addAnimations(const AnimationsParam& a_animations)
{
	for (auto it = a_animations.begin(); it != a_animations.end(); it++)
	{
		Animation* const animation = (*it);

		m_animations.emplace_back(animation);
	}
}

Panel::Animation::Animation(f64 a_duration, const Ogre::Vector2& a_origin, const Ogre::Vector2& a_destination) :
m_duration{ a_duration },
m_destination{ a_destination },
m_vector{ m_destination - a_origin },
m_movementFactor{ m_vector.length() / m_duration },
m_position{ a_origin },
m_elapsed{ 0 }
{
	m_vector.normalise();
}

Panel::Animation::~Animation()
{

}

void Panel::Animation::start(Panel* a_panel)
{
	a_panel->getOgrePanel()->setPosition(m_position.x, m_position.y);
	setParent(a_panel);
}

void Panel::Animation::update(f64 dt)
{
	m_elapsed += dt;

	if (!finished())
	{
		Ogre::Vector2 rel = m_vector * static_cast<f32>(dt * m_movementFactor);
		m_position += rel;
		getParent()->getOgrePanel()->setPosition(m_position.x, m_position.y);
	}
	else if (m_position != m_destination)
	{
		finish();
	}
}

void Panel::Animation::finish()
{
	m_position = m_destination;
	getParent()->getOgrePanel()->setPosition(m_destination.x, m_destination.y);
}

bool Panel::Animation::finished() const
{
	return m_elapsed >= m_duration;
}

Panel::Text::Text(const std::string& a_id, const std::string& a_fontName, const std::string& a_caption,
	const Ogre::Vector2& a_position, const Ogre::ColourValue& a_colour, f32 a_charHeight, Ogre::TextAreaOverlayElement::Alignment a_alignment) :
	m_fontName{ a_fontName },
	m_textArea{ static_cast<Ogre::TextAreaOverlayElement*>(Ogre::OverlayManager::getSingleton().createOverlayElement("TextArea", "TextArea_" + a_id)) },
	m_caption{ a_caption },
	m_position{ a_position }
{
	Ogre::FontManager& fontManager = Ogre::FontManager::getSingleton();

	if (fontManager.resourceExists(m_fontName))
	{
		Ogre::FontPtr font = fontManager.getByName(m_fontName);

		m_textArea->setMetricsMode(Ogre::GMM_PIXELS);
		m_textArea->setPosition(m_position.x, m_position.y);
		m_textArea->setCaption(m_caption);
		m_textArea->setCharHeight(a_charHeight);
		m_textArea->setFontName(font->getName());
		m_textArea->setColour(a_colour);
		m_textArea->setAlignment(a_alignment);

		m_textArea->show();

		//m_textArea->setColourBottom(ColourValue(0.3, 0.5, 0.3));
		//m_textArea->setColourTop(ColourValue(0.5, 0.7, 0.5));
	}
}

Panel::Text::~Text()
{

}

void Panel::Text::setCaption(const std::string& a_caption)
{
	m_caption = a_caption;

	m_textArea->setCaption(m_caption);
}

const std::string& Panel::Text::getCaption() const
{
	return m_caption;
}

Ogre::TextAreaOverlayElement* Panel::Text::getTextArea() const
{
	return m_textArea;
}
