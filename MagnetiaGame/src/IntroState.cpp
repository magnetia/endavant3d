#include <Core/CCoreEngine.h>
#include <State/CMainStateManager.h>
#include <Options/OptionManager.h>
#include <Utils/generic.hpp>
#include <Sound/CSoundManager.h>
#include "MainMenuState.h"
#include "IntroPausedState.h"
#include "IntroFallingLogoState.h"
#include "IntroMovingLogoState.h"
#include "IntroState.h"

const std::string SDL_LOGO_MATERIAL{ "Logos/SDL" };
const std::string OGRE_LOGO_MATERIAL{ "Logos/Ogre" };
const std::string BULLET_LOGO_MATERIAL{ "Logos/Bullet" };
const std::string LUA_LOGO_MATERIAL{ "Logos/Lua" };
const std::string ENDAVANT_LOGO_MATERIAL{ "Logos/Endavant" };

IntroState::IntroState() :
	CState{ "IntroState" },
	m_currentStep{ STEP_NONE }
{
	CCoreEngine::Instance().GetMainStateManager().RegisterState(IntroPausedState::Pointer());
	CCoreEngine::Instance().GetMainStateManager().RegisterState(IntroFallingLogoState::Pointer());
	CCoreEngine::Instance().GetMainStateManager().RegisterState(IntroMovingLogoState::Pointer());
}

IntroState::~IntroState()
{
	CCoreEngine::Instance().GetMainStateManager().UnregisterState(IntroPausedState::Pointer());
	CCoreEngine::Instance().GetMainStateManager().UnregisterState(IntroFallingLogoState::Pointer());
	CCoreEngine::Instance().GetMainStateManager().UnregisterState(IntroMovingLogoState::Pointer());
}

void IntroState::Start()
{
	Ogre::OverlayManager::getSingleton().create("IntroStateOverlay");

	Ogre::MaterialManager& materialManager{ Ogre::MaterialManager::getSingleton() };
	materialManager.load(SDL_LOGO_MATERIAL, "General");
	materialManager.load(OGRE_LOGO_MATERIAL, "General");
	materialManager.load(BULLET_LOGO_MATERIAL, "General");
	materialManager.load(LUA_LOGO_MATERIAL, "General");
	materialManager.load(ENDAVANT_LOGO_MATERIAL, "General");

	CSoundManager& soundManager{ CCoreEngine::Instance().GetSoundManager() };
	soundManager.preloadSound("intro_impact");
	soundManager.preloadSound("intro_crash");

	changeToStep(STEP_FALLING_SDL);
}

void IntroState::Finish()
{
	Ogre::OverlayManager::getSingleton().destroy("IntroStateOverlay");

	Ogre::MaterialManager& materialManager{ Ogre::MaterialManager::getSingleton() };
	materialManager.unload(SDL_LOGO_MATERIAL);
	materialManager.unload(OGRE_LOGO_MATERIAL);
	materialManager.unload(BULLET_LOGO_MATERIAL);
	materialManager.unload(LUA_LOGO_MATERIAL);
	materialManager.unload(ENDAVANT_LOGO_MATERIAL);

	CSoundManager& soundManager{ CCoreEngine::Instance().GetSoundManager() };
	soundManager.releaseSound("intro_impact");
	soundManager.releaseSound("intro_crash");
}

void IntroState::Pause()
{
	
}

void IntroState::Resume()
{
	changeToStep(edv::increment_enum(m_currentStep));
}

void IntroState::Update(f64 dt)
{

}

void IntroState::changeToStep(Step a_step)
{
	CMainStateManager& stateManager{ CCoreEngine::Instance().GetMainStateManager() };
	OptionManager& optionManager{ CCoreEngine::Instance().GetOptionManager() };
	bool finished{ false };
	std::string nextState;

	switch (a_step)
	{
	case STEP_FALLING_SDL:
		nextState = "IntroFallingLogoState";
		optionManager.setOption("isc/" + nextState + "/in/duration", 1.2f);
		optionManager.setOption("isc/" + nextState + "/in/material_name", SDL_LOGO_MATERIAL);
		optionManager.setOption("isc/" + nextState + "/in/texture_name", "SDL_Logo.svg.png");
		break;

	case STEP_FALLING_OGRE:
		nextState = "IntroFallingLogoState";
		optionManager.setOption("isc/" + nextState + "/in/duration", 1.2f);
		optionManager.setOption("isc/" + nextState + "/in/material_name", OGRE_LOGO_MATERIAL);
		optionManager.setOption("isc/" + nextState + "/in/texture_name", "Ogre3d-logo.svg.png");
		break;

	case STEP_FALLING_BULLET:
		nextState = "IntroFallingLogoState";
		optionManager.setOption("isc/" + nextState + "/in/duration", 1.2f);
		optionManager.setOption("isc/" + nextState + "/in/material_name", BULLET_LOGO_MATERIAL);
		optionManager.setOption("isc/" + nextState + "/in/texture_name", "1024px-Bullet_Physics_Logo.svg.png");
		break;

	case STEP_FALLING_LUA:
		nextState = "IntroFallingLogoState";
		optionManager.setOption("isc/" + nextState + "/in/duration", 1.2f);
		optionManager.setOption("isc/" + nextState + "/in/material_name", LUA_LOGO_MATERIAL);
		optionManager.setOption("isc/" + nextState + "/in/texture_name", "600px-Lua-logo-nolabel.svg_3.png");
		break;

	case STEP_MOVING_ENDAVANT:
		nextState = "IntroMovingLogoState";
		optionManager.setOption("isc/" + nextState + "/in/duration", 5.f);
		optionManager.setOption("isc/" + nextState + "/in/material_name", LUA_LOGO_MATERIAL);
		optionManager.setOption("isc/" + nextState + "/in/texture_name", "600px-Lua-logo-nolabel.svg_3.png");
		optionManager.setOption("isc/" + nextState + "/in/new_material_name", ENDAVANT_LOGO_MATERIAL);
		optionManager.setOption("isc/" + nextState + "/in/new_texture_name", "endavant_logo_alpha.png");
		break;

	case STEP_NONE:
	case STEP_END:
	default:
		finished = true;
		break;
	}

	if (!finished && !nextState.empty())
	{
		m_currentStep = a_step;
		stateManager.PushState(nextState);
	}
	else
	{
		CCoreEngine::Instance().GetMainStateManager().ChangeState(MainMenuState::Pointer());
		MainMenuState::Pointer()->showMenu("/MainMenu/MainMenu");
	}
}
