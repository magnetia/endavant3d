#include <Core/CCoreEngine.h>
#include <State/CMainStateManager.h>
#include <Time/CTimeManager.h>
#include <Options/OptionManager.h>
#include "IntroPausedState.h"

IntroPausedState::IntroPausedState() :
	CState{ "IntroPausedState" },
	m_pauseTime{ 0. },
	m_startTimestamp{ 0 }
{

}

IntroPausedState::~IntroPausedState()
{

}

void IntroPausedState::Start()
{
	m_pauseTime = CCoreEngine::Instance().GetOptionManager().getOption("isc/" + GetName() + "/in/pause_time");
	m_startTimestamp = CCoreEngine::Instance().GetTimerManager().GetTotalTimeSeconds();
}

void IntroPausedState::Finish()
{
	m_startTimestamp = 0;
}

void IntroPausedState::Pause()
{
	// warning - not implemented!
}

void IntroPausedState::Resume()
{
	// warning - not implemented!
}

void IntroPausedState::Update(f64 dt)
{
	f64 now{ CCoreEngine::Instance().GetTimerManager().GetTotalTimeSeconds() };

	if (m_startTimestamp + m_pauseTime >= now)
	{
		CCoreEngine::Instance().GetMainStateManager().PopState();
	}
}
