#ifndef MAGNETIA_GAME_WIN_ANIMATION_H_
#define MAGNETIA_GAME_WIN_ANIMATION_H_

#include <memory>
#include <Core/CBasicTypes.h>
#include "LevelAnimation.h"

class Fader;

class WinAnimation : public LevelAnimation
{
public:
	WinAnimation();
	~WinAnimation();

	void update(f64 dt) override;

private:
	void startImpl() override;

	std::unique_ptr<Fader>	m_fader;
};

#endif
