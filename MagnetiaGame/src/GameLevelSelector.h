#ifndef GAMELEVELSELECTOR_H_
#define GAMELEVELSELECTOR_H_

#include "GameLevel.h"
#include "Utils/CSingleton.h"
#include <string>

class GameLevelTiledLoader;
class GameLevelTiledParser;
class GameLevelSelector : public CSingleton<GameLevelSelector>
{

public:
	typedef std::vector<std::string> LevelList;
	
	GameLevelSelector(const GameLevelSelector&) = delete;
	GameLevelSelector& operator=(const GameLevelSelector&) = delete;
	~GameLevelSelector();

	void loadLevel(const std::string& a_level_name);
	void reloadLevel();	//Recarrega el nivell amb la mateixa configuració
	void reloadLevelRaw(); //Recarrega el nivell llegint de nou la configuracio del fitxer maps
	void unloadLevel();

	LevelList getLevels() const;
	std::shared_ptr<const GameLevel> getCurrentLevel() const;
	GameLevelTiledParser::tGameLevelXMLInfo getLevelsXMLinfo() const;

	void pause();
	void resume();
	void update(f64 dt);

	
	GameLevelSelector();


	std::string	m_levelid;

protected:

private:
	
	typedef std::pair<std::string, std::shared_ptr<GameLevel> > tCurLevelInfo;
	std::shared_ptr<tCurLevelInfo >			m_current_level;
	std::shared_ptr<GameLevelTiledParser>	m_tiled_parser;
};

#endif