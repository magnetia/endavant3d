#ifndef MAGNETIA_GAME_COUNTER_H_
#define MAGNETIA_GAME_COUNTER_H_

#include <Core/CBasicTypes.h>
#include <memory>
#include <OGRE/Ogre.h>
#include "HudElement.h"

class Panel;
class OverlayMaterialAnimator;

class Counter : public HudElement
{
public:
	Counter();
	~Counter();

	virtual void start() override;
	virtual void update(f64 dt) override;
	virtual void stop() override;
	virtual tPanelsVector getPanels() const override;

private:
	typedef std::unique_ptr<Panel>						tPanelPtr;
	typedef std::unique_ptr<OverlayMaterialAnimator>	tOverlayMaterialAnimatorPtr;
	
	bool		m_running;
	tPanelPtr	m_counterPanel;
	tOverlayMaterialAnimatorPtr					m_ballspanelAnimator;
	u32			m_playerNeutralBalls;
};

#endif