-- Spaceship

Spaceship = {
};

function SpaceshipOnInit(entity)

end

function SpaceshipOnUpdate(dt, entity)

end

function SpaceshipOnCollision(entity, other, hitPoint, hitNormal)

end

function SpaceshipOnCollisionEnter(entity, other)

end

function SpaceshipOnCollisionExit(entity, other)

end

function SpaceshipOnMagneticRayHit(targetEntity, originEntity, originPos, direction, pointWorld, force, charge)

end

function SpaceshipOnMagneticRayTarget(targetEntity, originEntity, originPos, direction, pointWorld)

end

function SpaceshipOnMagneticRayUnTarget(targetEntity)

end
