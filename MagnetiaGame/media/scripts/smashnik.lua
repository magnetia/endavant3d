-------------------
---- SMASHNIK -----
-------------------

Smashnik = {
	states = {
		wait = 0,
		startup = 1,
		follow = 2,
		rebound = 3,
		shutdown = 4
	},

	times = {
		startup = 1,
		rebound = 0.3
	},

	action_radius = 50,
	movement_offset = 1000
}


function SmashnikHandleCollision(entity, other)

end

function SmashnikOnInit(entity)
	entity:getActor():setState(Smashnik.states.wait);
end

function SmashnikOnUpdate(dt, entity)

	local actor = entity:getActor();
	local state = actor:getState();
	local player = getPlayer();

	if (state == Smashnik.states.wait) then
--~ 		print("WAITING");

		if (not actor:onGround()) then
			actor:move(0, actor:getGravity());
		else
			if (entity:distanceToEntity(player) <= Smashnik.action_radius) then
				if (entity:canSeeEntity(player)) then
					actor:showParticles(true);
					actor:startUp(Smashnik.times.startup);
					actor:checkOrientation(false);
					actor:setState(Smashnik.states.startup);
				end
			end
		end

	elseif (state == Smashnik.states.startup) then
--~ 		print("STARTUP");

		if (actor:stateElapsed() > Smashnik.times.startup) then
			actor:finishAnimation();
			actor:setState(Smashnik.states.follow);
		end

	elseif (state == Smashnik.states.follow) then

		if (not actor:onGround()) then
--~  			print("DOWN");
			actor:move(0, actor:getGravity());
		else
--~ 			print("AEAAEAEEAAEWAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAEAEAE");
			if (entity:distanceToEntity(player) > Smashnik.action_radius) then
				actor:shutdown();
				actor:finishAnimation();
				actor:checkOrientation(true);
				actor:setState(Smashnik.states.shutdown);
			else
				local playerPosition = player:getWorldPosition();
				local smashnikPosition = entity:getWorldPosition();

				if (playerPosition.x < smashnikPosition.x) then
					if (not actor:collidingLeft()) then
						actor:move(-Smashnik.movement_offset, 0);
					end
				else
					if (not actor:collidingRight()) then
						actor:move(Smashnik.movement_offset, 0);
					end
				end

				actor:checkOrientation(false);

			end
		end

	elseif (state == Smashnik.states.rebound) then
--~ 		print("REBOUND");

		if (actor:stateElapsed() > Smashnik.times.rebound) then
			actor:setState(Smashnik.states.follow);
--~ 		else
--~ 			actor:rebound();
		end

	elseif (state == Smashnik.states.shutdown) then
--~ 		print("SHUTDOWN");

		if (not actor:onGround()) then
			actor:move(0, actor:getGravity());
		else
			actor:showParticles(false);
			actor:setState(Smashnik.states.wait);
		end

	end

end

function SmashnikOnCollision(entity, other, hitPoint, hitNormal)

--~ 	local smashnikPos = entity:getWorldPosition();
--~ 	local smashnikHalfSize = entity:getScaledHalfSize();

--~ 	if (hitPoint.y < (smashnikPos.y + smashnikHalfSize.y) and
--~ 		hitPoint.y > (smashnikPos.y - smashnikHalfSize.y)) then

--~ 		if (hitPoint.x > (smashnikPos.x - smashnikHalfSize.x) and
--~ 			hitPoint.x < smashnikPos.x) then

--~ 			-- collision from left side

--~ 			print("LEFT");
--~ 			entity:getActor():move(Smashnik.movement_offset, 0);


--~ 		elseif (hitPoint.x < (smashnikPos.x + smashnikHalfSize.x) and
--~ 				hitPoint.x > (smashnikPos.x)) then

--~ 			-- collision from right side

--~ 			print("RIGHT");
--~ 			entity:getActor():move(-Smashnik.movement_offset, 0);

--~ 		end

--~ 	end

	local otherActor = other:getActor();

	if (otherActor ~= nil) then
		otherActor:kill();
	end

end

function SmashnikOnCollisionEnter(entity, other)

end

function SmashnikOnCollisionExit(entity, other)

end

function SmashnikOnMagneticRayHit(targetEntity, originEntity, originPos, direction, pointWorld, force, charge)

end

function SmashnikOnMagneticRayTarget(targetEntity, originEntity, originPos, direction, pointWorld)

end

function SmashnikOnMagneticRayUnTarget(targetEntity)

end
