-------------------
----- APLASTA -----
-------------------

Aplasta = {
	states = {
		up_wait = 0,
		up_activation = 1,
		down = 2,
		down_wait = 3,
		down_deactivation = 4,
		up = 5,
	},
}

function AplastaOnInit(entity)
	entity:getActor():setState(Aplasta.states.up_wait);
	entity:getGraphics():getMainEntity():setFrame(0);
end

function AplastaOnUpdate(dt, entity)

	local actor = entity:getActor();
	local state = actor:getState();
	local elapsed = actor:stateElapsed();

	if (state == Aplasta.states.up_wait) then

		if (elapsed > (actor:getUpWaitTime() - actor:getChangingTime())) then
			actor:setState(Aplasta.states.up_activation);
			entity:getGraphics():getMainEntity():setFrame(0);
		end

	elseif (state == Aplasta.states.up_activation) then
		
		if(elapsed < actor:getChangingTime()) then
			local NumFrames = entity:getGraphics():getMainEntity():getNumFrames();
			local newFrame = (elapsed/actor:getChangingTime())*NumFrames;
			entity:getGraphics():getMainEntity():setFrame(newFrame);
		else
			actor:goDown();
			actor:setState(Aplasta.states.down);
		end

	elseif (state == Aplasta.states.down) then

		if (elapsed > actor:getDownTime()) then
			actor:setState(Aplasta.states.down_wait);
			actor:playCrushAnimation();
		end

	elseif (state == Aplasta.states.down_wait) then

		if (elapsed > (actor:getDownWaitTime() - actor:getChangingTime())) then
			actor:setState(Aplasta.states.down_deactivation);
		end
		
	elseif (state == Aplasta.states.down_deactivation) then
		
		if(elapsed < actor:getChangingTime()) then
			local NumFrames = entity:getGraphics():getMainEntity():getNumFrames();
			local newFrame = NumFrames - (elapsed/actor:getChangingTime())*NumFrames;
			entity:getGraphics():getMainEntity():setFrame(newFrame);
		else
			actor:goUp();
			actor:setState(Aplasta.states.up);
		end

	elseif (state == Aplasta.states.up) then

		if (elapsed > actor:getUpTime()) then
			actor:setState(Aplasta.states.up_wait);
		end

	end
end

function AplastaOnCollision(entity, other, hitPoint, hitNormal)

	local aplastaPos = entity:getWorldPosition();
	local aplastaSize = entity:getScaledHalfSize();
	local otherPos = other:getWorldPosition();
	local otherSize = other:getScaledHalfSize();
	local sizeX = otherSize.x / 4;
	local sizeY = otherSize.y * 1.5;
	local ceilingPos = entity:getActor():getOriginPosition().y + aplastaSize.y;
	local groundPos = entity:getActor():getGroundPosition().y - aplastaSize.y;
	local otherActor = other:getActor();

	if(otherActor ~= nil) then
		-- eix X
		if ((otherPos.x + sizeX > aplastaPos.x - aplastaSize.x) and
			(otherPos.x - sizeX < aplastaPos.x + aplastaSize.x)) then

			-- sobre
			if (hitPoint.y > aplastaPos.y) then
				if (hitPoint.y + sizeY >= ceilingPos) then
					otherActor:kill();
				end
			-- sota
			else
				if (hitPoint.y - sizeY <= groundPos) then
					otherActor:kill();
				end
			end

		end
	end
end

function AplastaOnCollisionEnter(entity, other)

end

function AplastaOnCollisionExit(entity, other)

end

function AplastaOnMagneticRayHit(targetEntity, originEntity, originPos, direction, pointWorld, force, charge)

end

function AplastaOnMagneticRayTarget(targetEntity, originEntity, originPos, direction, pointWorld)

end

function AplastaOnMagneticRayUnTarget(targetEntity)

end
