--~ -- Nik behavior default script

Nik = {
	power = 5,
	life = 10
}
function NikOnCollision(entity, other)

end

function NikOnCollisionEnter(entity, other)

end

function NikOnCollisionExit(entity, other)

end

function NikOnInit(entity)

end

function NikOnUpdate(dt, entity, hitPoint, hitNormal)

end

function NikOnMagneticRayHit(targetEntity, originEntity, originPos, direction, force, charge)

end

function NikOnMagneticRayHit(targetEntity, originEntity, originPos, direction, pointWorld, force, charge)

end

function NikOnMagneticRayTarget(targetEntity, originEntity, originPos, direction, pointWorld)

end

function NikOnMagneticRayUnTarget(targetEntity)

end
