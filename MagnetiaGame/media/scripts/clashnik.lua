-------------------
---- CLASHNIK -----
-------------------

Clashnik = {
	states = {
		idle = 0,
		moving = 1,
		slowing = 2
	},

	action_radius = 50,
	max_speed = 800,
	max_speed_time = 0.2,

	orientation = {
		left = 0,
		right = 1
	}
}

function ClashnikGetSpeed(dt, orientation)
	local diff = (Clashnik.max_speed * dt) / Clashnik.max_speed_time;

	if (orientation == Clashnik.orientation.left) then
		diff = diff * -1;
	end

	return diff;
end

function ClashnikInverseOrientation(orientation)
	local newOrientation;

	if (orientation == Clashnik.orientation.left) then
		newOrientation = Clashnik.orientation.right;
	else
		newOrientation = Clashnik.orientation.left;
	end

	return newOrientation;
end

function ClashnikOnInit(entity)
	local clashnik = entity:getActor();

	clashnik:setState(Clashnik.states.idle);
	clashnik:setSpeed(0);
	clashnik:setOrientation(Clashnik.orientation.left);
end

function ClashnikOnUpdate(dt, entity)
	local player = getPlayer();
	local distance = entity:distanceToEntity(player);
	local clashnik = entity:getActor();
	local state = clashnik:getState();
	local inRadius = distance <= Clashnik.action_radius;
--~ 	local visible = entity:canSeeEntity(player);
	local position = entity:getWorldPosition();
	local playerPosition = player:getWorldPosition();
	local speed = clashnik:getSpeed();
	local speedAbs = math.abs(speed);
	local orientation = clashnik:getClashnikOrientation();
--~ 	local stuck = clashnik:isStuck();

	if (state == Clashnik.states.idle) then

		if (inRadius) then

			clashnik:setState(Clashnik.states.moving);
			clashnik:setSpeed(0);

			if (playerPosition.x <= position.x) then
				clashnik:setOrientation(Clashnik.orientation.left);
			else
				clashnik:setOrientation(Clashnik.orientation.right);
			end
		end

	elseif (state == Clashnik.states.moving) then

		if (inRadius) then

			local facingPlayer =
				(playerPosition.x <= position.x and orientation == Clashnik.orientation.left) or
				(playerPosition.x > position.x and orientation == Clashnik.orientation.right);

			if (facingPlayer) then

				if (speedAbs < Clashnik.max_speed) then
					local newSpeed = speed + ClashnikGetSpeed(dt, orientation);
					clashnik:setSpeed(newSpeed);
				end

			else

				clashnik:setOrientation(ClashnikInverseOrientation(orientation));
				clashnik:setState(Clashnik.states.slowing);

			end
		else

			clashnik:setOrientation(ClashnikInverseOrientation(orientation));
			clashnik:setState(Clashnik.states.slowing);
		end

	elseif (state == Clashnik.states.slowing) then

		if (speedAbs > 0) then

			local diff = ClashnikGetSpeed(dt, orientation);
			local newSpeed = speed + diff;

			if ((speed < 0 and newSpeed >= 0) or (speed > 0 and newSpeed < 0)) then
				newSpeed = 0;
			end

			clashnik:setSpeed(newSpeed);

		else
			clashnik:setState(Clashnik.states.idle);

		end

	end
end

function ClashnikOnCollision(entity, other, hitPoint, hitNormal)
	local player = getPlayer();

	if (other:sameEntity(player)) then
		player:getActor():kill();
	end
end

function ClashnikOnMagneticRayHit(targetEntity, originEntity, originPos, direction, pointWorld, force, charge)

end

function ClashnikOnMagneticRayTarget(targetEntity, originEntity, originPos, direction, pointWorld)

end

function ClashnikOnMagneticRayUnTarget(targetEntity)

end
