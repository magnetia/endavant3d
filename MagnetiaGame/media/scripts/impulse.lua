-------------------
----- IMPULSE -----
-------------------

Impulse = {
	states = {
		off = 0,
		on = 1,
	}
}

function ImpulseOnInit(entity)
	entity:getActor():setState(Impulse.states.on);
end

function ImpulseOnUpdate(dt, entity)

end

function ImpulseOnCollision(entity, other, hitPoint, hitNormal)

end

function ImpulseOnCollisionEnter(entity, other)
	if (entity:getActor():getState() == Impulse.states.on) then
		local player = getPlayer();

		if (other:sameEntity(player)) then
			local actor = entity:getActor();

			actor:ejectPlayer();
			actor:setState(Impulse.states.off);
		end
	end
end

function ImpulseOnCollisionExit(entity, other)
	if (entity:getActor():getState() == Impulse.states.off) then
		local player = getPlayer();

		if (other:sameEntity(player)) then
			entity:getActor():setState(Impulse.states.on);
		end
	end
end

function ImpulseOnMagneticRayHit(targetEntity, originEntity, originPos, direction, pointWorld, force, charge)

end

function ImpulseOnMagneticRayTarget(targetEntity, originEntity, originPos, direction, pointWorld)

end

function ImpulseOnMagneticRayUnTarget(targetEntity)

end
