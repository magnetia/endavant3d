-- NeutralBall

NeutralBall = {
	states = {
		active = 0,
		animation = 1
	}
};

function NeutralBallOnInit(entity)
	entity:getActor():setState(NeutralBall.states.active);
end

function NeutralBallOnUpdate(dt, entity)
	local actor = entity:getActor();
	local state = actor:getState();

	if (state == NeutralBall.states.animation) then
		local elapsed = actor:stateElapsed();
		local duration = actor:getAnimationDuration();

		if (elapsed >= duration) then
			local player = getPlayer();

			player:getActor():setNeutralizerBalls(player:getActor():getNeutralizerBalls() + 1);
			getGameManager():getCurrentLevel():destroyItem(entity:getName());
		end
	end
end

function NeutralBallOnCollision(entity, other)
	local actor = entity:getActor();
	local state = actor:getState();

	if(state == NeutralBall.states.active) then
		local player = getPlayer();

		if (player:sameEntity(other)) then
			actor:playGetAnimation();
			actor:setState(NeutralBall.states.animation)
		end
	end
end

function NeutralBallOnCollisionEnter(entity, other)

end

function NeutralBallOnCollisionExit(entity, other)

end

function NeutralBallOnMagneticRayHit(targetEntity, originEntity, originPos, direction, pointWorld, force, charge)

end

function NeutralBallOnMagneticRayTarget(targetEntity, originEntity, originPos, direction, pointWorld)

end

function NeutralBallOnMagneticRayUnTarget(targetEntity)

end
