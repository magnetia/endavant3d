-------------------
-- LETHAL BARRIER -
-------------------

LethalBarrier = {
	states = {
		off = 0,
		on = 1,
	},
}

function LethalBarrierOnInit(entity)
	entity:getActor():setState(LethalBarrier.states.off);
end

function LethalBarrierOnUpdate(dt, entity)

	local actor = entity:getActor();
	local state = actor:getState();
	local elapsed = actor:stateElapsed();

	if (state == LethalBarrier.states.off) then

		if (elapsed > actor:getOffTime()) then
			actor:showRay(true);
			actor:setState(LethalBarrier.states.on);
		end

	elseif (state == LethalBarrier.states.on) then

		if (elapsed > actor:getOnTime()) then
			actor:showRay(false);
			actor:setState(LethalBarrier.states.off);
		end

	end

end

function LethalBarrierOnCollision(entity, other, hitPoint, hitNormal)

	local player = getPlayer();

	if (player:sameEntity(other)) then

		local state = entity:getActor():getState();

		if (state == LethalBarrier.states.on) then
			player:getActor():kill();
		end
	end
end

function LethalBarrierOnCollisionEnter(entity, other)

end

function LethalBarrierOnCollisionExit(entity, other)

end

function LethalBarrierOnMagneticRayHit(targetEntity, originEntity, originPos, direction, pointWorld, force, charge)

end

function LethalBarrierOnMagneticRayTarget(targetEntity, originEntity, originPos, direction, pointWorld)

end

function LethalBarrierOnMagneticRayUnTarget(targetEntity)

end

