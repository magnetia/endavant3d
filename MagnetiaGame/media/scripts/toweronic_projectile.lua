-- TOWERONIC PROJECTILE

ToweronicProjectile = {
	power = 10,
	states = {
		active = 0,
		inactive = 1,
		growing = 2
	},
	inactive_time = 1,
	grow_increment = 2,
	grow_time = 0.6
};

function ToweronicProjectileOnInit(entity)
	entity:getActor():setState(ToweronicProjectile.states.active);
end

function ToweronicProjectileOnUpdate(dt, entity)
	local projectile = entity:getActor();
	local state = projectile:getState();
	local elapsed = projectile:stateElapsed();

	if (state == ToweronicProjectile.states.inactive) then

		if (elapsed > ToweronicProjectile.inactive_time) then
			projectile:destroy();

		end
	elseif (state == ToweronicProjectile.states.growing) then

		if (elapsed > ToweronicProjectile.grow_time) then
			projectile:setState(ToweronicProjectile.states.active);

		end

	end
end

function ToweronicProjectileOnCollision(entity, other, hitPoint, hitNormal)
	local projectile = entity:getActor();
	local state = projectile:getState();

	if (state ~= ToweronicProjectile.states.inactive) then
		local player = getPlayer();
		local toweronic = projectile:getToweronicEntity();
		local inactive = false;

		if (other:sameEntity(player)) then
--~ 			player:getActor():decreaseLife(ToweronicProjectile.power);
				player:getActor():kill();
			inactive = true;

		elseif (not other:sameEntity(toweronic)) then
			inactive = true;

		end

		if (inactive) then
			projectile:setInactive(ToweronicProjectile.inactive_time);
			projectile:setState(ToweronicProjectile.states.inactive);

		end
	end
end

function ToweronicProjectileOnMagneticRayHit(targetEntity, originEntity, originPos, direction, pointWorld, force, charge)

	local projectile = targetEntity:getActor();
	local state = projectile:getState();

	if (state == ToweronicProjectile.states.active) then
		local player = getPlayer();

		if (originEntity:sameEntity(player) and charge ~= MagneticProperty.MAGNET_TYPE_NEUTRAL) then
			local ownCharge = targetEntity:getActor():getCharge();

			if (charge == ownCharge) then
				projectile:setInactive(ToweronicProjectile.inactive_time);
				projectile:setState(ToweronicProjectile.states.inactive);
			else
				projectile:grow(ToweronicProjectile.grow_increment,
					ToweronicProjectile.grow_time);
				projectile:setState(ToweronicProjectile.states.growing);
			end
		end
	end

end

function ToweronicProjectileOnCollisionEnter(entity, other)

end

function ToweronicProjectileOnCollisionExit(entity, other)

end

function ToweronicProjectileOnMagneticRayTarget(targetEntity, originEntity, originPos, direction, pointWorld)

end

function ToweronicProjectileOnMagneticRayUnTarget(targetEntity)

end
