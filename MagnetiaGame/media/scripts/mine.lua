-- Mine

Mine = {
	states = {
		waiting = 0,
		exploding = 1,
		off = 2
	},
	explosion_time = 2
};

function MineOnInit(entity)
	entity:getActor():setState(Mine.states.waiting);
end

function MineOnUpdate(dt, entity)
	local mine = entity:getActor();

	if (mine:getState() == Mine.states.exploding) then
		if (mine:stateElapsed() > Mine.explosion_time) then
			mine:setState(Mine.states.off);
		end
	end
end

function MineOnCollision(entity, other, hitPoint, hitNormal)
	local mine = entity:getActor();

	if (mine:getState() == Mine.states.waiting) then
		local player = getPlayer();

		if (player:sameEntity(other)) then
			player:getActor():kill();
		end

		local boss = entity:getActor():getBossEntity();

		if (not other:sameEntity(boss)) then
			mine:explode();
			mine:setState(Mine.states.exploding);
			entity:getGraphics():getNode():setVisible(false);
		end
	end
end

function MineOnCollisionEnter(entity, other)

end

function MineOnCollisionExit(entity, other)

end

function MineOnMagneticRayHit(targetEntity, originEntity, originPos, direction, pointWorld, force, charge)

end

function MineOnMagneticRayTarget(targetEntity, originEntity, originPos, direction, pointWorld)

end

function MineOnMagneticRayUnTarget(targetEntity)

end
