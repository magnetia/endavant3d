-- UnstablePlatform

UnstablePlatform = {
	states = {
		sleeping = 0,
		active = 1,
		advise = 2,
		inactive = 3
	}
};

function UnstablePlatformOnInit(entity)
	entity:getActor():setState(UnstablePlatform.states.sleeping);
end

function UnstablePlatformOnUpdate(dt, entity)
	local actor = entity:getActor();
	local elapsed = actor:stateElapsed();
 	local state = actor:getState();

	if ( (state == UnstablePlatform.states.active) and (elapsed > actor:getTimeInActive()) ) then
		actor:setState(UnstablePlatform.states.advise);
		actor:playAdvise();
	elseif ( (state == UnstablePlatform.states.advise) and (elapsed > actor:getTimeInAdvise()) ) then
		actor:setState(UnstablePlatform.states.inactive);
		actor:stop();
	elseif( (state == UnstablePlatform.states.inactive) and (elapsed > actor:getTimeInInactive()) ) then
		actor:setState(UnstablePlatform.states.sleeping);
		actor:init();
	end
end

function UnstablePlatformOnCollision(entity, other, hitPoint, hitNormal)
	local actor = entity:getActor();
	local state = actor:getState();
	
--  Only active tramp if player is on top
	if (hitNormal.y == 1 and state == UnstablePlatform.states.sleeping) then
		local player = getPlayer();
		if (player:sameEntity(other)) then
			actor:setState(UnstablePlatform.states.active);
		end
	end
end

function UnstablePlatformOnCollisionEnter(entity, other)

end

function UnstablePlatformOnCollisionExit(entity, other)
-- Reset test
--	local actor = entity:getActor();
--	local state = actor:getState();
			
-- reset timer if state is active
--	if (state == UnstablePlatform.states.active) then
--		local player = getPlayer();
--		if (player:sameEntity(other)) then
--			actor:setState(UnstablePlatform.states.sleeping);
--		end
--	end
end

function UnstablePlatformOnMagneticRayHit(targetEntity, originEntity, originPos, direction, pointWorld, force, charge)

end

function UnstablePlatformOnMagneticRayTarget(targetEntity, originEntity, originPos, direction, pointWorld)

end

function UnstablePlatformOnMagneticRayUnTarget(targetEntity)

end
