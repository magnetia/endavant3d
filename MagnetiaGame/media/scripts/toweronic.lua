-- TOWERONIC

Toweronic = {
	states = {
		waiting = 0,
		attacking = 1,
		shooting = 2
	},
	time_between_shoots = 5,
	power = 10,
	minXAngle = 0
};

function canAttackPlayer(entity)
	local player = getPlayer();
	local distance = entity:distanceToEntity(player);
	local angle = entity:angleToEntity(player);
	local canSee = entity:getActor():canSeePlayer();
	local validAngle = angle >= Toweronic.minXAngle and angle <= (180 - Toweronic.minXAngle);

	return (distance <= entity:getActor():getActionRadius() and canSee and validAngle)
end

function ToweronicOnInit(entity)
	entity:getActor():setState(Toweronic.states.waiting);
end

function ToweronicOnUpdate(dt, entity)
	local actor = entity:getActor();
	local elapsed = actor:stateElapsed();
 	local state = actor:getState();

	if (state == Toweronic.states.waiting) then
		if (canAttackPlayer(entity)) then
			actor:setState(Toweronic.states.attacking);
		end

	elseif (state == Toweronic.states.attacking) then
		if (not canAttackPlayer(entity)) then
			actor:setState(Toweronic.states.waiting);
		else
			actor:shootPlayer();
			actor:setState(Toweronic.states.shooting);
		end

	elseif (state == Toweronic.states.shooting) then
		if(canAttackPlayer(entity)) then
			actor:targetPlayer();
			if (elapsed > Toweronic.time_between_shoots) then
				actor:setState(Toweronic.states.attacking);
			end
		else
			actor:setState(Toweronic.states.waiting);
		end
	end
end

function ToweronicOnCollision(entity, other, hitPoint, hitNormal)
--~ 	local player = getPlayer();

--~ 	if (player:sameEntity(other)) then
--~ 		getGameManager():getCurrentLevel():destroyEnemy(entity:getName());
--~ 	end
end

function ToweronicOnCollisionEnter(entity, other)

end

function ToweronicOnCollisionExit(entity, other)

end

function ToweronicOnMagneticRayHit(targetEntity, originEntity, originPos, direction, pointWorld, force, charge)

end

function ToweronicOnMagneticRayTarget(targetEntity, originEntity, originPos, direction, pointWorld)

end

function ToweronicOnMagneticRayUnTarget(targetEntity)

end
