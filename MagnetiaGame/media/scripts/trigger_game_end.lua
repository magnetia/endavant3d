-- TriggerGameEnd

TriggerGameEnd = {
	states = {
		active = 0,
		story = 1
	}
};

function TriggerGameEndOnInit(entity)
	entity:getActor():setState(TriggerGameEnd.states.active);
	entity:getGraphics():getNode():setVisible(false);
end

function TriggerGameEndOnUpdate(dt, entity)
	local actor = entity:getActor();
	local state = actor:getState();

	if (state == TriggerGameEnd.states.story) then
--~ 		local elapsed = actor:stateElapsed();
--~ 		local duration = actor:getAnimationDuration();

--~ 		if (elapsed >= duration) then
--~ 			local player = getPlayer();

--~ 			player:getActor():setNeutralizerBalls(player:getActor():getNeutralizerBalls() + 1);
--~ 			getGameManager():getCurrentLevel():destroyItem(entity:getName());
--~ 		end
	end
end

function TriggerGameEndOnCollision(entity, other)
	local actor = entity:getActor();
	local state = actor:getState();

	if(state == TriggerGameEnd.states.active) then
		local player = getPlayer();

		if (player:sameEntity(other)) then
			actor:startStory();
			actor:setState(TriggerGameEnd.states.story)
		end
	end
end

function TriggerGameEndOnCollisionEnter(entity, other)

end

function TriggerGameEndOnCollisionExit(entity, other)

end

function TriggerGameEndOnMagneticRayHit(targetEntity, originEntity, originPos, direction, pointWorld, force, charge)

end

function TriggerGameEndOnMagneticRayTarget(targetEntity, originEntity, originPos, direction, pointWorld)

end

function TriggerGameEndOnMagneticRayUnTarget(targetEntity)

end
