-- MortalGas end

MortalGas = {};

function MortalGasOnInit(entity)

end

function MortalGasOnUpdate(dt, entity)

end

function MortalGasOnCollision(entity, other)
	local player = getPlayer();
	if (player:sameEntity(other)) then
		player:getActor():kill();
	end
end

function MortalGasOnCollisionEnter(entity, other)

end

function MortalGasOnCollisionExit(entity, other)

end

function MortalGasOnMagneticRayHit(targetEntity, originEntity, originPos, direction, pointWorld, force, charge)

end

function MortalGasOnMagneticRayTarget(targetEntity, originEntity, originPos, direction, pointWorld)

end

function MortalGasOnMagneticRayUnTarget(targetEntity)

end
