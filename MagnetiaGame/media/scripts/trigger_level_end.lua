-- Trigger level end

TriggerLevelEnd = {
	states = {
		disabled = 0,
		enabled = 1
	},
	enable_distance = 3.3,
	material = "punt_final_mat",
	anim_material = "punt_final_anim_mat"
};

function TriggerLevelEndOnInit(entity)
	-- preload materials
	entity:getGraphics():getMainEntity():setMaterialName(TriggerLevelEnd.anim_material);
	entity:getGraphics():getMainEntity():setMaterialName(TriggerLevelEnd.material);
	entity:getGraphics():getParticleSystemNode(entity:getName() .. "level_endparticle"):setVisible(false);

	local actor = entity:getActor();
	actor:setState(TriggerLevelEnd.states.disabled);
end

function TriggerLevelEndOnUpdate(dt, entity)
	local actor = entity:getActor();
	local state = actor:getState();
	if(state == TriggerLevelEnd.states.disabled) then
		if(getGameManager():getCurrentLevel():getCurrentNeutralBalls() == 0) then
			entity:getGraphics():getMainEntity():setMaterialName(TriggerLevelEnd.anim_material);
			entity:getGraphics():getMainEntity():setFrame(0);
			entity:getGraphics():getParticleSystemNode(entity:getName() .. "level_endparticle"):setVisible(true);
			actor:setState(TriggerLevelEnd.states.enabled);
		end
	end

	local playerAtEnd = getGameManager():getCurrentLevel():isPlayerAtEnd();
	if(playerAtEnd) then
		local player = getPlayer();
		local distance = entity:distanceToEntity(player);
		if(distance > TriggerLevelEnd.enable_distance) then
			getGameManager():getCurrentLevel():setPlayerAtEnd(false);
		end
	end
end

function TriggerLevelEndOnCollision(entity, other, hitPoint, hitNormal)
	local player = getPlayer();
	if (player:sameEntity(other)) then
		local distance = entity:distanceToEntity(player);
		local playerAtEnd = getGameManager():getCurrentLevel():isPlayerAtEnd();
		if((distance < TriggerLevelEnd.enable_distance) and (playerAtEnd == false)) then
			getGameManager():getCurrentLevel():setPlayerAtEnd(true);
		end
	end
end

function TriggerLevelEndOnCollisionEnter(entity, other)

end

function TriggerLevelEndOnCollisionExit(entity, other)

end

function TriggerLevelEndOnMagneticRayHit(targetEntity, originEntity, originPos, direction, pointWorld, force, charge)

end

function TriggerLevelEndOnMagneticRayTarget(targetEntity, originEntity, originPos, direction, pointWorld)

end

function TriggerLevelEndOnMagneticRayUnTarget(targetEntity)

end
