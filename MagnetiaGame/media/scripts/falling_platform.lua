-- FallingPlatform

FallingPlatform = {
	states = {
		sleeping = 0,
		active = 1,
		falling = 2,
		inactive = 3
	}
};

function FallingPlatformOnInit(entity)
	entity:getActor():setState(FallingPlatform.states.sleeping);
end

function FallingPlatformOnUpdate(dt, entity)
	local actor = entity:getActor();
	local elapsed = actor:stateElapsed();
 	local state = actor:getState();

	if ( (state == FallingPlatform.states.active) and (elapsed > actor:getTimeInActive()) ) then
		actor:setState(FallingPlatform.states.falling);
		actor:playFalling();
	elseif ( (state == FallingPlatform.states.falling) and (elapsed > actor:getTimeInFalling()) ) then
		actor:setState(FallingPlatform.states.inactive);
		actor:stop();
	elseif( (state == FallingPlatform.states.inactive) and (elapsed > actor:getTimeInInactive()) ) then
		getGameManager():getCurrentLevel():destroyEnemy(entity:getName());
	end
end

function FallingPlatformOnCollision(entity, other, hitPoint, hitNormal)
	local actor = entity:getActor();
	local state = actor:getState();
	
--  Only active tramp if player is on top
	if (hitNormal.y == 1 and state == FallingPlatform.states.sleeping) then
		local player = getPlayer();
		if (player:sameEntity(other)) then
			actor:setState(FallingPlatform.states.active);
		end
	end
end

function FallingPlatformOnCollisionEnter(entity, other)

end

function FallingPlatformOnCollisionExit(entity, other)
end

function FallingPlatformOnMagneticRayHit(targetEntity, originEntity, originPos, direction, pointWorld, force, charge)

end

function FallingPlatformOnMagneticRayTarget(targetEntity, originEntity, originPos, direction, pointWorld)

end

function FallingPlatformOnMagneticRayUnTarget(targetEntity)

end
