-------------------
----- MINSECT -----
-------------------

Minsect = {
	power = 5,
	life = 10,
	states = {
		wait_hidden = 0,
		show = 1,
		wait_visible = 2,
		attack = 3,
		death = 4
	},
	action_radius = 35,
	show_time = 0.7,
	attack_duration = 0.85,
	death_duration = 10
}

function MinsectDie(entity)
	local actor = entity:getActor();

	actor:dieAnimation(Minsect.death_duration);
	actor:setState(Minsect.states.death);
end

function MinsectOnInit(entity)
	entity:getActor():setState(Minsect.states.wait_hidden);
	entity:getGraphics():getNode():setVisible(false);
	entity:getPhysics():setCollisionEnabled(false);
end

function MinsectOnUpdate(dt, entity)
	local actor = entity:getActor();
	local state = actor:getState();
	local elapsed = actor:stateElapsed();
	local player = getPlayer();

	if (state == Minsect.states.wait_hidden) then

		local distance = entity:distanceToEntity(player);

		if (distance < Minsect.action_radius) then
			actor:show(Minsect.show_time);
			entity:getGraphics():getNode():setVisible(true);
			entity:getPhysics():setCollisionEnabled(true);
			actor:setState(Minsect.states.show);
		end

	elseif (state == Minsect.states.show) then

		if (elapsed > Minsect.show_time) then
			actor:setState(Minsect.states.wait_visible);
		end

	elseif (state == Minsect.states.wait_visible) then

		local visible = entity:canSeeEntity(player);

		if (visible) then
			actor:attack(Minsect.attack_duration);
			actor:setState(Minsect.states.attack);
		end

	elseif (state == Minsect.states.attack) then

		if (elapsed > Minsect.attack_duration) then
			MinsectDie(entity);
		end

	elseif (state == Minsect.states.death) then

		if (elapsed > Minsect.death_duration) then
			getGameManager():getCurrentLevel():destroyEnemy(entity:getName());
		end

	end
end

function MinsectOnCollision(entity, other, hitPoint, hitNormal)
	local player = getPlayer();

	if (player:sameEntity(other)) then
		player:getActor():kill();
	end

	local otherPhysics = other:getPhysics();
	if(otherPhysics ~= nil and not otherPhysics:hasTriggerCollision()) then
		MinsectDie(entity);
	end
end

function MinsectOnCollisionEnter(entity, other)

end

function MinsectOnCollisionExit(entity, other)

end

function MinsectOnMagneticRayHit(targetEntity, originEntity, originPos, direction, pointWorld, force, charge)
	local actor = targetEntity:getActor();
	local state = actor:getState();
	if (state ~= Minsect.states.wait_hidden) then
		MinsectDie(targetEntity);
	end
end

function MinsectOnMagneticRayTarget(targetEntity, originEntity, originPos, direction, pointWorld)

end

function MinsectOnMagneticRayUnTarget(targetEntity)

end
