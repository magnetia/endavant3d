
TYPE STATIC_MESH PROPERTIES
	<Node name="0">
		<Node name="type" value="static_mesh"/>			// Es carregara com a malla estatica al nivell
		<Node name="mesh" value="metal_tile_01.mesh"/>	// Fitxer de mesh que carregarem en aquesta tile
	</Node>
	
TYPE ACTOR PROPERTIES
	<Node name="0">
		<Node name="type" value="actor"/>
		<Node name="subtype" value="enemy"/>		// Subtipus perque la classe nivell sapiga quin tipus d'actor es
													// Valids: ("enemy" "item" "player")
		<Node name="entityID" value="Clashnik"/>   	// Aquest valor sera el que anira a buscar a l'XML d'entitats
	</Node>
		
		
		
KinematicController
{
	vec3 m_lateralaccelartion;
	vec3 m_currentposition;
	f64 m_currentvelocitylateral;
	f64 m_maxvelocitylateral;
}

Constructor:
{
	m_lateralaccelartion = 1.0;  //per defecte que acceleri a una unitat per segon2
	m_currentposition ={0,0,0}; //posicio del nik actual
	m_currentvelocitylateral = 0; //comença parat
	m_maxvelocitylateral = 4.0; //pot arribar fins a 4 unitats per segon
}
	
Update:
{
	if (left or right)  
	{
		m_currentvelocitylateral = m_currentvelocitylateral + m_lateralacceleration * dt;
		if ( m_currentvelocitylateral > m_maxvelocitylateral ) 
			m_currentvelocitylateral = m_maxvelocitylateral;
		m_currentposition = m_currentposition + (m_currentvelocitylateral * dt);
	
	}
	else
	{
		m_currentvelocitylateral = m_currentvelocitylateral - m_lateralacceleration * dt;
		if ( m_currentvelocitylateral < 0 ) 
			m_currentvelocitylateral = 0;
		m_currentposition = m_currentposition + (m_currentvelocitylateral * dt);
	}

}